// import { createGlobalStyle } from 'styled-components';

// const GlobalStyle = createGlobalStyle`
//   html,
//   body {
//     height: 100%;
//     width: 100%;
//     line-height: 1.5;
//   }

//   body {
//     font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
//   }
//   .header {
//     padding: 14px 0;
//     /* border-bottom: solid 1px rgba(40, 44, 71, 0.2); */
//     background-color: #ffffff;
//   }

//   .header .site-logo {
//     padding-top: 8px;
//     padding-bottom: 8px;
//   }

//   .header .help-btn {
//     border-radius: 2px;
//     border: solid 1px #4b70b8;
//     background-color: #ffffff;
//     font-family: "Encode Sans", sans-serif;
//     font-size: 16px;
//     font-weight: 500;
//     font-style: normal;
//     line-height: 30px;
//     text-align: center;
//     color: #4b70b8;
//     display: inline-block;
//     padding: 4px 15px;
//     min-width: 170px;
//     text-decoration: none;
//     -moz-transition: all 0.3s ease-in-out;
//     -webkit-transition: all 0.3s ease-in-out;
//     -ms-transition: all 0.3s ease-in-out;
//     transition: all 0.3s ease-in-out;
//   }

//   .header .help-btn:hover {
//     color: #ffffff;
//     background: #4b70b8;
//     text-decoration: none;
//   }

//   body.fontLoaded {
//     font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
//   }

//   #app {
//     background-color: #fafafa;
//     min-height: 100%;
//     min-width: 100%;
//   }

//   p,
//   label {
//     font-family: Georgia, Times, 'Times New Roman', serif;
//     line-height: 1.5em;
//   }
// `;

// export default GlobalStyle;

import { createGlobalStyle } from 'styled-components';
import footerImage from './images/bg-artwork.png';

const GlobalStyle = createGlobalStyle`


.ant-descriptions-item-label{
  font-weight:600 !important;
}

.ant-page-header-heading-sub-title {
  color: #1890ff !important;
 font-size: 20px !important; 

}
.ant-descriptions-title{
  margin-top:20px !important;
}

#components-layout-demo-responsive .logo {
  height: 32px;
  background: rgba(255, 255, 255, 0.2);
  margin: 16px;
}

.navbar-light{
  text-align:left !important;
  padding-left:10px !important;
}
.content-wrap{
  background: #f0f0f0;
  padding: 20px;
}
.detailsPrimaryButton {
  margin-right: 10px;
}
.content-wrap::before {
  left: 0 !important;
  background:#f0f0f0 !important;
  width: calc((100% - 1335px) / 2) !important;
  z-index:-1;
}
.ant-input-number{
  width:100% !important;
}
.ant-collapse-item-disabled{
  opacity:0.5 !important;
}

a {
  color: #1890ff !important;
  text-decoration: none; /* no underline */
}
.content-wrap::after {
  
  // width: calc((100% - 1335px) / 2) !important;
}

header{
  padding-left: 13px !important; 
}

nav{

  float: left !important;
}
.site-layout-sub-header-background {
  background: #fff;
}

.site-layout-background {
  margin: 2px 0px !important;
  background: #fff;
}


.ant-page-header-ghost {
  background-color: white !important; 
}

.loanDetails label {
  font-weight:600;
}
  html,
  body {
    height: 100%;
    width: 100%;
    line-height: 1.5;
  }
  body {
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }
  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  .img-fluid {
    max-width: 100%;
    height: auto;
    width: 5em;
}
  .profile{
    padding: 1rem;
    position: relative;
    background: linear-gradient(to right, red, purple);
    padding: 10px;
  }
  .sub-profile{
    background: white;
    color: black;
    padding: 2rem;
  }
  #app {
    background-color: #ffffff;
    min-height: 100%;
    min-width: 100%;
  }
  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }
  .header {
    padding: 14px 0;
    /* border-bottom: solid 1px rgba(40, 44, 71, 0.2); */
    background-color: #ffffff;
  }
  .header .site-logo {
    padding-top: 8px;
    padding-bottom: 8px;
  }
  .header .help-btn {
    border-radius: 2px;
    border: solid 1px #4b70b8;
    background-color: #ffffff;
    font-family: "Encode Sans", sans-serif;
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    line-height: 30px;
    text-align: center;
    color: #4b70b8;
    display: inline-block;
    padding: 4px 15px;
    min-width: 170px;
    text-decoration: none;
    -moz-transition: all 0.3s ease-in-out;
    -webkit-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
  }
  .header .help-btn:hover {
    color: #ffffff;
    background: #4b70b8;
    text-decoration: none;
  }
  /* End header */
  .App {
    /* min-height: 100vh; */
    padding: 0 20px 0 20px;
  }
  footer {
    position: absolute;
    height: 200px;
    bottom: 0;
    width: 100%;
  }
  /* content wrap */
  .content-wrap {
    position: relative;
    min-height: 57vh;
  }
  // .content-wrap::before,
  // .content-wrap::after {
  //   content: "";
  //   position: absolute;
  //   display: block;
  //   top: 0;
  //   bottom: 0;
  //   width: calc((100% - 1200px) / 2);
  // }
  .content-wrap::before {
    left: 0;
    background: #ffffff;
  }
  .content-wrap::after {
    right: 0;
    background: rgba(219, 241, 248, 0.06);
  }
.bgArtwork {
   background: url(${footerImage}) left bottom repeat-x;
  /* min-height: 100vh; */
}
.bgArtwork .modal-backdrop.show {
  opacity: 0.8 !important;
}
.flex-row {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-left: -15px;
  margin-right: -15px;
}
.flex-col-70 {
  -webkit-box-flex: 0;
  -ms-flex: 0 0 60%;
  flex: 0 0 60%;
  max-width: 60%;
  padding-left: 15px;
  padding-right: 15px;
  background: #ffffff;
}
.formTitle {
  font-weight: 600;
}
.headTitle h3.formTitle {
  font-size: 18px;
  font-weight: 700;
  text-align: center;
  display: block;
  width: 100%;
  margin-top: 20px;
  margin-bottom: 0;
}
.headTitle p {
  display: block;
  text-align: center;
  width: 100%;
}
.floating-form-group {
  position: relative;
  height: 40px;
}
.floating-form-group .form-control {
  width: 100%;
  top: 0;
  background: transparent;
  border: 0 none;
  outline: none;
  vertical-align: middle;
  z-index: 999;
  padding: 8px 8px 8px 4px;
  font-size: 16px;
  color: rgba(29, 52, 77, 1);
}
.floating-form-group .control-label {
  position: absolute;
  top: 5px;
  font-size: 14px;
  color: rgba(29, 52, 77, 0.4);
  left: 4px;
  transition: all 0.3s;
  pointer-events: none;
}
.floating-form-group .form-control:focus ~ .control-label,
.floating-form-group .form-control.notEmpty ~ .control-label {
  top: -7px;
  font-size: 11px;
  letter-spacing: 2px;
  pointer-events: none;
}
.floating-form-group .form-control:focus ~ .control-label.cstm-label,
.floating-form-group .form-control.notEmpty ~ .control-label.cstm-label {
  top: -9px;
  left: -28px;
  font-size: 11px;
  letter-spacing: 2px;
  pointer-events: none;
}
.floating-form-group .form-control:-webkit-autofill ~ .control-label {
  top: -7px;
  font-size: 11px;
  letter-spacing: 2px;
  pointer-events: none;
}
.ifSaved {
  top: -7px;
  font-size: 11px;
  letter-spacing: 2px;
  position: absolute;
  color: rgba(29, 52, 77, 0.4);
  left: 4px;
}
.floating-form-group .line {
  position: absolute;
  height: 1px;
  width: 100%;
  top: 36px;
  background: rgba(29, 52, 77, 1);
  left: 0;
  transition: all 0.5s ease-out;
}
.floating-form-group .form-control:focus {
  outline: 0;
  box-shadow: none;
}
.floating-form-group .form-control:focus ~ .line,
.floating-form-group .form-control.error ~ .line {
  background: #ea357e;
}
.floating-form-group .form-control.error ~ .errorMsg,
.select2DropUp.error .errorMsg {
  position: absolute;
  left: 5px;
  bottom: -15px;
  font-size: 11px;
  color: #ff0000;
}
.floating-form-group .line {
  position: absolute;
  height: 1px;
  width: 100%;
  top: 36px;
  background: rgba(29, 52, 77, 1);
  left: 0;
  transition: all 0.5s ease-out;
}
.floating-form-group .form-control:focus {
  outline: 0;
  box-shadow: none;
}
.floating-form-group .form-control:focus ~ .line,
.floating-form-group .form-control.error ~ .line {
  background: #ea357e;
}
.btnLink1 {
  color: #1d344d;
  /* border-bottom: solid 1px rgba(29, 52, 77, 0.3); */
  padding-left: 4px;
  padding-top: 0;
  padding-bottom: 0;
  margin-left: 12px;
  font-size: 14px;
  margin-bottom: 20px;
}
.btnLink1 {
  font-size: 13px;
  border-bottom: 0;
  padding: 0;
  text-decoration: none !important;
}
.floating-form-group .form-control.error ~ .errorMsg,
.select2DropUp.error .errorMsg {
  position: absolute;
  left: 5px;
  bottom: -15px;
  font-size: 11px;
  color: #ff0000;
}
.errorMsg {
  position: absolute;
  left: 5px;
  bottom: -15px;
  font-size: 12px;
  color: #ff0000;
}
.errorMsgForLongText {
  position: absolute;
  left: 5px;
  bottom: -30px;
  font-size: 12px;
  color: #ff0000;
}
.ant-space-item .tagStyle{
  font-size: 14px;
  padding: 4px;
}
.ant-space-item .tagStyle .anticon{
  font-size: 17px;
  color: red;
}
.ant-btn[disabled], .ant-input[disabled]{
  color: darkgrey !important;
}
 .no-border .ant-card-bordered{
  border: none;
}
.width-limit .ant-table-content{
   max-width: 65vw;
}
`;
export default GlobalStyle;
