import moment from 'moment';
import React, { Fragment } from 'react';
import _ from 'lodash';
const stringSimilarity = require('string-similarity');

export const formatDate = (
  date,
  inputFormat = 'YYYY-MM-DD',
  outputFormat = 'DD-MM-YYYY',
) => {
  const inputDate = moment(date, inputFormat);
  if (inputDate.isValid()) {
    return inputDate;
  }
  return '';
};

export const revertFormatDate = (
  date,
  outputFormat = 'YYYY-MM-DD',
  inputFormat = 'DD-MM-YYYY',
) => {
  const inputDate = moment(date, inputFormat);
  if (inputDate.isValid()) {
    return inputDate.format(outputFormat);
  }
  return '';
};

export const stringSimilar = (string1, string2) => {
  const similarity = stringSimilarity.compareTwoStrings(
    string1.toUpperCase(),
    string2.toUpperCase(),
  );

  return similarity;
};

export function* downloadFile(data, filename, mime) {
  // It is necessary to create a new blob object with mime-type explicitly set
  // otherwise only Chrome works like it should
  const byteCharacters = atob(data);
  const byteNumbers = new Array(byteCharacters.length);
  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  const byteArray = new Uint8Array(byteNumbers);
  const blob = new Blob([byteArray], {
    type: mime || 'application/octet-stream',
  });
  if (typeof window.navigator.msSaveBlob !== 'undefined') {
    // IE doesn't allow using a blob object directly as link href.
    // Workaround for "HTML7007: One or more blob URLs were
    // revoked by closing the blob for which they were created.
    // These URLs will no longer resolve as the data backing
    // the URL has been freed."
    window.navigator.msSaveBlob(blob, filename);
    return;
  }
  // Other browsers
  // Create a link pointing to the ObjectURL containing the blob
  const blobURL = window.URL.createObjectURL(blob);
  const tempLink = document.createElement('a');
  tempLink.style.display = 'none';
  tempLink.href = blobURL;
  tempLink.setAttribute('download', filename);
  // Safari thinks _blank anchor are pop ups. We only want to set _blank
  // target if the browser does not support the HTML5 download attribute.
  // This allows you to download files in desktop safari if pop up blocking
  // is enabled.
  if (typeof tempLink.download === 'undefined') {
    tempLink.setAttribute('target', '_blank');
  }
  document.body.appendChild(tempLink);
  tempLink.click();
  document.body.removeChild(tempLink);
  // yield put(downloadNachFormSuccess(true));
  setTimeout(() => {
    // For Firefox it is necessary to delay revoking the ObjectURL
    window.URL.revokeObjectURL(blobURL);
  }, 100);
}
export const formatNumber = number =>
  number.toLocaleString(
    undefined, // leave undefined to use the browser's locale,
    // or use a string like 'en-US' to override it.
    { minimumFractionDigits: 2 },
  );

export const formatToCurrency = (number, currency = 'en-IN') => {
  const numb = (number || 0).toFixed(2);
  return (
    <Fragment>&nbsp; ₹{new Intl.NumberFormat(currency).format(numb)}</Fragment>
  );
};

// this function checks if object has any value non-empty
export const isEmpty = object =>
  !Object.values(object).some(x => x !== null && x !== '' && x !== undefined);

// this function decides salutation based on gender and marital status
export const getSalutation = (gender, maritalStatus) => {
  const genderCaps = _.upperCase(gender);
  const maritalStatusCaps = _.upperCase(maritalStatus);
  switch (genderCaps) {
    case 'MALE':
      return 'MR';
    case 'FEMALE': {
      if (maritalStatusCaps === 'SINGLE') return 'MS';
      return 'MRS';
    }
    default:
      return '';
  }
};
