export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const LoanDetailsDeclarations = [
  ' I/we hereby certify that information provided in the application form for the purpose of loan under Emergency Credit Line Guarantee Scheme are true and correct, and any change in information from documents submitted for KYC (Know your customer) for earlier loan facility has been appropriately highlighted.',
  ' I/we agree to provide documents supporting any change in said information.',
  "I/we also certify that the annual turnover as of Mar'31 2020 for entity is correct",
  'I/we shall initiate the process of preparing audited financials for the same at the earliest (if not done already). I/we certify that the current credit exposure (outstanding loan) from all the banks has been duly mentioned above and cumulatively is less that INR 25 Cr as 29th Feb, 2020.',
  ,
  'I/we hereby authorize/give consent to Clix Capital to disclose information provided by me/us in the application form and related documents, without prior notice to me/us, to its branches, credit bureau, rating agencies, government/regulatory authorities for the purpose of credit analysis, regulatory bureau reporting, KYC document verification and other related purposes',
];

export const STATUS_WISE_COLOR = {
  active: '#108ee9',
  approved: '#87d068',
  rejected: '#f50',
  send_back: '#FFFF66',
  auto_approved: '#87d068',
  auto_rejected: '#f50',
};

export const numberPattern = {
  value: /^(0|[1-9][0-9]*)$/,
  message: 'Invalid Number',
};

export const SALUTATION = ['Dr', 'Er', 'MR', 'MRS', 'MS', 'CA'];

export const MARITAL_STATUS = ['SINGLE', 'MARRIED'];

export const REFERENCE_RELATIONSHIP_FAMILY = [
  'Father',
  'Mother',
  'Brother',
  'Sister',
  'Spouse',
  'Son',
  'Daughter',
  'Father In Law',
  'Mother In Law',
];

export const REFERENCE_RELATIONSHIP_ANY = [
  'Father',
  'Mother',
  'Brother',
  'Sister',
  'Spouse',
  'Son',
  'Daughter',
  'Father In Law',
  'Mother In Law',
  'Friends',
  'Relatives',
  'Son In Law',
  'Others',
];

export const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

export const columnResponsiveLayout = {
  colSingle: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
  colHalf: { xs: 24, sm: 24, md: 24, lg: 12, xl: 10 },
  colOneThird: { xs: 24, sm: 24, md: 24, lg: 8, xl: 8 },
};

export const DESIGNATION = [
  'Management trainer',
  'Officer',
  'Senior officer',
  'Executive',
  'Senior executive',
  'Associate',
  'Senior associate',
  'Assistant manager',
  'Deputy manager',
  'Manager',
  'Senior manager',
  'Deputy general manager',
  'General manager',
  'Assistant Vice President',
  'Deputy Vice President',
  'Vice President',
  'Senior Vice President',
  'President',
  'Senior president',
  'CEO',
  'COO',
  'CXO',
  'Director',
  'Chairman',
  'Vice chairman',
  'Founder',
  'Managing director',
  'Others',
];

export const EXPERIENCE_YEARS = [
  '< 1',
  '1 - 2',
  '3 - 4',
  '5 - 6',
  '7 - 8',
  '9 - 10',
  '11 - 12',
  '13 - 14',
  '15 - 16',
  '17 - 18',
  '19 - 20',
  '> 20',
];

export const COMPANY_TYPE = [
  'Private Sector',
  'Public Sector',
  'Proprietorship',
  'Partnership',
  'Government',
  'Others',
];

export const employmentTypeArray = [
  'SALARIED',
  'CONSULTANT',
  'SELF-EMPLOYED',
  'UNEMPLOYED',
];

export const DoctorEmploymentTypeArray = [
  'Self employed Doctors',
  'Consultant Doctors',
  'Others',
];

export const INDUSTRY_TYPE = [
  'Education',
  'Pharmaceuticals',
  'Telecom_and_Telecom_Products',
  'Software/Hardware & IT',
  'Engineering & Automotive',
  'Travel and Hospitality (Aviation,tour,hotel & travel)',
  'Banking, Finance & insurance',
  'Pharma & healthcare',
  'BPO, KPO and ITES',
  'Core Sector (oil,gas, power,steel ,mineral,etc)',
  'Manufacturing',
  'Entertainment',
  'Telecom',
  'Others',
];

export const STATE_GSTS = {
  '01': 'JAMMU AND KASHMIR',
  '02': 'HIMACHAL PRADESH',
  '03': 'PUNJAB',
  '04': 'CHANDIGARH',
  '05': 'UTTARANCHAL',
  '06': 'HARYANA',
  '07': 'DELHI',
  '08': 'RAJASTHAN',
  '09': 'UTTAR PRADESH',
  '10': 'BIHAR',
  '11': 'SIKKIM',
  '12': 'ARUNACHAL PRADESH',
  '13': 'NAGALAND',
  '14': 'MANIPUR',
  '15': 'MIZORAM',
  '16': 'TRIPURA',
  '17': 'MEGHLAYA',
  '18': 'ASSAM',
  '19': 'WEST BENGAL',
  '20': 'JHARKHAND',
  '21': 'ODISHA',
  '22': 'CHhATTISGARH',
  '23': 'MADHYA PRADESH',
  '24': 'GUJARAT',
  '26': 'DAMAN AND DIU',
  '27': 'MAHARASHTRA',
  '28': 'ANDHRA PRADESH',
  '29': 'KARNATAKA',
  '30': 'GOA',
  '31': 'LAKSHWADEEP',
  '32': 'KERALA',
  '33': 'TAMIL NADU',
  '34': 'PONDICHERRY',
  '35': 'ANDAMAN AND NICOBAR ISLANDS',
  '36': 'TELANGANA',
};

export const ENABLED_FIELDS_GAM = [
  'brand',
  'assetType',
  'assetPrice',
  'assetCondition',
  'assetUnits',
  'marginMoney',
  'assetDeployment',
  'supplierName',
  'address',
  'valuation1',
  'collateralLocation',
  'valuation2',
  'collateralType',
  'supplierTextValue',
  'ltv',
];

export const HFS_ADDRESSES = [
  {
    code: 'OFFICE',
    value: 'Office Address',
  },
  {
    code: 'CURRENT',
    value: 'Current Residential Address',
  },
  {
    code: 'PERMANENT',
    value: 'Permanent Address',
  },
];

export const EMAIL_CATEGORY = [
  {
    code: 'CC_EMAIL_ID',
    value: 'CC Email ID',
  },
  {
    code: 'BCC_EMAIL_ID',
    value: 'BCC Email ID',
  },
  {
    code: 'SENDER_EMAIL_ID',
    value: 'Sender Email ID',
  },
];

export const DEFAULT_MAIL_ID = [
  {
    code: 'Y',
    value: 'Yes',
  },
  {
    code: 'N',
    value: 'No',
  },
];

// export const ACTIVE = [
//   {
//     code: 'Y',
//     value: 'Yes'
//   },
//   {
//     code: 'N',
//     value: 'No'
//   },
// ];

export const ADDRESSES = ['CURRENT', 'PERMANENT', 'OFFICE'];
