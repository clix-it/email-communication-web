import React, { useEffect } from 'react';
import { Row, Col } from 'antd';
import _ from 'lodash';
import InputField from 'components/InputField';
import { MARITAL_STATUS } from '../../utils/constants';

function Relationships({
  responsiveColumns,
  values,
  userIndex,
  control,
  errors,
  setValue,
  clearError,
  allFieldsDisabled,
  applicant,
  isGuarantor,
}) {
  useEffect(() => {
    const fValue = _.get(applicant, 'userDetails.fatherName');
    const motherValue = _.get(applicant, `userDetails.motherName`);
    const maritalValue = _.get(applicant, `userDetails.maritalStatus`);
    if (isGuarantor) {
      setValue(`guarantors[${userIndex}].userDetails.fatherName`, fValue);
      setValue(`guarantors[${userIndex}].userDetails.motherName`, motherValue);
      setValue(
        `guarantors[${userIndex}].userDetails.maritalStatus`,
        maritalValue,
      );
    } else {
      setValue(`applicants[${userIndex}].userDetails.fatherName`, fValue);
      setValue(`applicants[${userIndex}].userDetails.motherName`, motherValue);
      setValue(
        `applicants[${userIndex}].userDetails.maritalStatus`,
        maritalValue,
      );
    }

    if (fValue) {
      if (isGuarantor) {
        clearError(`guarantors[${userIndex}].userDetails.fatherName`);
      } else {
        clearError(`applicants[${userIndex}].userDetails.fatherName`);
      }
    }
    if (motherValue) {
      if (isGuarantor) {
        clearError(`guarantors[${userIndex}].userDetails.motherName`);
      } else {
        clearError(`applicants[${userIndex}].userDetails.motherName`);
      }
    }
    if (maritalValue) {
      if (isGuarantor) {
        clearError(`guarantors[${userIndex}].userDetails.maritalStatus`);
      } else {
        clearError(`applicants[${userIndex}].userDetails.maritalStatus`);
      }
    }
  }, [
    _.get(applicant, `userDetails.fatherName`),
    _.get(applicant, `userDetails.motherName`),
    _.get(applicant, `userDetails.maritalStatus`),
  ]);
  return (
    <Row gutter={[16, 16]}>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userDetails.fatherName`
              : `guarantors[${userIndex}].userDetails.fatherName`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userDetails.fatherName`
              : `guarantors[${userIndex}].userDetails.fatherName`
          }
          type="string"
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
            pattern: {
              value: /^[a-zA-Z]+(?:[\s][a-zA-Z]+)*$/,
              message: 'Please fill in the correct name!',
            },
          }}
          errors={errors}
          placeholder="Father's Name"
          labelHtml="Father's Name"
          disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userDetails.motherName`
              : `guarantors[${userIndex}].userDetails.motherName`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userDetails.motherName`
              : `guarantors[${userIndex}].userDetails.motherName`
          }
          type="string"
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
            pattern: {
              value: /^[a-zA-Z]+(?:[\s][a-zA-Z]+)*$/,
              message: 'Please fill in the correct name!',
            },
          }}
          errors={errors}
          placeholder="Mother's Name"
          labelHtml="Mother's Name"
          disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userDetails.maritalStatus`
              : `guarantors[${userIndex}].userDetails.maritalStatus`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userDetails.maritalStatus`
              : `guarantors[${userIndex}].userDetails.maritalStatus`
          }
          type="select"
          options={MARITAL_STATUS}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Marital Status"
          labelHtml="Marital Status"
          disabled={allFieldsDisabled}
        />
      </Col>
    </Row>
  );
}

export default Relationships;
