import React, { useEffect, memo } from 'react';
import PropTypes from 'prop-types';
import { Col } from 'antd';
import _ from 'lodash';
import InputField from '../InputField';
import {
  PRODUCT,
  columnResponsiveLayout,
} from '../../containers/LoanDetails/constants';

function LoanDetailsProduct({
  control,
  errors,
  product,
  partner,
  finalLoanAmount,
  exposure,
  setValue,
  values,
}) {
  const DEVIATION_ARR_BL = [
    { lowestAmt: 0, highestAmt: 1500000, deviation: 'L1' },
    { lowestAmt: 1500000, highestAmt: 3000000, deviation: 'L3' },
    { lowestAmt: 3000000, highestAmt: 5000000, deviation: 'L4' },
    // { lowestAmt: 5000000, highestAmt: 6000000, deviation: 'L5' },
  ];

  const DEVIATION_ARR_LAEP = [
    { lowestAmt: 0, highestAmt: 1000000, deviation: 'L1' },
    { lowestAmt: 1000000, highestAmt: 2000000, deviation: 'L3' },
    { lowestAmt: 2000000, highestAmt: 5000000, deviation: 'L4' },
    // { lowestAmt: 5000000, highestAmt: 6000000, deviation: 'L5' },
  ];

  useEffect(() => {
    let deviationValue = '';
    const loanAmount = parseInt(finalLoanAmount, 10) + parseInt(exposure, 10);
    if (product === 'HFS' && partner === 'HFSAPP' && loanAmount > 5000000) {
      deviationValue = 'L5';
    }
    if (product === 'BL' && partner === 'D2C') {
      // deviationObj['Eligible Amount'] = 'L4';
      deviationValue = _.get(
        _.find(
          product === 'BL' ? DEVIATION_ARR_BL : DEVIATION_ARR_LAEP,
          d => loanAmount > d.lowestAmt && loanAmount <= d.highestAmt,
        ),
        'deviation',
        'L5',
      );
      // console.log('deviationValue', deviationValue);
    }
    if (deviationValue)
      setValue('additionalData.data.deviations', {
        ..._.get(values, 'additionalData.data.deviations', {}),
        'Eligible Amount': deviationValue,
      });
  }, [product, finalLoanAmount, exposure]);

  // console.log('laon comp product', product, finalLoanAmount, exposure);
  return (
    <>
      <Col {...columnResponsiveLayout.colHalf}>
        <InputField
          showSearch
          type="select"
          id="product"
          name="product"
          rules={{
            required: {
              value: true,
              message: 'This field cannot be left empty',
            },
          }}
          control={control}
          errors={errors}
          options={PRODUCT}
          placeholder="Product"
          labelHtml="Product*"
        />
      </Col>
      <Col {...columnResponsiveLayout.colHalf}>
        <InputField
          showSearch
          type="string"
          id="partner"
          name="partner"
          defaultValue={partner}
          disabled
          rules={{
            required: {
              value: true,
              message: 'This field cannot be left empty',
            },
          }}
          control={control}
          errors={errors}
          placeholder="Partner"
          labelHtml="Partner*"
        />
      </Col>
    </>
  );
}

LoanDetailsProduct.propTypes = {};

export default memo(LoanDetailsProduct);
