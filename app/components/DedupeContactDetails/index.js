/**
 *
 * DedupeContactDetails
 *
 */
import _ from 'lodash';
import { Row, Col, Typography } from 'antd';
import React, { memo } from 'react';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

const { Text, Title } = Typography;

function DedupeContactDetails({ defaultValues, isNoEntity }) {
  return (
    <Row gutter={[16, 16]}>
      {isNoEntity ? (
        <>
          <Col {...responsiveColumns}>
            <Text strong>First Name : </Text>
            <Text>
              {defaultValues.firstName ? defaultValues.firstName : ''}
            </Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>Last Name : </Text>
            <Text>{defaultValues.lastName ? defaultValues.lastName : ''}</Text>
          </Col>
        </>
      ) : null}
      <Col {...responsiveColumns}>
        <Text strong>Registered Mobile Number : </Text>
        <Text>
          {defaultValues.preferredPhone
            ? defaultValues.preferredPhone
            : defaultValues.contactibilities
            ? defaultValues.contactibilities[0].phoneNumber
            : ''}
        </Text>
      </Col>

      <Col {...responsiveColumns}>
        <Text strong>Registered Email : </Text>
        <Text>
          {defaultValues.preferredEmail
            ? defaultValues.preferredEmail
            : defaultValues.contactibilities
            ? defaultValues.contactibilities[0].email
            : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>PAN : </Text>
        <Text>
          {defaultValues.identities ? defaultValues.identities.pan || '' : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>CKYC Number : </Text>
        <Text>
          {defaultValues.identities ? defaultValues.identities.ckycNumber || '' : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Aadhar : </Text>
        <Text>
          {defaultValues.identities ? defaultValues.identities.aadhar || '' : ''}
        </Text>
      </Col>      
      <Col {...responsiveColumns}>
        <Text strong>Contact number of office : </Text>
        <Text>
          {defaultValues.preferredPhone ? defaultValues.preferredPhone : ''}
        </Text>
      </Col>
    </Row>
  );
}

DedupeContactDetails.propTypes = {};

export default memo(DedupeContactDetails);
