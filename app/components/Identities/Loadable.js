/**
 *
 * Asynchronously loads the component for Identities
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
