import React, { memo } from 'react';
import { Col } from 'antd';
import InputField from 'components/InputField';

function Identities({
  responsiveColumns,
  userIndex,
  control,
  errors,
  allFieldsDisabled,
  values,
  isGuarantor,
}) {
  console.log('values', values);
  return (
    <>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.aadhar`
              : `guarantors[${userIndex}].userIdentities.aadhar`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.aadhar`
              : `guarantors[${userIndex}].userIdentities.aadhar`
          }
          type="string"
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="AADHAR Number"
          labelHtml="AADHAR Number"
          disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.voterId`
              : `guarantors[${userIndex}].userIdentities.voterId`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.voterId`
              : `guarantors[${userIndex}].userIdentities.voterId`
          }
          type="string"
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Voter Id"
          labelHtml="Voter Id"
          disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.ckycNumber`
              : `guarantors[${userIndex}].userIdentities.ckycNumber`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.ckycNumber`
              : `guarantors[${userIndex}].userIdentities.ckycNumber`
          }
          type="string"
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="CKYC No."
          labelHtml="CKYC No."
          disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.drivingLicense`
              : `guarantors[${userIndex}].userIdentities.drivingLicense`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.drivingLicense`
              : `guarantors[${userIndex}].userIdentities.drivingLicense`
          }
          type="string"
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Driving License"
          labelHtml="Driving License"
          disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.passport`
              : `guarantors[${userIndex}].userIdentities.passport`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userIdentities.passport`
              : `guarantors[${userIndex}].userIdentities.passport`
          }
          type="string"
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Passport"
          labelHtml="Passport"
          disabled={allFieldsDisabled}
        />
      </Col>
    </>
  );
}

export default memo(Identities);
