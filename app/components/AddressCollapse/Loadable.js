/**
 *
 * Asynchronously loads the component for AddressCollapse
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
