/**
 *
 * AddressCollapse
 *
 */

import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import Pincode from 'containers/Pincode';
import InputField from 'components/InputField';
import { Row, Col, Collapse } from 'antd';
import moment from 'moment';

const { Panel } = Collapse;

const CollapseWrapper = styled.div`
  margin-bottom: 2em;
`;

function AddressCollapse({
  responsiveColumns,
  userIndex,
  control,
  errors,
  allFieldsDisabled,
  reset,
  values,
  setValue,
  getValues,
  setError,
  clearError,
  contactArr,
}) {
  const [contactTypeArr, setContactTypeArr] = useState([]);
  useEffect(() => {
    if (Object.keys(contactArr) && Object.keys(contactArr).length > 0) {
      setContactTypeArr(Object.keys(contactArr));
    } else {
      setContactTypeArr([]);
    }
  }, [contactArr]);
  return (
    <CollapseWrapper>
      <Collapse>
        {contactTypeArr.length > 0
          ? contactTypeArr.map((contactType, index) => (
              <Panel
                header={`${
                  contactType != 'undefined' ? contactType : 'Other'
                } Address`}
                key={`${index}`}
                extra={
                  contactArr[contactType]
                    ? `Address Added On - ${moment(
                        contactArr[contactType][0]['createdAt'],
                      ).format('DD-MM-YYYY')}`
                    : ''
                }
              >
                <Row gutter={[16, 16]}>
                  <Col {...responsiveColumns}>
                    <InputField
                      id={`contactArr[${contactType}][0]['addressLine1']`}
                      control={control}
                      name={`contactArr[${contactType}][0]['addressLine1']`}
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['addressLine1']
                          : ''
                      }
                      type="string"
                      rules={{
                        required: {
                          value: true,
                          message: 'This field cannot be left empty',
                        },
                      }}
                      errors={errors}
                      placeholder="Address Line 1"
                      labelHtml="Address Line 1*"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      id={`contactArr[${contactType}][0]['addressLine2']`}
                      control={control}
                      name={`contactArr[${contactType}][0]['addressLine2']`}
                      type="string"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['addressLine2']
                          : ''
                      }
                      rules={{
                        required: {
                          value: true,
                          message: 'This field cannot be left empty',
                        },
                      }}
                      errors={errors}
                      placeholder="Address Line 2"
                      labelHtml="Address Line 2*"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      id={`contactArr[${contactType}][0]['addressLine3']`}
                      control={control}
                      name={`contactArr[${contactType}][0]['addressLine3']`}
                      type="string"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['addressLine3']
                          : ''
                      }
                      rules={{
                        required: {
                          value: false,
                          message: 'This field cannot be left empty',
                        },
                      }}
                      errors={errors}
                      placeholder="Address Line 3"
                      labelHtml="Address Line 3"
                      disabled={true}
                    />
                  </Col>
                  {/*<Pincode
                    namePrefix={`contactArr[${contactType}][0]`}
                    control={control}
                    reset={reset}
                    values={values}
                    setValue={setValue}
                    getValues={getValues}
                    setError={setError}
                    defaultValue={
                      contactArr[contactType] ? contactArr[contactType][0] : ''
                    }
                    clearError={clearError}
                    pincode={
                      contactArr[contactType]
                        ? _.get(values, contactArr[contactType][0]['pincode'])
                        : ''
                    }
                    type="string"
                    rules={{
                      required: true,
                      message: 'This field is required',
                      pattern: {
                        value: /[1-9][0-9]{5}/i,
                        message: 'Please enter correct pincode',
                      },
                    }}
                    errors={errors}
                    disabled={true}
                  />*/}
                  <Col {...responsiveColumns}>
                    <InputField
                      type="text"
                      className="form-control"
                      control={control}
                      id={`contactArr[${contactType}][0]['pincode']`}
                      name={`contactArr[${contactType}][0]['pincode']`}
                      placeholder="Pincode"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['pincode']
                          : ''
                      }
                      errors={errors}
                      type="string"
                      rules={{
                        required: true,
                        message: 'This field is required',
                        pattern: {
                          value: /[1-9][0-9]{5}/i,
                          message: 'Please enter correct pincode',
                        },
                      }}
                      placeholder="Pincode"
                      labelHtml="Pincode"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      type="text"
                      className="form-control"
                      control={control}
                      id={`contactArr[${contactType}][0]['locality']`}
                      name={`contactArr[${contactType}][0]['locality']`}
                      placeholder="Locality"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['locality']
                          : ''
                      }
                      errors={errors}
                      type="string"
                      rules={{
                        required: true,
                        message: 'This field is required',
                        pattern: {
                          value: /[1-9][0-9]{5}/i,
                          message: 'Please enter correct pincode',
                        },
                      }}
                      placeholder="Locality"
                      labelHtml="Locality"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      type="text"
                      className="form-control"
                      control={control}
                      id={`contactArr[${contactType}][0]['city']`}
                      name={`contactArr[${contactType}][0]['city']`}
                      placeholder="City"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['city']
                          : ''
                      }
                      errors={errors}
                      type="string"
                      rules={{
                        required: true,
                        message: 'This field is required',
                        pattern: {
                          value: /[1-9][0-9]{5}/i,
                          message: 'Please enter correct pincode',
                        },
                      }}
                      placeholder="City"
                      labelHtml="City"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      type="text"
                      className="form-control"
                      control={control}
                      id={`contactArr[${contactType}][0]['state']`}
                      name={`contactArr[${contactType}][0]['state']`}
                      placeholder="State"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['state']
                          : ''
                      }
                      errors={errors}
                      type="string"
                      rules={{
                        required: true,
                        message: 'This field is required',
                        pattern: {
                          value: /[1-9][0-9]{5}/i,
                          message: 'Please enter correct pincode',
                        },
                      }}
                      placeholder="State"
                      labelHtml="State"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      type="text"
                      className="form-control"
                      control={control}
                      id={`contactArr[${contactType}][0]['country']`}
                      name={`contactArr[${contactType}][0]['country']`}
                      placeholder="Country"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['country']
                          : 'India'
                      }
                      errors={errors}
                      type="string"
                      rules={{
                        required: true,
                        message: 'This field is required',
                        pattern: {
                          value: /[1-9][0-9]{5}/i,
                          message: 'Please enter correct pincode',
                        },
                      }}
                      placeholder="Country"
                      labelHtml="Country"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      type="text"
                      className="form-control"
                      control={control}
                      id={`contactArr[${contactType}][0]['phoneNumber']`}
                      name={`contactArr[${contactType}][0]['phoneNumber']`}
                      placeholder="Phone number"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['phoneNumber']
                          : 'India'
                      }
                      errors={errors}
                      type="string"
                      rules={{
                        required: true,
                        message: 'This field is required',
                        pattern: {
                          value: /[1-9][0-9]{5}/i,
                          message: 'Please enter correct phone number',
                        },
                      }}
                      placeholder="Phone Number"
                      labelHtml="Phone Number"
                      disabled={true}
                    />
                  </Col>
                  <Col {...responsiveColumns}>
                    <InputField
                      type="text"
                      className="form-control"
                      control={control}
                      id={`contactArr[${contactType}][0]['email']`}
                      name={`contactArr[${contactType}][0]['email']`}
                      placeholder="Email"
                      defaultValue={
                        contactArr[contactType]
                          ? contactArr[contactType][0]['email']
                          : 'India'
                      }
                      errors={errors}
                      type="string"
                      rules={{
                        required: true,
                        message: 'This field is required',
                        pattern: {
                          value: /[1-9][0-9]{5}/i,
                          message: 'Please enter correct email',
                        },
                      }}
                      placeholder="Email"
                      labelHtml="Email"
                      disabled={true}
                    />
                  </Col>
                </Row>
              </Panel>
            ))
          : null}
      </Collapse>
    </CollapseWrapper>
  );
}

AddressCollapse.propTypes = {};

export default memo(AddressCollapse);
