import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { Tabs, Card, Row, Col } from 'antd';
import SendBack from 'containers/SendBack';
import PropTypes from 'prop-types';
import PopUp from '../PopUp/index';

import './style.css';

const { TabPane } = Tabs;

const TAB_PANELS = [
  {
    id: 0,
    name: 'Tab-1',
    component: <div>Tab 1</div>,
    tabErrors: {},
    requestType: 'PMA',
  },
  {
    id: 1,
    name: 'Tab-2',
    component: <div>Tab 2</div>,
    tabErrors: {},
    requestType: 'PMA',
  },
];

function tabs({
  removeFormDirty,
  formDirty,
  mode,
  tabPanel,
  hideEntity,
  ifSendBack,
  navigateToDocumentTab,
  isUserChanged,
  tabBarExtraContent,
}) {
  const [showPopUp, setShowPopUp] = useState(false);
  const [currentTab, setCurrentTab] = useState(tabPanel[0].id);
  const [newTabKey, setNewTabKey] = useState('0');

  useEffect(() => {
    setCurrentTab(tabPanel[0].id);
  }, [hideEntity]);

  useEffect(() => {
    if (isUserChanged) {
      setCurrentTab(tabPanel[0].id);
    }
  }, [isUserChanged]);

  useEffect(() => {
    if (navigateToDocumentTab) {
      setCurrentTab('documents');
    }
  }, [navigateToDocumentTab]);

  const handleOk = () => {
    setCurrentTab(newTabKey);
    setShowPopUp(false);

    // removeFormDirty();
  };
  const handleCancel = () => {
    setShowPopUp(false);
  };
  const handleTabChange = key => {
    setNewTabKey(key);
    if (formDirty && formDirty[currentTab]) {
      setShowPopUp(true);
    } else {
      setCurrentTab(key);
    }
  };

  const getTabColor = () => {
    if (tabPanel[0].requestType !== 'PMA') return;
    let children = Array.from(
      document.getElementsByClassName('ant-tabs-tab-btn'),
    );
    if (!children || children.length === 0) {
      children = Array.from(document.getElementsByClassName('ant-tabs-tab'));
    }
    if (!children && children.length === 0) return;
    // console.log('called', children);
    children.forEach(child => {
      const name = child.id.split('-');
      if (!name && name.length === 0) return;
      let isValid = _.get(tabPanel[0].tabErrors, name[name.length - 1]);
      const ifKeyExists = _.has(tabPanel[0].tabErrors, name[name.length - 1]);
      // debugger;
      if (
        (name[name.length - 1] === 'applicantDetails' ||
          name[name.length - 1] === 'entityDetails' ||
          name[name.length - 1] === 'guarantorDetails') &&
        ifKeyExists
      ) {
        isValid = Object.values(
          tabPanel[0].tabErrors[name[name.length - 1]],
        ).every(applicantError => applicantError === true);
      }
      if (ifKeyExists && !isValid) child.style.color = 'red';
      if (ifKeyExists && isValid) child.style.color = 'limegreen';
    });
  };

  return (
    <>
      <Card
        bordered={false}
        className={mode === 'top' ? 'card-no-padding' : ''}
      >
        {/* {ifSendBack && (
          <Row>
            <Col push="20">
              <SendBack />
            </Col>
          </Row>
        )} */}
        <Tabs
          defaultActiveKey={tabPanel[0].id}
          activeKey={currentTab}
          tabPosition={mode}
          className={`tab-container${getTabColor()}`}
          onTabClick={key => handleTabChange(key)}
          tabBarExtraContent={tabBarExtraContent}
        >
          {tabPanel.map(panel => (
            <TabPane tab={panel.name} key={panel.id}>
              {panel.component}
            </TabPane>
          ))}
        </Tabs>
      </Card>
      <PopUp
        handleOk={handleOk}
        handleCancel={handleCancel}
        visible={showPopUp}
        textAreaLabel="Have you saved the details for current form?"
        title="Saved details?"
        isTabChange
      />
    </>
  );
}

tabs.propTypes = {
  mode: PropTypes.string,
  tabPanel: PropTypes.array,
  ifSendBack: PropTypes.bool,
};

tabs.defaultProps = {
  mode: 'left',
  tabPanel: TAB_PANELS,
  ifSendBack: false,
};

export default tabs;
