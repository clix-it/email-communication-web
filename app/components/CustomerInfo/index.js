/**
 *
 * CustomerInfo
 *
 */

import _ from 'lodash';
import React, { memo } from 'react';
import { Descriptions, Spin } from 'antd';
import DedupeAddressCollapse from 'components/DedupeAddressCollapse';
import moment from 'moment';

function CustomerInfo({ user, loading }) {
  let contactDetails = {};
  let contactabilities = _.orderBy(user.contactibilities, ['id'], ['desc']);
  console.log('contactabilities', contactabilities);
  if (user && user.cuid) {
    let arr =
      user.contactibilities &&
      user.contactibilities.filter(item => item.addressLine1);
    contactDetails = _.mapValues(
      _.groupBy(_.orderBy(arr, ['id'], ['desc']), 'contactType'),
      clist => clist.map(contact => _.omit(contact, 'contactType')),
    );
  }
  return (
    <Spin spinning={loading} tip="Loading...">
      <Descriptions
        title={`Customer Information`}
        column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        bordered
        layout="vertical"
        size="small"
      >
        {user && user.type === 'INDIVIDUAL' ? (
          <>
            <Descriptions.Item label="First Name">
              {user && user.firstName}
            </Descriptions.Item>
            <Descriptions.Item label="Last Name">
              {user && user.lastName}
            </Descriptions.Item>
          </>
        ) : (
          <Descriptions.Item label="Registered Name">
            {user && user.registeredName}
          </Descriptions.Item>
        )}
        <Descriptions.Item label="Email">
          {user && user.preferredEmail
            ? user.preferredEmail
            : contactabilities.length > 0
            ? contactabilities[0].email
            : ''}
        </Descriptions.Item>
        <Descriptions.Item label="Phone Number">
          {user && user.preferredPhone
            ? user.preferredPhone
            : contactabilities.length > 0
            ? contactabilities[0].phoneNumber
            : ''}
        </Descriptions.Item>
        <Descriptions.Item label="DOB/DOI">
          {user && moment(user.dateOfBirthIncorporation).format('DD-MM-YYYY')}
        </Descriptions.Item>
      </Descriptions>
      <Descriptions
        title={`Customer Identities`}
        column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        bordered
        layout="vertical"
        size="small"
      >
        <Descriptions.Item label="PAN">
          {user && user.identities && user.identities.pan}
        </Descriptions.Item>
        {user && user.type === 'INDIVIDUAL' ? (
          <>
            {user && user.identities && user.identities.aadhar ? (
              <Descriptions.Item label="AADHAR Number">
                {user.identities.aadhar}
              </Descriptions.Item>
            ) : null}
            {user && user.identities && user.identities.voterId ? (
              <Descriptions.Item label="Voter ID">
                {user.identities.voterId}
              </Descriptions.Item>
            ) : null}
            {user && user.identities && user.identities.ckycNumber ? (
              <Descriptions.Item label="CKYC Number">
                {user.identities.ckycNumber}
              </Descriptions.Item>
            ) : null}
            {user && user.identities && user.identities.drivingLicense ? (
              <Descriptions.Item label="Driving License">
                {user.identities.drivingLicense}
              </Descriptions.Item>
            ) : null}
            {user && user.identities && user.identities.passport ? (
              <Descriptions.Item label="Passport">
                {user.identities.passport}
              </Descriptions.Item>
            ) : null}
          </>
        ) : (
          <>
            <Descriptions.Item label="CIN">
              {user && user.identities && user.identities.cin}
            </Descriptions.Item>
            <Descriptions.Item label="TAN">
              {user && user.identities && user.identities.tan}
            </Descriptions.Item>
          </>
        )}
      </Descriptions>
      <Descriptions
        title={`All Addresses`}
        column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      />
      <DedupeAddressCollapse contactDetails={contactDetails} />
    </Spin>
  );
}

CustomerInfo.propTypes = {};

export default memo(CustomerInfo);
