/**
 *
 * Asynchronously loads the component for KycComponent
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
