/**
 *
 * KycComponent
 *
 */

import React, { memo, useEffect } from 'react';
import _ from 'lodash';
import {
  Menu,
  Empty,
  Divider,
  Descriptions,
  List,
  Space,
  Typography,
  Table,
  Button,
} from 'antd';
import {
  setSelectedOkycUser,
  setSelectedCkycUser,
  navigateToDocumentsTab,
} from '../../containers/KycDetails/actions';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

const { Text } = Typography;
function KycComponent({
  kycType,
  data,
  OkycUsers,
  CkycUsers,
  selectedOkycUser,
  selectedCkycUser,
  okycDocsData,
  ckycDocsData,
  dispatch,
  dkycDocs,
  dkycDetails,
}) {
  const columns = [
    {
      title: 'Doc Name',
      dataIndex: 'docName',
      key: 'docName',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
  ];

  const setOkycUser = id => {
    if (id) dispatch(setSelectedOkycUser(id.key));
  };

  const setCkycUser = id => {
    if (id) dispatch(setSelectedCkycUser(id.key));
  };

  const navigate = () => {
    dispatch(navigateToDocumentsTab(true));
  };

  const convertAddress = address => {
    let parsedAddress = address && JSON.parse(address);
    if (parsedAddress) {
      return `${parsedAddress.addressLine1} ${parsedAddress.addressLine2} ${
        parsedAddress.addressLine3
      } ${parsedAddress.city} ${parsedAddress.locality} ${
        parsedAddress.pincode
      } ${parsedAddress.state}`;
    } else {
      return '';
    }
  };

  function download(filename, text, mimeType) {
    var element = document.createElement('a');
    element.setAttribute(
      'href',
      'data:' + mimeType + ';base64,' + encodeURIComponent(text),
    );
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  const showOkycContent = () => {
    return (
      <>
        <Menu
          onClick={setOkycUser}
          defaultSelectedKeys={[selectedOkycUser || '']}
          selectedKeys={[String(selectedOkycUser)]}
          mode="horizontal"
          style={{ width: '95%' }}
        >
          {_.map(OkycUsers, (cuid, index) => (
            <Menu.Item key={String(cuid)}>{`Applicant ${index + 1}`}</Menu.Item>
          ))}
        </Menu>
        {data[selectedOkycUser].map((item, index) => (
          <>
            <Descriptions
              title={`KYC Info ${index + 1}`}
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
            >
              <Descriptions.Item label="name">
                {item.details && item.details.name}
              </Descriptions.Item>
              <Descriptions.Item label="dateOfBirth">
                {item.details && item.details.dateOfBirth}
              </Descriptions.Item>
              <Descriptions.Item label="careOf">
                {item.details && item.details.careOf}
              </Descriptions.Item>
              <Descriptions.Item label="gender">
                {item.details && item.details.gender}
              </Descriptions.Item>
              <Descriptions.Item label="Address Line 1">
                {item.details &&
                  item.details.address &&
                  item.details.address.addressLine1}
              </Descriptions.Item>
              <Descriptions.Item label="Address Line 2">
                {item.details &&
                  item.details.address &&
                  item.details.address.addressLine2}
              </Descriptions.Item>
              <Descriptions.Item label="District">
                {item.details &&
                  item.details.address &&
                  item.details.address.district}
              </Descriptions.Item>
              <Descriptions.Item label="Pincode">
                {item.details &&
                  item.details.address &&
                  item.details.address.pincode}
              </Descriptions.Item>
              <Descriptions.Item label="Street">
                {item.details &&
                  item.details.address &&
                  item.details.address.street}
              </Descriptions.Item>
              <Descriptions.Item label="State">
                {item.details &&
                  item.details.address &&
                  item.details.address.state}
              </Descriptions.Item>
              <Descriptions.Item label="Postoffice">
                {item.details &&
                  item.details.address &&
                  item.details.address.postOffice}
              </Descriptions.Item>
              <Descriptions.Item label="City">
                {item.details &&
                  item.details.address &&
                  item.details.address.city}
              </Descriptions.Item>
              <Descriptions.Item label="Country">
                {item.details &&
                  item.details.address &&
                  item.details.address.country}
              </Descriptions.Item>
            </Descriptions>
            <Divider />
          </>
        ))}
        {okycDocsData[selectedOkycUser] && (
          <>
            <Descriptions title={`Documents`} />
            <List
              size="small"
              dataSource={okycDocsData[selectedOkycUser]}
              style={{ width: '50%' }}
              renderItem={(item, index) => (
                <List.Item
                  actions={[
                    <a
                      key={index}
                      onClick={() => {
                        download(
                          item.data.fileName,
                          item.data.fileStream,
                          item.data.mimeType,
                        );
                      }}
                    >
                      Download
                    </a>,
                  ]}
                >
                  <List.Item.Meta
                    title={<Text strong>{item.data.fileName}</Text>}
                  />
                </List.Item>
              )}
            />
          </>
        )}
      </>
    );
  };

  const showCkycContent = () => {
    return (
      <>
        <Menu
          onClick={setCkycUser}
          defaultSelectedKeys={[selectedCkycUser || '']}
          selectedKeys={[String(selectedCkycUser)]}
          mode="horizontal"
          style={{ width: '95%' }}
        >
          {_.map(CkycUsers, (cuid, index) => (
            <Menu.Item key={String(cuid)}>{`Applicant ${index + 1}`}</Menu.Item>
          ))}
        </Menu>
        {data[selectedCkycUser] &&
          data[selectedCkycUser].map(
            (item, index) =>
              item.kycDetails &&
              item.kycDetails.details.map((item, index) => (
                <>
                  <Descriptions
                    title={`KYC Info ${index + 1}`}
                    column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  >
                    <Descriptions.Item label="ckycNumber">
                      {item.ckycNumber}
                    </Descriptions.Item>
                    <Descriptions.Item label="customerId">
                      {item.customerId}
                    </Descriptions.Item>
                    <Descriptions.Item label="constitutionType">
                      {item.constitutionType}
                    </Descriptions.Item>
                    <Descriptions.Item label="accountType">
                      {item.accountType}
                    </Descriptions.Item>
                    <Descriptions.Item label="fullName">
                      {item.fullName}
                    </Descriptions.Item>
                    <Descriptions.Item label="fatherFullName">
                      {item.fatherFullName}
                    </Descriptions.Item>
                    <Descriptions.Item label="motherFullName">
                      {item.motherFullName}
                    </Descriptions.Item>
                    <Descriptions.Item label="gender">
                      {item.gender}
                    </Descriptions.Item>
                    <Descriptions.Item label="maritalStatus">
                      {item.maritalStatus}
                    </Descriptions.Item>
                    <Descriptions.Item label="nationality">
                      {item.nationality}
                    </Descriptions.Item>
                    <Descriptions.Item label="occupation">
                      {item.occupation}
                    </Descriptions.Item>
                    <Descriptions.Item label="dateOfBirth">
                      {item.dateOfBirth}
                    </Descriptions.Item>
                    <Descriptions.Item label="residentialStatus">
                      {item.residentialStatus}
                    </Descriptions.Item>
                    <Descriptions.Item label="permanentAddressType">
                      {item.permanentAddressType}
                    </Descriptions.Item>
                    <Descriptions.Item label="phoneNumber">
                      {item.phoneNumber}
                    </Descriptions.Item>
                    <Descriptions.Item label="emailId">
                      {item.emailId}
                    </Descriptions.Item>
                    <Descriptions.Item label="organisationName" span={2}>
                      {item.organisationName}
                    </Descriptions.Item>
                    <Descriptions.Item label="permanentAddress" span={3}>
                      {convertAddress(item.permanentAddress)}
                    </Descriptions.Item>
                    <Descriptions.Item label="correspondenceAddress" span={3}>
                      {convertAddress(item.correspondenceAddress)}
                    </Descriptions.Item>
                  </Descriptions>
                  <Divider />
                </>
              )),
          )}
        {ckycDocsData[selectedCkycUser] && (
          <>
            <Descriptions title={`Documents`} />
            <List
              size="small"
              dataSource={ckycDocsData[selectedCkycUser]}
              style={{ width: '50%' }}
              renderItem={(item, index) => (
                <List.Item
                  actions={[
                    <a
                      key={index}
                      onClick={() => {
                        download(
                          item.data.fileName,
                          item.data.fileStream,
                          item.data.mimeType,
                        );
                      }}
                    >
                      Download
                    </a>,
                  ]}
                >
                  <List.Item.Meta
                    title={<Text strong>{item.data.fileName}</Text>}
                  />
                </List.Item>
              )}
            />
          </>
        )}
      </>
    );
  };

  const showDkycContent = () => {
    return (
      <>
        <Descriptions
          title={`DKYC Info`}
          column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        >
          <Descriptions.Item label="leadStatus">
            {dkycDetails.leadStatus}
          </Descriptions.Item>
          <Descriptions.Item label="customerName">
            {dkycDetails.customerName}
          </Descriptions.Item>
          <Descriptions.Item label="customerMobile">
            {dkycDetails.customerMobile}
          </Descriptions.Item>
          <Descriptions.Item label="customerEmail">
            {dkycDetails.customerEmail}
          </Descriptions.Item>
          <Descriptions.Item label="addType">
            {dkycDetails.addType}
          </Descriptions.Item>
          <Descriptions.Item label="addLine1">
            {dkycDetails.addLine1}
          </Descriptions.Item>
          <Descriptions.Item label="addLine2">
            {dkycDetails.addLine2}
          </Descriptions.Item>
          <Descriptions.Item label="pinCode">
            {dkycDetails.pinCode}
          </Descriptions.Item>
          <Descriptions.Item label="city">{dkycDetails.city}</Descriptions.Item>
          <Descriptions.Item label="state">
            {dkycDetails.state}
          </Descriptions.Item>
          <Descriptions.Item label="locality">
            {dkycDetails.locality}
          </Descriptions.Item>
          <Descriptions.Item label="scheduleDate">
            {dkycDetails.scheduleDate}
          </Descriptions.Item>
          <Descriptions.Item label="scheduleTime">
            {dkycDetails.scheduleTime}
          </Descriptions.Item>
          <Descriptions.Item label="product">
            {dkycDetails.product}
          </Descriptions.Item>
          <Descriptions.Item label="partner">
            {dkycDetails.partner}
          </Descriptions.Item>
          <Descriptions.Item label="leadType">
            {dkycDetails.leadType}
          </Descriptions.Item>
          <Descriptions.Item label="agentName">
            {dkycDetails.agentName}
          </Descriptions.Item>
        </Descriptions>
        <Divider />
        <Descriptions
          title={`DKYC Documents`}
          column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        />
        {dkycDocs.length > 0 ? (
          <Space>
            <Text>Click</Text>
            <Button type="primary" onClick={navigate} style={{marginBottom: '10px'}}>
              here
            </Button>
            <Text>to perform action on documents</Text>
          </Space>
        ) : null}
        <Table columns={columns} dataSource={dkycDocs || []} bordered />
      </>
    );
  };
  return (
    <>
      {kycType === 'OKYC' && OkycUsers.length > 0 ? (
        showOkycContent()
      ) : kycType === 'CKYC' && CkycUsers.length > 0 ? (
        showCkycContent()
      ) : kycType === 'DKYC' ? (
        showDkycContent()
      ) : (
        <Empty />
      )}
    </>
  );
}

KycComponent.propTypes = {};

export default memo(KycComponent);
