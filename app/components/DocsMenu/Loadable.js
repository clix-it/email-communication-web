/**
 *
 * Asynchronously loads the component for DocsMenu
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
