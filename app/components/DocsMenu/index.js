/**
 *
 * DocsMenu
 *
 */

import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import {
  Menu,
  Space,
  Button,
  Divider,
  Typography,
  List,
  Popconfirm,
  Upload,
  Avatar,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import styled from 'styled-components';
import docSvg from '../../images/document.svg';
import moment from 'moment';
import {
  fetchleadsDoc,
  deleteFile,
  uploadleadsDocRequest,
  setAllDocs,
} from '../../containers/LeadDocuments/actions';
import { setFormValidationError } from '../../containers/ApplicantDetails/actions';

const ListWrapper = styled.div`
  height: 300px;
  overflow: auto;
`;

function DocsMenu({
  appDocumentDetails,
  appV1Docs,
  dispatch,
  requestType,
  activityType,
  hideUploadDiv,
  setVisible,
  setCurrentItem,
  applicantDetails,
  generateAcceptanceLetter,
  docsData,
}) {
  const { Title, Text } = Typography;
  const [selectedDocument, setSelectedDocument] = useState({});
  const [validateTabs, setValidateTabs] = useState({});
  const [downloadedDocs, setDownloadedDocs] = useState({});

  useEffect(() => {
    if (appDocumentDetails && appDocumentDetails.length > 0) {
      setSelectedDocument(appDocumentDetails[0]);
      dispatch(fetchleadsDoc([appDocumentDetails[0]]));
      setValidateTabs({ ...validateTabs, [0]: true });
      dispatch(setFormValidationError('documents', false));
    }
  }, []);

  useEffect(() => {
    if (
      selectedDocument &&
      Object.keys(selectedDocument).length > 0 &&
      validateTabs[selectedDocument.key]
    ) {
      const filteredDocsData = docsData.filter(
        item =>
          _.get(item, 'fileAttrs.isDocumentActive') === true ||
          _.get(item, 'fileAttrs.isDocumentActive') === undefined,
      );
      setDownloadedDocs({
        ...downloadedDocs,
        [selectedDocument.key]: filteredDocsData,
      });
    }
  }, [docsData]);

  useEffect(() => {
    if (Object.keys(downloadedDocs).length > 0) {
      const arr = [];
      Object.keys(downloadedDocs).forEach(item => {
        if (downloadedDocs[item] && downloadedDocs[item].length > 0) {
          downloadedDocs[item].forEach(item => {
            arr.push(item);
          });
        }
      });
      if (arr.length > 0) {
        dispatch(setAllDocs(arr));
      }
    }
  }, [downloadedDocs]);

  useEffect(() => {
    if (Object.keys(validateTabs).length == appDocumentDetails.length) {
      dispatch(setFormValidationError('documents', true));
    }
  }, [validateTabs]);

  const setDocument = id => {
    const applicant =
      _.find(appDocumentDetails, { key: id.key }) ||
      _.find(appDocumentDetails, { key: parseInt(id.key) });
    if (applicant) {
      setSelectedDocument(applicant);
    }
    if (applicant && !validateTabs[id.key]) {
      dispatch(fetchleadsDoc([applicant]));
      setValidateTabs({ ...validateTabs, [id.key]: true });
    }
  };

  const refreshDocs = () => {
    dispatch(fetchleadsDoc([selectedDocument]));
  };

  const showDMSDrawer = (e, item) => {
    const dmsItem = {
      fileextension: item.extension,
      signedUrl: `data:${item.mimeType};base64,${item.fileStream}`,
    };
    setCurrentItem(dmsItem);
    setVisible(true);
  };

  function download(filename, text, mimeType) {
    const element = document.createElement('a');
    element.setAttribute(
      'href',
      `data:${mimeType};base64,${encodeURIComponent(text)}`,
    );
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  const showDrawer = (e, item) => {
    setCurrentItem(item);
    setVisible(true);
  };

  const deleteDocument = item => {
    dispatch(deleteFile(item));
    setTimeout(() => {
      dispatch(fetchleadsDoc([selectedDocument]));
    }, 1000);
  };

  const handleFileUpload = (file, item = fileToUpload) => {
    const reader = new FileReader();
    if (item.type !== undefined) {
      reader.onload = function() {
        dispatch(
          uploadleadsDocRequest(
            { ...file.file, name: `${item.obj_id}-${file.file.name}` },
            item.obj_type,
            `${item.obj_id}`,
            item.type,
            reader.result,
            file.file,
            '',
          ),
        );
        setTimeout(() => {
          dispatch(fetchleadsDoc([selectedDocument]));
        }, 1000);
      };
    }
    reader.readAsDataURL(file.file);
  };

  const setHeading = item => {
    if (item.fileAttrs && item.fileAttrs.docName) {
      return (
        <Space>
          <Text>{item.type}</Text>
          <Text>{`(${item.fileAttrs.docName})`}</Text>
        </Space>
      );
    }
    return <Text>{item.type}</Text>;
  };

  const setDescription = item => {
    if (item) {
      const arr = appDocumentDetails.filter(
        detail => detail.cuid === item.obj_id.toString(),
      );
      if (arr.length > 0) {
        return (
          <Space>
            {_.get(item, 'fileAttrs.filePassword') &&
              _.get(item, 'fileAttrs.filePassword') !== 'yyyyMMdd' && (
                <>
                  <Text>Password:</Text>
                  <Text>{item.fileAttrs.filePassword}</Text>
                </>
            )}
            {item.date && (
              <>
                <Text>Uploaded :</Text>
                <Text>{moment(item.date).format('DD-MM-YYYY')}</Text>
              </>
            )}
          </Space>
        );
      }
      return (
        <Space>
          {item.date && (
            <>
              <Text>Uploaded :</Text>
              <Text>{moment(item.date).format('DD-MM-YYYY')}</Text>
            </>
          )}
        </Space>
      );
    }
    return <Text>App</Text>;
  };

  return (
    <>
      <Space>
        <Title level={3}>Documents</Title>
        <Button
          type="primary"
          style={{ marginBottom: '0.5em' }}
          onClick={() => refreshDocs(selectedDocument)}
        >
          Refresh
        </Button>
        {activityType === 'CREDIT_PSV' &&
          requestType === 'PMA' &&
          _.get(applicantDetails, 'entity.partner') === 'RLA' && (
            <Button
              type="primary"
              style={{ marginBottom: '0.5em' }}
              onClick={generateAcceptanceLetter}
            >
              Generate Acceptance Letter
            </Button>
          )}
      </Space>

      <Divider />
      <Menu
        onClick={setDocument}
        defaultSelectedKeys={[(selectedDocument || {}).key]}
        selectedKeys={[String((selectedDocument || {}).key)]}
        mode="horizontal"
        style={{ width: '95%' }}
      >
        {_.map(appDocumentDetails, applicant => (
          <Menu.Item key={String(applicant.key)}>
            {applicant.appId ? 'App' : applicant.name}
          </Menu.Item>
        ))}
      </Menu>
      <ListWrapper>
        {appV1Docs && appV1Docs.length > 0 && selectedDocument.key == 0 ? (
          <List
            className="demo-loadmore-list"
            itemLayout="horizontal"
            size="small"
            dataSource={appV1Docs}
            renderItem={(item, index) =>
              item.data && (
                <List.Item
                  actions={[
                    <Button
                      type="link"
                      key={index}
                      onClick={event => showDMSDrawer(event, item.data)}
                    >
                      View
                    </Button>,

                    <a
                      key={index}
                      onClick={() => {
                        download(
                          item.data.fileName,
                          item.data.fileStream,
                          item.data.mimeType,
                        );
                      }}
                    >
                      Download
                    </a>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar shape="square" src={docSvg} />}
                    title={<Text>{item.data.fileName}</Text>}
                    description={
                      <Space>
                        <Text>Name:</Text>
                        <Text>{item.data.fileName}</Text>
                        {item.data.password && (
                          <>
                            <Text> Password:</Text>
                            <Text> {item.data.password}</Text>
                          </>
                        )}
                      </Space>
                    }
                  />
                </List.Item>
              )
            }
          />
        ) : null}
        <List
          className="demo-loadmore-list"
          itemLayout="horizontal"
          size="small"
          dataSource={
            downloadedDocs[selectedDocument.key]
              ? downloadedDocs[selectedDocument.key]
              : docsData
          }
          renderItem={(item, index) => (
            <>
              <List.Item
                actions={[
                  <Button
                    type="link"
                    key={index}
                    onClick={event => showDrawer(event, item)}
                  >
                    View
                  </Button>,

                  <a key={index} href={item.signedUrl} target="_blank">
                    Download
                  </a>,
                  <Popconfirm
                    placement="topLeft"
                    title="Are you sure you want to delete this Document?"
                    onConfirm={() => deleteDocument(item)}
                    disabled={requestType !== 'PMA'}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Button
                      type="link"
                      key={index}
                      disabled={requestType !== 'PMA'}
                    >
                      Delete
                    </Button>
                  </Popconfirm>,
                  <Upload
                    // accept=".jpg, .pdf, .png"
                    showUploadList={false}
                    customRequest={e => handleFileUpload(e, item)}
                  >
                    {!hideUploadDiv ? (
                      <Button>
                        <UploadOutlined /> Re-upload
                      </Button>
                    ) : null}
                  </Upload>,
                ]}
              >
                <List.Item.Meta
                  avatar={<Avatar shape="square" src={docSvg} />}
                  title={setHeading(item)}
                  description={setDescription(item)}
                />
              </List.Item>
            </>
          )}
        />
      </ListWrapper>
    </>
  );
}

DocsMenu.propTypes = {};

export default memo(DocsMenu);
