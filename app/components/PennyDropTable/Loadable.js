/**
 *
 * Asynchronously loads the component for PennyDropTable
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
