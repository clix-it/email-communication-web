/**
 *
 * PennyDropTable
 *
 */

import React, { memo } from 'react';
import { Table, Tag, Space, Typography } from 'antd';

const { Text } = Typography;

const columns = [
  {
    title: 'Success',
    dataIndex: 'success',
    key: 'success',
    render: val => val ? <Text>{val.toString()}</Text> : <Text type="danger">{val.toString()}</Text>,
  },
  {
    title: 'Bank Account',
    dataIndex: 'bankAccount',
    key: 'bankAccount',
    render: val => <Text>{val.toString()}</Text>,
  },
  {
    title: 'Bank Verified Name',
    dataIndex: 'bankVerifiedName',
    key: 'bankVerifiedName',
    render: val => val != 'unavailable' ? <Text>{val.toString()}</Text> : <Text type="danger">{val.toString()}</Text>,
  },
  {
    title: 'Account Validation Status',
    dataIndex: 'accountValidationStatus',
    key: 'accountValidationStatus',
    render: val => val != 'invalid' ? <Text>{val.toString()}</Text> : <Text type="danger">{val.toString()}</Text>,
  },
  {
    title: 'Penny Request Status',
    dataIndex: 'pennyRequestStatus',
    key: 'pennyRequestStatus',
    render: val => <Text>{val.toString()}</Text>,
  },
];

function PennyDropTable({ dataSource }) {
  const dataArr = dataSource.map((item, index) => ({
    key: index,
    success: item.success,
    bankAccount: item.bankAccount,
    bankVerifiedName: item.bankVerifiedName,
    accountValidationStatus: item.accountValidationStatus,
    pennyRequestStatus: item.pennyRequestStatus,
  }));
  return <Table columns={columns} dataSource={dataArr} />;
}

PennyDropTable.propTypes = {};

export default memo(PennyDropTable);
