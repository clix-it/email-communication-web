/**
 *
 * Asynchronously loads the component for ShowBankDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
