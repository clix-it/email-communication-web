/**
 *
 * ShowBankDetails
 *
 */

import React, { memo } from 'react';
import { Descriptions, Empty } from 'antd';
import PennyDropTable from '../PennyDropTable';
import _ from 'lodash';

function ShowBankDetails({ appDetails, pennyDropDetails }) {
  return (
    <>
      {_.get(appDetails, 'bankDetail.collectionAccNumber') && (
        <Descriptions
          title={`Collection Bank Details`}
          column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          bordered
          layout="vertical"
          size="small"
        >
          <Descriptions.Item label="Bank Name">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.collectionBankName}
          </Descriptions.Item>
          <Descriptions.Item label="Account Type">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.collectionAccType}
          </Descriptions.Item>
          <Descriptions.Item label="Account Number">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.collectionAccNumber}
          </Descriptions.Item>
          <Descriptions.Item label="IFSC code">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.collectionBankIfsc}
          </Descriptions.Item>
        </Descriptions>
      )}
      {_.get(appDetails, 'bankDetail.disbursmentAccNumber') && (
        <Descriptions
          title={`Disbursement Bank Details`}
          column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          bordered
          layout="vertical"
          size="small"
        >
          <Descriptions.Item label="Bank Name">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.disbursmentBankName}
          </Descriptions.Item>
          <Descriptions.Item label="Account Type">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.disbursmentAccType}
          </Descriptions.Item>
          <Descriptions.Item label="Account Number">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.disbursmentAccNumber}
          </Descriptions.Item>
          <Descriptions.Item label="IFSC code">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.disbursmentBankIfsc}
          </Descriptions.Item>
          <Descriptions.Item label="Account Holder Name">
            {appDetails &&
              appDetails.bankDetail &&
              appDetails.bankDetail.disbursmentAccHolderName}
          </Descriptions.Item>
        </Descriptions>
      )}
      {appDetails.mandate && (
        <Descriptions
          title={`Mandate Details`}
          column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          bordered
          layout="vertical"
          size="small"
        >
          <Descriptions.Item label="Status">
            {appDetails && appDetails.mandate && appDetails.mandate.status}
          </Descriptions.Item>
          <Descriptions.Item label="E-mandate Id">
            {appDetails && appDetails.mandate && appDetails.mandate.emandateId}
          </Descriptions.Item>
          <Descriptions.Item label="UMRN">
            {appDetails && appDetails.mandate && appDetails.mandate.umrn}
          </Descriptions.Item>
        </Descriptions>
      )}
      {pennyDropDetails && pennyDropDetails.length > 0 && (
        <>
          <Descriptions
            title={`Penny Drop Details`}
            column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          />
          <PennyDropTable dataSource={pennyDropDetails} />
        </>
      )}
      {_.get(appDetails, 'bankDetail.collectionAccNumber') ||
      _.get(appDetails, 'bankDetail.disbursmentAccNumber') ? (
        ''
      ) : (
        <Empty />
      )}
    </>
  );
}

ShowBankDetails.propTypes = {};

export default memo(ShowBankDetails);
