export const mobilePattern = {
    value: /^([+]\d{2})?\d{10}$/,
    message: 'Invalid Mobile Number',
  };
  export const emailPattern = {
    // eslint-disable-next-line no-useless-escape
    value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
    message: 'Invalid Email',
  };
  export const numberPattern = {
    value: /^(0|[1-9][0-9]*)$/,
    message: 'Invalid Number',
  };
  
  export const addressesBluPrint = [
    {
      id: 'addressLine1',
      type: 'text',
      placeholder: 'Address Line 1',
    },
    {
      id: 'addressLine2',
      type: 'text',
      placeholder: 'Address Line 2',
    },
    {
      id: 'addressLine3',
      type: 'text',
      placeholder: 'Address Line 3',
    },
  ];
  
  export const columnResponsiveLayout = {
    colSingle: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
    colHalf: { xs: 24, sm: 24, md: 24, lg: 12, xl: 10 },
    colOneThird: { xs: 24, sm: 24, md: 24, lg: 5, xl: 5 },
  };
  