import _ from 'lodash';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { Col, Divider, Typography } from 'antd';

import DedupeEntityForm from 'components/DedupeEntityForm';
import { columnResponsiveLayout } from './constants';
import DedupeContactDetails from 'components/DedupeContactDetails';
import DedupeAddressCollapse from 'components/DedupeAddressCollapse';

const { Title } = Typography;

function DedupeEntityDetails({ detailsData, dispatch, appId, isNoEntity }) {
  let contactDetails = {};
  const defaultValues = {
    ...detailsData.userDetails,
  };

  if (defaultValues && defaultValues.cuid) {
    let arr =
      defaultValues.contactibilities &&
      defaultValues.contactibilities.filter(item => item.addressLine1);
    contactDetails = _.mapValues(
      _.groupBy(_.orderBy(arr, ['id'], ['desc']), 'contactType'),
      clist => clist.map(contact => _.omit(contact, 'contactType')),
    );
  }

  return (
    <>
      {/* {error.status && <Redirect to="/" />} */}
      <div className="App">
        <form>
          <Col {...columnResponsiveLayout.colSingle}>
            <Title level={3}>
              {isNoEntity ? `Applicant Details` : `Entity Details`}
            </Title>
          </Col>

          {!isNoEntity ? (
            <>
              <DedupeEntityForm defaultValues={defaultValues} />
              <Divider />
              <Title level={3}>Contact Details</Title>
            </>
          ) : null}
          <DedupeContactDetails
            defaultValues={defaultValues}
            isNoEntity={isNoEntity}
          />
          <DedupeAddressCollapse contactDetails={contactDetails} />
        </form>
      </div>
    </>
  );
}

DedupeEntityDetails.propTypes = {
  detailsData: PropTypes.array,
  dispatch: PropTypes.func.isRequired,
};

export default DedupeEntityDetails;
