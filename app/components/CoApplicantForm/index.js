/**
 *
 * CoApplicantForm
 *
 */

import _ from 'lodash';
import React, { memo } from 'react';
import { Row, Col, Typography } from 'antd';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

const { Text } = Typography;

function CoApplicantForm({ defaultValues, relationship }) {
  return (
    <Row gutter={[16, 16]}>
      <Col {...responsiveColumns}>
        <Text strong>First Name : </Text>
        <Text>{defaultValues.firstName}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Middle Name : </Text>
        <Text>{defaultValues.middleName}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Last Name : </Text>
        <Text>{defaultValues.lastName}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Gender : </Text>
        <Text>
          {defaultValues.userDetails ? defaultValues.userDetails.gender : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Role/Relation with Borrower Entity : </Text>
        <Text>{relationship ? relationship.designation : ''}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Co-Applicant Mobile Number : </Text>
        <Text>
          {defaultValues.preferredPhone
            ? defaultValues.preferredPhone
            : defaultValues.contactibilities
            ? defaultValues.contactibilities[0].phoneNumber
            : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Co-Applicant Email : </Text>
        <Text>
          {defaultValues.preferredEmail
            ? defaultValues.preferredEmail
            : defaultValues.contactibilities
            ? defaultValues.contactibilities[0].email
            : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Date Of Birth : </Text>
        <Text>{defaultValues.dateOfBirthIncorporation}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>PAN : </Text>
        <Text>
          {defaultValues.identities ? defaultValues.identities.pan || '' : ''}
        </Text>
      </Col>
    </Row>
  );
}

CoApplicantForm.propTypes = {};

export default memo(CoApplicantForm);
