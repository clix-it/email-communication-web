import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import _ from 'lodash';
import InputField from 'components/InputField';
import {
  DESIGNATION,
  EXPERIENCE_YEARS,
  INDUSTRY_TYPE,
  COMPANY_TYPE,
  employmentTypeArray,
} from '../../utils/constants';

function OccupationalDetails({
  responsiveColumns,
  values,
  userIndex,
  control,
  errors,
  setError,
  clearError,
  allFieldsDisabled,
  userEmployments,
  isGuarantor,
}) {
  const [employments, setEmployments] = useState([]);
  useEffect(() => {
    if (userEmployments && userEmployments.length > 0) {
      setEmployments(_.get(_.orderBy(userEmployments, 'id', 'desc'), '[0]'));
    } else setEmployments([]);
  }, [userEmployments]);

  return (
    <Row gutter={[16, 16]}>
      {/*<Col xs={0}>
        <InputField
          id={`applicants[${userIndex}].userEmployments[0].id`}
          control={control}
          name={`applicants[${userIndex}].userEmployments[0].id`}
          type="string"
          defaultValue={employments.id || ''}
          disabled
        />
  </Col>*/}
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].employmentType`
              : `guarantors[${userIndex}].userEmployments[0].employmentType`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].employmentType`
              : `guarantors[${userIndex}].userEmployments[0].employmentType`
          }
          type="select"
          options={employmentTypeArray}
          defaultValue={employments.employmentType || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Occupation Type"
          labelHtml="Occupation Type"
          //disabled
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].companyType`
              : `guarantors[${userIndex}].userEmployments[0].companyType`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].companyType`
              : `guarantors[${userIndex}].userEmployments[0].companyType`
          }
          type="select"
          options={COMPANY_TYPE}
          defaultValue={employments.companyType || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Company Type"
          labelHtml="Company Type"
          //disabled
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].companyIndutry`
              : `guarantors[${userIndex}].userEmployments[0].companyIndutry`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].companyIndutry`
              : `guarantors[${userIndex}].userEmployments[0].companyIndutry`
          }
          type="select"
          options={INDUSTRY_TYPE}
          defaultValue={employments.companyIndutry || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Industry Type"
          labelHtml="Industry Type"
          //disabled
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].companyName`
              : `guarantors[${userIndex}].userEmployments[0].companyName`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].companyName`
              : `guarantors[${userIndex}].userEmployments[0].companyName`
          }
          type="string"
          defaultValue={employments.companyName || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Employer Name"
          labelHtml="Employer Name"
          //disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].profession`
              : `guarantors[${userIndex}].userEmployments[0].profession`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].profession`
              : `guarantors[${userIndex}].userEmployments[0].profession`
          }
          type="select"
          options={DESIGNATION}
          defaultValue={employments.profession || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Designation"
          labelHtml="Designation"
          //disabled
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].officialEmail`
              : `guarantors[${userIndex}].userEmployments[0].officialEmail`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].officialEmail`
              : `guarantors[${userIndex}].userEmployments[0].officialEmail`
          }
          type="string"
          defaultValue={employments.officialEmail || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Email ID (Official)"
          labelHtml="Email ID (Official)"
          //disabled={allFieldsDisabled}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].currentExpInMonths`
              : `guarantors[${userIndex}].userEmployments[0].currentExpInMonths`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].currentExpInMonths`
              : `guarantors[${userIndex}].userEmployments[0].currentExpInMonths`
          }
          type="number"
          //options={EXPERIENCE_YEARS}
          defaultValue={employments.currentExpInMonths || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Current Work Experience (Months)"
          labelHtml="Current Work Experience (Months)"
          //disabled
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].totalExperienceInMonths`
              : `guarantors[${userIndex}].userEmployments[0].postQualificationExpInMonths`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].totalExperienceInMonths`
              : `guarantors[${userIndex}].userEmployments[0].postQualificationExpInMonths`
          }
          type="number"
          //options={EXPERIENCE_YEARS}
          defaultValue={
            employments.totalExperienceInMonths ||
            employments.postQualificationExpInMonths
          }
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder={
            isGuarantor
              ? 'Post Qualification Experience(in months)'
              : 'Total Work Experience (Months)'
          }
          labelHtml={
            isGuarantor
              ? 'Post Qualification Experience(in months)'
              : 'Total Work Experience (Months)'
          }
          //disabled
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          id={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].totalIncomeInMonths`
              : `guarantors[${userIndex}].userEmployments[0].totalIncomeInMonths`
          }
          control={control}
          name={
            !isGuarantor
              ? `applicants[${userIndex}].userEmployments[0].totalIncomeInMonths`
              : `guarantors[${userIndex}].userEmployments[0].totalIncomeInMonths`
          }
          type="string"
          defaultValue={employments.totalIncomeInMonths || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Declared Net Monthly Income"
          labelHtml="Declared Net Monthly Income"
          //disabled={allFieldsDisabled}
        />
      </Col>
    </Row>
  );
}

export default OccupationalDetails;
