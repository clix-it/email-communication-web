/**
 *
 * DedupeCoApplicantDetails
 *
 */

import _ from 'lodash';
import React, { useEffect } from 'react';
import { Row, Col, Empty, Menu } from 'antd';

import {
  setSelectedCoApplicant,
  fetchCoApplicantDetails,
} from '../../containers/LeadDetails/actions';
import { columnResponsiveLayout } from '../EntityDetails/constants';
import CoApplicantForm from 'components/CoApplicantForm';
import DedupeAddressCollapse from 'components/DedupeAddressCollapse';
import '../PersonalDetails/personalStyle.css';

function DedupeCoApplicantDetails({ detailsData, dispatch }) {
  const { coApplicantData, selectedApplicant, userDetails } = detailsData;
  let contactDetails = {};

  useEffect(() => {
    if (detailsData.userDetails) {
      dispatch(fetchCoApplicantDetails(detailsData.userDetails));
    }
  }, [detailsData.userDetails]);

  const setApplicant = id => {
    const applicant =
      _.find(coApplicantData, { cuid: id.key }) ||
      _.find(coApplicantData, { cuid: parseInt(id.key) });
    if (applicant) dispatch(setSelectedCoApplicant(applicant));
  };

  const defaultValues = {
    ...selectedApplicant,
  };

  if (defaultValues && defaultValues.cuid) {
    let arr =
      defaultValues.contactibilities &&
      defaultValues.contactibilities.filter(item => item.addressLine1);
    contactDetails = _.mapValues(
      _.groupBy(_.orderBy(arr, ['id'], ['desc']), 'contactType'),
      clist => clist.map(contact => _.omit(contact, 'contactType')),
    );
  }

  const relationship = selectedApplicant
    ? _.orderBy(
        _.filter(_.get(userDetails, 'entityOfficers') || [], {
          belongsTo: selectedApplicant.cuid,
        }),
        'id',
        'desc',
      )[0]
    : {};

  return (
    <div className="App">
      <Menu
        onClick={setApplicant}
        defaultSelectedKeys={[(selectedApplicant || {}).cuid]}
        selectedKeys={[String((selectedApplicant || {}).cuid)]}
        mode="horizontal"
        style={{ width: '95%' }}
      >
        {_.map(coApplicantData, (applicant, index) => (
          <Menu.Item key={String(applicant.cuid)}>{`Co Applicant ${index +
            1}`}</Menu.Item>
        ))}
      </Menu>
      {selectedApplicant ? (
        <>
          <Row>
            <Col
              {...columnResponsiveLayout.colSingle}
              style={{
                fontSize: '18px',
                fontWeight: 600,
                padding: '12px 8px',
              }}
            />
          </Row>
          <CoApplicantForm
            relationship={relationship}
            defaultValues={defaultValues}
          />
          <DedupeAddressCollapse contactDetails={contactDetails} />
        </>
      ) : (
        <Empty />
      )}
    </div>
  );
}

DedupeCoApplicantDetails.propTypes = {};

export default DedupeCoApplicantDetails;
