/**
 *
 * Asynchronously loads the component for DedupeLoanDetail
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
