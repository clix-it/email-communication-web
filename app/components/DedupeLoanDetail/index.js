/**
 *
 * DedupeLoanDetail
 *
 */

import _ from 'lodash';
import React, { useEffect, memo } from 'react';
import { useForm } from 'react-hook-form';
import { Row, Col, Empty, Menu } from 'antd';

import {
  setSelectedLoan,
  fetchLoanDetails
} from '../../containers/LeadDetails/actions';
import DedupeLoanForm from '../DedupeLoanForm'
import { columnResponsiveLayout } from '../EntityDetails/constants';
import '../PersonalDetails/personalStyle.css';

function DedupeLoanDetail({ detailsData, dispatch }) {
  const { dataDetails, selectedLoan } = detailsData;

  useEffect(() => {
    if(detailsData.userDetails){
      dispatch(fetchLoanDetails(detailsData.userDetails));
    }
  }, [detailsData.userDetails])

  const setLoan = id => {
    const loan =
      _.find(dataDetails, { loanAccountNumber: id.key }) ||
      _.find(dataDetails, { loanAccountNumber: parseInt(id.key) });
    if (loan) dispatch(setSelectedLoan(loan));
  };

  const defaultValues = {
    ...selectedLoan
  };

  return (
      <div className="App">
        <Menu
          onClick={setLoan}
          defaultSelectedKeys={[(selectedLoan || {}).loanAccountNumber]}
          selectedKeys={[String((selectedLoan || {}).loanAccountNumber)]}
          mode="horizontal"
          style={{width: '95%'}}
        >
          {_.map(dataDetails, (loan, index) => (
            <Menu.Item key={String(loan.loanAccountNumber)}>{`Loan ${index +
              1}`}</Menu.Item>
          ))}
        </Menu>
        {selectedLoan ? (
          <>
            <Row>
              <Col
                {...columnResponsiveLayout.colSingle}
                style={{
                  fontSize: '18px',
                  fontWeight: 600,
                  padding: '12px 8px',
                }}
              >
              </Col>
            </Row>
            <DedupeLoanForm
              defaultValues={defaultValues}
            />
          </>
        ) : (
          <Empty />
        )}
      </div>
  );
}

DedupeLoanDetail.propTypes = {};

export default memo(DedupeLoanDetail);
