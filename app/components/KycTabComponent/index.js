/**
 *
 * KycTabComponent
 *
 */

import React, { memo, useEffect, useState } from 'react';
import Tabs from '../Tabs';
import KycDetails from '../../containers/KycDetails';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function KycTabComponent({ applicantDetails, requestType, appId, creditData, isDkycAvailable }) {
  const [kycTabPanel, setKycTabPanel] = useState([
    {
      id: 'ckyc',
      name: 'CKYC Details',
      component: (
        <KycDetails
          applicantDetails={applicantDetails}
          requestType={requestType}
          appId={appId}
          kycType="CKYC"
          creditData={creditData}
        />
      ),
    },
    {
      id: 'okyc',
      name: 'OKYC Details',
      component: (
        <KycDetails
          applicantDetails={applicantDetails}
          requestType={requestType}
          appId={appId}
          kycType="OKYC"
          creditData={creditData}
        />
      ),
    },
  ]);

  useEffect(() => {
    if(isDkycAvailable){
      setKycTabPanel([...kycTabPanel, {
        id: 'dkyc',
        name: 'DKYC Details',
        component: (
          <KycDetails
            applicantDetails={applicantDetails}
            requestType={requestType}
            appId={appId}
            kycType="DKYC"
            creditData={creditData}
          />
        ),
      }])
    }
  }, [isDkycAvailable])
  return (
    <Tabs
      mode="top"
      ifSendBack={false}
      tabPanel={kycTabPanel}
    />
  );
}

KycTabComponent.propTypes = {};

export default memo(KycTabComponent);
