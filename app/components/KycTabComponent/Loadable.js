/**
 *
 * Asynchronously loads the component for KycTabComponent
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
