import React from 'react';
import { Form, Input, Col, Select, Option } from 'antd';

function AddressField({
  columnResponsiveLayout,
  addressLine1,
  addressLine2,
  addressLine3,
  pincode,
  city,
  country,
}) {
  return (
    <>
      <Col {...columnResponsiveLayout.colHalf}>
        <Form.Item
          label="Office Address*"
          name="addressLine1"
          rules={[
            {
              required: true,
              message: 'Please input the Office Address!',
            },
          ]}
        >
          <Input
            placeholder="Address Line 1"
            value={addressLine1}
            style={{ marginBottom: '10px' }}
          />
          <Input
            placeholder="Address Line 2"
            value={addressLine2}
            style={{ marginBottom: '10px' }}
          />
          <Input
            placeholder="Address Line 3"
            value={addressLine3}
            style={{ marginBottom: '10px' }}
          />
          <Input placeholder="pincode" value={pincode} />
        </Form.Item>
      </Col>
      <Col {...columnResponsiveLayout.colOneThird}>
        <Form.Item
          label="City"
          name="city"
          rules={[
            {
              required: true,
              message: 'Please input the City!',
            },
          ]}
        >
          <Select
            defaultValue="Gurgaon"
            style={{ width: 120 }}
            onChange={() => {}}
            value={city}
          >
            <Option value="jack">Jack</Option>
            <Option value="lucy">Lucy</Option>
            <Option value="Yiminghe">yiminghe</Option>
          </Select>
        </Form.Item>
      </Col>
      <Col {...columnResponsiveLayout.colOneThird}>
        <Form.Item
          label="Country"
          name="country"
          rules={[
            {
              required: true,
              message: 'Please input the Country!',
            },
          ]}
        >
          <Input placeholder="Country" disabled value={country} />
        </Form.Item>
      </Col>
    </>
  );
}

export default AddressField;
