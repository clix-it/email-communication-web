/**
 *
 * Asynchronously loads the component for CustomerDetailsPageHeader
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
