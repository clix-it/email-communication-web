/**
 *
 * CustomerDetailsPageHeader
 *
 */

import React, { memo } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { PageHeader, Breadcrumb, Button } from 'antd';
import { setMainUser } from '../../containers/RelationshipDetails/actions';

const BreadcrumbWrapper = styled.div`
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  color: rgba(0, 0, 0, 0.65);
  font-size: 14px;
  font-variant: tabular-nums;
  line-height: 1.5715;
  list-style: none;
  -webkit-font-feature-settings: 'tnum';
  font-feature-settings: 'tnum';
  position: relative;
  padding: 16px 10px;
  background-color: #f0f0f0;
`;

function CustomerDetailsPageHeader({ userDetails, userArray, dispatch }) {
  const history = useHistory();
  const handleBreadcrumb = (value, index) => {
    if (value === 'home') {
      history.push(`/customerDetails`);
    } else {
      dispatch(setMainUser(value, index + 1));
    }
  };
  return (
    <>
      <PageHeader
        className="site-page-header"
        title="Details Viewed for Customer :"
        subTitle={
          userDetails && userDetails.cuid && userDetails.type === 'INDIVIDUAL' && userDetails.firstName
            ? `${userDetails.firstName} ${userDetails.lastName}`
            : userDetails && userDetails.cuid && `${userDetails.registeredName || userDetails.displayName}`
        }
      />
      <BreadcrumbWrapper>
        <Breadcrumb>
          <Breadcrumb.Item>
            <Button type="link" onClick={() => handleBreadcrumb('home')}>
              Home
            </Button>
          </Breadcrumb.Item>
          {userArray &&
            userArray.length > 0 &&
            userArray.map((item, index) => {
              return (
                <Breadcrumb.Item>
                  {userArray.length !== index + 1 ? (
                    <Button
                      type="link"
                      onClick={() => handleBreadcrumb(item.cuid, index)}
                    >
                      {item.type === 'INDIVIDUAL' && item.firstName
                        ? `${item.firstName} ${item.lastName}`
                        : item.registeredName || item.displayName}
                    </Button>
                  ) : (
                    <>
                      {item.type === 'INDIVIDUAL' && item.firstName
                        ? `${item.firstName} ${item.lastName}`
                        : item.registeredName || item.displayName}
                    </>
                  )}
                </Breadcrumb.Item>
              );
            })}
        </Breadcrumb>
      </BreadcrumbWrapper>
    </>
  );
}

CustomerDetailsPageHeader.propTypes = {};

export default memo(CustomerDetailsPageHeader);
