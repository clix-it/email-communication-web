import styled from 'styled-components';
// import Button from '@material-ui/core/Button';
import buttonStyles from './buttonStyles';
// import { withStyles } from '@material-ui/core/styles';
const StyledButton = styled.button`
  ${buttonStyles};
`;

const ColorButton = withStyles(theme => ({
  root: {
    color: '#ffffff',
    backgroundColor: '#873682',
    paddingLeft: '30px',
    paddingRight: '30px',
    outline: 'none',
    border: 'none',
    '&:hover': {
      backgroundColor: 'purple[700]',
    },
    '&:focus': {
      outline: 'none',
    },
  },
}))(Button);

export { StyledButton, ColorButton };
