import React, { memo } from 'react';
import EditableTable from 'components/EditableTable';
import _ from 'lodash';
import { isEmpty } from '../../utils/helpers';

function BusinessDetailsEntity({
  values,
  userIndex,
  setValue,
  initDataSource,
  disabled,
  BUSINESSTYPE,
  COMPANYCATEGORY,
}) {
  const initColumns = [
    // {
    //   title: 'Business Type',
    //   dataIndex: 'companyType',
    //   editable: true,
    //   width: 200,
    //   type: 'select',
    //   dropdown:
    //     BUSINESSTYPE.length > 0 ? BUSINESSTYPE.map(item => item.code) : [],
    //   required: false,
    //   render: value => {
    //     const index = _.findIndex(BUSINESSTYPE, e => value === e.value, 0);
    //     return (
    //       <div>
    //         {BUSINESSTYPE.length > 0 && index >= 0
    //           ? BUSINESSTYPE[index].code
    //           : value}
    //       </div>
    //     );
    //   },
    // },
    {
      title: 'Business Profile',
      dataIndex: 'companyIndutry',
      editable: true,
      width: 200,
      type: 'select',
      dropdown:
        COMPANYCATEGORY.length > 0
          ? COMPANYCATEGORY.map(item => item.value)
          : [],
      required: false,
      render: value => {
        const index = _.findIndex(COMPANYCATEGORY, e => value === e.code, 0);
        return (
          <div>
            {COMPANYCATEGORY.length > 0 && index >= 0
              ? COMPANYCATEGORY[index].value
              : value}
          </div>
        );
      },
    },
    {
      title: 'Business Vintage(in months)',
      dataIndex: 'businessVintageInMonths',
      editable: true,
      width: 200,
      required: false,
    },
    {
      title: 'Entity Name',
      dataIndex: 'companyName',
      editable: true,
      width: 200,
      required: false,
    },
  ];

  const onChangeValues = data => {
    const dataToSave = [];
    if (data.length > 0) {
      data.forEach(item => {
        const updatedItem = { ...item };
        Object.keys(item).forEach(element => {
          if (item[element] && item[element].toString().includes('Enter')) {
            updatedItem[element] = '';
          } else if (item[element] && !item.id) {
            if (element === 'companyType') {
              const index = _.findIndex(
                BUSINESSTYPE,
                e => item[element] === e.code,
                0,
              );
              updatedItem[element] =
                index >= 0 ? BUSINESSTYPE[index].value : '';
            }
            if (element === 'companyIndutry') {
              const index = _.findIndex(
                COMPANYCATEGORY,
                e => item[element] === e.value,
                0,
              );
              updatedItem[element] =
                index >= 0 ? COMPANYCATEGORY[index].code : '';
            }
          }
        });
        if (!isEmpty(_.omit(updatedItem, ['key', 'id']))) {
          dataToSave.push(_.omit(updatedItem, ['key']));
        }
      });
      setValue(`users[${userIndex}].userEmployments`, dataToSave);
    }
  };

  return (
    <EditableTable
      initDataSource={initDataSource}
      initColumns={initColumns}
      onChangeValues={onChangeValues}
      columnsGiven
      notShowDeleteBtn
      addTitle="Add Business details"
      disabled={disabled}
    />
  );
}

export default memo(BusinessDetailsEntity);
