/**
 *
 * ContactDetails
 *
 */
import _ from 'lodash';
import { Row, Col, Divider } from 'antd';
import InputField from 'components/InputField';
import Pincode from 'containers/Pincode';
import React, { memo, useEffect } from 'react';
import AddressCollapse from 'components/AddressCollapse';
import StyledTitle from 'components/StyledTitle';
import AddressChanged from '../AddressChanged';

import {
  mobilePattern,
  emailPattern,
  numberPattern,
} from '../../containers/App/constants';

// import PropTypes from 'prop-types';
// import styled from 'styled-components';
const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};
function ContactDetails({
  control,
  errors,
  cuid,
  userIndex,
  values,
  setError,
  clearError,
  reset,
  getValues,
  setValue,
  defaultValues,
  contactMap,
  entityDataId,
  allFieldsDisabled,
  partner,
}) {
  return (
    <>
      <Row gutter={[16, 16]}>
        {_.get(values, `users[${userIndex}].contactibilities[0].id`) && (
          <Col xs={0}>
            <InputField
              id="id"
              control={control}
              name={`users[${userIndex}].contactibilities[0].id`}
              type="hidden"
              errors={errors}
            />
          </Col>
        )}
        <Col {...responsiveColumns}>
          <InputField
            id={`users[${userIndex}].contactibilities[0].phoneNumber`}
            control={control}
            name={`users[${userIndex}].contactibilities[0].phoneNumber`}
            type="string"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              pattern: mobilePattern,
            }}
            errors={errors}
            placeholder="Mobile Number"
            labelHtml="Registered Mobile Number*"
            disabled={allFieldsDisabled}
          />
        </Col>

        <Col {...responsiveColumns}>
          <InputField
            id={`users[${userIndex}].contactibilities[0].email`}
            control={control}
            name={`users[${userIndex}].contactibilities[0].email`}
            type="string"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              pattern: emailPattern,
            }}
            errors={errors}
            placeholder="Email"
            labelHtml="Registered Email*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`users[${userIndex}].preferredPhone`}
            control={control}
            reset={reset}
            name={`users[${userIndex}].preferredPhone`}
            type="string"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Contact number of office"
            labelHtml="Contact number of office*"
            disabled={allFieldsDisabled}
          />
        </Col>
        {partner != 'HFSAPP' && (
          <>
            <InputField
              id="additionalData.data.addressChangesFlages"
              control={control}
              name="additionalData.data.addressChangesFlages"
              type="hidden"
              errors={errors}
              disabled={allFieldsDisabled}
            />
            <Col {...responsiveColumns}>
              <AddressChanged cuid={cuid} values={values} />
            </Col>
          </>
        )}
      </Row>
      {entityDataId && String(entityDataId).includes('NEW') && (
        <>
          <StyledTitle>Primary Address</StyledTitle>
          <Row gutter={[16, 16]}>
            <Col {...responsiveColumns}>
              <InputField
                className="form-control"
                control={control}
                id={`users[${userIndex}].contactibilities[0].contactType`}
                name={`users[${userIndex}].contactibilities[0].contactType`}
                errors={errors}
                type="select"
                options={
                  partner === 'HFSAPP'
                    ? [
                        {
                          code: 'OFFICE',
                          value: 'Office Address',
                        },
                      ]
                    : ['OFFICE']
                }
                isAddressSelect={partner === 'HFSAPP'}
                rules={{
                  required: true,
                  message: 'This field is required',
                }}
                placeholder="Address Type"
                labelHtml="Address Type"
              />
            </Col>
            <Col {...responsiveColumns}>
              <InputField
                id={`users[${userIndex}].contactibilities[0].addressLine1`}
                control={control}
                name={`users[${userIndex}].contactibilities[0].addressLine1`}
                type="string"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Address Line 1"
                labelHtml="Address Line 1*"
              />
            </Col>
            <Col {...responsiveColumns}>
              <InputField
                id={`users[${userIndex}].contactibilities[0].addressLine2`}
                control={control}
                name={`users[${userIndex}].contactibilities[0].addressLine2`}
                type="string"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Address Line 2"
                labelHtml="Address Line 2*"
              />
            </Col>
            <Col {...responsiveColumns}>
              <InputField
                id={`users[${userIndex}].contactibilities[0].addressLine3`}
                control={control}
                name={`users[${userIndex}].contactibilities[0].addressLine3`}
                type="string"
                rules={{
                  required: {
                    value: false,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Address Line 3"
                labelHtml="Address Line 3"
              />
            </Col>
            <Pincode
              namePrefix={`users[${userIndex}].contactibilities[0]`}
              control={control}
              reset={reset}
              values={values}
              setValue={setValue}
              getValues={getValues}
              setError={setError}
              clearError={clearError}
              pincode={_.get(
                values,
                `users[${userIndex}].contactibilities[0].pincode`,
              )}
              type="string"
              defaultValue={
                defaultValues.users.length > 0
                  ? defaultValues.users[userIndex].contactibilities[0].pincode
                  : ''
              }
              rules={{
                required: true,
                message: 'This field is required',
                pattern: {
                  value: /[1-9][0-9]{5}/i,
                  message: 'Please enter correct pincode',
                },
              }}
              errors={errors}
            />
          </Row>
        </>
      )}
      {/* {contactMap && (
        <AddressCollapse
          responsiveColumns={responsiveColumns}
          userIndex={userIndex}
          control={control}
          errors={errors}
          reset={reset}
          values={values}
          setValue={setValue}
          getValues={getValues}
          setError={setError}
          clearError={clearError}
          contactArr={contactMap}
        />
      )} */}
    </>
  );
}

ContactDetails.propTypes = {};

export default memo(ContactDetails);
