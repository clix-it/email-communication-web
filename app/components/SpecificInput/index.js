import React from 'react';

const SpecificInput = React.forwardRef(
  (
    { placeholder, id, type, name, className, children, value, onChange },
    ref,
  ) => (
    <label htmlFor={id}>
      <input
        placeholder={placeholder}
        type={type}
        id={id}
        aria-label={placeholder}
        ref={ref}
        name={name}
        className={className}
        value={value}
        onChange={() => onChange()}
      />
      {children}
    </label>
  ),
);

export default SpecificInput;
