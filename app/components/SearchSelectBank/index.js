/**
 *
 * SearchSelectBank
 *
 */

import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { useForm, Controller } from 'react-hook-form';

import _ from 'lodash';

import { Select } from 'antd';
import InputField from '../InputField';

function SearchSelectBank({
  applicantDetails,
  setSelectedBank,
  selectBank,
  handleFetchBanks,
}) {
  const { register, handleSubmit, errors, watch, control } = useForm({
    mode: 'onChange',
  });
  const values = watch();
  values.bank = values.bank || selectedBank;
  const options = () => (selectBank.response || {}).banks || [];
  const [selectedBank, setselectedBank] = useState(false);

  const noOptionsMessage = value => {
    const { inputValue } = value;
    if (!inputValue) {
      return 'Please enter atleast 3 chars';
    }
  };

  return (
    <div>
      <InputField
        id="disbursmentBankName"
        control={control}
        name="disbursmentBankName"
        type="select"
        noOptionsMessage={noOptionsMessage}
        options={options()}
        isSearchable
        onInputChange={handleFetchBanks}
        defaultValue={
          applicantDetails.entity.bankDetail &&
          applicantDetails.entity.bankDetail.disbursmentBankName
        }
        rules={{
          required: {
            value: true,
            message: 'This field cannot be left empty',
          },
        }}
        errors={errors}
        placeholder="Bank Name"
        labelHtml="Bank Name*"
      />
      {/* <Controller
      
        name="bankName"
        control={control}
        onChange={([selected]) => {
          setselectedBank(selected);
          return selected;
        }}
        as={
          <Select
            className="nice-select"
            options={options()}
            onInputChange={handleFetchBanks}
            labelKey="name"
            noOptionsMessage={noOptionsMessage}
            // onChange={setselectedBank}
            searchable
            clearable
            isLoading={selectBank.loading}
            isSearchable
            // components={optionComponent}
            key="id"
          />
        }
      /> */}
    </div>
  );
}

SearchSelectBank.propTypes = {};

export default SearchSelectBank;
