/*
 * SearchSelectBank Messages
 *
 * This contains all the text for the SearchSelectBank component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.SearchSelectBank';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the SearchSelectBank component!',
  },
});
