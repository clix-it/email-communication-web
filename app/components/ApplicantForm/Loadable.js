/**
 *
 * Asynchronously loads the component for ApplicantForm
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
