/**
 *
 * ApplicantForm
 *
 */

import _ from 'lodash';
import { Row, Col, Divider, Button, Radio, Form } from 'antd';
import InputField from 'components/InputField';
import Pincode from 'containers/Pincode';
import SelectRelationship from 'containers/SelectRelationship';
import PanNumber from 'containers/PanNumber';
import Identities from 'components/Identities';
import Education from 'components/Education';
import Employment from 'components/Employment';
import DoctorOccupationalDetails from 'components/DoctorOccupationalDetails';
import Relationships from 'components/Relationships';
import StyledTitle from 'components/StyledTitle';
import AddressChanged from 'components/AddressChanged';
import moment from 'moment';
import { Controller } from 'react-hook-form';

import React, { memo, useEffect } from 'react';
import {
  mobilePattern,
  emailPattern,
  numberPattern,
} from '../../containers/App/constants';
import { stringSimilar, getSalutation } from '../../utils/helpers';
import { SALUTATION, HFS_ADDRESSES, ADDRESSES } from '../../utils/constants';

const RELATIONS_WITH_BORROWER = [
  'Proprietor',
  'Director',
  'Partner',
  'Trustee',
  'Shareholder',
  'Property Owner',
  'Secretary',
  'Treasurer',
  'Member',
  'Others',
];

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function ApplicantForm({
  control,
  errors,
  userIndex,
  values,
  setError,
  clearError,
  reset,
  entity,
  getValues,
  setValue,
  counter,
  cuid,
  allFieldsDisabled,
  cibilScore,
  foir,
  applicant,
  contactArr,
  isNoEntity,
  requestType,
  handleGenerateCibilReport,
  handleGenerateDelphiReport,
  applicantId,
  product,
  partner,
  setEducation,
  education,
  applicantDetails,
}) {
  const [panNamed, setPanNamed] = React.useState();
  const userEdu = _.get(applicant, `userEducations`, []).filter(
    item => item.qualification,
  );
  const panName = val => {
    setPanNamed(val);
  };

  // useEffect(() => () => reset(), [applicant]);

  const checkField = (fieldName, fValue) => {
    setValue(`${fieldName}`, fValue);
    if (fValue && _.get(errors, fieldName)) {
      clearError(`${fieldName}`);
    } else {
      setError(`${fieldName}`);
    }
  };

  useEffect(() => {
    // debugger;
    let varCibilScore = '';
    let varFoir = '';
    if (applicant && applicant.cuid) {
      if (
        _.get(entity, 'additionalData.data.extraDataField.consumerCibilScore')
      ) {
        const consumerCibilScore = JSON.parse(
          _.get(
            entity,
            'additionalData.data.extraDataField.consumerCibilScore',
          ),
        );
        varCibilScore = consumerCibilScore[`${applicant.cuid}`] || '';
      }
      if (_.get(entity, 'additionalData.data.extraDataField.foir')) {
        varFoir = _.get(entity, 'additionalData.data.extraDataField.foir', '');
      }
    }
    checkField('consumerCibilScore', varCibilScore);
    checkField('foir', varFoir);
    checkField('applicants[0].salutation', _.get(applicant, 'salutation', ''));
  }, [applicant]);

  useEffect(() => {
    if (panNamed) {
      const name =
        `${_.get(values, `applicants[${userIndex}].firstName`)} ` +
        `${_.get(values, `applicants[${userIndex}].middleName`)} ` +
        ` ${_.get(values, `applicants[${userIndex}].lastName`)}`;

      const similarity = stringSimilar(name, panNamed);
      if (similarity * 100 < 90) {
        // setError(
        //   `applicants[${userIndex}].userIdentities.pan`,
        //   'pattern',
        //   'PAN name doesnt match with Name entered.',
        // );
      } else {
        clearError(`applicants[${userIndex}].userIdentities.pan`);
      }
    }
  }, [
    _.get(values, `applicants[${userIndex}].firstName`),
    _.get(values, `applicants[${userIndex}].middleName`),
    _.get(values, `applicants[${userIndex}].lastName`),
  ]);

  useEffect(() => {
    const genderValue = _.get(
      values,
      `applicants[${userIndex}].userDetails.gender`,
    );
    const maritalStatusValue = _.get(
      values,
      `applicants[${userIndex}].userDetails.maritalStatus`,
    );
    if (genderValue && maritalStatusValue)
      setValue(
        `applicants[${userIndex}].salutation`,
        getSalutation(genderValue, maritalStatusValue),
      );
  }, [
    _.get(values, `applicants[${userIndex}].userDetails.gender`),
    _.get(values, `applicants[${userIndex}].userDetails.maritalStatus`),
  ]);

  console.log('values in applicant form', values, 'errors', errors);
  return (
    <>
      <StyledTitle>Basic Details</StyledTitle>
      <Row gutter={[16, 16]}>
        <Col xs={0}>
          <InputField
            id="id"
            control={control}
            name={`applicants[${userIndex}].entityOfficers.belongsTo`}
            type="hidden"
            errors={errors}
          />
          <InputField
            id="id"
            control={control}
            name={`applicants[${userIndex}].cuid`}
            type="hidden"
            errors={errors}
          />
          {/* <InputField
          id="id"
          control={control}
          name={`applicants[${userIndex}].userDetails.id`}
          type="hidden"
          errors={errors}
        /> */}
          {_.get(values, `applicants[${userIndex}].contactibilities[0].id`) && (
            <InputField
              id="id"
              control={control}
              name={`applicants[${userIndex}].contactibilities[0].id`}
              type="hidden"
              errors={errors}
            />
          )}
          <InputField
            id="id"
            control={control}
            name={`applicants[${userIndex}].id`}
            type="hidden"
            errors={errors}
          />
          {/* _.get(applicant, 'userIdentities', '') &&
            _.get(applicant, 'userIdentities.id', '') && (
              <InputField
                id={`applicants[${userIndex}].userIdentities.id`}
                control={control}
                name={`applicants[${userIndex}].userIdentities.id`}
                type="hidden"
                defaultValue={_.get(applicant, 'userIdentities.id', '')}
                errors={errors}
              />
            ) */}
          <InputField
            id={`applicants[${userIndex}].entityOfficers.id`}
            control={control}
            name={`applicants[${userIndex}].entityOfficers.id`}
            type="hidden"
            errors={errors}
          />
          <InputField
            id={`applicants[${userIndex}].userEducations`}
            control={control}
            name={`applicants[${userIndex}].userEducations`}
            type="hidden"
            errors={errors}
          />
          <InputField
            id={`applicants[${userIndex}].userEmployments`}
            control={control}
            name={`applicants[${userIndex}].userEmployments`}
            type="hidden"
            errors={errors}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].salutation`}
            control={control}
            name={`applicants[${userIndex}].salutation`}
            type="string"
            // rules={{
            //   required: {
            //     value: true,
            //     message: 'This field cannot be left empty',
            //   },
            // }}
            // options={SALUTATION}
            errors={errors}
            placeholder="Salutation"
            labelHtml="Salutation*"
            disabled
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].firstName`}
            control={control}
            name={`applicants[${userIndex}].firstName`}
            type="string"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              // pattern: emailPattern,
            }}
            errors={errors}
            placeholder="First Name"
            labelHtml={
              isNoEntity
                ? 'First Name*'
                : `First Name of Co Applicant ${counter}*`
            }
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].middleName`}
            control={control}
            name={`applicants[${userIndex}].middleName`}
            type="string"
            // rules={{
            //   required: {
            //     value: true,
            //     message: 'This field cannot be left empty',
            //   },
            //   // pattern: emailPattern,
            // }}
            errors={errors}
            placeholder="Middle Name"
            labelHtml={
              isNoEntity
                ? 'Middle Name'
                : `Middle Name of Co Applicant ${counter}`
            }
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].lastName`}
            control={control}
            name={`applicants[${userIndex}].lastName`}
            type="string"
            rules={{
              required: {
                value: false,
                message: 'This field cannot be left empty',
              },
              // pattern: mobilePattern,
            }}
            errors={errors}
            placeholder="Last Name"
            labelHtml={
              isNoEntity ? 'Last Name' : `Last Name of Co Applicant ${counter}`
            }
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].userDetails.gender`}
            control={control}
            name={`applicants[${userIndex}].userDetails.gender`}
            type="select"
            options={['Male', 'Female', 'Others']}
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              // pattern: mobilePattern,
            }}
            errors={errors}
            placeholder="Gender"
            labelHtml="Select Gender*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].dateOfBirthIncorporation`}
            control={control}
            reset={reset}
            // disabledDate={
            //   current => current && moment().diff(current, 'years') < 25 // check for age to be greater than 25 years
            // }
            name={`applicants[${userIndex}].dateOfBirthIncorporation`}
            type="datepicker"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Date Of Birth"
            labelHtml="Date Of Birth*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <SelectRelationship
            key={`applicants[${userIndex}].entityOfficers`}
            setValue={setValue}
            userIndex={userIndex}
            reset={reset}
            namePrefix={`applicants[${userIndex}].entityOfficers`}
            id={`applicants[${userIndex}].entityOfficers.designation`}
            control={control}
            name={`applicants[${userIndex}].entityOfficers.designation`}
            type="select"
            options={RELATIONS_WITH_BORROWER}
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Role/Relation"
            labelHtml="Role/Relation with Borrower Entity*"
            disabled={allFieldsDisabled}
            isNoEntity={isNoEntity}
            clearError={clearError}
          />
        </Col>
        {partner === 'DSA' && (
          <>
            <Col {...responsiveColumns}>
              <InputField
                id={`applicants[${userIndex}].userDetails.per_share_holding_if_any`}
                control={control}
                name={`applicants[${userIndex}].userDetails.per_share_holding_if_any`}
                type="string"
                rules={{
                  required: {
                    value: false,
                    message: 'This field cannot be left empty',
                  },
                  pattern: {
                    value: /^[0-9]+([.][0-9]+)?$/,
                    message: 'Please enter valid number',
                  },
                }}
                errors={errors}
                placeholder="% Holding in Business"
                labelHtml="% Holding in Business"
              />
            </Col>
            {/* <Col {...responsiveColumns}>
              <Form.Item label="If none of the property is owned, then ask- Is any other built-up property?">
                <Controller
                  name={`applicants[${userIndex}].userDetails.any_build_up_property`}
                  control={control}
                  onChange={([event]) => event.target.value}
                  rules={{
                    required: {
                      value: false,
                      message: 'Please select either credit or sales',
                    },
                  }}
                  as={
                    <Radio.Group
                      name={`applicants[${userIndex}].userDetails.any_build_up_property`}
                    >
                      <Radio value="Yes">Yes</Radio>
                      <Radio value="No">No</Radio>
                    </Radio.Group>
                  }
                />
              </Form.Item>
            </Col> */}
          </>
        )}
      </Row>
      <Divider />
      <StyledTitle>Identities</StyledTitle>
      <Row gutter={[16, 16]}>
        <Col {...responsiveColumns}>
          <PanNumber
            entityName={
              `${_.get(values, `applicants[${userIndex}].firstName`, '')} ` +
              `${_.get(values, `applicants[${userIndex}].middleName`, '')} ` +
              ` ${_.get(values, `applicants[${userIndex}].lastName`, '')}`
            }
            panName={panName}
            id={`applicants[${userIndex}].userIdentities.pan`}
            pan={_.get(values, `applicants[${userIndex}].userIdentities.pan`)}
            control={control}
            name={`applicants[${userIndex}].userIdentities.pan`}
            type="string"
            errors={errors}
            values={values}
            setError={setError}
            clearError={clearError}
            rules={{
              required: {
                value: product != 'TW',
                message: 'This Field should not be empty!',
              },
              pattern: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/,
            }}
            placeholder="PAN/CKYC ID"
            labelHtml="PAN/CKYC ID*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Identities
          responsiveColumns={responsiveColumns}
          control={control}
          type="string"
          errors={errors}
          values={values}
          setError={setError}
          clearError={clearError}
          disabled={allFieldsDisabled}
          userIndex={userIndex}
        />
      </Row>
      {/* {applicant &&
        applicant.userIdentities &&
        applicant.userIdentities.ckycNumber ? (
          <Col {...responsiveColumns}>
            <InputField
              id={`applicants[${userIndex}].userIdentities.ckycNumber`}
              control={control}
              name={`applicants[${userIndex}].userIdentities.ckycNumber`}
              type="string"
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="CKYC No."
              labelHtml="CKYC No."
              disabled={allFieldsDisabled}
            />
          </Col>
        ) : null}
        {applicant &&
        applicant.userIdentities &&
        applicant.userIdentities.aadhar ? (
          <Col {...responsiveColumns}>
            <InputField
              id={`applicants[${userIndex}].userIdentities.aadhar`}
              control={control}
              name={`applicants[${userIndex}].userIdentities.aadhar`}
              type="string"
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="AADDHAR No."
              labelHtml="AADDHAR No."
              disabled={allFieldsDisabled}
            />
          </Col>
        ) : null} */}
      <Divider />
      <StyledTitle>Education</StyledTitle>
      <Education
        responsiveColumns={responsiveColumns}
        values={values}
        userIndex={userIndex}
        setValue={setValue}
        disabled={requestType !== 'PMA' || partner !== 'HFSAPP'}
        initDataSource={_.get(applicant, `userEducations`, [])}
        isGuarantor={false}
        applicantDetails={applicantDetails}
        partner={partner}
      />
      {partner == 'DL' && (
        <>
          <Divider />
          <StyledTitle>OccupationalDetails</StyledTitle>
          <DoctorOccupationalDetails
            responsiveColumns={responsiveColumns}
            values={values}
            userIndex={userIndex}
            setValue={setValue}
            control={control}
            userEmployments={_.get(applicant, `userEmployments`, [])}
          />
        </>
      )}
      {partner !== 'DSA' && partner !== 'DL' && (
        <>
          <Divider />
          <StyledTitle>Employment Details</StyledTitle>
          <Employment
            responsiveColumns={responsiveColumns}
            values={values}
            userIndex={userIndex}
            setValue={setValue}
            disabled
            initDataSource={_.get(applicant, `userEmployments`, [])}
          />
          {/* <OccupationalDetails
            responsiveColumns={responsiveColumns}
            values={values}
            userIndex={userIndex}
            setValue={setValue}
            control={control}
            userEmployments={_.get(applicant, `userEmployments`, [])}
            disabled
          /> */}
        </>
      )}
      <Divider />
      <StyledTitle>Relationships</StyledTitle>
      <Relationships
        responsiveColumns={responsiveColumns}
        control={control}
        type="string"
        errors={errors}
        values={values}
        setError={setError}
        clearError={clearError}
        disabled={allFieldsDisabled}
        userIndex={userIndex}
        setValue={setValue}
        applicant={applicant}
      />
      <Divider />
      <StyledTitle>Contact Details</StyledTitle>
      <Row gutter={[16, 16]}>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].preferredPhone`}
            control={control}
            reset={reset}
            name={`applicants[${userIndex}].preferredPhone`}
            type="string"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              pattern: mobilePattern,
            }}
            errors={errors}
            placeholder="Mobile Number"
            labelHtml={
              isNoEntity
                ? 'Registered Mobile Number*'
                : 'Co-Applicant Mobile Number*'
            }
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`applicants[${userIndex}].preferredEmail`}
            control={control}
            reset={reset}
            name={`applicants[${userIndex}].preferredEmail`}
            type="string"
            rules={{
              pattern: emailPattern,
            }}
            errors={errors}
            placeholder="Email"
            labelHtml={isNoEntity ? 'Registered Email' : 'Co-Applicant Email'}
            disabled={allFieldsDisabled}
          />
        </Col>
      </Row>

      <Divider />
      {applicantId && String(applicantId).includes('NEW') && (
        <>
          <StyledTitle>Primary Address</StyledTitle>
          <Row gutter={[16, 16]}>
            <Col {...responsiveColumns}>
              <InputField
                className="form-control"
                control={control}
                id={`applicants[${userIndex}].contactibilities[0].contactType`}
                name={`applicants[${userIndex}].contactibilities[0].contactType`}
                errors={errors}
                type="select"
                options={partner === 'HFSAPP' ? HFS_ADDRESSES : ADDRESSES}
                isAddressSelect={partner === 'HFSAPP'}
                rules={{
                  required: true,
                  message: 'This field is required',
                }}
                placeholder="Address Type"
                labelHtml="Address Type"
              />
            </Col>
            <Col {...responsiveColumns}>
              <InputField
                id={`applicants[${userIndex}].contactibilities[0].addressLine1`}
                control={control}
                name={`applicants[${userIndex}].contactibilities[0].addressLine1`}
                type="string"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Address Line 1"
                labelHtml="Address Line 1*"
                disabled={allFieldsDisabled}
              />
            </Col>
            <Col {...responsiveColumns}>
              <InputField
                id={`applicants[${userIndex}].contactibilities[0].addressLine2`}
                control={control}
                name={`applicants[${userIndex}].contactibilities[0].addressLine2`}
                type="string"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Address Line 2"
                labelHtml="Address Line 2*"
                disabled={allFieldsDisabled}
              />
            </Col>
            <Col {...responsiveColumns}>
              <InputField
                id={`applicants[${userIndex}].contactibilities[0].addressLine3`}
                control={control}
                name={`applicants[${userIndex}].contactibilities[0].addressLine3`}
                type="string"
                rules={{
                  required: {
                    value: false,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Address Line 3"
                labelHtml="Address Line 3"
                disabled={allFieldsDisabled}
              />
            </Col>
            <Pincode
              namePrefix={`applicants[${userIndex}].contactibilities[0]`}
              control={control}
              reset={reset}
              values={values}
              setValue={setValue}
              getValues={getValues}
              setError={setError}
              clearError={clearError}
              pincode={_.get(
                values,
                `applicants[${userIndex}].contactibilities[0].pincode`,
              )}
              defaultValue={
                applicant && applicant.contactibilities
                  ? applicant.contactibilities[0].pincode
                  : ''
              }
              type="string"
              rules={{
                required: true,
                message: 'This field is required',
                pattern: {
                  value: /[1-9][0-9]{5}/i,
                  message: 'Please enter correct pincode',
                },
              }}
              errors={errors}
              disabled={allFieldsDisabled}
            />
          </Row>
        </>
      )}
      {/* <Col {...responsiveColumns}>
          <InputField
            id="consumerCibilScore"
            control={control}
            reset={reset}
            name="consumerCibilScore"
            type="string"
            defaultValue={cibilScore || ''}
            rules={{
              required: {
                value: !isNoEntity,
                message: 'This field cannot be left empty',
              },
              pattern: /^([0-9]){1,}$/,
            }}
            errors={errors}
            placeholder="Consumer Cibil Score"
            labelHtml={
              isNoEntity ? 'Consumer Cibil Score' : 'Consumer Cibil Score*'
            }
            disabled={allFieldsDisabled}
          />
        </Col>
        {cuid ? (
          <Col {...responsiveColumns}>
            <AddressChanged cuid={cuid} values={values} />
          </Col>
        ) : (
          ''
        )} */}

      <StyledTitle>Other Details</StyledTitle>
      <Row gutter={[16, 16]}>
        {partner !== 'HFSAPP' && (
          <Col {...responsiveColumns}>
            <InputField
              id="foir"
              control={control}
              reset={reset}
              name="foir"
              type="string"
              defaultValue={foir || ''}
              rules={{
                required: {
                  value: false,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="FOIR"
              labelHtml="FOIR"
              disabled={allFieldsDisabled}
            />
          </Col>
        )}
        <Col {...responsiveColumns}>
          <InputField
            id="consumerCibilScore"
            control={control}
            reset={reset}
            name="consumerCibilScore"
            type="string"
            defaultValue={cibilScore || ''}
            rules={{
              required: {
                value: !isNoEntity,
                message: 'This field cannot be left empty',
              },
              pattern: /^([0-9]){1,}$/,
            }}
            errors={errors}
            placeholder="Consumer Cibil Score"
            labelHtml={
              isNoEntity ? 'Consumer Cibil Score' : 'Consumer Cibil Score*'
            }
            disabled={allFieldsDisabled}
          />
        </Col>
        {cuid && partner !== 'DSA' && partner !== 'HFSAPP' ? (
          <Col {...responsiveColumns}>
            <AddressChanged entity={entity} cuid={cuid} values={values} />
          </Col>
        ) : (
          ''
        )}
        {cuid && (
          <Col {...responsiveColumns}>
            <Button
              type="primary"
              htmlType="button"
              // style={{
              //   marginRight: '10px',
              //   marginTop: '42px',
              //   marginLeft: '100px',
              // }}
              onClick={() => handleGenerateCibilReport(applicant)}
              disabled={!applicant.userLinked}
            >
              Generate CIBIL Bureau Report
            </Button>
            {product && product != 'BL' && product != 'LAEP' && (
              <Button
                type="primary"
                htmlType="button"
                style={{
                  marginRight: '10px',
                  marginTop: '42px',
                  marginLeft: '100px',
                }}
                onClick={() => handleGenerateDelphiReport(applicant)}
                disabled={!applicant.userLinked}
              >
                Generate Bureau and Delphi Report
              </Button>
            )}
          </Col>
        )}
      </Row>
    </>
  );
}

ApplicantForm.propTypes = {};

export default memo(ApplicantForm);
