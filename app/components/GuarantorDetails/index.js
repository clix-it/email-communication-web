import _ from 'lodash';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import GuarantorPage from 'containers/GuarantorPage';
import { Row, Col, Button, Empty, Spin, message, Menu } from 'antd';
import { formatDate } from 'utils/helpers';
import AddressSection from '../../containers/AddressSection';

import {
  removeApplicant,
  updateApplicantDetails,
  loadEntityDetails,
  clearMessage,
  generateCibilReport,
  addGuarantor,
  setSelectedGuarantor,
  isDirtyForm,
  unlinkGuarantor,
  setFormValidationError,
  updateCoApplicants,
} from '../../containers/ApplicantDetails/actions';
import { columnResponsiveLayout } from '../EntityDetails/constants';
import './personalStyle.css';
function GuarantorDetails({
  appId,
  applicantsData,
  dispatch,
  applicationData,
  requestType,
  applicantDetails,
  isNoEntity,
  activity,
}) {
  const {
    loading,
    updateLoading,
    cibilDocUploaded,
    delphiGenerated,
    success,
    masterData: { APPLICANTTYPE = [] } = {},
  } = applicantDetails;

  const [education, setEducation] = React.useState([]);

  const defaultValues = {
    guarantors: [
      {
        ...selectedGuarantor,
        entityOfficers: _.get(selectedGuarantor, 'entityOfficers', {}),
        userIdentities: _.get(selectedGuarantor, 'userIdentities')
          ? _.get(selectedGuarantor, 'userIdentities')
          : { pan: '' },
        userEmployments: _.get(selectedGuarantor, 'userEmployments[0].id')
          ? _.get(selectedGuarantor, 'userEmployments')
          : [],
        userEducations: _.get(selectedGuarantor, 'userEducations[0].id')
          ? _.get(selectedGuarantor, 'userEducations')
          : [],
        userDetails: _.get(selectedGuarantor, 'userDetails', {}),
        dateOfBirthIncorporation: formatDate(
          _.get(selectedGuarantor, 'dateOfBirthIncorporation', ''),
        ),
        firstName: _.get(selectedGuarantor, 'firstName', ''),
        lastName: _.get(selectedGuarantor, 'lastName', ''),
        middleName: _.get(selectedGuarantor, 'middleName', ''),
        salutation: _.get(selectedGuarantor, 'salutation', ''),
        preferredEmail: _.get(selectedGuarantor, 'preferredEmail', ''),
        preferredPhone: _.get(selectedGuarantor, 'preferredPhone', ''),
      },
    ],
    contactArr,
    'guarantors[0].userDetails.gender': _.get(
      selectedGuarantor,
      'userDetails.gender',
      '',
    ),
    // 'applicants[0].userDetails.any_build_up_property': _.get(
    //   selectedApplicant,
    //   'userDetails.any_build_up_property',
    //   '',
    // ),
    'guarantors[0].userDetails.per_share_holding_if_any': _.get(
      selectedGuarantor,
      'userDetails.per_share_holding_if_any',
      '',
    ),
  };

  const product =
    applicationData &&
    applicationData.entity &&
    _.get(applicationData.entity, 'product');
  const { entity, selectedGuarantor } = applicationData;
  let contactArr = {};
  let cibilScore = '';
  let foir = '';
  if (selectedGuarantor && selectedGuarantor.cuid) {
    if (
      entity &&
      entity.additionalData &&
      entity.additionalData.data &&
      entity.additionalData.data.extraDataField &&
      entity.additionalData.data.extraDataField.consumerCibilScore
    ) {
      const consumerCibilScore = JSON.parse(
        entity.additionalData.data.extraDataField.consumerCibilScore,
      );
      cibilScore = consumerCibilScore[`${selectedGuarantor.cuid}`] || '';
    }
    if (
      entity &&
      entity.additionalData &&
      entity.additionalData.data &&
      entity.additionalData.data.extraDataField &&
      entity.additionalData.data.extraDataField.foir
    ) {
      foir = entity.additionalData.data.extraDataField.foir || '';
    }
  }

  useEffect(() => {
    if (isNoEntity) {
      dispatch(setFormValidationError('all', {}));
    }
    reset();
  }, [appId]);

  if (selectedGuarantor && selectedGuarantor.cuid) {
    const contactibilities = _.orderBy(
      _.get(selectedGuarantor, 'contactibilities'),
      'id',
      'desc',
    );
    if (contactibilities && contactibilities.length > 0) {
      // contactibilities.shift();
      if (contactibilities.length === 1) {
        if (!_.get(contactibilities, '[0].contactType'))
          contactArr.Latest = contactibilities;
        else {
          contactArr[
            _.get(contactibilities, '[0].contactType')
          ] = contactibilities;
        }
      } else {
        contactArr = _.mapValues(
          _.groupBy(contactibilities, 'contactType'),
          // clist => clist.map(contact => _.omit(contact, 'contactType')),
        );
        Object.keys(contactArr).forEach(key => {
          if (!key) {
            contactArr.undefined = contactArr[key];
            delete contactArr[key];
          }
        });
      }
    }
    console.log('contactArr', contactArr);
  }

  const setApplicant = id => {
    reset({
      'guarantors[0].firstName': '',
      'guarantors[0].lastName': '',
      'guarantors[0].middleName': '',
      'guarantors[0].preferredPhone': '',
      'guarantors[0].preferredEmail': '',
      'guarantors[0].dateOfBirthIncorporation': '',
      'guarantors[0].salutation': '',
      // 'applicants[0].holdingInBuisness': '',
      // 'applicants[0].anotherBuiltInProperty': '',
      'guarantors[0].userEmployments[0].id': '',
      'guarantors[0].userEmployments[0].employmentType': '',
      'guarantors[0].userEmployments[0].companyType': '',
      'guarantors[0].userEmployments[0].companyIndutry': '',
      'guarantors[0].userEmployments[0].companyName': '',
      'guarantors[0].userEmployments[0].profession': '',
      'guarantors[0].userEmployments[0].officialEmail': '',
      'guarantors[0].userEmployments[0].currentExpInMonths': '',
      'guarantors[0].userEmployments[0].postQualificationExpInMonths': '',
      'guarantors[0].userEmployments[0].totalIncomeInMonths': '',
    });
    const applicant =
      _.find(entity.guarantors, { id: id.key }) ||
      _.find(entity.guarantors, { id: parseInt(id.key) });
    if (applicant) dispatch(setSelectedGuarantor(applicant));
  };

  useEffect(() => {
    if (success.status) {
      dispatch(loadEntityDetails(appId));
    }
    dispatch(clearMessage());
  }, [success.status]);

  useEffect(() => {
    if (cibilDocUploaded) {
      message.success(
        'Cibil Document is Uploaded SuccessFully. You can see the document in Documents Section!!',
        2,
      );
    }
    dispatch(clearMessage());
  }, [cibilDocUploaded]);

  useEffect(() => {
    if (delphiGenerated) {
      message.success('Delphi report generated SuccessFully.!!', 2);
    }
    dispatch(clearMessage());
  }, [delphiGenerated]);

  const {
    handleSubmit,
    errors,
    control,
    watch,
    setError,
    clearError,
    getValues,
    reset,
    setValue,
    triggerValidation,
    formState,
  } = useForm({
    mode: 'onChange',
    defaultValues,
  });

  useEffect(() => {
    dispatch(isDirtyForm({ 1: formState.dirty }));
  }, [formState.dirty]);

  useEffect(() => {
    reset({ guarantors: [selectedGuarantor] });
  }, [selectedGuarantor, reset]);

  const values = watch();
  const onSubmit = data => {
    // if (!formState.isValid) {
    //   triggerValidation();
    //   return message.error('Please fill valid data');
    // }
    console.log('data to save from personal', data);
    const entityCuid = _.get(
      _.find(
        _.get(applicantDetails, 'entity.users') || [],
        ele =>
          (_.get(ele, 'appLMS.role') === 'Applicant' ||
            !_.get(ele, 'appLMS.role')) &&
          ele.type === 'COMPANY',
      ),
      'cuid',
      '',
    );
    // dispatch(updateApplicantDetails(data, reset, values));
    data.applicants = data.guarantors;
    dispatch(updateCoApplicants(data, entityCuid, reset, values, true));
  };

  /**
   * function to add applicant
   */
  return (
    <Spin
      spinning={loading || updateLoading}
      tip={loading ? 'Loading...' : 'Submitting Data...'}
    >
      <div className="App">
        <Menu
          onClick={setApplicant}
          defaultSelectedKeys={[(selectedGuarantor || {}).id]}
          selectedKeys={[String((selectedGuarantor || {}).id)]}
          mode="horizontal"
        >
          {_.map(entity.guarantors, (applicant, index) => (
            <Menu.Item key={String(applicant.id)}>
              {`Guarantor ${index + 1}`}
            </Menu.Item>
          ))}
          <Menu.Item key="add">
            <Button type="link" onClick={() => dispatch(addGuarantor())}>
              + Guarantor
            </Button>
          </Menu.Item>
        </Menu>
        {selectedGuarantor ? (
          <>
            {requestType === 'PMA' && !isNoEntity && (
              <Row gutter={[16, 16]}>
                <Col
                  {...columnResponsiveLayout.colSingle}
                  style={{
                    fontSize: '18px',
                    fontWeight: 600,
                    padding: '12px 8px',
                  }}
                >
                  <Button
                    type={selectedGuarantor.userLinked ? 'default' : 'primary'}
                    htmlType="button"
                    className="personalDetailsRemoveButton"
                    style={{ marginLeft: '10px' }}
                    disabled={
                      activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'
                    }
                    onClick={() =>
                      dispatch(
                        unlinkGuarantor(
                          selectedGuarantor.id,
                          !selectedGuarantor.userLinked,
                        ),
                      )
                    }
                  >
                    {selectedGuarantor.userLinked
                      ? 'Unlink Guarantor'
                      : 'Link Guarantor'}
                  </Button>
                </Col>
              </Row>
            )}
            <form onSubmit={handleSubmit(onSubmit)}>
              <GuarantorPage
                userIndex={0}
                counter={1}
                control={control}
                errors={errors}
                // values={{}}
                clearError={clearError}
                setError={setError}
                entity={entity}
                values={values}
                applicant={selectedGuarantor}
                cuid={selectedGuarantor.cuid}
                isGuarantor={selectedGuarantor.cuid}
                reset={reset}
                setValue={setValue}
                getValues={getValues}
                triggerValidation={triggerValidation}
                allFieldsDisabled={!selectedGuarantor.userLinked}
                dispatch={dispatch}
                requestType={requestType}
                cibilScore={cibilScore}
                foir={foir}
                contactArr={contactArr}
                isNoEntity={isNoEntity}
                activity={activity}
                appId={appId}
                product={product}
                partner={_.get(applicationData, 'entity.partner')}
                defaultValues={defaultValues}
                setEducation={setEducation}
                education={education}
                APPLICANTTYPE={APPLICANTTYPE}
              />
            </form>
          </>
        ) : (
          <Empty />
        )}
        <div style={{ marginBottom: '2em', marginTop: '2em' }}>
          {contactArr &&
            Object.keys(contactArr) &&
            Object.keys(contactArr).length > 0 &&
            Object.keys(contactArr).map((contactType, index) => (
              <AddressSection
                cuid={selectedGuarantor.cuid}
                userId={_.get(values, `guarantors[${0}].id`, '')}
                contactType={contactType}
                index={index}
                contactArr={contactArr}
                isGuarantor={true}
                partner={_.get(applicantDetails, 'entity.partner')}
              />
            ))}
        </div>
      </div>
    </Spin>
  );
}

GuarantorDetails.propTypes = {
  applicantsData: PropTypes.array,
};

GuarantorDetails.defaultProps = {
  applicantsData: [],
};

export default GuarantorDetails;
