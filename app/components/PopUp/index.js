import React from 'react';
import { Modal, Input } from 'antd';
const { TextArea } = Input;

function PopUp({
  handleOk,
  handleCancel,
  visible,
  value,
  handleChange,
  textAreaLabel,
  title,
  isTabChange,
}) {
  return (
    <Modal
      title={title}
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
      okText={!isTabChange ? 'Ok' : 'Yes'}
      cancelText={!isTabChange ? 'Cancel' : 'No'}
    >
      {!isTabChange ? (
        <>
          <label htmlFor="textArea">{textAreaLabel}</label>
          <TextArea
            id="textArea"
            name="textArea"
            value={value}
            rows={3}
            onChange={handleChange}
          />{' '}
        </>
      ) : <label htmlFor="textArea">{textAreaLabel}</label>}
    </Modal>
  );
}

export default PopUp;
