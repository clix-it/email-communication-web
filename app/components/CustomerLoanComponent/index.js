/**
 *
 * CustomerLoanComponent
 *
 */

import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import {
  fetchAppDetails,
  fetchPennyDropDetails,
  loadDKYCDocs,
} from '../../containers/CustomerLoanDetails/actions';
import Tabs from '../Tabs';
import ShowBankDetails from '../ShowBankDetails';
import ShowDocuments from '../ShowDocuments';
import ShowDkycDetails from '../ShowDkycDetails';
import ShowCoApplicantDetails from '../ShowCoApplicantDetails';
import ShowApplicantDetails from '../ShowApplicantDetails';
import { Descriptions, Divider, Button, Drawer } from 'antd';
import Remarks from '../../containers/Remarks';
import moment from 'moment';

function CustomerLoanComponent({
  selectedLoan,
  dispatch,
  appDetails,
  pennyDropDetails,
  docsLoading,
  docsData,
  appV1Docs,
  dkycDocs,
  dkycDetails,
  kycDocsLoading,
  dkycDocsData,
  primaryUser,
  selectedCoApplicant,
}) {
  const [appid, setappid] = useState(false);
  const [visible, setVisible] = useState(false);

  const handleShowDrawer = () => {
    setVisible(true);
    setappid(selectedLoan.appid);
  };

  const historyAction = (
    <Button type="primary" onClick={() => handleShowDrawer()}>
      Remarks
    </Button>
  );
  const tabPanel = [
    {
      id: 'bankInfo',
      name: 'Bank Information',
      component: (
        <ShowBankDetails
          appDetails={appDetails}
          pennyDropDetails={pennyDropDetails}
        />
      ),
    },
    {
      id: 'appDocuments',
      name: 'App Documents',
      component: (
        <ShowDocuments
          appDetails={appDetails}
          docsLoading={docsLoading}
          docsData={docsData}
          appV1Docs={appV1Docs}
          dispatch={dispatch}
        />
      ),
    },
    {
      id: 'applicantDetails',
      name: 'Applicant Details',
      component: (
        <ShowApplicantDetails
          users={appDetails.users}
          dispatch={dispatch}
          mainApplicant={primaryUser}
        />
      ),
    },
  ];
  if (appDetails && appDetails.users && appDetails.users.length > 1) {
    tabPanel.push({
      id: 'coApplicantDetails',
      name: 'Co-Applicant Details',
      component: (
        <ShowCoApplicantDetails
          users={appDetails.users}
          dispatch={dispatch}
          mainApplicant={primaryUser}
          selectedCoApplicant={selectedCoApplicant}
        />
      ),
    });
  }
  if (dkycDetails.leadId) {
    tabPanel.push({
      id: 'dkycDetails',
      name: 'DKYC Details',
      component: (
        <ShowDkycDetails
          appDetails={appDetails}
          dkycDocs={dkycDocs}
          dkycDetails={dkycDetails}
          dispatch={dispatch}
          kycDocsLoading={kycDocsLoading}
          dkycDocsData={dkycDocsData}
        />
      ),
    });
  }
  useEffect(() => {
    if (selectedLoan && selectedLoan.appid) {
      dispatch(fetchAppDetails(selectedLoan.appid));
      dispatch(fetchPennyDropDetails(selectedLoan.appid));
    }
  }, [selectedLoan]);

  useEffect(() => {
    if (appDetails && appDetails.appId) {
      dispatch(loadDKYCDocs(appDetails.appId));
    }
  }, [appDetails]);

  const appliedLoanAmount = _.get(
    _.orderBy(
      _.filter(appDetails.loanOffers, {
        type: 'applied_amount',
      }),
      'id',
      'desc',
    ),

    '[0].loanAmount',
  );
  const finalLoanAmount = _.get(
    _.orderBy(
      _.filter(
        appDetails.loanOffers,
        loanOffer =>
          loanOffer.type === 'credit_amount' ||
          loanOffer.type === 'eligible_amount',
      ),
      'id',
      'desc',
    ),

    '[0].loanAmount',
  );
  const loanTenure = _.get(
    _.orderBy(
      _.filter(
        appDetails.loanOffers,
        loanOffer =>
          loanOffer.type === 'credit_amount' ||
          loanOffer.type === 'eligible_amount',
      ),
      'id',
      'desc',
    ),
    '[0].loanTenure',
    48,
  );
  const roi = _.get(
    _.orderBy(
      _.filter(
        appDetails.loanOffers,
        loanOffer =>
          loanOffer.type === 'credit_amount' ||
          loanOffer.type === 'eligible_amount',
      ),
      'id',
      'desc',
    ),
    '[0].roi',
  );
  const processingFee = _.get(
    _.orderBy(appDetails.loanCharges, 'id', 'desc'),
    '[0].amount',
    0,
  );
  const groupExposure = _.get(
    appDetails,
    'additionalData.data.extraDataField.groupExposure',
    0,
  );
  const fetchDate = accNumber => {
    if (accNumber) {
      var matches = accNumber.match(/(\d+)/);
      if (matches && matches[0]) {
        let date = `20${matches[0].substring(4, 6)}-${matches[0].substring(
          2,
          4,
        )}-${matches[0].substring(0, 2)}`;
        let formattedDate = moment(date).format('DD-MM-YYYY');
        return formattedDate != 'Invalid date' ? formattedDate : '';
      } else {
        return '';
      }
    } else {
      return '';
    }
  };
  return (
    <>
      <Descriptions
        title={`Loan Information`}
        column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        bordered
        layout="vertical"
        size="small"
      >
        <Descriptions.Item label="Product">
          {appDetails && appDetails.product}
        </Descriptions.Item>
        <Descriptions.Item label="Partner">
          {appDetails && appDetails.partner}
        </Descriptions.Item>
        <Descriptions.Item label="LAN">
          {`${selectedLoan && selectedLoan.loanAccountNumber} / ${
            selectedLoan.appid
          }`}
        </Descriptions.Item>
        {appliedLoanAmount && (
          <Descriptions.Item label="Applied Loan Amount">
            {appDetails && appliedLoanAmount}
          </Descriptions.Item>
        )}
        {finalLoanAmount ? (
          <Descriptions.Item label="Final Loan Amount">
            {appDetails && finalLoanAmount}
          </Descriptions.Item>
        ) : (
          <Descriptions.Item label="Final Loan Amount">
            {selectedLoan && selectedLoan.totalLoanAmount}
          </Descriptions.Item>
        )}
        <Descriptions.Item label="Number of Installments (in months)">
          {appDetails && loanTenure}
        </Descriptions.Item>
        {roi && (
          <Descriptions.Item label="Rate Of Interest(roi)">
            {appDetails && roi}
          </Descriptions.Item>
        )}
        <Descriptions.Item label="Processing Fee (in percentage)">
          {appDetails && processingFee}
        </Descriptions.Item>
        {appDetails && appDetails.loanPurpose && (
          <Descriptions.Item label="Purpose of Loan">
            {appDetails.loanPurpose}
          </Descriptions.Item>
        )}
        {groupExposure && (
          <Descriptions.Item label="Clix Existing Exposure">
            {appDetails && groupExposure}
          </Descriptions.Item>
        )}
        {selectedLoan && selectedLoan.amountPayable && (
          <Descriptions.Item label="Amount Payable">
            {selectedLoan.amountPayable}
          </Descriptions.Item>
        )}
        {selectedLoan && selectedLoan.emi && (
          <Descriptions.Item label="EMI">{selectedLoan.emi}</Descriptions.Item>
        )}
        {selectedLoan && selectedLoan.loanAmountDue && (
          <Descriptions.Item label="Due Amount">
            {selectedLoan.loanAmountDue}
          </Descriptions.Item>
        )}
        {selectedLoan &&
          selectedLoan.loan_start_date &&
          selectedLoan.lmsSystem != 'induslos' && (
            <Descriptions.Item label="Loan Start Date">
              {moment(selectedLoan.loan_start_date).format('DD-MM-YYYY')}
            </Descriptions.Item>
          )}
        {selectedLoan && selectedLoan.lmsSystem == 'induslos' && (
          <Descriptions.Item label="Loan Start Date">
            {fetchDate(selectedLoan.loanAccountNumber)}
          </Descriptions.Item>
        )}
      </Descriptions>
      <Divider />
      {appDetails && (
        <Tabs
          mode="top"
          ifSendBack={false}
          tabPanel={tabPanel}
          tabBarExtraContent={historyAction}
        />
      )}
      {appDetails && (
        <Drawer
          placement="right"
          visible={visible}
          onClose={() => setVisible(false)}
          key="remarks"
          width="80%"
        >
          <Remarks appId={appid} />
        </Drawer>
      )}
    </>
  );
}

CustomerLoanComponent.propTypes = {};

export default memo(CustomerLoanComponent);
