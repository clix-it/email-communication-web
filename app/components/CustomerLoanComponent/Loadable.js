/**
 *
 * Asynchronously loads the component for CustomerLoanComponent
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
