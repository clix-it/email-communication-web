import React, { memo, useState, useEffect } from 'react';
import { Radio, Form } from 'antd';
import _ from 'lodash';

function AddressChanged({ entity, values, cuid }) {
  const flag = _.find(
    _.get(entity, 'additionalData.data.addressChangesFlages'),
    {
      cuid,
    },
  );

  const [addressFlag, setAddressFlag] =
    flag && flag['addresschanged'] ? useState('Yes') : useState('No');

  useEffect(() => {
    if(flag && flag['addresschanged']){
      setAddressFlag('Yes')
    } else {
      setAddressFlag('No')
    }
    return () => {
      setAddressFlag('No');
    }
  }, [cuid])

  return (
    <Form.Item label="Is Address Changed?">
      <Radio.Group value={addressFlag} disabled>
        <Radio value="Yes">Yes</Radio>
        <Radio value="No">No</Radio>
      </Radio.Group>
    </Form.Item>
  );
}

export default memo(AddressChanged);
