/**
 *
 * KycTabComponent
 *
 */

import React, { memo, useEffect, useState } from 'react';
import Tabs from '../Tabs';
import LanDetails from '../../containers/LanDetails';
import DownloadEmiSchedule from '../../containers/DownloadEmiSchedule';
import DownloadSoa from '../../containers/DownloadSoa';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function LanTabComponent({
  lan,
  lanDetails,
  dispatch,
  lms,
  product,
  partner,
  applicantDetails,
}) {
  const [LanTabPanel, setLanTabPanel] = useState([
    {
      id: 'lan',
      name: 'Lan Details',
      component: (
        <LanDetails
          lan={lan}
          lanDetails={lanDetails}
          dispatch={dispatch}
          lms={lms}
          product={product}
          partner={partner}
          applicantDetails={applicantDetails}
        />
      ),
    },
    {
      id: 'emiSchedule',
      name: 'EMI Schedule',
      component: (
        <DownloadEmiSchedule
          loan={{
            product,
            partner,
            loanAccountNumber: lan,
          }}
        />
      ),
    },
    {
      id: 'soa',
      name: 'Account Statement',
      component: (
        <DownloadSoa
          loan={{
            product,
            partner,
            loanAccountNumber: lan,
            ...applicantDetails.entity,
          }}
        />
      ),
    },
  ]);

  return <Tabs mode="top" ifSendBack={false} tabPanel={LanTabPanel} />;
}

LanTabComponent.propTypes = {};

export default memo(LanTabComponent);
