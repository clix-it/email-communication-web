import React, { useEffect, useState, memo } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import { Table, Button, Space, Input, DatePicker, Spin } from 'antd';
import { FilterOutlined } from '@ant-design/icons';
import moment from 'moment';

import { loadLeads, setCurrentApp } from '../../containers/LeadsList/actions';

const { RangePicker } = DatePicker;

function LeadsRecord({ leads, user, requestType, dispatch, isLoading }) {
  const defaultSearchValues = {
    applicationId: '',
    panNumber: '',
    customerName: '',
    updatedAt: [],
  };

  const [dataSource, setDataSource] = useState([]);
  const [sortedInfo, setSortedInfo] = useState(null);
  const [searchValues, setSearchValues] = useState(defaultSearchValues);

  useEffect(() => {
    dispatch(loadLeads(user, requestType));
  }, [requestType]);

  useEffect(() => {
    setDataSource(leads);
  }, [leads]);

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: () => (
      <div style={{ padding: 8 }}>
        {dataIndex === 'updatedAt' ? (
          <RangePicker
            value={searchValues.updatedAt}
            onChange={e => handleSearchValues(e || [], dataIndex)}
            onPressEnter={handleSearch}
            onBlur={handleSearch}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
        ) : (
          <Input
            placeholder={`Search ${dataIndex}`}
            value={searchValues[dataIndex]}
            onPressEnter={handleSearch}
            onBlur={handleSearch}
            onChange={e => handleSearchValues(e.target.value, dataIndex)}
          />
        )}
      </div>
    ),
    filterIcon: filtered => (
      <FilterOutlined
        style={{
          color: filtered ? '#1890ff' : undefined,
          paddingRight: '100px',
        }}
      />
    ),
  });

  const columns = [
    {
      title: 'Product',
      dataIndex: 'product',
      key: 'product',
      align: 'center',
      ellipsis: true,
    },
    {
      title: 'Partner',
      dataIndex: 'partner',
      key: 'partner',
      align: 'center',
    },
    {
      title: 'Application ID',
      dataIndex: 'applicationId',
      key: 'applicationId',
      align: 'center',
      sorter: (a, b) =>
        a.applicationId && b.applicationId && a.applicationId - b.applicationId,
      sortOrder:
        sortedInfo &&
        sortedInfo.columnKey === 'applicationId' &&
        sortedInfo.order,
      ellipsis: true,
      // ...getColumnSearchProps('applicationId'),
      render: (text, record) =>
        record.activityRequestId ? (
          <Link
            onClick={() => dispatch(setCurrentApp(record))}
            to={`${location.pathname}/${text}`}
          >
            {text}
          </Link>
        ) : (
          text
        ),
    },
    {
      title: 'Pan Number',
      dataIndex: 'panNumber',
      key: 'panNumber',
      align: 'center',
      sorter: (a, b) =>
        a.panNumber &&
        a.panNumber.localeCompare(b.panNumber, 'en', { numeric: true }),
      sortOrder:
        sortedInfo && sortedInfo.columnKey === 'panNumber' && sortedInfo.order,
      ellipsis: true,
      ...getColumnSearchProps('panNumber'),
    },
    {
      title: 'Customer Name',
      dataIndex: 'customerName',
      key: 'customerName',
      align: 'center',
      sorter: (a, b) =>
        a.customerName
          ? a.customerName.localeCompare(b.customerName, 'en', {
              numeric: true,
          })
          : b.customerName &&
            b.customerName.localeCompare(a.customerName, 'en', {
              numeric: true,
            }),
      sortOrder:
        sortedInfo &&
        sortedInfo.columnKey === 'customerName' &&
        sortedInfo.order,
      ellipsis: true,
      ...getColumnSearchProps('customerName'),
    },
    {
      title: 'Last Submitted On',
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      align: 'center',
      sorter: (a, b) => a.updatedAt.localeCompare(b.updatedAt),
      sortOrder:
        sortedInfo && sortedInfo.columnKey === 'updatedAt' && sortedInfo.order,
      ellipsis: true,
      ...getColumnSearchProps('updatedAt'),
      render: updatedAt => <div>{moment(updatedAt).format('DD-MM-YYYY')}</div>,
    },
  ];

  const clearSorter = () => {
    setSortedInfo(null);
  };

  const clearAllFilters = () => {
    setSearchValues(defaultSearchValues);
    dispatch(loadLeads(user, requestType));
  };

  const handleChange = (pagination, filters, sorter) => {
    setSortedInfo(sorter);
  };

  const handleSearch = () => {
    dispatch(
      loadLeads(
        {
          appId: searchValues.applicationId,
          panNumber: searchValues.panNumber,
          customerName: searchValues.customerName,
          startDate: searchValues.updatedAt[0],
          endDate: searchValues.updatedAt[1],
        },
        requestType,
      ),
    );
  };

  const handleSearchValues = (value, dataIndex) => {
    const newSearchValues = { ...searchValues };
    newSearchValues[dataIndex] = value;
    setSearchValues(newSearchValues);
    // console.log('searchValues', newSearchValues);
  };

  const locale = {
    emptyText: 'No Data Found',
  };

  return (
    <Spin spinning={isLoading} tip="Loading...">
      <Space style={{ marginBottom: 16 }}>
        <Button onClick={clearSorter}>Clear Sorters</Button>
        <Button onClick={clearAllFilters}>Clear All Filters</Button>
      </Space>
      <Table
        locale={locale}
        dataSource={dataSource || []}
        pagination={{ defaultPageSize: 10, showSizeChanger: true }}
        columns={columns}
        onChange={handleChange}
      />
    </Spin>
  );
}

LeadsRecord.propTypes = {
  user: PropTypes.object,
  leads: PropTypes.arrayOf(PropTypes.object),
  requestType: PropTypes.string,
  isLoading: PropTypes.bool,
  dispatch: PropTypes.func.isRequired,
};

LeadsRecord.defaultProps = {
  user: {},
  leads: [],
  isLoading: false,
  requestType: '',
};

export default memo(LeadsRecord);
