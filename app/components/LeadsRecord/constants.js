export const fields = {
  startDate: {
    name: 'startDate',
    id: 'startDate',
    type: 'datepicker',
    label: 'Start Date',
    placeholder: '',
  },
  endDate: {
    name: 'endDate',
    id: 'endDate',
    type: 'datepicker',
    label: 'End Date',
    placeholder: '',
  },
  // applicationNumber: {
  //   name: 'applicationNumber',
  //   id: 'applicationNumber',
  //   type: 'string',
  //   label: 'Application Number',
  //   placeholder: 'Application Number',
  // },
  panNumber: {
    name: 'panNumber',
    id: 'panNumber',
    type: 'string',
    label: 'Pan',
    placeholder: 'Pan',
  },
  customerName: {
    name: 'customerName',
    id: 'customerName',
    type: 'string',
    label: 'Customer Name',
    placeholder: 'Customer Name',
  },
};
