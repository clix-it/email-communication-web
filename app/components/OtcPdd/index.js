import React, { memo, useEffect, useState } from 'react';
import EditableTable from 'components/EditableTable';
import _ from 'lodash';
import moment from 'moment';
import { isEmpty } from '../../utils/helpers';
// import { DESIGNATION } from '../../utils/constants';

function OtcPdd({
  values,
  userIndex,
  setValue,
  initDataSource,
  disabled,
  masterConvenantTypeData,
}) {
  console.log('initDataSource', initDataSource);
  const initColumns = [
    {
      title: 'Covenant Type',
      dataIndex: 'covenantType',
      editable: true,
      width: 220,
      type: 'select',
      dropdown: masterConvenantTypeData,
      required: false,
      // render: value => {
      //   let index = -1;
      //   if (_.get(masterConvenantTypeData, '[0].code') && value) {
      //     index = _.findIndex(masterConvenantTypeData, e => value === e.code);
      //   }
      //   return (
      //     <div>
      //       {masterConvenantTypeData.length > 0 &&
      //       _.get(masterConvenantTypeData, '[0].code')
      //         ? _.get(masterConvenantTypeData, '[index].code')
      //         : value}
      //     </div>
      //   );
      // },
    },
    {
      title: 'Allow Waiver',
      dataIndex: 'allowWaiver',
      editable: true,
      width: 220,
      type: 'select',
      dropdown: ['true', 'false'],
      required: false,
      render: (data, record) =>
        data && String(data).includes('Enter') ? (
          <div>{data}</div>
        ) : (
          <div>
            {data === true ? 'true' : 'false'}
            &nbsp;&nbsp;
          </div>
        ),
    },
    {
      title: 'Allow Postpone',
      dataIndex: 'allowPostpone',
      editable: true,
      width: 220,
      type: 'select',
      dropdown: ['true', 'false'],
      required: false,
      render: (data, record) =>
        data && String(data).includes('Enter') ? (
          <div>{data}</div>
        ) : (
          <div>
            {data === 'true' ? 'true' : 'false'}
            &nbsp;&nbsp;
          </div>
        ),
    },
    {
      title: 'Postpone Days(in number)',
      dataIndex: 'postponeDays',
      editable: true,
      width: 220,
      required: false,
    },
    {
      title: 'Allow Otc',
      dataIndex: 'allowOtc',
      editable: true,
      width: 220,
      type: 'select',
      dropdown: ['true', 'false'],
      required: false,
      render: (data, record) =>
        data && String(data).includes('Enter') ? (
          <div>{data}</div>
        ) : (
          <div>
            {data === 'true' ? 'true' : 'false'}
            &nbsp;&nbsp;
          </div>
        ),
    },
    {
      title: 'Receivable Date',
      dataIndex: 'receivableDate',
      type: 'datepicker',
      editable: true,
      width: 250,
      required: false,
      render: (receivableDate, record) => (
        <div>
          {receivableDate ? moment(receivableDate).format('DD-MM-YYYY') : ''}
          &nbsp;&nbsp;
        </div>
      ),
    },
  ];

  const onChangeValues = data => {
    const dataToSave = [];
    if (data.length > 0) {
      data.forEach(item => {
        const updatedItem = { ...item };
        Object.keys(item).forEach(element => {
          if (item[element] && item[element].toString().includes('Enter')) {
            updatedItem[element] = '';
          }
          if (
            element === 'receivableDate' &&
            item[element] &&
            !item[element].toString().includes('Enter')
          ) {
            updatedItem[element] = moment(item[element]).format(
              'YYYY-MM-DDTHH:mm:ss',
            );
          }
        });
        if (!isEmpty(_.omit(updatedItem, ['key']))) {
          dataToSave.push(updatedItem);
        }
      });
      console.log('appCovenants', dataToSave);
      setValue('appCovenants', dataToSave);
    }
  };

  return (
    <EditableTable
      initDataSource={initDataSource}
      initColumns={initColumns}
      onChangeValues={onChangeValues}
      columnsGiven
      notShowDeleteBtn
      addTitle="Add OTC/PDD"
      disabled={disabled}
    />
  );
}

export default memo(OtcPdd);
