import React, { useEffect, useState, memo } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import _ from 'lodash';
import {
  Table,
  Button,
  Space,
  Input,
  DatePicker,
  Spin,
  Drawer,
  Tag,
  Upload,
  message,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons'
import Remarks from 'containers/Remarks';
import moment from 'moment';
import styled from 'styled-components';

import { useHistory } from 'react-router-dom';

import {
  excelUpload,
  loadEmailsSearch,
  loadEmails,
} from '../../containers/EmailPortal/actions';
import { STATUS_WISE_COLOR } from '../../utils/constants';

const { RangePicker } = DatePicker;

function EmailListingPage({
  applications,
  user,
  requestType,
  dispatch,
  isLoading,
  activityType,
  emailCommunications,
  emailPortal,
}) {
  const defaultSearchValues = {
    emailId: '',
    category: '',
  };

  const [dataSource, setDataSource] = useState([]);
  const [sortedInfo, setSortedInfo] = useState(null);
  const [searchValues, setSearchValues] = useState(defaultSearchValues);
  const [response, setResponse] = useState(false);
  const [excelResponse, setExcelResponse] = useState(false)


  useEffect(() => {
    if (emailPortal.response && response) {
      message.success(emailPortal.response);
      dispatch(loadEmails());
      setResponse(false);
    }

    if (emailPortal.error && response) {
      message.error(emailPortal.error);
    }
  }, [emailPortal.response, emailPortal.error]);

  useEffect(() => {
    if (emailPortal.response && excelResponse) {
      message.success(emailPortal.response);
      dispatch(loadEmails());
      setResponse(false);
    }

    if (emailPortal.error && excelResponse) {
      message.error(creditPortal.error);
    }
  }, [emailPortal.response, emailPortal.error]);


  useEffect(() => {
    setDataSource([]);
    setSearchValues(defaultSearchValues);
    dispatch(loadEmails(user, requestType, activityType));
  }, [requestType, activityType]);

  useEffect(() => {
    setDataSource(emailCommunications);
  }, [emailCommunications]);

  const [visible, setVisible] = useState(false);
  const [appid, setappid] = useState(false);

  const getColumnSearchProps = dataIndex => ({
    filterDropdown: () => (
      <div style={{ padding: 8 }}>
        {dataIndex === 'updatedAt' ? (
          <RangePicker
            value={searchValues.updatedAt}
            onChange={e => handleSearchValues(e || [], dataIndex)}
            onPressEnter={handleSearch}
            onBlur={handleSearch}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
        ) : (
          <Input
            placeholder={`Search ${dataIndex}`}
            value={searchValues[dataIndex]}
            onPressEnter={handleSearch}
            onBlur={handleSearch}
            onChange={e => handleSearchValues(e.target.value, dataIndex)}
          />
        )}
      </div>
    ),
  });

  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
      align: 'center',
      ellipsis: true,
      width: 80,
      render: (appIdEmail) => (
        <Link
          to={`${location.pathname}/${appIdEmail}`}
        >
          {appIdEmail}
        </Link>
      ),
    },
    {
      title: 'Email ID',
      dataIndex: 'emailId',
      key: 'emailId',
      align: 'center',
      ellipsis: true,
      width: 80,
      ...getColumnSearchProps('emailId'),
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
      align: 'center',
      ellipsis: true,
      width: 80,
      ...getColumnSearchProps('category'),
    },
    {
      title: 'Default Mail ID',
      dataIndex: 'defaultMailId',
      key: 'defaultMailId',
      align: 'center',
      ellipsis: true,
      width: 80,
    },
    {
      title: 'Remarks',
      dataIndex: 'remarks',
      key: 'remarks',
      align: 'center',
      ellipsis: true,
      width: 80,
    },
    {
      title: 'Active',
      dataIndex: 'active',
      key: 'active',
      align: 'center',
      ellipsis: true,
      width: 80,
    },
  ];

  const getFilters = () =>
    Object.keys(searchValues).map(sValue => {
      if (!searchValues[sValue]) return <div />;
      if (sValue === 'updatedAt' && searchValues[sValue].length > 0) {
        return (
          <Tag
            closable
            className="tagStyle"
            onClose={() => removeFilter(sValue)}
          >
            <Caption>Last Submitted On:</Caption>
            {moment(searchValues[sValue][0]).format('DD-MM-YYYY')} - {'  '}
            {moment(searchValues[sValue][1]).format('DD-MM-YYYY')}
          </Tag>
        );
      }
      if (sValue !== 'updatedAt') {
        return (
          <Tag
            closable
            className="tagStyle"
            onClose={() => removeFilter(sValue)}
          >
            <Caption>
              {_.startCase(_.lowerCase(_.replace(sValue, /_/g, ' ')))}:
            </Caption>
            {searchValues[sValue]}
          </Tag>
        );
      }
      return null;
    });

  const removeFilter = filterName => {
    const newSearchValues = { ...searchValues };
    newSearchValues[filterName] = '';
    setSearchValues(newSearchValues);
    dispatch(
      loadEmails(
        {
          appId: newSearchValues.applicationId,
          panNumber: newSearchValues.panNumber,
          customerName: newSearchValues.customerName,
          startDate: newSearchValues.updatedAt
            ? newSearchValues.updatedAt[0]
            : '',
          endDate: newSearchValues.updatedAt
            ? newSearchValues.updatedAt[1]
            : '',
          product: newSearchValues.product,
          partner: newSearchValues.partner,
        },
        requestType,
        activityType,
      ),
    );
  };

  const clearSorter = () => {
    setSortedInfo(null);
  };

  const clearAllFilters = () => {
    setSearchValues(defaultSearchValues);
    dispatch(loadEmails(user, requestType, activityType));
  };

  const history = useHistory();
  const addEmail = () => {
    history.push(`/addEmail`)
  };

  //ONchange

  const handleFileUpload =  async (file) => {
     const fileUpload = file.file;
     const base64 =  await convertBase64(fileUpload);
   //  setBaseImage(base64);
    const splitBase64 = base64.split('base64,')
    const splitOutput = splitBase64[1]
    dispatch(excelUpload({base: splitOutput}));
    setExcelResponse(true);
  };

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };


  const handleChange = (pagination, filters, sorter) => {
    setSortedInfo(sorter);
  };


  const handleSearch = () => {
    dispatch(
      loadEmailsSearch(
        {
          emailId: searchValues.emailId,
          category: searchValues.category,
        },
      ),
    );
  };

  const handleSearchValues = (value, dataIndex) => {
    const newSearchValues = { ...searchValues };
    newSearchValues[dataIndex] = value;
    setSearchValues(newSearchValues);
    // console.log('searchValues', newSearchValues);
  };

  const locale = {
    emptyText: 'No Data Found',
    triggerAsc: 'Click to sort by ascending order',
    triggerDesc: 'Click to sort by descending  order',
  };

  const ifFilterExists = () => {
    let ifExists = true;
    Object.keys(searchValues).forEach(key => {
      if (
        (key === 'updatedAt' &&
          searchValues[key] &&
          searchValues[key].length > 0) ||
        (key !== 'updatedAt' && searchValues[key])
      )
        ifExists = false;
    });
    return ifExists;
  };

  return (
    <Spin spinning={isLoading} tip="Loading...">
      <Space style={{ marginBottom: 16 }}>
        {getFilters()}
        <Button onClick={clearSorter} disabled={!sortedInfo}>
          Clear Sorters
        </Button>
        <Button onClick={clearAllFilters} disabled={ifFilterExists()}>
          Clear All Filters
        </Button>
        <Button onClick={addEmail}>
          Add Email
        </Button>
        <Upload
        name="file"
         accept=".xlsx"
          // accept=".jpg, .pdf, .png"
         showUploadList={false}
         customRequest={handleFileUpload}
        // onChange={e => FileonChange(e)}
        ><Button>
        <UploadOutlined /> Upload Excel
      </Button></Upload>
      </Space>
      <Table
        locale={locale}
        dataSource={dataSource || []}
        pagination={{ defaultPageSize: 10, showSizeChanger: true }}
        columns={columns}
        onChange={handleChange}
      />
      <Drawer
        placement="right"
        visible={visible}
        onClose={() => setVisible(false)}
        key="remarks"
        width="80%"
      >
        <Remarks appId={appid} />
      </Drawer>
    </Spin>
  );
}

EmailListingPage.propTypes = {
  user: PropTypes.object,
  applications: PropTypes.arrayOf(PropTypes.object),
  requestType: PropTypes.string,
  isLoading: PropTypes.bool,
  dispatch: PropTypes.func.isRequired,
  activityType: PropTypes.string,
};

EmailListingPage.defaultProps = {
  user: {},
  applications: [],
  isLoading: false,
  requestType: '',
  activityType: 'CREDIT_REVIEW',
};

const Caption = styled.span`
  margin-right: 7px;
  font-weight: bold;
`;

export default memo(EmailListingPage);
