import React from 'react';
import { Row, Col } from 'antd';
import _ from 'lodash';
import InputField from 'components/InputField';
import {
  REFERENCE_RELATIONSHIP_FAMILY,
  REFERENCE_RELATIONSHIP_ANY,
  columnResponsiveLayout,
} from '../../utils/constants';

function References({ control, errors, appReferences }) {
  console.log('appReferences', appReferences);
  return (
    <>
      {appReferences.map((person, index) => (
        <Row gutter={[16, 16]}>
          <Col xs={0}>
            <InputField
              id={`appReferences[${index}].id`}
              control={control}
              errors={errors}
              defaultValue={person.id || ''}
              name={`appReferences[${index}].id`}
              type="hidden"
            />
          </Col>
          <Col {...columnResponsiveLayout.colOneThird}>
            <InputField
              id={`appReferences[${index}].firstName`}
              control={control}
              name={`appReferences[${index}].firstName`}
              defaultValue={person.firstName || ''}
              type="string"
              rules={{
                required: {
                  value: false,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder={`Reference ${index + 1}  Name`}
              labelHtml={`Reference ${index + 1}  Name`}
            />
          </Col>
          <Col {...columnResponsiveLayout.colOneThird}>
            <InputField
              id={`appReferences[${index}].phoneNumber`}
              control={control}
              name={`appReferences[${index}].phoneNumber`}
              type="string"
              defaultValue={person.phoneNumber || ''}
              rules={{
                required: {
                  value: false,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder={`Reference ${index + 1} Mobile Number`}
              labelHtml={`Reference ${index + 1} Mobile Number`}
            />
          </Col>
          <Col {...columnResponsiveLayout.colOneThird}>
            <InputField
              id={`appReferences[${index}].relationship`}
              control={control}
              name={`appReferences[${index}].relationship`}
              type="select"
              defaultValue={person.relationship || ''}
              rules={{
                required: {
                  value: false,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder={`Reference ${index + 1} Relationship ${
                index === 0 ? '(Family)' : 'Any'
              }`}
              labelHtml={`Reference ${index + 1} Relationship`}
              options={
                index === 0
                  ? REFERENCE_RELATIONSHIP_FAMILY
                  : REFERENCE_RELATIONSHIP_ANY
              }
              disabled
            />
          </Col>
        </Row>
      ))}
    </>
  );
}

export default References;
