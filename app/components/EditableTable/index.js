/**
 *
 * Editable Table
 *
 */

import React, { memo, useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, Card, Select } from 'antd';

import _ from 'lodash';

const { Option } = Select;

const EditableContext = React.createContext();

const EditableRow = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  type,
  dropdown,
  required = true,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    if (record[dataIndex] && !record[dataIndex].toLowerCase().includes('enter'))
      form.setFieldsValue({
        [dataIndex]: record[dataIndex],
      });
  };

  const getInputOnType = () => {
    switch (type) {
      case 'select':
        return (
          <Select ref={inputRef} onPressEnter={save} onBlur={save}>
            {dropdown &&
              dropdown.length > 0 &&
              dropdown.map(item =>
                // console.log(_.get(item, 'code', ''), _.get(item, 'value', ''));
                _.get(item, 'code', '') && _.get(item, 'value', '') ? (
                  <Option value={item.code}>
                    {' '}
                    {`${item.code}-${item.value}`}
                  </Option>
                ) : (
                  <Option value={item}> {item}</Option>
                ),
              )}
          </Select>
        );
      case 'datepicker':
        return (
          <Input ref={inputRef} onPressEnter={save} onBlur={save} type="date" />
        );
      default:
        return <Input ref={inputRef} onPressEnter={save} onBlur={save} />;
    }
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  let childNode = children;
  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required,
            message: `${title} is required.`,
          },
        ]}
      >
        {getInputOnType()}
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}&nbsp;
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

const EditableTable = ({
  disabled,
  initDataSource,
  columnsGiven,
  initColumns,
  onChangeValues,
  notShowDeleteBtn,
  addTitle,
}) => {
  const [dynamicCols, setDynamicCols] = useState([]);

  const [dataSource, setDataSource] = useState(initDataSource);
  const [count, setCount] = useState(0);

  useEffect(() => {
    const array = initDataSource.map((v, index) => ({ ...v, key: index }));
    setDataSource(array);
    setCount(array.length);
  }, [initDataSource]);

  useEffect(() => {
    let newColumns = [];
    if (!columnsGiven) {
      if (initDataSource.length === 0) return;
      Object.keys(initDataSource[0]).forEach(col => {
        if (col === 'key') return;
        newColumns.push({
          title: _.upperCase(col),
          dataIndex: col,
          col,
        });
      });
    } else {
      newColumns = [...initColumns];
      if (!notShowDeleteBtn) {
        newColumns.push({
          title: 'Operation',
          dataIndex: 'operation',
          render: (text, record) =>
            dataSource.length >= 1 && (
              <Popconfirm
                title="Sure to delete?"
                onConfirm={() => handleDelete(record.key)}
              >
                <a>Delete</a>
              </Popconfirm>
            ),
        });
      }
    }
    setDynamicCols(newColumns);
  }, [dataSource]);

  const handleDelete = key => {
    const data = [...dataSource];
    setDataSource(data.filter(item => item.key !== key));
    onChange(data.filter(item => item.key !== key));
  };

  const handleAdd = () => {
    const newData = {};
    dynamicCols.forEach(col => {
      if (col.type === 'datepicker') {
        newData[col.dataIndex] = new Date();
      } else newData[col.dataIndex] = `Enter ${col.title}`;
      //   newData[col.dataIndex] = 'Info';
    });
    newData.key = count + 1;
    setDataSource([...dataSource, newData]);
    setCount(count + 1);
  };

  const handleSave = row => {
    const newData = [...dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    setDataSource(newData);
    onChange(newData);
  };

  // const { dataSource } = this.state;
  // this.props.tableValues = dataSource;

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };
  const columns = dynamicCols.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        type: col.type,
        dropdown: col.dropdown,
        required: col.required,
        handleSave,
      }),
    };
  });

  const onChange = data => {
    onChangeValues(data);
  };

  return (
    <div
      className={
        dataSource && dataSource.length > 0 ? 'width-limit' : 'no-border'
      }
    >
      <Card>
        <Button
          disabled={disabled}
          onClick={handleAdd}
          type="primary"
          style={{
            marginBottom: 16,
            float: 'right',
            zIndex: 9999,
          }}
        >
          {addTitle}
        </Button>
        {dataSource && dataSource.length > 0 && (
          <Table
            // onChange={() => onChange(dataSource)}
            components={components}
            rowClassName={() => 'editable-row'}
            bordered
            dataSource={dataSource}
            columns={columns}
            scroll={{ x: 'max-content' }}
          />
        )}
      </Card>
    </div>
  );
};

EditableTable.propTypes = {};

export default memo(EditableTable);
