import React from 'react';
import { FormattedMessage } from 'react-intl';
import { LogoutOutlined } from '@ant-design/icons';
import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { PageHeader, Button, Radio } from 'antd';
import A from './A';
import Img from './Img';
import NavBar from './NavBar';
import HeaderLink from './HeaderLink';
import Banner from './banner.jpg';
import messages from './messages';
import appLogo from '../../images/clixCapitalLogo.png';
// import { Button } from '@material-ui/core';
import { logUserOut, deRegisterUser } from '../../containers/App/actions';

import './style.css';

function Header({ isAuthorized, dispatch }) {
  function handleLogout() {
    if (sessionStorage.getItem('loginFrom') === 'COMMON_PORTAL') {
      const parentURL = sessionStorage.getItem('parentURL');
        sessionStorage.clear();
        window.parent.location.href = `${parentURL}?action=logout`;
    } else {
      dispatch(deRegisterUser());
    }
    dispatch(logUserOut());
  }

  return (
    <div className="site-page-header-ghost-wrapper">
      {isAuthorized ? (
        <PageHeader
          ghost={false}
          title={<img src={appLogo} alt="Clix logo" />}
          extra={[
            <Button
              type="primary"
              onClick={handleLogout}
              icon={<LogoutOutlined />}
            >
              Logout
            </Button>,
            // <Button key="1" onClick={handleLogout} type="primary">
            //   Logout
            // </Button>,
          ]}
        />
      ) : (
        <PageHeader
          ghost={false}
          title={<img src={appLogo} alt="Clix logo" />}
        />
      )}
    </div>

    // <header className="header">
    //   <Navbar
    //     collapseOnSelect
    //     className="d-flex justify-content-between"
    //     style={{ padding: 0 }}
    //   >
    //     <Navbar.Brand>
    //       <img src={appLogo} alt="Clix logo" />
    //     </Navbar.Brand>
    //     {isAuthorized ? (
    //       <div>
    //         <Button color="primary" variant="outlined" onClick={handleLogout}>
    //           Logout
    //         </Button>
    //       </div>
    //     ) : null}
    //   </Navbar>
    // </header>
  );
}
export default Header;
