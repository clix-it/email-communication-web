/**
 *
 * EntityForm
 *
 */

import _ from 'lodash';
import React, { useEffect, useState } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import {
  Row,
  Col,
  Divider,
  Button,
  Form,
  Select,
  message,
  Typography,
} from 'antd';
import moment from 'moment';
import InputField from 'components/InputField';
import { useForm, Controller } from 'react-hook-form';
import ErrorMessage from 'components/ErrorMessage';
import StyledTitle from 'components/StyledTitle';
import { formatDate } from 'utils/helpers';
import HealthQuestions from '../../containers/HealthQuestions';
import NomineeDetails from '../NomineeDetails';
import { numberPattern } from '../../containers/App/constants';

const { Text } = Typography;

const colSingle = { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 };
const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: { span: 10, offset: 2, pull: 2 },
};

const responsiveColumnsPremium = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
};

const { Option } = Select;

function InsuranceForm({
  selectedInsurance,
  applicants,
  submitInsurance,
  generateInsurance,
  docsData = [],
  applicantDetails,
  corpInsurances,
  insurancePlan,
  calculatePremium,
  dispatch,
  allAnswers,
  insuranceForms,
  premiumData,
}) {
  // debugger;
  const [applicantsArray, setApplicantsArray] = useState([]);

  const eligibleLoanTenure = _.get(
    _.orderBy(
      _.filter(
        _.get(applicantDetails, 'entity.loanOffers'),
        loanOffer =>
          loanOffer.type === 'credit_amount' ||
          loanOffer.type === 'eligible_amount',
      ),
      'id',
      'desc',
    ),
    '[0].loanTenure',
    24,
  );
  const eligibleLoanAmount = _.get(
    _.orderBy(
      _.filter(
        _.get(applicantDetails, 'entity.loanOffers'),
        loanOffer =>
          loanOffer.type === 'credit_amount' ||
          loanOffer.type === 'eligible_amount',
      ),
      'id',
      'desc',
    ),
    '[0].loanAmount',
    0,
  );

  useEffect(() => {
    let applicantsForIns = applicants;
    if (String(selectedInsurance.id).includes('NEW'))
      applicantsForIns = applicants.filter(
        item =>
          !corpInsurances.some(resultItem => resultItem.cuid === item.cuid),
      );
    setApplicantsArray(applicantsForIns);
  }, [applicants, corpInsurances, selectedInsurance]);

  const defaultValues = {
    appInsurances: [
      {
        ...selectedInsurance,
        insuranceForApplicant: selectedInsurance
          ? _.get(selectedInsurance, 'cuid')
          : '',
        nominee1_DateOfBirth: formatDate(
          selectedInsurance.nominee1_DateOfBirth,
        ),
        nominee2_DateOfBirth: formatDate(
          selectedInsurance.nominee2_DateOfBirth,
        ),
        weight: _.get(selectedInsurance, 'cuid')
          ? _.get(
              _.find(applicants, { cuid: _.get(selectedInsurance, 'cuid') }),
              'userDetails.weight',
              '',
            )
          : '',
        height: _.get(selectedInsurance, 'cuid')
          ? _.get(
              _.find(applicants, { cuid: _.get(selectedInsurance, 'cuid') }),
              'userDetails.height',
              '',
            )
          : '',
        coverageTerm: Math.ceil(eligibleLoanTenure / 12) * 12,
        sumAssured: _.get(selectedInsurance, 'sumAssured', ''),
      },
    ],
  };

  const { handleSubmit, errors, control, reset, watch } = useForm({
    mode: 'onChange',
    defaultValues,
  });

  const values = watch();

  const onSubmit = data => {
    const {
      selectedApplicant: { preferredPhone = '', contactibilities = [] } = {},
    } = applicantDetails;
    const nominee1Mobile = _.get(data, 'appInsurances[0].nominee1_mobile', '');
    const nominee2Mobile = _.get(data, 'appInsurances[0].nominee2_mobile', '');
    let checker = contactibilities.some(
      item =>
        item.phoneNumber &&
        (item.phoneNumber === nominee1Mobile ||
          item.phoneNumber === nominee2Mobile),
    );
    checker =
      preferredPhone === nominee1Mobile || preferredPhone === nominee2Mobile;
    if (checker) {
      message.info(
        "Nominee's and Appointee phone number cannot be same as Appicant's!",
      );
      return;
    }
    console.log('data to save in insurance', data);
    const ageDiff = moment().diff(
      moment(data.nominee1_DateOfBirth, 'DD-MM-YYYY'),
      'years',
    );
    if (
      ageDiff < 18 &&
      !(
        data.nominee2_firstName ||
        data.nominee2_nomineeRelation ||
        data.nominee2_DateOfBirth ||
        data.nominee2_Gender ||
        data.nominee2_mobile
      )
    ) {
      message.info('Please fill Appointee Details as Nominee is a Minor!');
      return;
    }
    const insurance = _.get(data, 'appInsurances[0]');
    if (_.get(insurance, 'insuranceForApplicant')) {
      delete insurance.insuranceForApplicant;
    }
    if (insurance) {
      // if (String(insurance.id).includes('NEW'))
      delete insurance.id;
      if (
        _.get(selectedInsurance, 'premiumAmount', '') !==
        _.get(premiumData, 'annualPremiumWithTax', '')
      ) {
        insurance.premiumAmount = _.get(
          premiumData,
          'annualPremiumWithTax',
          '',
        );
      }
      submitInsurance(insurance);
    } else message.info('Fill the correct data!');
  };

  useEffect(() => {
    reset(defaultValues);
  }, [selectedInsurance]);
  const handleGenerateInsurance = () => {
    dispatch(generateInsurance());
  };

  const isInsuranceValid = () => {
    const answersPerCuid = allAnswers.find(
      ans => ans.cuid === _.get(selectedInsurance, 'cuid'),
    );
    if (
      answersPerCuid &&
      Object.keys(answersPerCuid).length > 0 &&
      _.get(selectedInsurance, 'id')
    ) {
      return true;
    }
    return false;
  };

  const premiumCalculation = () => {
    const dataToCalPrem = {
      benefitOption: _.get(values, 'appInsurances[0].benefitOptions', ''),
      cuid: _.get(values, 'appInsurances[0].insuranceForApplicant'),
      term: Math.ceil(eligibleLoanTenure / 12) * 12,
    };
    dispatch(calculatePremium(dataToCalPrem));
  };

  const showField = () => {
    if (_.get(applicantDetails, 'entity.product', '') != 'HFS') {
      return true;
    }
    if (!_.get(selectedInsurance, 'insurance_partner', '')) {
      return true;
    }
    if (_.get(selectedInsurance, 'insurance_partner', '').includes('ICICI')) {
      return true;
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={[16, 16]}>
          <Col xs={0}>
            <InputField
              id="appInsurances[0].id"
              control={control}
              name="appInsurances[0].id"
              type="hidden"
              errors={errors}
            />
          </Col>
          {(_.get(applicantDetails, 'entity.product') === 'BL' ||
            _.get(applicantDetails, 'entity.product', '') === 'HFS') && (
            <>
              <Col {...responsiveColumns}>
                <Form.Item label="Select Applicant">
                  <Controller
                    name="appInsurances[0].insuranceForApplicant"
                    control={control}
                    rules={{
                      required: {
                        value: true,
                        message: 'This field cannot be left empty',
                      },
                    }}
                    disabled
                    showSearch
                    onChange={([value]) => {
                      console.log('applicant', value);
                      return value;
                    }}
                    as={
                      <Select size="large" placeholder="Select Applicant">
                        {applicantsArray.map(item => (
                          <Option value={item.cuid}>{`${
                            item.cuid
                          } - ${item.registeredName ||
                            `${item.firstName} ${item.lastName}`}`}</Option>
                        ))}
                      </Select>
                    }
                  />
                  <ErrorMessage
                    name="appInsurances[0].insuranceForApplicant"
                    errors={errors}
                  />
                </Form.Item>
              </Col>
              {showField() && (
                <Col {...responsiveColumns}>
                  <Form.Item label="Plan*">
                    <Controller
                      name="appInsurances[0].benefitOptions"
                      control={control}
                      rules={{
                        required: {
                          value: true,
                          message: 'This field cannot be left empty',
                        },
                      }}
                      disabled
                      showSearch
                      onChange={([value]) => value}
                      as={
                        <Select size="large" placeholder="Plan">
                          {insurancePlan &&
                            insurancePlan.map(item => (
                              <Option value={item.cmValue}>
                                {item.cmCode}
                              </Option>
                            ))}
                        </Select>
                      }
                    />
                    <ErrorMessage
                      name="appInsurances[0].insuranceForApplicant"
                      errors={errors}
                    />
                  </Form.Item>
                </Col>
              )}
            </>
          )}
          <Col {...responsiveColumns}>
            <InputField
              id="appInsurances[0].sumAssured"
              control={control}
              name="appInsurances[0].sumAssured"
              type="string"
              disabled
              rules={{
                required: {
                  value: false,
                  message: 'This field cannot be left empty',
                },
              }}
              reset={reset}
              errors={errors}
              placeholder="Sum Assured"
              labelHtml="Sum Assured*"
            />
          </Col>
          {showField() && (
            <Col {...responsiveColumns}>
              <InputField
                id="appInsurances[0].coverageTerm"
                control={control}
                name="appInsurances[0].coverageTerm"
                type="number"
                // min={parseInt(eligibleLoanTenure, 10)}
                defaultValue={Math.ceil(eligibleLoanTenure / 12) * 12}
                step="12"
                disabled
                rules={{
                  required: {
                    value: false,
                    message: 'This field cannot be left empty',
                  },
                  validate: value =>
                    value % 12 === 0 ||
                    'Coverage Term should be a multiple of 12 Only!',
                }}
                reset={reset}
                errors={errors}
                placeholder="Coverage Term(in months)"
                labelHtml="Coverage Term(in months)*"
              />
            </Col>
          )}
          {(_.get(applicantDetails, 'entity.product') === 'BL' ||
            _.get(applicantDetails, 'entity.product', '') === 'HFS') && (
            <>
              {showField() && (
                <Col {...responsiveColumns}>
                  <InputField
                    id="appInsurances[0].height"
                    control={control}
                    name="appInsurances[0].height"
                    type="string"
                    disabled
                    rules={{
                      required: {
                        value: true,
                        message: 'This field cannot be left empty',
                      },
                      pattern: numberPattern,
                    }}
                    reset={reset}
                    errors={errors}
                    placeholder="Height(in cm)"
                    labelHtml="Height(in cm)*"
                  />
                </Col>
              )}
              {showField() && (
                <Col {...responsiveColumns}>
                  <InputField
                    id="appInsurances[0].weight"
                    control={control}
                    name="appInsurances[0].weight"
                    type="string"
                    disabled
                    rules={{
                      required: {
                        value: true,
                        message: 'This field cannot be left empty',
                      },
                      pattern: numberPattern,
                    }}
                    reset={reset}
                    errors={errors}
                    placeholder="Weight(in Kg)"
                    labelHtml="Weight(in Kg)*"
                  />
                </Col>
              )}
            </>
          )}
          {_.get(applicantDetails, 'entity.product') === 'BL' && false && (
            <Col {...colSingle} style={{ margin: '20px 0' }}>
              <Button
                type="primary"
                onClick={() => premiumCalculation()}
                disabled={
                  !(
                    _.get(values, 'appInsurances[0].coverageTerm') &&
                    _.get(values, 'appInsurances[0].benefitOptions')
                  )
                }
              >
                Calculate Premium
              </Button>
            </Col>
          )}
        </Row>

        <Divider />
        <StyledTitle>Premium Values</StyledTitle>
        <Row gutter={[16, 16]}>
          <Col {...responsiveColumnsPremium}>
            <Text strong>Premium Payable : </Text>
            <Text>
              &#8377;
              {_.get(selectedInsurance, 'premiumAmount', '') ||
                _.get(premiumData, 'annualPremiumWithTax', '')}
            </Text>
          </Col>
          {_.get(premiumData, 'annualPremium', '') && (
            <>
              <Col {...responsiveColumnsPremium}>
                <Text strong>Base Premium : </Text>
                <Text>
                  &#8377;{_.get(premiumData, 'annualPremiumwithoutTax', '')}
                </Text>
              </Col>
              <Col {...responsiveColumnsPremium}>
                <Text strong>Service Tax : </Text>
                <Text>&#8377;{_.get(premiumData, 'serviceTax', '')}</Text>
              </Col>
            </>
          )}
        </Row>
        <Divider />

        {showField() && (
          <NomineeDetails
            control={control}
            errors={errors}
            values={values}
            applicantDetails={applicantDetails}
            selectedInsurance={selectedInsurance}
          />
        )}
        {_.get(applicantDetails, 'entity.product', '') === 'BL' && false && (
          <Col {...colSingle}>
            <Button
              type="primary"
              onClick={handleGenerateInsurance}
              disabled={!isInsuranceValid()}
            >
              Generate Insurance Form
            </Button>
            {!isInsuranceValid() && (
              <span>
                <strong>
                  (It will be enabled after Insurance, Nominee and Health info
                  are filled)
                </strong>
              </span>
            )}
          </Col>
        )}
        <Col
          {...responsiveColumns}
          style={{ marginTop: '10px', marginBottom: '55px' }}
        >
          {_.get(insuranceForms, '[0].url', '') && (
            <a href={_.get(insuranceForms, '[0].url', '')} target="_blank">
              Download Insurance Form
            </a>
          )}
        </Col>
        {_.get(applicantDetails, 'entity.product', '') === 'BL' && false && (
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        )}
      </form>
      {showField() && <HealthQuestions selectedInsurance={selectedInsurance} />}
    </div>
  );
}

InsuranceForm.propTypes = {};

export default InsuranceForm;
