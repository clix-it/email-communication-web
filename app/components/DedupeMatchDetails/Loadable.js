/**
 *
 * Asynchronously loads the component for DedupeMatchDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
