/**
 *
 * DedupeMatchDetails
 *
 */

import _ from 'lodash';
import React, { useEffect, useState, memo } from 'react';
import {
  Row,
  Col,
  Collapse,
  Empty,
  Spin,
  Menu,
  Divider,
  Descriptions,
  Card,
  Button,
  Space,
  Checkbox,
} from 'antd';
import {
  setSelectedMatchedUser,
  setCheckedUser,
  fetchMatchUsersLoanDetails,
  setSelectedMatchLoan,
} from '../../containers/LeadDetails/actions';
import { columnResponsiveLayout } from '../EntityDetails/constants';
import DedupeMatchForm from '../DedupeMatchForm';
import DedupeAddressCollapse from 'components/DedupeAddressCollapse';
import '../PersonalDetails/personalStyle.css';
import DedupeLoanForm from '../DedupeLoanForm';
import moment from 'moment';

function DedupeMatchDetails({ detailsData, dispatch }) {
  const { Panel } = Collapse;

  const [filteredPartialLeads, setFilteredPartialLeads] = useState([]);
  const [checkboxArr, setCheckboxArr] = useState([]);

  const {
    partialUsers,
    selectedMatchedUser,
    partialLeads,
    loading,
    selectedMatchedUserLoan,
  } = detailsData;

  let contactDetails = {};

  useEffect(() => {
    if (detailsData.partialUsers.length > 0) {
      dispatch(fetchMatchUsersLoanDetails(detailsData.partialUsers));
    }
  }, [detailsData.partialUsers]);

  const setMatchedUser = id => {
    const matchedUser =
      _.find(partialUsers, { cuid: id.key }) ||
      _.find(partialUsers, { cuid: parseInt(id.key) });
    if (matchedUser) dispatch(setSelectedMatchedUser(matchedUser));
  };

  const defaultValues = {
    ...selectedMatchedUser,
  };

  const defaultLoanValues = selectedMatchedUserLoan
    ? {
        ...selectedMatchedUserLoan,
      }
    : {};

  if (defaultValues && defaultValues.cuid) {
    let arr =
      defaultValues.contactibilities &&
      defaultValues.contactibilities.filter(item => item.addressLine1);
    contactDetails = _.mapValues(
      _.groupBy(
        _.orderBy(arr, ['id'], ['desc']),
        'contactType',
      ),
      clist => clist.map(contact => _.omit(contact, 'contactType')),
    );
  }

  useEffect(() => {
    const partialLoanArr =
      partialLeads.length > 0 &&
      partialLeads.filter(
        item => item.customerUnique_ID === selectedMatchedUser.cuid,
      );
    setFilteredPartialLeads(partialLoanArr);
    dispatch(
      setSelectedMatchLoan(partialLoanArr.length > 0 ? partialLoanArr[0] : {}),
    );
  }, [selectedMatchedUser, detailsData.partialLeads]);

  const onCheckboxChange = cuid => {
    let arr = checkboxArr.slice();
    if (arr.indexOf(cuid) >= 0) {
      arr.splice(arr.indexOf(cuid), 1);
    } else {
      arr.push(cuid);
    }
    setCheckboxArr(arr);
  };

  useEffect(() => {
    dispatch(setCheckedUser(checkboxArr));
  }, [checkboxArr]);

  const setMatchedUserLoan = id => {
    const loan =
      _.find(filteredPartialLeads, { loanAccountNumber: id.key }) ||
      _.find(filteredPartialLeads, { loanAccountNumber: parseInt(id.key) });
    if (loan) dispatch(setSelectedMatchLoan(loan));
  };

  return (
    <div className="App">
      <Menu
        onClick={setMatchedUser}
        defaultSelectedKeys={[(selectedMatchedUser || {}).cuid]}
        selectedKeys={[String((selectedMatchedUser || {}).cuid)]}
        mode="horizontal"
        style={{ width: '95%' }}
      >
        {_.map(partialUsers, (user, index) => (
          <Menu.Item key={String(user.cuid)}>
            <Space>
              {`Matching Profile ${index + 1}`}
              <Checkbox
                id={`checkbox_${user.cuid}`}
                onChange={() => onCheckboxChange(user.cuid)}
              />
            </Space>
          </Menu.Item>
        ))}
      </Menu>
      {selectedMatchedUser ? (
        <>
          <Row>
            <Col
              {...columnResponsiveLayout.colSingle}
              style={{
                fontSize: '18px',
                fontWeight: 600,
                padding: '12px 8px',
              }}
            />
          </Row>
          <DedupeMatchForm defaultValues={defaultValues} />
          <DedupeAddressCollapse contactDetails={contactDetails} />
          <Divider />
          {filteredPartialLeads.length > 0 ? (
            <>
              <div
                style={{
                  fontSize: '18px',
                  fontWeight: '600',
                  marginBottom: '15px',
                }}
              >
                Loan Details
              </div>
              <Menu
                onClick={setMatchedUserLoan}
                defaultSelectedKeys={[
                  (selectedMatchedUserLoan || {}).loanAccountNumber,
                ]}
                selectedKeys={[
                  String((selectedMatchedUserLoan || {}).loanAccountNumber),
                ]}
                mode="horizontal"
                style={{ width: '95%' }}
              >
                {filteredPartialLeads.map((loan, index) => {
                  return (
                    <Menu.Item key={String(loan.loanAccountNumber)}>
                      {`Loan ${index + 1}`}
                    </Menu.Item>
                  );
                })}
              </Menu>
            </>
          ) : null}
          {selectedMatchedUserLoan && filteredPartialLeads.length > 0 ? (
            <>
              <Row>
                <Col
                  {...columnResponsiveLayout.colSingle}
                  style={{
                    fontSize: '18px',
                    fontWeight: 600,
                    padding: '12px 8px',
                  }}
                />
              </Row>
              <DedupeLoanForm defaultValues={defaultLoanValues} />
            </>
          ) : null}
        </>
      ) : (
        <Empty />
      )}
    </div>
  );
}

DedupeMatchDetails.propTypes = {};

export default memo(DedupeMatchDetails);
