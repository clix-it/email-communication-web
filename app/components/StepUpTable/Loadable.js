/**
 *
 * Asynchronously loads the component for StepUpTable
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
