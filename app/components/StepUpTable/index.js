/**
 *
 * StepUpTable
 *
 */

import React, { memo, useContext, useState, useEffect, useRef } from 'react';
import {
  Table,
  Select,
  InputNumber,
  Button,
  Popconfirm,
  Form,
  Card,
  message,
} from 'antd';

const { Option } = Select;

const EditableContext = React.createContext();

const EditableRow = ({ ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  inputType,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      message.info('Save failed:', errInfo);
    }
  };

  let childNode = children;
  const inputNode =
    inputType === 'number' ? (
      <InputNumber ref={inputRef} onPressEnter={save} onBlur={save} />
    ) : (
      <Select
        ref={inputRef}
        placeholder="Select Step Type"
        onChange={save}
        onBlur={save}
        onPressEnter={save}
      >
        <Option value="Percentage">Percentage</Option>
        <Option value="Absolute Installment">Absolute Installment</Option>
      </Select>
    );

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `Please enter numeric value.`,
          },
        ]}
      >
        {inputNode}
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

const StepUpTable = ({ setValue, stepUpArr }) => {
  const columnData = [
    {
      title: 'Step Range',
      dataIndex: 'stepRange',
      width: '30%',
      editable: true,
    },
    {
      title: 'Step Type',
      dataIndex: 'stepType',
      editable: true,
    },
    {
      title: 'Step Value',
      dataIndex: 'stepValue',
      editable: true,
    },
    {
      title: 'Operation',
      dataIndex: 'operation',
      render: (text, record) =>
        dataSource.length >= 1 ? (
          <Popconfirm
            title="Sure to delete?"
            onConfirm={() => handleDelete(record.key)}
          >
            <a>Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];

  const [dataSource, setDataSource] = useState([]);
  const [count, setCount] = useState(0);

  useEffect(() => {
    if (stepUpArr && stepUpArr.length > 0) {
      const stepUpArray = stepUpArr.map((item, index) => ({
        key: index,
        stepRange: item.stepRange,
        stepType: item.stepType == 1 ? 'Percentage' : 'Absolute Installment',
        stepValue: item.stepValue,
      }));
      setDataSource(stepUpArray);
      setCount(stepUpArray.length);
    }
  }, []);

  const handleDelete = key => {
    const data = [...dataSource];
    setDataSource(data.filter(item => item.key !== key));
    const newData = data.filter(item => item.key !== key);
    tableValues(newData);
  };

  const handleAdd = () => {
    // const { count, dataSource } = this.state;
    const newData = {
      key: count + 1,
      stepRange: 'Enter Step Range',
      stepType: 'Percentage',
      stepValue: 'Enter Step Value',
    };
    setDataSource([...dataSource, newData]);
    setCount(count + 1);
  };

  const tableValues = newData => {
    if (newData.length > 0) {
      const newStepUpArr = newData
        .filter(
          item =>
            item.stepRange !== 'Enter Step Range' &&
            item.stepType !== 'Enter Step Type' &&
            item.stepValue !== 'Enter Step Value',
        )
        .map(item => ({
          stepRange: item.stepRange,
          stepType: item.stepType == 'Percentage' ? 1 : 2,
          stepValue: item.stepValue,
        }));
      setValue(
        'additionalData.data.extraDataField.stepUpDetails',
        JSON.stringify(newStepUpArr),
      );
    } else {
      setValue('additionalData.data.extraDataField.stepUpDetails', '[]');
    }
  };

  const handleSave = row => {
    const newData = [...dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    newData.forEach(item => {
      item.stepType = newData[index].stepType;
    })
    setDataSource(newData);
    tableValues(newData);
  };

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };

  const columns = columnData.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        inputType:
          col.dataIndex === 'stepRange' || col.dataIndex === 'stepValue'
            ? 'number'
            : 'select',
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave,
      }),
    };
  });
  return (
    <div>
      <Card>
        <Button
          onClick={handleAdd}
          type="primary"
          style={{
            marginBottom: 16,
            float: 'right',
            zIndex: 9,
          }}
          disabled={dataSource && dataSource.length >= 3}
        >
          Add a row
        </Button>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
      </Card>
    </div>
  );
};

StepUpTable.propTypes = {};

export default memo(StepUpTable);
