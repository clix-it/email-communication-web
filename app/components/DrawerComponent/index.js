/**
 *
 * DrawerComponent
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Drawer } from 'antd';
//import PDFViewer from 'pdf-viewer-reactjs';
import PdfNavigation from '../PdfNavigation/index';

const Wrapper = styled.div`
  /* width: 700px; */
  /* height: 700px; */
`;

function DrawerComponent({ currentItem, visible, onClose }) {
  useEffect(() => {
    if (currentItem.fileextension === 'doc') {
      setFileType('docx');
    } else {
      setFileType(currentItem.fileextension);
    }
  }, [currentItem]);

  const [fileType, setFileType] = React.useState('');
  return (
    <Drawer
      width="60%"
      placement="right"
      closable={false}
      onClose={onClose}
      visible={visible}
    >
      <h1>{currentItem.type}</h1>
      <Wrapper>
        {fileType !== 'pdf' ? (
          <img
            src={currentItem.signedUrl}
            width={'80%'}
            style={{ objectFit: 'cover' }}
          />
        ) : (
          <>
          {/* <PDFViewer
            document={{
              url: currentItem.signedUrl,
            }}
            scale={1.6}
            hideZoom
            hideRotation
            scaleStep={0.2}
            maxScale={3}
            minScale={1}
            navigation={PdfNavigation}
            navbarOnTop
          /> */}
          </>
        )}
      </Wrapper>
    </Drawer>
  );
}

DrawerComponent.propTypes = {
  currentItem: PropTypes.object,
  visible: PropTypes.bool,
  onClose: PropTypes.func,
};

export default memo(DrawerComponent);
