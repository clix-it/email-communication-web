/**
 *
 * LeadDocsList
 *
 */

import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import {
  List,
  Avatar,
  Divider,
  Upload,
  Button,
  Typography,
  Select,
  Space,
  Spin,
  message,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import moment from 'moment';
import _ from 'lodash';
import docSvg from '../../images/document.svg';
import DrawerComponent from '../DrawerComponent';
import {
  uploadleadsDocRequest,
  fetchDMSV1Docs,
  fetchAppDocumentsV1,
  fetchOtherDocs,
  fetchAcceptanceLetter,
} from '../../containers/LeadDocuments/actions';
import data from '../../containers/LeadDocuments/mockData';
import { navigateToDocumentsTab } from '../../containers/KycDetails/actions';
import DocsMenu from '../DocsMenu';

const Wrapper = styled.section`
  background: #fff;
  margin: 0 1em;
  padding: 0.25em 1em;
`;

const ListWrapper = styled.div`
  height: 300px;
  overflow: auto;
`;

const UploadDiv = styled.div`
  margin-bottom: 10px;
`;

function LeadDocsList({
  docsData,
  dispatch,
  appDocumentDetails,
  isLoading,
  appId,
  isFileUploaded,
  isFileUploading,
  FileUploadError,
  dmsV1Docs,
  dmsDocsLoading,
  applicantDetails,
  appV1Docs,
  navigateToDocumentTab,
  otherDocs,
  requestType,
  acceptanceLetterDocs,
  activityType,
  allDocs,
}) {
  const [visible, setVisible] = useState(false);
  const [currentItem, setCurrentItem] = useState({});
  const [fileToUpload, setFileToUpload] = useState('');
  const [objType, setObjType] = useState('');
  const [cuid, setCuid] = useState([]);
  const [selectedCuid, setSelectedCuid] = useState('');
  const [otherDoc, setOtherDoc] = useState('');
  const { Title, Text } = Typography;
  const { Option } = Select;

  const array = [
    'applicantDetailsPma',
    'applicantDetailsPoa',
    'dedudeListpma',
    'postSanctionpma',
    'postSanctionpoa',
    'employmentReviewpma',
    'employmentReviewpoa',
    'incomeReviewpma',
    'incomeReviewpoa',
  ];
  const address = location.pathname.split('/');
  const hideUploadDiv = !array.includes(address[1]);

  const { entity } = applicantDetails;

  useEffect(() => {
    if (appDocumentDetails.length > 0) {
      //dispatch(fetchleadsDoc(appDocumentDetails));
    }
    dispatch(fetchDMSV1Docs(appId));
    return () => {
      setCuid([]);
    };
  }, []);

  useEffect(() => {
    dispatch(navigateToDocumentsTab(false));
  }, [navigateToDocumentTab]);

  useEffect(() => {
    const cuidArr = appDocumentDetails.filter(
      item => item.cuid !== undefined && item.userLinked,
    );
    setCuid(cuidArr);
  }, [appDocumentDetails]);

  useEffect(() => {
    if (entity && entity.appDocuments && entity.appDocuments.length > 0) {
      dispatch(fetchAppDocumentsV1(entity.appDocuments));
    }
  }, [entity]);

  useEffect(() => {
    if (_.get(applicantDetails, 'entity.partner')) {
      dispatch(
        fetchOtherDocs(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
    }
  }, []);

  useEffect(() => {
    if (acceptanceLetterDocs && acceptanceLetterDocs.length > 0) {
      acceptanceLetterDocs.forEach(item => {
        const element = document.createElement('a');
        element.setAttribute('href', `${item.url}`);
        element.setAttribute('download', '');
        element.setAttribute('target', '_blank');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
      });
    }
  }, [acceptanceLetterDocs]);

  useEffect(() => {
    if (isFileUploaded) {
      message.success('File uploaded successfully', 4);
    }
    if (FileUploadError !== '') {
      message.error(FileUploadError, 4);
    }
  }, [isFileUploaded, FileUploadError]);

  const handleFileUpload = (file, item = fileToUpload) => {
    const obj_id = selectedCuid !== '' ? selectedCuid : appId;
    const fileName = `${obj_id}-${file.file.name}`;
    const otherDocumentName = otherDoc !== '' ? otherDoc : '';
    const reader = new FileReader();
    if (item.type !== undefined) {
      reader.onload = function() {
        dispatch(
          uploadleadsDocRequest(
            { ...file.file, name: fileName },
            item.obj_type,
            `${item.obj_id}`,
            item.type,
            reader.result,
            file.file,
            otherDocumentName,
          ),
        );
        /*setTimeout(() => {
          dispatch(fetchleadsDoc(appDocumentDetails));
        }, 1000);*/
      };
    } else {
      reader.onload = function() {
        dispatch(
          uploadleadsDocRequest(
            { ...file.file, name: fileName },
            objType,
            selectedCuid !== '' ? selectedCuid : appId,
            fileToUpload,
            reader.result,
            file.file,
            otherDocumentName,
          ),
        );
        /*setTimeout(() => {
          dispatch(fetchleadsDoc(appDocumentDetails));
        }, 1000);*/
      };
    }
    reader.readAsDataURL(file.file);
  };

  function handleFileTypeChange(value) {
    if (value !== 'default') {
      setFileToUpload(value);
      setOtherDoc('');
      // Set ObjectType also for selected fileType
      const objTyp = data.docType.filter(item => item.name === value);
      handleObjTypeChange(objTyp[0].objType);
      if (objTyp[0].objType == 'app') {
        setSelectedCuid('');
      }
    } else {
      setFileToUpload('');
      setOtherDoc('');
    }
  }

  function handleObjTypeChange(value) {
    if (value !== 'default') {
      setObjType(value);
      setOtherDoc('');
    } else {
      setObjType('');
      setSelectedCuid('');
      setOtherDoc('');
    }
  }

  function handleCuidChange(value) {
    if (value !== 'default') {
      setSelectedCuid(value);
      setOtherDoc('');
    } else {
      setSelectedCuid('');
      setOtherDoc('');
    }
  }

  function handleOtherChange(value) {
    if (value !== 'default') {
      setOtherDoc(value);
    } else {
      setOtherDoc('');
    }
  }

  const showDMSDrawer = (e, item) => {
    const dmsItem = {
      fileextension: item.extension,
      signedUrl: `data:${item.mimeType};base64,${item.fileStream}`,
    };
    setCurrentItem(dmsItem);
    setVisible(true);
  };

  const onClose = () => {
    setCurrentItem({});
    setVisible(false);
  };  

  function download(filename, text, mimeType) {
    const element = document.createElement('a');
    element.setAttribute(
      'href',
      `data:${mimeType};base64,${encodeURIComponent(text)}`,
    );
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  const generateAcceptanceLetter = () => {
    dispatch(fetchAcceptanceLetter(appId));
  };

  return (
    <Spin
      spinning={isFileUploading || isLoading}
      tip={isLoading ? 'Loading...' : 'Uploading Document...'}
    >
      <Wrapper>
        <DrawerComponent
          currentItem={currentItem}
          visible={visible}
          onClose={onClose}
        />        
        <DocsMenu
          appDocumentDetails={appDocumentDetails}
          appV1Docs={appV1Docs}
          dispatch={dispatch}
          requestType={requestType}
          activityType={activityType}
          hideUploadDiv={hideUploadDiv}
          setVisible={setVisible}
          setCurrentItem={setCurrentItem}
          applicantDetails={applicantDetails}
          generateAcceptanceLetter={generateAcceptanceLetter}
          docsData={docsData}
          allDocs={allDocs}
        />       
        <Divider />
        {/* KYC-Documents from DMS Version 1 starts here----- */}
        <Title level={4}>Fullfilment KYC Documents</Title>
        <Spin spinning={dmsDocsLoading} tip="Loading...">
          <ListWrapper>
            <List
              className="demo-loadmore-list"
              itemLayout="horizontal"
              size="small"
              dataSource={dmsV1Docs}
              renderItem={(item, index) => (
                <List.Item
                  actions={[
                    <Button
                      type="link"
                      key={index}
                      onClick={event => showDMSDrawer(event, item.data)}
                    >
                      View
                    </Button>,
                    <a
                      key={index}
                      onClick={() => {
                        download(
                          item.data.properties.Document_Name,
                          item.data.fileStream,
                          item.data.mimeType,
                        );
                      }}
                    >
                      Download
                    </a>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar shape="square" src={docSvg} />}
                    title={<Text>{item.data.properties.Document_Name}</Text>}
                    description={
                      <Space>
                        <Text>Name:</Text>
                        <Text>{item.data.properties.First_Name}</Text>
                      </Space>
                    }
                  />
                </List.Item>
              )}
            />
          </ListWrapper>
        </Spin>
        <Divider />
        {/* KYC-Documents from DMS Version 1 ends here----- */}
        {!hideUploadDiv ? (
          <UploadDiv>
            <Space>
              <Upload
                // accept=".jpg, .png, .pdf"
                showUploadList={false}
                customRequest={handleFileUpload}
                disabled={fileToUpload === '' || objType === ''}
              >
                <Button>
                  <UploadOutlined /> Upload
                </Button>
              </Upload>
              <Select
                defaultValue="default"
                style={{ width: 300 }}
                showSearch
                onChange={handleFileTypeChange}
              >
                <Option value="default">Select document to upload</Option>
                {data.docType.map(item => (
                  <Option value={item.name}>{item.name}</Option>
                ))}
              </Select>

              {cuid.length > 0 && objType === 'user' ? (
                <Select
                  defaultValue="default"
                  showSearch
                  style={{ width: 250 }}
                  onChange={handleCuidChange}
                >
                  <Option value="default">Select user</Option>
                  {cuid.map(item => (
                    <Option value={item.cuid}>
                      {item.name} {item.cuid}
                    </Option>
                  ))}
                </Select>
              ) : Object.keys(otherDocs).length > 0 &&
                fileToUpload == 'Others' &&
                objType === 'app' ? (
                <Select
                  defaultValue="default"
                  showSearch
                  style={{ width: 250 }}
                  onChange={handleOtherChange}
                >
                  <Option value="default">Select document</Option>
                  {Object.keys(otherDocs).map(item => (
                    <Option value={item}>{item}</Option>
                  ))}
                </Select>
              ) : null}
            </Space>
          </UploadDiv>
        ) : null}
      </Wrapper>
    </Spin>
  );
}

LeadDocsList.propTypes = {
  docsData: PropTypes.array,
  dispatch: PropTypes.func,
  appDocumentDetails: PropTypes.array,
  isLoading: PropTypes.bool,
  appId: PropTypes.string,
  isFileUploaded: PropTypes.bool,
  isFileUploading: PropTypes.bool,
  FileUploadError: PropTypes.string,
};

export default memo(LeadDocsList);
