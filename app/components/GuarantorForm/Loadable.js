/**
 *
 * Asynchronously loads the component for GuarantorForm
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
