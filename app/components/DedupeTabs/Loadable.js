/**
 *
 * Asynchronously loads the component for DedupeTabs
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
