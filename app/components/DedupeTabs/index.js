/**
 *
 * DedupeTabs
 *
 */

import React, { memo, useState, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Tabs, Card } from 'antd';

import './style.css';

const { TabPane } = Tabs;

const TAB_PANELS = [
  {
    id: 0,
    name: 'Tab-1',
    component: <div>Tab 1</div>,
    tabErrors: {},
    requestType: 'PMA',
  },
  {
    id: 1,
    name: 'Tab-2',
    component: <div>Tab 2</div>,
    tabErrors: {},
    requestType: 'PMA',
  },
];

function DedupeTabs({ mode, tabPanel, isNoEntity }) {
  const [currentTab, setCurrentTab] = useState(tabPanel[0].id);

  const handleTabChange = key => {
    setCurrentTab(key);
  };

  useEffect(() => {
    setCurrentTab(tabPanel[0].id);
  }, [isNoEntity])

  const getTabColor = () => {
    if (tabPanel[0].requestType !== 'PMA') return;
    const children = Array.from(
      document.getElementsByClassName('ant-tabs-tab'),
    );
    if (!children && children.length === 0) return;
    // console.log('called', children);
    children.forEach(child => {
      const name = child.id.split('-');
      if (!name && name.length === 0) return;
      let isValid = _.get(tabPanel[0].tabErrors, name[name.length - 1]);
      const ifKeyExists = _.has(tabPanel[0].tabErrors, name[name.length - 1]);
      // debugger;
      if (name[name.length - 1] === 'applicantDetails' && ifKeyExists) {
        isValid = Object.values(
          tabPanel[0].tabErrors[name[name.length - 1]],
        ).every(applicantError => applicantError === true);
      }
      if (ifKeyExists && !isValid) child.style.color = 'red';
      if (ifKeyExists && isValid) child.style.color = 'limegreen';
    });
  };

  return (
    <>
      <Card bordered={false}>
        <Tabs
          defaultActiveKey={tabPanel[0].id}
          activeKey={currentTab}
          tabPosition={mode}
          className={`tab-container${getTabColor()}`}
          onTabClick={key => handleTabChange(key)}
        >
          {tabPanel.map(panel => (
            <TabPane tab={panel.name} key={panel.id}>
              {panel.component}
            </TabPane>
          ))}
        </Tabs>
      </Card>
    </>
  );
}


DedupeTabs.propTypes = {
  mode: PropTypes.string,
  tabPanel: PropTypes.array,
};


DedupeTabs.defaultProps = {
  mode: 'left',
  tabPanel: TAB_PANELS,
};

export default memo(DedupeTabs);
