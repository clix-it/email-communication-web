/**
 *
 * DedupeAddressCollapse
 *
 */

import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import styled from 'styled-components';
import { Collapse, Descriptions } from 'antd';
import moment from 'moment';

const { Panel } = Collapse;

const CollapseWrapper = styled.div`
  margin-bottom: 2em;
  width: 90%;
`;

function DedupeAddressCollapse({ contactDetails }) {
  const [contactTypeArr, setContactTypeArr] = useState([]);
  useEffect(() => {
    if (Object.keys(contactDetails) && Object.keys(contactDetails).length > 0) {
      setContactTypeArr(Object.keys(contactDetails));
    } else {
      setContactTypeArr([]);
    }
  }, [contactDetails]);
  return (
    <CollapseWrapper>
      <Collapse>
        {contactTypeArr.length > 0
          ? contactTypeArr.map((contactType, index) => (
              <Panel
                header={`${
                  contactType != 'undefined' ? contactType : 'Other'
                } Address`}
                key={`${index}`}
                extra={
                  contactDetails[contactType]
                    ? `Address Added On - ${moment(
                        contactDetails[contactType][0]['createdAt'],
                      ).format('DD-MM-YYYY')}`
                    : ''
                }
              > 
                <Descriptions
                  title=""
                  column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  bordered
                  size="small"
                >
                  <Descriptions.Item label="Address" span={3}>
                    {contactDetails[contactType]
                      ? `${contactDetails[contactType][0]['addressLine1'] ||
                          ''} ${contactDetails[contactType][0][
                          'addressLine2'
                        ] || ''} ${contactDetails[contactType][0][
                          'addressLine3'
                        ] || ''}`
                      : ''}
                  </Descriptions.Item>
                  {/*<Descriptions.Item label="Address Line 2">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['addressLine2']
                      : ''}
                  </Descriptions.Item>
                  <Descriptions.Item label="Address Line 3">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['addressLine3']
                      : ''}
                  </Descriptions.Item>*/}
                  <Descriptions.Item label="Pincode">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['pincode']
                      : ''}
                  </Descriptions.Item>
                  <Descriptions.Item label="Locality">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['locality']
                      : ''}
                  </Descriptions.Item>
                  <Descriptions.Item label="City">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['city']
                      : ''}
                  </Descriptions.Item>
                  <Descriptions.Item label="State">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['state']
                      : ''}
                  </Descriptions.Item>
                  <Descriptions.Item label="Country">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['country']
                      : ''}
                  </Descriptions.Item>
                  <Descriptions.Item label="Phone Number">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['phoneNumber']
                      : ''}
                  </Descriptions.Item>
                  <Descriptions.Item label="Email">
                    {contactDetails[contactType]
                      ? contactDetails[contactType][0]['email']
                      : ''}
                  </Descriptions.Item>
                </Descriptions>
              </Panel>
            ))
          : null}
      </Collapse>
    </CollapseWrapper>
  );
}

DedupeAddressCollapse.propTypes = {};

export default memo(DedupeAddressCollapse);
