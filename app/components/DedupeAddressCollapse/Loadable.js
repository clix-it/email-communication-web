/**
 *
 * Asynchronously loads the component for DedupeAddressCollapse
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
