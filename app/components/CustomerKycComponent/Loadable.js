/**
 *
 * Asynchronously loads the component for CustomerKycComponent
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
