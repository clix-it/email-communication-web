import styled from 'styled-components';

const StyledTitle = styled.div`
  font-size: 18px;
  font-weight: 600;
  margin: 15px 0;
`;

export default StyledTitle;
