import React, { useEffect, useState } from 'react';
import { Table, Descriptions } from 'antd';
import _ from 'lodash';

function DynamicTable({ dataSource, defaultPageSize }) {
  const [dynamicCols, setDynamicCols] = useState([]);
  useEffect(() => {
    const newColumns = [];
    if (dataSource.length === 0) return;
    Object.keys(dataSource[0]).forEach(col => {
      if (dataSource[0][col] instanceof Object) {
        newColumns.push({
          title: _.upperCase(col),
          dataIndex: col,
          width: 180,
          col,
          // align: 'left',
          render: colObj =>
            Object.keys(colObj).map(ele => (
              <Descriptions.Item
                label={_.startCase(_.lowerCase(_.replace(ele, /_/g, ' ')))}
              >
                {Array.isArray(colObj[ele])
                  ? colObj[ele].map((e, i) =>
                    colObj[ele][i + 1] ? (
                      <span>{`${e}, `}</span>
                    ) : (
                      <span>{e}</span>
                    ),
                  )
                  : colObj[ele]}
              </Descriptions.Item>
            )),
        });
      } else {
        newColumns.push({
          title: _.upperCase(col),
          dataIndex: col,
          width: 200,
          col,
        });
      }
    });
    setDynamicCols(newColumns);
  }, [dataSource]);

  return (
    <Table
      locale={{
        emptyText: 'No Data Found',
      }}
      dataSource={dataSource || []}
      pagination={{ defaultPageSize, showSizeChanger: true }}
      columns={dynamicCols}
    />
  );
}

export default DynamicTable;
