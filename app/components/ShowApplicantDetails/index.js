/**
 *
 * ShowApplicantDetails
 *
 */
import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import { Menu, Empty, Descriptions, Table } from 'antd';
import DedupeAddressCollapse from 'components/DedupeAddressCollapse';
import moment from 'moment';

export const Education = ({ educations }) => {
  const columns = [
    {
      title: 'Qualification',
      dataIndex: 'qualification',
      editable: true,
    },
    {
      title: 'Institute',
      dataIndex: 'institute',
      editable: true,
    },
    {
      title: 'Year of Graduation',
      dataIndex: 'yearOfGraduation',
      editable: true,
    },
    {
      title: 'Major',
      dataIndex: 'major',
      editable: true,
    },
    {
      title: 'Marks',
      dataIndex: 'marks',
      editable: true,
    },
  ];
  return <Table columns={columns} dataSource={educations} size="small" />;
};

export const Employment = ({ employments }) => {
  const columns = [
    {
      title: 'Employment Type',
      dataIndex: 'employmentType',
      editable: true,
    },
    {
      title: 'Official Email',
      dataIndex: 'officialEmail',
      editable: true,
    },
    {
      title: 'Company Name',
      dataIndex: 'companyName',
      editable: true,
    },
    {
      title: 'Income In Months',
      dataIndex: 'totalIncomeInMonths',
      editable: true,
    },
  ];
  return <Table columns={columns} dataSource={employments} size="small" />;
};

function ShowApplicantDetails({ users, mainApplicant, dispatch }) {
  let contactArr = [];
  const applicant =
    users &&
    users.length > 0 &&
    users.filter(item => item.cuid == mainApplicant);

  const contactibilities =
    applicant &&
    applicant.length > 0 &&
    _.orderBy(applicant[0].contactibilities, ['id'], ['desc']);

  if (contactibilities && contactibilities.length > 1) {
    debugger;
    contactibilities.shift();
    contactArr = _.mapValues(
      _.groupBy(contactibilities, 'contactType'),
      clist => clist.map(contact => _.omit(contact, 'contactType')),
    );
  }

  return (
    <>
      {applicant && applicant.length > 0 && (
        <>
          {applicant[0].type === 'INDIVIDUAL' &&
          (applicant[0].firstName ||
            applicant[0].lastName ||
            (applicant[0].userDetails && applicant[0].userDetails.gender) ||
            applicant[0].dateOfBirthIncorporation) ? (
            <Descriptions
              title="Basic Details"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {applicant[0].firstName && (
                <Descriptions.Item label="First Name">
                  {applicant[0].firstName}
                </Descriptions.Item>
              )}
              {applicant[0].lastName && (
                <Descriptions.Item label="Last Name">
                  {applicant[0].lastName}
                </Descriptions.Item>
              )}
              {applicant[0].userDetails && applicant[0].userDetails.gender && (
                <Descriptions.Item label="Gender">
                  {applicant[0].userDetails.gender}
                </Descriptions.Item>
              )}
              {applicant[0].dateOfBirthIncorporation && (
                <Descriptions.Item label="DOB/DOI">
                  {moment(applicant[0].dateOfBirthIncorporation).format(
                    'DD-MM-YYYY',
                  )}
                </Descriptions.Item>
              )}
            </Descriptions>
            ) : applicant[0].type === 'COMPANY' &&
            (_.get(applicant, '[0].appLMS.role') === 'Applicant' ||
              !_.get(applicant, '[0].appLMS.role')) &&
            (applicant[0].registeredName ||
              applicant[0].dateOfBirthIncorporation) ? (
            <Descriptions
              title="Basic Details"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {applicant[0].registeredName && (
                <Descriptions.Item label="Registered Name">
                  {applicant[0].registeredName}
                </Descriptions.Item>
              )}
              {applicant[0].corporateStructure && (
                <Descriptions.Item label="Existing Constitution">
                  {applicant[0].corporateStructure}
                </Descriptions.Item>
              )}
              {applicant[0].dateOfBirthIncorporation && (
                <Descriptions.Item label="DOB/DOI">
                  {moment(applicant[0].dateOfBirthIncorporation).format(
                    'DD-MM-YYYY',
                  )}
                </Descriptions.Item>
              )}
              {applicant[0].additionalData &&
                applicant[0].additionalData.data &&
                applicant[0].additionalData.data.extraDataField && (
                  <Descriptions.Item label="CMR">
                    {applicant[0].additionalData.data.extraDataField.cmr || 0}
                  </Descriptions.Item>
                )}
              {applicant[0].additionalData &&
                applicant[0].additionalData.data &&
                applicant[0].additionalData.data.extraDataField && (
                  <Descriptions.Item label="Customer Priority">
                    {applicant[0].additionalData.data.extraDataField
                      .customerPriority || ''}
                  </Descriptions.Item>
                )}
              {applicant[0].userDetails &&
                applicant[0].userDetails.natureOfBusiness && (
                  <Descriptions.Item label="Nature Of Business">
                    {applicant[0].userDetails.natureOfBusiness}
                  </Descriptions.Item>
                )}
              {applicant[0].userDetails &&
                applicant[0].userDetails.industry && (
                  <Descriptions.Item label="Industry">
                    {applicant[0].userDetails.industry}
                  </Descriptions.Item>
                )}
              {applicant[0].userDetails && applicant[0].userDetails.segment && (
                <Descriptions.Item label="Segment">
                  {applicant[0].userDetails.segment}
                </Descriptions.Item>
              )}
              {applicant[0].userDetails && applicant[0].userDetails.product && (
                <Descriptions.Item label="Product">
                  {applicant[0].userDetails.product}
                </Descriptions.Item>
              )}
            </Descriptions>
          ) : null}
          {applicant[0].userIdentities && (
            <Descriptions
              title="Identities"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {applicant[0].userIdentities &&
                applicant[0].userIdentities.pan && (
                  <Descriptions.Item label="PAN">
                    {applicant[0].userIdentities.pan}
                  </Descriptions.Item>
                )}
              {applicant[0].userIdentities &&
                applicant[0].userIdentities.aadhar && (
                  <Descriptions.Item label="Aadhar">
                    {applicant[0].userIdentities.aadhar}
                  </Descriptions.Item>
                )}
              {applicant[0].userIdentities &&
                applicant[0].userIdentities.ckycNumber && (
                  <Descriptions.Item label="Ckyc Number">
                    {applicant[0].userIdentities.ckycNumber}
                  </Descriptions.Item>
                )}
              {applicant[0].userIdentities &&
                applicant[0].userIdentities.voterId && (
                  <Descriptions.Item label="Voter Id">
                    {applicant[0].userIdentities.voterId}
                  </Descriptions.Item>
                )}
              {applicant[0].userIdentities &&
                applicant[0].userIdentities.passport && (
                  <Descriptions.Item label="Passport">
                    {applicant[0].userIdentities.passport}
                  </Descriptions.Item>
                )}
              {applicant[0].userIdentities &&
                applicant[0].userIdentities.cin && (
                  <Descriptions.Item label="CIN">
                    {applicant[0].userIdentities.cin}
                  </Descriptions.Item>
                )}
              {applicant[0].userIdentities &&
                applicant[0].userIdentities.tan && (
                  <Descriptions.Item label="TAN">
                    {applicant[0].userIdentities.tan}
                  </Descriptions.Item>
                )}
            </Descriptions>
          )}
          {applicant[0].userEducations &&
            applicant[0].userEducations.length > 0 && (
              <>
                <Descriptions
                  title="Education"
                  column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  bordered
                  layout="vertical"
                  size="small"
                />
                <Education educations={applicant[0].userEducations} />
              </>
            )}
          {applicant[0].userEmployments &&
            applicant[0].userEmployments.length > 0 && (
              <>
                <Descriptions
                  title="Employments"
                  column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  bordered
                  layout="vertical"
                  size="small"
                />
                <Employment employments={applicant[0].userEmployments} />
              </>
            )}
          {applicant[0].userDetails &&
          (applicant[0].userDetails.fatherName ||
            applicant[0].userDetails.motherName ||
            applicant[0].userDetails.maritalStatus) ? (
            <Descriptions
              title="Relationships"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {applicant[0].userDetails &&
                applicant[0].userDetails.fatherName && (
                  <Descriptions.Item label="Father Name">
                    {applicant[0].userDetails.fatherName}
                  </Descriptions.Item>
                )}
              {applicant[0].userDetails &&
                applicant[0].userDetails.motherName && (
                  <Descriptions.Item label="Mother Name">
                    {applicant[0].userDetails.motherName}
                  </Descriptions.Item>
                )}
              {applicant[0].userDetails &&
                applicant[0].userDetails.maritalStatus && (
                  <Descriptions.Item label="Marital Status">
                    {applicant[0].userDetails.maritalStatus}
                  </Descriptions.Item>
                )}
            </Descriptions>
          ) : null}
          {applicant[0].preferredPhone || applicant[0].preferredEmail ? (
            <Descriptions
              title="Contact Details"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              <Descriptions.Item label="Mobile Number">
                {applicant[0].preferredPhone}
              </Descriptions.Item>
              <Descriptions.Item label="Email">
                {applicant[0].preferredEmail}
              </Descriptions.Item>
            </Descriptions>
          ) : null}
          {contactibilities.length > 0 && (
            <Descriptions
              title="Primary Address"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {contactibilities[0].addressLine1 && (
                <Descriptions.Item label="Address Line 1">
                  {contactibilities[0].addressLine1}
                </Descriptions.Item>
              )}
              {contactibilities[0].addressLine2 && (
                <Descriptions.Item label="Address Line 2">
                  {contactibilities[0].addressLine2}
                </Descriptions.Item>
              )}
              {contactibilities[0].addressLine3 && (
                <Descriptions.Item label="Address Line 3">
                  {contactibilities[0].addressLine3}
                </Descriptions.Item>
              )}
              {contactibilities[0].pincode && (
                <Descriptions.Item label="Pincode">
                  {contactibilities[0].pincode}
                </Descriptions.Item>
              )}
              {contactibilities[0].locality && (
                <Descriptions.Item label="Locality">
                  {contactibilities[0].locality}
                </Descriptions.Item>
              )}
              {contactibilities[0].city && (
                <Descriptions.Item label="City">
                  {contactibilities[0].city}
                </Descriptions.Item>
              )}
              {contactibilities[0].state && (
                <Descriptions.Item label="State">
                  {contactibilities[0].state}
                </Descriptions.Item>
              )}
              {contactibilities[0].country && (
                <Descriptions.Item label="Country">
                  {contactibilities[0].country}
                </Descriptions.Item>
              )}
            </Descriptions>
          )}
          {contactArr && contactibilities.length > 1 && (
            <>
              <Descriptions
                title="Other Addresses"
                column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                bordered
                layout="vertical"
                size="small"
              />
              <DedupeAddressCollapse contactDetails={contactArr} />
            </>
          )}
        </>
      )}
      {applicant.length == 0 && <Empty />}
      {applicant &&
      applicant.length > 0 &&
      (contactibilities.length > 0 ||
        applicant[0].preferredPhone ||
        applicant[0].preferredEmail ||
        (applicant[0].userDetails &&
          (applicant[0].userDetails.fatherName ||
            applicant[0].userDetails.motherName ||
            applicant[0].userDetails.maritalStatus)) ||
        (applicant[0].firstName ||
          applicant[0].lastName ||
          (applicant[0].userDetails && applicant[0].userDetails.gender) ||
          (applicant[0].userDetails &&
            applicant[0].userDetails.natureOfBusiness) ||
          (applicant[0].userDetails && applicant[0].userDetails.segment) ||
          (applicant[0].userDetails && applicant[0].userDetails.industry) ||
          (applicant[0].userDetails && applicant[0].userDetails.product) ||
          applicant[0].dateOfBirthIncorporation ||
          applicant[0].registeredName)) ? null : (
        <Empty />
      )}
    </>
  );
}

ShowApplicantDetails.propTypes = {};

export default memo(ShowApplicantDetails);
