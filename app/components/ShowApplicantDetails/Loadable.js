/**
 *
 * Asynchronously loads the component for ShowApplicantDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
