/**
 *
 * ShowCoApplicantDetails
 *
 */
import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import { Menu, Empty, Descriptions, Table } from 'antd';
import DedupeAddressCollapse from 'components/DedupeAddressCollapse';
import moment from 'moment';
import { setCoApplicant } from '../../containers/CustomerLoanDetails/actions';

export const Education = ({ educations }) => {
  const columns = [
    {
      title: 'Qualification',
      dataIndex: 'qualification',
      editable: true,
    },
    {
      title: 'Institute',
      dataIndex: 'institute',
      editable: true,
    },
    {
      title: 'Year of Graduation',
      dataIndex: 'yearOfGraduation',
      editable: true,
    },
    {
      title: 'Major',
      dataIndex: 'major',
      editable: true,
    },
    {
      title: 'Marks',
      dataIndex: 'marks',
      editable: true,
    },
  ];
  return <Table columns={columns} dataSource={educations} size="small" />;
};

export const Employment = ({ employments }) => {
  const columns = [
    {
      title: 'Employment Type',
      dataIndex: 'employmentType',
      editable: true,
    },
    {
      title: 'Official Email',
      dataIndex: 'officialEmail',
      editable: true,
    },
    {
      title: 'Company Name',
      dataIndex: 'companyName',
      editable: true,
    },
    {
      title: 'Income In Months',
      dataIndex: 'totalIncomeInMonths',
      editable: true,
    },
  ];
  return <Table columns={columns} dataSource={employments} size="small" />;
};

function ShowCoApplicantDetails({
  users,
  mainApplicant,
  dispatch,
  selectedCoApplicant,
}) {
  const coApplicants =
    users &&
    users.length > 1 &&
    users
      .filter(item => item.cuid != mainApplicant)
      .reduce((unique, o) => {
        if (!unique.some(obj => obj.cuid === o.cuid)) {
          unique.push(o);
        }
        return unique;
      }, []);
  const [showCoApplicant, setShowCoApplicant] = useState({});
  const [contactibilities, setContactibilities] = useState([]);
  const [contactArr, setContactArr] = useState([]);
  const setCurrentCoApplicant = id => {
    const user =
      _.find(coApplicants, { cuid: id.key }) ||
      _.find(coApplicants, { cuid: parseInt(id.key) });
    if (user) dispatch(setCoApplicant(user));
  };
  useEffect(() => {
    if (selectedCoApplicant && selectedCoApplicant.cuid) {
      setShowCoApplicant(selectedCoApplicant);
    } else {
      setShowCoApplicant(coApplicants[0]);
    }
  }, [selectedCoApplicant]);

  useEffect(() => {
    if (showCoApplicant && showCoApplicant.cuid) {
      const contactDetailsArr = _.orderBy(
        showCoApplicant.contactibilities,
        ['id'],
        ['desc'],
      );
      setContactibilities(contactDetailsArr);
    }
  }, [showCoApplicant]);

  useEffect(() => {
    if (contactibilities && contactibilities.length > 1) {
      debugger;
      contactibilities.shift();
      const contactArray = _.mapValues(
        _.groupBy(contactibilities, 'contactType'),
        clist => clist.map(contact => _.omit(contact, 'contactType')),
      );
      setContactArr(contactArray);
    }
  }, [contactibilities]);

  return (
    <>
      {coApplicants.length > 0 && (
        <Menu
          onClick={setCurrentCoApplicant}
          defaultSelectedKeys={[
            selectedCoApplicant.cuid || coApplicants[0].cuid,
          ]}
          selectedKeys={[
            String(selectedCoApplicant.cuid || coApplicants[0].cuid),
          ]}
          mode="horizontal"
          style={{ width: '95%' }}
        >
          {_.map(coApplicants, (user, index) => (
            <Menu.Item key={String(user.cuid)}>{`Co-Applicant ${index +
              1}`}</Menu.Item>
          ))}
        </Menu>
      )}
      {showCoApplicant && showCoApplicant.cuid && (
        <>
          {showCoApplicant.type === 'INDIVIDUAL' &&
          (showCoApplicant.firstName ||
            showCoApplicant.lastName ||
            (showCoApplicant.userDetails &&
              showCoApplicant.userDetails.gender) ||
            showCoApplicant.dateOfBirthIncorporation) ? (
            <Descriptions
              title="Basic Details"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {showCoApplicant.firstName && (
                <Descriptions.Item label="First Name">
                  {showCoApplicant.firstName}
                </Descriptions.Item>
              )}
              {showCoApplicant.lastName && (
                <Descriptions.Item label="Last Name">
                  {showCoApplicant.lastName}
                </Descriptions.Item>
              )}
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.gender && (
                  <Descriptions.Item label="Gender">
                    {showCoApplicant.userDetails.gender}
                  </Descriptions.Item>
                )}
              {showCoApplicant.dateOfBirthIncorporation && (
                <Descriptions.Item label="DOB/DOI">
                  {moment(showCoApplicant.dateOfBirthIncorporation).format(
                    'DD-MM-YYYY',
                  )}
                </Descriptions.Item>
              )}
            </Descriptions>
            ) : showCoApplicant.type === 'COMPANY' &&
            (_.get(showCoApplicant, 'appLMS.role') === 'Applicant' ||
              !_.get(defaultValues, 'appLMS.role')) &&
            (showCoApplicant.registeredName ||
              showCoApplicant.dateOfBirthIncorporation) ? (
            <Descriptions
              title="Basic Details"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {showCoApplicant.registeredName && (
                <Descriptions.Item label="Registered Name">
                  {showCoApplicant.registeredName}
                </Descriptions.Item>
              )}
              {showCoApplicant.corporateStructure && (
                <Descriptions.Item label="Existing Constitution">
                  {showCoApplicant.corporateStructure}
                </Descriptions.Item>
              )}
              {showCoApplicant.dateOfBirthIncorporation && (
                <Descriptions.Item label="DOB/DOI">
                  {moment(showCoApplicant.dateOfBirthIncorporation).format(
                    'DD-MM-YYYY',
                  )}
                </Descriptions.Item>
              )}
              {showCoApplicant.additionalData &&
                showCoApplicant.additionalData.data &&
                showCoApplicant.additionalData.data.extraDataField && (
                  <Descriptions.Item label="CMR">
                    {showCoApplicant.additionalData.data.extraDataField.cmr ||
                      0}
                  </Descriptions.Item>
                )}
              {showCoApplicant.additionalData &&
                showCoApplicant.additionalData.data &&
                showCoApplicant.additionalData.data.extraDataField && (
                  <Descriptions.Item label="Customer Priority">
                    {showCoApplicant.additionalData.data.extraDataField
                      .customerPriority || ''}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.natureOfBusiness && (
                  <Descriptions.Item label="Nature Of Business">
                    {showCoApplicant.userDetails.natureOfBusiness}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.industry && (
                  <Descriptions.Item label="Industry">
                    {showCoApplicant.userDetails.industry}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.segment && (
                  <Descriptions.Item label="Segment">
                    {showCoApplicant.userDetails.segment}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.product && (
                  <Descriptions.Item label="Product">
                    {showCoApplicant.userDetails.product}
                  </Descriptions.Item>
                )}
            </Descriptions>
          ) : null}
          {showCoApplicant.userIdentities && (
            <Descriptions
              title="Identities"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {showCoApplicant.userIdentities &&
                showCoApplicant.userIdentities.pan && (
                  <Descriptions.Item label="PAN">
                    {showCoApplicant.userIdentities.pan}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userIdentities &&
                showCoApplicant.userIdentities.aadhar && (
                  <Descriptions.Item label="Aadhar">
                    {showCoApplicant.userIdentities.aadhar}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userIdentities &&
                showCoApplicant.userIdentities.ckycNumber && (
                  <Descriptions.Item label="Ckyc Number">
                    {showCoApplicant.userIdentities.ckycNumber}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userIdentities &&
                showCoApplicant.userIdentities.voterId && (
                  <Descriptions.Item label="Voter Id">
                    {showCoApplicant.userIdentities.voterId}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userIdentities &&
                showCoApplicant.userIdentities.passport && (
                  <Descriptions.Item label="Passport">
                    {showCoApplicant.userIdentities.passport}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userIdentities &&
                showCoApplicant.userIdentities.cin && (
                  <Descriptions.Item label="CIN">
                    {showCoApplicant.userIdentities.cin}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userIdentities &&
                showCoApplicant.userIdentities.tan && (
                  <Descriptions.Item label="TAN">
                    {showCoApplicant.userIdentities.tan}
                  </Descriptions.Item>
                )}
            </Descriptions>
          )}
          {showCoApplicant.userEducations &&
            showCoApplicant.userEducations.length > 0 && (
              <>
                <Descriptions
                  title="Education"
                  column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  bordered
                  layout="vertical"
                  size="small"
                />
                <Education educations={showCoApplicant.userEducations} />
              </>
            )}
          {showCoApplicant.userEmployments &&
            showCoApplicant.userEmployments.length > 0 && (
              <>
                <Descriptions
                  title="Employments"
                  column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  bordered
                  layout="vertical"
                  size="small"
                />
                <Employment employments={showCoApplicant.userEmployments} />
              </>
            )}
          {showCoApplicant.userDetails &&
          (showCoApplicant.userDetails.fatherName ||
            showCoApplicant.userDetails.motherName ||
            showCoApplicant.userDetails.maritalStatus) ? (
            <Descriptions
              title="Relationships"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.fatherName && (
                  <Descriptions.Item label="Father Name">
                    {showCoApplicant.userDetails.fatherName}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.motherName && (
                  <Descriptions.Item label="Mother Name">
                    {showCoApplicant.userDetails.motherName}
                  </Descriptions.Item>
                )}
              {showCoApplicant.userDetails &&
                showCoApplicant.userDetails.maritalStatus && (
                  <Descriptions.Item label="Marital Status">
                    {showCoApplicant.userDetails.maritalStatus}
                  </Descriptions.Item>
                )}
            </Descriptions>
          ) : null}
          {showCoApplicant.preferredPhone || showCoApplicant.preferredEmail ? (
            <Descriptions
              title="Contact Details"
              column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              bordered
              layout="vertical"
              size="small"
            >
              <Descriptions.Item label="Mobile Number">
                {showCoApplicant.preferredPhone}
              </Descriptions.Item>
              <Descriptions.Item label="Email">
                {showCoApplicant.preferredEmail}
              </Descriptions.Item>
            </Descriptions>
          ) : null}
          {contactibilities.length > 0 &&
            (contactibilities[0].addressLine1 ||
              contactibilities[0].addressLine2 ||
              contactibilities[0].addressLine3) && (
              <Descriptions
                title="Primary Address"
                column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                bordered
                layout="vertical"
                size="small"
              >
                {contactibilities[0].addressLine1 && (
                  <Descriptions.Item label="Address Line 1">
                    {contactibilities[0].addressLine1}
                  </Descriptions.Item>
                )}
                {contactibilities[0].addressLine2 && (
                  <Descriptions.Item label="Address Line 2">
                    {contactibilities[0].addressLine2}
                  </Descriptions.Item>
                )}
                {contactibilities[0].addressLine3 && (
                  <Descriptions.Item label="Address Line 3">
                    {contactibilities[0].addressLine3}
                  </Descriptions.Item>
                )}
                {contactibilities[0].pincode && (
                  <Descriptions.Item label="Pincode">
                    {contactibilities[0].pincode}
                  </Descriptions.Item>
                )}
                {contactibilities[0].locality && (
                  <Descriptions.Item label="Locality">
                    {contactibilities[0].locality}
                  </Descriptions.Item>
                )}
                {contactibilities[0].city && (
                  <Descriptions.Item label="City">
                    {contactibilities[0].city}
                  </Descriptions.Item>
                )}
                {contactibilities[0].state && (
                  <Descriptions.Item label="State">
                    {contactibilities[0].state}
                  </Descriptions.Item>
                )}
                {contactibilities[0].country && (
                  <Descriptions.Item label="Country">
                    {contactibilities[0].country}
                  </Descriptions.Item>
                )}
              </Descriptions>
            )}
          {contactArr && contactibilities.length > 1 && (
            <>
              <Descriptions
                title="Other Addresses"
                column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                bordered
                layout="vertical"
                size="small"
              />
              <DedupeAddressCollapse contactDetails={contactArr} />
            </>
          )}
        </>
      )}
      {coApplicants.length == 0 && <Empty />}
      {showCoApplicant &&
      (contactibilities.length > 0 ||
        showCoApplicant.preferredPhone ||
        showCoApplicant.preferredEmail ||
        (showCoApplicant.userDetails &&
          (showCoApplicant.userDetails.fatherName ||
            showCoApplicant.userDetails.motherName ||
            showCoApplicant.userDetails.maritalStatus)) ||
        (showCoApplicant.firstName ||
          showCoApplicant.lastName ||
          (showCoApplicant.userDetails && showCoApplicant.userDetails.gender) ||
          showCoApplicant.dateOfBirthIncorporation ||
          showCoApplicant.registeredName)) ? null : (
        <Empty />
      )}
    </>
  );
}

ShowCoApplicantDetails.propTypes = {};

export default memo(ShowCoApplicantDetails);
