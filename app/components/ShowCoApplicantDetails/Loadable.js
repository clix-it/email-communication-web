/**
 *
 * Asynchronously loads the component for ShowCoApplicantDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
