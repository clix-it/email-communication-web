/**
 *
 * EntityForm
 *
 */

import _ from 'lodash';
import React, { useEffect } from 'react';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import { Row, Col, Divider } from 'antd';

import InputField from 'components/InputField';

import Constitution from 'containers/Constitutions';
import GstnInput from 'containers/GstnInput';
import PanNumber from 'containers/PanNumber';
import {
  INDUSTRY_RES,
  DL_INDUSTRY,
  RELATIONS_WITH_BORROWER,
} from 'containers/ApplicantDetails/constants';
import SelectRelationship from 'containers/SelectRelationship';
import BusinessDetailsEntity from 'components/BusinessDetailsEntity';
import StyledTitle from 'components/StyledTitle';
import { stringSimilar } from '../../utils/helpers';
import { cmrData } from '../EntityDetails/constants';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

function EntityForm({
  control,
  errors,
  values,
  setError,
  clearError,
  setValue,
  userIndex,
  cuid,
  ifEntityCoApplicant,
  entityData,
  reset,
  masterPriorityData,
  masterRiskData,
  masterCategoryData,
  allFieldsDisabled,
  partner,
  product,
  requestType,
  BUSINESSTYPE,
  COMPANYCATEGORY,
}) {
  const existingLAN = _.get(values, 'users[0].corporateStructure');

  const natureOptions =
    partner == 'DL'
      ? _.map(
          _.filter(_.get(DL_INDUSTRY, 'data.children'), { type: 'Nature' }),
          'key',
        )
      : _.map(
          _.filter(_.get(INDUSTRY_RES, 'data.children'), { type: 'Nature' }),
          'key',
        );

  const selectedNature = _.get(values, 'users[0].userDetails.natureOfBusiness');

  const data = partner == 'DL' ? DL_INDUSTRY : INDUSTRY_RES;

  const industryOptionObject = _.get(
    _.find(_.get(data, 'data.children'), {
      key: selectedNature,
    }),
    'children',
  );

  const industryOptions = selectedNature
    ? _.map(industryOptionObject, 'key')
    : [];

  const selectedIndustry = _.get(values, 'users[0].userDetails.industry');

  const segmentOptionObject = _.get(
    _.find(industryOptionObject, {
      key: selectedIndustry,
    }),
    'children',
  );

  const segmentOptions = selectedIndustry
    ? _.map(segmentOptionObject, 'key')
    : [];

  const selectedSegment = _.get(values, 'users[0].userDetails.segment');

  const productOptionObject = _.get(
    _.find(segmentOptionObject, {
      key: selectedSegment,
    }),
    'children',
  );

  const productOptions = selectedSegment
    ? _.map(productOptionObject, 'key')
    : [];

  const onChangeNature = () => {
    setValue(`users[0].userDetails.industry`, false);
    setValue(`users[0].userDetails.segment`, false);
    setValue(`users[0].userDetails.product`, false);
  };

  const onChangeIndustry = () => {
    setValue(`users[0].userDetails.segment`, false);
    setValue(`users[0].userDetails.product`, false);
  };

  const onChangeSegment = () => {
    setValue(`users[0].userDetails.product`, false);
  };
  console.log('values in entityform', values);
  return (
    <>
      <Row gutter={[16, 16]}>
        <Col xs={0}>
          <InputField
            id="id"
            control={control}
            name="users[0].id"
            type="hidden"
            errors={errors}
          />
          {/* <InputField
            id="id"
            control={control}
            name="users[0].userDetails.id"
            type="hidden"
            errors={errors}
          /> */}
          <InputField
            id="id"
            control={control}
            name="additionalData.data.id"
            type="hidden"
            errors={errors}
          />
          <InputField
            id="id"
            control={control}
            name="users[0].appLMS.id"
            type="hidden"
            errors={errors}
            defaultValue={_.get(entityData, 'appLMS.id', '')}
          />

          <InputField
            id="id"
            control={control}
            name="users[0].appLMS.role"
            type="hidden"
            errors={errors}
            defaultValue={_.get(entityData, 'appLMS.role', '')}
          />
          <InputField
            id="entityOfficers.belongsTo"
            control={control}
            name="entityOfficers.belongsTo"
            type="hidden"
            errors={errors}
          />
          <InputField
            id="entityOfficers.id"
            control={control}
            name="entityOfficers.id"
            type="hidden"
            errors={errors}
          />
          <InputField
            id={`users[${userIndex}].userEmployments`}
            control={control}
            name={`users[${userIndex}].userEmployments`}
            type="hidden"
            errors={errors}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id="users[0].registeredName"
            control={control}
            name="users[0].registeredName"
            type="string"
            reset={reset}
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Name of Entity"
            labelHtml="Entity Name*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <Constitution
            id="users[0].corporateStructure"
            control={control}
            name="users[0].corporateStructure"
            type="string"
            reset={reset}
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Name of Constitution"
            labelHtml="Existing Constitution*"
            existingLAN={existingLAN}
            cuid={cuid}
            clearError={clearError}
            setError={setError}
            entityData={entityData}
            setValue={setValue}
            disabled={allFieldsDisabled}
            partner={partner}
            product={product}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id={`users[${userIndex}].dateOfBirthIncorporation`}
            control={control}
            name={`users[${userIndex}].dateOfBirthIncorporation`}
            type="datepicker"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Date Of Incorporation"
            labelHtml="Date Of Incorporation*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id="users[0].userDetails.natureOfBusiness"
            control={control}
            name="users[0].userDetails.natureOfBusiness"
            type="select"
            handleChange={onChangeNature}
            options={natureOptions}
            errors={errors}
            values={values}
            rules={{
              required: {
                value: true,
                message: 'This Field should not be empty!',
              },
            }}
            placeholder="Nature Of Business"
            labelHtml="Nature Of Business*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id="users[0].userDetails.industry"
            control={control}
            handleChange={onChangeIndustry}
            name="users[0].userDetails.industry"
            type="select"
            showSearch
            reset={reset}
            options={industryOptions}
            errors={errors}
            values={values}
            rules={{
              required: {
                value: true,
                message: 'This Field should not be empty!',
              },
            }}
            placeholder="Industry"
            labelHtml="Industry*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id="users[0].userDetails.segment"
            control={control}
            name="users[0].userDetails.segment"
            type="select"
            handleChange={onChangeSegment}
            options={segmentOptions}
            errors={errors}
            showSearch
            values={values}
            rules={{
              required: {
                value: true,
                message: 'This Field should not be empty!',
              },
            }}
            placeholder="Segment"
            labelHtml="Segment*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id="users[0].userDetails.product"
            control={control}
            name="users[0].userDetails.product"
            type="select"
            showSearch
            options={productOptions}
            errors={errors}
            values={values}
            rules={{
              required: {
                value: true,
                message: 'This Field should not be empty!',
              },
            }}
            placeholder="Product"
            labelHtml="Product*"
            disabled={allFieldsDisabled}
          />
        </Col>
        {masterCategoryData && masterCategoryData.length > 0 && (
          <Col {...responsiveColumns}>
            <InputField
              id="users[0].userDetails.category"
              control={control}
              name="users[0].userDetails.category"
              type="select"
              options={Object.values(masterCategoryData)}
              errors={errors}
              values={values}
              rules={{
                required: {
                  value: true,
                  message: 'This Field should not be empty!',
                },
              }}
              placeholder="Category"
              labelHtml="Category*"
              disabled={allFieldsDisabled}
            />
          </Col>
        )}
        {!ifEntityCoApplicant && partner !== 'HFSAPP' && (
          <Col {...responsiveColumns}>
            <InputField
              id="additionalData.data.noOfEmployee"
              control={control}
              name="additionalData.data.noOfEmployee"
              type="string"
              errors={errors}
              values={values}
              rules={{
                required: {
                  value: true,
                  message: 'This Field should not be empty!',
                },
                pattern: /^([0-9]){1,}$/,
              }}
              placeholder="Employees Count"
              labelHtml="Number of Employees*"
              disabled={allFieldsDisabled}
            />
          </Col>
        )}
        {partner !== 'HFSAPP' && (
          <Col {...responsiveColumns}>
            <InputField
              id="users[0].userDetails.turnover"
              control={control}
              name="users[0].userDetails.turnover"
              rules={{
                required: true,
                message: 'This Field should not be empty!',
              }}
              type="number"
              onChange={([e]) => e}
              errors={errors}
              placeholder="Annual Turnover"
              labelHtml="Annual Turnover*"
              disabled={allFieldsDisabled}
            />
          </Col>
        )}
        {masterRiskData && masterRiskData.length > 0 && (
          <Col {...responsiveColumns}>
            <InputField
              id="users[0].riskLevel"
              control={control}
              name="users[0].riskLevel"
              type="select"
              options={Object.values(masterRiskData)}
              errors={errors}
              values={values}
              rules={{
                required: {
                  value: false,
                  message: 'This Field should not be empty!',
                },
              }}
              placeholder="Risk Segment"
              labelHtml="Risk Segment"
              disabled={allFieldsDisabled}
            />
          </Col>
        )}
        {ifEntityCoApplicant && (
          <Col {...responsiveColumns}>
            <SelectRelationship
              isNoEntity={false}
              key="entityOfficers"
              setValue={setValue}
              userIndex={userIndex}
              reset={reset}
              namePrefix="entityOfficers"
              id="entityOfficers.designation"
              control={control}
              name="entityOfficers.designation"
              type="select"
              options={RELATIONS_WITH_BORROWER}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="Role/Relation"
              labelHtml="Role/Relation with Borrower Entity*"
              ifEntityCoApplicant={ifEntityCoApplicant}
              entityData={entityData}
              clearError={clearError}
              disabled={allFieldsDisabled}
            />
          </Col>
        )}
        {!ifEntityCoApplicant && (
          <>
            <Col {...responsiveColumns}>
              <InputField
                id="additionalData.data.extraDataField.cmr"
                control={control}
                name="additionalData.data.extraDataField.cmr"
                type="select"
                options={cmrData}
                errors={errors}
                values={values}
                rules={{
                  required: {
                    value: true,
                    message: 'This Field should not be empty!',
                  },
                }}
                placeholder="CMR"
                labelHtml="CMR*"
                disabled={allFieldsDisabled}
              />
            </Col>

            {masterPriorityData &&
              masterPriorityData.length > 0 &&
              partner !== 'X2C' && (
                <Col {...responsiveColumns}>
                  <InputField
                    id="additionalData.data.extraDataField.customerPriority"
                    control={control}
                    name="additionalData.data.extraDataField.customerPriority"
                    type="select"
                    options={Object.values(masterPriorityData)}
                    errors={errors}
                    values={values}
                    rules={{
                      required: {
                        value: true,
                        message: 'This Field should not be empty!',
                      },
                    }}
                    placeholder="Customer Priority"
                    labelHtml="Customer Priority*"
                    disabled={allFieldsDisabled}
                  />
                </Col>
              )}
            {partner != 'HFSAPP' && (
              <>
                <Col {...responsiveColumns}>
                  <InputField
                    id="additionalData.data.directorsChanged"
                    control={control}
                    name="additionalData.data.directorsChanged"
                    type="radio"
                    errors={errors}
                    labelHtml="Has Directors Changed ?"
                    values={values}
                    disabled={allFieldsDisabled}
                  />
                </Col>
                <Col {...responsiveColumns}>
                  <InputField
                    id="additionalData.data.sharedholginPatternChanged"
                    control={control}
                    name="additionalData.data.sharedholginPatternChanged"
                    type="radio"
                    errors={errors}
                    labelHtml="Has Share Holding Pattern Changed ?"
                    values={values}
                    disabled={allFieldsDisabled}
                  />
                </Col>
              </>
            )}
          </>
        )}
      </Row>
      <Divider />
      <div
        style={{
          fontSize: '18px',
          fontWeight: '600',
          marginBottom: '15px',
        }}
      >
        Identities
      </div>
      <Row gutter={[16, 16]}>
        <Col {...responsiveColumns}>
          <PanNumber
            // entityName={_.get(values, 'users[0].registeredName')}
            // panName={panName}
            id="users[0].userIdentities.pan"
            pan={_.get(values, 'users[0].userIdentities.pan')}
            control={control}
            name="users[0].userIdentities.pan"
            type="string"
            errors={errors}
            values={values}
            setError={setError}
            clearError={clearError}
            rules={{
              required: {
                value: product != 'TW',
                message: 'This Field should not be empty!',
              },
              pattern: /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/,
            }}
            placeholder="Pan"
            labelHtml="PAN*"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <GstnInput
            id="users[0].appGsts[0].gstn"
            pan={_.get(values, 'users[0].userIdentities.pan')}
            control={control}
            setValue={setValue}
            name="users[0].appGsts[0].gstn"
            type="select"
            errors={errors}
            values={values}
            setError={setError}
            clearError={clearError}
            cuid={cuid}
            disabled={allFieldsDisabled}
            // rules={{
            //   required: {
            //     value: true,
            //     message: 'This Field should not be empty!',
            //   },
            // }}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id="users[0].userIdentities.cin"
            control={control}
            name="users[0].userIdentities.cin"
            type="string"
            errors={errors}
            values={values}
            rules={{
              required: {
                value: false,
                message: 'This Field should not be empty!',
              },
            }}
            placeholder="CIN"
            labelHtml="Corporate Identity Number (CIN)"
            disabled={allFieldsDisabled}
          />
        </Col>
        <Col {...responsiveColumns}>
          <InputField
            id="users[0].userIdentities.tan"
            control={control}
            name="users[0].userIdentities.tan"
            type="string"
            errors={errors}
            values={values}
            placeholder="TAN"
            labelHtml="Tax Deduction Account Number (TAN)"
            disabled={allFieldsDisabled}
          />
        </Col>
      </Row>
      {partner === 'HFSAPP' && (
        <>
          <Divider />
          <StyledTitle>Business details</StyledTitle>
          <BusinessDetailsEntity
            responsiveColumns={responsiveColumns}
            values={values}
            userIndex={userIndex}
            setValue={setValue}
            // disabled
            initDataSource={_.get(entityData, `userEmployments`, [])}
            BUSINESSTYPE={BUSINESSTYPE}
            COMPANYCATEGORY={COMPANYCATEGORY}
          />
        </>
      )}
    </>
  );
}

EntityForm.propTypes = {};

export default EntityForm;
