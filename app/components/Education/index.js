import React from 'react';
import EditableTable from 'components/EditableTable';
import _ from 'lodash';
import { isEmpty } from '../../utils/helpers';

function Education({
  values,
  userIndex,
  setValue,
  initDataSource,
  disabled,
  setEducation,
  education,
  isGuarantor,
  applicantDetails,
  partner,
}) {
  const {
    masterData: { QUALIFICATION = [] },
  } = applicantDetails;
  const initColumns = [
    {
      title: 'Qualification',
      dataIndex: 'qualification',
      editable: true,
      width: 400,
      type: partner == 'HFSAPP' ? 'select' : 'text',
      dropdown:
        QUALIFICATION.length > 0 ? QUALIFICATION.map(item => item.value) : [],
    },
    {
      title: 'Institute',
      dataIndex: 'institute',
      editable: true,
    },
    {
      title: 'Year of Graduation',
      dataIndex: 'yearOfGraduation',
      editable: true,
    },
    {
      title: 'Major',
      dataIndex: 'major',
      editable: true,
    },
    {
      title: 'Marks',
      dataIndex: 'marks',
      editable: true,
    },
  ];

  const ifFieldExists = (item, field) => {
    if (!(field in item)) return true;
    if (
      (item[field] && item[field].includes('Enter')) ||
      item[field] === 'undefined'
    )
      return false;
    return true;
  };

  const onChangeValues = data => {
    const dataToSave = [];
    if (data.length > 0) {
      data.forEach(item => {
        const updatedItem = { ...item };
        Object.keys(item).forEach(element => {
          if (item[element] && item[element].toString().includes('Enter')) {
            updatedItem[element] = '';
          }
        });
        if (!isEmpty(_.omit(updatedItem, ['key']))) {
          dataToSave.push(updatedItem);
        }
      });
    }
    // let dataToSave = [];
    // if (data.length > 0) {
    //   dataToSave = data
    //     .filter(
    //       item =>
    //         ifFieldExists(item, 'qualification') &&
    //         ifFieldExists(item, 'institute') &&
    //         ifFieldExists(item, 'yearOfGraduation') &&
    //         ifFieldExists(item, 'major') &&
    //         ifFieldExists(item, 'marks'),
    //     )
    //     .map(item => {
    //       if (item.id)
    //         return {
    //           qualification: item.qualification,
    //           institute: item.institute,
    //           yearOfGraduation: item.yearOfGraduation,
    //           major: item.major,
    //           marks: item.marks,
    //           id: item.id,
    //         };

    //       return {
    //         qualification: item.qualification,
    //         institute: item.institute,
    //         yearOfGraduation: item.yearOfGraduation,
    //         major: item.major,
    //         marks: item.marks,
    //       };
    //     });
    //   if (dataToSave.length > 0 && dataToSave.length === data.length) {
    //     setEducation([...dataToSave]);
    //     // setValue(`applicants[${userIndex}].userEducations`, dataToSave);
    //   }
    // }
    if (isGuarantor)
      setValue(`guarantors[${userIndex}].userEducations`, dataToSave);
    else setValue(`applicants[${userIndex}].userEducations`, dataToSave);
  };

  return (
    <EditableTable
      initDataSource={initDataSource}
      initColumns={initColumns}
      columnsGiven
      onChangeValues={onChangeValues}
      notShowDeleteBtn
      addTitle="Add Education"
      disabled={disabled}
    />
  );
}

export default Education;
