/**
 *
 * RelationshipsComponent
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import { Empty, Menu, Descriptions, Spin, Button } from 'antd';
import moment from 'moment';

import {
  setSelectedRelationships,
  setMainUser,
} from '../../containers/RelationshipDetails/actions';
import DedupeAddressCollapse from 'components/DedupeAddressCollapse';

function RelationshipsComponent({
  loading,
  user,
  cuid,
  coApplicantData,
  selectedCoApplicant,
  dispatch,
}) {
  let contactDetails = {};
  let contactabilities = _.orderBy(
    selectedCoApplicant.contactibilities,
    ['id'],
    ['desc'],
  );
  const setRelationship = id => {
    const applicant =
      _.find(coApplicantData, { cuid: id.key }) ||
      _.find(coApplicantData, { cuid: parseInt(id.key) });
    if (applicant) dispatch(setSelectedRelationships(applicant));
  };

  if (selectedCoApplicant && selectedCoApplicant.cuid) {
    let arr =
      selectedCoApplicant.contactibilities &&
      selectedCoApplicant.contactibilities.filter(item => item.addressLine1);
    contactDetails = _.mapValues(
      _.groupBy(
        _.orderBy(arr, ['id'], ['desc']),
        'contactType',
      ),
      clist => clist.map(contact => _.omit(contact, 'contactType')),
    );
  }

  const relationship = selectedCoApplicant
    ? _.orderBy(
        _.filter(_.get(user, 'entityOfficers') || [], {
          belongsTo: selectedCoApplicant.cuid,
        }),
        'id',
        'desc',
      )[0]
    : {};

  const handleClick = () => {
    dispatch(setMainUser(selectedCoApplicant.cuid));
  };

  return (
    <Spin spinning={loading} tip="Loading...">
      <Menu
        onClick={setRelationship}
        defaultSelectedKeys={[(selectedCoApplicant || {}).cuid]}
        selectedKeys={[String((selectedCoApplicant || {}).cuid)]}
        mode="horizontal"
        style={{ width: '95%' }}
      >
        {_.map(coApplicantData, (applicant, index) => (
          <Menu.Item key={String(applicant.cuid)}>{`Relationship ${index +
            1}`}</Menu.Item>
        ))}
      </Menu>
      {selectedCoApplicant ? (
        <>
          <Descriptions
            title={`Basic Information`}
            column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
            bordered
            layout="vertical"
            size="small"
          >
            {selectedCoApplicant &&
            selectedCoApplicant.type === 'INDIVIDUAL' ? (
              <>
                <Descriptions.Item label="First Name">
                  {selectedCoApplicant && selectedCoApplicant.firstName}
                </Descriptions.Item>
                <Descriptions.Item label="Last Name">
                  {selectedCoApplicant && selectedCoApplicant.lastName}
                </Descriptions.Item>
              </>
            ) : (
              <Descriptions.Item label="Registered Name">
                {selectedCoApplicant && selectedCoApplicant.registeredName}
              </Descriptions.Item>
            )}
            <Descriptions.Item label="Email">
              {selectedCoApplicant && selectedCoApplicant.preferredEmail
                ? selectedCoApplicant.preferredEmail
                : contactabilities.length > 0
                ? contactabilities[0].email
                : ''}
            </Descriptions.Item>
            <Descriptions.Item label="Phone Number">
              {selectedCoApplicant && selectedCoApplicant.preferredPhone
                ? user.preferredPhone
                : contactabilities.length > 0
                ? contactabilities[0].phoneNumber
                : ''}
            </Descriptions.Item>
            <Descriptions.Item label="DOB/DOI">
              {selectedCoApplicant &&
                moment(selectedCoApplicant.dateOfBirthIncorporation).format(
                  'DD-MM-YYYY',
                )}
            </Descriptions.Item>
            <Descriptions.Item label="Role/Relation with Borrower Entity">
              {relationship ? relationship.designation : ''}
            </Descriptions.Item>
          </Descriptions>
          <Descriptions
            title={`Identities`}
            column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
            bordered
            layout="vertical"
            size="small"
          >
            <Descriptions.Item label="PAN">
              {selectedCoApplicant &&
                selectedCoApplicant.identities &&
                selectedCoApplicant.identities.pan}
            </Descriptions.Item>
            {selectedCoApplicant &&
            selectedCoApplicant.type === 'INDIVIDUAL' ? (
              <>
                {selectedCoApplicant &&
                selectedCoApplicant.identities &&
                selectedCoApplicant.identities.aadhar ? (
                  <Descriptions.Item label="AADHAR Number">
                    {selectedCoApplicant.identities.aadhar}
                  </Descriptions.Item>
                ) : null}
                {selectedCoApplicant &&
                selectedCoApplicant.identities &&
                selectedCoApplicant.identities.voterId ? (
                  <Descriptions.Item label="Voter ID">
                    {selectedCoApplicant.identities.voterId}
                  </Descriptions.Item>
                ) : null}
                {selectedCoApplicant &&
                selectedCoApplicant.identities &&
                selectedCoApplicant.identities.ckycNumber ? (
                  <Descriptions.Item label="CKYC Number">
                    {selectedCoApplicant.identities.ckycNumber}
                  </Descriptions.Item>
                ) : null}
                {selectedCoApplicant &&
                selectedCoApplicant.identities &&
                selectedCoApplicant.identities.drivingLicense ? (
                  <Descriptions.Item label="Driving License">
                    {selectedCoApplicant.identities.drivingLicense}
                  </Descriptions.Item>
                ) : null}
                {selectedCoApplicant &&
                selectedCoApplicant.identities &&
                selectedCoApplicant.identities.passport ? (
                  <Descriptions.Item label="Passport">
                    {selectedCoApplicant.identities.passport}
                  </Descriptions.Item>
                ) : null}
              </>
            ) : (
              <>
                <Descriptions.Item label="CIN">
                  {selectedCoApplicant &&
                    selectedCoApplicant.identities &&
                    selectedCoApplicant.identities.cin}
                </Descriptions.Item>
                <Descriptions.Item label="TAN">
                  {selectedCoApplicant &&
                    selectedCoApplicant.identities &&
                    selectedCoApplicant.identities.tan}
                </Descriptions.Item>
              </>
            )}
          </Descriptions>
          <Descriptions
            title={`All Addresses`}
            column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
          />
          <DedupeAddressCollapse contactDetails={contactDetails} />
          <Button onClick={handleClick} type="primary">
            View all details
          </Button>
        </>
      ) : (
        <Empty />
      )}
    </Spin>
  );
}

RelationshipsComponent.propTypes = {};

export default memo(RelationshipsComponent);
