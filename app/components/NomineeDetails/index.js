import React, { useState, memo, useEffect } from 'react';
import moment from 'moment';
import { Row, Col, message } from 'antd';
import _ from 'lodash';
import StyledTitle from 'components/StyledTitle';
import InputField from 'components/InputField';
import { columnResponsiveLayout } from '../../utils/constants';
import { mobilePattern } from '../../containers/App/constants';

const RELATION = [
  'Aunt',
  'Brother in law',
  'Daughter',
  'Father',
  'Grand Father',
  'Grand Mother',
  'Grand Son',
  'Husband',
  'Mother',
  'Niece',
  'Sister',
  'Wife',
  'Son',
  'Uncle',
  'Brother',
  'Grand Daughter',
  'Nephew',
  'Sister in law',
];

function NomineeDetails({
  control,
  errors,
  values,
  applicantDetails,
  selectedInsurance,
}) {
  const fieldDisabled = true;
  const [showAppointee, setShowAppointee] = useState(false);
  const onDOBChange = (value, type) => {
    if (!value) return value;
    const ageDiff = moment().diff(moment(value, 'DD-MM-YYYY'), 'years');
    if (ageDiff < 18) {
      if (type === 'nominee1') setShowAppointee(true);
      else {
        message.info(
          "Appointee's age should be greater than or equal to 18years !",
        );
        return '';
      }
    } else if (type === 'nominee1') setShowAppointee(false);
    return value;
  };

  useEffect(() => {
    const dob = _.get(selectedInsurance, 'nominee1_DateOfBirth');
    if (dob && moment(dob)) {
      onDOBChange(moment(dob), 'nominee1');
    }
  }, [_.get(selectedInsurance, 'nominee1_DateOfBirth')]);

  useEffect(() => {
    if (_.get(selectedInsurance, 'nominee2_firstName')) setShowAppointee(true);
    else setShowAppointee(false);
  }, [selectedInsurance]);

  return (
    <>
      <StyledTitle>Nominee Details</StyledTitle>
      <Row gutter={[16, 16]}>
        <Col {...columnResponsiveLayout.colOneThird}>
          <InputField
            id="appInsurances[0].nominee1_firstName"
            control={control}
            name="appInsurances[0].nominee1_firstName"
            type="string"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Nominee Name"
            labelHtml="Nominee Name"
            disabled={fieldDisabled}
          />
        </Col>
        <Col {...columnResponsiveLayout.colOneThird}>
          <InputField
            id="appInsurances[0].nominee1_nomineeRelation"
            control={control}
            name="appInsurances[0].nominee1_nomineeRelation"
            type="select"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            labelHtml="Nominee Relation"
            options={RELATION}
            disabled={fieldDisabled}
          />
        </Col>
        <Col {...columnResponsiveLayout.colOneThird}>
          <InputField
            id="appInsurances[0].nominee1_DateOfBirth"
            control={control}
            name="appInsurances[0].nominee1_DateOfBirth"
            type="datepicker"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            handleChange={e => onDOBChange(e, 'nominee1')}
            errors={errors}
            placeholder="Nominee Date Of Birth"
            labelHtml="Nominee Date Of Birth*"
            disabled={fieldDisabled}
          />
        </Col>
        <Col {...columnResponsiveLayout.colOneThird}>
          <InputField
            id="appInsurances[0].nominee1_Gender"
            control={control}
            name="appInsurances[0].nominee1_Gender"
            type="select"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            labelHtml="Nominee Gender"
            options={['Male', 'Female', 'Others']}
            disabled={fieldDisabled}
          />
        </Col>
        <Col {...columnResponsiveLayout.colOneThird}>
          <InputField
            id="appInsurances[0].nominee1_mobile"
            control={control}
            name="appInsurances[0].nominee1_mobile"
            type="string"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              pattern: mobilePattern,
            }}
            errors={errors}
            placeholder="Nominee Mobile"
            labelHtml="Nominee Mobile"
            disabled={fieldDisabled}
          />
        </Col>
      </Row>
      {showAppointee && (
        <>
          <StyledTitle>Appointee Details</StyledTitle>
          <Row gutter={[16, 16]}>
            <Col {...columnResponsiveLayout.colOneThird}>
              <InputField
                id="appInsurances[0]nominee2_firstName"
                control={control}
                name="appInsurances[0].nominee2_firstName"
                type="string"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Appointee Name"
                labelHtml="Appointee Name"
                disabled={fieldDisabled}
              />
            </Col>
            <Col {...columnResponsiveLayout.colOneThird}>
              <InputField
                id="appInsurances[0].nominee2_nomineeRelation"
                control={control}
                name="appInsurances[0].nominee2_nomineeRelation"
                type="select"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                labelHtml="Appointee Relation"
                options={RELATION}
                disabled={fieldDisabled}
              />
            </Col>
            <Col {...columnResponsiveLayout.colOneThird}>
              <InputField
                id="appInsurances[0].nominee2_DateOfBirth"
                control={control}
                name="appInsurances[0].nominee2_DateOfBirth"
                handleChange={e => onDOBChange(e, 'nominee2')}
                type="datepicker"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Appointee Date Of Birth"
                labelHtml="Appointee Date Of Birth*"
                disabled={fieldDisabled}
              />
            </Col>
            <Col {...columnResponsiveLayout.colOneThird}>
              <InputField
                id="appInsurances[0].nominee2_Gender"
                control={control}
                name="appInsurances[0].nominee2_Gender"
                type="select"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                labelHtml="Appointee Gender"
                options={['Male', 'Female', 'Others']}
                disabled={fieldDisabled}
              />
            </Col>
            <Col {...columnResponsiveLayout.colOneThird}>
              <InputField
                id="appInsurances[0].nominee2_mobile"
                control={control}
                name="appInsurances[0].nominee2_mobile"
                type="string"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                  pattern: mobilePattern,
                }}
                errors={errors}
                placeholder="Appointee Mobile"
                labelHtml="Appointee Mobile"
                disabled={fieldDisabled}
              />
            </Col>
          </Row>
        </>
      )}
    </>
  );
}

export default memo(NomineeDetails);
