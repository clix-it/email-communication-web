/**
 *
 * Input
 *
 */

import _, { valuesIn } from 'lodash';
import React, { memo } from 'react';
import { Form, Input, InputNumber, Select, Radio, DatePicker } from 'antd';
import ReactQuill from 'react-quill';
import { Controller } from 'react-hook-form';
import { Link, withRouter } from 'react-router-dom';
import ErrorMessage from '../ErrorMessage/index';
import { ENABLED_FIELDS_GAM } from '../../utils/constants';
import { useState } from 'react';
// import TextArea from 'antd/lib/input/TextArea';
import 'react-quill/dist/quill.snow.css'
//import { Editor } from "react-draft-wysiwyg";
// import RichTextEditor from 'react-rte';

// import SunEditor from 'suneditor-react';
// import 'suneditor/dist/css/suneditor.min.css';

 import {Editor, EditorState} from 'draft-js';


  

const { TextArea } = Input;
const defaults = {
  populatedClass: 'populated',
  focusedClass: 'focused',
};
const InputField = ({
  name,
  register,
  wrapperClassname,
  prefix,
  postfix,
  labelHtml,
  errors,
  placeholder,
  onBlurAction,
  rules,
  disabled,
  type,
  options,
  control,
  handleChange,
  isAddressSelect,
  ...rest
}) => {
  const array = [
    'applicantDetailsPma',
    'dedudeListpma',
    'postSanctionpma',
    'employmentReviewpma',
    'incomeReviewpma',
    'addEmail',
    'emailDetails',
    'addTemplate',
    'templateUpdate',
    'sendEmail'
  ];

  const address = location.pathname.split('/');

  let disabled2 = disabled || !array.includes(address[1]);

  if (
    _.get(address, '[1]', '') === 'gamReviewpma' &&
    name &&
    ENABLED_FIELDS_GAM.find(
      field => name.includes('asset') || name.includes(field),
    )
  ) {
    disabled2 = false;
  }
  let rules2 = rules;
  if (disabled2) {
    rules2 = {};
  }
  // console.log(address[1], 'pathnameee');
  const handleChange2 = value => {
    if (handleChange) handleChange(value);
    return value;
    // return Array.isArray(value) ? _.last(value) : value;
  };
  const handleChange3 = value => {
    if (handleChange) handleChange(value);
    return value;
    // return Array.isArray(value) ? _.last(value) : value;
  };

  if (type == 'string') {
    return (
      <>
        <Form.Item label={labelHtml}>
          <Controller
            disabled={disabled2}
            name={name}
            // onFocus={handleFocus}
            onBlur={([e]) => {
              if (onBlurAction) onBlurAction(e);
              return e.target.value;
            }}
            onChange={([e]) => {
              if (handleChange) handleChange(e);
              return e.target.value;
            }}
            control={control}
            rules={rules2}
            {...rest}
            as={<Input disabled={disabled2} size="large" />}
          />
          <ErrorMessage name={name} errors={errors} />
        </Form.Item>

        {/* <Input tabIndex={0} ref={register} name={name} {...rest} /> */}
      </>
    );
  }
  if (type == 'datepicker') {
    return (
      <Form.Item label={labelHtml}>
        <Controller
          disabled={disabled2}
          name={name}
          control={control}
          rules={rules2}
          style={{ width: '100%' }}
          {...rest}
          onChange={([dateString]) => handleChange2(dateString)}
          as={<DatePicker format="DD-MM-YYYY" size="large" />}
        />
        <ErrorMessage name={name} errors={errors} />
      </Form.Item>
    );
  }
  if (type == 'select') {
    return (
      <Form.Item label={labelHtml}>
        <Controller
          disabled={disabled2}
          name={name}
          control={control}
          rules={rules2}
          {...rest}
          showSearch
          onChange={([value]) => handleChange2(value)}
          as={
            <Select
              disabled={disabled2}
              size="large"
              placeholder={placeholder || ''}
            >
              {!isAddressSelect
                ? (options || []).map((item, index) => (
                    <Option value={item}>{item}</Option>
                  ))
                : (options || []).map(option => (
                    <Option key={option.code} value={option.code}>
                      {option.value}
                    </Option>
                  ))}
            </Select>
          }
        />
        <ErrorMessage name={name} errors={errors} />
      </Form.Item>
    );
  }
  if (type == 'number') {
    return (
      <Form.Item label={labelHtml}>
        <Controller
          disabled={disabled2}
          name={name}
          onChange={([e]) => {
            if (handleChange) handleChange(e);
            return e || e.target.value;
          }}
          // onFocus={handleFocus}
          // onBlur={handleBlur}
          control={control}
          rules={rules2}
          {...rest}
          as={<InputNumber disabled={disabled2} size="large" />}
        />
        <ErrorMessage name={name} errors={errors} />
      </Form.Item>
    );
  }

  if (type == 'radio') {
    return (
      <Form.Item label={labelHtml}>
        <Controller
          disabled="true"
          name={name}
          control={control}
          rules={rules2}
          {...rest}
          as={
            <Radio.Group
              name={name}
              value={!!(rest.value || (rest.values && rest.values[name]))}
            >
              <Radio value>Yes</Radio>
              <Radio value={false}>No</Radio>
            </Radio.Group>
          }
        />
      </Form.Item>
    );
  }
  if (type == 'hidden') {
    return (
      <Form.Item label={labelHtml}>
        <Controller
          disabled="true"
          name={name}
          control={control}
          rules={rules2}
          {...rest}
          as={<Input type="hidden" disabled={disabled2} size="large" />}
        />
      </Form.Item>
    );
  }

  return (
    <Form.Item label={labelHtml}>
      <Controller
        disabled={disabled2}
        name={name}
        onChange={([e]) => {
          if (handleChange) handleChange(e);
          return e.target.value;
        }}
        // onFocus={handleFocus}
        // onBlur={handleBlur}
        control={control}
        rules={rules2}
        {...rest}
        as={<TextArea disabled={disabled2} rows={4} />}
      />
      <ErrorMessage name={name} errors={errors} />
    </Form.Item>
  );
};

InputField.propTypes = {};

// export default InputField;
export default withRouter(memo(InputField));

{
  /* <Form.Item
              // type="number"
              label="Final Loan Amount"
            >
              <Controller
                name="finalLoanAmount"
                onChange={([e]) => {
                  console.log(e);
                  return e;
                }}
                control={control}
                rules={{
                  required: true,
                  maxLength: {
                    value: 100,
                    message: 'Max length 100 chracters allowed',
                  },
                  pattern: {
                    value: /[0-9]/i,
                    message: 'Please enter valid number',
                  },
                }}
                as={<InputNumber size="large" />}
              />
              {errors.finalLoanAmount && (
                <p style={{ color: 'red', fontSize: '15px' }}>
                  {errors.finalLoanAmount.message
                    ? errors.finalLoanAmount.message
                    : 'This Field should not be empty!'}
                </p>
              )}
            </Form.Item> */
}


