/**
 *
 * DedupeMatchForm
 *
 */

import _ from 'lodash';
import React from 'react';
import { Row, Col, Divider, Typography } from 'antd';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

const { Text } = Typography;

function DedupeMatchForm({ defaultValues }) {
  return (
    <>
      {defaultValues.type === 'COMPANY' &&
      (_.get(defaultValues, 'appLMS.role') === 'Applicant' ||
        !_.get(defaultValues, 'appLMS.role')) ? (
        <Row gutter={[16, 16]}>
          <Col {...responsiveColumns}>
            <Text strong>Entity Name : </Text>
            <Text>{defaultValues.registeredName}</Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>Existing Constitution : </Text>
            <Text>{defaultValues.corporateStructure}</Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>PAN : </Text>
            <Text>
              {defaultValues.identities
                ? defaultValues.identities.pan || ''
                : ''}
            </Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>GSTN : </Text>
            <Text>{_.get(defaultValues, 'gsts[0].gstn') || ''}</Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>Corporate Identity Number (CIN) : </Text>
            <Text>
              {defaultValues.identities ? defaultValues.identities.cin : ''}
            </Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>Nature Of Business : </Text>
            <Text>
              {defaultValues.userDetails
                ? defaultValues.userDetails.natureOfBusiness
                : ''}
            </Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>Industry : </Text>
            <Text>
              {defaultValues.userDetails
                ? defaultValues.userDetails.industry
                : ''}
            </Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>Segment : </Text>
            <Text>
              {defaultValues.userDetails
                ? defaultValues.userDetails.segment
                : ''}
            </Text>
          </Col>
          <Col {...responsiveColumns}>
            <Text strong>Product : </Text>
            <Text>
              {defaultValues.userDetails
                ? defaultValues.userDetails.product
                : ''}
            </Text>
          </Col>
        </Row>
      ) : null}
      <Divider />
      <div
        style={{
          fontSize: '18px',
          fontWeight: '600',
          marginBottom: '15px',
        }}
      >
        Contact Details
      </div>
      <Row gutter={[16, 16]}>
        <Col {...responsiveColumns}>
          <Text strong>First Name : </Text>
          <Text>{defaultValues.firstName}</Text>
        </Col>
        <Col {...responsiveColumns}>
          <Text strong>Middle Name : </Text>
          <Text>{defaultValues.middleName}</Text>
        </Col>
        <Col {...responsiveColumns}>
          <Text strong>Last Name : </Text>
          <Text>{defaultValues.lastName}</Text>
        </Col>
        <Col {...responsiveColumns}>
          <Text strong>Gender : </Text>
          <Text>
            {defaultValues.userDetails ? defaultValues.userDetails.gender : ''}
          </Text>
        </Col>
        <Col {...responsiveColumns}>
          <Text strong>Co-Applicant Mobile Number : </Text>
          <Text>
            {defaultValues.preferredPhone
              ? defaultValues.preferredPhone
              : defaultValues.contactibilities
              ? defaultValues.contactibilities[0].phoneNumber
              : ''}
          </Text>
        </Col>
        <Col {...responsiveColumns}>
          <Text strong>Co-Applicant Email : </Text>
          <Text>
            {defaultValues.preferredEmail
              ? defaultValues.preferredEmail
              : defaultValues.contactibilities
              ? defaultValues.contactibilities[0].email
              : ''}
          </Text>
        </Col>
        <Col {...responsiveColumns}>
          <Text strong>Date Of Birth : </Text>
          <Text>{defaultValues.dateOfBirthIncorporation}</Text>
        </Col>
        <Col {...responsiveColumns}>
          <Text strong>PAN : </Text>
          <Text>
            {defaultValues.identities ? defaultValues.identities.pan || '' : ''}
          </Text>
        </Col>
      </Row>
    </>
  );
}

DedupeMatchForm.propTypes = {};

export default DedupeMatchForm;
