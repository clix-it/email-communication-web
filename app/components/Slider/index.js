/**
 *
 * Slider
 *
 */

import React, { memo, useState } from 'react';
import PropTypes from 'prop-types';

import { Link, withRouter } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { PieChartOutlined } from '@ant-design/icons';

import './sliderStyle.css';

const { Sider } = Layout;
const { SubMenu } = Menu;

function Slider({ location }) {
  const menuLoc = location.pathname.split('/');
  const [openKey, setOpenKey] = useState(['credit']);
  const rootSubmenuKeys = [
    'credit',
    'dedupeList',
    'psv',
    'doa',
    'hunter',
    'employment',
    'income',
    'fraud',
    'gamReview',
    'customer360',
  ];

  const onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(key => openKey.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKey([openKeys]);
    } else {
      setOpenKey(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  return (
    <Sider
      style={{
        flex: '0 0 256px',
        maxWidth: '256px',
        minWidth: '256px',
        width: '256px',
      }}
      breakpoint="lg"
      collapsedWidth="0"
      collapsible
      className="siderStyle"
    >
      <div className="logo" />
      <Menu
        theme="dark"
        selectedKeys={[menuLoc[1]]}
        onOpenChange={onOpenChange}
        openKeys={openKey}
        mode="inline"
      >
        <SubMenu
          key="credit"
          icon={<PieChartOutlined />}
          title="Email Master"
        >
          <Menu.Item key="emailDetails">
            Emails
            <Link
              style={{ color: 'white' }}
              to="/emailDetails"
              className="nav-text"
            />
          </Menu.Item>
          {/* <Menu.Item key="applicantDetailsPma">
            Create Email
            <Link
              style={{ color: 'white' }}
              to="/applicantDetailsPma"
              className="nav-text"
            >
              {' '}
            </Link>
          </Menu.Item> */}
          {/* <Menu.Item key="applicantDetailsClo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/applicantDetailsClo"
              className="nav-text"
            />
          </Menu.Item> */}
        </SubMenu>

        <SubMenu key="dedupeList" icon={<PieChartOutlined />} title="Template Master">
          <Menu.Item key="templateDetails">
            Templates
            <Link
              style={{ color: 'white' }}
              to="/templateDetails"
              className="nav-text"
            />
          </Menu.Item>
          
        </SubMenu>
        <SubMenu key="psv" icon={<PieChartOutlined />} title="Send Email">
          <Menu.Item key="sendEmail">
            Send Email
            <Link
              style={{ color: 'white' }}
              to="/sendEmail"
              className="nav-text"
            />
          </Menu.Item>
          </SubMenu>
        {/* <SubMenu
          key="psv"
          icon={<PieChartOutlined />}
          title="Post Sanctioned Verification"
        >
          <Menu.Item key="postSanctionpma">
            Pending With Me
            <Link
              style={{ color: 'white' }}
              to="/postSanctionpma"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="postSanctionpoa">
            Pending With Others
            <Link
              style={{ color: 'white' }}
              to="/postSanctionpoa"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="postSanctionclo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/postSanctionclo"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu> */}

        {/* Already commented */}
        {/* <Menu.Item key="dedudeList" icon={<PieChartOutlined />}>
          Dedupe
          <Link
            style={{ color: 'white' }}
            to="/dedudeList"
            className="nav-text"
          >
            {' '}
          </Link>
        </Menu.Item> */}

        
        {/* <SubMenu key="doa" icon={<PieChartOutlined />} title="DOA Approval">
          <Menu.Item key="doaApprovalpma">
            Pending With Me
            <Link
              style={{ color: 'white' }}
              to="/doaApprovalpma"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="doaApprovalpoa">
            Pending With Others
            <Link
              style={{ color: 'white' }}
              to="/doaApprovalpoa"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="doaApprovalclo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/doaApprovalclo"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu> */}

        {/* <SubMenu key="hunter" icon={<PieChartOutlined />} title="Hunter">
          <Menu.Item key="hunterReviewpma">
            Pending With Me
            <Link
              style={{ color: 'white' }}
              to="/hunterReviewpma"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="hunterReviewpoa">
            Pending With Others
            <Link
              style={{ color: 'white' }}
              to="/hunterReviewpoa"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="hunterReviewclo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/hunterReviewclo"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu> */}
        {/* <SubMenu
          key="employment"
          icon={<PieChartOutlined />}
          title="Employment"
        >
          <Menu.Item key="employmentReviewpma">
            Pending With Me
            <Link
              style={{ color: 'white' }}
              to="/employmentReviewpma"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="employmentReviewpoa">
            Pending With Others
            <Link
              style={{ color: 'white' }}
              to="/employmentReviewpoa"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="employmentReviewclo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/employmentReviewclo"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu>
        <SubMenu key="income" icon={<PieChartOutlined />} title="Income">
          <Menu.Item key="incomeReviewpma">
            Pending With Me
            <Link
              style={{ color: 'white' }}
              to="/incomeReviewpma"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="incomeReviewpoa">
            Pending With Others
            <Link
              style={{ color: 'white' }}
              to="/incomeReviewpoa"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="incomeReviewclo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/incomeReviewclo"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu>
        <SubMenu key="fraud" icon={<PieChartOutlined />} title="Fraud">
          <Menu.Item key="fraudReviewpma">
            Pending With Me
            <Link
              style={{ color: 'white' }}
              to="/fraudReviewpma"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="fraudReviewpoa">
            Pending With Others
            <Link
              style={{ color: 'white' }}
              to="/fraudReviewpoa"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="fraudReviewclo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/fraudReviewclo"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu>
        <SubMenu key="gamReview" icon={<PieChartOutlined />} title="GAM">
          <Menu.Item key="gamReviewpma">
            Pending With Me
            <Link
              style={{ color: 'white' }}
              to="/gamReviewpma"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="gamReviewpoa">
            Pending With Others
            <Link
              style={{ color: 'white' }}
              to="/gamReviewpoa"
              className="nav-text"
            />
          </Menu.Item>
          <Menu.Item key="gamReviewclo">
            Closed
            <Link
              style={{ color: 'white' }}
              to="/gamReviewclo"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu>
        <SubMenu
          key="customer360"
          icon={<PieChartOutlined />}
          title="Customer 360"
        >
          <Menu.Item key="customerDetails">
            Customer Details
            <Link
              style={{ color: 'white' }}
              to="/customerDetails"
              className="nav-text"
            />
          </Menu.Item>
        </SubMenu> */}
      </Menu>
    </Sider>
  );
}

Slider.propTypes = {
  // click: PropTypes.func.isRequired,
};

export default withRouter(memo(Slider));
