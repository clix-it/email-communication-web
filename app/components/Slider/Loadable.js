/**
 *
 * Asynchronously loads the component for Slider
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
