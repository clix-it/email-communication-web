/**
 *
 * Asynchronously loads the component for CustomerKycTabComponent
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
