/**
 *
 * CustomerKycTabComponent
 *
 */

import React, { memo, useEffect, useState } from 'react';
import Tabs from '../Tabs';
import CustomerKycDetails from '../../containers/CustomerKycDetails';

function CustomerKycTabComponent({ user, cuid }) {
  const kycTabPanel = [
    {
      id: 'ckyc',
      name: 'CKYC Details',
      component: <CustomerKycDetails user={user} cuid={cuid} kycType="CKYC" />,
    },
    {
      id: 'okyc',
      name: 'OKYC Details',
      component: <CustomerKycDetails user={user} cuid={cuid} kycType="OKYC" />,
    },
  ]

  return <Tabs mode="top" ifSendBack={false} tabPanel={kycTabPanel} />;
}

CustomerKycTabComponent.propTypes = {};

export default memo(CustomerKycTabComponent);
