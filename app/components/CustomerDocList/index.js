/**
 *
 * CustomerDocList
 *
 */

import React, { memo, useState, useEffect } from 'react';
import styled from 'styled-components';
import { List, Avatar, Divider, Button, Typography, Space, Spin } from 'antd';
import docSvg from '../../images/document.svg';
import DrawerComponent from '../DrawerComponent';
import { fetchDocs } from '../../containers/CustomerDocuments/actions';

const Wrapper = styled.section`
  background: #fff;
  margin: 0 1em;
  padding: 0.25em 1em;
`;

const ListWrapper = styled.div`
  height: 300px;
  overflow: auto;
`;

function CustomerDocList({
  dispatch,
  customerDocuments,
  user,
  cuid,
}) {
  const [visible, setVisible] = useState(false);
  const [currentItem, setCurrentItem] = useState({});
  const [existingDocs, setExistingDocs] = useState([]);
  const { Title, Text } = Typography;
  const { docsData = [], loading = false } = customerDocuments;

  useEffect(() => {
    if (cuid) {
      dispatch(fetchDocs(cuid));
    }
    return () => {
      setExistingDocs([]);
    };
  }, [cuid]);


  useEffect(() => {
    if (docsData.length > 0) {
      setExistingDocs(docsData);
    }
  }, [docsData, cuid]);

  const showDrawer = (e, item) => {
    setCurrentItem(item);
    setVisible(true);
  };

  const onClose = () => {
    setCurrentItem({});
    setVisible(false);
  };

  const setDescription = item => {
    if (item) {
      return (
        <Space>
          <Text>Cuid:</Text>
          <Text>{user.cuid}</Text>
          <Text>Name:</Text>
          <Text>
            {user.type === 'INDIVIDUAL'
              ? `${user.firstName} ${user.lastName}`
              : user.registeredName}
          </Text>
        </Space>
      );
      return <Text>App</Text>;
    }
    return <Text>App</Text>;
  };

  function download(filename, text, mimeType) {
    var element = document.createElement('a');
    element.setAttribute(
      'href',
      'data:' + mimeType + ';base64,' + encodeURIComponent(text),
    );
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  return (
    <Spin spinning={loading} tip={'Loading...'}>
      <Wrapper>
        <DrawerComponent
          currentItem={currentItem}
          visible={visible}
          onClose={onClose}
        />
        <Space>
          <Title level={3}>Documents</Title>
        </Space>

        <Divider />
        <ListWrapper>
          <List
            className="demo-loadmore-list"
            itemLayout="horizontal"
            size="small"
            dataSource={existingDocs}
            renderItem={(item, index) => (
              <>
                <List.Item
                  actions={[
                    <Button
                      type="link"
                      key={index}
                      onClick={event => showDrawer(event, item)}
                    >
                      View
                    </Button>,

                    <a key={index} href={item.signedUrl} target="_blank">
                      Download
                    </a>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar shape="square" src={docSvg} />}
                    title={<Text>{item.type}</Text>}
                    description={setDescription(item)}
                  />
                </List.Item>
              </>
            )}
          />
        </ListWrapper>
        <Divider />
      </Wrapper>
    </Spin>
  );
}

CustomerDocList.propTypes = {};

export default memo(CustomerDocList);
