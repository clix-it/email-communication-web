/**
 *
 * BankAnalysisComponent
 *
 */

import _ from 'lodash';
import React, { memo, useState, useEffect } from 'react';
import { Spin, Descriptions, Divider, Table, Empty } from 'antd';
// import PropTypes from 'prop-types';
import styled from 'styled-components';

const Title = styled.div`
  font-size: 16px;
  font-weight: bold;
  line-height: 1.5715;
  margin-top: 20px;
  margin-bottom: 20px;
  color: rgba(0, 0, 0, 0.85);
`;

const NoData = styled.div`
  text-align: center;
  font-size: large;
  font-weight: bold;
`;

function BankAnalysisComponent({ selectedReport, loading, error }) {
  const [monthlyDetailsCols, setMonthlyDetailsCols] = useState([]);
  const [eODBalancesCols, setEODBalancesCols] = useState([]);
  const [top5FundsReceivedCols, setTop5FundsReceivedCols] = useState([]);
  const [eMILOANXnsCols, setEMILOANXnsCols] = useState([]);
  const [loanDisbursalXnsCols, setLoanDisbursalXnsCols] = useState([]);
  const [eMIECSXnsCols, seteMIECSXnsCols] = useState([]);
  const [regularDebitsCols, setRegularDebitsCols] = useState([]);
  const [bouncedOrPenalXnsCols, setBouncedOrPenalXnsCols] = useState([]);
  const [monthlySummarydetailsCols, setMonthlySummarydetailsCols] = useState(
    [],
  );
  const [
    additionalMonthlyDetailsCols,
    setAdditionalMonthlyDetailsCols,
  ] = useState([]);
  const [additionalCustomerInfoCols, setAdditionalCustomerInfoCols] = useState(
    [],
  );
  const [top5FundsTransferredCols, setTop5FundsTransferredCols] = useState([]);
  const [
    AdditionalSummaryDetailsCols,
    setAdditionalSummaryDetailsCols,
  ] = useState([]);
  const [accountTxnsCols, setAccountTxnsCols] = useState([]);
  const [statementAccountsCols, setStatementAccountsCols] = useState([]);
  const [jsonCols, setJsonCols] = useState([]);

  const {
    analysisData: {
      monthlyDetails = [],
      fCUAnalysis = {},
      accountXns = [],
      statementdetails = [],
    } = {},
  } = selectedReport;

  const displayFCUValues = (obj, value) => {
    if (!obj) return null;
    if (obj[value] && Array.isArray(obj[value])) {
      const columns = [];
      Object.keys(obj[value][0]).forEach(col => {
        columns.push({
          title: col,
          dataIndex: col,
          ellipsis: true,
        });
      });
      return (
        <>
          <Table columns={columns} dataSource={obj[value]} />
        </>
      );
    }
    if (
      obj[value] &&
      typeof obj[value] === 'object' &&
      obj[value].constructor === Object
    ) {
      return (
        <Descriptions.Item
          label={_.startCase(_.lowerCase(_.replace(value, /_/g, ' ')))}
          span={3}
        >
          {Object.keys(obj[value]).map(ele => (
            <div>
              <span style={{ fontWeight: 'bold' }}>
                {_.startCase(_.lowerCase(_.replace(ele, /_/g, ' ')))} :
              </span>
              {displayFCUValues(obj[value], ele)}
            </div>
          ))}
        </Descriptions.Item>
      );
    }
    return (
      <Descriptions.Item
        label={_.startCase(_.lowerCase(_.replace(value, /_/g, ' ')))}
      >
        {String(obj[value])}
      </Descriptions.Item>
    );
  };

  /*const displayValues = (obj, value) => {
    if (!obj) return null;
    if (
      obj[value] &&
      typeof obj[value] === 'object' &&
      obj[value].constructor === Object &&
      !Array.isArray(obj[value])
    ) {
      //debugger;
      return (
        <Descriptions.Item
          label={_.startCase(_.lowerCase(_.replace(value, /_/g, ' ')))}
        >
          {Object.keys(obj[value]).map(ele => (
            <div>
              <span style={{ fontWeight: 'bold' }}>
                {_.startCase(_.lowerCase(_.replace(ele, /_/g, ' ')))} :
              </span>
              {displayValues(obj[value], ele)}
            </div>
          ))}
        </Descriptions.Item>
      );
    }
    return (
      <Descriptions.Item
        label={_.startCase(_.lowerCase(_.replace(value, /_/g, ' ')))}
      >
        {String(obj[value])}
      </Descriptions.Item>
    );
  };*/

  useEffect(() => {
    if (selectedReport && selectedReport.analysisData) {
      const colArr = [];
      Object.keys(selectedReport.analysisData).forEach(item => {
        if (
          selectedReport.analysisData[item] &&
          Array.isArray(selectedReport.analysisData[item])
        ) {
          const columns = [];
          Object.keys(selectedReport.analysisData[item][0]).forEach(col => {
            columns.push({
              title: col,
              dataIndex: col,
              ellipsis: true,
            });
          });
          //debugger;
          colArr.push({ [item]: columns });
        }
      });
      if (colArr && colArr.length > 0) {
        setJsonCols(colArr);
      }
    }
    if (accountXns && accountXns.length > 0) {
      const columns = [];
      Object.keys(accountXns[0].xns && accountXns[0].xns[0]).forEach(col => {
        columns.push({
          title: _.upperCase(col),
          dataIndex: col,
          ellipsis: true,
        });
      });
      setAccountTxnsCols(columns);
    }
    if (statementdetails && statementdetails.length > 0) {
      const columns = [];
      const index = _.findIndex(statementdetails, function(o) {
        return o.statementAccounts;
      });
      Object.keys(
        statementdetails[index].statementAccounts &&
          statementdetails[index].statementAccounts[0],
      ).forEach(col => {
        columns.push({
          title: _.upperCase(col),
          dataIndex: col,
          ellipsis: true,
        });
      });
      setStatementAccountsCols(columns);
    }
  }, [selectedReport]);
  return (
    <Spin spinning={loading} tip="Loading...">
      {error == '' ? (
        <>
          {selectedReport.analysisData &&
            Object.keys(selectedReport.analysisData).map(
              ele =>
                !Array.isArray(selectedReport.analysisData[ele]) &&
                typeof selectedReport.analysisData[ele] === 'object' && (
                  <>
                    <Descriptions
                      title={ele}
                      column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                    >
                      {selectedReport.analysisData[ele] &&
                        Object.keys(selectedReport.analysisData[ele]).map(
                          value =>
                            displayFCUValues(
                              selectedReport.analysisData[ele],
                              value,
                            ),
                        )}
                    </Descriptions>
                    <Divider />
                  </>
                ),
            )}
          <Divider />
          {statementdetails &&
            statementdetails.length > 0 &&
            statementdetails.map((item, index) => (
              <>
                <Descriptions
                  title={`Statement Details ${index + 1}`}
                  column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  size="small"
                  bordered
                  layout="vertical"
                >
                  {item &&
                    Object.keys(item).map(value =>
                      displayFCUValues(item, value),
                    )}
                </Descriptions>
                <Table
                  columns={statementAccountsCols}
                  dataSource={item.statementAccounts}
                />
              </>
            ))}
          {accountXns &&
            accountXns.length > 0 &&
            accountXns.map((item, index) => (
              <>
                <Descriptions
                  title={`Account Txn ${index + 1}`}
                  column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
                  size="small"
                  bordered
                  layout="vertical"
                >
                  <Descriptions.Item label="account No">
                    {item.accountNo}
                  </Descriptions.Item>
                  <Descriptions.Item label="account Type">
                    {item.accountType}
                  </Descriptions.Item>
                  <Descriptions.Item label="ifsc Code">
                    {item.ifscCode}
                  </Descriptions.Item>
                  <Descriptions.Item label="micr Code">
                    {item.micrCode}
                  </Descriptions.Item>
                  <Descriptions.Item label="location">
                    {item.location}
                  </Descriptions.Item>
                </Descriptions>
                <Table columns={accountTxnsCols} dataSource={item.xns} />
              </>
            ))}
          {monthlyDetails && monthlyDetails.length > 0 && (
            <Descriptions
              title={`Monthly Details`}
              column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              size="small"
              bordered
              layout="vertical"
            >
              {monthlyDetails.map((detail, index) => (
                <Descriptions.Item label={`Monthly Detail ${index + 1}`}>
                  {Object.keys(detail).map(item => {
                    return (
                      <>
                        {`${item}: ${detail[item]}`} <br />
                      </>
                    );
                  })}
                </Descriptions.Item>
              ))}
            </Descriptions>
          )}
          {jsonCols &&
            jsonCols.length > 0 &&
            jsonCols.map(item => {
              if (
                Object.keys(item)[0] != 'monthlyDetails' &&
                Object.keys(item)[0] != 'accountXns' &&
                Object.keys(item)[0] != 'statementdetails'
              ) {
                //debugger;
                return (
                  <>
                    <Title>{Object.keys(item)[0]}</Title>
                    <Table
                      columns={item[Object.keys(item)[0]]}
                      dataSource={
                        selectedReport.analysisData[Object.keys(item)[0]]
                      }
                    />
                  </>
                );
              }
            })}
        </>
      ) : (
        <Empty />
      )}
    </Spin>
  );
}

BankAnalysisComponent.propTypes = {};

export default memo(BankAnalysisComponent);
