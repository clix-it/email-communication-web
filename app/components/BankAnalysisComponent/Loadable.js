/**
 *
 * Asynchronously loads the component for BankAnalysisComponent
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
