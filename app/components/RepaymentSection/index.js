/**
 *
 * RepaymentSection
 *
 */

import React, { memo } from 'react';
// import { Grid, Typography, Divider } from '@material-ui/core';
// import PropTypes from 'prop-types';
// import styled from 'styled-components';
import './repaymentSection.css';
import repaymentImg from '../../images/business.svg';
import { formatDate } from '../LeadsRecord';

function RepaymentSection({ bankDetails, mandate }) {
  return (
    <>
      <img src={repaymentImg} style={{ maxHeight: 40, marginRight: 5 }} />
      <Typography
        className="repayment"
        variant="subtitle2"
        align="center"
        gutterBottom
      >
        Repayment Details
      </Typography>
      <Divider variant="middle" />
      <Grid container>
        {bankDetails.disbursmentAccHolderName && (
          <Grid item xs sm md>
            <div className="repayment-col">
              <div className="repayment-label ">Account Holder</div>
              <div className="repayment-smText">
                {bankDetails.disbursmentAccHolderName}
              </div>
            </div>
          </Grid>
        )}
        {bankDetails.disbursmentAccNumber && (
          <Grid item xs md sm>
            <div className="repayment-col">
              <div className="repayment-label ">Account No. &amp; Type </div>
              <div className="repayment-smText">
                {`${
                  bankDetails.disbursmentAccNumber
                } / ${bankDetails.disbursmentAccType || 'NA'}`}
              </div>
            </div>
          </Grid>
        )}

        {bankDetails.disbursmentBankName && (
          <Grid item xs md sm>
            <div className="repayment-col">
              <div className="repayment-label ">Bank Name</div>
              <div className="repayment-smText">
                {bankDetails.disbursmentBankName}
              </div>
            </div>
          </Grid>
        )}

        {bankDetails.disbursmentBankIfsc && (
          <Grid item xs md sm>
            <div className="repayment-col">
              <div className="repayment-label ">IFSC No.</div>
              <div className="repayment-smText">
                {bankDetails.disbursmentBankIfsc}
              </div>
            </div>
          </Grid>
        )}
        {bankDetails.disbursmentBankBranch && (
          <Grid item xs md sm>
            <div className="repayment-col">
              <div className="repayment-label ">Bank Branch</div>
              <div className="repayment-smText">
                {bankDetails.disbursmentBankBranch}
              </div>
            </div>
          </Grid>
        )}
        <Grid item container xs={12} md={12} sm={12}>
          {mandate && mandate.umrn ? (
            <>
              <Grid item xs={6} md={6} sm={6}>
                <div className="repayment-col">
                  <div className="repayment-label ">UMRN </div>
                  <div className="repayment-smText">{mandate.umrn}</div>
                </div>
              </Grid>
              <Grid item md={6} xs={6} sm={6}>
                <div className="repayment-col">
                  <div className="repayment-label-mandate ">
                    {`E-mandate registered at : ${formatDate(
                      mandate.updatedAt,
                    )}`}
                  </div>
                </div>
              </Grid>
            </>
          ) : (
            <Grid item>
              <div className="repayment-col">
                <div className="repayment-label-penny ">
                  {`Penny-Drop completed at : ${formatDate(
                    bankDetails.updatedAt,
                  )}`}
                </div>
              </div>
            </Grid>
          )}
        </Grid>
      </Grid>
    </>
  );
}

RepaymentSection.propTypes = {};

export default memo(RepaymentSection);
