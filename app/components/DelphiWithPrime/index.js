import React, { memo } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Spin, Collapse } from 'antd';
import styled from 'styled-components';
import DelphiDisplay from '../DelphiDisplay';

const { Panel } = Collapse;

function DelphiWithPrime({ data, loading }) {
  if (Object.keys(data).length === 0)
    return <NoData>No Delphi Output Data Available!</NoData>;

  return (
    <Spin spinning={loading} tip="Loading...">
      <Collapse defaultActiveKey={['1']}>
        {data.nearPrimeResponse && (
          <Panel header="Near Prime Response" key="1">
            <DelphiDisplay
              loading={loading}
              showData={data.nearPrimeResponse}
            />
          </Panel>
        )}
        <Panel header="Prime Response" key="2">
          <DelphiDisplay loading={loading} showData={data.response} />
        </Panel>
      </Collapse>
    </Spin>
  );
}

const Title = styled.div`
  font-size: 20px;
  font-weight: bold;
  line-height: 1.5715;
  margin-top: 20px;
  margin-bottom: 20px;
  color: rgba(0, 0, 0, 0.85);
`;

const NoData = styled.div`
  text-align: center;
  font-size: large;
  font-weight: bold;
`;

DelphiWithPrime.propTypes = {
  data: PropTypes.object,
};

DelphiWithPrime.defaultProps = {
  data: {},
};

export default memo(DelphiWithPrime);
