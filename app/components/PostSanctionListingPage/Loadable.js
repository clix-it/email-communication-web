/**
 *
 * Asynchronously loads the component for PostSanctionListingPage
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
