import React, { useEffect, useState } from 'react';
import { Row, Col } from 'antd';
import _ from 'lodash';
import InputField from 'components/InputField';
import { DoctorEmploymentTypeArray } from '../../utils/constants';

function DoctorOccupationalDetails({
  responsiveColumns,
  values,
  userIndex,
  control,
  errors,
  setError,
  clearError,
  allFieldsDisabled,
  userEmployments,
}) {
  const [employments, setEmployments] = useState([]);
  useEffect(() => {
    if (userEmployments && userEmployments.length > 0) {
      setEmployments(_.get(_.orderBy(userEmployments, 'id', 'desc'), '[0]'));
    } else setEmployments([]);
  }, [userEmployments]);

  return (
    <Row gutter={[16, 16]}>
      {/*<Col xs={0}>
        <InputField
          id={`applicants[${userIndex}].userEmployments[0].id`}
          control={control}
          name={`applicants[${userIndex}].userEmployments[0].id`}
          type="string"
          defaultValue={employments.id || ''}
          disabled
        />
  </Col>*/}
      <Col {...responsiveColumns}>
        <InputField
          id={`applicants[${userIndex}].userEmployments[0].employmentType`}
          control={control}
          name={`applicants[${userIndex}].userEmployments[0].employmentType`}
          type="select"
          options={DoctorEmploymentTypeArray}
          defaultValue={employments.employmentType || ''}
          rules={{
            required: {
              value: false,
              message: 'This field cannot be left empty',
            },
          }}
          errors={errors}
          placeholder="Profile"
          labelHtml="Profile"
          //disabled
        />
      </Col>
    </Row>
  );
}

export default DoctorOccupationalDetails;
