/**
 *
 * Asynchronously loads the component for DoctorOccupationalDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
