/**
 *
 * ShowDocuments
 *
 */

import React, { memo, useState, useEffect } from 'react';
import DrawerComponent from '../DrawerComponent';
import styled from 'styled-components';
import {
  List,
  Avatar,
  Divider,
  Button,
  Typography,
  Spin,
  Space,
  Empty,
} from 'antd';
import docSvg from '../../images/document.svg';
import {
  fetchDocs,
  fetchDocsV1,
} from '../../containers/CustomerLoanDetails/actions';

const Wrapper = styled.section`
  background: #fff;
  margin: 0 1em;
  padding: 0.25em 1em;
`;

const ListWrapper = styled.div`
  height: 300px;
  overflow: auto;
`;

function ShowDocuments({
  appDetails,
  docsLoading,
  docsData,
  appV1Docs,
  dispatch,
}) {
  const [visible, setVisible] = useState(false);
  const [currentItem, setCurrentItem] = useState({});
  const [existingDocs, setExistingDocs] = useState([]);
  const { Title, Text } = Typography;

  useEffect(() => {
    if (appDetails && appDetails.appId) {
      dispatch(fetchDocs(appDetails.appId));
      dispatch(fetchDocsV1(appDetails.appDocuments));
    }
  }, [appDetails]);
  useEffect(() => {
    if (docsData.length > 0) {
      setExistingDocs(docsData);
    } else {
      setExistingDocs([]);
    }
  }, [docsData]);
  const showDrawer = (e, item) => {
    setCurrentItem(item);
    setVisible(true);
  };

  const showDMSDrawer = (e, item) => {
    let dmsItem = {
      fileextension: item.extension,
      signedUrl: `data:${item.mimeType};base64,${item.fileStream}`,
    };
    setCurrentItem(dmsItem);
    setVisible(true);
  };

  const onClose = () => {
    setCurrentItem({});
    setVisible(false);
  };
  const setDescription = item => {
    return <Text>App</Text>;
  };
  function download(filename, text, mimeType) {
    var element = document.createElement('a');
    element.setAttribute(
      'href',
      'data:' + mimeType + ';base64,' + encodeURIComponent(text),
    );
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
  return (
    <Spin spinning={docsLoading} tip={'Loading...'}>
      <Wrapper>
        <DrawerComponent
          currentItem={currentItem}
          visible={visible}
          onClose={onClose}
        />
        <ListWrapper>
          {appV1Docs.length > 0 && (
            <List
              className="demo-loadmore-list"
              itemLayout="horizontal"
              size="small"
              bordered
              dataSource={appV1Docs}
              renderItem={(item, index) => (
                <List.Item
                  actions={[
                    <Button
                      type="link"
                      key={index}
                      onClick={event => showDMSDrawer(event, item.data)}
                    >
                      View
                    </Button>,

                    <a
                      key={index}
                      onClick={() => {
                        download(
                          item.data.fileName,
                          item.data.fileStream,
                          item.data.mimeType,
                        );
                      }}
                    >
                      Download
                    </a>,
                  ]}
                >
                  <List.Item.Meta
                    avatar={<Avatar shape="square" src={docSvg} />}
                    title={<Text>{item.data.fileName}</Text>}
                    description={
                      <Space>
                        <Text>Name:</Text>
                        <Text>{item.data.fileName}</Text>
                      </Space>
                    }
                  />
                </List.Item>
              )}
            />
          )}
          {existingDocs.length > 0 && (
            <List
              className="demo-loadmore-list"
              itemLayout="horizontal"
              size="small"
              bordered
              dataSource={existingDocs}
              renderItem={(item, index) => (
                <>
                  <List.Item
                    actions={[
                      <Button
                        type="link"
                        key={index}
                        onClick={event => showDrawer(event, item)}
                      >
                        View
                      </Button>,

                      <a key={index} href={item.signedUrl} target="_blank">
                        Download
                      </a>,
                    ]}
                  >
                    <List.Item.Meta
                      avatar={<Avatar shape="square" src={docSvg} />}
                      title={<Text>{item.type}</Text>}
                      description={setDescription(item)}
                    />
                  </List.Item>
                </>
              )}
            />
          )}
          {existingDocs.length == 0 && appV1Docs.length == 0 && <Empty />}
        </ListWrapper>
        <Divider />
      </Wrapper>
    </Spin>
  );
}

ShowDocuments.propTypes = {};

export default memo(ShowDocuments);
