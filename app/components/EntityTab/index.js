import React from 'react';
import { Menu, Spin, Row, Col, Button } from 'antd';
import _ from 'lodash';

import CallCommercialBureau from 'containers/CallCommercialBureau/Loadable';
import EntityDetails from '../EntityDetails';
import {
  setSelectedEntity,
  unlinkApplicant,
} from '../../containers/ApplicantDetails/actions';
import { columnResponsiveLayout } from '../EntityDetails/constants';

function EntityTab({
  dispatch,
  appId,
  creditData,
  applicationData,
  applicantDetails,
  requestType,
  addApplicantEntity,
  activity,
}) {
  const {
    entity: { entities = [], entityCuid },
    entity,
    selectedEntity,
    loading,
  } = applicantDetails;
  console.log('loading', applicantDetails);
  const setEntity = id => {
    const applicant =
      _.find(entity.entities, { id: id.key }) ||
      _.find(entity.entities, { id: parseInt(id.key) });
    if (applicant) dispatch(setSelectedEntity(applicant));
  };

  let orderedEntities = entities;
  if (entities && entities.length > 1) {
    orderedEntities = _.filter(
      entities,
      user => _.get(user, 'appLMS.role') === 'Co-Applicant',
    );
    const company = _.find(
      entities,
      user =>
        !_.get(user, 'appLMS.role') ||
        _.get(user, 'appLMS.role') === 'Applicant',
    );
    if (company) orderedEntities.splice(0, 0, company);
  }

  return (
    <Spin spinning={loading} tip="Loading...">
      <div className="App">
        <Menu
          onClick={setEntity}
          defaultSelectedKeys={[(selectedEntity || {}).id]}
          selectedKeys={[String((selectedEntity || {}).id)]}
          mode="horizontal"
          overflowedIndicator
        >
          {_.map(orderedEntities, (entityEle, index) => (
            <Menu.Item key={String(entityEle.id)}>
              {!index ? 'Primary Entity' : `Entity ${index + 1}`}
            </Menu.Item>
          ))}
          <Menu.Item key="add">
            <Button type="link" onClick={() => dispatch(addApplicantEntity())}>
              + Applicant Entity
            </Button>
          </Menu.Item>
        </Menu>
        {selectedEntity && requestType === 'PMA' ? (
          <Row gutter={[16, 16]}>
            <Col
              {...columnResponsiveLayout.colSingle}
              style={{
                fontSize: '18px',
                fontWeight: 600,
                padding: '12px 8px',
              }}
            >
              <CallCommercialBureau cuid={selectedEntity.cuid} />
            </Col>
          </Row>
        ) : (
          ''
        )}
        {selectedEntity && (
          <>
            {requestType === 'PMA' &&
              selectedEntity.appLMS &&
              selectedEntity.appLMS.role === 'Co-Applicant' && (
                <Row gutter={[16, 16]}>
                  <Col
                    {...columnResponsiveLayout.colSingle}
                    style={{
                      fontSize: '18px',
                      fontWeight: 600,
                      padding: '12px 8px',
                    }}
                  >
                    <Button
                      type={selectedEntity.userLinked ? 'default' : 'primary'}
                      htmlType="button"
                      className="personalDetailsRemoveButton"
                      style={{ marginLeft: '10px' }}
                      disabled={
                        activity === 'CREDIT_FCU' ||
                        activity === 'HUNTER_REVIEW'
                      }
                      onClick={() =>
                        dispatch(
                          unlinkApplicant(
                            selectedEntity.id,
                            !selectedEntity.userLinked,
                          ),
                        )
                      }
                    >
                      {selectedEntity.userLinked
                        ? 'Unlink Entity'
                        : 'Link Entity'}
                    </Button>
                  </Col>
                </Row>
              )}
            <EntityDetails
              dispatch={dispatch}
              appId={appId}
              requestType={requestType}
              entityData={selectedEntity}
              applicantDetails={applicantDetails}
              applicationData={applicationData}
              creditData={creditData}
              entityCuid={entityCuid}
              allFieldsDisabled={!selectedEntity.userLinked}
            />
          </>
        )}
      </div>
    </Spin>
  );
}

export default EntityTab;
