import _ from 'lodash';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';

import { Col, Button, Spin, message, Divider, Row } from 'antd';
import { formatDate, revertFormatDate } from 'utils/helpers';

import EntityForm from 'components/EntityForm';
import ContactDetails from 'components/ContactDetails';
// import FinancialUpload from 'containers/FinancialUpload';
import StyledTitle from 'components/StyledTitle';
import InputField from '../InputField';
import {
  responsiveColumns,
  numberPattern,
  STATE_GSTS,
} from '../../utils/constants';

import {
  updateEntityDetails,
  loadUpdateUserRequest,
  setFormValidationError,
  isDirtyForm,
  updateUsers,
  fetchPriority,
  fetchRiskLevel,
  fetchCategory,
} from '../../containers/ApplicantDetails/actions';
import AddressSection from '../../containers/AddressSection';

function EntityDetails({
  entityData,
  dispatch,
  appId,
  applicantDetails,
  applicationData,
  requestType,
  activity,
  creditData,
  entityCuid,
  allFieldsDisabled,
}) {
  const { registeredName, loanOffers = [] } = entityData;
  const defaultValues = {
    ...entityData,
    users: [
      {
        ...entityData,
        dateOfBirthIncorporation: formatDate(
          entityData.dateOfBirthIncorporation,
        ),
      },
    ],
    'additionalData.data.extraDataField.cmr':
      _.get(
        applicantDetails,
        'entity.additionalData.data.extraDataField.cmr',
      ) || '0',
    'users[0].riskLevel': _.get(entityData, 'riskLevel')
      ? _.get(entityData, 'riskLevel')
      : '',
    'users[0].registeredName': _.get(entityData, 'registeredName')
      ? _.get(entityData, 'registeredName')
      : '',
    'users[0].preferredPhone': _.get(entityData, 'preferredPhone')
      ? _.get(entityData, 'preferredPhone')
      : '',
    'users[0].userIdentities.pan': _.get(entityData, 'userIdentities.pan')
      ? _.get(entityData, 'userIdentities.pan')
      : '',
    'users[0].contactibilities[0].phoneNumber': _.get(
      _.orderBy(_.get(entityData, 'contactibilities'), 'id', 'desc'),

      '[0].phoneNumber',
    ),
    'users[0].contactibilities[0].email': _.get(
      _.orderBy(_.get(entityData, 'contactibilities'), 'id', 'desc'),

      '[0].email',
    ),
    additionalData: _.get(creditData, 'entity.additionalData', {}),
    'users[0].appLMS.id': _.get(entityData, 'appLMS')
      ? _.get(entityData, 'appLMS.id')
      : '',
    'users[0].appLMS.role': _.get(entityData, 'appLMS')
      ? _.get(entityData, 'appLMS.role')
      : '',
    'users[0].appGsts[0].id': _.get(
      _.orderBy(_.get(entityData, 'appGsts'), 'id', 'desc'),
      '[0].id',
      '',
    ),
    'users[0].appGsts[0].gstn': _.get(
      _.orderBy(_.get(entityData, 'appGsts'), 'id', 'desc'),
      '[0].gstn',
      '',
    ),
    // 'users[0].userDetails.natureOfBusiness': _.get(
    //   entityData,
    //   'userDetails.natureOfBusiness',
    // ),
  };

  const {
    handleSubmit,
    errors,
    control,
    reset,
    watch,
    setError,
    clearError,
    getValues,
    setValue,
    formState,
    triggerValidation,
  } = useForm({
    mode: 'onChange',
    defaultValues,
  });

  let consumerCibilScore = JSON.parse(
    _.get(
      applicantDetails,
      'entity.additionalData.data.extraDataField.consumerCibilScore',
      '{}',
    ),
  );
  let cibilScore = '';
  if (consumerCibilScore && Object.keys(consumerCibilScore).length > 0) {
    cibilScore = consumerCibilScore
      ? consumerCibilScore[`${entityData.cuid}`]
      : '';
  }

  const values = watch();
  const {
    loading,
    updateLoading,
    formValidationErrors,
    entity: { appGsts = [], users = [] } = {},
    masterData: { BUSINESSTYPE = [], COMPANYCATEGORY = [] },
  } = applicantDetails;

  useEffect(() => {
    dispatch(isDirtyForm({ 0: false }));
    dispatch(isDirtyForm({ 1: false }));
    dispatch(isDirtyForm({ 2: false }));
    dispatch(isDirtyForm({ 3: false }));
    dispatch(isDirtyForm({ 4: false }));
    dispatch(isDirtyForm({ 5: false }));
    triggerValidation();
  }, []);

  const array = [
    'applicantDetailsPma',
    'dedudeListpma',
    'postSanctionpma',
    'employmentReviewpma',
    'incomeReviewpma',
  ];

  const address = location.pathname.split('/');

  const disabled2 = !array.includes(address[1]);
  useEffect(() => {
    dispatch(isDirtyForm({ 0: formState.dirty }));
  }, [formState.dirty]);
  const onSubmit = data => {
    // if (!formState.isValid) {
    //   triggerValidation().then(valid => {
    //     if (!valid) message.error('Please fill valid data');
    //   });
    // }
    if (
      ['Limited Company', 'Private Limited Co', 'LLP'].indexOf(
        values['users[0].corporateStructure'],
      ) >= 0 &&
      (values['users[0].userIdentities.cin'] == undefined ||
        values['users[0].userIdentities.cin'] == '')
    ) {
      setError('users[0].userIdentities.cin', 'pattern', 'Please enter CIN');
      return;
    } else {
      clearError('users[0].userIdentities.cin');
    }
    data.users = _.map(data.users, user => ({
      ...user,
      dateOfBirthIncorporation: revertFormatDate(user.dateOfBirthIncorporation),
    }));

    let userToCheck = entityData;
    if (String(entityData.id).includes('NEW'))
      userToCheck = _.get(data, 'users', []);

    // the below condition checks if gst is from the same state as the office address of the entity
    if (userToCheck && Object.keys(userToCheck).length > 0) {
      let isGstnValid = true;
      const appGst =
        _.get(
          _.orderBy(_.get(entityData, 'appGsts', []), 'id', 'desc'),
          '[0]',
          {},
        ) || {};
      const gstn = _.get(data, 'users[0].appGsts[0].gstn', '');
      const officeAddressState = _.get(
        _.orderBy(_.get(userToCheck, 'contactibilities'), 'id', 'desc'),
        '[0].state',
      );
      // console.log('state', STATE_GSTS[gstn.substring(0, 2)]);
      if (officeAddressState && gstn) {
        if (
          STATE_GSTS[gstn.substring(0, 2)] !== _.upperCase(officeAddressState)
        ) {
          isGstnValid = false;
        }
      }

      if (!isGstnValid) {
        message.error(
          'GSTN Number is not of the State where the Office Address, please change either of two!',
        );
        return;
      }
      if (_.get(appGst, 'id', '')) {
        data.users[0].appGsts[0].id = _.get(appGst, 'id', '');
      }
    }
    if (!_.get(data, 'users[0].appLMS.id')) {
      delete data.users[0].appLMS;
    }
    const userUpdate = {
      ...data.users[0],
      identities: data.users[0].userIdentities,
    };
    if (entityData.cuid !== undefined && entityData.cuid !== '') {
      if (data.consumerCibilScore) {
        const cibilScores = JSON.parse(
          _.get(
            applicantDetails,
            'entity.additionalData.data.extraDataField.consumerCibilScore',
            '{}',
          ),
        );
        cibilScores[entityData.cuid] = data.consumerCibilScore;
        data.additionalData = {
          data: {
            extraDataField: {
              consumerCibilScore: JSON.stringify({
                ...cibilScores,
              }),
            },
          },
        };
      }
      /* dispatch(
        loadUpdateUserRequest(
          userUpdate,
          entityData.cuid,
          entityData.type.toLowerCase(),
        ),
      ); */
    }
    dispatch(
      updateUsers(
        {
          id: data.users[0].id,
          cuid: entityData.cuid, // 1000017055
          entityOfficers: [
            {
              ...data.entityOfficers,
              belongsTo: entityData.cuid,
            },
          ],
        },
        entityCuid,
        data,
      ),
    );
    // if (!String(entityData.id).includes('NEW'))
    //   dispatch(updateEntityDetails(data, reset, values));
  };

  useEffect(() => {
    // if (requestType === 'PMA')
    //   triggerValidation().then(valid => {
    //     dispatch(setFormValidationError('entityDetails', valid));
    //   });
    consumerCibilScore = JSON.parse(
      _.get(
        applicantDetails,
        'entity.additionalData.data.extraDataField.consumerCibilScore',
        '{}',
      ),
    );

    if (consumerCibilScore && Object.keys(consumerCibilScore).length > 0) {
      cibilScore = consumerCibilScore
        ? consumerCibilScore[`${entityData.cuid}`]
        : '';
    }
    setValue('consumerCibilScore', cibilScore);
    setValue(
      'users[0].userDetails.turnover',
      _.get(entityData, 'userDetails.turnover'),
    );
    setValue(
      'users[0].userDetails.natureOfBusiness',
      _.get(entityData, 'userDetails.natureOfBusiness'),
    );
    setValue(
      'users[0].userDetails.industry',
      _.get(entityData, 'userDetails.industry'),
    );
    setValue(
      'users[0].userDetails.segment',
      _.get(entityData, 'userDetails.segment'),
    );
    setValue(
      'users[0].userDetails.product',
      _.get(entityData, 'userDetails.product'),
    );

    let validationObj = _.get(formValidationErrors, 'entityDetails', {});
    if (
      requestType === 'PMA' &&
      entityData.id &&
      !String(entityData.id).includes('NEW')
    ) {
      if (
        !validationObj ||
        (validationObj &&
          Object.keys(validationObj).length !==
            applicantDetails.entity.entities.length) ||
        (validationObj && validationObj[entityData.id] === undefined)
      ) {
        validationObj = {};
        applicantDetails.entity.entities.forEach(app => {
          validationObj[app.id] = false;
        });
      }

      triggerValidation().then(valid => {
        dispatch(
          setFormValidationError('entityDetails', {
            ...validationObj,
            [entityData.id]: valid,
          }),
        );
      });
    }
    reset(defaultValues);
  }, [entityData]);

  useEffect(() => {
    if (_.get(values, 'users[0].registeredName')) {
      clearError('users[0].registeredName');
    }
    if (_.get(values, 'users[0].dateOfBirthIncorporation')) {
      clearError('users[0].dateOfBirthIncorporation');
    }
    if (_.get(values, 'users[0].preferredPhone')) {
      clearError('users[0].preferredPhone');
    }
    if (_.get(values, 'users[0].userDetails.natureOfBusiness')) {
      clearError('users[0].userDetails.natureOfBusiness');
    }
    if (_.get(values, 'users[0].userDetails.industry')) {
      clearError('users[0].userDetails.industry');
    }
    if (_.get(values, 'users[0].userDetails.product')) {
      clearError('users[0].userDetails.product');
    }
    if (_.get(values, 'users[0].userDetails.segment')) {
      clearError('users[0].userDetails.segment');
    }
    if (_.get(values, 'users[0].userDetails.turnover')) {
      clearError('users[0].userDetails.turnover');
    }
    if (
      _.get(values, 'users[0].userIdentities.pan') &&
      _.get(errors, 'users[0].userIdentities.pan.type') === 'required'
    ) {
      clearError('users[0].userIdentities.pan');
    }
    if (_.get(values, 'users[0].contactibilities[0].email')) {
      clearError('users[0].contactibilities[0].email');
    }
    if (_.get(values, 'users[0].contactibilities[0].phoneNumber')) {
      clearError('users[0].contactibilities[0].phoneNumber');
    }
  }, [_.get(errors, 'users[0]')]);

  const ifEntityCoApplicant =
    _.get(entityData, 'appLMS.role', '') === 'Co-Applicant';

  let contactMap = {};
  if (entityData.cuid !== undefined && entityData.cuid !== '') {
    const contactibilities = _.orderBy(
      _.get(entityData, 'contactibilities'),
      'id',
      'desc',
    );
    // debugger;
    if (contactibilities && contactibilities.length > 0) {
      // contactibilities.shift();
      if (contactibilities.length === 1) {
        if (!_.get(contactibilities, '[0].contactType'))
          contactMap.Latest = contactibilities;
        else {
          contactMap[
            _.get(contactibilities, '[0].contactType')
          ] = contactibilities;
        }
      } else {
        contactMap = _.mapValues(
          _.groupBy(contactibilities, 'contactType'),
          // clist => clist.map(contact => _.omit(contact, 'contactType')),
        );
        Object.keys(contactMap).forEach(key => {
          if (!key) {
            contactMap.undefined = contactMap[key];
            delete contactMap[key];
          }
        });
      }
    }
  }

  useEffect(() => {
    if (_.get(applicantDetails, 'entity.partner')) {
      dispatch(
        fetchPriority(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      dispatch(
        fetchCategory(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      dispatch(
        fetchRiskLevel(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
    }
  }, [
    _.get(applicantDetails, 'entity.partner', ''),
    _.get(applicantDetails, 'entity.product', ''),
  ]);

  console.log('applicationData', applicationData);

  return (
    <>
      {/* {error.status && <Redirect to="/" />} */}
      {/* <Spin
        spinning={loading || updateLoading}
        tip={loading ? 'Loading...' : 'Submitting Data...'}
      > */}
      <div className="App">
        <form onSubmit={handleSubmit(onSubmit)}>
          {/* <Col {...columnResponsiveLayout.colSingle} className="">
              <div
                style={{
                  fontSize: '18px',
                  fontWeight: '600',
                  marginBottom: '20px',
                }}
              >
                Entity Details
              </div>
            </Col> 
            <Divider /> */}
          <StyledTitle> Basic Details</StyledTitle>
          <EntityForm
            control={control}
            errors={errors}
            values={values}
            clearError={clearError}
            getValues={getValues}
            setError={setError}
            setValue={setValue}
            userIndex={0}
            cuid={entityData.cuid}
            ifEntityCoApplicant={ifEntityCoApplicant}
            entityData={entityData}
            reset={reset}
            defaultValues={defaultValues}
            masterPriorityData={applicantDetails.masterPriorityData}
            masterRiskData={applicantDetails.masterRiskData}
            masterCategoryData={applicantDetails.masterCategoryData}
            allFieldsDisabled={allFieldsDisabled}
            partner={_.get(applicantDetails, 'entity.partner', '')}
            product={_.get(applicantDetails, 'entity.product')}
            requestType={requestType}
            BUSINESSTYPE={BUSINESSTYPE}
            COMPANYCATEGORY={COMPANYCATEGORY}
          />
          <Divider />
          <div
            style={{
              fontSize: '18px',
              fontWeight: '600',
              marginBottom: '15px',
            }}
          >
            Contact Details
          </div>
          <ContactDetails
            cuid={entityData.cuid}
            userIndex={0}
            control={control}
            errors={errors}
            values={values}
            clearError={clearError}
            setError={setError}
            reset={reset}
            setValue={setValue}
            getValues={getValues}
            defaultValues={defaultValues}
            contactMap={contactMap}
            ifEntityCoApplicant={ifEntityCoApplicant}
            entityDataId={entityData.id}
            allFieldsDisabled={allFieldsDisabled}
            partner={_.get(applicantDetails, 'entity.partner', '')}
          />
          {_.get(entityData, 'appLMS.role', '') === 'Co-Applicant' &&
            entityData.cuid !== undefined &&
            entityData.cuid !== '' && (
              <>
                <Divider />
                <StyledTitle>Other Details</StyledTitle>
                <Row gutter={[16, 16]}>
                  <Col {...responsiveColumns}>
                    <InputField
                      id="consumerCibilScore"
                      // id="additionalData.data.extraDataField.consumerCibilScore"
                      control={control}
                      reset={reset}
                      name="consumerCibilScore"
                      defaultValue={cibilScore}
                      type="string"
                      rules={{
                        required: {
                          value:
                            _.get(entityData, 'appLMS.role', '') ===
                            'Co-Applicant',
                          message: 'This field cannot be left empty',
                        },
                        pattern: numberPattern,
                      }}
                      errors={errors}
                      placeholder="Consumer Cibil Score"
                      labelHtml="Consumer Cibil Score"
                      disabled={allFieldsDisabled}
                    />
                  </Col>
                </Row>
              </>
            )}
          <Button
            disabled={disabled2 || allFieldsDisabled}
            type="primary"
            htmlType="submit"
            className="entityDetailsButton"
          >
            Save
          </Button>
        </form>
        <Divider />
        <div style={{ marginBottom: '2em' }}>
          {contactMap &&
            Object.keys(contactMap) &&
            Object.keys(contactMap).length > 0 &&
            Object.keys(contactMap).map((contactType, index) => (
              <AddressSection
                cuid={entityData.cuid}
                userId={_.get(values, `users[${0}].id`, '')}
                contactType={contactType}
                index={index}
                contactArr={contactMap}
                appId={appId}
                partner={_.get(applicantDetails, 'entity.partner')}
              />
            ))}
        </div>
        <Divider />
        {/* <FinancialUpload
          loanOffers={loanOffers}
          registeredName={registeredName}
          appId={appId}
        /> */}
      </div>
      {/* </Spin> */}
    </>
  );
}

EntityDetails.propTypes = {
  entityData: PropTypes.object,
  appId: PropTypes.string.isRequired || PropTypes.number.isRequired,
  dispatch: PropTypes.func.isRequired,
};

EntityDetails.defaultProps = {
  entityData: {},
};

export default EntityDetails;
