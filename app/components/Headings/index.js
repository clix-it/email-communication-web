import styled from 'styled-components';

const H1 = styled.h1`
  font-size: ${props => `${props.fontSize || 1.75}rem`};
  margin-bottom: 0.25em;
  color: ${props => props.color || 'black'};
  /* text-shadow: 0 1px 2px rgba(0, 0, 0, 0.5); */
  letter-spacing: 0.5px;
  font-weight: 400;
  text-transform: ${props => (props.caps ? 'uppercase' : 'inherit')};

  &:after {
    content: '';
    display: ${props => (props.nopuck ? 'none' : 'block')};
    width: 80px;
    height: 4px;
    background: ${props => props.theme.colors.primary};
    margin: 16px auto;
  }
`;

const H2 = styled.h2`
  font-size: ${props => `${props.fontSize || 1.2}em`};
  font-weight: 200;
  /* color: inherit;
  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.5); */
  line-height: 20px;
  letter-spacing: 0.5px;
  text-transform: ${props => (props.caps ? 'uppercase' : 'inherit')};
`;

const H3 = styled.h3`
  font-size: 1.5em;
  color: ${props => props.color || 'inherit'};
  text-transform: ${props => (props.caps ? 'uppercase' : 'inherit')};
`;

export { H1, H2, H3 };
