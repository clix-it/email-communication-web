/**
 *
 * DedupeEntityForm
 *
 */

import _ from 'lodash';
import React, { useEffect } from 'react';
import { Row, Col, Typography } from 'antd';

import GstnInput from 'containers/GstnInput';
import InputField from 'components/InputField';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

const { Text } = Typography;

function DedupeEntityForm({ defaultValues }) {
  return (
    <Row gutter={[16, 16]}>
      <Col {...responsiveColumns}>
        <Text strong>Entity Name : </Text>
        <Text>{defaultValues.registeredName}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Existing Constitution : </Text>
        <Text>{defaultValues.corporateStructure}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>PAN : </Text>
        <Text>
          {defaultValues.identities ? defaultValues.identities.pan || '' : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>GSTN : </Text>
        <Text>{_.get(defaultValues, 'gsts[0].gstn') || ''}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Corporate Identity Number (CIN) : </Text>
        <Text>
          {defaultValues.identities ? defaultValues.identities.cin : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Nature Of Business : </Text>
        <Text>
          {defaultValues.userDetails
            ? defaultValues.userDetails.natureOfBusiness
            : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Industry : </Text>
        <Text>
          {defaultValues.userDetails ? defaultValues.userDetails.industry : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Segment : </Text>
        <Text>
          {defaultValues.userDetails ? defaultValues.userDetails.segment : ''}
        </Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Product : </Text>
        <Text>
          {defaultValues.userDetails ? defaultValues.userDetails.product : ''}
        </Text>
      </Col>
    </Row>
  );
}

DedupeEntityForm.propTypes = {};

export default DedupeEntityForm;
