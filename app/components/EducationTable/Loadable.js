/**
 *
 * Asynchronously loads the component for EducationTable
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
