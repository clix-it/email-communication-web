/**
 *
 * ShowDkycDetails
 *
 */

import React, { memo, useState, useEffect } from 'react';
import DrawerComponent from '../DrawerComponent';
import styled from 'styled-components';
import {
  List,
  Avatar,
  Divider,
  Button,
  Typography,
  Spin,
  Space,
  Descriptions,
} from 'antd';
import docSvg from '../../images/document.svg';
import {
  fetchDkycDocuments,
} from '../../containers/CustomerLoanDetails/actions';

const Wrapper = styled.section`
  background: #fff;
  margin: 0 1em;
  padding: 0.25em 1em;
`;

const ListWrapper = styled.div`
  height: 300px;
  overflow: auto;
`;

function ShowDkycDetails({
  dkycDocs,
  dkycDetails,
  kycDocsLoading,
  dispatch,
  dkycDocsData,
}) {
  const [visible, setVisible] = useState(false);
  const [currentItem, setCurrentItem] = useState({});
  const { Text } = Typography;

  useEffect(() => {
    if(dkycDocs.length > 0){
      dispatch(fetchDkycDocuments(dkycDocs));
    }
  }, [dkycDocs])

  const showDMSDrawer = (e, item) => {
    let dmsItem = {
      fileextension: item.extension,
      signedUrl: `data:${item.mimeType};base64,${item.fileStream}`,
    };
    setCurrentItem(dmsItem);
    setVisible(true);
  };

  const onClose = () => {
    setCurrentItem({});
    setVisible(false);
  };

  function download(filename, text, mimeType) {
    var element = document.createElement('a');
    element.setAttribute(
      'href',
      'data:' + mimeType + ';base64,' + encodeURIComponent(text),
    );
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }
  return (
    <>
      <Descriptions
        title={`DKYC Info`}
        column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      >
        <Descriptions.Item label="leadStatus">
          {dkycDetails.leadStatus}
        </Descriptions.Item>
        <Descriptions.Item label="customerName">
          {dkycDetails.customerName}
        </Descriptions.Item>
        <Descriptions.Item label="customerMobile">
          {dkycDetails.customerMobile}
        </Descriptions.Item>
        <Descriptions.Item label="customerEmail">
          {dkycDetails.customerEmail}
        </Descriptions.Item>
        <Descriptions.Item label="addType">
          {dkycDetails.addType}
        </Descriptions.Item>
        <Descriptions.Item label="addLine1">
          {dkycDetails.addLine1}
        </Descriptions.Item>
        <Descriptions.Item label="addLine2">
          {dkycDetails.addLine2}
        </Descriptions.Item>
        <Descriptions.Item label="pinCode">
          {dkycDetails.pinCode}
        </Descriptions.Item>
        <Descriptions.Item label="city">{dkycDetails.city}</Descriptions.Item>
        <Descriptions.Item label="state">{dkycDetails.state}</Descriptions.Item>
        <Descriptions.Item label="locality">
          {dkycDetails.locality}
        </Descriptions.Item>
        <Descriptions.Item label="scheduleDate">
          {dkycDetails.scheduleDate}
        </Descriptions.Item>
        <Descriptions.Item label="scheduleTime">
          {dkycDetails.scheduleTime}
        </Descriptions.Item>
        <Descriptions.Item label="product">
          {dkycDetails.product}
        </Descriptions.Item>
        <Descriptions.Item label="partner">
          {dkycDetails.partner}
        </Descriptions.Item>
        <Descriptions.Item label="leadType">
          {dkycDetails.leadType}
        </Descriptions.Item>
        <Descriptions.Item label="agentName">
          {dkycDetails.agentName}
        </Descriptions.Item>
      </Descriptions>
      <Divider />
      {dkycDocs.length > 0 && (
        <Descriptions
          title={`DKYC Documents`}
          column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
        />
      )}
      <Spin spinning={kycDocsLoading} tip={'Loading...'}>
        <Wrapper>
          <DrawerComponent
            currentItem={currentItem}
            visible={visible}
            onClose={onClose}
          />
          <ListWrapper>
            {dkycDocsData.length > 0 && (
              <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                size="small"
                bordered
                dataSource={dkycDocsData}
                renderItem={(item, index) => (
                  <List.Item
                    actions={[
                      <Button
                        type="link"
                        key={index}
                        onClick={event => showDMSDrawer(event, item.data)}
                      >
                        View
                      </Button>,

                      <a
                        key={index}
                        onClick={() => {
                          download(
                            item.data.properties.Document_Name,
                            item.data.fileStream,
                            item.data.mimeType,
                          );
                        }}
                      >
                        Download
                      </a>,
                    ]}
                  >
                    <List.Item.Meta
                      avatar={<Avatar shape="square" src={docSvg} />}
                      title={<Text>{item.data.properties.Document_Name}</Text>}
                      description={
                        <Space>
                          <Text>Name:</Text>
                          <Text>{item.data.properties.First_Name}</Text>
                        </Space>
                      }
                    />
                  </List.Item>
                )}
              />
            )}
          </ListWrapper>
        </Wrapper>
      </Spin>
    </>
  );
}

ShowDkycDetails.propTypes = {};

export default memo(ShowDkycDetails);
