/**
 *
 * Asynchronously loads the component for ShowDkycDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
