/**
 *
 * SearchDialog
 *
 */

import React, { memo, useState } from 'react';

import DateFnsUtils from '@date-io/date-fns';
import { useHistory } from 'react-router-dom';

// import PropTypes from 'prop-types';
// import styled from 'styled-components';

function SearchDialog({ modalVisible, open, handleClose }) {
  const history = useHistory();
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000),
  );
  const [endDate, setEndDate] = useState(new Date());
  const [appId, setAppId] = useState('');
  const [isSearchDisabled, setIsSearchDisabled] = useState(false);
  // const [modalVisible, setModalVisible] = useState(false);

  function handleStartDateChange(date) {
    '';
    setStartDate(date);
  }
  function handleEndDateChange(date) {
    setEndDate(date);
  }
  function handleAppIdChange(event) {
    setAppId(event.target.value);
  }
  function handleSearch() {
    handleClose();
    history.push(
      `/dedudeList?sd=${
        startDate instanceof Date ? startDate.getTime() : ''
      }&ed=${endDate.getTime()}&appId=${appId}`,
    );
  }

  function isDisabled() {
    if (
      (startDate &&
        (startDate.toString().includes('Invalid') ||
          startDate.getTime() > Date.now())) ||
      (endDate &&
        (endDate.toString().includes('Invalid') ||
          endDate.getTime() > Date.now())) ||
      (!endDate && !startDate && !appId)
    )
      return true;
    else return false;
  }
  return (
    <div>
      {/* <Button type="primary" onClick={() => setModalVisible(true)}>
        Display a modal dialog at 20px to Top
      </Button> */}
      <Modal
        title="20px to Top"
        style={{ top: 20 }}
        visible={modalVisible}
        onOk={handleClose}
        onCancel={handleClose}
      >
        <p>some contents...</p>
        <p>some contents...</p>
        <p>some contents...</p>
      </Modal>
    </div>
    // <Dialog open={open} onClose={handleClose} maxWidth="xl">
    //   <DialogTitle>Search Applications for Review</DialogTitle>
    //   <Divider variant="inset" />
    //   <DialogContent>
    //     <DialogContentText>
    //       {' '}
    //       To Search for Application(s) either enter a time period or an
    //       Application Number.{' '}
    //     </DialogContentText>

    //     <MuiPickersUtilsProvider utils={DateFnsUtils}>
    //       <Grid container justify="space-around">
    //         <KeyboardDatePicker
    //           // onError={handleStartDateError}
    //           autoOk
    //           disableFuture
    //           disableToolbar
    //           variant="inline"
    //           format="dd/MM/yyyy"
    //           margin="dense"
    //           id="date-picker-inline"
    //           label="Start Date "
    //           value={startDate}
    //           onChange={handleStartDateChange}
    //           KeyboardButtonProps={{
    //             'aria-label': 'change date',
    //           }}
    //         />

    //         <KeyboardDatePicker
    //           // onError={handleEndDateError}
    //           autoOk
    //           disableFuture
    //           disableToolbar
    //           variant="inline"
    //           format="dd/MM/yyyy"
    //           margin="dense"
    //           id="date-picker-inline"
    //           label=" End Date"
    //           value={endDate}
    //           onChange={handleEndDateChange}
    //           KeyboardButtonProps={{
    //             'aria-label': 'change date',
    //           }}
    //         />
    //       </Grid>
    //     </MuiPickersUtilsProvider>
    //     <Grid container>
    //       <Grid
    //         item
    //         xs={12}
    //         md={12}
    //         style={{ paddingLeft: 20, paddingRight: 20 }}
    //       >
    //         <TextField
    //           autoFocus
    //           placeholder="Enter Application Number "
    //           fullWidth
    //           margin="normal"
    //           value={appId}
    //           onChange={handleAppIdChange}
    //           label="#Application Number"
    //         />
    //       </Grid>
    //     </Grid>
    //   </DialogContent>
    //   <DialogActions>
    //     <Button variant="contained" onClick={handleClose}>
    //       Cancel
    //     </Button>
    //     <Button
    //       disabled={isDisabled()}
    //       variant="contained"
    //       style={{ backgroundColor: '#6F0EDF', color: 'white' }}
    //       onClick={handleSearch}
    //     >
    //       Search
    //     </Button>
    //   </DialogActions>
    // </Dialog>
  );
}

SearchDialog.propTypes = {};

export default memo(SearchDialog);
