import React from 'react';

import { Navbar } from 'react-bootstrap';
function Footer() {
  return (
    <Navbar
      sticky="bottom"
      className="bgArtwork"
      style={{ height: '240px', paddingTop: '20px' }}
      collapseOnSelect
    />
  );
}

export default Footer;
