/*
 * LoanDetailsTable Messages
 *
 * This contains all the text for the LoanDetailsTable component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.LoanDetailsTable';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the LoanDetailsTable component!',
  },
});
