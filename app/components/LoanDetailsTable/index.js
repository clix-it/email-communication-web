/**
 *
 * LoanDetailsTable
 *
 */

import React, { memo, useContext, useState, useEffect, useRef } from 'react';
import { Table, Input, Button, Popconfirm, Form, Card, message } from 'antd';

const EditableContext = React.createContext();

const EditableRow = ({ ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const EditableCell = ({
  title,
  editable,
  children,
  dataIndex,
  record,
  handleSave,
  ...restProps
}) => {
  const [editing, setEditing] = useState(false);
  const inputRef = useRef();
  const form = useContext(EditableContext);
  useEffect(() => {
    if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    form.setFieldsValue({
      [dataIndex]: record[dataIndex],
    });
  };

  const save = async () => {
    try {
      const values = await form.validateFields();
      toggleEdit();
      handleSave({ ...record, ...values });
    } catch (errInfo) {
      message.info('Save failed:', errInfo);
    }
  };

  let childNode = children;

  if (editable) {
    childNode = editing ? (
      <Form.Item
        style={{
          margin: 0,
        }}
        name={dataIndex}
        rules={[
          {
            required: true,
            message: `${title} is required.`,
          },
        ]}
      >
        <Input ref={inputRef} onPressEnter={save} onBlur={save} />
      </Form.Item>
    ) : (
      <div
        className="editable-cell-value-wrap"
        style={{
          paddingRight: 24,
        }}
        onClick={toggleEdit}
      >
        {children}
      </div>
    );
  }

  return <td {...restProps}>{childNode}</td>;
};

const LoanDetailsTable = ({ setValue, outstandingLoans }) => {
  const columnData = [
    {
      title: 'Amount',
      dataIndex: 'oblicationAmount',
      width: '30%',
      editable: true,
    },
    {
      title: 'Bank/NBFC',
      dataIndex: 'obligationInstitute',
      editable: true,
    },

    {
      title: 'Operation',
      dataIndex: 'operation',
      render: record =>
        dataSource.length >= 1 ? (
          <Popconfirm
            title="Sure to delete?"
            onConfirm={() => handleDelete(record.key)}
          >
            <a>Delete</a>
          </Popconfirm>
        ) : null,
    },
  ];

  const [dataSource, setDataSource] = useState([]);
  const [count, setCount] = useState(0);

  useEffect(() => {
    if (outstandingLoans) {
      const loanOutstanding = outstandingLoans.map((item, index) => ({
        key: index,
        obligationInstitute: item.obligationInstitute,
        oblicationAmount: item.oblicationAmount,
      }));
      setDataSource(loanOutstanding);
      setCount(loanOutstanding.length);
    }
  }, []);

  const handleDelete = key => {
    const data = [...dataSource];
    setDataSource(data.filter(item => item.key !== key));
  };

  const handleAdd = () => {
    // const { count, dataSource } = this.state;
    const newData = {
      key: count + 1,
      oblicationAmount: 'Enter Amount',
      obligationInstitute: 'Enter Bank Name',
    };
    setDataSource([...dataSource, newData]);
    setCount(count + 1);
  };

  const tableValues = newData => {
    if (newData.length > 0) {
      const newOutstandingLoans = newData
        .filter(
          item =>
            item.oblicationAmount !== 'Enter Amount' &&
            item.obligationInstitute !== 'Enter Bank Name',
        )
        .map(item => ({
          obligationInstitute: item.obligationInstitute,
          oblicationAmount: item.oblicationAmount,
        }));
      // console.log('newOutstandingLoans', newOutstandingLoans, dataSource);
      setValue('additionalData.data.outstandingLoans', newOutstandingLoans);
    }
  };

  const handleSave = row => {
    const newData = [...dataSource];
    const index = newData.findIndex(item => row.key === item.key);
    const item = newData[index];
    newData.splice(index, 1, { ...item, ...row });
    setDataSource(newData);
    tableValues(newData);
  };

  const components = {
    body: {
      row: EditableRow,
      cell: EditableCell,
    },
  };

  const columns = columnData.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        editable: col.editable,
        dataIndex: col.dataIndex,
        title: col.title,
        handleSave,
      }),
    };
  });
  return (
    <div>
      <Card>
        <Button
          onClick={handleAdd}
          type="primary"
          style={{
            marginBottom: 16,
            float: 'right',
            zIndex: 9,
          }}
        >
          Add a row
        </Button>
        <Table
          components={components}
          rowClassName={() => 'editable-row'}
          bordered
          dataSource={dataSource}
          columns={columns}
        />
      </Card>
    </div>
  );
};

LoanDetailsTable.propTypes = {};

export default memo(LoanDetailsTable);
