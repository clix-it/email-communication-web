import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { Descriptions, Table, Spin } from 'antd';
import _ from 'lodash';
import styled from 'styled-components';

function DelphiDisplay({ showData: delphiOutput, loading }) {
  const [eligibilityCols, setEligibilityCols] = useState([]);
  const [tradelinesCols, setTradelinesCols] = useState([]);

  const {
    bureau_response: {
      calculated_variables: calculatedVariables = {},
      eligibility = [],
      tradelines = [],
      decision = {},
    } = {},
    scorecard: { scorecard_name = '', scorecard_version = '' } = {},
  } = delphiOutput;

  useEffect(() => {
    if (eligibility && eligibility.length > 0) {
      const columns = [];
      Object.keys(eligibility[0]).forEach(col => {
        columns.push({
          title: _.upperCase(col),
          dataIndex: col,
          col,
          align: 'center',
          ellipsis: true,
        });
      });
      setEligibilityCols(columns);
    }
    if (tradelines && tradelines.length > 0) {
      const columns = [];
      Object.keys(tradelines[0]).forEach(col => {
        columns.push({
          title: _.upperCase(col),
          dataIndex: col,
          col,
          align: 'center',
          ellipsis: true,
        });
      });
      setTradelinesCols(columns);
    }
  }, [delphiOutput]);

  const displayValues = (obj, value) => {
    if (!obj) return null;
    if (obj[value] && Array.isArray(obj[value])) {
      return (
        <Descriptions.Item
          label={_.startCase(_.lowerCase(_.replace(value, /_/g, ' ')))}
        >
          {obj[value].map(ele => (
            <div>
              {JSON.stringify(ele, null, 4)} <br />
            </div>
          ))}
        </Descriptions.Item>
      );
    }
    if (
      obj[value] &&
      typeof obj[value] === 'object' &&
      obj[value].constructor === Object
    ) {
      return (
        <Descriptions.Item
          label={_.startCase(_.lowerCase(_.replace(value, /_/g, ' ')))}
        >
          {Object.keys(obj[value]).map(ele => (
            <div>
              <span style={{ fontWeight: 'bold' }}>
                {_.startCase(_.lowerCase(_.replace(ele, /_/g, ' ')))} :
              </span>
              {displayValues(obj[value], ele)}
            </div>
          ))}
        </Descriptions.Item>
      );
    }
    return (
      <Descriptions.Item
        label={_.startCase(_.lowerCase(_.replace(value, /_/g, ' ')))}
      >
        {String(obj[value])}
      </Descriptions.Item>
    );
  };

  if (Object.keys(delphiOutput).length === 0)
    return <NoData>No Delphi Output Data Available!</NoData>;

  return (
    <Spin spinning={loading} tip="Loading...">
      <Descriptions
        title="Score Card"
        column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      >
        <Descriptions.Item label="Score Card Name">
          {scorecard_name}
        </Descriptions.Item>
        <Descriptions.Item label="Score Card Version">
          {scorecard_version}
        </Descriptions.Item>
        <Descriptions.Item label="Status">{decision.status}</Descriptions.Item>
      </Descriptions>
      {eligibility && eligibility.length > 0 && (
        <>
          <Title>Eligibility</Title>
          <Table
            locale={{
              emptyText: 'No Data Found',
            }}
            dataSource={eligibility || []}
            pagination={false}
            columns={eligibilityCols}
          />
        </>
      )}
      {tradelines && tradelines.length > 0 && (
        <>
          <Title>TradeLines</Title>
          <Table
            locale={{
              emptyText: 'No Data Found',
            }}
            dataSource={tradelines || []}
            pagination={{ defaultPageSize: 5, showSizeChanger: true }}
            columns={tradelinesCols}
          />
        </>
      )}
      <Descriptions
        title="Calculated Variable"
        column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      >
        {Object.keys(calculatedVariables).length === 0 && (
          <NoData>No Calculated Variables are Available!</NoData>
        )}
        {calculatedVariables &&
          Object.keys(calculatedVariables).map(calVar =>
            displayValues(calculatedVariables, calVar),
          )}
      </Descriptions>
    </Spin>
  );
}

const Title = styled.div`
  font-size: 16px;
  font-weight: bold;
  line-height: 1.5715;
  margin-top: 20px;
  margin-bottom: 20px;
  color: rgba(0, 0, 0, 0.85);
`;

const NoData = styled.div`
  text-align: center;
  font-size: large;
  font-weight: bold;
`;

DelphiDisplay.propTypes = {
  showData: PropTypes.object,
};

DelphiDisplay.defaultProps = {
  showData: {},
};

export default memo(DelphiDisplay);
