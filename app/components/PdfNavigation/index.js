import React from 'react';
import PropTypes from 'prop-types';

import { Button, Typography, Space } from 'antd';

const { Text } = Typography;

export const CustomPrevButton = props => {
  const { page, handlePrevClick } = props;

  return (
    <Button onClick={handlePrevClick} disabled={page === 1}>
      Previous page
    </Button>
  );
};
CustomPrevButton.propTypes = {
  page: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired,
  handlePrevClick: PropTypes.func.isRequired,
};

export const CustomNextButton = props => {
  const { page, pages, handleNextClick } = props;

  return (
    <Button onClick={handleNextClick} disabled={page === pages}>
      Next page
    </Button>
  );
};
CustomNextButton.propTypes = {
  page: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired,
  handleNextClick: PropTypes.func.isRequired,
};

export const CustomPages = props => {
  const { page, pages } = props;
  return (
    <Space>
      <Text>{page}</Text>
      <Text>/</Text>
      <Text>{pages}</Text>
    </Space>
  );
};
CustomPages.propTypes = {
  page: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired,
};

const PdfNavigation = props => {
  const { page, pages } = props;

  const { handlePrevClick, handleNextClick } = props;

  return (
    <Space>
      <CustomPrevButton
        page={page}
        pages={pages}
        handlePrevClick={handlePrevClick}
      />
      <CustomPages page={page} pages={pages} />
      <CustomNextButton
        page={page}
        pages={pages}
        handleNextClick={handleNextClick}
      />
    </Space>
  );
};
PdfNavigation.propTypes = {
  page: PropTypes.number.isRequired,
  pages: PropTypes.number.isRequired,
  handlePrevClick: PropTypes.func.isRequired,
  handleNextClick: PropTypes.func.isRequired,
};

export default PdfNavigation;
