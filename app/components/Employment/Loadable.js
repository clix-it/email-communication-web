/**
 *
 * Asynchronously loads the component for Employment
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
