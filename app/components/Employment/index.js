import React, { memo, useEffect, useState } from 'react';
import EditableTable from 'components/EditableTable';
import _ from 'lodash';
import { isEmpty } from '../../utils/helpers';
import {
  DESIGNATION,
  INDUSTRY_TYPE,
  COMPANY_TYPE,
  employmentTypeArray,
} from '../../utils/constants';

function Employment({ values, userIndex, setValue, initDataSource, disabled }) {
  const initColumns = [
    {
      title: 'Occupation Type',
      dataIndex: 'employmentType',
      editable: true,
      width: 200,
      type: 'select',
      dropdown: employmentTypeArray,
      required: false,
    },
    {
      title: 'Company Type',
      dataIndex: 'companyType',
      editable: true,
      width: 200,
      type: 'select',
      dropdown: COMPANY_TYPE,
      required: false,
    },
    {
      title: 'Industry Type',
      dataIndex: 'companyIndutry',
      editable: true,
      width: 200,
      type: 'select',
      dropdown: INDUSTRY_TYPE,
      required: false,
    },
    {
      title: 'Employer Name',
      dataIndex: 'companyName',
      editable: true,
      width: 200,
      required: false,
    },
    {
      title: 'Designation',
      dataIndex: 'profession',
      editable: true,
      width: 200,
      type: 'select',
      dropdown: DESIGNATION,
      required: false,
    },
    {
      title: 'Email ID (Official)',
      dataIndex: 'officialEmail',
      editable: true,
      width: 300,
      required: false,
    },
    {
      title: 'Current Work Experience (Months)',
      dataIndex: 'currentExpInMonths',
      editable: true,
      width: 200,
      required: false,
    },
    {
      title: 'Total Work Experience (Months)',
      dataIndex: 'totalExperienceInMonths',
      editable: true,
      width: 200,
      required: false,
    },
    {
      title: 'Declared Net Monthly Income',
      dataIndex: 'totalIncomeInMonths',
      editable: true,
      width: 200,
      required: false,
    },
    {
      title: 'Post Qualification Experience',
      dataIndex: 'postQualificationExpInMonths',
      editable: true,
      width: 200,
      required: false,
    },
    {
      title: 'Business Vintage',
      dataIndex: 'businessVintageInMonths',
      editable: true,
      width: 200,
      required: false,
    },
  ];

  const onChangeValues = data => {
    const dataToSave = [];
    if (data.length > 0) {
      data.forEach(item => {
        const updatedItem = { ...item };
        Object.keys(item).forEach(element => {
          if (item[element] && item[element].toString().includes('Enter')) {
            updatedItem[element] = '';
          }
        });
        if (!isEmpty(_.omit(updatedItem, ['key']))) {
          dataToSave.push(updatedItem);
        }
      });
      // dataToSave = data
      //   .filter(
      //     item =>
      //       !item.employmentType.includes('Enter') &&
      //       !item.companyType.includes('Enter') &&
      //       !item.companyIndutry.includes('Enter') &&
      //       !item.companyName.includes('Enter') &&
      //       !item.profession.includes('Enter') &&
      //       !item.officialEmail.includes('Enter') &&
      //       !item.currentExpInMonths.includes('Enter') &&
      //       !item.totalExperienceInMonths.includes('Enter') &&
      //       !item.claimedIncomeInMonths.includes('Enter'),
      //   )
      //   .map(item => ({
      //     employmentType: item.employmentType,
      //     companyType: item.companyType,
      //     companyIndutry: item.companyIndutry,
      //     companyName: item.companyName,
      //     profession: item.profession,
      //     officialEmail: item.officialEmail,
      //     currentExpInMonths: item.currentExpInMonths,
      //     totalExperienceInMonths: item.totalExperienceInMonths,
      //     claimedIncomeInMonths: item.claimedIncomeInMonths,
      //   }));
    }
    setValue(`applicants[${userIndex}].userEmployments`, dataToSave);
  };

  return (
    <EditableTable
      initDataSource={initDataSource}
      initColumns={initColumns}
      onChangeValues={onChangeValues}
      columnsGiven
      notShowDeleteBtn
      addTitle="Add Employment"
      disabled={disabled}
    />
  );
}

export default memo(Employment);
