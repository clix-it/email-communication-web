/**
 *
 * DedupeLoanForm
 *
 */

import _ from 'lodash';
import { Row, Col, Typography } from 'antd';
import React, { memo } from 'react';
import moment from 'moment';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

const { Text } = Typography;

function DedupeLoanForm({ defaultValues }) {
  const fetchDate = (accNumber) => {
    if(accNumber){
      var matches = accNumber.match(/(\d+)/);
      if(matches && matches[0]){
        let date = `20${matches[0].substring(4,6)}-${matches[0].substring(2,4)}-${matches[0].substring(0,2)}`;
        let formattedDate = moment(date).format('DD-MM-YYYY')
        return formattedDate != 'Invalid date' ? formattedDate : '';
      } else {
        return '';
      } 
    } else {
      return '';
    }
  }
  return (
    <Row gutter={[16, 16]}>
      <Col {...responsiveColumns}>
        <Text strong>Loan Type : </Text>
        <Text>{defaultValues.loanType}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Loan Amount : </Text>
        <Text>{defaultValues.loanAmount}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Tenure : </Text>
        <Text>{defaultValues.tenure}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>EMI : </Text>
        <Text>{defaultValues.emi}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>EMI Due Date : </Text>
        <Text>{defaultValues.emi_DueDate}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Customer Unique ID : </Text>
        <Text>{defaultValues.customerUnique_ID}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Loan Account Number : </Text>
        <Text>{defaultValues.loanAccountNumber}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Interest Rate : </Text>
        <Text>{defaultValues.interestRate}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>EMI Cycle : </Text>
        <Text>{defaultValues.emi_Cycle}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Total Loan Amount : </Text>
        <Text>{defaultValues.totalLoanAmount}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Overdue Amount : </Text>
        <Text>{defaultValues.loanAmountDue}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Last Payment Made : </Text>
        <Text>{defaultValues.lastPaymentMade}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Next Payment Due Date : </Text>
        <Text>{defaultValues.nextPaymentDueDate}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Emi Amount : </Text>
        <Text>{defaultValues.amountPayable}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Status : </Text>
        <Text>{defaultValues.status}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Lms System : </Text>
        <Text>{defaultValues.lmsSystem}</Text>
      </Col>
      <Col {...responsiveColumns}>
        <Text strong>Loan StartDate / Application Date : </Text>
        {defaultValues.lmsSystem != 'induslos' ? (
          <Text>
            {defaultValues.loan_start_date
              ? moment(defaultValues.loan_start_date).format('DD-MM-YYYY')
              : ''}
          </Text>
        ) : (
          fetchDate(defaultValues.loanAccountNumber)
        )}
      </Col>
    </Row>
  );
}

DedupeLoanForm.propTypes = {};

export default memo(DedupeLoanForm);
