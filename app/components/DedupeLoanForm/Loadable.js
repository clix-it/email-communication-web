/**
 *
 * Asynchronously loads the component for DedupeLoanForm
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
