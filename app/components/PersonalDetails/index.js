import _ from 'lodash';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import ApplicantPage from 'containers/ApplicantPage';
import { Row, Col, Button, Empty, Spin, message, Menu } from 'antd';
import { formatDate } from 'utils/helpers';
import AddressSection from '../../containers/AddressSection';

import {
  removeApplicant,
  updateApplicantDetails,
  loadEntityDetails,
  clearMessage,
  generateCibilReport,
  addApplicant,
  setSelectedApplicant,
  isDirtyForm,
  unlinkApplicant,
  setFormValidationError,
  updateCoApplicants,
} from '../../containers/ApplicantDetails/actions';
import { columnResponsiveLayout } from '../EntityDetails/constants';
import './personalStyle.css';
function PersonalDetails({
  appId,
  applicantsData,
  dispatch,
  applicationData,
  requestType,
  applicantDetails,
  isNoEntity,
  activity,
}) {
  const {
    loading,
    updateLoading,
    cibilDocUploaded,
    delphiGenerated,
    success,
  } = applicantDetails;

  const [education, setEducation] = React.useState([]);

  const defaultValues = {
    applicants: [
      {
        ...selectedApplicant,
        entityOfficers: _.get(selectedApplicant, 'entityOfficers', {}),
        userIdentities: _.get(selectedApplicant, 'userIdentities')
          ? _.get(selectedApplicant, 'userIdentities')
          : { pan: '' },
        userEmployments: _.get(selectedApplicant, 'userEmployments[0].id')
          ? _.get(selectedApplicant, 'userEmployments')
          : [],
        userEducations: _.get(selectedApplicant, 'userEducations[0].id')
          ? _.get(selectedApplicant, 'userEducations')
          : [],
        userDetails: _.get(selectedApplicant, 'userDetails', {}),
        dateOfBirthIncorporation: formatDate(
          _.get(selectedApplicant, 'dateOfBirthIncorporation', ''),
        ),
        firstName: _.get(selectedApplicant, 'firstName', ''),
        lastName: _.get(selectedApplicant, 'lastName', ''),
        middleName: _.get(selectedApplicant, 'middleName', ''),
        salutation: _.get(selectedApplicant, 'salutation', ''),
        preferredEmail: _.get(selectedApplicant, 'preferredEmail', ''),
        preferredPhone: _.get(selectedApplicant, 'preferredPhone', ''),
      },
    ],
    contactArr,
    'applicants[0].userDetails.gender': _.get(
      selectedApplicant,
      'userDetails.gender',
      '',
    ),
    // 'applicants[0].userDetails.any_build_up_property': _.get(
    //   selectedApplicant,
    //   'userDetails.any_build_up_property',
    //   '',
    // ),
    'applicants[0].userDetails.per_share_holding_if_any': _.get(
      selectedApplicant,
      'userDetails.per_share_holding_if_any',
      '',
    ),
  };

  const product =
    applicationData &&
    applicationData.entity &&
    _.get(applicationData.entity, 'product');
  const { entity, selectedApplicant } = applicationData;
  let contactArr = {};
  let cibilScore = '';
  let foir = '';
  if (selectedApplicant && selectedApplicant.cuid) {
    if (
      entity &&
      entity.additionalData &&
      entity.additionalData.data &&
      entity.additionalData.data.extraDataField &&
      entity.additionalData.data.extraDataField.consumerCibilScore
    ) {
      const consumerCibilScore = JSON.parse(
        entity.additionalData.data.extraDataField.consumerCibilScore,
      );
      cibilScore = consumerCibilScore[`${selectedApplicant.cuid}`] || '';
    }
    if (
      entity &&
      entity.additionalData &&
      entity.additionalData.data &&
      entity.additionalData.data.extraDataField &&
      entity.additionalData.data.extraDataField.foir
    ) {
      foir = entity.additionalData.data.extraDataField.foir || '';
    }
  }

  useEffect(() => {
    if (isNoEntity) {
      dispatch(setFormValidationError('all', {}));
    }
    reset();
  }, [appId]);

  if (selectedApplicant && selectedApplicant.cuid) {
    const contactibilities = _.orderBy(
      _.get(selectedApplicant, 'contactibilities'),
      'id',
      'desc',
    );
    if (contactibilities && contactibilities.length > 0) {
      // contactibilities.shift();
      if (contactibilities.length === 1) {
        if (!_.get(contactibilities, '[0].contactType'))
          contactArr.Latest = contactibilities;
        else {
          contactArr[
            _.get(contactibilities, '[0].contactType')
          ] = contactibilities;
        }
      } else {
        contactArr = _.mapValues(
          _.groupBy(contactibilities, 'contactType'),
          // clist => clist.map(contact => _.omit(contact, 'contactType')),
        );
        Object.keys(contactArr).forEach(key => {
          if (!key) {
            contactArr.undefined = contactArr[key];
            delete contactArr[key];
          }
        });
      }
    }
    console.log('contactArr', contactArr);
  }

  const setApplicant = id => {
    reset({
      'applicants[0].firstName': '',
      'applicants[0].lastName': '',
      'applicants[0].middleName': '',
      'applicants[0].preferredPhone': '',
      'applicants[0].preferredEmail': '',
      'applicants[0].dateOfBirthIncorporation': '',
      'applicants[0].salutation': '',
      // 'applicants[0].holdingInBuisness': '',
      // 'applicants[0].anotherBuiltInProperty': '',
      'applicants[0].userEmployments[0].id': '',
      'applicants[0].userEmployments[0].employmentType': '',
      'applicants[0].userEmployments[0].companyType': '',
      'applicants[0].userEmployments[0].companyIndutry': '',
      'applicants[0].userEmployments[0].companyName': '',
      'applicants[0].userEmployments[0].profession': '',
      'applicants[0].userEmployments[0].officialEmail': '',
      'applicants[0].userEmployments[0].currentExpInMonths': '',
      'applicants[0].userEmployments[0].totalExperienceInMonths': '',
      'applicants[0].userEmployments[0].totalIncomeInMonths': '',
    });
    const applicant =
      _.find(entity.applicants, { id: id.key }) ||
      _.find(entity.applicants, { id: parseInt(id.key) });
    if (applicant) dispatch(setSelectedApplicant(applicant));
  };

  useEffect(() => {
    if (success.status) {
      dispatch(loadEntityDetails(appId));
    }
    dispatch(clearMessage());
  }, [success.status]);

  useEffect(() => {
    if (cibilDocUploaded) {
      message.success(
        'Cibil Document is Uploaded SuccessFully. You can see the document in Documents Section!!',
        2,
      );
    }
    dispatch(clearMessage());
  }, [cibilDocUploaded]);

  useEffect(() => {
    if (delphiGenerated) {
      message.success('Delphi report generated SuccessFully.!!', 2);
    }
    dispatch(clearMessage());
  }, [delphiGenerated]);

  const {
    handleSubmit,
    errors,
    control,
    watch,
    setError,
    clearError,
    getValues,
    reset,
    setValue,
    triggerValidation,
    formState,
  } = useForm({
    mode: 'onChange',
    defaultValues,
  });

  useEffect(() => {
    dispatch(isDirtyForm({ 1: formState.dirty }));
  }, [formState.dirty]);

  useEffect(() => {
    reset({ applicants: [selectedApplicant] });
  }, [selectedApplicant, reset]);

  const values = watch();
  const onSubmit = data => {
    // if (!formState.isValid) {
    //   triggerValidation();
    //   return message.error('Please fill valid data');
    // }
    console.log('data to save from personal', data);
    const entityCuid = _.get(
      _.find(
        _.get(applicantDetails, 'entity.users') || [],
        ele =>
          (_.get(ele, 'appLMS.role') === 'Applicant' ||
            !_.get(ele, 'appLMS.role')) &&
          ele.type === 'COMPANY',
      ),
      'cuid',
      '',
    );
    // dispatch(updateApplicantDetails(data, reset, values));
    dispatch(updateCoApplicants(data, entityCuid, reset, values));
  };

  /**
   * function to add applicant
   */
  return (
    <Spin
      spinning={loading || updateLoading}
      tip={loading ? 'Loading...' : 'Submitting Data...'}
    >
      <div className="App">
        <Menu
          onClick={setApplicant}
          defaultSelectedKeys={[(selectedApplicant || {}).id]}
          selectedKeys={[String((selectedApplicant || {}).id)]}
          mode="horizontal"
        >
          {_.map(entity.applicants, (applicant, index) => (
            <Menu.Item key={String(applicant.id)}>
              {isNoEntity ? 'Entity' : `Co Applicant ${index + 1}`}
            </Menu.Item>
          ))}
          <Menu.Item key="add">
            <Button type="link" onClick={() => dispatch(addApplicant())}>
              + Applicant
            </Button>
          </Menu.Item>
        </Menu>
        {selectedApplicant ? (
          <>
            {requestType === 'PMA' && !isNoEntity && (
              <Row gutter={[16, 16]}>
                <Col
                  {...columnResponsiveLayout.colSingle}
                  style={{
                    fontSize: '18px',
                    fontWeight: 600,
                    padding: '12px 8px',
                  }}
                >
                  <Button
                    type={selectedApplicant.userLinked ? 'default' : 'primary'}
                    htmlType="button"
                    className="personalDetailsRemoveButton"
                    style={{ marginLeft: '10px' }}
                    disabled={
                      activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'
                    }
                    onClick={() =>
                      dispatch(
                        unlinkApplicant(
                          selectedApplicant.id,
                          !selectedApplicant.userLinked,
                        ),
                      )
                    }
                  >
                    {selectedApplicant.userLinked
                      ? 'Unlink Applicant'
                      : 'Link Applicant'}
                  </Button>
                </Col>
              </Row>
            )}
            <form onSubmit={handleSubmit(onSubmit)}>
              <ApplicantPage
                userIndex={0}
                counter={1}
                control={control}
                errors={errors}
                // values={{}}
                clearError={clearError}
                setError={setError}
                entity={entity}
                values={values}
                applicant={selectedApplicant}
                cuid={selectedApplicant.cuid}
                reset={reset}
                setValue={setValue}
                getValues={getValues}
                triggerValidation={triggerValidation}
                allFieldsDisabled={!selectedApplicant.userLinked}
                dispatch={dispatch}
                requestType={requestType}
                cibilScore={cibilScore}
                foir={foir}
                contactArr={contactArr}
                isNoEntity={isNoEntity}
                activity={activity}
                appId={appId}
                product={product}
                partner={_.get(applicationData, 'entity.partner')}
                defaultValues={defaultValues}
                setEducation={setEducation}
                education={education}
              />
            </form>
          </>
        ) : (
          <Empty />
        )}
        <div style={{ marginBottom: '2em', marginTop: '2em' }}>
          {contactArr &&
            Object.keys(contactArr) &&
            Object.keys(contactArr).length > 0 &&
            Object.keys(contactArr).map((contactType, index) => (
              <AddressSection
                cuid={selectedApplicant.cuid}
                userId={_.get(values, `applicants[${0}].id`, '')}
                contactType={contactType}
                index={index}
                contactArr={contactArr}
                partner={_.get(applicantDetails, 'entity.partner')}
              />
            ))}
        </div>
      </div>
    </Spin>
  );
}

PersonalDetails.propTypes = {
  applicantsData: PropTypes.array,
};

PersonalDetails.defaultProps = {
  applicantsData: [],
};

export default PersonalDetails;
