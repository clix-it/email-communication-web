export const newApplicantDataBluePrint = {
  id: '',
  localities: [],
  addressArray: ['', '', '', '', ''],
  contactibilities: [
    {
      addressLine1: '',
      addressLine2: '',
      addressLine3: '',
      locality: '',
      city: '',
      state: '',
    },
  ],
  userIdentities: {
    pan: '',
  },
  dateOfBirthIncorporation: '',
  firstName: '',
  lastName: '',
  preferredEmail: '',
  preferredPhone: '',
  userDetails: {
    //   "gender": ""
  },
  entityOfficers: {
    designation: '',
  },

  type: 'INDIVIDUAL',
  userLinked: true,
};

export const CIBIL_DATA = {
  applicantSegment: {
    applicantName: { name1: 'vivek', name3: 'Goel' },
    dob: { dobDate: '12-11-1991' },
    ids: [{ type: 'PAN', value: 'BCMPV4424L' }],
    addresses: [
      {
        type: 'Residence',
        address1: 'Maina',
        city: 'Rohtak',
        state: 'Haryana',
        pincode: '124021',
      },
    ],
    gender: 'Male',
    entityId: 'eNTITY23',
  },
  applicationSegment: {
    loanType: 'BUSINESSL',
    consumerId: '1000006946',
    applicationId: '1000006946',
    applicationDate: '02-07-2020',
    loanAmount: 1,
  },
};

export const RELATIONS_WITH_BORROWER = [
  'Proprietor',
  'Director',
  'Partner',
  'Trustee',
  'Shareholder',
  'Property Owner',
  'Secretary',
  'Treasurer',
  'Member',
  'Others',
];
