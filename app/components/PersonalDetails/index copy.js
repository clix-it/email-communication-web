import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';

import { Row, Col, Button, Form, Upload, Spin, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { last } from 'lodash';
import {
  loadCityByPinCode,
  updateEntityDetails,
  loadEntityDetails,
  clearMessage,
} from '../../containers/ApplicantDetails/actions';
import { uploadleadsDocRequest } from '../../containers/LeadDocuments/actions';
import {
  addressesBluPrint,
  columnResponsiveLayout,
} from '../EntityDetails/constants';
import { newApplicantDataBluePrint } from './constants';
import ErrorMessage from '../ErrorMessage';
import './personalStyle.css';

function PersonalDetails({
  appId,
  applicantsData,
  dispatch,
  citiesByPincodes,
  loading,
  updateLoading,
  success,
  error,
  isFileUploading,
  isFileUploaded,
  FileUploadError,
}) {
  console.log('updateapp', applicantsData);
  const [detailsState, setDetailsState] = useState(applicantsData || []);

  useEffect(() => {
    dispatch(loadEntityDetails(appId));
  }, []);

  useEffect(() => {
    if (success && success.success && success.message) {
      message.success(success.message, 2);
    }
    if (error && error.status && error.message) {
      message.error(error.message, 2);
    }
    const timer1 = setTimeout(() => {
      if (success.success) {
        dispatch(loadEntityDetails(appId));
      }
      dispatch(clearMessage());
    }, 1000);
    return () => {
      clearTimeout(timer1);
    };
  }, [success.success, error.error]);

  useEffect(() => {
    if (detailsState && detailsState.length < applicantsData.length)
      setDetailsState(applicantsData);
  }, [applicantsData]);

  const { register, handleSubmit, errors } = useForm();
  const onSubmit = () => {
    const dataToSave = {
      users: [],
    };
    detailsState.forEach((details, i) => {
      dataToSave.users[i] = {};
      if (details.type) {
        dataToSave.users[i].type = details.type;
      }
      if (details.id) {
        dataToSave.users[i].id = details.id;
      }
      dataToSave.users[i].lastName = details.lastName;
      dataToSave.users[i].firstName = details.firstName;
      dataToSave.users[i].dateOfBirthIncorporation =
        details.dateOfBirthIncorporation;
      dataToSave.users[i].preferredPhone = details.phoneNumber;
      dataToSave.users[i].userIdentities = {};
      dataToSave.users[i].userIdentities = {
        // id: details.identities.id,
        pan: details.identities.pan || '',
      };
      dataToSave.users[i].contactibilities = [];
      dataToSave.users[i].contactibilities[0] = {
        addressLine1: details.addressArray[0],
        addressLine2: details.addressArray[1],
        addressLine3: details.addressArray[2],
        locality: details.contactDetails.locality,
        pincode: details.contactDetails.pincode,
        city: details.contactDetails.city,
      };

      if (details.addressArray[4])
        dataToSave.users[i].contactibilities[0].id = details.addressArray[4];
    });
    dispatch(updateEntityDetails(dataToSave));
  };

  /**
   * function to add applicant
   */
  const addDetails = () => {
    setDetailsState([...detailsState, { ...newApplicantDataBluePrint }]);
  };

  /**
   * function to remove applicant by id
   * @param {*} idToRemove
   */
  const removeDetail = idToRemove => {
    let updatedDetails = [...detailsState];
    updatedDetails = updatedDetails.filter(item => item.id !== idToRemove);
    setDetailsState(updatedDetails);
  };

  const handleDetailsChange = e => {
    updateState(e.target.dataset.idx, e.target.name, e.target.value);
  };

  const updateState = (arrayId, fieldName, fieldvalue) => {
    const updatedDetails = [...detailsState];
    updatedDetails[arrayId][fieldName] = fieldvalue;
    setDetailsState(updatedDetails);
  };

  const handleAddressChange = (e, addressIndex) => {
    const updatedDetails = [...detailsState];
    updatedDetails[e.target.dataset.idx].addressArray[addressIndex] =
      e.target.value;
    setDetailsState(updatedDetails);
  };

  const handleContactDetailsChange = (e, fieldName) => {
    const { value } = e.target;
    if (value.length === 6 && fieldName === 'pincode') {
      dispatch(loadCityByPinCode(value, e.target.dataset.idx));
    }
    const updatedDetails = [...detailsState];
    updatedDetails[e.target.dataset.idx].contactDetails[fieldName] = value;
    setDetailsState(updatedDetails);
  };

  const handleIdentitiesChange = (e, fieldName) => {
    const { value } = e.target;
    const updatedDetails = [...detailsState];
    updatedDetails[e.target.dataset.idx].identities[fieldName] = value;
    setDetailsState(updatedDetails);
  };

  const getHorizontalLine = () => (
    <hr
      style={{
        backgroundColor: 'grey',
        height: 3,
      }}
    />
  );

  const beforeUpload = (file, id) => {
    const { uploadedDocs = {} } = detailsState[id];
    const newUploadedDocs = { ...uploadedDocs };
    newUploadedDocs.cibil = file;
    updateState(id, 'uploadedDocs', newUploadedDocs);
    return false;
  };

  const onRemove = id => {
    updateState(id, 'uploadedDocs', {});
  };

  const handleFileUpload = id => {
    const { cibil: file } = detailsState[id].uploadedDocs;
    const reader = new FileReader();
    reader.onload = () => {
      dispatch(
        uploadleadsDocRequest(
          file,
          'user',
          detailsState[id].cuid || appId,
          'CIBIL Bureau Report',
          reader.result,
        ),
      );
    };
    reader.readAsDataURL(file);
  };

  useEffect(() => {
    let city = '';
    const updatedDetails = [...detailsState];
    if (citiesByPincodes.length > 0) {
      citiesByPincodes.forEach((data, index) => {
        if (!data) return;
        city = last(data.states[0].cities);
        updatedDetails[index].contactDetails.city = city.name || '';
        if (city && city.localities) {
          updatedDetails[index].localities = city.localities || [];
        }
        setDetailsState(updatedDetails);
      });
    }
  }, [citiesByPincodes]);

  const getApplicantScreen = details => {
    if (details && details.length > 0) {
      return details.map((val, idx) => {
        const applicantId = `applicant-${idx}`;
        return (
          <>
            <Col
              {...columnResponsiveLayout.colSingle}
              style={{ fontSize: '18px', fontWeight: 600, padding: '12px 8px' }}
            >
              {`Co Applicant ${idx + 1} Information`}
              {details.length > 1 && (
                <Button
                  type="primary"
                  htmlType="button"
                  className="personalDetailsRemoveButton"
                  style={{ marginLeft: '10px' }}
                  onClick={() => removeDetail(val.id)}
                  disabled
                >
                  Remove Applicant
                </Button>
              )}
            </Col>
            <Col
              {...columnResponsiveLayout.colHalf}
              className="personalStyleCol"
            >
              <Form.Item label={`Name of Co Applicant ${idx + 1}`}>
                <input
                  type="text"
                  className="personalInputStyle"
                  data-idx={idx}
                  id={applicantId}
                  name="registeredName"
                  placeholder={`Name of Co Applicant ${idx}`}
                  value={details[idx].registeredName || ''}
                  onChange={handleDetailsChange}
                  ref={register({
                    required: {
                      value: true,
                      message: 'This Field should not be empty!',
                    },
                  })}
                />
                <ErrorMessage name="registeredName" errors={errors} />
              </Form.Item>
            </Col>
            <Col
              {...columnResponsiveLayout.colHalf}
              className="personalStyleCol"
            >
              <Form.Item label="Role/Relation with Borrower Entity">
                <input
                  type="text"
                  className="personalInputStyle"
                  data-idx={idx}
                  id={applicantId}
                  name="relation"
                  placeholder="Role/Relation with Borrower Entity"
                  // value={details[idx].locality}
                  onChange={handleDetailsChange}
                  ref={register}
                />
              </Form.Item>
            </Col>
            <>
              {addressesBluPrint &&
                addressesBluPrint.map((address, index) => (
                  <Col
                    {...columnResponsiveLayout.colHalf}
                    className="personalStyleCol"
                  >
                    <Form.Item label={`Address Line ${index + 1}`}>
                      <input
                        type="text"
                        className="personalInputStyle"
                        data-idx={idx}
                        id={applicantId}
                        name={`addressLine${index}`}
                        placeholder={`Address Line ${index + 1}`}
                        onChange={e => handleAddressChange(e, index)}
                        value={
                          details[idx].addressArray &&
                          details[idx].addressArray.length > 0
                            ? details[idx].addressArray[index]
                            : ''
                        }
                        ref={register({
                          required: {
                            value: true,
                            message: 'This Field should not be empty!',
                          },
                        })}
                      />
                      <ErrorMessage
                        name={`addressLine${index}`}
                        errors={errors}
                      />
                    </Form.Item>
                  </Col>
                ))}
              <Col
                {...columnResponsiveLayout.colHalf}
                className="personalStyleCol"
              >
                <Form.Item label="Pincode">
                  <input
                    type="text"
                    className="personalInputStyle"
                    data-idx={idx}
                    id={applicantId}
                    name="pincode"
                    placeholder="Pincode"
                    value={
                      details[idx].contactDetails
                        ? details[idx].contactDetails.pincode
                        : ''
                    }
                    onChange={e => handleContactDetailsChange(e, 'pincode')}
                    ref={register({
                      required: {
                        value: true,
                        message: 'This Field should not be empty!',
                      },
                      minLength: {
                        value: 6,
                        message: 'The length of Pincode should be equal to 6!',
                      },
                    })}
                  />
                  <ErrorMessage name="pincode" errors={errors} />
                </Form.Item>
              </Col>
              <Col
                {...columnResponsiveLayout.colHalf}
                className="personalStyleCol"
              >
                <Form.Item label="City">
                  <input
                    type="text"
                    className="personalInputStyle"
                    data-idx={idx}
                    id={applicantId}
                    name="city"
                    placeholder="Co-Applicant City"
                    value={
                      details[idx].contactDetails
                        ? details[idx].contactDetails.city
                        : ''
                    }
                    onChange={e => handleContactDetailsChange(e, 'city')}
                    ref={register({
                      required: {
                        value: true,
                        message: 'This Field should not be empty!',
                      },
                    })}
                  />
                  <ErrorMessage name="city" errors={errors} />
                </Form.Item>
              </Col>
              <Col
                {...columnResponsiveLayout.colHalf}
                className="personalStyleCol"
              >
                <Form.Item label="Locality">
                  <select
                    ref={register({
                      required: {
                        value: true,
                        message: 'This Field should not be empty!',
                      },
                    })}
                    // defaultValue={gstn && gstn.length > 0 && gstn[0].gstinId}
                    className="personalInputStyle"
                    data-idx={idx}
                    id={applicantId}
                    name="locality"
                    value={
                      (details[idx].contactDetails &&
                        details[idx].contactDetails.locality) ||
                      ''
                    }
                    onChange={e => handleContactDetailsChange(e, 'locality')}
                  >
                    {details[idx].localities.map(loc => (
                      <option value={loc}>{loc}</option>
                    ))}
                  </select>
                  <ErrorMessage name="locality" errors={errors} />
                </Form.Item>
              </Col>
              <Col
                {...columnResponsiveLayout.colHalf}
                className="personalStyleCol"
              >
                <Form.Item label="Country">
                  <input
                    type="text"
                    className="personalInputStyle"
                    data-idx={idx}
                    id={applicantId}
                    name="country"
                    placeholder="Co-Applicant Mobile Number"
                    value="India"
                    disabled
                    ref={register}
                  />
                </Form.Item>
              </Col>
            </>
            <Col
              {...columnResponsiveLayout.colHalf}
              className="personalStyleCol"
            >
              <Form.Item label="Co-Applicant Mobile Number*">
                <input
                  type="text"
                  className="personalInputStyle"
                  data-idx={idx}
                  id={applicantId}
                  name="phoneNumber"
                  placeholder="Co-Applicant Mobile Number"
                  value={details[idx].phoneNumber || ''}
                  onChange={handleDetailsChange}
                  ref={register({
                    required: {
                      value: true,
                      message: 'This Field should not be empty!',
                    },
                    pattern: {
                      value: /^([+]\d{2})?\d{10}$/,
                      message: 'Invalid Mobile Number',
                    },
                  })}
                />
                <ErrorMessage name="phoneNumber" errors={errors} />
              </Form.Item>
            </Col>
            <Col
              {...columnResponsiveLayout.colHalf}
              className="personalStyleCol"
            >
              <Form.Item label="Date Of Birth*">
                <input
                  type="text"
                  className="personalInputStyle"
                  data-idx={idx}
                  id={applicantId}
                  name="dob"
                  placeholder="DOB"
                  defaultValue={details[idx].dateOfBirthIncorporation}
                  ref={register({
                    required: {
                      value: true,
                      message: 'This Field should not be empty!',
                    },
                  })}
                />
                <ErrorMessage name="dob" errors={errors} />
              </Form.Item>
            </Col>
            <Col
              {...columnResponsiveLayout.colHalf}
              className="personalStyleCol"
            >
              <Form.Item label="PAN/CKYC ID*">
                <input
                  type="text"
                  className="personalInputStyle"
                  data-idx={idx}
                  id={applicantId}
                  name="pan"
                  placeholder={`PAN/CKYC ID ${idx}`}
                  value={
                    details[idx] && details[idx].identities
                      ? details[idx].identities.pan
                      : ''
                  }
                  // disabled={!!details[idx].id}
                  onChange={e => handleIdentitiesChange(e, 'pan')}
                  ref={register({
                    required: {
                      value: true,
                      message: 'This Field should not be empty!',
                    },
                  })}
                />
                <ErrorMessage name="dob" errors={errors} />
              </Form.Item>
            </Col>
            <Col
              {...columnResponsiveLayout.colSingle}
              className="personalStyleCol"
            >
              <Form.Item label="Generate CIBIL Bureau Report (You can see the Uploaded File in Documents Section)">
                <Upload
                  accept=".jpg, .png, .pdf"
                  // customRequest={handleFileUpload}
                  beforeUpload={file => beforeUpload(file, idx)}
                  multiple={false}
                  fileList={
                    details[idx].uploadedDocs && details[idx].uploadedDocs.cibil
                      ? [details[idx].uploadedDocs.cibil]
                      : []
                  }
                  onRemove={() => onRemove(idx)}
                >
                  <Button>
                    <UploadOutlined />
                    Select File To Upload
                  </Button>
                </Upload>
                <Button
                  type="primary"
                  onClick={() => handleFileUpload(idx)}
                  style={{ marginTop: 16 }}
                  disabled={
                    !(
                      details[idx].uploadedDocs &&
                      Object.keys(details[idx].uploadedDocs).length > 0 &&
                      details[idx].uploadedDocs.cibil
                    )
                  }
                >
                  Start Upload
                </Button>
              </Form.Item>
            </Col>
            {details.length > 1 && idx < details.length - 1 && (
              <Col
                {...columnResponsiveLayout.colSingle}
                className="personalStyleCol"
              >
                {getHorizontalLine()}
              </Col>
            )}
          </>
        );
      });
    }
    return <div />;
  };

  return (
    <Spin
      spinning={loading || updateLoading}
      tip={loading ? 'Loading...' : 'Submitting Data...'}
    >
      <div className="App">
        <form>
          <Row gutter={[16, 24]}>{getApplicantScreen(detailsState)}</Row>
          <Button
            type="primary"
            htmlType="button"
            className="personalDetailsButton"
            style={{ marginRight: '10px' }}
            onClick={() => addDetails()}
          >
            Add New Applicant
          </Button>
          <Button
            type="primary"
            htmlType="button"
            className="personalDetailsButton"
            onClick={handleSubmit(onSubmit)}
          >
            Save
          </Button>
        </form>
      </div>
    </Spin>
  );
}

PersonalDetails.propTypes = {
  applicantsData: PropTypes.array,
};

PersonalDetails.defaultProps = {
  applicantsData: [],
};

export default PersonalDetails;
