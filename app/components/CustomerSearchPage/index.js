import React, { useEffect, useState, memo } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import _ from 'lodash';
import styled from 'styled-components';
import { Table, Input, Spin, message, Typography } from 'antd';

const Div = styled.div`
  text-align: center;
  margin-bottom: 1em;
`;

const { Text } = Typography;

function CustomerSearchPage({
  users,
  error,
  dispatch,
  loading,
  setCurrentUser,
  searchCustomer,
}) {
  useEffect(() => {
    if (users.length > 0) {
      setDataSource(users);
      setVisible(true);
    } else {
      setDataSource([]);
      setVisible(false);
    }
  }, [users]);

  const [dataSource, setDataSource] = useState([]);
  const [visible, setVisible] = useState(false);
  const [searchVal, setSearchVal] = useState('');

  const columns = [
    {
      title: 'S No.',
      dataIndex: 'index',
      key: 'index',
      align: 'center',
      ellipsis: true,
      width: 80,
    },
    {
      title: 'DOB/DOI',
      dataIndex: 'DOB',
      key: 'DOB',
      align: 'center',
    },
    {
      title: 'CUID',
      dataIndex: 'cuid',
      key: 'cuid',
      align: 'center',
      render: (cuid, record) => (
        <Link
          onClick={() => dispatch(setCurrentUser(record))}
          to={`${location.pathname}/${cuid}`}
        >
          {cuid}
        </Link>
      ),
    },
    {
      title: 'PAN',
      dataIndex: 'pan',
      key: 'pan',
      align: 'center',
    },
  ];

  const locale = {
    emptyText: 'No data to show',
  };

  const debounce = (func, delay) => {
    let inDebounce;
    return function() {
      const context = this;
      const args = arguments;
      clearTimeout(inDebounce);
      inDebounce = setTimeout(() => func.apply(context, args), delay);
    };
  };

  const getUsers = debounce(query => {
    dispatch(searchCustomer(query));
  }, 1000);

  const handleSearch = e => {
    let input = e.target.value;
    getUsers(input);
  };

  return (
    <Spin spinning={loading} tip="Loading...">
      <Div>
        <Input
          type="text"
          onChange={handleSearch}
          style={{
            width: 200,
          }}
          placeholder="Enter PAN/MOBILE/EMAIL"
        />
      </Div>

      <Table
        locale={locale}
        dataSource={dataSource || []}
        pagination={{ defaultPageSize: 10, showSizeChanger: true }}
        columns={columns}
      />
    </Spin>
  );
}

CustomerSearchPage.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  dispatch: PropTypes.func.isRequired,
};

CustomerSearchPage.defaultProps = {
  users: [],
  loading: false,
};

export default memo(CustomerSearchPage);
