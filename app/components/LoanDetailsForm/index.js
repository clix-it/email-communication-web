import React, { memo, useEffect } from 'react';

import { Form, Col } from 'antd';
import _ from 'lodash';

import InputField from '../InputField';
import {
  LOAN_OPTIONS_BL,
  LOAN_OPTIONS_PL,
  LOAN_TENURE_OPTIONS_PL,
  LOAN_TENURE_OPTIONS_BL,
  columnResponsiveLayout,
  RESTRUCTURE_REASON,
  RESTRUCTURE_TAGGING,
  RESTRUCTURE_FEATURE,
} from '../../containers/LoanDetails/constants';
import { setFormValidationError } from '../../containers/ApplicantDetails/actions';
import StepUpTable from '../StepUpTable';

const ECLGS_PROG_TYPE = ['ECLGS', 'ECLGS-LAEP'];
// const X_SELL_PROG_TYPE = ['X2CBL-S4', 'X2CBL-S1', 'X2CBL-S2', 'X2CBL-S3'];
const PROG_TYPE = {
  X2C: ['X2CBL-S4', 'X2CBL-S1', 'X2CBL-S2', 'X2CBL-S3'],
  RLA: [],
};

function LoanDetailsForm({
  control,
  errors,
  values,
  triggerValidation,
  dispatch,
  product,
  requestType,
  setValue,
  // loanTenure,
  isNoEntity,
  partner,
  masterProgData,
  progType,
  loanPlan,
  loanFlag,
  grace,
  masterRestructureData,
  masterLoanTenureData,
  masterLoanFlagData,
  stepUpArr,
  loanChargesLabel,
  activity,
  masterFeeCodeData,
  forclosureAmount,
  feeCodeObj,
  masterLoanPurposeData,
  finStartDate,
  lmsSystem,
}) {
  useEffect(() => {
    if (requestType === 'PMA')
      triggerValidation().then(valid => {
        dispatch(setFormValidationError('loanDetails', valid));
      });
  }, [product]);

  const checkFinalLoanAmount = () => {
    // isNoEntity ? 1000000 : 1000000000
    const scheme = _.get(values, 'scheme');
    if (scheme === 'X2CBL-S1' || scheme === 'X2CBL-S2') return 5000000;
    if (scheme === 'X2CBL-S3' || scheme === 'X2CBL-S4') return 4000000;
    return isNoEntity ? 1000000 : 1000000000;
  };

  const onLoanPlanChange = value => {
    const key = _.findKey(masterRestructureData, function(o) {
      return o == value;
    });
    if (key && key == 'NORMAL') {
      setValue('additionalData.data.extraDataField.grace', '0');
    }
  };

  const checkDisabledGrace = () => {
    let disabled = true;
    const key = _.findKey(masterRestructureData, function(o) {
      return o == values['additionalData.data.extraDataField.loanPlan'];
    });
    if (
      key == 'NORMAL' ||
      values['additionalData.data.extraDataField.loanPlan'] == 'NORMAL'
    ) {
      disabled = true;
    } else {
      disabled = false;
    }
    return disabled;
  };

  const checkStepUp = () => {
    let flag = false;
    if (
      values['additionalData.data.extraDataField.loanPlan'] &&
      values['additionalData.data.extraDataField.loanPlan']
        .toUpperCase()
        .includes('STEP')
    ) {
      flag = true;
    } else if (
      loanPlan &&
      !values['additionalData.data.extraDataField.loanPlan'] &&
      loanPlan.toUpperCase().includes('STEP')
    ) {
      flag = true;
    } else {
      flag = false;
    }
    return flag;
  };

  const isLoanPlanDisabled = () => {
    if (
      partner === 'HFSAPP' &&
      activity !== 'CREDIT_PSV' &&
      activity !== 'CREDIT_REVIEW'
    )
      return true;
    return false;
  };

  return (
    <>
      <Col {...columnResponsiveLayout.colHalf}>
        {(progType ||
          (masterProgData && Object.values(masterProgData).length > 0)) && (
          <InputField
            id="scheme"
            name="scheme"
            type="select"
            defaultValue={progType}
            options={masterProgData && Object.values(masterProgData)}
            control={control}
            // disabled={product !== 'BL'}
            errors={errors}
            labelHtml="Prog Type"
          />
        )}
      </Col>
      <Col {...columnResponsiveLayout.colHalf}>
        <InputField
          id="appliedLoanAmount"
          name="appliedLoanAmount"
          control={control}
          type="string"
          disabled
          errors={errors}
          labelHtml="Applied Loan Amount*"
        />
      </Col>
      {partner === 'X2C' && (
        <>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="loanOffersIncremental[0].loanAmount"
              control={control}
              name="loanOffersIncremental[0].loanAmount"
              type="number"
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
                pattern: {
                  value: /[0-9]/i,
                  message: 'Please enter valid number',
                },
                min: {
                  value: 100000,
                  message: 'Loan amount should be greater than 1 lakh',
                },
                validate: value =>
                  Number.isInteger(parseFloat(value)) ||
                  'value should not be in decimal',
              }}
              errors={errors}
              placeholder="Offered/Incremental Loan Amount"
              labelHtml="Offered/Incremental Loan Amount*"
            />
          </Col>
        </>
      )}
      {partner === 'RLA' &&
        activity == 'CREDIT_PSV' &&
        (lmsSystem || finStartDate) && (
          <>
            <Col {...columnResponsiveLayout.colHalf}>
              <InputField
                id="finStartDate"
                control={control}
                name="finStartDate"
                type="datepicker"
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="Loan Value Date"
                labelHtml="Loan Value Date*"
              />
            </Col>
          </>
        )}
      {partner === 'DSA' && (
        <>
          {(loanFlag || Object.values(masterLoanFlagData).length > 0) && (
            <Col {...columnResponsiveLayout.colHalf}>
              <InputField
                id="additionalData.data.extraDataField.loanFlag"
                control={control}
                name="additionalData.data.extraDataField.loanFlag"
                // defaultValue={applicantDetails.entity.product}
                type="select"
                rules={{
                  required: true,
                  message: 'This Field should not be empty!',
                }}
                defaultValue={loanFlag}
                options={
                  masterLoanFlagData && Object.values(masterLoanFlagData)
                }
                errors={errors}
                placeholder="Loan FLag"
                labelHtml="Loan FLag"
              />
            </Col>
          )}
        </>
      )}
      {(partner === 'RLA' || partner === 'HFSAPP') && (
        <>
          {(loanPlan || Object.values(masterRestructureData).length > 0) && (
            <Col {...columnResponsiveLayout.colHalf}>
              <InputField
                id="additionalData.data.extraDataField.loanPlan"
                control={control}
                name="additionalData.data.extraDataField.loanPlan"
                // defaultValue={applicantDetails.entity.product}
                disabled={isLoanPlanDisabled()}
                type="select"
                rules={{
                  required: false,
                  message: 'This Field should not be empty!',
                }}
                defaultValue={loanPlan}
                handleChange={onLoanPlanChange}
                options={
                  masterRestructureData && Object.values(masterRestructureData)
                }
                errors={errors}
                placeholder={
                  partner === 'HFSAPP' ? 'Morat Impact' : 'Restructure Feature'
                }
                labelHtml={
                  partner === 'HFSAPP' ? 'Morat Impact' : 'Restructure Feature'
                }
              />
            </Col>
          )}
        </>
      )}
      {product !== 'PL' && (
        <Col {...columnResponsiveLayout.colHalf}>
          <InputField
            id="additionalData.data.extraDataField.grace"
            control={control}
            defaultValue={
              partner === 'D2C' || partner === 'LAEP' ? '12' : grace
            }
            name="additionalData.data.extraDataField.grace"
            type="string"
            rules={{
              required: {
                value: false,
                message: 'This field cannot be left empty',
              },
              pattern: {
                value: /[0-9]/i,
                message: 'Please enter valid number',
              },
            }}
            errors={errors}
            placeholder="Grace Period (Months)"
            labelHtml="Grace Period (Months)"
            disabled={checkDisabledGrace() || partner === 'DSA'}
          />
        </Col>
      )}
      {forclosureAmount && (
        <Col {...columnResponsiveLayout.colHalf}>
          <InputField
            id="forclosureAmount"
            control={control}
            name="forclosureAmount"
            type="number"
            disabled
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
            }}
            errors={errors}
            placeholder="Forclosure Amount"
            labelHtml="Forclosure Amount"
          />
        </Col>
      )}
      {partner === 'XSELL' && (
        <>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="loanOffersQualified[0].loanAmount"
              control={control}
              defaultValue={_.get(values, 'loanOffersQualified[0].loanAmount')}
              name="loanOffersQualified[0].loanAmount"
              type="number"
              disabled
              errors={errors}
              placeholder="Qualified Loan Amount"
              labelHtml="Qualified Loan Amount*"
            />
          </Col>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="loanOffersQualified[0].loanTenure"
              name="loanOffersQualified[0].loanTenure"
              control={control}
              defaultValue={_.get(values, 'loanOffersQualified[0].loanTenure')}
              type="number"
              disabled
              errors={errors}
              placeholder="Number of Installments (in months)"
              labelHtml="Qualified Offer - Number of Installments (in months)*"
            />
          </Col>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="loanOffersQualified[0].roi"
              control={control}
              defaultValue={_.get(values, 'loanOffersQualified[0].roi')}
              name="loanOffersQualified[0].roi"
              type="string"
              disabled
              errors={errors}
              placeholder="Rate Of Interest(roi)"
              labelHtml="Qualified Offer - Rate Of Interest(roi)"
            />
          </Col>
        </>
      )}
      <Col {...columnResponsiveLayout.colHalf}>
        <InputField
          id="loanOffersEligible[0].loanAmount"
          control={control}
          name="loanOffersEligible[0].loanAmount"
          type="number"
          rules={{
            required: {
              value: true,
              message: 'This field cannot be left empty',
            },
            pattern: {
              value: /[0-9]/i,
              message: 'Please enter valid number',
            },
            min: {
              value:
                activity === 'CREDIT_PSV'
                  ? 0
                  : partner === 'HFSAPP'
                  ? 200000
                  : product === 'PL'
                  ? 25000
                  : 10000,
              // value: 10000,
              message:
                activity === 'CREDIT_PSV'
                  ? 'Loan amount should be greater than 0'
                  : partner === 'HFSAPP'
                  ? 'Loan amount should be greater than 2,00,000'
                  : product === 'PL'
                  ? 'Loan amount should be greater than 25,000'
                  : 'Loan amount should be greater than 10,000',
            },
            // max: {
            //   value: checkFinalLoanAmount(),
            //   message: `Loan amount should be less than ${
            //     isNoEntity ? '10 Lakh' : '100 Crore'
            //   }`,
            // },
            validate: value =>
              Number.isInteger(parseFloat(value)) ||
              'value should not be in decimal',
          }}
          errors={errors}
          placeholder="Final/Gross Loan Amount"
          labelHtml="Final/Gross Loan Amount*"
        />
      </Col>
      {Object.values(masterLoanTenureData).length == 0 && (
        <Col {...columnResponsiveLayout.colHalf}>
          <InputField
            id="loanOffersEligible[0].loanTenure"
            name="loanOffersEligible[0].loanTenure"
            control={control}
            defaultValue={_.get(values, 'loanOffersEligible[0].loanTenure')}
            type="number"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              pattern: {
                value: /^([1-9][0-9]*)$/,
                message: 'Please enter valid number',
              },
            }}
            // options={Object.values(masterLoanTenureData)}
            errors={errors}
            placeholder="Number of Installments (in months)"
            labelHtml="Number of Installments (in months)*"
          />
        </Col>
      )}
      {partner === 'RLA' &&
        (loanPlan || values['additionalData.data.extraDataField.loanPlan']) &&
        checkStepUp() && (
          <Col {...columnResponsiveLayout.colSingle}>
            <Form.Item name="stepUp" label="Step Up Details">
              <StepUpTable
                values={values}
                setValue={setValue}
                stepUpArr={stepUpArr}
              />
            </Form.Item>
          </Col>
        )}
      {Object.values(masterLoanTenureData).length > 0 && (
        <Col {...columnResponsiveLayout.colHalf}>
          <InputField
            id="loanOffersEligible[0].loanTenure"
            name="loanOffersEligible[0].loanTenure"
            control={control}
            defaultValue={_.get(values, 'loanOffersEligible[0].loanTenure')}
            type="select"
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              pattern: {
                value: /^([1-9][0-9]*)$/,
                message: 'Please enter valid number',
              },
            }}
            options={Object.values(masterLoanTenureData)}
            errors={errors}
            placeholder="Number of Installments (in months)"
            labelHtml="Number of Installments (in months)*"
          />
        </Col>
      )}
      <Col {...columnResponsiveLayout.colHalf}>
        <InputField
          id="loanOffersEligible[0].roi"
          control={control}
          name="loanOffersEligible[0].roi"
          type="string"
          rules={{
            required: {
              value: true,
              message: 'This field cannot be left empty',
            },
            pattern: {
              value: /^(?=.*[1-9])[0-9]*[.]?[0-9]*$/,
              message: 'Please enter valid number',
            },
          }}
          errors={errors}
          placeholder="Rate Of Interest(roi)"
          labelHtml="Rate Of Interest(roi)"
        />
      </Col>
      {Object.keys(masterFeeCodeData).length > 0 &&
        Object.keys(masterFeeCodeData).map((item, index) => (
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id={`loanChargesValue[${index}].amount`}
              control={control}
              name={`loanChargesValue[${index}].amount`}
              type="string"
              errors={errors}
              placeholder={item}
              defaultValue={feeCodeObj[item]}
              labelHtml={item}
              rules={{
                required: false,
                pattern: {
                  value: /^[0-9]+([.][0-9]+)?$/,
                  message: 'Please enter valid number',
                },
              }}
            />
          </Col>
        ))}
      {product === 'HFS' && (
        <Col {...columnResponsiveLayout.colHalf}>
          <InputField
            id="securityDepositAmount"
            control={control}
            name="securityDepositAmount"
            type="string"
            errors={errors}
            placeholder="Enter Security Deposit"
            labelHtml="Security Deposit"
            rules={{
              required: false,
              pattern: {
                value: /^[0-9]+([.][0-9]+)?$/,
                message: 'Please enter valid number',
              },
            }}
          />
        </Col>
      )}
      {!isNoEntity && (
        <>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="loanOffersEligible[0].advanceEMI"
              control={control}
              name="loanOffersEligible[0].advanceEMI"
              type="select"
              rules={{
                required: {
                  value: !isNoEntity,
                  message: 'This Field should not be empty!',
                },
              }}
              options={[0, 1, 2, 3, 4]}
              errors={errors}
              placeholder="Please select your Advance EMI"
              labelHtml="Advance EMI*"
            />
          </Col>
          {_.get(values, 'scheme') === 'ECLGS' && (
            <Col {...columnResponsiveLayout.colHalf}>
              <InputField
                id="morat"
                control={control}
                disabled
                type="number"
                name="morat"
                errors={errors}
                placeholder="Number of morat for Interest (Months)"
                labelHtml="Number of morat for Interest (Months)*"
              />
            </Col>
          )}
        </>
      )}
      {Object.keys(masterLoanPurposeData).length > 0 && (
        <Col {...columnResponsiveLayout.colHalf}>
          <InputField
            id="loanPurpose"
            control={control}
            name="loanPurpose"
            defaultValue={_.get(
              values,
              'loanPurpose',
              Object.keys(masterLoanPurposeData)[0],
            )}
            type="select"
            rules={{
              required: {
                value: true,
                message: 'This Field should not be empty!',
              },
              maxLength: {
                value: 100,
                message: 'Max length 100 characters allowed',
              },
            }}
            options={Object.keys(masterLoanPurposeData)}
            errors={errors}
            placeholder="Please select Purpose of Loan"
            labelHtml="Purpose of Loan*"
          />
        </Col>
      )}
      {partner === 'RLA' && (
        <>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="expectedEMI"
              control={control}
              name="expectedEMI"
              type="string"
              rules={{
                required: false,
                message: 'This Field should not be empty!',
              }}
              placeholder="Comfortable EMI"
              labelHtml="Comfortable EMI"
              disabled
            />
          </Col>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="additionalData.data.extraDataField.restructureReason"
              control={control}
              name="additionalData.data.extraDataField.restructureReason"
              // defaultValue={applicantDetails.entity.product}
              type="select"
              rules={{
                required: true,
                message: 'This Field should not be empty!',
              }}
              options={RESTRUCTURE_REASON}
              errors={errors}
              placeholder="Restructure Reason"
              labelHtml="Restructure Reason"
            />
          </Col>
          <Col {...columnResponsiveLayout.colHalf}>
            <InputField
              id="additionalData.data.extraDataField.restructureTagging"
              control={control}
              name="additionalData.data.extraDataField.restructureTagging"
              // defaultValue={applicantDetails.entity.product}
              type="select"
              rules={{
                required: false,
                message: 'This Field should not be empty!',
              }}
              options={RESTRUCTURE_TAGGING}
              errors={errors}
              placeholder="Restructure Tagging"
              labelHtml="Restructure Tagging"
            />
          </Col>
        </>
      )}
      <Col {...columnResponsiveLayout.colHalf}>
        <InputField
          id="additionalData.data.extraDataField.groupExposure"
          control={control}
          name="additionalData.data.extraDataField.groupExposure"
          type="string"
          rules={{
            pattern: {
              value: /^(0|[1-9][0-9]*)$/,
              message: 'Invalid Number',
            },
            validate: value =>
              Number.isInteger(parseFloat(value)) ||
              'value should not be in decimal',
          }}
          errors={errors}
          placeholder="Clix Existing Exposure"
          labelHtml="Clix Existing Exposure"
        />
      </Col>
    </>
  );
}

export default memo(LoanDetailsForm);
