/**
 *
 * Asynchronously loads the component for LoanDetailsTable
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
