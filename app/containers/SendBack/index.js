/**
 *
 * SendBack
 *
 */

import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useForm, Controller } from 'react-hook-form';
import ErrorMessage from 'components/ErrorMessage';
import makeSelectAppAuthDetails from 'containers/AppAuthorization/selectors';
import { makeSelectApplications } from 'containers/CreditPortal/selectors';

import {
  Button,
  Modal,
  Radio,
  Col,
  Row,
  Spin,
  Divider,
  Input,
  Form,
} from 'antd';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import { makeAppDocumentDetailsData } from 'containers/ApplicantDetails/selectors';
import InputField from 'components/InputField';

import makeSelectSendBack from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { sendBackApp } from './actions';
const { TextArea } = Input;

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
};

export function SendBack({
  applicantDetails,
  sendBack,
  sendBackApp,
  employeeRoles,
  creditListData,
}) {
  useInjectReducer({ key: 'sendBack', reducer });
  useInjectSaga({ key: 'sendBack', saga });

  const { loading, response } = sendBack;

  useEffect(() => {
    if (response) {
      setShowModal(false);
    }
  }, [response]);

  const { handleSubmit, errors, control, reset, watch } = useForm({
    mode: 'onChange',
  });

  const values = watch();

  console.log(values, 'values');

  const [showModal, setShowModal] = useState(false);
  const onSubmit = values => {
    const userActivity = `${values.approver.toUpperCase()}_REVIEW`;
    const creditManagerId =
      _.get(applicantDetails, 'entity.appSourcing.creditManagerId') ||
      JSON.parse(sessionStorage.getItem('userId'));
    const salesManager =
      _.get(applicantDetails, 'entity.appSourcing.salesManager') ||
      JSON.parse(sessionStorage.getItem('userId'));
    let nextActor = creditManagerId;
    if (values.approver == 'sales') {
      nextActor = salesManager;
    }
    sendBackApp({ ...values, userActivity, nextActor });
  };

  const ruleDesc =
    _.get(creditListData, 'currentApplication.ruleDescription') || '';
  const allRoleString = _.map(
    _.get(employeeRoles, 'allRoles') || [],
    'role',
  ).join('');

  if (
    ruleDesc.toLowerCase().includes('doa') &&
    allRoleString.toLowerCase().includes('doa')
  ) {
    return (
      <>
        <Button type="default" onClick={() => setShowModal(true)}>
          Send Back
        </Button>
        <Modal
          title="SendBack Application"
          visible={showModal}
          footer={null}
          onCancel={() => setShowModal(false)}
        >
          <Spin spinning={loading}>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Row>
                <Col {...responsiveColumns}>
                  <Form.Item label="Remarks">
                    <Controller
                      onChange={([e]) => {
                        return e.target.value;
                      }}
                      id="remark"
                      control={control}
                      name="remark"
                      type="textarea"
                      errors={errors}
                      rules={{
                        required: {
                          value: true,
                          message: 'This Field should not be empty!',
                        },
                      }}
                      as={
                        <TextArea
                          placeholder="Remarks"
                          rows={4}
                          labelHtml="Remarks"
                        />
                      }
                    />
                    <ErrorMessage name="remark" errors={errors} />
                  </Form.Item>
                </Col>

                <Col {...responsiveColumns}>
                  <Form.Item label="Credit/Sales">
                    <Controller
                      name="approver"
                      control={control}
                      onChange={([event]) => {
                        return event.target.value;
                      }}
                      rules={{
                        required: {
                          value: true,
                          message: 'Please select either credit or sales',
                        },
                      }}
                      as={
                        <Radio.Group name="approver">
                          <Radio.Button value="credit">
                            Send to Credit
                          </Radio.Button>
                          <Radio.Button value="sales">
                            Send to Sales
                          </Radio.Button>
                        </Radio.Group>
                      }
                    />
                    <ErrorMessage name="approver" errors={errors} />
                  </Form.Item>
                </Col>
              </Row>
              <Divider />
              <Col {...responsiveColumns}>
                <Button htmlType="submit">Submit</Button>
              </Col>
            </form>
          </Spin>
        </Modal>
      </>
    );
  }

  return <div />;
}

SendBack.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  sendBack: makeSelectSendBack(),
  applicantDetails: makeAppDocumentDetailsData(),
  employeeRoles: makeSelectAppAuthDetails(),
  creditListData: makeSelectApplications(),
});

function mapDispatchToProps(dispatch) {
  return {
    sendBackApp: data => dispatch(sendBackApp(data)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(SendBack);
