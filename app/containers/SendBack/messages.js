/*
 * SendBack Messages
 *
 * This contains all the text for the SendBack container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.SendBack';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the SendBack container!',
  },
});
