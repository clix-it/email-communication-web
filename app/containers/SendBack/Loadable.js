/**
 *
 * Asynchronously loads the component for SendBack
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
