import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the sendBack state domain
 */

const selectSendBackDomain = state => state.sendBack || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by SendBack
 */

const makeSelectSendBack = () =>
  createSelector(
    selectSendBackDomain,
    substate => substate,
  );

export default makeSelectSendBack;
export { selectSendBackDomain };
