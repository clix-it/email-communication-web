/*
 *
 * SendBack actions
 *
 */

import {
  SEND_BACK_APPLICATION,
  SEND_BACK_APPLICATION_SUCCESS,
  SEND_BACK_APPLICATION_FAILURE,
} from './constants';

export function sendBackApp(data) {
  return {
    type: SEND_BACK_APPLICATION,
    data,
  };
}

export function sendBackDone(response) {
  return {
    type: SEND_BACK_APPLICATION_SUCCESS,
    response,
  };
}

export function sendBackFailed(error) {
  return {
    type: SEND_BACK_APPLICATION_FAILURE,
    error,
  };
}
