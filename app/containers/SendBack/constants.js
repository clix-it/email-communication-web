/*
 *
 * SendBack constants
 *
 */

export const SEND_BACK_APPLICATION = 'app/SendBack/SEND_BACK_APPLICATION';
export const SEND_BACK_APPLICATION_SUCCESS =
  'app/SendBack/SEND_BACK_APPLICATION_SUCCESS';
export const SEND_BACK_APPLICATION_FAILURE =
  'app/SendBack/SEND_BACK_APPLICATION_FAILURE';
