import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { message } from 'antd';
import request from 'utils/request';
import { SEND_BACK_APPLICATION } from './constants';
import { sendBackDone, sendBackFailed } from './actions';
import { makeSelectApplications } from '../CreditPortal/selectors';

import env from '../../../environment';
// Individual exports for testing

export function* createTransaction(data) {
  const { currentApplication = {} } = yield select(makeSelectApplications());
  const {
    applicationId,
    partner,
    product,
    activityRequestId,
    source,
    followHierarchy,
  } = currentApplication;
  try {
    yield call(
      request,
      `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v1/transaction/create`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({
          userActivity: data.userActivity,
          source: source,
          applicationId: applicationId,
          product: product,
          partner: partner,
          activityRequestId,
          ruleDesc: `1-${data.userActivity[0].toUpperCase()}M`,
          nextActor: data.nextActor,
          panNumber: currentApplication.panNumber,
          customerName: currentApplication.customerName,
          followHierarchy,
        }),
      },
    );
  } catch (err) {
    yield put(sendBackFailed(err));
  }
}
export function* sendBack({ data }) {
  const { currentApplication = {} } = yield select(makeSelectApplications());
  const { activitySrlNo } = currentApplication;
  try {
    // Call our request helper (see 'utils/request')

    const response = yield call(
      request,
      `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v1/user/decision`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({
          activitySrlNo,
          employeeId: JSON.parse(sessionStorage.getItem('userId')),
          status: 'REJECTED',
          remarks: data.remark,
          nextApprovalUserId: '',
        }),
      },
    );

    yield call(createTransaction, data);
    yield put(sendBackDone(response));
    yield message.success(
      `${currentApplication.customerName} sendback successfully`,
    );
    yield put(push('/applicantDetailsPma'));
  } catch (err) {
    yield put(sendBackFailed(err));
    yield message.error(`${currentApplication.customerName} sendback failed`);
  }
}

export default function* sendBackSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(SEND_BACK_APPLICATION, sendBack);
}
