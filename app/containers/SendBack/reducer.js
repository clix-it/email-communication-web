/*
 *
 * SendBack reducer
 *
 */
import produce from 'immer';
import {
  SEND_BACK_APPLICATION,
  SEND_BACK_APPLICATION_SUCCESS,
  SEND_BACK_APPLICATION_FAILURE,
} from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const sendBackReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SEND_BACK_APPLICATION:
        draft.loading = true;
        draft.response = false;
        draft.error = false;
        break;
      case SEND_BACK_APPLICATION_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        draft.error = false;
        break;

      case SEND_BACK_APPLICATION_FAILURE:
        draft.loading = false;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default sendBackReducer;
