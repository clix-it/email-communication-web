import React, { useEffect, useState, memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Table, Spin, message, Button } from 'antd';
import moment from 'moment';
import { formatNumber } from 'utils/helpers';
import _ from 'lodash';
import reducer from './reducer';
import saga from './saga';

import { loadPaymentSchedule } from './actions';
import { makeSelectPaymentSchedule } from './selectors';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';
import { makeSelectApplications } from '../CreditPortal/selectors';

function PaymentSchedule({
  paymentSchedule,
  dispatch,
  applicantDetails,
  creditPortal,
}) {
  useInjectReducer({ key: 'paymentSchedule', reducer });
  useInjectSaga({ key: 'paymentSchedule', saga });
  const {
    paymentSchedules: { schedules = [], summary = {} } = {},
    loading,
  } = paymentSchedule;

  const [dataSource, setDataSource] = useState([]);

  const buildSchedule = () => {
    setDataSource([]);
    const { currentApplication = {} } = creditPortal;
    const { entity = {} } = applicantDetails;
    const stepUpArr = _.get(
      entity,
      'additionalData.data.extraDataField.stepUpDetails',
    )
      ? JSON.parse(
          _.get(entity, 'additionalData.data.extraDataField.stepUpDetails'),
        )
      : [];
    const loanOfferData = _.get(
      _.orderBy(
        _.filter(
          _.get(entity, 'loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      [0],
    ) || { loanAmount: 0, loanTenure: 48, roi: 14 };
    const grace = _.get(entity, 'additionalData.data.extraDataField.grace', '');
    const loanPlan = _.get(
      entity,
      'additionalData.data.extraDataField.loanPlan',
    );
    let bpiTreatment = '';
    if (!loanOfferData.loanAmount)
      return message.warning('Please fill valid Loan Amount!');
    if (stepUpArr && stepUpArr.length > 0)
      return message.warning('Cannot generate schedule for STEP UP plan!');
    const totalPremium = getTotalPremiumOfInsurances(entity);
    const data = {
      loanAmount:
        (parseInt(loanOfferData.loanAmount, 10) + parseInt(totalPremium, 10)) *
        100,
      numberOfTerms: parseInt(loanOfferData.loanTenure, 10),
      roi: Number(loanOfferData.roi),
      product: entity.product,
      partner: entity.partner,
    };
    if (grace) {
      data.grcTerms = grace;
    }
    if (loanPlan) {
      data.loanPlan = loanPlan;
    }
    if (loanPlan) {
      if (data.partner == 'RLA') {
        bpiTreatment = 'E';
      } else {
        bpiTreatment = '';
      }
    } else if (data.partner == 'RLA') {
      bpiTreatment = 'E';
    } else {
      bpiTreatment = 'D';
    }
    if (bpiTreatment) {
      data.bpiTreatment = bpiTreatment;
    }
    dispatch(loadPaymentSchedule(data));
  };

  const getTotalPremiumOfInsurances = entity => {
    let totalPremium = 0;
    if (_.get(entity, 'product', '') === 'BL') {
      const cropInsApplicants = _.compact(
        _.filter(_.get(entity, 'users', []), {
          type: 'INDIVIDUAL',
        }).map(ele => ele.cropInsurance),
      );
      if (cropInsApplicants && cropInsApplicants.length > 0) {
        totalPremium = cropInsApplicants.reduce(
          (acc, { premiumAmount }) => acc + parseInt(premiumAmount, 10),
          0,
        );
      }
    }
    return totalPremium;
  };

  useEffect(() => {
    setDataSource([]);
    const { currentApplication = {} } = creditPortal;
    const { entity = {} } = applicantDetails;
    const stepUpArr = _.get(
      entity,
      'additionalData.data.extraDataField.stepUpDetails',
    )
      ? JSON.parse(
          _.get(entity, 'additionalData.data.extraDataField.stepUpDetails'),
        )
      : [];
    const loanOfferData = _.get(
      _.orderBy(
        _.filter(
          _.get(entity, 'loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      [0],
    ) || { loanAmount: 0, loanTenure: 48, roi: 14 };
    const grace = _.get(entity, 'additionalData.data.extraDataField.grace', '');
    const loanPlan = _.get(
      entity,
      'additionalData.data.extraDataField.loanPlan',
    );
    let bpiTreatment = '';
    if (!loanOfferData.loanAmount)
      return message.warning('Please fill valid Loan Amount!');
    if (stepUpArr && stepUpArr.length > 0)
      return message.warning('Cannot generate schedule for STEP UP plan!');
    const totalPremium = getTotalPremiumOfInsurances(entity);
    const data = {
    loanAmount:
        (parseInt(loanOfferData.loanAmount, 10) + parseInt(totalPremium, 10)) *
        100,
      numberOfTerms: parseInt(loanOfferData.loanTenure, 10),
      roi: Number(loanOfferData.roi),
      product: entity.product,
      partner: entity.partner,
    };
    if (grace) {
      data.grcTerms = grace;
    }
    if (loanPlan) {
      data.loanPlan = loanPlan;
    }
    if (loanPlan) {
      if (data.partner == 'RLA') {
        bpiTreatment = 'E';
      } else {
        bpiTreatment = '';
      }
    } else if (data.partner == 'RLA') {
      bpiTreatment = 'E';
    } else {
      bpiTreatment = 'D';
    }
    if (bpiTreatment) {
      data.bpiTreatment = bpiTreatment;
    }
    dispatch(loadPaymentSchedule(data));
  }, []);

  useEffect(() => {
    let arr = [];
    schedules.forEach((element, i) => {
      let remark = 'Installment';
      if (i === 0) {
        remark = 'Disbursement';
      } else if (
        moment(element.schDate).isBefore(moment(summary.firstInstDate))
      ) {
        if (element.totalAmount == 0) {
          remark = 'EMI Capitalization';
        } else {
          remark = 'BPI Payment';
        }
      } else if (element.schdPri == 0) {
        remark = 'Interest Payment';
      }
      console.log('element', element);
      arr = arr.concat({
        key: element,
        sno: i + 1,
        date: moment(element.schDate).format('DD-MMM-YYYY'),
        remarks: remark,
        interest: formatNumber(element.schdPft / 100),
        principal: formatNumber(element.schdPri / 100),
        totalIns: formatNumber(element.totalAmount / 100),
        endBal: formatNumber(element.endBal / 100),
      });
    });
    setDataSource(arr);
  }, [paymentSchedule]);

  const columns = [
    {
      title: 'S.No',
      dataIndex: 'sno',
      key: 'sno',
      align: 'center',
      ellipsis: true,
    },
    {
      title: 'Date',
      dataIndex: 'date',
      key: 'date',
      align: 'center',
      ellipsis: true,
    },
    {
      title: 'Remarks',
      dataIndex: 'remarks',
      key: 'remarks',
      align: 'center',
    },
    {
      title: 'Interest',
      dataIndex: 'interest',
      key: 'interest',
      align: 'center',
      ellipsis: true,
    },
    {
      title: 'Principal',
      dataIndex: 'principal',
      key: 'principal',
      align: 'center',
      ellipsis: true,
    },
    {
      title: 'Total Installment',
      dataIndex: 'totalIns',
      key: 'totalIns',
      align: 'center',
      ellipsis: true,
    },
    {
      title: 'End Balance',
      dataIndex: 'endBal',
      key: 'endBal',
      align: 'center',
      ellipsis: true,
    },
  ];

  const locale = {
    emptyText: 'No Data Found',
  };

  return (
    <Spin spinning={loading} tip="Loading...">
      <Button type="primary" onClick={buildSchedule}>
        Build
      </Button>
      <Table
        rowClassName={(record, index) => (index === 0 ? 'table-row-bold' : '')}
        locale={locale}
        dataSource={dataSource || []}
        pagination={{ defaultPageSize: 10, showSizeChanger: true }}
        columns={columns}
      />
    </Spin>
  );
}

PaymentSchedule.propTypes = {
  paymentSchedule: PropTypes.arrayOf(PropTypes.object),
  dispatch: PropTypes.func.isRequired,
};

PaymentSchedule.defaultProps = {
  paymentSchedule: [],
};

const mapStateToProps = createStructuredSelector({
  paymentSchedule: makeSelectPaymentSchedule(),
  applicantDetails: makeSelectApplicantDetails2(),
  creditPortal: makeSelectApplications(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PaymentSchedule);
