/**
 * Credit Portal Reducer
 */
import * as actions from './constants';
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';

export const initialState = {
  loading: false,
  paymentSchedules: [],
  error: {},
};

const paymentScheduleReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.LOAD_PAYMENT_SCHEDULE:
        draft.loading = true;
        break;

      case actions.LOAD_PAYMENT_SCHEDULE_SUCCESS:
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.paymentSchedules = action.payload.data;
        break;

      case actions.LOAD_PAYMENT_SCHEDULE_ERROR:
        draft.loading = false;
        draft.paymentSchedules = [];
        draft.error = {
          status: true,
          message: action.payload.errorMessage,
          severity: action.severity,
        };
        break;

      case LOCATION_CHANGE:
        draft.loading = false;
        draft.paymentSchedules = [];
        draft.error = {};
        break;
    }
  });

export default paymentScheduleReducer;
