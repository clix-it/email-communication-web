import { createSelector } from 'reselect';

const selectPaymentSchedule = state => state.paymentSchedule;

const makeSelectPaymentSchedule = () =>
  createSelector(
    selectPaymentSchedule,
    paymentSchedule => paymentSchedule,
  );

export { makeSelectPaymentSchedule };
