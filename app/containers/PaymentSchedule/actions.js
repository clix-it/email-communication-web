import * as actions from './constants';
export function loadPaymentSchedule(data) {
  return {
    type: actions.LOAD_PAYMENT_SCHEDULE,
    payload: {
      data,
    },
  };
}

export function loadPaymentScheduleSuccess(data) {
  return {
    type: actions.LOAD_PAYMENT_SCHEDULE_SUCCESS,
    payload: {
      data,
    },
  };
}

export function loadPaymentScheduleError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_PAYMENT_SCHEDULE_ERROR,
    payload: {
      errorMessage,
      severity,
    },
  };
}
