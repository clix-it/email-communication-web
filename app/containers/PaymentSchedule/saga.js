import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import _ from 'lodash';
import data from './data.json';
import * as actions from './constants';
import {
  loadPaymentScheduleSuccess,
  loadPaymentScheduleError,
} from './actions';

import env from '../../../environment';

function* fetchPaymentScheduleSaga({ payload }) {
  const { product, partner, ...rest } = payload.data;
  try {
    const result = yield call(
      request,
      `${env.LOAN_SCHEDULE}/?product=${product}&partner=${partner}`,
      {
        headers: env.headers,
        method: 'POST',
        body: JSON.stringify(rest),
      },
    );
    yield put(loadPaymentScheduleSuccess(result.loanScheduleResponse));
  } catch (err) {
    let errMessage = '';
    errMessage = _.get(
      err.errors ? err.errors : [],
      0,
      'Network error occurred! Check connection!',
    );
    yield put(loadPaymentScheduleError(errMessage));
    message.error(errMessage);
  }
}

export default function* paymentScheduleSaga() {
  yield takeEvery(actions.LOAD_PAYMENT_SCHEDULE, fetchPaymentScheduleSaga);
}
