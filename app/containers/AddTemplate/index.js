/**
 *
 * BankDetails
 *
 */
import _, { constant } from 'lodash';
import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useForm, Controller } from 'react-hook-form';
import {
  Form,
  Row,
  Col,
  Button,
  Radio,
  message,
  Divider,
  Card,
  Tag,
} from 'antd';
import makeSelectTemplateDetails from './selectors';
import reducer from './reducer';
import saga from './saga';
import { EMAIL_CATEGORY, DEFAULT_MAIL_ID } from '../../utils/constants';
import InputField from '../../components/InputField';
import { makeSelectTemplates } from '../TemplatePortal/selectors';
import { updateTemplateDetails } from './actions';
import {
  setFormValidationError,
  isDirtyForm,
  loadEntityDetails,
} from '../ApplicantDetails/actions';
import { loadTemplatesSearch } from '../TemplatePortal/actions';
import { useHistory } from 'react-router-dom';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: { span: 10, offset: 2, pull: 2 },
};
export function AddTemplateDetails({
  dispatch,
  addTemplateDetails,
  activity,
  templatePortal,
}) {
  useInjectReducer({ key: 'addTemplateDetails', reducer });
  useInjectSaga({ key: 'addTemplateDetails', saga });

  const templateCodes = templatePortal.templateCommunications.map(
    item => item.templateId,
  );

  const {
    register,
    handleSubmit,
    getValues,
    errors,
    control,
    watch,
    setError,
    reset,
    clearError,
    formState,
    setValue,
    triggerValidation,
  } = useForm({
    mode: 'onChange',
  });

  const { dirtyFields, dirtyFieldsSinceLastSubmit } = formState;
  //  console.log(dirtyFields, 'dirtyFields');
  useEffect(() => {
    dispatch(isDirtyForm({ 5: formState.dirty }));
    console.log(formState.dirty, 'dirty bank', formState.dirtySinceLastSubmit);
  }, [formState.dirty]);

  const values = watch();
  //  console.log(values.templateId, 'values');

  const [tempId, setTempId] = useState(false);
  const [tempIdCaps, setTempIdCaps] = useState('');

  //   console.log(formState.dirty, 'dirty val');
  const options = () => (templateDetails.response || {}).banks || [];

  // console.log(values, 'values bank details', errors);

  const [response, setResponse] = useState(false);

  useEffect(() => {
    // if (Object.keys(errors).length > 0) {
    //   document.getElementById(Object.keys(errors)[0]).focus();
    // }
  }, [errors]);

  useEffect(() => {
    if (addTemplateDetails.response && response) {
      message.success(addTemplateDetails.response);
      dispatch(loadEntityDetails());
      setResponse(false);
    }

    if (addTemplateDetails.error && response) {
      message.error(addTemplateDetails.error);
    }
  }, [addTemplateDetails.response, addTemplateDetails.error]);

  //Submit
  const onSubmit = data => {
    dispatch(updateTemplateDetails(data));
    setResponse(true);
    setTimeout(addTemplate, 3000);
  };

  const history = useHistory();
  const addTemplate = () => {
    history.push(`/templateDetails`);
  };

  const handleTemplate = e => {
    setTempIdCaps(e.target.value);
    setTempId(true);
    dispatch(
      loadTemplatesSearch({
        templateId: e.target.value.toUpperCase(),
      }),
    );
  //  console.log('Template', templateportal.data);
  };
  // const handleTemplate2 = (e) => {
  //   const tempIdSearch = e.target.value

  //   dispatch(loadTemplatesSearch({
  //     templateId: tempIdSearch
  //     }));
  //    if (templatePortal.error) {
  //      setTempId(false)
  //      console.log('Set Temp ID', tempId)
  //    } else {
  //      setTempId(true)
  //    }
  // }

  const handleTemplate2 = e => {
    setTempId(false);
  };

  useEffect(() => {
    const id = tempIdCaps.toUpperCase();
    setValue('templateId', id);
  }, [tempIdCaps]);

  return (
    <div className="App">
      <h4>Template Details</h4>
      <Divider />
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={[16, 16]}>
          {/* start */}
          <Col md={24}>
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="templateId"
              control={control}
              name="templateId"
              type="string"
              values={values}
              handleChange={e => handleTemplate(e)}
              onBlurAction={e => handleTemplate2(e)}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
                //  pattern: /^[A-Z0-9_]+$/,
                pattern: /^[a-zA-Z0-9_]+$/,
              }}
              errors={errors}
              placeholder="Template Code"
              labelHtml="Template Code*"
            />
            {tempId == true && (
              <Card
                title="Existing Templates"
                bordered={true}
                style={{ width: 400 }}
              >
                {templateCodes &&
                  templateCodes.map((data, key) => {
                    return (
                      <Tag
                        style={{ marginLeft: '1%', marginBottom: '1%' }}
                        key={key}
                      >
                        {data}
                      </Tag>
                    );
                  })}
              </Card>
            )}
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="validationMsg"
              control={control}
              name="validationMsg"
              type="string"
              values={values}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
                //  pattern: emailPattern,
              }}
              errors={errors}
              placeholder="Validation Message"
              labelHtml="Validation Message*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="remarks"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              name="remarks"
              type="string"
              values={values}
              errors={errors}
              placeholder="Reamrks"
              labelHtml="Remarks*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="active"
              control={control}
              name="active"
              type="select"
              values={values}
              options={DEFAULT_MAIL_ID}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Active"
              labelHtml="Active*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <Form.Item label="Content">
              <Controller
                name="content"
                control={control}
                defaultValue=""
                style={{ height: '8rem', width: '61rem', marginBottom: '2rem' }}
                as={<ReactQuill theme="snow" />}
              />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            disabled={activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'}
          >
            Save
          </Button>
          {'   '}
          <Button
            type="primary"
            onClick={addTemplate}
          >
            Cancel
          </Button>
        </Form.Item>
      </form>
    </div>
  );
}

AddTemplateDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  addTemplateDetails: makeSelectTemplateDetails(),
  templatePortal: makeSelectTemplates(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddTemplateDetails);
