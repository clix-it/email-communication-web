/*
 *
 * AddTemplate reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  ADD_TEMPLATE_DETAILS,
  ADD_TEMPLATE_DETAILS_ERROR,
  ADD_TEMPLATE_DETAILS_SUCCESS,
} from './constants';

export const initialState = {
  response: false,
  error: false,
  payload: false,
};

/* eslint-disable default-case, no-param-reassign */
const addTemplateDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case ADD_TEMPLATE_DETAILS:
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case ADD_TEMPLATE_DETAILS_SUCCESS:
        draft.error = false;
        draft.response = action.data.message;
        break;
      case ADD_TEMPLATE_DETAILS_ERROR:
        draft.response = false;
        draft.error = `Data not saved`;
        break;
     }
  });

export default addTemplateDetailsReducer;
