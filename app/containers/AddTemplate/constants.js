/*
 *
 * AddTemplate constants
 *
 */

export const ADD_TEMPLATE_DETAILS = 'app/TemplateDetails/ADD_TEMPLATE_DETAILS';
export const ADD_TEMPLATE_DETAILS_SUCCESS =
  'app/TemplateDetails/ADD_TEMPLATE_DETAILS_SUCCESS';
export const ADD_TEMPLATE_DETAILS_ERROR =
  'app/TemplateDetails/ADD_TEMPLATE_DETAILS_ERROR';