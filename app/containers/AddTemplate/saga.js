import _ from 'lodash';
import {
  take,
  call,
  put,
  select,
  takeEvery,
  debounce,
} from 'redux-saga/effects';
import request from 'utils/request';
import {
  ADD_TEMPLATE_DETAILS,
} from './constants';
import {
  addTemplateDetailsSuccess,
} from './actions';
import env from '../../../environment';

export default function* addTemplateDetailsSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(ADD_TEMPLATE_DETAILS, addTemplate);
}

function* addTemplate({ payload }) {

//console.log('Payload', payload)
  const payloadData = {
    templateId: payload.templateId,
    templateType: "Email",
    content: payload.content,
    validationMessage: payload.validationMsg,
    active: payload.active,
    remarks: payload.remarks,
    createdBy: JSON.parse(sessionStorage.getItem('userId'))
  };

  const reqUrl = `${env.ADD_TEMPLATE}`;
  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const appRes = yield call(request, reqUrl, options);
    if (appRes.success) {
      yield put(addTemplateDetailsSuccess({message : _.get(appRes, 'body.message') || 'Template saved successfully!!'}));
    }
  } catch (error) {
    console.error(error);
  }
}