/*
 *
 * AddTemplate actions
 *
 */

import {
  DEFAULT_ACTION,
  ADD_TEMPLATE_DETAILS,
  ADD_TEMPLATE_DETAILS_ERROR,
  ADD_TEMPLATE_DETAILS_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function updateTemplateDetails(payload) {
  return {
    type: ADD_TEMPLATE_DETAILS,
    payload,
  };
}

export function addTemplateDetailsSuccess(data) {
  return {
    type: ADD_TEMPLATE_DETAILS_SUCCESS,
    data,
  };
}

export function addTemplateDetailsError(err) {
  return {
    type: ADD_TEMPLATE_DETAILS_ERROR,
    data: err,
  };
}