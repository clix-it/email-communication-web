import * as actions from './constants';
export function loadApplications(user, role) {
  return {
    type: actions.LOAD_PSV_APPLICATIONS,
    payload: {
      userId: JSON.parse(sessionStorage.getItem('userId')),
      userRole: JSON.parse(sessionStorage.getItem('userRole')),
      startDate: user.startDate ? user.startDate.format('YYYY-MM-DD') : '',
      endDate: user.endDate ? user.endDate.format('YYYY-MM-DD') : '',
      appId: user.appId || '',
      requestType: role || 'PMA',
      panNumber: user.panNumber,
      customerName: user.customerName,
    },
  };
}

export function loadApplicationsSuccess(applications) {
  return {
    type: actions.LOAD_PSV_APPLICATIONS_SUCCESS,
    payload: {
      applications,
    },
  };
}

export function loadApplicationsError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_PSV_APPLICATIONS_ERROR,
    payload: {
      errorMessage,
      severity,
    },
  };
}

export function loadApplicationsInit() {
  return {
    type: actions.LOAD_PSV_APPLICATIONS_INIT,
  };
}
