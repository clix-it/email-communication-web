import { takeEvery, put } from 'redux-saga/effects';
import { message } from 'antd';
import * as actions from './constants';
import env from '../../../environment';
import {
  loadApplicationsInit,
  loadApplicationsSuccess,
  loadApplicationsError,
} from './actions';

function* fetchApplicationsSaga(action) {
  yield put(loadApplicationsInit());

  try {
    const result = yield fetch(
      `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v2/fetch/pending/activities`,
      {
        headers: env.headers,
        method: 'POST',
        body: JSON.stringify({
          userId: JSON.parse(sessionStorage.getItem('userId')),
          // userRole: JSON.parse(sessionStorage.getItem('userRole')),
          userRole: '',
          startDate: action.payload.startDate,
          endDate: action.payload.endDate,
          appId: action.payload.appId,
          requestType: action.payload.requestType,
          panNumber: action.payload.panNumber,
          customerName: action.payload.customerName,
          userActivity: ['CREDIT_PSV'],
          //userActivity: 'CREDIT_REVIEW',           
        }),
      },
    );

    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.success || responseBody.status) {
        yield put(loadApplicationsSuccess(responseBody.data));
      } else {
        yield put(loadApplicationsSuccess([]));
        message.info(
          'Oops! No pending applications found for the searched criteria :( ',
        );
      }
    } else {
      yield put(
        loadApplicationsError(`HTTP Status${result.status} occurred.Try Again`),
      );
      message.error(`HTTP Status${result.status} occurred.Try Again`);
    }
  } catch (err) {
    yield put(
      loadApplicationsError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

export default function* creditPortalSaga() {
  yield takeEvery(actions.LOAD_PSV_APPLICATIONS, fetchApplicationsSaga);
}
