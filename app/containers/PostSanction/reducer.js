/*
 *
 * PostSanction reducer
 *
 */
import produce from 'immer';
import * as actions from './constants';

export const initialState = {
  loading: true,
  applications: [],
  error: {},
};

/* eslint-disable default-case, no-param-reassign */
const postSanctionReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case actions.LOAD_PSV_APPLICATIONS_INIT:
        return { ...state, loading: true };
  
      case actions.LOAD_PSV_APPLICATIONS_SUCCESS:
        return {
          ...state,
          loading: false,
          error: { status: false, message: '' },
          applications: action.payload.applications,
        };
  
      case actions.LOAD_PSV_APPLICATIONS_ERROR: {
        return {
          ...state,
          loading: false,
          error: {
            status: true,
            message: action.payload.errorMessage,
            severity: action.severity,
          },
        };
      }
  
      case actions.LOAD_ENTITY_DETAILS_SUCCESS: {
        // console.log('action', action);
        return {
          ...state,
          loading: false,
          error: { status: false, message: '' },
        };
      }
  
      case actions.LOAD_ENTITY_DETAILS_ERROR: {
        return {
          ...state,
          loading: false,
          error: {
            status: true,
            message: action.payload.errorMessage,
            severity: action.severity,
          },
        };
      }
      default: {
        return state;
      }
    }
  });

export default postSanctionReducer;
