import { createSelector } from 'reselect';

const selectCreditPortal = state => state.postSanction;

const makeSelectApplications = () =>
  createSelector(
    selectCreditPortal,
    postSanction => postSanction,
  );

export { makeSelectApplications };
