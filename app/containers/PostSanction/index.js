/**
 *
 * PostSanction
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectApplications } from './selectors';

import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';

import PostSanctionListingPage from '../../components/PostSanctionListingPage';

export function PostSanction({ dispatch, postSanction, requestType }) {
  const { isLoading = false, error, applications } = postSanction;
  return (
    <PostSanctionListingPage
      applications={applications}
      error={error}
      dispatch={dispatch}
      requestType={requestType}
      isLoading={isLoading}
    />
  );
}

PostSanction.propTypes = {
  postSanction: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  requestType: PropTypes.string.isRequired,
};

const mapStateToProps = createStructuredSelector({
  postSanction: makeSelectApplications(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

PostSanction.propTypes = {
  postSanction: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  requestType: PropTypes.string.isRequired,
};

PostSanction.defaultProps = {
  postSanction: {},
};


const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'postSanction', reducer });
const withSaga = useInjectSaga({ key: 'postSanction', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(PostSanction);
