/**
 *
 * Asynchronously loads the component for PostSanction
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
