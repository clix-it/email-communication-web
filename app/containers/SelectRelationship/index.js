/**
 *
 * SelectRelationship
 *
 */

import React, { memo, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import { connect } from 'react-redux';
import { compose } from 'redux';

import InputField from 'components/InputField';
import { useInjectSaga } from 'utils/injectSaga';
import {
  makeAppDocumentDetailsData,
  makeSelectApplicantDetails2,
} from '../ApplicantDetails/selectors';
import saga from './saga';

export function SelectRelationship({
  applicantDetails,
  applicationData,
  name,
  reset,
  setValue,
  userIndex,
  namePrefix,
  isNoEntity,
  ifEntityCoApplicant,
  clearError,
  isGuarantor,
  ...rest
}) {
  useInjectSaga({ key: 'selectRelationship', saga });

  const {
    selectedApplicant,
    selectedEntity,
    selectedGuarantor,
  } = applicationData;
  let relationship = '';
  let relationshipObj = {};
  if (ifEntityCoApplicant) {
    relationship = _.get(
      _.find(_.get(applicantDetails, 'entity.entities'), {
        cuid: selectedEntity.cuid,
      }),
      name,
    );
    relationshipObj =
      _.get(
        _.find(_.get(applicantDetails, 'entity.entities'), {
          cuid: selectedEntity.cuid,
        }),
        namePrefix,
      ) || {};
  } else {
    relationship = !isGuarantor
      ? _.get(selectedApplicant, 'entityOfficers.designation')
      : _.get(selectedGuarantor, 'entityOfficers.designation');
    relationshipObj = !isGuarantor
      ? _.get(selectedApplicant, 'entityOfficers') || {}
      : _.get(selectedGuarantor, 'entityOfficers') || {};
  }

  useEffect(() => {
    if (relationship) {
      setValue(name, relationship);
      setValue(`${namePrefix}.id`, relationshipObj.id);
      setValue(`${namePrefix}.belongsTo`, relationshipObj.belongsTo);
      clearError(name);
    } else {
      setValue(name, '');
      setValue(`${namePrefix}.id`, '');
      setValue(`${namePrefix}.belongsTo`, '');
    }
  }, [selectedApplicant, selectedGuarantor, selectedEntity, relationship]);
  if (isNoEntity) return <div />;
  return <InputField name={name} {...rest} />;
}

SelectRelationship.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const mapStateToProps = createStructuredSelector({
  applicationData: makeAppDocumentDetailsData(),
  applicantDetails: makeSelectApplicantDetails2(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SelectRelationship);
