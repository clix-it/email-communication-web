/**
 *
 * Asynchronously loads the component for SelectRelationship
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
