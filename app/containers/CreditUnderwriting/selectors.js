import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the creditUnderwriting state domain
 */

const selectCreditUnderwritingDomain = state =>
  state.creditUnderwriting || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by CreditUnderwriting
 */

const makeSelectCreditUnderwriting = () =>
  createSelector(
    selectCreditUnderwritingDomain,
    substate => substate,
  );

export default makeSelectCreditUnderwriting;
export { selectCreditUnderwritingDomain };
