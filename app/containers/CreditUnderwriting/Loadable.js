/**
 *
 * Asynchronously loads the component for CreditUnderwriting
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
