/*
 * CreditUnderwriting Messages
 *
 * This contains all the text for the CreditUnderwriting container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.CreditUnderwriting';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the CreditUnderwriting container!',
  },
});
