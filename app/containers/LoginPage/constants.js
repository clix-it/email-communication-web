/*
 *
 * LoginPage constants
 *
 */

export const ATTEMPT_LOGIN = 'ATTEMPT_LOGIN';
export const LOGIN_INIT = 'LOGIN_INIT';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
