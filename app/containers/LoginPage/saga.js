import { put, takeEvery, call } from 'redux-saga/effects';
import request from 'utils/request';
import env from '../../../environment';
import * as actions from './constants';
import { loginInit, loginFailed, loginSuccess } from './actions';
import { registerUser, logUserIn } from '../App/actions';

function* authenticateAndAuthorize(action) {
  try {
    yield put(loginInit());

    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        userName: action.userCredentials.userId,
        password: btoa(action.userCredentials.password),
      }),
    };

    // const authenticationRes = yield call(request, env.authUrl, options);
    const authenticationRes = { status: true };
    if (!authenticationRes.status) {
      yield put(loginFailed('Wrong UserID or Password Entered!', 'error'));
    } else {
      yield put(loginSuccess());
      yield put(registerUser(action.userCredentials.userId, ''));
      yield put(logUserIn(action.userCredentials.userId, ''));
      sessionStorage.setItem('userRole', JSON.stringify(''));
      sessionStorage.setItem(
        'userId',
        JSON.stringify(action.userCredentials.userId),
      );
    }
  } catch (err) {
    yield put(
      loginFailed('Network error occurred! Check connection!', 'error'),
    );
  }
}

export default function* loginPageSaga() {
  yield takeEvery(actions.ATTEMPT_LOGIN, authenticateAndAuthorize);
}
