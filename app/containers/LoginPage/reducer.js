/*
 *
 * LoginPage reducer
 *
 */
import * as actions from './constants';

export const initialState = {
  error: { status: false, message: '', code: '' },
  loading: false,
};

/* eslint-disable default-case, no-param-reassign */
const loginPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOGIN_INIT: {
      return {
        ...state,
        loading: true,
        error: { status: false, message: '', code: '' },
      };
    }
    case actions.LOGIN_FAILED: {
      return {
        ...state,
        loading: false,
        error: {
          status: true,
          message: action.errMessage,
          code: action.errCode,
        },
      };
    }
    case actions.LOGIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: { status: false, message: '', code: '' },
      };
    }
    default: {
      return state;
    }
  }
};

export default loginPageReducer;
