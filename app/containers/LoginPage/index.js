/**
 *
 * LoginPage
 *
 */

import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import env from '../../../environment';
import reducer from './reducer';
import saga from './saga';
import { attemptLogin, loginFailed } from './actions';
import './loginStyle.css';

import {
  message,
  Form,
  Spin,
  Space,
  Input,
  Row,
  Col,
  Button,
  Alert,
} from 'antd';
import { useLocation } from 'react-router-dom';

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

export function LoginPage({ isLoading, dispatch, error }) {
  const query = new URLSearchParams(useLocation().search);
  const userIdParam = query.get('userId');
  const [userId, setUserId] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);

  const handleIdChange = evt => {
    setUserId(evt.target.value);
  };
  const handlePasswordChange = evt => {
    setPassword(evt.target.value);
  };
  const handleClickShowPassword = () => {
    setShowPassword(prev => !prev);
  };

  // const onFinish = values => {
  //   console.log('Success:', values);
  //   e.preventDefault();
  //   if (userId === '') {
  //     dispatch(loginFailed('Employee ID Cannot be empty!', 'warning'));
  //   } else if (password === '') {
  //     dispatch(loginFailed('Password Cannot be empty!', 'warning'));
  //   } else dispatch(attemptLogin({ userId, password }));
  // };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  useEffect(() => {
    if (
      userIdParam &&
      window.self !== window.top &&
      document.referrer === env.COMMON_PORTAL_URL
    ) {
      sessionStorage.setItem('loginFrom', 'COMMON_PORTAL');
      sessionStorage.setItem('parentURL', query.get('parentURL'));
      dispatch(attemptLogin({ userId: userIdParam, password: '12' }));
    }
  }, [userIdParam]);

  useEffect(() => {
    if (error.status) {
      message.error(error.message);
      sessionStorage.clear();
    }
  }, [error.status]);
  // const handleMouseDownPassword = event => {
  //   event.preventDefault();
  // };
  const handleSubmit = e => {
    // e.preventDefault();
    if (userId === '') {
      dispatch(loginFailed('Employee ID Cannot be empty!', 'warning'));
    } else if (password === '') {
      dispatch(loginFailed('Password Cannot be empty!', 'warning'));
    } else dispatch(attemptLogin({ userId, password }));
  };

  return (
    // <Spin spinning={isLoading} indicator={<LoadingIndicator />}>
    <div className="loginForm">
      {isLoading ? (
        <div style={{ textAlign: 'center' }}>
          <Space size="large">
            <Spin size="large" />
          </Space>
        </div>
      ) : null}
      {/* <Card> */}
      <Row gutter={24}>
        <Col span={4} />
        <Col span={12}>
          <Form
            onSubmit={handleSubmit}
            {...layout}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={handleSubmit}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              label="User Id*"
              value={userId}
              onChange={handleIdChange}
              name="userId"
              rules={[
                { required: true, message: 'Please input your user Id!' },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password*"
              value={password}
              onChange={handlePasswordChange}
              name="password"
              rules={[
                { required: true, message: 'Please input your password!' },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item {...tailLayout}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
    // </Spin>
  );
}

LoginPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
const withReducer = useInjectReducer({ key: 'login', reducer });
const withSaga = useInjectSaga({ key: 'login', saga });
const mapStateToProps = state => ({
  error: state.login.error,
  isLoading: state.login.loading,
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(LoginPage);
