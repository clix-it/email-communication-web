/*
 *
 * LoginPage actions
 *
 */

import * as actions from './constants';

export function attemptLogin(userCredentials) {
  return {
    type: actions.ATTEMPT_LOGIN,
    userCredentials,
  };
}
export function loginInit() {
  return {
    type: actions.LOGIN_INIT,
  };
}
export function loginFailed(errMessage, errCode) {
  return {
    type: actions.LOGIN_FAILED,
    errMessage,
    errCode,
  };
}
export function loginSuccess() {
  return {
    type: actions.LOGIN_SUCCESS,
  };
}
