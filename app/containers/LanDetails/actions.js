/*
 *
 * LanDetails actions
 *
 */

import {
  GET_LAN_DETAILS,
  GET_LAN_DETAILS_SUCCESS,
  GET_LAN_DETAILS_FAILURE,
} from './constants';

export function fetchLanDetails({ lan, lms }) {
  return {
    type: GET_LAN_DETAILS,
    lan,
    lms,
  };
}

export function lanDetailsFetched(response) {
  return {
    type: GET_LAN_DETAILS_SUCCESS,
    response,
  };
}

export function fetchLanDetailsFailed(error) {
  return {
    type: GET_LAN_DETAILS_FAILURE,
    error,
  };
}
