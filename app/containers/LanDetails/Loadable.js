/**
 *
 * Asynchronously loads the component for LanDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
