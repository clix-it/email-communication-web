/**
 *
 * LanDetails
 *
 */
import _ from 'lodash';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { formatToCurrency } from 'utils/helpers';
import FetchProgram from 'containers/FetchProgram/Loadable';
import ForeClosureDetails from 'containers/ForeClosureDetails/Loadable';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectLanDetails from './selectors';
import reducer from './reducer';
import saga from './saga';
import { Typography, Card, Divider, Spin, Space } from 'antd';
import DownloadEmiSchedule from '../DownloadEmiSchedule';
import DownloadSoa from '../DownloadSoa';
//import _ from 'lodash';

import { fetchLanDetails } from './actions';
const { Text } = Typography;
const gridStyle = {
  width: '25%',
  textAlign: 'left',
};

export function LanDetails({
  lan,
  lanDetails,
  dispatch,
  lms,
  product,
  partner,
  applicantDetails,
}) {
  useInjectReducer({ key: 'lanDetails', reducer });
  useInjectSaga({ key: 'lanDetails', saga });

  useEffect(() => {
    dispatch(fetchLanDetails({ lan, lms }));
  }, [lan]);

  const ProgramComp = ({ response, scheme }) => {
    const desc = (_.find(response.masterData, { cmCode: String(scheme) }) || {})
      .cmValue;
    return <>{desc}</>;
  };
  console.log("applicantDetails",applicantDetails)
  const total_outstanding =
    lanDetails.response.interest_overdue +
    lanDetails.response.outStandingPrincipal;
  const additional30dayInterest =
    (total_outstanding * lanDetails.response.interest_rate * 30) / 36000;
  return (
    <Spin spinning={lanDetails.loading} tip="Loading...">
      <h4>{`LAN Details (${lan}) `}</h4>
      <Card title="Loan Overview">
        <Card.Grid style={gridStyle}>
          <Text type="secondary">Loan #</Text> {lanDetails.response.loan_id}
        </Card.Grid>
        <Card.Grid hoverable={false} style={gridStyle}>
          <Text type="success">Disbursed Amount: </Text>{' '}
          {formatToCurrency(lanDetails.response.disbursed_amount)}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Actual Tenure(Months): </Text>{' '}
          {lanDetails.response.actual_tenure}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Interset Rate: </Text>{' '}
          {lanDetails.response.interest_rate}%
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Emi Amount: </Text>{' '}
          {formatToCurrency(lanDetails.response.emi_amount)}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Branch: </Text> {lanDetails.response.branch}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">DSA: </Text> {lanDetails.response.dsa}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Program Type: </Text>{' '}
          <FetchProgram
            programcode={'PROGRAMTYPE'}
            partner={partner}
            product={product}
            cmCode={lanDetails.response.scheme}
          >
            <ProgramComp scheme={lanDetails.response.scheme} />
          </FetchProgram>
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Loan Start Date: </Text>{' '}
          {lanDetails.response.disbursed_date}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Maturity Date: </Text>{' '}
          {lanDetails.response.maturity_date}
        </Card.Grid>
      </Card>
      <Divider />
      <Card title="Loan Current Status">
        <Card.Grid style={gridStyle}>
          <Text type="secondary">Remaining Tenure(Months):</Text>{' '}
          {lanDetails.response.tenor}
        </Card.Grid>
        <Card.Grid hoverable={false} style={gridStyle}>
          <Text type="success">Next EMI Date: </Text>{' '}
          {lanDetails.response.next_emi_date}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Total Outstanding: </Text>{' '}
          {formatToCurrency(total_outstanding)}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Principal Outstanding: </Text>{' '}
          {formatToCurrency(lanDetails.response.outStandingPrincipal)}
        </Card.Grid>
        {/* <Card.Grid style={gridStyle}>
          <Text type="success">Intereset Outstanding: </Text>{' '}
          {formatToCurrency(lanDetails.response.outstanding_interest)}
        </Card.Grid> */}
        {/* <Card.Grid style={gridStyle}>
          <Text type="success">Total Overdue: </Text>{' '}
          {formatToCurrency(lanDetails.response.total_overdue)}
        </Card.Grid> */}
        <Card.Grid style={gridStyle}>
          <Text type="success">Principal Overdue: </Text>{' '}
          {formatToCurrency(lanDetails.response.principal_overdue)}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Interest Overdue: </Text>{' '}
          {formatToCurrency(lanDetails.response.interest_overdue)}
        </Card.Grid>
        {/* <Card.Grid style={gridStyle}>
          <Text type="success">Overdue Charges: </Text>{' '}
          {formatToCurrency(lanDetails.response.overdue_charges || 0)}
        </Card.Grid> */}
        <Card.Grid style={gridStyle}>
          <Text type="success">Dpd Days: </Text> {lanDetails.response.dpd_days}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text strong type="success">
            Additional 30 days interest:
            {formatToCurrency(additional30dayInterest)}
          </Text>{' '}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text strong type="success">
            Total Outstanding after 30 days:
            {formatToCurrency(additional30dayInterest + total_outstanding)}
          </Text>{' '}
        </Card.Grid>
      </Card>
      {lanDetails.response._loan_account_no && (
        <div>
          <Divider />
          <Card title="DA details">
            <Card.Grid style={gridStyle}>
              <Text type="secondary">Deal Type:</Text>{' '}
              {lanDetails.response.dealtype}
            </Card.Grid>
            <Card.Grid hoverable={false} style={gridStyle}>
              <Text type="success">Partner Name: </Text>{' '}
              {lanDetails.response.partnername}
            </Card.Grid>
            <Card.Grid style={gridStyle}>
              <Text type="success">Date of Assignment: </Text>{' '}
              {lanDetails.response.dtdate}
            </Card.Grid>
            <Card.Grid style={gridStyle}>
              <Text type="success">Date of List creation: </Text>{' '}
              {lanDetails.response.dtcreatedby}
            </Card.Grid>
            <Card.Grid style={gridStyle}>
              <Text type="success">Date of List Updation : </Text>{' '}
              {lanDetails.response.dtupdatedby}
            </Card.Grid>
          </Card>
        </div>
      )}
      <Divider />
      <ForeClosureDetails lan={lan} appid={applicantDetails.entity.appId}/>
    </Spin>
  );
}

LanDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  lanDetails: makeSelectLanDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(LanDetails);
