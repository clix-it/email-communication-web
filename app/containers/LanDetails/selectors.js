import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the lanDetails state domain
 */

const selectLanDetailsDomain = state => state.lanDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by LanDetails
 */

const makeSelectLanDetails = () =>
  createSelector(
    selectLanDetailsDomain,
    substate => substate,
  );

export default makeSelectLanDetails;
export { selectLanDetailsDomain };
