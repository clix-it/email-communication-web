import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import env from '../../../environment';
import { GET_LAN_DETAILS } from './constants';
import { lanDetailsFetched, fetchLanDetailsFailed } from './actions';

function* fetchLanDetails(action) {
  try {
    const responseBody = yield call(
      request,
      `${env.DEDUPE_SEARCH}/fetch/lan/details/?lan=${action.lan}&lms=${
        action.lms
      }`,
      {
        method: 'GET',
        headers: env.headers,
      },
    );

    // const responseBody = yield result.json();
    // const responseBody = data;
    yield put(lanDetailsFetched(responseBody));
  } catch (err) {
    yield put(
      fetchLanDetailsFailed('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

export default function* lanDetailsSaga() {
  yield takeEvery(GET_LAN_DETAILS, fetchLanDetails);
}
