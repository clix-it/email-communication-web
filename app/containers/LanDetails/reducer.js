/*
 *
 * LanDetails reducer
 *
 */
import produce from 'immer';
import {
  GET_LAN_DETAILS,
  GET_LAN_DETAILS_SUCCESS,
  GET_LAN_DETAILS_FAILURE,
} from './constants';
export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const lanDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_LAN_DETAILS:
        draft.loading = true;
        draft.lan = action.lan;
        break;
      case GET_LAN_DETAILS_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        break;
      case GET_LAN_DETAILS_FAILURE:
        draft.loading = false;
        draft.error = action.error;
        draft.response = false;
        break;
    }
  });

export default lanDetailsReducer;
