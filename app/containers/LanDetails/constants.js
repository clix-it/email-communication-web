/*
 *
 * LanDetails constants
 *
 */

export const GET_LAN_DETAILS = 'app/LanDetails/GET_LAN_DETAILS';
export const GET_LAN_DETAILS_SUCCESS = 'app/LanDetails/GET_LAN_DETAILS_SUCCESS';
export const GET_LAN_DETAILS_FAILURE = 'app/LanDetails/GET_LAN_DETAILS_FAILURE';
