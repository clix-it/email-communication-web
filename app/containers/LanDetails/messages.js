/*
 * LanDetails Messages
 *
 * This contains all the text for the LanDetails container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.LanDetails';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the LanDetails container!',
  },
});
