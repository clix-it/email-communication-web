/*
 *
 * PslSubCategory constants
 *
 */

export const FETCH_PSL_SUB_CATEGORY =
  'app/PslSubCategory/FETCH_PSL_SUB_CATEGORY';
export const FETCH_PSL_SUB_CATEGORY_SUCCESS =
  'app/PslSubCategory/FETCH_PSL_SUB_CATEGORY_SUCCESS';
export const FETCH_PSL_SUB_CATEGORY_FAILED =
  'app/PslSubCategory/FETCH_PSL_SUB_CATEGORY_FAILED';
