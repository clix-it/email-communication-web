/**
 *
 * Asynchronously loads the component for PslSubCategory
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
