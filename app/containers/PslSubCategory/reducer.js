/*
 *
 * PslPurpose reducer
 *
 */
import _ from 'lodash';
import produce from 'immer';
import {
  FETCH_PSL_SUB_CATEGORY,
  FETCH_PSL_SUB_CATEGORY_SUCCESS,
  FETCH_PSL_SUB_CATEGORY_FAILED,
} from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const pslSubCategoryReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_PSL_SUB_CATEGORY:
        draft.loading = true;
        draft.error = false;
        break;
      case FETCH_PSL_SUB_CATEGORY_SUCCESS: {
        draft.loading = true;
        const temp = [];
        action.response.forEach(item => {
          temp.push({ code: item.cmCode, value: item.cmValue });
        });
        draft.response = temp;
        break;
      }
      case FETCH_PSL_SUB_CATEGORY_FAILED:
        draft.loading = true;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default pslSubCategoryReducer;
