import { call, put, takeEvery } from 'redux-saga/effects';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import { FETCH_PSL_SUB_CATEGORY } from './constants';
import { pslSubCategoryFetched, pslSubCategoryError } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* fetchPslSubCategory() {
  const requestURL = `${env.MASTER_URL}?masterType=PSL_SUB_CATEGORY`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });

    if (response && response.success) {
      yield put(pslSubCategoryFetched(response.masterData));
    } else {
      yield put(pslSubCategoryError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(pslSubCategoryError('Master Data not found'));
    message.error(
      (error && error.message) || 'Master Data for PSL Sub Category not found',
    );
  }
}

export default function* pslSubCategorySaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_PSL_SUB_CATEGORY, fetchPslSubCategory);
}
