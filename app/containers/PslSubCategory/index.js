/**
 *
 * PslSubCategory
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import InputField from 'components/InputField';
import { Controller } from 'react-hook-form';
import { Select, Form } from 'antd';
import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPslSubCategory from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchPslSubCategory } from './actions';

const { Option } = Select;

export function PslSubCategory({
  pslSubCategory,
  getPslSubCategory,
  errors,
  setError,
  clearError,
  setValue,
  name,
  values,
  ...rest
}) {
  useInjectReducer({ key: 'pslSubCategory', reducer });
  useInjectSaga({ key: 'pslSubCategory', saga });

  useEffect(() => {
    getPslSubCategory();
  }, []);

  const { response = [], error } = pslSubCategory;

  return (
    <Form.Item label="PSL Sub Category">
      <Controller
        name={name}
        rules={{
          required: {
            value: false,
            message: 'This field cannot be left empty',
          },
        }}
        showSearch
        onChange={([value]) => value}
        {...rest}
        as={
          <Select size="large" placeholder="PSL Sub Category">
            {response &&
              response.length > 0 &&
              response.map(item => (
                <Option value={item.code}>{item.value}</Option>
              ))}
          </Select>
        }
      />
      <ErrorMessage name={name} errors={errors} />
    </Form.Item>
  );
}

PslSubCategory.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pslSubCategory: makeSelectPslSubCategory(),
});

function mapDispatchToProps(dispatch) {
  return {
    getPslSubCategory: () => dispatch(fetchPslSubCategory()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PslSubCategory);
