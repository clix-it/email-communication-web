/*
 *
 * PslSector actions
 *
 */

import {
  FETCH_PSL_SUB_CATEGORY,
  FETCH_PSL_SUB_CATEGORY_SUCCESS,
  FETCH_PSL_SUB_CATEGORY_FAILED,
} from './constants';

export function fetchPslSubCategory() {
  return {
    type: FETCH_PSL_SUB_CATEGORY,
  };
}

export function pslSubCategoryFetched(response) {
  return {
    type: FETCH_PSL_SUB_CATEGORY_SUCCESS,
    response,
  };
}

export function pslSubCategoryError(error) {
  return {
    type: FETCH_PSL_SUB_CATEGORY_FAILED,
    error,
  };
}
