import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pslSubCategory state domain
 */

const selectPslSubCategoryDomain = state =>
  state.pslSubCategory || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by pslSubCategory
 */

const makeSelectPslSubCategoryInput = () =>
  createSelector(
    selectPslSubCategoryDomain,
    substate => substate,
  );

export default makeSelectPslSubCategoryInput;
export { selectPslSubCategoryDomain };
