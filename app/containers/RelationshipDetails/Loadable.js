/**
 *
 * Asynchronously loads the component for RelationshipDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
