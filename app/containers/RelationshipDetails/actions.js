/*
 *
 * RelationshipDetails actions
 *
 */

import * as actions from './constants';

export function fetchRelationshipsInit() {
  return {
    type: actions.FETCH_RELATIONSHIPS_INIT,
  };
}

export function fetchRelationships(user) {
  return {
    type: actions.FETCH_RELATIONSHIPS,
    user,
  };
}

export function fetchRelationshipsSuccess(coApplicantData) {
  return {
    type: actions.FETCH_RELATIONSHIPS_SUCCESS,
    coApplicantData,
  };
}

export function fetchRelationshipsError() {
  return {
    type: actions.FETCH_RELATIONSHIPS_ERROR,
  };
}

export function setSelectedRelationships(relationship) {
  return {
    type: actions.SET_CURRENT_RELATIONSHIP,
    relationship,
  };
}

export function setMainUser(cuid, index) {
  return {
    type: actions.SET_MAIN_USER,
    cuid,
    index,
  };
}
