import produce from 'immer';
import _ from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';

import * as actions from './constants';

export const initialState = {
  loading: false,
  error: false,
  coApplicantData: [],
  selectedCoApplicant: false,
};

/* eslint-disable default-case, no-param-reassign */
const relationshipDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.FETCH_RELATIONSHIPS_INIT:
        draft.loading = true;
        break;
      case actions.FETCH_RELATIONSHIPS_SUCCESS:
        draft.loading = false;
        draft.coApplicantData = action.coApplicantData;
        draft.selectedCoApplicant = draft.selectedCoApplicant
          ? _.find(draft.coApplicantData, {
              cuid: draft.selectedCoApplicant.cuid,
            })
          : draft.coApplicantData[0];
        break;
      case actions.FETCH_RELATIONSHIPS_ERROR:
        draft.loading = false;
        draft.error = false;
        draft.coApplicantData = [];
        draft.selectedCoApplicant = false;
        break;
      case actions.SET_CURRENT_RELATIONSHIP:
        draft.selectedCoApplicant = action.relationship;
        break;
      case LOCATION_CHANGE:
        draft.loading = false;
        draft.error = false;
        draft.coApplicantData = [];
        draft.selectedCoApplicant = false;
        break;
      case actions.SET_MAIN_USER:
        draft.loading = false;
        draft.error = false;
        draft.coApplicantData = [];
        draft.selectedCoApplicant = false;
        break;
      default: {
        return state;
      }
    }
  });

export default relationshipDetailsReducer;
