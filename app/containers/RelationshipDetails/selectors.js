import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the relationshipDetails state domain
 */

const selectRelationshipDetailsDomain = state =>
  state.relationshipDetails || initialState;

const makeSelectRelationshipDetails = () =>
  createSelector(
    selectRelationshipDetailsDomain,
    substate => substate,
  );

export { makeSelectRelationshipDetails };
