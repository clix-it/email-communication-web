/**
 *
 * RelationshipDetails
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import { fetchRelationships } from './actions';
import { makeSelectRelationshipDetails } from './selectors';
import { Spin } from 'antd';
import RelationshipsComponent from '../../components/RelationshipsComponent';

export function RelationshipDetails({ user, cuid, relationshipDetails, dispatch }) {
  const {
    loading = false,
    error = false,
    coApplicantData = [],
    selectedCoApplicant = false,
  } = relationshipDetails;

  useEffect(() => {
    if(user && user.cuid){
      dispatch(fetchRelationships(user));
    }    
  }, [cuid]);

  return (
    <RelationshipsComponent
      loading={loading}
      user={user}
      cuid={cuid}
      coApplicantData={coApplicantData}
      selectedCoApplicant={selectedCoApplicant}
      dispatch={dispatch}
    />
  );
}

RelationshipDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  relationshipDetails: makeSelectRelationshipDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withReducer = useInjectReducer({ key: 'relationshipDetails', reducer });
const withSaga = useInjectSaga({ key: 'relationshipDetails', saga });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(RelationshipDetails);
