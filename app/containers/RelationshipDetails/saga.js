import { takeEvery, put, takeLatest, select, all } from 'redux-saga/effects';
import _ from 'lodash';
import * as actions from './constants';
import { message } from 'antd';
import {
  fetchRelationshipsInit,
  fetchRelationshipsSuccess,
  fetchRelationshipsError,
} from './actions';

import env from '../../../environment';

function* fetchCoApplicantDetailsSaga(action) {
  yield put(fetchRelationshipsInit());
  debugger;
  const coApplicantDetails = [];
  const userResponseBody = action.user;
  try {
    if (
      userResponseBody.entityOfficers &&
      userResponseBody.entityOfficers.length > 0
    ) {
      yield all(
        userResponseBody.entityOfficers.map(function*(item) {
          if (item.belongsTo) {
            yield* fetchCoApplicantDetails(item.belongsTo, coApplicantDetails);
          }
        }),
      );
    }
    const coApplicantData = coApplicantDetails.reduce((unique, o) => {
      if (!unique.some(obj => obj.cuid === o.cuid)) {
        unique.push(o);
      }
      return unique;
    }, []);
    yield put(fetchRelationshipsSuccess(coApplicantData));
  } catch (err) {
    yield put(
      fetchRelationshipsError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

export function* fetchCoApplicantDetails(coApplicantCuid, coApplicantDetails) {
  const apiresult = yield fetch(
    `${env.USER_SERVICE_API_URL}/${coApplicantCuid}`,
    {
      headers: env.headers,
    },
  );
  const responseBody = yield apiresult.json();
  if (responseBody.success === true) {
    coApplicantDetails.push(responseBody.user);
  }
}

export default function* relationshipDetailsSaga() {
  yield takeLatest(actions.FETCH_RELATIONSHIPS, fetchCoApplicantDetailsSaga);
}
