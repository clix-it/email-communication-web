/**
 *
 * LeadsList
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import LeadsRecord from '../../components/LeadsRecord';
// import './style.css';
export function LeadsList({
  user,
  leads,
  isLoading,
  error,
  dispatch,
  updatedAtSort,
  requestType,
}) {
  return (
    <div>
      <LeadsRecord
        leads={leads}
        error={error}
        user={user}
        dispatch={dispatch}
        isLoading={isLoading}
        updatedAtSort={updatedAtSort}
        requestType={requestType}
      />
    </div>
  );
}

LeadsList.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  user: state.global.user,
  isLoading: state.leadsList.loading,
  leads: state.leadsList.leads,
  error: state.leadsList.error,
  updatedAtSort: state.leadsList.updatedAtSort,
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = useInjectReducer({ key: 'leadsList', reducer });

const withSaga = useInjectSaga({ key: 'leadsList', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(LeadsList);
