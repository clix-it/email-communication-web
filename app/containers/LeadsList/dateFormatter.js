export default function dateFormatter(date) {
  var d = new Date(+date);
  var year = d.getFullYear();
  var month = d.getMonth() + 1;
  var day = d.getDate();
  if (month < 10) {
    month = '0' + month;
  }
  if (day < 10) {
    day = '0' + day;
  }

  return [year, month, day].join('-');
}
