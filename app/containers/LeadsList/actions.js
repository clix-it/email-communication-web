/*
 *
 * LeadsList actions
 *
 */
import * as actions from './constants';
export function loadLeads(user, requestType) {
  return {
    type: actions.LOAD_LEADS,
    payload: {
      userId: JSON.parse(sessionStorage.getItem('userId')),
      userRole: JSON.parse(sessionStorage.getItem('userRole')),
      startDate: user.startDate ? user.startDate.format('YYYY-MM-DD') : '',
      endDate: user.endDate ? user.endDate.format('YYYY-MM-DD') : '',
      appId: user.appId || '',
      requestType: requestType || 'PMA',
      panNumber: user.panNumber,
      customerName: user.customerName,
    },
  };
}
export function loadLeadsSuccess(leads) {
  return {
    type: actions.LOAD_LEADS_SUCCESS,
    leads,
  };
}
export function loadLeadsError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_LEADS_ERROR,
    errorMessage,
    severity,
  };
}
export function loadLeadsInit() {
  return {
    type: actions.LOAD_LEADS_INIT,
  };
}
export function loadLeadsException(exceptionMessage) {
  return {
    type: actions.LOAD_LEADS_EXCEPTION,
    exceptionMessage,
  };
}
export function changeUpdatedAtSort(sortAs) {
  return { type: actions.CHANGE_UPDATEDAT_SORT, sortAs };
}

export function setCurrentApp(app) {
  return {
    type: actions.SET_CURRENT_APPLICATION,
    app,
  };
}
