import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import { loadLeadsInit, loadLeadsSuccess, loadLeadsError } from './actions';

function* fetchLeadsSaga(action) {
  yield put(loadLeadsInit());
  try {
    const responseBody = yield call(
      request,
      `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v2/fetch/pending/activities`,
      {
        headers: env.headers,
        method: 'POST',
        body: JSON.stringify({
          userId: JSON.parse(sessionStorage.getItem('userId')),
          // userRole: JSON.parse(sessionStorage.getItem('userRole')),
          userRole: '',
          startDate: action.payload.startDate,
          endDate: action.payload.endDate,
          appId: action.payload.appId,
          requestType: action.payload.requestType,
          panNumber: action.payload.panNumber,
          customerName: action.payload.customerName,
          userActivity: ['DEDUPE_REVIEW'],
        }),
      },
    );
    if (responseBody.success || responseBody.status) {
      yield put(loadLeadsSuccess(responseBody.data));
    } else {
      yield put(loadLeadsSuccess([]));
      message.info(
        'Oops! No pending applications found for the searched criteria :( ',
      );
    }
  } catch (err) {
    yield put(loadLeadsError('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}
export default function* leadsListSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(actions.LOAD_LEADS, fetchLeadsSaga);
}
