/*
 *
 * LeadsList reducer
 *
 */
import produce from 'immer';
import * as actions from './constants';
export const initialState = {
  leads: [],
  loading: true,
  error: {},
  exception: {},
  updatedAtSort: 'latest',
};

/* eslint-disable default-case, no-param-reassign */
const leadsListReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.LOAD_LEADS_INIT:
        draft.loading = true;
        break;
      case actions.LOAD_LEADS_SUCCESS:
        draft.loading = false;
        draft.leads =
          draft.updatedAtSort === 'oldest'
            ? action.leads.reverse()
            : action.leads;

        break;

      case actions.LOAD_LEADS_ERROR:
        draft.loading = false;
        draft.error = {
          status: true,
          message: action.errorMessage,
          severity: action.severity,
        };
        break;
      case actions.LOAD_LEADS_EXCEPTION:
        draft.loading = false;
        draft.exception = { status: true, message: action.exceptionMessage };
        break;
      case actions.CHANGE_UPDATEDAT_SORT:
        draft.updatedAtSort = action.sortAs;
        draft.leads = draft.leads.reverse();
        break;
      case actions.SET_CURRENT_APPLICATION:
        draft.currentApplication = action.app;
        break;
    }
  });

export default leadsListReducer;
