import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the leadsList state domain
 */

const selectLeadsListDomain = state => state.leadsList || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by LeadsList
 */

export const makeSelectLeadsList = () =>
  createSelector(
    selectLeadsListDomain,
    substate => substate,
  );

export default makeSelectLeadsList;
export { selectLeadsListDomain };
