/**
 *
 * Asynchronously loads the component for GenerateCibil
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
