/*
 * GenerateCibil Messages
 *
 * This contains all the text for the GenerateCibil container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.GenerateCibil';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the GenerateCibil container!',
  },
});
