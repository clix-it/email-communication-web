import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the generateCibil state domain
 */

const selectGenerateCibilDomain = state => state.generateCibil || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by GenerateCibil
 */

const makeSelectGenerateCibil = () =>
  createSelector(
    selectGenerateCibilDomain,
    substate => substate,
  );

export default makeSelectGenerateCibil;
export { selectGenerateCibilDomain };
