import * as actions from './constants';

export function loadEmails() {
  return {
    type: actions.LOAD_EMAILS,
  };
};

export function loadEmailsSuccess(response) {
  return {
    type: actions.LOAD_EMAILS_SUCCESS,
    response,
  };
}

export function loadEmailsError(error) {
  return {
    type: actions.LOAD_EMAILS_ERROR,
    error
  };
}

// Excel Uplaod

export function excelUpload(payload) {
  console.log('Action')
  return {
    type: actions.UPDATE_EXCEL_DETAILS,
    payload,
  };
}

export function excelUploadSuccess(data) {
  return {
    type: actions.UPDATE_EXCEL_DETAILS_SUCCESS,
    data,
  };
}

export function excelUploadError(error) {
  return {
    type: actions.UPDATE_EXCEL_DETAILS_ERROR,
    data: error,
  };
}


// Search

export function loadEmailsSearch(payload) {
  return {
    type: actions.LOAD_EMAILS_SEARCH,
    payload,
  };
};

export function loadEmailsSearchSuccess(response) {
  return {
    type: actions.LOAD_EMAILS_SEARCH_SUCCESS,
    response,
  };
}

export function loadEmailsSearchError(error) {
  return {
    type: actions.LOAD_EMAILS_SEARCH_ERROR,
    error
  };
}