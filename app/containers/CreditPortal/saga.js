import _ from 'lodash';
import { takeEvery, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { loadEmailsError, loadEmailsSuccess, excelUploadSuccess, excelUploadError, loadEmailsSearchSuccess, loadEmailsSearchError } from './actions';
import { LOAD_EMAILS, UPDATE_EXCEL_DETAILS, LOAD_EMAILS_SEARCH } from './constants';
import env from '../../../environment';

// Individual exports for testing
export default function* getEmailSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(LOAD_EMAILS, getEmailData);
  yield takeEvery(UPDATE_EXCEL_DETAILS, updateExcel);
  yield takeEvery(LOAD_EMAILS_SEARCH, getEmailDataSearch);
}

function* getEmailData(action) {
  const masterUrl = `${env.GET_EMAIL}`;
  const options = {
    method: 'GET',
    headers: env.headers,
  };
  try {
    const response = yield call(request, masterUrl, options);
     console.log('response', response);
    if (response.success)
      yield put(loadEmailsSuccess(response));
    else yield put(loadEmailsError(response));
  } catch (error) {
    yield put(loadEmailsError(error));
  }
}

// Excel Upload

function* updateExcel({ payload }) {

  console.log('Payloadddddd', payload)

  const payloadData = {
    excelFileBase64Stream: payload.base,
  }
  
    const reqUrl = `${env.EMAIL_COMMUNICATION_EXCEL_URL}`;
    const options = {
      method: 'POST',
      body: JSON.stringify(payloadData),
      headers: env.headers,
    };
  
    try {
      const appRes = yield call(request, reqUrl, options);
      console.log('app', appRes)
      if (appRes.success) {
        yield put(excelUploadSuccess({message : _.get(appRes, 'body.message') || 'Emails saved successfully!!'}));
      } else yield put(excelUploadError({message : _.get(appRes, 'message') || 'Emails Not saved successfully!!'}));
      
    } catch (error) {
     yield put(excelUploadError(error));
      console.error(error);
    }
  }


  //Search

  
function* getEmailDataSearch({ payload }) {
  console.log('payload Search', payload);
  const searchData = {
    emailId: payload.emailId,
    category: payload.category,
    searchCriteria: 'partial'
  };
  const masterUrl = `${env.GET_EMAIL_SEARCH}`;
  const options = {
    method: 'POST',
    body: JSON.stringify(searchData),
    headers: env.headers,
  };
  try {
    const response = yield call(request, masterUrl, options);
     console.log('response', response);
    if (response.success)
      yield put(loadEmailsSearchSuccess(response));
    else yield put(loadEmailsError(response));
  } catch (error) {
    yield put(loadEmailsError(error));
  }
}
