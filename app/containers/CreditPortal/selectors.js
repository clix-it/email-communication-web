import { createSelector } from 'reselect';

const selectEmailPortal = state => state.emailPortal;

const makeSelectApplications = () =>
  createSelector(
    selectEmailPortal,
    emailPortal => emailPortal,
  );

export { makeSelectApplications };
