/**
 *
 * LoanDetails
 *
 */
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import _ from 'lodash';
import moment from 'moment';
import { Form, Row, Col, Button, List, Divider, Spin, message } from 'antd';

import ExistingLans from 'containers/ExistingLans';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useForm } from 'react-hook-form';
import { formatDate } from 'utils/helpers';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';
import LoanDetailsProduct from '../../components/LoanDetailsProduct';
import reducer from './reducer';
import saga from './saga';
import makeSelectLoanDetails from './selectors';
import makeSelectExistingLans from '../ExistingLans/selectors';

import LoanDetailsTable from '../../components/LoanDetailsTable';
import LoanDetailsForm from '../../components/LoanDetailsForm';
import InputField from '../../components/InputField';
import {
  updateLoanDetails,
  fetchProgram,
  fetchRestructureFeature,
  fetchLoanTenure,
  fetchFeeCode,
  fetchLoanPurpose,
  fetchLoanFlag,
} from './actions';
import {
  setFormValidationError,
  isDirtyForm,
  loadEntityDetails,
  callCommercialBureau,
} from '../ApplicantDetails/actions';
import DownloadEmiSchedule from '../DownloadEmiSchedule';
import DownloadSoa from '../DownloadSoa';

import { columnResponsiveLayout } from './constants';
import './loanDetails.css';

export function LoanDetails({
  dispatch,
  applicantDetails,
  loanDetails,
  requestType,
  activity,
  isNoEntity,
  existingLans,
}) {
  useInjectReducer({ key: 'loanDetails', reducer });
  useInjectSaga({ key: 'loanDetails', saga });

  const {
    masterProgData = {},
    masterRestructureData = {},
    masterLoanTenureData = {},
    masterFeeCodeData = {},
    masterLoanPurposeData = {},
    masterLoanFlagData = {},
  } = loanDetails;

  const loanChargesLabel = Object.keys(masterFeeCodeData)[0]
    ? Object.keys(masterFeeCodeData)[0]
    : 'Processing Fee';
  const loanChargesLabelType = Object.values(masterFeeCodeData)[0]
    ? Object.values(masterFeeCodeData)[0].split('^')[1]
    : '';
  const loanChargeCode = Object.values(masterFeeCodeData)[0]
    ? Object.values(masterFeeCodeData)[0].split('^')[0]
    : 'pf';
  const progType = masterProgData[_.get(applicantDetails, 'entity.scheme', '')];
  const loanFlag =
    masterLoanFlagData[
      _.get(
        applicantDetails,
        'entity.additionalData.data.extraDataField.loanFlag',
      )
    ];
  const loanPlan =
    masterRestructureData[
      _.get(
        applicantDetails,
        'entity.additionalData.data.extraDataField.loanPlan',
      )
    ];

  let feeCodeObj = {};

  const forclosureAmount = _.get(
    _.orderBy(
      _.filter(
        _.get(applicantDetails, 'entity.loanOffers'),

        { type: 'forclosure_amount' },
      ),
      'id',
      'desc',
    ),
    '[0].loanAmount',
  );

  const optionValues = ((existingLans && existingLans.response) || []).filter(
    lan =>
      lan.loanAccountNumber ==
      _.get(applicantDetails, 'entity.additionalData.data.existingLAN'),
  );

  let lmsSystem = '';

  if (optionValues.length > 0) {
    debugger;
    if (optionValues[0].lmsSystem == 'pennant') {
      lmsSystem = 'pennant';
    }
    // lmsSystem = 'pennant';
  }

  const grace =
    _.get(
      applicantDetails,
      'entity.additionalData.data.extraDataField.grace',
    ) || '0';

  const stepUpArr = _.get(
    applicantDetails,
    'entity.additionalData.data.extraDataField.stepUpDetails',
  )
    ? JSON.parse(
        _.get(
          applicantDetails,
          'entity.additionalData.data.extraDataField.stepUpDetails',
        ),
      )
    : [];

  const finStartDate = formatDate(
    _.get(
      applicantDetails,
      'entity.additionalData.data.extraDataField.finStartDate',
    ),
  );

  const defaultValues = {
    ...applicantDetails.entity,
    'users[0].userDetails.turnover': _.get(
      _.find(
        _.get(applicantDetails, 'entity.users') || [],
        user =>
          (_.get(user, 'appLMS.role') === 'Applicant' ||
            !_.get(user, 'appLMS.role')) &&
          user.type === 'COMPANY',
      ),
      'userDetails.turnover',
    )
      ? _.get(
          _.find(
            _.get(applicantDetails, 'entity.users') || [],
            user =>
              (_.get(user, 'appLMS.role') === 'Applicant' ||
                !_.get(user, 'appLMS.role')) &&
              user.type === 'COMPANY',
          ),
          'userDetails.turnover',
        )
      : _.get(
          _.find(_.get(applicantDetails, 'entity.users', []), {
            type: 'INDIVIDUAL',
          }),
          'userDetails.turnover',
        ),
    appliedLoanAmount: _.get(
      _.orderBy(
        _.filter(_.get(applicantDetails, 'entity.loanOffers'), {
          type: 'applied_amount',
        }),
        'id',
        'desc',
      ),

      '[0].loanAmount',
    ),
    'loanOffersEligible[0].loanAmount': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].loanAmount',
    ),
    'loanOffersEligible[0].roi': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].roi',
    ),
    forclosureAmount: _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),

          { type: 'forclosure_amount' },
        ),
        'id',
        'desc',
      ),
      '[0].loanAmount',
    ),
    'loanOffersEligible[0].advanceEMI': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].advanceEMI',
    ),
    'loanOffersEligible[0].id': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].id',
    ),
    'loanOffersEligible[0].loanTenure': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].loanTenure',
    ),
    'loanOffersQualified[0].loanAmount': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),

          { type: 'qualified_offer' },
        ),
        'id',
        'desc',
      ),

      '[0].loanAmount',
    ),
    'loanOffersQualified[0].roi': _.get(
      _.orderBy(
        _.filter(_.get(applicantDetails, 'entity.loanOffers'), {
          type: 'qualified_offer',
        }),
        'id',
        'desc',
      ),
      '[0].roi',
    ),
    'loanOffersQualified[0].loanTenure': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),

          { type: 'qualified_offer' },
        ),
        'id',
        'desc',
      ),
      '[0].loanTenure',
    ),
    finStartDate: formatDate(
      _.get(
        applicantDetails,
        'entity.additionalData.data.extraDataField.finStartDate',
      ),
    ),
    /* 'loanChargesValue[0].amount': _.get(
      _.orderBy(_.get(applicantDetails, 'entity.loanCharges'), 'id', 'desc'),
      '[0].amount',
      0,
    ),
    'loanChargesValue[0].id': _.get(
      _.orderBy(_.get(applicantDetails, 'entity.loanCharges'), 'id', 'desc'),
      '[0].id',
    ), */
    'additionalData.data.extraDataField.groupExposure': _.get(
      applicantDetails,
      'entity.additionalData.data.extraDataField.groupExposure',
      0,
    ),
    'loanOffersIncremental[0].id': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),

          { type: 'incremental_amount' },
        ),
        'id',
        'desc',
      ),

      '[0].id',
    ),
    'loanOffersIncremental[0].loanAmount': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),

          { type: 'incremental_amount' },
        ),
        'id',
        'desc',
      ),
      '[0].loanAmount',
    ),
    expectedEMI: _.get(
      _.orderBy(
        _.filter(_.get(applicantDetails, 'entity.loanOffers'), {
          type: 'applied_amount',
        }),
        'id',
        'desc',
      ),

      '[0].estimatedEMI',
      '',
    ),
    currentEMI: _.get(
      applicantDetails,
      'entity.additionalData.data.outstandingLoans[0].oblicationAmount',
    ),
    'additionalData.data.extraDataField.restructureReason': _.get(
      applicantDetails,
      'entity.additionalData.data.extraDataField.restructureReason',
    ),
    'additionalData.data.extraDataField.restructureTagging': _.get(
      applicantDetails,
      'entity.additionalData.data.extraDataField.restructureTagging',
    ),
    installments: 48,
    // scheme: _.get(applicantDetails, 'entity.scheme', ''),
    product: _.get(applicantDetails, 'entity.product', ''),
    securityDepositAmount: _.get(
      _.find(_.get(applicantDetails, 'entity.loanCharges', []), {
        chargeCode: 'CASHCLT',
      }),
      'amount',
    ),
  };

  let loanChargeValues = {};
  if (_.get(applicantDetails, 'entity.loanCharges')) {
    loanChargeValues = _.mapValues(
      _.groupBy(
        _.orderBy(applicantDetails.entity.loanCharges, ['id'], ['desc']),
        'chargeCode',
      ),
      clist => clist.map(charge => _.omit(charge, 'chargeCode')),
    );
  }

  const loanSecurityDeposit = _.find(
    _.get(applicantDetails, 'entity.loanCharges', []),
    {
      chargeCode: 'CASHCLT',
    },
  );

  if (loanChargeValues && Object.keys(loanChargeValues).length > 0) {
    if (Object.values(masterFeeCodeData).length > 0) {
      const value = {};
      Object.values(masterFeeCodeData).forEach((item, index) => {
        if (
          loanChargeValues[
            Object.values(masterFeeCodeData)[index].split('^')[0]
          ]
        ) {
          /* defaultValues[`loanChargesValue[${index}].amount`] =
            loanChargeValues[
              Object.values(masterFeeCodeData)[index].split('^')[0]
            ][0].amount; */
          value[Object.keys(masterFeeCodeData)[index]] =
            loanChargeValues[
              Object.values(masterFeeCodeData)[index].split('^')[0]
            ][0].amount;
        }
      });
      feeCodeObj = { ...value };
    }
    /* Object.keys(loanChargeValues).forEach((item, index) => {
      defaultValues[`loanChargesValue[${index}].amount`] =
        loanChargeValues[item][0].amount;
    }); */
  }

  const {
    handleSubmit,
    errors,
    control,
    watch,
    setError,
    clearError,
    getValues,
    setValue,
    reset,
    formState,
    triggerValidation,
  } = useForm({
    mode: 'onBlur',
    defaultValues,
  });

  const roi = _.get(
    _.orderBy(
      _.filter(
        _.get(applicantDetails, 'entity.loanOffers'),
        loanOffer =>
          loanOffer.type === 'credit_amount' ||
          loanOffer.type === 'eligible_amount',
      ),
      'id',
      'desc',
    ),
    '[0].roi',
  );

  // const loanTenure = _.get(
  //   _.orderBy(
  //     _.filter(_.get(applicantDetails, 'entity.loanOffers'), {
  //       type: 'eligible_amount',
  //     }),
  //     'id',
  //     'desc',
  //   ),
  //   '[0].loanTenure',
  // );

  const DECLARATIONS = [
    'Final charges applicable would be on Sanction Letter/Facility Agreement.',
    `Rate of Interest: ${roi || 14}%.`,
    'Processing Fee: 0.',
    'Foreclosure Charges- Nil.',
    'Bounce Charges: Rs 500 +GST per bounce.',
    'Penal Interest-3% per month on overdue amount',
  ];

  const DECLARATIONS_HFS = [
    'Final charges applicable would be on Sanction Letter/Facility Agreement.',
    `Rate of Interest: ${roi || 14}%.`,
    'Processing Fee: Upto 3% of the loan amount',
    'Foreclosure Charges: Foreclosure<= 12 Month - upto 5% of principal outstanding for loan amount>12 Month – upto 4% of Principal outstanding for loan amount',
    'Bounce Charges: Upto INR 800 + GST',
    'Penal Interest- Upto 3% pm on EMI overdue',
  ];

  const DECLARATIONS_X_SELL = [
    'Final charges applicable would be on Sanction Letter/Facility Agreement.',
    'Foreclosure Charges (as a % of outstanding loan amount) : <12 months: 6%, 12 months to <24 months: 5%, 24 months to <48 Months: 4%, <48 Months: 4%, >48 Months: 3%.',
    'Bounce charges: Business Loan - Rs 500 + GST per presentation of cheque/ NACH.',
    'Penal Interest: 3% pm on EMI overdue.',
  ];

  const values = watch();
  const { loading } = loanDetails;

  useEffect(() => {
    reset(defaultValues);
  }, []);

  useEffect(() => {
    if (_.get(applicantDetails, 'entity.partner')) {
      dispatch(
        fetchProgram(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      dispatch(
        fetchLoanTenure(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      dispatch(
        fetchFeeCode(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      dispatch(
        fetchLoanPurpose(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      if (
        _.get(applicantDetails, 'entity.partner') === 'RLA' ||
        _.get(applicantDetails, 'entity.partner') === 'HFSAPP'
      ) {
        dispatch(
          fetchRestructureFeature(
            applicantDetails.entity.partner,
            applicantDetails.entity.product,
          ),
        );
      }
      if (_.get(applicantDetails, 'entity.partner') === 'DSA') {
        dispatch(
          fetchLoanFlag(
            applicantDetails.entity.partner,
            applicantDetails.entity.product,
          ),
        );
      }
    }
  }, []);

  useEffect(() => {
    if (loanDetails.response) {
      const appIdFromSession = sessionStorage
        .getItem('id')
        .replace(/^"(.*)"$/, '$1');
      dispatch(loadEntityDetails(appIdFromSession));
      dispatch(isDirtyForm({ loanDetails: false }));
    }
  }, [loanDetails.response]);

  useEffect(() => {
    dispatch(isDirtyForm({ loanDetails: formState.dirty }));
  }, [formState.dirty]);

  const cuid = _.get(
    _.find(
      _.get(applicantDetails, 'entity.users') || [],
      user =>
        (_.get(user, 'appLMS.role') === 'Applicant' ||
          !_.get(user, 'appLMS.role')) &&
        user.type === 'COMPANY',
    ),
    'cuid',
  )
    ? _.get(
        _.find(
          _.get(applicantDetails, 'entity.users') || [],
          user =>
            (_.get(user, 'appLMS.role') === 'Applicant' ||
              !_.get(user, 'appLMS.role')) &&
            user.type === 'COMPANY',
        ),
        'cuid',
      )
    : _.get(
        _.find(_.get(applicantDetails, 'entity.users') || [], {
          type: 'INDIVIDUAL',
        }),
        'cuid',
      );

  const loanAccountNumber = _.get(
    applicantDetails,
    'entity.additionalData.data.existingLAN',
  );

  const noEntityCuid = _.get(
    _.find(_.get(applicantDetails, 'entity.users') || [], {
      type: 'INDIVIDUAL',
    }),
    'cuid',
  );

  const outstandingLoans = _.get(
    applicantDetails,
    'entity.additionalData.data.outstandingLoans',
    [],
  );

  const checkFinalAmount = (
    loanValue,
    scheme,
    loanOffersIncrementalAmount,
    appliedLoanAmount,
  ) => {
    if (parseInt(loanOffersIncrementalAmount, 10) > parseInt(loanValue, 10))
      return 'Incremental/Offered Amount cannot be more than Final/Gross Loan Amount !';
    if (parseInt(loanValue, 10) > parseInt(appliedLoanAmount, 10)) {
      return 'Final/Gross Loan Amount cannot be more than Applied Loan Amount!';
    }
    if (
      parseInt(loanOffersIncrementalAmount, 10) >
      parseInt(appliedLoanAmount, 10)
    ) {
      return 'Incremental/Offered Amount cannot be more than Applied Loan Amount!';
    }
    let finalLoanAmountLabel = '';
    if (
      (scheme === 'X2CBL-S1' || scheme === 'X2CBL-S2') &&
      parseInt(loanValue, 10) > 5000000
    )
      finalLoanAmountLabel = '50 Lakhs';
    else if (
      (scheme === 'X2CBL-S3' || scheme === 'X2CBL-S4') &&
      parseInt(loanValue, 10) > 4000000
    )
      finalLoanAmountLabel = '40 Lakhs';
    else if (
      isNoEntity &&
      parseInt(loanValue, 10) > 1000000 &&
      _.get(applicantDetails, 'entity.partner', '') !== 'HFSAPP'
    ) {
      finalLoanAmountLabel = '10 Lakhs';
    } else if (
      !isNoEntity &&
      parseInt(loanValue, 10) > 1000000000 &&
      _.get(applicantDetails, 'entity.partner', '') !== 'HFSAPP'
    ) {
      finalLoanAmountLabel = '100 Crore';
    }
    if (finalLoanAmountLabel)
      return `Final/Gross Loan Amount should be less than ${finalLoanAmountLabel}`;
    return '';
  };

  const onSubmit = data => {
    if (cuid !== undefined && cuid !== '') {
      if (_.get(data, 'loanOffersEligible[0].loanAmount', '')) {
        const correctValue = checkFinalAmount(
          _.get(data, 'loanOffersEligible[0].loanAmount', ''),
          masterProgData[_.get(data, 'scheme', '')],
          _.get(data, 'loanOffersIncremental[0].loanAmount', ''),
          _.get(data, 'appliedLoanAmount', ''),
        );
        if (correctValue) {
          message.warn(correctValue);
          return;
        }

        const loanAmount =
          parseInt(_.get(data, 'loanOffersEligible[0].loanAmount', ''), 10) +
          parseInt(
            _.get(data, 'additionalData.data.extraDataField.groupExposure'),
            10,
          );
        if (
          data.product === 'HFS' &&
          data.partner === 'HFSAPP' &&
          loanAmount > 10000000
        ) {
          message.warn(
            'Final Loan Amount(Eligible Loan Amount + Group Exposure) cannot be greater than 1 Crore!',
          );
          return;
        }
      }
      const newLoanOffersEligible = _.get(data, 'loanOffersEligible[0]');
      const newLoanChargesValue = _.get(data, 'loanChargesValue', []);
      const loanChargesId = _.get(
        _.orderBy(
          _.filter(_.get(applicantDetails, 'entity.loanCharges'), {
            chargeCode: 'pf',
          }),
          'id',
          'desc',
        ),
        '[0].id',
      );
      Object.values(masterFeeCodeData).forEach((item, index) => {
        newLoanChargesValue[index].chargeCode = Object.values(
          masterFeeCodeData,
        )[index].split('^')[0];
        if (newLoanChargesValue[index].chargeCode === 'pf') {
          newLoanChargesValue[index].type = 'credit_amount';
          newLoanChargesValue[index].id = loanChargesId;
        }
      });
      // newLoanChargesValue.chargeCode = loanChargeCode;
      const newLoanOffersInc = _.get(data, 'loanOffersIncremental[0]');
      data.scheme = _.findKey(masterProgData, function(o) {
        return o == data.scheme;
      });
      const loanplan =
        data.additionalData &&
        data.additionalData.data &&
        data.additionalData.data.extraDataField &&
        data.additionalData.data.extraDataField.loanPlan
          ? data.additionalData &&
            data.additionalData.data &&
            data.additionalData.data.extraDataField &&
            data.additionalData.data.extraDataField.loanPlan
          : loanPlan;
      if (
        data.additionalData &&
        data.additionalData.data &&
        data.additionalData.data.extraDataField &&
        data.additionalData.data.extraDataField.loanPlan
      ) {
        data.additionalData.data.extraDataField.loanPlan = _.findKey(
          masterRestructureData,
          function(o) {
            return o == data.additionalData.data.extraDataField.loanPlan;
          },
        );
      }

      data.additionalData.data.extraDataField.loanFlag = _.findKey(
        masterLoanFlagData,
        function(o) {
          return o == data.additionalData.data.extraDataField.loanFlag;
        },
      );

      if (data.finStartDate) {
        data.additionalData.data.extraDataField.finStartDate = moment(
          data.finStartDate,
        ).format('YYYY-MM-DD');
      }

      let stepUpArray = [];
      if (loanplan) {
        if (!loanplan.toUpperCase().includes('STEP')) {
          data.additionalData.data.extraDataField.stepUpDetails = '';
        } else if (loanplan.toUpperCase().includes('STEP')) {
          stepUpArray = data.additionalData.data.extraDataField.stepUpDetails
            ? JSON.parse(data.additionalData.data.extraDataField.stepUpDetails)
            : stepUpArr;
          if (stepUpArray && stepUpArray.length == 0) {
            message.info('Atleast one step record should be available!');
            return;
          }
        }
      }
      if (stepUpArray && stepUpArray.length > 0) {
        let stepValuePercentFlag = false;
        let stepValueAmountFlag = false;
        stepUpArray.forEach(item => {
          if (item.stepType == 1 && item.stepValue > 100) {
            stepValuePercentFlag = true;
          } else if (item.stepType == 2 && item.stepValue < 100) {
            stepValueAmountFlag = true;
          }
        });
        if (stepValuePercentFlag) {
          message.info('Step value cannot be greater than 100!');
          return;
        }
        if (stepValueAmountFlag) {
          message.info('Step value cannot be less than 100!');
          return;
        }
      }
      let newData = { ...data };
      if (newLoanChargesValue && newLoanChargesValue.length > 0) {
        if (!newLoanChargesValue.id) delete newLoanChargesValue.id;
        newLoanChargesValue.forEach(item => {
          if (!item.id) delete item.id;
        });
        newData = {
          ...newData,
          loanCharges: [...newLoanChargesValue],
        };
        let shouldSkip = false;
        newLoanChargesValue.forEach((item, index) => {
          if (item.amount) {
            if (
              Object.values(masterFeeCodeData)[index].split('^')[1] == 'PER' &&
              parseInt(item.amount) > 100
            ) {
              message.info('Processing fee should be less than 100');
              shouldSkip = true;
            }
            if (
              Object.values(masterFeeCodeData)[index].split('^')[1] ==
                'FIXED' &&
              parseInt(item.amount) < 100
            ) {
              message.info('Fee should be greater than 100');
              shouldSkip = true;
            }
          }
        });
        if (shouldSkip) {
          return;
        }
        /* if (newLoanChargesValue.amount) {
          if (
            loanChargesLabelType == 'PER' &&
            parseInt(newLoanChargesValue.amount) > 100
          ) {
            message.info('Processing fee should be less than 100');
            return;
          }
          if (
            loanChargesLabelType == 'FIXED' &&
            parseInt(newLoanChargesValue.amount) < 100
          ) {
            message.info('Restructuring fee should be greater than 100');
            return;
          }
        } */
      }
      if (data.securityDepositAmount) {
        newData = {
          ...newData,
          loanCharges: [
            ...newData.loanCharges,
            {
              id: (loanSecurityDeposit || {}).id,
              amount: data.securityDepositAmount,
              chargeCode: 'CASHCLT',
            },
          ],
        };
      }
      if (newLoanOffersEligible) {
        if (!newLoanOffersEligible.id) delete newLoanOffersEligible.id;
        newData = {
          ...newData,
          loanOffers: [
            {
              ...newLoanOffersEligible,
              type: 'credit_amount',
            },
          ],
        };
      }
      if (newLoanOffersInc && newLoanOffersInc.loanAmount) {
        if (!newLoanOffersInc.id) delete newLoanOffersInc.id;
        if (newData.loanOffers && newData.loanOffers.length > 0)
          newData = {
            ...newData,
            loanOffers: [
              ...newData.loanOffers,
              {
                ...newLoanOffersInc,
                type: 'incremental_amount',
              },
            ],
          };
        else
          newData = {
            ...newData,
            loanOffers: [
              {
                ...newLoanOffersInc,
                type: 'incremental_amount',
              },
            ],
          };
      }
      delete newData.loanOffersEligible;
      delete newData.loanChargesValue;
      delete newData.loanOffersIncremental;
      dispatch(updateLoanDetails(newData));
      // console.log(newData, 'loan data in form submit');
      // if (!isNoEntity) dispatch(callCommercialBureau({ cuid }));
    }
  };

  useEffect(() => {
    if (requestType === 'PMA')
      triggerValidation().then(valid => {
        dispatch(setFormValidationError('loanDetails', valid));
      });
    reset(defaultValues);
  }, [applicantDetails.entity]);

  return (
    <div className="App">
      <Spin spinning={loading} tip="Submitting Data...">
        <h4>Loan Details</h4>
        <Divider />
        <form onSubmit={handleSubmit(onSubmit)}>
          <Row gutter={[16, 16]}>
            <Col xs={0}>
              <InputField
                id="loanOffersEligible[0].id"
                control={control}
                errors={errors}
                name="loanOffersEligible[0].id"
                type="hidden"
              />
              {/* <InputField
                id="loanChargesValue[0].id"
                control={control}
                errors={errors}
                name="loanChargesValue[0].id"
                type="hidden"
              /> */}
              <InputField
                id="loanOffersIncremental[0].id"
                control={control}
                errors={errors}
                name="loanOffersIncremental[0].id"
                type="hidden"
              />
              <InputField
                id="users[0].id"
                control={control}
                errors={errors}
                name="users[0].id"
                type="hidden"
                value={_.get(
                  _.find(
                    _.get(applicantDetails, 'entity.users') || [],
                    ele =>
                      (_.get(ele, 'appLMS.role') === 'Applicant' ||
                        !_.get(ele, 'appLMS.role')) &&
                      ele.type === 'COMPANY',
                  ),
                  'id',
                  '',
                )}
              />
              <InputField
                id="users[0].cuid"
                control={control}
                errors={errors}
                name="users[0].cuid"
                type="hidden"
                value={cuid}
              />
              <InputField
                id="users[0].userDetails.id"
                control={control}
                errors={errors}
                name="users[0].userDetails.id"
                type="hidden"
                value=""
              />
              <InputField
                id="additionalData.data.deviations"
                control={control}
                errors={errors}
                name="additionalData.data.deviations"
                type="hidden"
                value=""
              />
              <InputField
                id="additionalData.data.outstandingLoans"
                control={control}
                errors={errors}
                name="additionalData.data.outstandingLoans"
                type="hidden"
                value=""
              />
              <InputField
                id="additionalData.data.extraDataField.stepUpDetails"
                control={control}
                errors={errors}
                name="additionalData.data.extraDataField.stepUpDetails"
                type="hidden"
                value=""
              />
              <InputField
                id="additionalData.id"
                control={control}
                errors={errors}
                name="additionalData.id"
                type="hidden"
                value=""
              />
            </Col>
            <LoanDetailsProduct
              control={control}
              errors={errors}
              values={values}
              product={_.get(values, 'product')}
              partner={_.get(values, 'partner')}
              finalLoanAmount={_.get(
                values,
                'loanOffersEligible[0].loanAmount',
                0,
              )}
              exposure={_.get(
                values,
                'additionalData.data.extraDataField.groupExposure',
              )}
              clearError={clearError}
              getValues={getValues}
              setError={setError}
              setValue={setValue}
            />
            <LoanDetailsForm
              control={control}
              errors={errors}
              values={values}
              clearError={clearError}
              getValues={getValues}
              setError={setError}
              setValue={setValue}
              requestType={requestType}
              triggerValidation={triggerValidation}
              dispatch={dispatch}
              product={_.get(values, 'product')}
              partner={_.get(applicantDetails, 'entity.partner', '')}
              // loanTenure={loanTenure}
              isNoEntity={isNoEntity}
              masterProgData={masterProgData}
              masterRestructureData={masterRestructureData}
              masterLoanTenureData={masterLoanTenureData}
              masterLoanFlagData={masterLoanFlagData}
              progType={progType}
              loanPlan={loanPlan}
              loanFlag={loanFlag}
              grace={grace}
              finStartDate={finStartDate}
              lmsSystem={lmsSystem}
              stepUpArr={stepUpArr}
              outstandingLoans={outstandingLoans}
              // loanChargesLabel={loanChargesLabel}
              // loanChargesLabelType={loanChargesLabelType}
              masterFeeCodeData={masterFeeCodeData}
              activity={activity}
              forclosureAmount={forclosureAmount}
              feeCodeObj={feeCodeObj}
              masterLoanPurposeData={masterLoanPurposeData}
            />
            <Col {...columnResponsiveLayout.colHalf}>
              {_.get(applicantDetails, 'entity.partner') != 'DL' && (
                <ExistingLans
                  cuid={cuid || noEntityCuid}
                  id="additionalData.data.existingLAN"
                  existingLAN={_.get(values, 'additionalData.data.existingLAN')}
                  control={control}
                  name="additionalData.data.existingLAN"
                  rules={{
                    required: {
                      value:
                        !isNoEntity &&
                        _.get(applicantDetails, 'entity.partner', '') !==
                          'DL' &&
                        _.get(applicantDetails, 'entity.partner', '') !==
                          'DSA' &&
                        _.get(applicantDetails, 'entity.partner', '') !==
                          'HFSAPP',
                      message: 'This Field should not be empty!',
                    },
                    maxLength: {
                      value: 100,
                      message: 'Max length 100 chracters allowed',
                    },
                  }}
                  product={_.get(values, 'product')}
                  partner={_.get(applicantDetails, 'entity.partner', '')}
                  type="string"
                  errors={errors}
                  placeholder="Existing Lan Details"
                  labelHtml="Existing Lan Details"
                  applicantDetails={applicantDetails}
                />
              )}
              {_.get(applicantDetails, 'entity.partner') === 'DL' && (
                <InputField
                  id="additionalData.data.existingLAN"
                  control={control}
                  name="additionalData.data.existingLAN"
                  defaultValue={
                    _.get(
                      applicantDetails,
                      'entity.additionalData.data.existingLAN',
                    ) || ''
                  }
                  rules={{
                    required: {
                      value:
                        !isNoEntity &&
                        _.get(applicantDetails, 'entity.partner', '') !==
                          'DL' &&
                        _.get(applicantDetails, 'entity.partner', '') !== 'DSA',
                      message: 'This Field should not be empty!',
                    },
                    maxLength: {
                      value: 100,
                      message: 'Max length 100 chracters allowed',
                    },
                  }}
                  type="string"
                  errors={errors}
                  placeholder="Existing Lan Details"
                  labelHtml="Existing Lan Details"
                />
              )}
              {/* loanAccountNumber && (
                <>
                  <DownloadEmiSchedule
                    loan={{
                      ...applicantDetails.entity,
                      loanAccountNumber: _.get(
                        values,
                        'additionalData.data.existingLAN',
                      ),
                    }}
                  />
                  {
                    <DownloadSoa
                      loan={{
                        ...applicantDetails.entity,
                        loanAccountNumber: _.get(
                          values,
                          'additionalData.data.existingLAN',
                        ),
                      }}
                    />
                  }
                </>
                ) */}
            </Col>
          </Row>
          <Form.Item name="loanOutstanding" label="Declared Obligation">
            <LoanDetailsTable
              values={values}
              setValue={setValue}
              outstandingLoans={outstandingLoans}
            />
          </Form.Item>
          {!isNoEntity && _.get(applicantDetails, 'entity.partner') === 'D2C' && (
            <Form.Item name="declarations" label="Schedule of Charges">
              <List
                size="small"
                bordered
                dataSource={
                  _.get(values, 'partner') === 'X2C'
                    ? DECLARATIONS_X_SELL
                    : DECLARATIONS
                }
                renderItem={item => <List.Item>{item}</List.Item>}
              />
            </Form.Item>
          )}
          {_.get(applicantDetails, 'entity.partner') === 'HFSAPP' && (
            <Form.Item name="declarations" label="Schedule of Charges">
              <List
                size="small"
                bordered
                dataSource={DECLARATIONS_HFS}
                renderItem={item => <List.Item>{item}</List.Item>}
              />
            </Form.Item>
          )}
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              disabled={
                requestType !== 'PMA' ||
                activity === 'CREDIT_FCU' ||
                activity === 'HUNTER_REVIEW'
              }
              // onClick={handleSubmit(onSubmit)}
            >
              Save
            </Button>
          </Form.Item>
        </form>
      </Spin>
    </div>
  );
}

LoanDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
  applicantDetails: PropTypes.object,
  loanDetails: PropTypes.object,
  requestType: PropTypes.string,
};

LoanDetails.defaultProps = {
  applicantDetails: {},
  loanDetails: {},
  requestType: 'PMA',
};

const mapStateToProps = createStructuredSelector({
  applicantDetails: makeSelectApplicantDetails2(),
  loanDetails: makeSelectLoanDetails(),
  existingLans: makeSelectExistingLans(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(LoanDetails);
