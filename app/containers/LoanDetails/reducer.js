/*
 *
 * LoanDetails reducer
 *
 */
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
  UPDATE_LOAN_DETAILS,
  UPDATE_LOAN_DETAILS_SUCCESS,
  UPDATE_LOAN_DETAILS_ERROR,
  FETCH_PROGRAM_SUCCESS,
  FETCH_PROGRAM_ERROR,
  FETCH_RESTRUCTURE_FEATURE_SUCCESS,
  FETCH_RESTRUCTURE_FEATURE_ERROR,
  FETCH_LOAN_TENURE_SUCCESS,
  FETCH_LOAN_TENURE_ERROR,
  FETCH_FEE_CODE_SUCCESS,
  FETCH_FEE_CODE_ERROR,
  FETCH_LOAN_PURPOSE_SUCCESS,
  FETCH_LOAN_PURPOSE_ERROR,
  FETCH_LOAN_FLAG,
  FETCH_LOAN_FLAG_SUCCESS,
  FETCH_LOAN_FLAG_ERROR,
} from './constants';

export const initialState = {
  response: false,
  error: false,
  payload: false,
  loading: false,
  masterProgData: {},
  masterRestructureData: {},
  masterLoanTenureData: {},
  masterFeeCodeData: {},
  masterLoanPurposeData: {},
  masterLoanFlagData: {},
};

/* eslint-disable default-case, no-param-reassign */
const loanDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_LOAN_DETAILS:
        draft.loading = true;
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case UPDATE_LOAN_DETAILS_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.response = action.data.message;
        break;
      case UPDATE_LOAN_DETAILS_ERROR:
        draft.loading = false;
        draft.response = false;
        draft.error = `Data not saved`;
        break;
      case FETCH_PROGRAM_SUCCESS:
        let tempObj = {};
        action.data.forEach(item => {
          tempObj[item.cmValue] = item.cmCode;
        });
        draft.masterProgData = tempObj;
        break;
      case FETCH_PROGRAM_ERROR:
        draft.masterProgData = {};
        break;
      case FETCH_RESTRUCTURE_FEATURE_SUCCESS:
        let tempObj1 = {};
        action.data.forEach(item => {
          tempObj1[item.cmValue] = item.cmCode;
        });
        draft.masterRestructureData = tempObj1;
        break;
      case FETCH_RESTRUCTURE_FEATURE_ERROR:
        draft.masterRestructureData = [];
        break;
      case FETCH_LOAN_TENURE_SUCCESS:
        let tempObj2 = {};
        action.data.forEach(item => {
          tempObj2[item.cmValue] = item.cmCode;
        });
        draft.masterLoanTenureData = tempObj2;
        break;
      case FETCH_LOAN_TENURE_ERROR:
        draft.masterLoanTenureData = {};
        break;
      case FETCH_FEE_CODE_SUCCESS:
        let tempObj3 = {};
        action.data.forEach(item => {
          tempObj3[item.cmValue] = item.cmCode;
        });
        draft.masterFeeCodeData = tempObj3;
        break;
      case FETCH_FEE_CODE_ERROR:
        draft.masterFeeCodeData = {};
        break;
      case FETCH_LOAN_PURPOSE_SUCCESS:
        let tempObj4 = {};
        action.data.forEach(item => {
          tempObj4[item.cmValue] = item.cmCode;
        });
        draft.masterLoanPurposeData = tempObj4;
        break;
      case FETCH_LOAN_PURPOSE_ERROR:
        draft.masterLoanPurposeData = {};
        break;
      case FETCH_LOAN_FLAG_SUCCESS:
        let tempObj5 = {};
        action.data.forEach(item => {
          tempObj5[item.cmValue] = item.cmCode;
        });
        draft.masterLoanFlagData = tempObj5;
        break;
      case FETCH_LOAN_FLAG_ERROR:
        draft.masterLoanFlagData = {};
        break;
      case LOCATION_CHANGE:
        draft.masterProgData = {};
        draft.masterRestructureData = {};
        draft.masterLoanTenureData = {};
        draft.masterFeeCodeData = {};
        draft.masterLoanPurposeData = {};
        draft.masterLoanFlagData = {};
        break;
    }
  });

export default loanDetailsReducer;
