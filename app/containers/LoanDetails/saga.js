import { call, put, takeEvery, all } from 'redux-saga/effects';
import { message } from 'antd';
import {
  UPDATE_LOAN_DETAILS,
  FETCH_PROGRAM,
  FETCH_RESTRUCTURE_FEATURE,
  FETCH_LOAN_TENURE,
  FETCH_FEE_CODE,
  FETCH_LOAN_PURPOSE,
  FETCH_LOAN_FLAG,
} from './constants';
import { updateApplication, updateCompositeApplication } from '../App/saga';
import {
  updateLoanDetailsSuccess,
  updateLoanDetailsError,
  fetchProgramSuccess,
  fetchProgramError,
  fetchRestructureFeatureSuccess,
  fetchRestructureFeatureError,
  fetchLoanTenureSuccess,
  fetchLoanTenureError,
  fetchFeeCodeSuccess,
  fetchFeeCodeError,
  fetchLoanPurposeSuccess,
  fetchLoanPurposeError,
  fetchLoanFlagSuccess,
  fetchLoanFlagError,
} from './actions';
import env from '../../../environment';
import request from 'utils/request';
import _ from 'lodash';
export default function* loanDetailsSaga() {
  yield takeEvery(UPDATE_LOAN_DETAILS, updateApplicationFunction);
  yield takeEvery(FETCH_PROGRAM, fetchProgram);
  yield takeEvery(FETCH_RESTRUCTURE_FEATURE, fetchRestructureFeature);
  yield takeEvery(FETCH_LOAN_TENURE, fetchLoanTenure);
  yield takeEvery(FETCH_FEE_CODE, fetchFeeCode);
  yield takeEvery(FETCH_LOAN_PURPOSE, fetchLoanPurpose);
  yield takeEvery(FETCH_LOAN_FLAG, fetchLoanFlag);
}

export function* fetchFeeCode({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=FEECODES_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchFeeCodeSuccess(response.masterData));
    } else {
      yield put(fetchFeeCodeError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchFeeCodeError('Master Data not found'));
  }
}

export function* fetchLoanPurpose({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=LOANPURPOSE_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchLoanPurposeSuccess(response.masterData));
    } else {
      yield put(fetchLoanPurposeError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchLoanPurposeError('Master Data not found'));
  }
}

export function* fetchLoanTenure({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=LOANTENURE_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchLoanTenureSuccess(response.masterData));
    } else {
      yield put(fetchLoanTenureError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchLoanTenureError('Master Data not found'));
  }
}

export function* fetchProgram({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=PROGRAM_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchProgramSuccess(response.masterData));
    } else {
      yield put(fetchProgramError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchProgramError('Master Data not found'));
  }
}

export function* fetchLoanFlag({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=LOANFLAG_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchLoanFlagSuccess(response.masterData));
    } else {
      yield put(fetchLoanFlagError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchLoanFlagError('Master Data not found'));
  }
}

export function* fetchRestructureFeature({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=LOANPLAN_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchRestructureFeatureSuccess(response.masterData));
    } else {
      yield put(fetchRestructureFeatureError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchRestructureFeatureError('Master Data not found'));
  }
}

export function* updateApplicationFunction(action) {
  try {
    const response = yield updateCompositeApplication({
      ...action.payload.data,
      appId: JSON.parse(sessionStorage.getItem('id')),
    });
    const appServiceResponse = _.get(response, 'data[1].appServiceResponse');

    if (_.get(appServiceResponse, 'body.success')) {
      message.success('Entity updated!!');
      yield all([
        put(
          updateLoanDetailsSuccess({
            message:
              _.get(appServiceResponse, 'body.message') ||
              'app updated successfully!!',
          }),
        ),
      ]);
    } else {
      message.success('Entity could not be Updated!!');
      yield put(updateLoanDetailsError({ error: 'Error' }));
    }
  } catch (error) {
    message.success(`Error ${error} occured!!`);
    yield put(updateLoanDetailsError({ error: 'Error' }));
  }
}
