/*
 *
 * LoanDetails constants
 *
 */

export const FETCH_PROGRAM = 'FETCH_PROGRAM';
export const FETCH_PROGRAM_SUCCESS = 'FETCH_PROGRAM_SUCCESS';
export const FETCH_PROGRAM_ERROR = 'FETCH_PROGRAM_ERROR';

export const FETCH_LOAN_FLAG = 'FETCH_LOAN_FLAG';
export const FETCH_LOAN_FLAG_SUCCESS = 'FETCH_LOAN_FLAG_SUCCESS';
export const FETCH_LOAN_FLAG_ERROR = 'FETCH_LOAN_FLAG_ERROR';

export const FETCH_RESTRUCTURE_FEATURE = 'FETCH_RESTRUCTURE_FEATURE';
export const FETCH_RESTRUCTURE_FEATURE_SUCCESS = 'FETCH_RESTRUCTURE_FEATURE_SUCCESS';
export const FETCH_RESTRUCTURE_FEATURE_ERROR = 'FETCH_RESTRUCTURE_FEATURE_ERROR';

export const FETCH_LOAN_TENURE = 'FETCH_LOAN_TENURE';
export const FETCH_LOAN_TENURE_SUCCESS = 'FETCH_LOAN_TENURE_SUCCESS';
export const FETCH_LOAN_TENURE_ERROR = 'FETCH_LOAN_TENURE_ERROR';

export const FETCH_LOAN_PURPOSE = 'FETCH_LOAN_PURPOSE';
export const FETCH_LOAN_PURPOSE_SUCCESS = 'FETCH_LOAN_PURPOSE_SUCCESS';
export const FETCH_LOAN_PURPOSE_ERROR = 'FETCH_LOAN_PURPOSE_ERROR';

export const FETCH_FEE_CODE = 'FETCH_FEE_CODE';
export const FETCH_FEE_CODE_SUCCESS = 'FETCH_FEE_CODE_SUCCESS';
export const FETCH_FEE_CODE_ERROR = 'FETCH_FEE_CODE_ERROR';

export const DEFAULT_ACTION = 'app/LoanDetails/DEFAULT_ACTION';
export const UPDATE_LOAN_DETAILS = 'app/LoanDetails/UPDATE_LOAN_DETAILS';
export const UPDATE_LOAN_DETAILS_SUCCESS =
  'app/LoanDetails/UPDATE_LOAN_DETAILS_SUCCESS';
export const UPDATE_LOAN_DETAILS_ERROR =
  'app/LoanDetails/UPDATE_LOAN_DETAILS_ERROR';

export const RESTRUCTURE_REASON = [
  'No Impact',
  'Salary cut',
  'Job loss',
  'Business slow',
  'Business closed temporary',
  'Business closed permanent',
  'Additional income is impacted',
];
export const RESTRUCTURE_TAGGING = [
  'MSME – less than 25 Cr',
  'Personal loans',
  'Other Exposure',
  'Small Businesses',
];

export const RESTRUCTURE_FEATURE = [];

export const PRODUCT = [
  'BL',
  'LAEP',
  'K12 Secured / LAP',
  'K12 Unsecured',
  'HFS',
  'EF',
  'SME Auto lease',
  'TW',
  'Used Car',
];

export const DECLARATIONS = [
  'Final charges applicable would be on Sanction Letter/Facility Agreement.',
  'Rate of Interest: 14%.',
  'Processing Fee: 0.',
  'Foreclosure Charges- Nil.',
  'Bounce Charges: Rs 500 +GST per bounce.',
  'Penal Interest-3% per month on overdue amount',
];

export const LOAN_OPTIONS_BL = [
  'Business Purpose',
  'Working Capital Requirement',
  'Restarting Business Operation',
  'For meeting the operation liabilities',
];

export const LOAN_OPTIONS_PL = [
  'Home Renovation',
  'Marriage',
  'Travel',
  'Education',
  'Consumer Durables',
  'Others',
];

export const LOAN_TENURE_OPTIONS_BL = [48];
export const LOAN_TENURE_OPTIONS_PL = [24, 30, 36, 42, 48, 54, 60];

export const columnResponsiveLayout = {
  colSingle: {
    xs: 24,
    sm: 24,
    md: 24,
    lg: 24,
    xl: { span: 24, offset: 2, pull: 2 },
  },
  colHalf: {
    xs: 24,
    sm: 24,
    md: 12,
    lg: 12,
    xl: { span: 10, offset: 2, pull: 2 },
  },
  colOneThird: {
    xs: 24,
    sm: 24,
    md: 24,
    lg: 5,
    xl: { span: 5, offset: 2, pull: 2 },
  },
};
