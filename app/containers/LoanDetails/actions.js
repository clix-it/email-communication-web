/*
 *
 * LoanDetails actions
 *
 */

import {
  DEFAULT_ACTION,
  UPDATE_LOAN_DETAILS,
  UPDATE_LOAN_DETAILS_ERROR,
  UPDATE_LOAN_DETAILS_SUCCESS,
  FETCH_PROGRAM,
  FETCH_PROGRAM_SUCCESS,
  FETCH_PROGRAM_ERROR,
  FETCH_RESTRUCTURE_FEATURE,
  FETCH_RESTRUCTURE_FEATURE_SUCCESS,
  FETCH_RESTRUCTURE_FEATURE_ERROR,
  FETCH_LOAN_TENURE,
  FETCH_LOAN_TENURE_SUCCESS,
  FETCH_LOAN_TENURE_ERROR,
  FETCH_FEE_CODE,
  FETCH_FEE_CODE_SUCCESS,
  FETCH_FEE_CODE_ERROR,
  FETCH_LOAN_PURPOSE,
  FETCH_LOAN_PURPOSE_SUCCESS,
  FETCH_LOAN_PURPOSE_ERROR,
  FETCH_LOAN_FLAG,
  FETCH_LOAN_FLAG_SUCCESS,
  FETCH_LOAN_FLAG_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function updateLoanDetails(data) {
  return {
    type: UPDATE_LOAN_DETAILS,
    payload: {
      data,
    },
  };
}

export function updateLoanDetailsSuccess(data) {
  return {
    type: UPDATE_LOAN_DETAILS_SUCCESS,
    data,
  };
}

export function updateLoanDetailsError(err) {
  return {
    type: UPDATE_LOAN_DETAILS_ERROR,
    data: err,
  };
}

export function fetchProgram(partner, product) {
  // ;
  return {
    type: FETCH_PROGRAM,
    partner,
    product,
  };
}

export function fetchProgramSuccess(data) {
  // ;
  return {
    type: FETCH_PROGRAM_SUCCESS,
    data,
  };
}

export function fetchProgramError(error) {
  // ;
  return {
    type: FETCH_PROGRAM_ERROR,
    error,
  };
}

export function fetchRestructureFeature(partner, product) {
  // ;
  return {
    type: FETCH_RESTRUCTURE_FEATURE,
    partner,
    product,
  };
}

export function fetchRestructureFeatureSuccess(data) {
  // ;
  return {
    type: FETCH_RESTRUCTURE_FEATURE_SUCCESS,
    data,
  };
}

export function fetchRestructureFeatureError(error) {
  // ;
  return {
    type: FETCH_RESTRUCTURE_FEATURE_ERROR,
    error,
  };
}

export function fetchLoanTenure(partner, product) {
  // ;
  return {
    type: FETCH_LOAN_TENURE,
    partner,
    product,
  };
}

export function fetchLoanTenureSuccess(data) {
  // ;
  return {
    type: FETCH_LOAN_TENURE_SUCCESS,
    data,
  };
}

export function fetchLoanTenureError(error) {
  // ;
  return {
    type: FETCH_LOAN_TENURE_ERROR,
    error,
  };
}

export function fetchFeeCode(partner, product) {
  // ;
  return {
    type: FETCH_FEE_CODE,
    partner,
    product,
  };
}

export function fetchFeeCodeSuccess(data) {
  // ;
  return {
    type: FETCH_FEE_CODE_SUCCESS,
    data,
  };
}

export function fetchFeeCodeError(error) {
  // ;
  return {
    type: FETCH_FEE_CODE_ERROR,
    error,
  };
}

export function fetchLoanPurpose(partner, product) {
  // ;
  return {
    type: FETCH_LOAN_PURPOSE,
    partner,
    product,
  };
}

export function fetchLoanPurposeSuccess(data) {
  // ;
  return {
    type: FETCH_LOAN_PURPOSE_SUCCESS,
    data,
  };
}

export function fetchLoanPurposeError(error) {
  // ;
  return {
    type: FETCH_LOAN_PURPOSE_ERROR,
    error,
  };
}

export function fetchLoanFlag(partner, product) {
  // ;
  return {
    type: FETCH_LOAN_FLAG,
    partner,
    product,
  };
}

export function fetchLoanFlagSuccess(data) {
  // ;
  return {
    type: FETCH_LOAN_FLAG_SUCCESS,
    data,
  };
}

export function fetchLoanFlagError(error) {
  // ;
  return {
    type: FETCH_LOAN_FLAG_ERROR,
    error,
  };
}
