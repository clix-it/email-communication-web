import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import { PINCODE_API_URL, HEADERS } from 'containers/App/constants';
import request from 'utils/request';
import { FETCH_APPLICANT } from './constants';
import env from '../../../environment';
import { applicantFetched, applicantFetchingFailed } from './actions';
// Individual exports for testing

export function* fetchApplicant({ cuid }) {
  const requestURL = `${env.USER_SERVICE_API_URL}/${cuid}`;
  try {
    // Call our request helper (see 'utils/request')
    const options = {
      headers: env.headers,
    };
    const response = yield call(request, requestURL, options);
    if (response.success) {
      yield put(applicantFetched(response.user, cuid));
    } else {
      yield put(applicantFetchingFailed(response.message));
    }
  } catch (err) {
    yield put(applicantFetchingFailed(err));
  }
}

export default function* applicantPageSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_APPLICANT, fetchApplicant);
}
