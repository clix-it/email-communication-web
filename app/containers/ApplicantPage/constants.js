/*
 *
 * ApplicantPage constants
 *
 */

export const FETCH_APPLICANT = 'app/ApplicantPage/FETCH_APPLICANT';
export const FETCH_APPLICANT_SUCCESS =
  'app/ApplicantPage/FETCH_APPLICANT_SUCCESS';
export const FETCH_APPLICANT_FAILED =
  'app/ApplicantPage/FETCH_APPLICANT_FAILED';

export const UPDATE_APPLICATION = 'app/ApplicantPage/UPDATE_APPLICATION';
export const UPDATE_APPLICATION_SUCCESS =
  'app/ApplicantPage/UPDATE_APPLICATION_SUCCESS';
export const UPDATE_APPLICATION_FAILED =
  'app/ApplicantPage/UPDATE_APPLICATION_FAILED';
