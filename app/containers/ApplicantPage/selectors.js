import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the applicantPage state domain
 */

const selectApplicantPageDomain = state => state.applicantPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ApplicantPage
 */

const makeSelectApplicantPage = () =>
  createSelector(
    selectApplicantPageDomain,
    substate => substate,
  );

export default makeSelectApplicantPage;
export { selectApplicantPageDomain };
