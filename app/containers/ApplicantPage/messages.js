/*
 * ApplicantPage Messages
 *
 * This contains all the text for the ApplicantPage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ApplicantPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ApplicantPage container!',
  },
});
