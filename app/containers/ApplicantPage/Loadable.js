/**
 *
 * Asynchronously loads the component for ApplicantPage
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
