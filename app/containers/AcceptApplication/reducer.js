/*
 *
 * AcceptApplication reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  APPROVE_APP_ERROR,
  APPROVE_APP,
  APPROVE_APP_SUCCESS,
} from './constants';

export const initialState = {
  loading: false,
  error: false,
  payload: false,
  response: false,
};

/* eslint-disable default-case, no-param-reassign */
const acceptApplicationReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case APPROVE_APP:
        draft.loading = true;
        draft.payload = action.appId;
        break;
      case APPROVE_APP_SUCCESS:
        draft.response = action.response;
        draft.loading = false;

        break;

      case APPROVE_APP_ERROR:
        break;
    }
  });

export default acceptApplicationReducer;
