import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the acceptApplication state domain
 */

const selectAcceptApplicationDomain = state =>
  state.acceptApplication || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by AcceptApplication
 */

const makeSelectAcceptApplication = () =>
  createSelector(
    selectAcceptApplicationDomain,
    substate => substate,
  );

export default makeSelectAcceptApplication;
export { selectAcceptApplicationDomain };
