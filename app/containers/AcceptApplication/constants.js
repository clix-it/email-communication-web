/*
 *
 * AcceptApplication constants
 *
 */

export const DEFAULT_ACTION = 'app/AcceptApplication/DEFAULT_ACTION';

export const APPROVE_APP = 'app/AcceptApplication/DEFAULT_ACTION';
export const APPROVE_APP_SUCCESS = 'app/AcceptApplication/APPROVE_APP_SUCCESS';
export const APPROVE_APP_ERROR = 'app/AcceptApplication/APPROVE_APP_ERROR';
APPROVE_APP;
