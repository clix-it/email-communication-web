/**
 *
 * Asynchronously loads the component for AcceptApplication
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
