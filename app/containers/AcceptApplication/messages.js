/*
 * AcceptApplication Messages
 *
 * This contains all the text for the AcceptApplication container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.AcceptApplication';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the AcceptApplication container!',
  },
});
