// import { take, call, put, select } from 'redux-saga/effects';

// Individual exports for testing

// import { take, call, put, select } from 'redux-saga/effects';
import { call, put, select, takeEvery } from 'redux-saga/effects';
// import { RPS_API_URL } from 'containers/App/constants';
import { approveApplicationSuccess, approveApplicationError } from './actions';
import {
  APPROVE_APP,
  APPROVE_APP_ERROR,
  APPROVE_APP_SUCCESS,
} from './constants';
// import moment from 'moment';
import * as actions from './constants';
import env from '../../../environment';

// import request from 'utils/request';
// import { downloadFile } from '../../utils/helpers';

// Individual exports for testing
export default function* acceptApplicationSaga() {
  yield takeEvery(APPROVE_APP, acceptApplication);
}

/**
 * Github repos request/response handler
 */

// export function* fetchRps({ params: { lms, product, ...rest } }) {
export function* acceptApplication({ appId }) {
  // 'https://laas-test-api.clix.capital/${env.MAKER_CHECKER_API_URL}/v1/user/decision';
  // const requestURL = `${env.API_URL}/8h6v7f7sr0//makerChecker/v1/user/decision`;

  try {
    let result = yield fetch(
      `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v1/user/decision`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({
          activitySrlNo: 36,
          employeeId: appId,
          status: 'APPROVED',
          remarks: 'OK',
          nextApprovalUserId: '',
        }),
      },
    );
    if (result.status === 200) {
      let responseBody = yield result.json();
      if (responseBody.success) {
        yield put(approveApplicationSuccess(responseBody));
      } else {
        yield put(approveApplicationError('Could not approve the request'));
      }
    } else {
      yield put(
        approveApplicationError(
          `HTTP Status${result.status} occurred.Try Again`,
        ),
      );
    }
  } catch (err) {
    yield put(
      approveApplicationError('Network error occurred! Check connection!'),
    );
  }
}

/**
 * Root saga manages watcher lifecycle
 */
