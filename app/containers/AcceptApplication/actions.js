/*
 *
 * AcceptApplication actions
 *
 */

// import { DEFAULT_ACTION } from './constants';

import * as actions from './constants';

export function approveApplication(appId) {
  return {
    type: actions.APPROVE_APP,
    appId,
  };
}

export function approveApplicationSuccess() {
  return {
    type: actions.APPROVE_APP_SUCCESS,
  };
}

export function approveApplicationError() {
  return {
    type: actions.APPROVE_APP_ERROR,
  };
}
