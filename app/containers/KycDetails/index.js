/**
 *
 * KycDetails
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import { makeSelectKycDetails } from './selectors';

import { loadOkycDetails, loadCkycDetails, loadDKYCDocs } from './actions';
import { Spin } from 'antd';
import KycComponent from '../../components/KycComponent';

export function KycDetails({
  kycDetails,
  dispatch,
  requestType,
  appId,
  kycType,
  applicantDetails,
}) {
  const {
    okycData = {},
    ckycData = {},
    loading = false,
    OkycUsers = [],
    CkycUsers = [],
    selectedOkycUser = '',
    selectedCkycUser = '',
    okycDocsData = {},
    ckycDocsData = {},
    dkycDocs = [],
    dkycDetails = {},
  } = kycDetails;

  useEffect(() => {     
    if (
      applicantDetails &&
      applicantDetails.entity &&
      applicantDetails.entity.users &&
      kycType == 'OKYC'
    ) {
      dispatch(loadOkycDetails(applicantDetails.entity.users));
    } else if (
      applicantDetails &&
      applicantDetails.entity &&
      applicantDetails.entity.users &&
      kycType == 'CKYC'
    ) {
      dispatch(loadCkycDetails(applicantDetails.entity.users));
      dispatch(loadDKYCDocs(appId));
    }
  }, [appId]);

  return (
    <Spin spinning={loading} tip="Loading...">
      {kycType === 'CKYC' ? (
        <KycComponent
          kycType={kycType}
          data={ckycData}
          CkycUsers={CkycUsers}
          selectedCkycUser={selectedCkycUser}
          ckycDocsData={ckycDocsData}
          dispatch={dispatch}
        />
      ) : kycType === 'CKYC' ? (
        <KycComponent
          kycType={kycType}
          data={okycData}
          OkycUsers={OkycUsers}
          selectedOkycUser={selectedOkycUser}
          okycDocsData={okycDocsData}
          dispatch={dispatch}
        />
      ) : (
        <KycComponent
          kycType={kycType}
          data={okycData}
          OkycUsers={OkycUsers}
          selectedOkycUser={selectedOkycUser}
          okycDocsData={okycDocsData}
          dispatch={dispatch}
          dkycDocs={dkycDocs}
          dkycDetails={dkycDetails}
        />
      )}
    </Spin>
  );
}

KycDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  kycDetails: makeSelectKycDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = useInjectReducer({ key: 'kycDetails', reducer });
const withSaga = useInjectSaga({ key: 'kycDetails', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(KycDetails);
