import { takeEvery, put, all, takeLatest, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import {
  loadOKYCSuccess,
  loadOKYCError,
  loadOKYCDocsSuccess,
  loadCKYCSuccess,
  loadCKYCDocsSuccess,
  loadDKYCDocsSuccess,
  loadDKYCDocsError,
  loadDKYCDetails,
} from './actions';
import _ from 'lodash';

function* fetchOkycDetailsSaga({ users }) {
  const result = [];
  const okycDocs = [];
  if (users.length > 0) {
    yield all(
      users.map(function*(item) {
        yield* getOkycData(item, result);
      }),
    );
  }
  debugger;
  if (result.length > 0) {
    yield all(
      result.map(function*(item) {
        yield* fetchOkycDocs(item, okycDocs);
      }),
    );
  }
  const okycData =
    result.length > 0 ? _.mapValues(_.groupBy(result, 'cuid')) : {};
  const okycDocsData =
    okycDocs.length > 0
      ? _.mapValues(
          _.groupBy(okycDocs.filter(item => item.status === true), 'cuid'),
        )
      : {};
  yield put(loadOKYCSuccess(okycData));
  yield put(loadOKYCDocsSuccess(okycDocsData));
}

export function* getOkycData(user, result) {
  const apiresult = yield fetch(
    `${env.OKYC_URL}/getById?type=CUID&value=${user.cuid}`,
    {
      headers: env.headers,
      method: 'GET',
    },
  );
  const responseBody = yield apiresult.json();
  if (responseBody.success === true) {
    responseBody.details.forEach(item => {
      result.push(item);
    });
  }
}

function* fetchOkycDocs(item, okycDocs) {
  let responses = item.details.dmsIds
    .filter(item => item.documentId != '' && item.documentId != null)
    .map(item => {
      let promise = request(
        `${env.APP_DOCS_V1_API_URL}/download/${item.documentId}`,
        {
          method: 'GET',
          headers: env.headers,
        },
      );
      return promise;
    });
  let responeBodies = yield Promise.all(responses);
  if(responeBodies && responeBodies.length > 0){
    let finalResponse = responeBodies.map(data => {
      return { ...data, cuid: item.cuid };
    });
    finalResponse.forEach(item => okycDocs.push(item));
  }
}

function* fetchCkycDetailsSaga({ users }) {
  const result = [];
  const ckycDocs = [];
  if (users.length > 0) {
    yield all(
      users.map(function*(item) {
        yield* getCkycData(item, result);
      }),
    );
  }
  debugger;
  if (result.length > 0) {
    yield all(
      result.map(function*(item) {
        yield* fetchCkycDocs(item, ckycDocs);
      }),
    );
  }
  const ckycData =
    result.length > 0 ? _.mapValues(_.groupBy(result, 'kycRefId')) : {};
  const ckycDocsData =
    ckycDocs.length > 0
      ? _.mapValues(
          _.groupBy(ckycDocs.filter(item => item.status === true), 'kycRefId'),
        )
      : {};
  yield put(loadCKYCSuccess(ckycData));
  yield put(loadCKYCDocsSuccess(ckycDocsData));
}

export function* getCkycData(user, result) {
  const apiresult = yield fetch(
    `${env.CKYC_URL}/find?type=CKYCNUMBER&value=${
      user.userIdentities && user.userIdentities.ckycNumber
        ? user.userIdentities.ckycNumber
        : '0'
    }`,
    {
      headers: env.headers,
      method: 'GET',
    },
  );
  const responseBody = yield apiresult.json();
  if (responseBody && responseBody.length > 0) {
    responseBody.forEach(item => {
      result.push(item);
    });
  }
}

function* fetchCkycDocs(item, ckycDocs) {
  let responses = item.kycDetails.details
    .filter(item => item.xmlDocumentId != '' && item.xmlDocumentId != null)
    .map(item => {
      let promise = request(
        `${env.APP_DOCS_V1_API_URL}/download/${item.xmlDocumentId}`,
        {
          method: 'GET',
          headers: env.headers,
        },
      );
      return promise;
    });
  let responeBodies = yield Promise.all(responses);
  if(responeBodies && responeBodies.length > 0){
    responeBodies[0].kycRefId = item.kycRefId;
    ckycDocs.push(responeBodies[0]);
  }
}

export function* fetchDkycDetailsSaga({ appId }) {
  const reqUrl = `${env.KYC_LEAD_API_URL}/search`;
  let data = {
    // id: '50010054',
    id: appId,
    isKycDocsRequired: true,
    isAppDocsRequired: true,
  };
  const options = {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify(data),
  };
  try {
    const response = yield call(request, reqUrl, options);
    debugger;
    let responeBodiesKycDocs = [];
    let responeBodiesAppDocs = [];
    if (response.status && response.data.length > 0) {
      yield put(loadDKYCDetails(response.data[0]));
      if (response.data[0].kycDocs && response.data[0].kycDocs.length > 0) {
        responeBodiesKycDocs = response.data[0].kycDocs;        
      }
      if (response.data[0].appDocs && response.data[0].appDocs.length > 0) {
        responeBodiesAppDocs = response.data[0].appDocs;  
      }
      if(responeBodiesKycDocs.length > 0 || responeBodiesAppDocs.length > 0){
        let concatedArray = responeBodiesKycDocs.concat(responeBodiesAppDocs);
        yield put(loadDKYCDocsSuccess(concatedArray));
      } else {
        yield put(loadDKYCDocsError());
      }
    } else {
      yield put(loadDKYCDocsError());
    }
  } catch (error) {
    debugger;
    yield put(loadDKYCDocsError());
  }
}

export default function* kycDetailsSaga() {
  yield takeLatest(actions.LOAD_OKYC_DETAILS, fetchOkycDetailsSaga);
  yield takeLatest(actions.LOAD_CKYC_DETAILS, fetchCkycDetailsSaga);
  yield takeLatest(actions.LOAD_DKYC_DETAILS, fetchDkycDetailsSaga);
}
