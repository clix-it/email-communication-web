import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the kycDetails state domain
 */

const selectKycDetailsDomain = state => state.kycDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by KycDetails
 */

const makeSelectKycDetails = () =>
  createSelector(
    selectKycDetailsDomain,
    substate => substate,
  );

export { makeSelectKycDetails };
