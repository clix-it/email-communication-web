/**
 *
 * Asynchronously loads the component for KycDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
