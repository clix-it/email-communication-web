import * as actions from './constants';
export function loadOkycDetails(users) {
  return {
    type: actions.LOAD_OKYC_DETAILS,
    users,
  };
}

export function loadOKYCSuccess(okycdata) {
  return {
    type: actions.LOAD_OKYC_DETAILS_SUCCESS,
    okycdata,
  };
}

export function loadOKYCError(errorMessage) {
  return {
    type: actions.LOAD_OKYC_DETAILS_ERROR,
    errorMessage
  };
}

export function setSelectedOkycUser(cuid) {
  return {
    type: actions.SET_OKYC_USER,
    cuid
  };
}

export function loadOKYCDocsSuccess(data) {
  return {
    type: actions.FETCH_OKYC_DOCS_SUCCESS,
    data,
  };
}

// CKYC actions
export function loadCkycDetails(users) {
  return {
    type: actions.LOAD_CKYC_DETAILS,
    users,
  };
}

export function loadCKYCSuccess(ckycdata) {
  return {
    type: actions.LOAD_CKYC_DETAILS_SUCCESS,
    ckycdata,
  };
}

export function loadCKYCError(errorMessage) {
  return {
    type: actions.LOAD_CKYC_DETAILS_ERROR,
    errorMessage
  };
}

export function setSelectedCkycUser(cuid) {
  return {
    type: actions.SET_CKYC_USER,
    cuid
  };
}

export function loadCKYCDocsSuccess(data) {
  return {
    type: actions.FETCH_CKYC_DOCS_SUCCESS,
    data,
  };
}

//DKYC actions
export function loadDKYCDocs(appId) {
  return {
    type: actions.LOAD_DKYC_DETAILS,
    appId,
  };
}

export function loadDKYCDocsSuccess(dkycDocs) {
  return {
    type: actions.LOAD_DKYC_DETAILS_SUCCESS,
    dkycDocs,
  };
}

export function loadDKYCDetails(data) {
  return {
    type: actions.DKYC_DETAILS,
    data,
  };
}

export function loadDKYCDocsError() {
  return {
    type: actions.LOAD_DKYC_DETAILS_ERROR,
  };
}

export function navigateToDocumentsTab(flag) {
  return {
    type: actions.NAVIGATE_TO_DOCUMENTS,
    flag,
  };
}