/**
 * Hunter O/P Reducer
 */
import * as actions from './constants';
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';

export const initialState = {
  loading: false,
  okycData: {},
  ckycData: {},
  dkycDocs: [],
  error: {},
  OkycUsers: [],
  CkycUsers: [],
  selectedOkycUser: '',
  selectedCkycUser: '',
  okycDocsData: {},
  ckycDocsData: {},
  dkycDetails: {},
  navigateToDocumentTab: false,
};

const kycDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.LOAD_OKYC_DETAILS:
        draft.loading = true;
        break;
      case actions.LOAD_CKYC_DETAILS:
        draft.loading = true;
        break;
      case actions.LOAD_DKYC_DETAILS:
        draft.loading = true;
        break;        
      case actions.LOAD_OKYC_DETAILS_SUCCESS:
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.okycData = action.okycdata;
        draft.OkycUsers = Object.keys(action.okycdata) || [];
        draft.selectedOkycUser =
          Object.keys(action.okycdata).length > 0
            ? Object.keys(action.okycdata)[0]
            : '';
        break;
      case actions.LOAD_CKYC_DETAILS_SUCCESS:
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.ckycData = action.ckycdata;
        draft.CkycUsers = Object.keys(action.ckycdata) || [];
        draft.selectedCkycUser =
          Object.keys(action.ckycdata).length > 0
            ? Object.keys(action.ckycdata)[0]
            : '';
        break;
      case actions.LOAD_DKYC_DETAILS_SUCCESS:
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.dkycDocs = action.dkycDocs;
        break;
      case actions.DKYC_DETAILS:
        draft.dkycDetails = action.data;
        break;
      case actions.LOAD_DKYC_DETAILS_ERROR:
        draft.loading = false;
        draft.dkycDocs = [];
        break;
      case actions.LOAD_OKYC_DETAILS_ERROR: {
        draft.loading = false;
        draft.okycData = {};
        draft.OkycUsers = [];
        draft.selectedOkycUser = '';
        draft.okycDocsData = {};
        draft.error = {
          status: true,
          message: action.errorMessage,
        };
        break;
      }
      case actions.SET_OKYC_USER: {
        draft.selectedOkycUser = action.cuid;
        break;
      }
      case actions.SET_CKYC_USER: {
        draft.selectedCkycUser = action.cuid;
        break;
      }
      case actions.FETCH_OKYC_DOCS_SUCCESS: {
        draft.okycDocsData = action.data;
        break;
      }
      case actions.FETCH_CKYC_DOCS_SUCCESS: {
        draft.ckycDocsData = action.data;
        break;
      }
      case actions.NAVIGATE_TO_DOCUMENTS: {
        draft.navigateToDocumentTab = action.flag;
        break;
      }
      case LOCATION_CHANGE:
        draft.loading = false;
        draft.okycData = {};
        draft.ckycData = {};
        draft.OkycUsers = [];
        draft.CkycUsers = [];
        draft.selectedOkycUser = '';
        draft.selectedCkycUser = '';
        draft.okycDocsData = {};
        draft.ckycDocsData = {};
        draft.dkycDocs = [];
        draft.dkycDetails = {};
        draft.navigateToDocumentTab = false;
        break;
      default: {
        return state;
      }
    }
  });

export default kycDetailsReducer;
