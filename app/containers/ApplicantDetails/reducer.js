/**
 * Reducer
 */

import _ from 'lodash';
import produce from 'immer';
import { formatDate } from 'utils/helpers';
import {
  FETCH_APPLICANT_SUCCESS,
  FETCH_APPLICANT,
} from 'containers/ApplicantPage/constants';
import { UPDATE_FORECLOSURE_AMOUNT_SUCCESS } from 'containers/UpdateForeclosureAmont/constants';
import {
  LOAD_ENTITY_DETAILS,
  LOAD_APPLICANT_DETAILS_INIT,
  LOAD_ENTITY_DETAILS_SUCCESS,
  LOAD_ENTITY_DETAILS_ERROR,
  UPDATE_APPLICANT_DETAILS_INIT,
  USER_DETAILS_SUCCESS,
  LOAD_UPDATE_APP_SUCCESS,
  UPDATE_USER_SUCCESS,
  CLEAR_MESSAGE,
  LOAD_INDUSTRY_SUCCESS,
  REMOVE_APPLICANT,
  ADD_APPLICANT,
  REMOVE_GUARANTOR,
  ADD_GUARANTOR,
  SET_CURRENT_APPLICANT,
  SET_FORM_VALIDATION_ERROR,
  IS_DIRTY,
  UNLINK_APPLICANT,
  UNLINK_GUARANTOR,
  LOAD_APPLICANT_DETAILS_END,
  CLEAR_STATE,
  SET_CURRENT_ENTITY,
  ADD_APPLICANT_ENTITY,
  FETCH_PRIORITY_SUCCESS,
  FETCH_PRIORITY_ERROR,
  FETCH_RISK_LEVEL_SUCCESS,
  FETCH_RISK_LEVEL_ERROR,
  UPDATE_USERS,
  SET_CURRENT_GUARANTOR,
  FETCH_MASTER_DATA_GENERIC_SUCCESS,
  FETCH_MASTER_DATA_GENERIC_ERROR,
  FETCH_CATEGORY_SUCCESS,
  FETCH_CATEGORY_ERROR,
  LOAD_DELPHI_OUTPUT_SUCCESS,
  LOAD_DELPHI_OUTPUT_ERROR,
} from './constants';
import {
  START_PERFIOS,
  GET_PERFIOS_REPORT,
  START_PERFIOS_ERROR,
  START_PERFIOS_SUCCESS,
  GET_PERFIOS_REPORT_ERROR,
  GET_PERFIOS_REPORT_SUCCESS,
} from '../FinancialUpload/constants';

const newGuarantorDataBluePrint = {
  id: '',
  localities: [],
  addressArray: ['', '', '', '', ''],
  contactibilities: [
    {
      addressLine1: '',
      addressLine2: '',
      addressLine3: '',
      locality: '',
      city: '',
      id: '',
      state: '',
    },
  ],
  userIdentities: {
    id: '',
    pan: '',
    aadhar: '',
    ckycNumber: '',
    drivingLicense: '',
    passport: '',
    voterId: '',
  },
  dateOfBirthIncorporation: '',
  firstName: '',
  lastName: '',
  preferredEmail: '',
  preferredPhone: '',
  userDetails: {
    fatherName: '',
    maritalStatus: '',
    motherName: '',
  },
  appLMS: {
    role: 'GUARANTOR',
  },
  entityOfficers: {
    id: '',
    designation: '',
  },
  userEducations: [],
  userEmployments: [],
  delphiData: {},
  type: 'INDIVIDUAL',
  userLinked: true,
};

const newApplicantDataBluePrint = {
  id: '',
  localities: [],
  addressArray: ['', '', '', '', ''],
  contactibilities: [
    {
      addressLine1: '',
      addressLine2: '',
      addressLine3: '',
      locality: '',
      city: '',
      id: '',
      state: '',
    },
  ],
  userIdentities: {
    id: '',
    pan: '',
    aadhar: '',
    ckycNumber: '',
    drivingLicense: '',
    passport: '',
    voterId: '',
  },
  dateOfBirthIncorporation: '',
  firstName: '',
  lastName: '',
  preferredEmail: '',
  preferredPhone: '',
  userDetails: {
    fatherName: '',
    maritalStatus: '',
    motherName: '',
  },
  entityOfficers: {
    id: '',
    designation: '',
  },
  userEducations: [],
  userEmployments: [],

  type: 'INDIVIDUAL',
  userLinked: true,
};

const newApplicantEntityDataBluePrint = {
  id: '',
  localities: [],
  addressArray: ['', '', '', '', ''],
  contactibilities: [
    {
      addressLine1: '',
      addressLine2: '',
      addressLine3: '',
      locality: '',
      city: '',
      id: '',
      state: '',
      phoneNumber: '',
      email: '',
      contactType: '',
    },
  ],
  userIdentities: {
    id: '',
    pan: '',
    aadhar: '',
    ckycNumber: '',
    drivingLicense: '',
    passport: '',
    voterId: '',
  },
  appLMS: {
    role: 'Co-Applicant',
  },
  corporateStructure: '',
  dateOfBirthIncorporation: '',
  registeredName: '',
  firstName: '',
  lastName: '',
  preferredEmail: '',
  preferredPhone: '',
  userDetails: {
    id: '',
    fatherName: '',
    maritalStatus: '',
    motherName: '',
  },
  entityOfficers: {
    id: '',
    designation: '',
  },
  userEducations: [],
  userEmployments: [],
  userLinked: true,
  type: 'COMPANY',
};

export const initialState = {
  loading: false,
  unlinkedUsers: [],
  updateLoading: false,
  entity: {},
  industry: {},
  dirty: false,
  gstArray: [],
  error: {},
  nsdlResponse: [],
  nsdlError: false,
  cityData: {},
  citiesByPincodes: [],
  success: {},
  cibilDocUploaded: false,
  delphiGenerated: false,
  userDetails: {},
  formValidationErrors: {},
  masterPriorityData: [],
  masterRiskData: [],
  masterCategoryData: [],
  masterData: {},
};

const applicantDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOAD_APPLICANT_DETAILS_INIT:
      case LOAD_ENTITY_DETAILS:
      case START_PERFIOS:
      case FETCH_APPLICANT:
      case GET_PERFIOS_REPORT:
      case UPDATE_USERS:
        draft.loading = true;
        break;
      case LOAD_APPLICANT_DETAILS_END:
        draft.loading = false;
        break;

      case UPDATE_APPLICANT_DETAILS_INIT:
        return { ...state, updateLoading: true };
      case UPDATE_FORECLOSURE_AMOUNT_SUCCESS:
      case LOAD_ENTITY_DETAILS_SUCCESS:
        // draft.formValidationErrors = {};
        draft.unlinkedUsers = [];
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.entity = action.payload.entity;
        draft.entity = {
          ...action.payload.entity,
          entities: _.map(
            _.filter(action.payload.entity.users, {
              type: 'COMPANY',
            }),
            user => ({
              ...user,
              entityOfficers: _.get(
                _.orderBy(
                  _.filter(_.get(draft.applicatData, 'entityOfficers') || [], {
                    belongsTo: user.cuid,
                  }),
                  'id',
                  'desc',
                ),
                '[0]',
                {},
              ),
            }),
          ),
          guarantors: _.map(
            _.filter(
              action.payload.entity.users,
              user =>
                _.get(user, 'appLMS.role') === 'GUARANTOR' ||
                _.get(user, 'appLMS.role') === 'PROMOTER',
            ),
            user => ({
              ...user,
              entityOfficers: _.orderBy(
                _.filter(_.get(draft.applicatData, 'entityOfficers') || [], {
                  belongsTo: user.cuid,
                }),
                'id',
                'desc',
              )[0],
              dateOfBirthIncorporation: formatDate(
                user.dateOfBirthIncorporation,
              ),
            }),
          ),
          applicants: _.map(
            _.filter(
              action.payload.entity.users,
              user =>
                user.type === 'INDIVIDUAL' &&
                _.get(user, 'appLMS.role') != 'GUARANTOR' &&
                _.get(user, 'appLMS.role') != 'PROMOTER',
            ),
            user => ({
              ...user,
              entityOfficers: _.orderBy(
                _.filter(_.get(draft.applicatData, 'entityOfficers') || [], {
                  belongsTo: user.cuid,
                }),
                'id',
                'desc',
              )[0],
              dateOfBirthIncorporation: formatDate(
                user.dateOfBirthIncorporation,
              ),
            }),
          ),
          entityCuid: _.find(
            action.payload.entity.users,
            user =>
              (_.get(user, 'appLMS.role') === 'Applicant' ||
                !_.get(user, 'appLMS.role')) &&
              user.type === 'COMPANY',
          )
            ? _.find(
              action.payload.entity.users,
              user =>
                (_.get(user, 'appLMS.role') === 'Applicant' ||
                    !_.get(user, 'appLMS.role')) &&
                  user.type === 'COMPANY',
            ).cuid
            : '',
        };
        draft.corpInsurances = action.payload.entity.users
            ? action.payload.entity.users.filter(
              applicant =>
                applicant.cropInsurance && applicant.cropInsurance.id,
            )
          : [];
        draft.corpInsurances = draft.corpInsurances
          ? draft.corpInsurances.map(applicant => ({
            ...applicant.cropInsurance,
            cuid: applicant.cuid,
          }))
          : [];
        draft.selectedApplicant = draft.selectedApplicant
          ? _.find(draft.entity.applicants, {
            id: draft.selectedApplicant.id,
          })
          : {};
        draft.selectedGuarantor = draft.selectedGuarantor
          ? _.find(draft.entity.guarantors, {
            id: draft.selectedGuarantor.id,
          })
          : {};
        draft.selectedApplicant =
          draft.selectedApplicant && draft.selectedApplicant.id
            ? draft.selectedApplicant
            : draft.entity.applicants[0];
        draft.selectedGuarantor =
          draft.selectedGuarantor && draft.selectedGuarantor.id
            ? draft.selectedGuarantor
            : draft.entity.guarantors[0];
        draft.selectedEntity = draft.selectedEntity
          ? _.find(draft.entity.entities, {
            id: draft.selectedEntity.id,
          })
          : _.find(action.payload.entity.users, {
            type: 'COMPANY',
            appLMS: { role: 'Applicant' },
          });
        draft.selectedEntity =
          draft.selectedEntity && draft.selectedEntity.id
            ? draft.selectedEntity
            : _.find(
              draft.entity.entities,
              user =>
                (_.get(user, 'appLMS.role') === 'Applicant' ||
                    !_.get(user, 'appLMS.role')) &&
                  user.type === 'COMPANY',
            );
        break;

      case FETCH_APPLICANT_SUCCESS:
        draft.applicatData = action.response;
        draft.entity = {
          ...{
            ...draft.entity,
            entities: _.map(
              _.filter(draft.entity.entities, {
                type: 'COMPANY',
              }),
              user => ({
                ...user,
                entityOfficers: _.orderBy(
                  _.filter(_.get(draft.applicatData, 'entityOfficers') || [], {
                    belongsTo: user.cuid,
                  }),
                  'id',
                  'desc',
                )[0],
              }),
            ),
            applicants: _.map(draft.entity.applicants, user => ({
              ...user,
              entityOfficers: _.orderBy(
                _.filter(_.get(draft.applicatData, 'entityOfficers') || [], {
                  belongsTo: user.cuid,
                }),
                'id',
                'desc',
              )[0],
            })),
            guarantors: _.map(draft.entity.guarantors, user => ({
              ...user,
              entityOfficers: _.orderBy(
                _.filter(_.get(draft.applicatData, 'entityOfficers') || [], {
                  belongsTo: user.cuid,
                }),
                'id',
                'desc',
              )[0],
            })),
            users: _.map(draft.entity.users, user => ({
              ...user,
              entityOfficers: _.filter(action.response.entityOfficers, {
                belongsTo: user.cuid,
              }),
            })),
          },
        };
        draft.selectedApplicant = draft.selectedApplicant
          ? _.find(draft.entity.applicants, {
            id: draft.selectedApplicant.id,
          })
          : draft.entity.applicants[0];
        draft.selectedGuarantor = draft.selectedGuarantor
          ? _.find(draft.entity.guarantors, {
            id: draft.selectedGuarantor.id,
          })
          : draft.entity.guarantors[0];

        break;

      case USER_DETAILS_SUCCESS: {
        return {
          ...state,
          // loading: false,
          error: { status: false, message: '' },
          userDetails: action.payload.userDetails,
          // updateLoading: false,
        };
      }

      case LOAD_ENTITY_DETAILS_ERROR:
        draft.loading = false;
        draft.updateLoading = false;
        draft.error = {
          status: true,
          message: action.payload.errorMessage,
          severity: action.payload.severity,
        };
        // draft.entity = action.payload.entity;
        break;

      case LOAD_INDUSTRY_SUCCESS: {
        return {
          ...state,
          // loading: false,
          error: { status: false, message: '' },
          industry: action.payload.industry,
        };
      }

      case LOAD_UPDATE_APP_SUCCESS: {
        return {
          ...state,
          updateLoading: false,
          loading: false,
          error: { status: false, message: '' },
          success: { status: true },
          cibilDocUploaded: action.payload.success.cibilDocUploaded,
          delphiGenerated: action.payload.success.delphiGenerated,
        };
      }
      case UPDATE_USER_SUCCESS: {
        return {
          ...state,
          loading: false,
          error: { status: false, message: '' },
          success: { status: true, message: action.payload.success },
          // updateLoading: false,
        };
      }
      case CLEAR_MESSAGE: {
        return {
          ...state,
          loading: false,
          updateLoading: false,
          error: { status: false, message: '' },
          success: {},
          cibilDocUploaded: false,
          delphiGenerated: false,
        };
      }
      case ADD_GUARANTOR:
        const newGuarantor = {
          ...newGuarantorDataBluePrint,
          id: `NEW-${draft.entity.guarantors.length}`,
          entityOfficers: {
            ...newGuarantorDataBluePrint.entityOfficers,
            belongsTo: draft.entityCuid,
            designation: '',
          },
        };
        draft.entity = {
          ...draft.entity,
          guarantors: [...draft.entity.guarantors, newGuarantor],
        };
        draft.selectedGuarantor = newGuarantor;
        break;
      case ADD_APPLICANT:
        const newApplicant = {
          ...newApplicantDataBluePrint,
          id: `NEW-${draft.entity.applicants.length}`,
          entityOfficers: {
            ...newApplicantDataBluePrint.entityOfficers,
            belongsTo: draft.entityCuid,
            designation: '',
          },
        };
        draft.entity = {
          ...draft.entity,
          applicants: [...draft.entity.applicants, newApplicant],
        };
        draft.selectedApplicant = newApplicant;
        break;

      case ADD_APPLICANT_ENTITY:
        {
          const newApplicantEntity = {
            ...newApplicantEntityDataBluePrint,
            id: `NEW-${draft.entity.entities.length}`,
            entityOfficers: {
              ...newApplicantEntityDataBluePrint.entityOfficers,
              belongsTo: draft.entityCuid,
              designation: '',
            },
          };
          draft.entity = {
            ...draft.entity,
            entities: [...draft.entity.entities, newApplicantEntity],
          };
          draft.selectedEntity = newApplicantEntity;
        }
        break;

      case REMOVE_APPLICANT:
        if (String(action.id).includes('NEW')) {
          draft.entity = {
            ...draft.entity,
            applicants: _.reject(draft.entity.applicants, {
              id: action.id,
            }),
          };
          draft.selectedApplicant = draft.entity.applicants[0];
          return;
        }

        if (_.find(draft.unlinkedUsers || [], { id: action.id })) {
          draft.unlinkedUsers = [
            ..._.reject(draft.unlinkedUsers || [], { id: action.id }),
          ];
          draft.selectedApplicant = {
            ...draft.selectedApplicant,
            userLinked: action.link,
          };
          return;
        }
        draft.unlinkedUsers = _.uniqBy(
          _.compact([
            ..._.reject(draft.unlinkedUsers || [], { id: action.id }),
            {
              ..._.find(draft.entity.users, { id: action.id }),
              userLinked: action.link,
            },
          ]),
          'id',
        );
        draft.entity = {
          ...draft.entity,
          applicants: _.filter(
            _.map(draft.entity.applicants, user =>
              user.id == action.id
                ? { ...user, userLinked: action.link }
                : user,
            ),
            function(o) {
              return !String(o.id).includes('NEW');
            },
          ),
        };
        draft.selectedApplicant = {
          ...draft.selectedApplicant,
          userLinked: action.link,
        };
        break;
      case IS_DIRTY:
        draft.dirty = { ...draft.dirty, ...action.dirty.val };
        break;
      case UNLINK_APPLICANT:
        if (String(action.payload.id).includes('NEW')) {
          draft.entity = {
            ...draft.entity,
            applicants: _.reject(draft.entity.applicants, {
              id: action.payload.id,
            }),
          };
          draft.selectedApplicant = draft.entity.applicants[0];
        }
        break;
      case UNLINK_GUARANTOR:
        if (String(action.payload.id).includes('NEW')) {
          draft.entity = {
            ...draft.entity,
            guarantors: _.reject(draft.entity.guarantors, {
              id: action.payload.id,
            }),
          };
          draft.selectedGuarantor = draft.entity.guarantors[0];
        }
        break;
      case SET_CURRENT_APPLICANT:
        draft.selectedApplicant = action.applicant;
        break;
      case SET_CURRENT_GUARANTOR:
        draft.selectedGuarantor = action.applicant;
        break;
      case SET_CURRENT_ENTITY: {
        draft.selectedEntity = action.entity;
        break;
      }
      case START_PERFIOS_ERROR:
      case START_PERFIOS_SUCCESS:
      case GET_PERFIOS_REPORT_ERROR:
      case GET_PERFIOS_REPORT_SUCCESS:
        draft.loading = false;
        break;
      case SET_FORM_VALIDATION_ERROR:
        if (action.payload.formName === 'all') {
          draft.formValidationErrors = {};
        } else {
          draft.formValidationErrors = {
            ...draft.formValidationErrors,
            [action.payload.formName]: action.payload.isValid,
          };
        }
        break;
      case CLEAR_STATE:
        return initialState;
      case FETCH_PRIORITY_SUCCESS:
        const tempArr = action.data.map(item => item.cmCode);
        draft.masterPriorityData = tempArr;
        break;
      case FETCH_PRIORITY_ERROR:
        draft.masterPriorityData = [];
        break;
      case FETCH_CATEGORY_SUCCESS:
        const tempArr2 = action.data.map(item => item.cmCode);
        draft.masterCategoryData = tempArr2;
        break;
      case FETCH_CATEGORY_ERROR:
        draft.masterCategoryData = [];
        break;
      case FETCH_RISK_LEVEL_SUCCESS:
        const tempArr1 = action.data.map(item => item.cmCode);
        draft.masterRiskData = tempArr1;
        break;
      case FETCH_RISK_LEVEL_ERROR:
        draft.masterRiskData = [];
        break;
      case FETCH_MASTER_DATA_GENERIC_SUCCESS: {
        const temp = [];
        action.data.forEach(item => {
          temp.push({ code: item.cmCode, value: item.cmValue });
        });
        draft.masterData = {
          ...draft.masterData,
          [action.masterType]: temp,
        };
        break;
      }
      case FETCH_MASTER_DATA_GENERIC_ERROR:
        draft.masterFreeFieldsData = {};
        draft.masterData = {
          ...draft.masterData,
          [action.masterType]: [],
        };
        break;

      case LOAD_DELPHI_OUTPUT_SUCCESS:
        draft.delphiData = action.payload.data;
        break;

      case LOAD_DELPHI_OUTPUT_ERROR:
        draft.delphiData = {};
        break;

      default: {
        return state;
      }
    }
  });

export default applicantDetailsReducer;
