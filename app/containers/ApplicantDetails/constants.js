export const UPDATE_CO_APPLICANTS = 'UPDATE_CO_APPLICANTS';
export const UPDATE_CO_APPLICANTS_SUCCESS = 'UPDATE_CO_APPLICANTS_SUCCESS';
export const UPDATE_CO_APPLICANTS_ERROR = 'UPDATE_CO_APPLICANTS_ERROR';
export const LOAD_ENTITY_DETAILS = 'LOAD_ENTITY_DETAILS';
export const LOAD_ENTITY_DETAILS_SUCCESS = 'LOAD_ENTITY_DETAILS_SUCCESS';
export const LOAD_ENTITY_DETAILS_ERROR = 'LOAD_ENTITY_DETAILS_ERROR';
export const LOAD_APPLICANT_DETAILS_INIT = 'LOAD_APPLICANT_DETAILS_INIT';
export const UPDATE_APPLICANT_DETAILS_INIT = 'UPDATE_APPLICANT_DETAILS_INIT';
export const CLEAR_STATE = 'applicantdetails/CLEAR_STATE';

export const LOAD_INDUSTRY_SUCCESS = 'LOAD_INDUSTRY_SUCCESS';
export const SET_CURRENT_APPLICANT = 'SET_CURRENT_APPLICANT';
export const SET_CURRENT_GUARANTOR = 'SET_CURRENT_GUARANTOR';

export const LOAD_CITY_BY_PINCODE = 'LOAD_CITY_BY_PINCODE';
export const LOAD_CITY_BY_PINCODE_SUCCESS = 'LOAD_CITY_BY_PINCODE_SUCCESS';
export const LOAD_CITIES_BY_PINCODES_SUCCESS =
  'LOAD_CITIES_BY_PINCODES_SUCCESS';

export const POPULATE_INDUSTRY_SUBTYPE = 'POPULATE_INDUSTRY_SUBTYPE';

export const UPDATE_ENTITY_DETAILS = 'UPDATE_ENTITY_DETAILS';
export const UPDATE_APPLICANT_DETAILS = 'UPDATE_APPLICANT_DETAILS';

export const LOAD_UPDATE_APP_SUCCESS = 'LOAD_UPDATE_APP_SUCCESS';

export const UPDATE_USER_REQUEST = 'UPDATE_USER_REQUEST';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';
export const IS_DIRTY = 'IS_DIRTY';
export const CLEAR_MESSAGE = 'CLEAR_MESSAGE';
export const GENERATE_CIBIL_REPORT = 'GENERATE_CIBIL_REPORT';
export const GENERATE_DELPHI_REPORT = 'GENERATE_DELPHI_REPORT';
export const LOAD_PAN_NSDL = 'LOAD_PAN_NSDL';

export const LOAD_GST_BY_PAN = 'LOAD_GST_BY_PAN';
export const LOAD_GST_BY_PAN_SUCCESS = 'LOAD_GST_BY_PAN_SUCCESS';

export const CHECK_NSDL = 'CHECK_NSDL';
export const CHECK_NSDL_SUCCESS = 'CHECK_NSDL_SUCCESS';
export const CHECK_NSDL_ERROR = 'CHECK_NSDL_ERROR';
export const USER_DETAILS_SUCCESS = 'USER_DETAILS_SUCCESS';

export const START_PERFIOS = 'START_PERFIOS';
export const START_PERFIOS_SUCCESS = 'START_PERFIOS_SUCCESS';
export const START_PERFIOS_ERROR = 'START_PERFIOS_ERROR';

export const GET_PERFIOS_REPORT = 'GET_PERFIOS_REPORT';
export const GET_PERFIOS_REPORT_SUCCESS = 'GET_PERFIOS_REPORT_SUCCESS';
export const GET_PERFIOS_REPORT_ERROR = 'GET_PERFIOS_REPORT_ERROR';

export const ADD_APPLICANT = 'app/ApplicantDetails/ADD_APPLICANT';
export const REMOVE_APPLICANT = 'app/ApplicantDetails/REMOVE_APPLICANT';

export const ADD_GUARANTOR = 'app/ApplicantDetails/ADD_GUARANTOR';
export const REMOVE_GUARANTOR = 'app/ApplicantDetails/REMOVE_GUARANTOR';

export const SET_FORM_VALIDATION_ERROR = 'SET_FORM_VALIDATION_ERROR';
export const UNLINK_APPLICANT = 'UNLINK_APPLICANT';
export const UNLINK_GUARANTOR = 'UNLINK_GUARANTOR';
export const LOAD_APPLICANT_DETAILS_END = 'LOAD_APPLICANT_DETAILS_END';
export const SET_CURRENT_ENTITY = 'SET_CURRENT_ENTITY';
export const UPDATE_USERS = 'UPDATE_USERS';
export const ADD_APPLICANT_ENTITY = 'ADD_APPLICANT_ENTITY';

export const FETCH_PRIORITY = 'FETCH_PRIORITY';
export const FETCH_PRIORITY_SUCCESS = 'FETCH_PRIORITY_SUCCESS';
export const FETCH_PRIORITY_ERROR = 'FETCH_PRIORITY_ERROR';

export const FETCH_CATEGORY = 'FETCH_CATEGORY';
export const FETCH_CATEGORY_SUCCESS = 'FETCH_CATEGORY_SUCCESS';
export const FETCH_CATEGORY_ERROR = 'FETCH_CATEGORY_ERROR';

export const FETCH_RISK_LEVEL = 'FETCH_RISK_LEVEL';
export const FETCH_RISK_LEVEL_SUCCESS = 'FETCH_RISK_LEVEL_SUCCESS';
export const FETCH_RISK_LEVEL_ERROR = 'FETCH_RISK_LEVEL_ERROR';

export const FETCH_MASTER_DATA_GENERIC = 'FETCH_MASTER_DATA_GENERIC';
export const FETCH_MASTER_DATA_GENERIC_SUCCESS =
  'FETCH_MASTER_DATA_GENERIC_SUCCESS';
export const FETCH_MASTER_DATA_GENERIC_ERROR =
  'FETCH_MASTER_DATA_GENERIC_ERROR';
export const CALL_COMMERCIAL_BUREAU = 'CALL_COMMERCIAL_BUREAU';

export const LOAD_DELPHI_OUTPUT = 'LOAD_DELPHI_OUTPUT';
export const LOAD_DELPHI_OUTPUT_SUCCESS = 'LOAD_DELPHI_OUTPUT_SUCCESS';
export const LOAD_DELPHI_OUTPUT_ERROR = 'LOAD_DELPHI_OUTPUT_ERROR';

export const RELATIONS_WITH_BORROWER = [
  'Proprietor',
  'Director',
  'Partner',
  'Trustee',
  'Shareholder',
  'Property Owner',
  'Secretary',
  'Treasurer',
  'Member',
  'Others',
];

export const GST_RES = {
  statusCode: 101,
  requestId: '3e81f44c-7b20-4e5d-a9d8-2b6c605f1eab',
  result: [
    {
      emailId: '',
      applicationStatus: '',
      mobNum: '',
      registrationName: '',
      gstinRefId: '',
      regType: '',
      authStatus: 'Active',
      tinNumber: '',
      pan: 'AACCD4691B',
      gstinId: '09AACCD4691B1ZS',
    },
  ],
};

export const DL_INDUSTRY = {
  success: true,
  data: {
    key: 'Nature',
    guid: null,
    type: null,
    children: [
      {
        key: 'Service',
        guid: '',
        type: 'Nature',
        children: [
          {
            key: 'Hospitals and Clinics',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Diagnostic Centre Hospitals or Gyms or health centre',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Activities of independent diagnostic/pathological laboratories',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Hospital activities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Activities of operating hospital',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Operates multi-specialty hospital',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Residential care activities for mental retardation, mental health and substance abuse',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
              },
            ],
          },
        ],
        mdmEntityList: null,
      },
    ],
    mdmEntityList: null,
  },
};

export const INDUSTRY_RES = {
  success: true,
  data: {
    key: 'Nature',
    guid: null,
    type: null,
    children: [
      {
        key: 'Service',
        guid: '',
        type: 'Nature',
        children: [
          {
            key: 'Automobile 4W',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Authorised car dealers  servicing  manufacturers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Maintenance and repair of motor vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Maintenance and repair of passenger motor vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Sale of motor vehicle parts and accessoriess',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Sale of motor vehicle parts and accessories',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Maintenance and repair of commercial vehicless',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Maintenance and repair of motor vehicles1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Aviation',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Maintenance and overhauling servicesl ground handling',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Cargo handling incidental to land transport',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Cargo handling incidental to water transport',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Cargo handling incidental to air transport',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Cable TV',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Cable and other pay TV services',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Installation of telecommunications wiring, computer network and cable television wiring, including fiber optic, satellite dishessx',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Cold Storage',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Cold storage chains',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Warehousing of refrigerated (cold storage)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Cold storage facility',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Cold storage facility for potato farmers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Commercial Vehicle',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'LCV  HCV  Commercial Vehicles',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Maintenance and repair of commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Computer and Peripherals',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Office Equipment  Networking',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Installation of telecommunications wiring, computer network and cable television wiring, including fiber optic, satellite dishes.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Construction',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Contractor',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Construction and maintenance of motorways, streets, roads, other vehicular and pedestrian ways, highways, bridges, tunnels and subwayss',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Structurals',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Construction of dams.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of outdoor sports facilities.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Civil construction & engineering servicess',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of bridges.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of roads.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and maintenance of railways and rail-bridges.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of railway infrastructures',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction and maintenance of airfield runways.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction and maintenance of power plants.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of hydroelectric power plants.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of power plants.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of power substations.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of power houses.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction/erection and maintenance of power, telecommunication and transmission lines.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of transmission lines.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction of long distance pipelines or urban pipelines.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of pipelines.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and maintenance of water main and line connection, water reservoirs including irrigation system (canal).',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of irrigation systems.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of water supply line.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of water tanks.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and repair of sewer systems including sewage disposal plants and pumping stations.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of sewage system.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of drainage works.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of utility projects n.e.c..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of utility projects.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and maintenance of industrial facilities such as refineries.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of industrial plants.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction of waterways, harbors and river works, dredging of waterways.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of dams etc..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Construction Equipment',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Prime Movers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Clearing of building sites, earth moving: excavation, landfill, levelling and grading of construction sites, trench digging, rock removal, blasting etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Activities of earth work',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Consumer Durables',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Electronic Equipment',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Renting of tent, furniture, pottery and glass, kitchen and tableware, utensils, household electrical and electronic equipments etc..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Education',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Education',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Education - hotel management & catering',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Higher education not leading to a degree or equivalent',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Sports and recreation education',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Cultural education',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Other educational services n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Educational support services',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Pre-primary education (education preceding the first level)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - pre-primary',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Primary education (education at the first level)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - primary',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'General school education in the first stage of the secondary level (up to X th standard) without any special subject pre-requisite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - school',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'General school education in the second stage of the secondary level (Senior/Higher secondary) giving, in principle, access to higher education',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - junior college',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Special education for handicapped students at first stage or second stage of secondary level',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Technical and vocational education below the level of higher education except for handicapped',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Technical and vocational education for handicapped students below the level of higher education',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Higher education in science, commerce, humanity and fine arts leading to a university degree or equivalent',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - non technical',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Higher education in engineering / other technical courses leading to a university degree or equivalent',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - technical',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Higher education in medical/bio-technology and related courses leading to a university degree or equivalent',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - medical',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - para-medical',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Higher education in management courses leading to a degree or equivalent',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - management',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Higher education in law leading to a degree or equivalent',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - law',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Higher education in other professional/ vocational courses leading to a degree or equivalent',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - fashion designing',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Education - film making',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Engineer',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Engineering',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Civil construction & engineering services.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Entertainment and Leisure',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Recreation and Amusement parks  event management  gyms',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Activities of amusement parks and theme parks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Activities of amusement parks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Financial Services',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Finance relared companies and consultancies  advisory firms',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Activities auxiliary to financial service activities n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Other financial service activities, except insurance and pension funding activities, n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Future Rentals',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'LRD  lease rentals  rental income',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Renting and leasing of other machinery and equipment n.e.c. without operator',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Rental of private cars with driver',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Renting and leasing of motor vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      "Renting of tent, furniture, pottery and glass, kitchen and tableware, utensils, household electrical and electronic equipment's etc.",
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting of jewellery, musical instruments, scenery and costumes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting of textiles, wearing apparel, footwear, sleeping bag, rucksack, household goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Renting of other personal and household goods n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing, without operator, of other machinery and equipment that are generally used as capital goods by industries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of agricultural and forestry machinery and equipment without operator',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of construction and civil-engineering machinery and equipment without operator',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of office machinery and equipment without operator',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of land-transport equipment (other than motor vehicles) without drivers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of water-transport equipment without operator',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of air transport equipment without operator',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Renting of other tangible goods with out operator',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Hospitals and Clinics',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Diagnostic Centre  Hospitals or Gyms or health Centre',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Activities of independent diagnostic/pathological laboratories.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Activities of independent diagnostic/pathological laboratories',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Hospital activities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Activities of operating hospital',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Operates multi-specialty hospital',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Residential care activities for mental retardation, mental health and substance abuse',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Hotel and Restaurants',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Hotels and Restaurants',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Hotels and Motels, inns, resorts providing short term lodging facilities includes accommodation in house boats',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Activity of operating hotel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Restaurants without bars',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Restaurant services',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Cafeterias, fast-food restaurants and other food preparation in market stalls',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Bars and Restaurants with bars',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Restaurant & bar services',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Industrial Equipment or Machinery',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Industrial Machinery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Installation of industrial machinery and equipment.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Installation of industrial machinery and equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Cleaning of industrial machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'IT or Software or ITES or BPO or KPO',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Computer Software and Education and post production animation',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Activities of programming, consultancy etc',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Computer consultancy and computer facilities management activities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'ERP or any type of protecting systems or anti virus',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Writing , modifying, testing of computer program to meet the needs of a particular client excluding web-page designing',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'ITES or Call Centres',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Other information technology and computer service activities n.e.c',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Media or Entertainment TV Broadcasting',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Media  Advertising and Broadcasting  Animation and Post production',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Advertising',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals Iron and Steel',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Iron  Steel and Steel Alloys  Wires and Cables',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Repair and maintenance of metal tanks, reservoirs, containers, steel shipping drums etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Other',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Industry not classified elsewhere',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Industries not classified elsewhere Service Segment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Packaging',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Plastic Packaging Goods',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Packaging activities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Paints',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Paints and Varnishes',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Interior and exterior painting, glazing, plastering and decorating of buildings or civil engineering structures',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Pipes',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Manufacturing and sales of pipes  PVC including dealers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Repair and maintenance of pipes and pipelines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Power',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Transmission line towers and equipment',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Construction/erection and maintenance of power, telecommunication and transmission lines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of transmission lines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Printing and Publishing',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Publishing',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Publishing of books, brochures, leaflets and similar publications, including publishing encyclopedias (including on CD-ROM)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Publishing of atlases, maps and charts',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Publishing of textbooks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Publishing of newspapers.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Publishing of newspapers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Publishing of journals and periodicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'On-line publishing of statistics and other information',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Other publishing activities (including on-line) n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Printing of newspapers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Printing of magazines and other periodicals, books and brochures, maps, atlases, posters etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Printing of postage stams, taxation stamps, cheques and other security papers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Printing of bank notes, currency notes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Printing directly onto textiles, flexographic plastic, glass, metal, wood and ceramics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Other printing activities like screen printing other than textile n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Other service activities related to printing n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Professional Services',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Technical Consultancy and Engg services  IT consulting  salaried employees  doctors  only rental income',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Renting and leasing of other machinery and equipment n.e.c. without operator..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Architectural and engineering activities and related technical consultancy',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Architectural and consultancy services',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Technical testing and analysis',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Computer consultancy and computer facilities management activities.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Activities of programming, consultancy etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Medical practice activities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Activities of nurses, masseurs, physiotherapists or other para-medical practitioners',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Rental of private cars with drivers.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Renting and leasing of motor vehicles.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and leasing of recreational and sports goods.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Renting of video tapes and disks..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      "Renting of tent, furniture, pottery and glass, kitchen and tableware, utensils, household electrical and electronic equipment's etc…",
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Renting of books, journals and magazines.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting of jewellery, musical instruments, scenery and costumes.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting of textiles, wearing apparel, footwear, sleeping bag, rucksack, household goods.x.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting of other personal and household goods n.e.c..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing, without operator, of other machinery and equipment that are generally used as capital goods by industries.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of agricultural and forestry machinery and equipment without operator..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of construction and civil-engineering machinery and equipment without operator..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of office machinery and equipment without operator.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of land-transport equipment (other than motor vehicles) without drivers.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of water-transport equipment without operator.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Renting and operational leasing of air transport equipment without operator.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Renting of other tangible goods with out operator.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key:
                  'Executive search or manpower servicess  hostel management',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Supply of skilled man power',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Human resources provision and management of human resources functions',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'HR and A or Medical Transcriptions',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Human resources provision and management of human resources functions.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Tax and Audit  Architects',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Architectural and engineering activities and related technical consultancys',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Architectural and consultancy servicess',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Real Estate',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Commercial residential industrial buildings',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Construction of sewage system',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of drainage works',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of utility projects n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of utility projects',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and maintenance of industrial facilities such as refineries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of industrial plants',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction of waterways, harbors and river works, dredging of waterways',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of dams etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of dams',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of outdoor sports facilities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Civil construction & engineering services',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of canals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of highways',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Other specialized construction activities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction of buildings carried out on own-account basis or on a fee or contract basis',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of buildings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of commercial buildings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of residential building',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and maintenance of motorways, streets, roads, other vehicular and pedestrian ways, highways, bridges, tunnels and subways',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of bridges',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of roads',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and maintenance of railways and rail-bridges',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of railway infrastructure',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction and maintenance of airfield runways',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction and maintenance of power plants',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of hydroelectric power plants',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of power plants',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of power substations',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of power houses',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction/erection and maintenance of power, telecommunication and transmission lines…',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of transmission linesx',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction of long distance pipelines or urban pipelines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of pipelines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and maintenance of water main and line connection, water reservoirs including irrigation system (canal)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of irrigation systems',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of water supply line',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Construction of water tanks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Construction and repair of sewer systems including sewage disposal plants and pumping stations',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tours and Travels',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Taxi or Car Rental',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Rental of private cars with drivers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Film Industry',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Entertainment and Medic content provider  motion picture production  distribution  exhibition',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Production of motion picture',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Motion picture distribution',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Motion picture or video tape projection in cinemas, in the open air or in other projection facilities',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Mining',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Mining',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Site preparation for mining including overburden removal and other development and preparation of mineral properties and sites except oil and gas sites',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Site preparation for mining',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Telecom and Telecom Products',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Other communication services  telex  wireless  fax  pager  other telephone or communication services  mobile phones retail wholesale seller',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Construction/erection and maintenance of power, telecommunication and transmission lines..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Installation of telecommunications wiring, computer network and cable television wiring, including fiber optic, satellite dishes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
        ],
        mdmEntityList: null,
      },
      {
        key: 'Trading',
        guid: '',
        type: 'Nature',
        children: [
          {
            key: 'Agriculture',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Spices and Grams',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of edible oils, fats, sugar and processed/manufactured spices etc.1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of spices',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of cereals and pulses, tea, coffee, spices and flour2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Dry Fruits',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of cashew kernels',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of dryfruits',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of cashew nuts',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Floriculture',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of flowers and plants',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Seed Related',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of ploughs, manure spreaders, seeders, harvesters, threshers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of oilseed oleaginous fruits',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of cotton seeds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of seeds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Auto Ancilliaries',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Auto Ancilliaries',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of automotive components',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of automotive fuel in specialized stores (includes the activity of petrol filling stations)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Storage Batteries',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of batteries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Valves',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of electronic valves and tubes, semiconductor devices, microchips, integrated circuits and printed circuits',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Automobile 2W',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Two wheeler dealers and manufacturers including scooters bikes etc',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of three wheeler',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of two wheeler',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale or retail sale of parts and accessories of motorcycles, mopeds, scooters and three wheelers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of three wheeler spares',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of two wheeler spares',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of two wheeler spares',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale or retail sale of new motorcycles, mopeds, scooters and three wheelers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of three wheeler',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of two wheeler',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Automobile 4W',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Authorised car dealers servicing manufacturers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Retail of medium commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of passenger motor vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of small commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of heavy commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of light commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of medium commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of passenger motor vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of small commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale and retail sale of used motor vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of used motor vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of heavy commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of light commercial vehicle',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Cement',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Cement and Asbestos Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of cement',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Chemicals',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Chemicals petrochemicals specility chemicals',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of industrial chemical',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of chemicals used in paper industries.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of industrial chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of speciality chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of fertilizers and agrochemical products.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Activities of commission agents dealing in wholesale trade in wood, paper, skin, leather and fur, fuel, petroleum products, chemicals, perfumery and cosmetics, glass, minerals, ores and metals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of petrochemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Coal',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Coal and Lignite Minerals',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of coal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of non-coking coal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Coffee',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Coffee Producers and coffee chains',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Retail sale of cereals and pulses, tea, coffee, spices and flours',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of tea, coffee & cocoa.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of coffee',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Commercial Vehicle',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'LCV HCV Commercial Vehicles',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Retail of medium commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of small commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of heavy commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of light commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of medium commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of small commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of heavy commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail of light commercial vehicles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Consumer Durables',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Electronic Equipment',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      "Wholesale of other electronic equipment's and parts thereof.",
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Consumer Electronic Spares   Components',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      "Wholesale of other electronic equipment's and parts thereof",
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Drug Stores',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Drugs and pharmaceuticals drug proprietaries and druggists sundries',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of pharmaceutical products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of pharmaceutical goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of pharmaceuticals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of bulk drugs',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of pharmaceuticals, medical and orthopedic goods and toilet articles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of pharmaceutical and medical goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Dyes and Pigments',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Dyes and Pigments',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Dyes and Pigments',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Edible Oils',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Edible Oils',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of edible oils, fats, sugar and processed/manufactured spices etc.2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of edible oil',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Electricals',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Electrical good and equipments',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Retail sale of refrigerators, washing machines and other electrical/electronic household goods.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of other electrical equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of electrical machinery, equipment and supplies, n.e.c..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of electrical equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Fertiliser',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Fertilisers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of fertilizers and agrochemical products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of fertilizers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'FMCG',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Wholesales of food products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of food products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Food Processing',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Cocoa Confectionary Dairy packaged food bakery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of raw milk & dairy products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of dairy products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of bakery products, dairy products and eggs',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of tea, coffee & cocoa..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of confectionery, bakery products and beverages other than intoxicants',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of confectionery products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Marine Foods Soya bean products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of meat, fish & eggs.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Gems and Jewellery',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Gems and jewellery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Retail sale of jewellery and imitation jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of gold jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of silver jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of diamond jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of diamond-studded jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of gold jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of precious metals and jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Industrial Equipment or Machinery',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Machine Tools',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of other machinery, equipment and supplies n.e.c. including computer-controlled machine tools and computer-controlled sewing and knitting machines.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Pumps',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of pump sets',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Industrial Machinery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of other agriculture machinery n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of electrical machinery, equipment and supplies, n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of construction and civil engineering machinery and equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of construction and civil machinery and equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of machinery and equipment for the textile, wood and metal industries etc..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of scientific, medical and surgical machinery and equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of other machinery, equipment and supplies n.e.c. including computer-controlled machine tools and computer-controlled sewing and knitting machines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of machinery, equipment and supplies',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of agricultural machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Liquor or Breweries or imfi',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Beer Wine and Distilled Alcoholic Beverages',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Retail sale of alcoholic beverages not consumed on the spot',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of beer',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Medical or Pharma Equipments',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Medical Equipment',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of medical equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals aluminium',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Aluminium and Aluminium Products  manufacturers ',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of aluminium products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals copper',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Copper and Copper Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of copper products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals Iron and Steel',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Iron Steel and Steel Alloys Wires and Cables',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of pig iron',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of sponge iron',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of cast iron',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of iron and steel product',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of iron ore fines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of iron scrap',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of other steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of structured steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel pipes.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of mild-steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel sheet',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel strips/tubes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of cold-rolled steel coils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of hot and cold rolled steel coils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of hot-rolled steel coils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of iron and steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel  billets',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel angles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel beams',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel channels',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel coils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel plates',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel product',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel round bars',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel utensils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel scrap',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of steel sheets',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of iron products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of iron ore',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals others',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'other than iron and steel zince copper aluminium',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of metal and non-metal waste and scrap n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of metal waste',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of other metal scrap',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of metal wastes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of metals and metal ores',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of other metal products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals zinc',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Zinc',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of zinc',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Others ',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Industry not classified elsewhere',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Industries not classified elsewhere Trading Segment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Paints',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Paints and Varnishes',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of paints, varnishes, and lacquers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of paints',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Paper',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Paper and Paper Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of newsprint paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'wholesale of coated paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of kraft paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of specialty paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of uncoated paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of chemicals used in paper industries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of paper in bulk',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of waste paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of wallpaper and floor coverings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of stationery office supplies such as pens, pencils, paper etc…',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of paper and other stationery items; books, magazines and newspapers.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of writing paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Pesticides',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Pesticied',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of pesticides',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Pipes',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Manufacturing and sales of pipes PVC including dealers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of steel pipes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of pipes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Plastics',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Polymers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Del-crdere agent for polymer products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of polymers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key:
                  'Plastic tubes and sheets and other plastic products plastic resins thermoplastics',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of plastic granuels',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of plastic materials in primary forms',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of plastic materials in primary forms.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of polyethylene plastic granules',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Poultry',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Poultry and Meat Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of live animals and poultry',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of straw, fodder & other animal/poultry feed',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of poultry feed',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of meat, fish & eggs',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Retail',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'General Merchandise Stores   Kiryana Stores grocery stores etc',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Retail sale of cereals and pulses, tea, coffee, spices and flour.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Multibrand Stores',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Other retail sale of new goods in specialized stores n.e.c (weapons and ammunition, non food products)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of textiles in specialized stores..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Rubber Natural',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Rubber and Rubber products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of rubber',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Speciality',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Home Appliances   Kitchen Appliances',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of radio, television and other consumer electronics including CD/DVD players and recorders',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of refrigerators, washing machines and other electrical/electronic household goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of household appliances',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Books office supplies and stationery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of paper and other stationery items; books, magazines and newspapers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of stationery items',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Sports Goods Sports Academy',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of textiles, fabrics, yarn, household linen, articles of clothing, floor coverings and tapestry, sports clothes..',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Home Furnishing   Kitchen and household hardware',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of hardware and sanitary fittings and fixtures and flat glass including tools such as hammers, saws, screwdrivers and other hand tools',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tea',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Tea',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Retail sale of cereals and pulses, tea, coffee, spices and flour',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of tea, coffee & cocoa',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of tea',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Telecom and Telecom Products',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Other communication services  telex wireless fax pager other telephone   communication services  mobile phones retail wholesale seller',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of mobile phones',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of telecommunication equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of telephone, mobile phone and communications equipment and parts',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Yarn',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Textiles Blended Yarn',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of other yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of synthetic yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of viscose yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of textiles, fabrics, yarn, household linen, articles of clothing, floor coverings and tapestry, sports clothes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of cotton yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Garments and Apparels',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Readymade garments',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Retail sale of readymade garments, hosiery goods, other articles of clothing and clothing accessories such as gloves, ties, braces etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Retail sale of ready made garments',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of ready-made garments',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of garment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Fabric',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Cotton synthetic blended knitted and silk fabric or cloth',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of grey fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of cotton fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Others',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Textile other than mentioned above',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Retail sale of textiles in specialized stores',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tiles Ceramic or Building Construction Material',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Construction materials ceramic tiles',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of construction material',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of ceramic tiles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of construction materials (sand, gravel etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Marble and Granite',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of marbles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of granite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tobacco',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Tobacco Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of un-manufactured tobacco, paan (betel leaf), opium, ganja, cinchona etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of manufactured tobacco & tobacco products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tractors',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Tractors',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Wholesale of tractors used in agriculture and forestry',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of tractors',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Cosmetics and Toiletries',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Perfumes cosmetics toiletries hair oil cream',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of toiletry, perfumery and cosmetics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Sugar',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Sugar',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of sugar',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of edible oils, fats, sugar and processed/manufactured spices etc.3',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Timber and Timber Products',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Wood and wood products  including furnitures',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Wholesale of wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of hardwood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of plywood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of softwood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Wholesale of teak wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of building material such as bricks, wood, sanitary equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Retail sale of household fuel oil, bottled gas, coal and fuel wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Wholesale of wood in the rough and products of primary processing of wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Watches',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Clocks and Watches',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Retail sale of watches and clocks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
        ],
        mdmEntityList: null,
      },
      {
        key: 'Manufacturing',
        guid: '',
        type: 'Nature',
        children: [
          {
            key: 'Textile Furnishing',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Home furnishing',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of home furnishing',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Yarn',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Textiles Blended Yarn',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of cotton yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Preparation and spinning of jute, Mesta and other natural fibers including blended natural fibers n.e.c. *Blended yarn/fabrics means, yarn/fabrics containing more than 50% of one fiber.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture hosiery yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metallized yarn or gimped yarn, rubber thread or cord covered with textile material, textile yarn or strip impregnated, covered or sheathed with rubber or plastic',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of textile yarn or strip impregnated',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of grey fabric yarns',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of multifilament yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of synthetic or artificial filament yarn, tenacity yarn whether or not textured including high tenacity yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of synthetic yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Garments and Apparels',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Readymade garments',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of all types of textile garments and clothing accessories',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of ready-made garments',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacturing of garments',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Fabric',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Cotton synthetic blended knitted and silk fabric or cloth',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of other knitted and crocheted fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture grey fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of grey fabric yarn',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of non woven fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of shirting fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of textile fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Weaving, manufacture of cotton and cotton mixture fabrics.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of rain coats of waterproof textile fabrics or plastic sheetings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of rain coats of waterproof textile fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of prefabricated buildings, or elements thereof, predominantly of wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of PP fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of HDPE fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of cotton fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of glass fiber (including glass-wool) and yarn of glass fiber nonwoven glass fabrics, mats, boards and similar non-woven products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Weaving, manufacturing of man-made fiber and man-made mixture fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of polyester fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Finishing of jute, mesta and other vegetable textiles fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Dyeing & printing grey fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of knitted and crocheted cotton fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of knitted and crocheted woolen fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of knitted and crocheted synthetic fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of synthetic fabrics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Ginning',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Ginning of cotton',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Cotton ginning, cleaning and bailing',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Ginning of cotton',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Activity of ginning and pressing cotton bales',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Others',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Textile other than mentioned above',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Other activities relating to finishing of textile n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other made-up textile articles, except apparel n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other textiles/textile products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of made-up textiles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tiles Ceramic or Building Construction Material',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Marble and Granite',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Quarrying of marble',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Quarrying of slate and building and monumental stone other than marble and granite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of marbles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Quarrying of marbles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Quarrying of granite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Quarrying of red granite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Quarrying of slate and building and monumental stone other than marble and granites',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of granite tiles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of granite slabs',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Construction materials ceramic tiles',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of ceramic tiles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufactures raw material used in ceramic tiles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal frameworks or skeletons for construction and parts thereof (towers, masts, trusses, bridges etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Operation of sand or gravel pits, basalt/porphyry, clay (ordinary), crushing and breaking of stone for use as a flux or raw material in lime or cement, manufacture or as building material, road metal or ballast and other materials for construction',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of m-sand',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tractors',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Tractors',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of tractors used in agriculture and forestry',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of commercial vehicles such as vans, lorries, over-the-road tractors for semi-trailers etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tyres',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Tyres',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of rubber tyres and tubes for motor vehicles, motorcycles, scooters, three-wheelers, tractors and aircraft',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of rubber tyres and tubes n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Watches',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Clocks and Watches',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of watches and clocks, including instrument panel clocks (except time-recording equipment)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Automobile 4W',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Authorised car dealers servicing manufacturers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of commercial vehicles such as vans, lorries, over-the-road tractors for semi-trailers etc.1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Glass',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Glass and Glass Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of other general purpose machinery n.e.c. ( fans intended for industrial applications, exhaust hoods for commercial, laboratory or industrial use; calendaring or other rolling machines other than for metals or glass; gaskets and similar j',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of ophthalmic goods, eyeglasses, sunglasses, lenses ground to prescription, contact lenses, safety goggles etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of glass in primary or semi-manufactured forms (such as sheets & plate glass) including mirror sheets and wired, colored, tinted, toughened or laminated glass',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of glass fiber (including glass-wool) and yarn of glass fiber nonwoven glass fabrics, mats, boards and similar non-woven products2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of hollow glassware (bottles, jars etc.) for the conveyance or packing of goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of glass bottles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other glassware/glass products: articles of glass used in construction such as glass blocks; clock or watch glasses, optical glass and optical glass elements not optically worked; and other glass products (including glass beads) n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of glass rectangular wires',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Paper',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Paper and Paper Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of paper cup',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of printing, writing and photocopying paper ready for use',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of writing & printing paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of packing paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of paper pulp articles other than containers (such as egg trays)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of wall paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of carbon paper & stationary items',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other paper products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of paper roll',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other special purpose paper (excluding computer stationary)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of paper stationery product',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of photo graphic color paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of paper board, straw board',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of paper board',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other primary paper materials including composite paper and paper board n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of kraft paper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of corrugated paper and paperboard',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of corrugated paper board containers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other containers of paper and paperboard n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of paper cups, saucers, plates, hoops, cones and other similar products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Sugar',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Sugar',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture or refining of sugar (sucrose) from sugarcane',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture or refining of sugercane sugar (sucrose)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Synthetic',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Woolen',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Preparation and spinning of wool, including other animal hair and blended* wool including other animal hair',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Weaving, manufacture of wool and wool mixture fabrics.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of knitted and crocheted woolen fabric',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of carpets and other floor coverings made of wool',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of carpets made of wool',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of rugs made of wool',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Silk PSF VSF nylon',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Preparation and spinning of man-made fiber including blended* man-made fiber',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Preparation of man-made fiber',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Spinning of man-made fibers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Weaving, manufacturing of man-made fiber and man-made mixture fabrics.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Finishing of man-made and blended man-made textiles.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Spinning of man-made fiber',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Timber and Timber Products',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Wood and wood products  including furnitures',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of wooden boxes, barrels, vats, tubs, packing cases etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of wooden boxes & racks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plywood chests',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other wooden containers and products entirely or mainly of cane, rattan, bamboo, willow, fiber, leaves and grass n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of wooden industrial goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of wooden agricultural implements',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Logging camps and loggers primarily engaged in felling timber and producing wood in the rough such as pitprops, split poles, pickets, hewn railway ties',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other wood products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Printing directly onto textiles, flexographic plastic, glass, metal, wood and ceramics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of cleaning and polishing products (preparations for perfuming or deodorizing rooms; artificial waxes and prepared waxes; polishes and creams for leather, wood, glass, metal etc.; scouring pastes and powders, including paper, wadding etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of insulation boards of vegetable fiber, straw or wood waste, agglomerated with cement & other mineral binders.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of furniture made of wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Producing wood in the rough',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of wooden furniture',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of ply wood and veneer sheets',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of decorative ply wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of ply wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of particle board and fiberboard including densified wood',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other plywood products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of structural wooden goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tobacco',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Tobacco Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Preparation of tobacco leaves',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other tobacco products including chewing tobacco n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Coffee',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Coffee Producers and coffee chains',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Coffee curing, roasting, grinding blending etc. and manufacturing of coffee products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Fertiliser',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Fertilisers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of urea and other organic fertilizers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of organic fertilizers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of straight mixed, compound or complex inorganic fertilizers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of NPK / mix fertilizers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other fertilizers n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of ploughs, manure spreaders, seeders, harrows and similar agricultural machinery for soil preparation, planting or fertilizing, harvesting or threshing machinery2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals copper',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Copper and Copper Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of Copper from ore, and other copper products and alloys',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of copper products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other electronic and electric wires and cables (insulated wire and cable made of steel, copper, aluminum)1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of copper wires',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals others',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'other than iron and steel zince copper aluminium',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of metal fasteners',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of metal containers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal cable and other articles made of wire (except for electric transmission)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal household articles (plates, saucers, pots, kettles, saucepans, frying pans and other non-electrical utensils, small hand-operated kitchen appliances and accessories)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal sanitary ware such as baths, sinks, washbasins and similar articles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of metal sanitary ware',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of reinforced safes, vaults, strong room doors, gates and metal goods for office use (other than office furniture) and other purposes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other fabricated metal products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other non-ferrous metals n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of fabricated metal products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Casting of non-ferrous metals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal frameworks or skeletons for construction and parts thereof (towers, masts, trusses, bridges etc.)1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other structural metal products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal containers for compressed or liquefied gas',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal reservoirs, tanks and similar containers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Forging, pressing, stamping and roll-forming of metal; powder metallurgy',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of precision sheet metal fabrication',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of sheet metal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Machining',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal fasteners (nails, rivets, tacks, pins, staples, washers and similar non-threaded products and nuts, bolts, screws and other threaded products)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of brass metal fasteners',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals zinc',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Zinc',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacturing of lead, zinc and tin products and alloys',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of zinc casted products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Rubber Natural',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Rubber and Rubber products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of rubber balloons',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other rubber products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other rubber products for automobile',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of rubber gaskets',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of rubber mats',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of rubber soles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of footwear made primarily of vulcanized or molded rubber and plastic.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of rubber tyres and tubes for motor vehicles, motorcycles, scooters, three-wheelers, tractors and aircrafts',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of rubber tyres and tubes n.e.c…',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of rubber plates, sheets, strips, rods, tubes, pipes, hoses and profile -shapes etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of rubber conveyor or transmission belts or belting',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Agriculture',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Milling Product',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Milling of maida',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Milling of maize',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Milling of sooji',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Rice milling',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Dal (pulses) milling',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Grain milling other than wheat, rice and dal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Vegetable milling (production of flour or meal of dried leguminous vegetables(except dal), of roots or tubers, or of edible nuts)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Other grain milling and processing n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Flour milling',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Spices and Grams',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Processing of spices',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Grinding and processing of spices',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Dry Fruits',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Processing of cashew',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Growing of cashews',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Fruits and Nurts and Vegatables',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Growing of edible nuts (almonds, cashew nuts, chestnuts, hazelnuts pistachios, walnuts and other nuts)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Seed Related',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Growing of other oil seeds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Growing of vegetable seeds (except beet seed)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Growing of vegetable seeds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Seed processing for propagation',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Growing of pulses (dal) and other leguminous crops such as peas and beans, not used as oilseeds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Cleaning and grading of seeds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Processing of seeds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Auto Ancilliaries',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Auto Ancilliaries',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Production of automation oil',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of automated machines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other automotive components',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of automotive components',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of automotive electrical equipment1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Bearings',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of bearings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of bearings, gears, gearing and driving elements',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Valves',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of fluid valves',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of non fluid valves',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other pumps, compressors, taps and valves etc.3',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Casting and Forgings',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Castings and Forgings',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of steel casting products2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Casting of non-ferrous metals3',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Casting of aluminium products1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of ductile iron castings1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Forging, pressing, stamping and roll-forming of metal; powder metallurgy1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other iron and steel casting and products thereof1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Cement',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Cement and Asbestos Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of Portland cement, aluminous cement, slag cement and similar hydraulic cement1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of articles articles of concrete, cement or artificial stone (tiles, bricks etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of articles of concrete and cement',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of clinkers and cement',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of hume pipes and other pre-fabricated structural components of cement and/or concrete for building or civil engineering',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other cement and asbestos cement products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Chemicals',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Chemicals petrochemicals specility chemicals',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of metal treatment chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of water treatment chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of armoatic chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of organic & inorganic chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of homoeopathic or biochemical pharmaceutical preparations1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of organic and inorganic chemical compounds n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of chemical and intermediates',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of chemical compounds',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of special chemicals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other agrochemical products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of essential oils; modification by chemical processes of oils and fats (e.g. by oxidation, polymerization etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of basic chemical elements',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of chemical products or preparations of a kind used in the textiles, paper, leather and like industries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of chemical blowing agents',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Coal',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Coal and Lignite Minerals',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Cleaning, sizing, grading, pulverizing, compressing etc. of coal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Other operations relating to mining and agglomeration of hard coal2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Opencast mining of hard coal1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Commercial Vehicle',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'LCV HCV Commercial Vehicles',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of commercial vehicles such as vans, lorries, over-the-road tractors for semi-trailers etc.11',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Cosmetics and Toiletries',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Perfumes cosmetics toiletries hair oil cream',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of perfumes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of cosmetics and toiletries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of cosmetics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of perfumes and cologne de-eau',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Drug Stores',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Drugs and pharmaceuticals drug proprietaries and druggists sundries',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of medicinal substances used in the manufacture of pharmaceuticals: antibiotics, endocrine products, basic vitamins; opium derivatives; sulpha drugs; serums and plasmas; salicylic acid, its salts and esters; glycosides and vegetable alkal2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of pharmaceutical formulations3',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of active pharmaceutical ingredient2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of allopathic pharmaceutical preparations2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of `ayurvedic’ or `unani’ pharmaceutical preparation2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other pharmaceutical and botanical products n.e.c. like hina powder etc.2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of pharmaceuticals2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of medicinal substances used in the manufacture of pharmaceuticals: antibiotics, endocrine products, basic vitamins; opium derivatives; sulpha drugs; serums and plasmas; salicylic acid, its salts and esters; glycosides and vegetable alkal1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Dyes and Pigments',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Dyes and Pigments',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of dyes and dye intermediates',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of dyes and pigments from any source in basic form or as concentrate',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacturer of color pigments',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of prepared pigments, prepared opacifiers and prepared colors, verifiable enamels and glazes engobes and similar preparations of a kind used in the ceramic, enameling or glass industry',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of dyes and pigments from any source in basic form or as concentrate1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Electricals',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Electrical good and equipments',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of metal household articles (plates, saucers, pots, kettles, saucepans, frying pans and other non-electrical utensils, small hand-operated kitchen appliances and accessories)1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of electrical control panel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of electrical cables',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other electrical equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of electrical equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of motor vehicle electrical equipment, such as generators, alternators, spark plugs, ignition wiring harnesses, power window and door systems, assembly of purchased gauges into instrument panels, voltage regulators, etc.1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of automotive electrical equipment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of electrical insulators and insulating fittings of ceramics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Engineering',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Precision Dyes and Parts Fasteners',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of hume pipes and other pre-fabricated structural components of cement and/or concrete for building or civil engineering1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Food Processing',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Food processing',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Cafeterias, fast-food restaurants and other food preparation in market stalls',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of packaged snacks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Soft drinks bottled water Non alcoholic beverages',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of mineral water',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Cocoa Confectionary Dairy packaged food bakery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of other cocoa, chocolate, sugar confectionery products n.e.c.1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of chocolate and chocolate confectionery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of chocolate confectionery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of sugar confectionery (except sweetmeats)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other cocoa, chocolate, sugar confectionery products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of sugar confectionery products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other dairy products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other dairy products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other bakery products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of cocoa products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Marine Foods Soya bean products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Processing of cuttlefish',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Processing of fish',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Processing and canning of fish',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Production of fishmeal for human consumption or animal feed1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Production, processing and preservation of other fish products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of fish oil',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Processing and preserving of fish crustacean and similar foods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Gems and Jewellery',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Gems and jewellery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of gold jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of diamond jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of diamond studded jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of silver jewellery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of jewellery of gold, silver and other precious or base metal metal clad with precious metals or precious or semi-precious stones, or of combinations of precious metal and precious or semi-precious stones or of other materials',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Industrial Equipment or Machinery',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Industrial Machinery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of other machinery for mining, quarrying and construction n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of machinery for the dairy industry',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for the grain milling industry',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of weighing machinery (other than sensitive laboratory balances)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for the bakery industry or for making macaroni, noodles, pasta etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for the extraction or preparation of animal or vegetable fats or oils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for the preparation of tobacco and for the making of cigarettes or cigars, or for pipe or chewing tobacco or snuff',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other machinery for the industrial preparation or manufacture of food or drink n.e.c. (including tea or coffee making machines)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of tea blending machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for preparation of textile fibers, spinning machines, machines for preparing textile yarns, weaving machines (looms), including hand looms, knitting machines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of textile printing machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for washing, bleaching, dyeing, dressing, finishing coating or impregnating textile fabrics, machines for reeling, unreeling, folding, cutting or pinking textile fabrics, and similar machinery for fabric processing',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of machinery for textile',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of ironing machines, commercial washing and drying machines, dry-cleaning machines and other laundry machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for preparing, tanning or working hides, skins or leather, machinery for making or repairing footwear or other articles of hides, skins, leather or fur skins',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other general purpose machinery n.e.c. ( fans intended for industrial applications, exhaust hoods for commercial, laboratory or industrial use; calendaring or other rolling machines other than for metals or glass; gaskets and similar j2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other machinery for textiles, apparel and leather production n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for making paper pulp, paper, paperboard and articles of paper board',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for working soft rubber or plastics or for the',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other special-purpose machinery n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of machinery for feeding',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of laboratory apparatus (laboratory ultrasonic cleaning machinery, laboratory sterilizers, laboratory type distilling apparatus, laboratory centrifuges etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of various parts for machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of ploughs, manure spreaders, seeders, harrows and similar agricultural machinery for soil preparation, planting or fertilizing, harvesting or threshing machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of spraying machinery for agricultural use',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other agricultural and forestry machinery n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of agricultural machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal-forming machinery and machine tools n.e.c',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of boring, cutting, sinking and tunneling machinery (whether or not for underground use)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of concrete and mortar mixers, piled rivers and pile-extractors, mortar spreaders, bitumen spreaders, concrete surfacing machinery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of parts and accessories for machinery/equipment used by construction and mining industries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Industrial Furnaces',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of ovens, furnaces and furnace burners',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Pumps',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of other pumps, compressors, taps and valves etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of fluid pumps',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Compressors',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of compressor',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other pumps, compressors, taps and valves etc.2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Machine Tools',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of parts and accessories for the machine tools listed above',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of metal-forming machinery and machine tools n.e.c2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machine tools for turning, drilling, milling, shaping, planning, boring, grinding etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machine tools for turning, drilling, milling, shaping, planning, boring, grinding etc.2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Engines',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of electric motors (except internal combustion engine starting motors)2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of engines and turbines, except aircraft, vehicle and cycle engines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of power generators (except battery charging alternators for internal combustion engines), motor generator sets (except turbine generator set units)2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Switching Appratus',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of switch, switch box, lamp holders, lugs etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of switch',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of switchgear',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'General Purpose Machinery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of other general purpose machinery n.e.c. ( fans intended for industrial applications, exhaust hoods for commercial, laboratory or industrial use; calendaring or other rolling machines other than for metals or glass; gaskets and similar j1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Jute',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Jute',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of coir and jute mats',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of jute bags',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of carpets, and other floor coverings made of jute, Mesta and coir',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Leather',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Luggage and Leather Goods or other leather products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of leather footwear',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of footwear made primarily of vulcanized or molded rubber and plastic.1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of leather footwear such as shoes, sandals, chappals, leather-cum-rubber/plastic cloth sandles and chappals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Liquor or Breweries or imfi',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Beer Wine and Distilled Alcoholic Beverages',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of distilled, potable, alcoholic beverages such as whisky, brandy, gin, “mixed drinks” etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of beer',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Live Stock',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Animal Feed',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Production of fishmeal for human consumption or animal feed',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals aluminium',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Aluminium and Aluminium Products  manufacturers ',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of aluminium powder',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of Portland cement, aluminous cement, slag cement and similar hydraulic cement',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of Aluminum from alumina and by other methods and products of aluminum and alloys',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of aluminium products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of aluminium extrusions',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of aluminium ingots',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of aluminium alloys',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Casting of aluminium products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of aluminium utensils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other electronic and electric wires and cables (insulated wire and cable made of steel, copper, aluminum)12',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of aluminium wires',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of Aluminium ore (bauxite)1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Metals Iron and Steel',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Iron Steel and Steel Alloys Wires and Cables',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of steel rolls',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of hot-rolled and cold-rolled products of steel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of cold rolled steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other structured steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of hot rolled steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of stainless steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of tube and tube fittings of basic iron and steel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of railway track materials (unassembled rails) of steel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of wire of steel by cold drawing or stretching',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other basic iron and steel n.e.c',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of alloy steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of tubes, pipes and hollow profiles and of tube or pipe fittings of cast-iron/cast-steel12',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel pipes 1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel strips/tubes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of direct reduction of iron (sponge iron) and other spongy ferrous products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other iron and steel casting and products thereof',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel casting products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel scaffolding',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Fabrication of structured steel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of stainless steel utensils',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel bathroom accessories',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other electronic and electric wires and cables (insulated wire and cable made of steel, copper, aluminum)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel wires',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of sponge iron',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of tube and tube fittings of basic iron and steel1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other basic iron and steel n.e.c11',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of tubes, pipes and hollow profiles and of tube or pipe fittings of cast-iron/cast-steel1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other iron and steel casting and products thereof12',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of iron casted products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of ductile iron castings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of galvanized iron wires',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of steel in ingots or other primary forms, and other semi-finished products of steel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of mild steel ingots',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of iron ores2',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel in ingots',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of steel mats',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Mining',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Mining',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Mining of precious metal ore (gold, silver)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of precious metal ore (gold)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of precious metal ore (silver)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of lead and zinc ore',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of lead and zinc ores',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of other non-ferrous metal ores, n.e.c. [titanium (limonite and rutile),niobium, tantalum, vanadium or zirconium ores; tin bearing ores and, nickel, cobalt, tungsten, molybdenum, antimony and other non-ferrous metal ores]',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of antimony',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of cobalt',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Other operations relating to mining and agglomeration of hard coal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of molybdenum',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of nickel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of niobium',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of other non-ferrous metal ores',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of tantalum',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of tin bearing ores',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of titanium (ilmenite and rutile)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of tungsten',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of vanadium or zirconium ores',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of dolomite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of gypsum including selenite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining/quarrying of limestone, lime shell, ‘kankar’ and other calcareous minerals including calcite, chalk and shale',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining/quarrying of calcite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining/quarrying of chalk',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining/quarrying of limestone',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining/quarrying of other calcareous minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining/quarrying of shale',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining/quarrying of limeshell',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of clays (kaolin, ball clay, wollastonite, bentonite, fuller’s earth, fire clay etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of ball clay',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of bentonite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of fire clay',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of fuller’s earth',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of kaolin',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of other clays',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of wollastonite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of refractory non-clay minerals (andalusite, kyanite, sillimanite, dunite, diaspore magnesite, diaspore, magnesite)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of andalusite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of diaspore magnesite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of dunite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of kyanite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of refractory non-clay minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of sillimanite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of native sulphur or pyrites and pyrrhotites valued chiefly for sulphur',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of native sulphur',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of pyrites and pyrrhotites valued chiefly',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of natural phosphates including apatite minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of apatite minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of natural phosphates',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of earth colors (ochre including red oxide).',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of earth colours',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of ochre',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of red oxide',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of fluorspar',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of barytes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of potash bearing salts/minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of borate minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of other fertiliser and chemical minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of potash bearing mineral',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of potash bearing salts',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Salt mining, quarrying, screening etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of gemstones (agate, diamond, emerald, garnet (gem), jasper, rubby/sapphire etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of agate',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of diamond',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of emerald',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of garnet (gem)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of gemstones',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of jasper',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of rubby/saphire',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining and quarrying of abrasive materials (pumice stone, emery, corundum, garnet and other natural abrasives)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining and quarrying of abrasive materials',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining and quarrying of corundum',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining and quarrying of emery',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining and quarrying of garnet',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining and quarrying of other natural abrasives',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining and quarrying pumice stone',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of mica',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of natural graphite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of asbestos',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of vermiculite, perlite and chlorites',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of chlorites',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of perlite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of vermiculite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of felspar and silica minerals including quartz, quartzite and fuch. Quartzite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of felspar',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of quartz',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of quartzite and fuch. quartzite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of silica minerals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of talc/steatite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of talc',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Mining of laterite, diatomite and siliceous fossil meals (e.g. diatomite) and other natural fluxes, natural asphalt or bitumen and other mining n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of bitumen',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of diatomite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of laterite',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of other',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of other natural fluxes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of silicious fossil meals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining ofnatural asphalt',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of iron ores',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of Aluminium ore (bauxite)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Opencast mining of hard coal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of manganese ore',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Mining of chromium ore',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Others M',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Industries not classified elsewhere ',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Industries not classified elsewhere  Mfg Segment',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Packaging',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Packaging Material',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of HDPE packaging material',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Plastic Packaging Goods',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of flexible packagingg',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of plastic articles for the packing of goods (plastic bags, sacks, containers, boxes, cases, carboys, bottles etc.)1',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Paints',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Paints and Varnishes',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of paints',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of caulking compounds and similar non-refractory filling or surfacing preparations, mastics, prepared paint or varnish removers, organic composite solvents and thinners and other related products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of paints and varnishes, enamels or lacquers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Pesticides',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Pesticied',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of pesticides',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Pharmaceuticals',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Biotech Research',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of medicinal substances used in the manufacture of pharmaceuticals: antibiotics, endocrine products, basic vitamins; opium derivatives; sulpha drugs; serums and plasmas; salicylic acid, its salts and esters; glycosides and vegetable alkal1x',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'CRAMS or Formulations or API or Ayurveda or Bulk Drugs',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of drug intermediaries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of iodine derivatives',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of pharmaceuticals',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of allopathic mediciness',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of ayurvedic medicinal productss',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of pharmaceutical formulations',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of active pharmaceutical ingredient',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of allopathic pharmaceutical preparations',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of allopathic medicines',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of `ayurvedic’ or `unani’ pharmaceutical preparation',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of ayurvedic medicinal products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of veterinary preparations',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of veterinary formulations',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of medical impregnated wadding, gauze, bandages, dressings, surgical gut string etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of medicinal substances used in the manufacture of pharmaceuticals: antibiotics, endocrine products, basic vitamins; opium derivatives; sulpha drugs; serums and plasmas; salicylic acid, its salts and esters; glycosides and vegetable alkal',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other pharmaceutical and botanical products n.e.c. like hina powder etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of contrast media intermediaries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Photographic and Allied Products',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Photgraphic and Allied Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of printing, writing and photocopying paper ready for uses',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Pipes',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Manufacturing and sales of pipes PVC including dealers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of HDPE pipe fittings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of HDPE pipes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of PVC pipe fittings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of PVC pipes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of hume pipes and other pre-fabricated structural components of cement and/or concrete for building or civil engineerings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of tubes, pipes and hollow profiles and of tube or pipe fittings of cast-iron/cast-steel',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of pipe fittings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of welded pipes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of rubber plates, sheets, strips, rods, tubes, pipes, hoses and profile -shapes etc',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Plastics',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Polymers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of polymer/ synthetic / PVC water storage tanks',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of display components (plasma, polymer, LCD, LED)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key:
                  'Plastic tubes and sheets and other plastic products plastic resins thermoplastics',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture  plastic containers',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of bathing tubs, wash-basins, lavatory pans and covers, flushing cisterns and similar sanitary-ware of plastics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic buckets',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of travel goods of plastics (suitcase, vanity bags, holdalls and similar articles)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of spectacle frames of plastic',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of molded industrial accessories of plastics',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of industrial plastic components',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic mouldings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic preforms',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of other plastics products n.e.c.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of footwear made primarily of vulcanized or molded rubber and plastic',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of disposable plastic cups',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of disposable plastic glass',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of disposable plastic plates',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic sheet products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of self-adhesive plastic tapes',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic components',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of semi-finished of plastic products (plastic plates, sheets, blocks, film, foil, strip etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic rolls',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of tableware, kitchenware and other household articles and toilet articles of plastic, including manufacture of vacuum flasks and other vacuum vessels',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastics jars and caps',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of plastic articles for the packing of goods (plastic bags, sacks, containers, boxes, cases, carboys, bottles etc.)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic bags',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic bottles',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Plastic Films',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of plastic in primary forms',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of plastic films',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Poultry',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Poultry and Meat Products',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Processing of buffalo meat',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Operation of poultry hatcheries',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of poultry feed',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Preservation, Processing and canning of meat',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Power',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Motors Generator and pumps and other power equipments',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of electric motors (except internal combustion engine starting motors)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of power generators (except battery charging alternators for internal combustion engines), motor generator sets (except turbine generator set units)',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of motor vehicle electrical equipment, such as generators, alternators, spark plugs, ignition wiring harnesses, power window and door systems, assembly of purchased gauges into instrument panels, voltage regulators, etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of power generators (except battery charging alternators for internal combustion engines), motor generator sets (except turbine generator set units',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Transformers',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of electric power distribution transformers, arc-welding transformers, fluorescent ballasts, transmission and distribution voltage regulators',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of transformer bushings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of electronic capacitors, resistors, chokes, coils, transformers (electronic) and similar components',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Soaps and Detergents',
            guid: '',
            type: 'Industry',
            children: [
              {
                key:
                  'Manufacturers of toiler soaps detergents shampoos toothpaste shaving products shoe polish and household accessories',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of soap all forms',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of detergent and similar washing agents excluding soap',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of hair oil, shampoo, hair dye etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of hair oil',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of hair oil, shampoo, hair dye etcs',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Speciality',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Sports Goods Sports Academy',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Manufacture of sports goods',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
              {
                key: 'Home Furnishing or Kitchen and household hardware',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of cutlery such as knives, forks, spoons, cleavers, choppers, razors, razor blades, scissors, hair clippers etc.',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Manufacture of door hardware products',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Tea',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Tea',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key: 'Plantation & cultivation of tea',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Processing and blending of tea including manufacture of instant tea',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Processing of tea',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of other machinery for the industrial preparation or manufacture of food or drink n.e.c. (including tea or coffee making machines).',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key: 'Growing of tea',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
          {
            key: 'Textile Machinery',
            guid: '',
            type: 'Industry',
            children: [
              {
                key: 'Textile machinery',
                guid: '',
                type: 'Segment',
                children: [
                  {
                    key:
                      'Manufacture of machinery for preparation of textile fibers, spinning machines, machines for preparing textile yarns, weaving machines (looms), including hand looms, knitting machine',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                  {
                    key:
                      'Manufacture of machinery for washing, bleaching, dyeing, dressing, finishing coating or impregnating textile fabrics, machines for reeling, unreeling, folding, cutting or pinking textile fabrics, and similar machinery for fabric processings',
                    guid: '',
                    type: 'Product',
                    children: [],
                    mdmEntityList: null,
                  },
                ],
                mdmEntityList: null,
              },
            ],
            mdmEntityList: null,
          },
        ],
        mdmEntityList: null,
      },
    ],
    mdmEntityList: null,
  },
};
