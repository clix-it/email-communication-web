import * as actions from './constants';

export function loadApplicantDetailsInit() {
  return {
    type: actions.LOAD_APPLICANT_DETAILS_INIT,
  };
}

export function loadApplicantDetailsEnd() {
  return {
    type: actions.LOAD_APPLICANT_DETAILS_END,
  };
}

export function updateApplicantDetailsInit() {
  return {
    type: actions.UPDATE_APPLICANT_DETAILS_INIT,
  };
}

export function setSelectedApplicant(applicant) {
  return {
    type: actions.SET_CURRENT_APPLICANT,
    applicant,
  };
}

export function setSelectedGuarantor(applicant) {
  return {
    type: actions.SET_CURRENT_GUARANTOR,
    applicant,
  };
}

export function setSelectedEntity(entity) {
  return {
    type: actions.SET_CURRENT_ENTITY,
    entity,
  };
}

export function loadEntityDetails(appId) {
  return {
    type: actions.LOAD_ENTITY_DETAILS,
    payload: {
      appId,
    },
  };
}

export function loadEntityDetailsSuccess(entity) {
  return {
    type: actions.LOAD_ENTITY_DETAILS_SUCCESS,
    payload: {
      entity,
    },
  };
}

export function generateCibilReport(entity, file, type, id, docType) {
  return {
    type: actions.GENERATE_CIBIL_REPORT,
    payload: {
      entity,
      file,
      type,
      id,
      docType,
    },
  };
}

export function generateDelphiReport(appId, cuid) {
  return {
    type: actions.GENERATE_DELPHI_REPORT,
    payload: {
      appId,
      cuid,
    },
  };
}

export function loadEntityDetailsError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_ENTITY_DETAILS_ERROR,
    payload: {
      errorMessage,
      severity,
    },
  };
}

export function loadIndustrySuccess(industry) {
  return {
    type: actions.LOAD_INDUSTRY_SUCCESS,
    payload: {
      industry,
    },
  };
}

export function loadGSTByPan(pan, type) {
  return {
    type: actions.LOAD_GST_BY_PAN,
    payload: {
      pan,
      type,
    },
  };
}

export function loadCityByPinCode(pinCode, index) {
  return {
    type: actions.LOAD_CITY_BY_PINCODE,
    payload: {
      pinCode,
      index,
    },
  };
}

export function loadCityByPinCodeSuccess(cityData) {
  return {
    type: actions.LOAD_CITY_BY_PINCODE_SUCCESS,
    payload: {
      cityData,
    },
  };
}

export function populateIndustrySubtype(level, value) {
  return {
    type: actions.POPULATE_INDUSTRY_SUBTYPE,
    payload: {
      level,
      value,
    },
  };
}

export function updateEntityDetails(data, reset, values) {
  return {
    type: actions.UPDATE_ENTITY_DETAILS,
    payload: {
      data,
      reset,
      values,
    },
  };
}

export function updateCoApplicants(data, entityCuid, reset, values, isGuarantor) {
  return {
    type: actions.UPDATE_CO_APPLICANTS,
    payload: {
      data,
      entityCuid,
      reset,
      values,
      isGuarantor,
    },
  };
}

export function loadUserDetailsSuccess(userDetails) {
  return {
    type: actions.USER_DETAILS_SUCCESS,
    payload: {
      userDetails,
    },
  };
}

export function loadCitiesByPinCodesSuccess(data) {
  return {
    type: actions.LOAD_CITIES_BY_PINCODES_SUCCESS,
    payload: {
      data,
    },
  };
}

export function loadUpdateAppSuccess(success) {
  return {
    type: actions.LOAD_UPDATE_APP_SUCCESS,
    payload: {
      success,
    },
  };
}

export function loadUpdateUserRequest(data, cuid, type) {
  return {
    type: actions.UPDATE_USER_REQUEST,
    payload: {
      data,
      cuid,
      type,
    },
  };
}

export function loadUpdateUserSuccess(success) {
  return {
    type: actions.UPDATE_USER_SUCCESS,
    payload: {
      success,
    },
  };
}

export function loadGstByPanSuccess(gstArray) {
  return {
    type: actions.LOAD_GST_BY_PAN_SUCCESS,
    payload: {
      gstArray,
    },
  };
}

export function clearMessage() {
  return {
    type: actions.CLEAR_MESSAGE,
  };
}

export function startPerfios({ payload, fileData, appId }) {
  return {
    type: actions.START_PERFIOS,
    payload,
    fileData,
    appId,
  };
}

export function startPerfiosSuccess(response) {
  return {
    type: actions.START_PERFIOS_SUCCESS,
    response,
  };
}

export function startPerfiosError(error) {
  return {
    type: actions.START_PERFIOS_ERROR,
    error,
  };
}
export function getPerfiosReport(payload) {
  return {
    type: actions.GET_PERFIOS_REPORT,
    payload,
  };
}

export function getPerfiosReportSuccess(response) {
  return {
    type: actions.GET_PERFIOS_REPORT_SUCCESS,
    response,
  };
}

export function getPerfiosReportError(error) {
  return {
    type: actions.GET_PERFIOS_REPORT_ERROR,
    error,
  };
}

export function checkNSDL(data) {
  return {
    type: actions.CHECK_NSDL,
    payload: data,
  };
}

export function checkNSDLSuccess(data) {
  return {
    type: actions.CHECK_NSDL_SUCCESS,
    nsdlResponse: data,
  };
}

export function checkNSDLError(data) {
  return {
    type: actions.CHECK_NSDL_ERROR,
    nsdlError: data,
  };
}

export function loadPanNsdl(pan, type) {
  return {
    type: actions.LOAD_PAN_NSDL,
    payload: {
      pan,
      type,
    },
  };
}

export function addApplicantEntity() {
  return {
    type: actions.ADD_APPLICANT_ENTITY,
  };
}

export function addApplicant() {
  return {
    type: actions.ADD_APPLICANT,
  };
}

export function addGuarantor() {
  return {
    type: actions.ADD_GUARANTOR,
  };
}

export function removeApplicant(id, link) {
  return {
    type: actions.REMOVE_APPLICANT,
    id,
    link,
  };
}

export function setFormValidationError(formName, isValid) {
  return {
    type: actions.SET_FORM_VALIDATION_ERROR,
    payload: {
      formName,
      isValid,
    },
  };
}

export function isDirtyForm(val) {
  return {
    type: actions.IS_DIRTY,
    dirty: {
      val,
    },
  };
}

export function unlinkApplicant(id, link) {
  return {
    type: actions.UNLINK_APPLICANT,
    payload: {
      id,
      link,
    },
  };
}

export function unlinkGuarantor(id, link) {
  return {
    type: actions.UNLINK_GUARANTOR,
    payload: {
      id,
      link,
    },
  };
}

export function clear_state() {
  return {
    type: actions.CLEAR_STATE,
  };
}

export function updateUsers(userData, cuid, data) {
  return {
    type: actions.UPDATE_USERS,
    userData,
    cuid,
    data,
  };
}

export function fetchPriority(partner, product) {
  // ;
  return {
    type: actions.FETCH_PRIORITY,
    partner,
    product,
  };
}

export function fetchPrioritySuccess(data) {
  // ;
  return {
    type: actions.FETCH_PRIORITY_SUCCESS,
    data,
  };
}

export function fetchPriorityError(error) {
  // ;
  return {
    type: actions.FETCH_PRIORITY_ERROR,
    error,
  };
}

export function fetchRiskLevel(partner, product) {
  // ;
  return {
    type: actions.FETCH_RISK_LEVEL,
    partner,
    product,
  };
}

export function fetchRiskLevelSuccess(data) {
  // ;
  return {
    type: actions.FETCH_RISK_LEVEL_SUCCESS,
    data,
  };
}

export function fetchRiskLevelError(error) {
  // ;
  return {
    type: actions.FETCH_RISK_LEVEL_ERROR,
    error,
  };
}

export function fetchMasterDataGeneric(partner, product, masterType) {
  return {
    type: actions.FETCH_MASTER_DATA_GENERIC,
    partner,
    product,
    masterType,
  };
}

export function fetchMasterDataGenericSuccess(data, masterType) {
  return {
    type: actions.FETCH_MASTER_DATA_GENERIC_SUCCESS,
    data,
    masterType,
  };
}

export function fetchMasterDataGenericError(error, masterType) {
  return {
    type: actions.FETCH_MASTER_DATA_GENERIC_ERROR,
    error,
    masterType,
  };
}

export function fetchCategory(partner, product) {
  // ;
  return {
    type: actions.FETCH_CATEGORY,
    partner,
    product,
  };
}

export function fetchCategorySuccess(data) {
  // ;
  return {
    type: actions.FETCH_CATEGORY_SUCCESS,
    data,
  };
}

export function fetchCategoryError(error) {
  // ;
  return {
    type: actions.FETCH_CATEGORY_ERROR,
    error,
  };
}

export function callCommercialBureau(payload) {
  return {
    type: actions.CALL_COMMERCIAL_BUREAU,
    payload,
  };
}

export function loadDelphiOutput() {
  return {
    type: actions.LOAD_DELPHI_OUTPUT,
  };
}

export function loadDelphiOutputSuccess(data) {
  return {
    type: actions.LOAD_DELPHI_OUTPUT_SUCCESS,
    payload: {
      data,
    },
  };
}

export function loadDeplhiOutputError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_DELPHI_OUTPUT_ERROR,
    payload: {
      errorMessage,
      severity,
    },
  };
}
