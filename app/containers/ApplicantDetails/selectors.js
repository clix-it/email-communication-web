import _ from 'lodash';
import moment from 'moment';
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const getDetails = (details, appGsts, userDetails) => {
  const userData = {};
  const {
    cuid = '',
    firstName = '',
    lastName = '',
    registeredName = '',
    preferredPhone = '',
    preferredEmail = '',
    corporateStructure = '',
    id = '',
    dateOfBirthIncorporation = '',
  } = details;
  //  contactibilities on basis of id]
  const newContactDetails =
    details &&
    Object.keys(details).length > 0 &&
    details.contactibilities &&
    details.contactibilities.length > 0
      ? details.contactibilities.sort((a, b) => b.id - a.id)
      : [];

  userData.contactDetails = newContactDetails[0] || {};
  userData.identities = (details && details.userIdentities) || {};

  userData.addressArray = [];
  userData.addressArray.push(
    newContactDetails[0] ? newContactDetails[0].addressLine1 : '',
  );
  userData.addressArray.push(
    newContactDetails[0] ? newContactDetails[0].addressLine2 : '',
  );
  userData.addressArray.push(
    newContactDetails[0] ? newContactDetails[0].addressLine3 : '',
  );
  userData.addressArray.push(
    newContactDetails[0] ? newContactDetails[0].pincode : '',
  );
  userData.addressArray.push(
    newContactDetails[0] ? newContactDetails[0].id : '',
  );
  userData.dateOfBirthIncorporation = dateOfBirthIncorporation;

  userData.registeredName = registeredName;
  userData.firstName = firstName;
  userData.lastName = lastName;
  userData.preferredPhone = preferredPhone;
  userData.preferredEmail = preferredEmail;
  userData.userDetails = details ? details.userDetails : {};
  userData.appGsts = appGsts && appGsts.length > 0 ? appGsts : [];
  userData.corporateStructure = corporateStructure;
  userData.userDetails = details ? details.userDetails : '';
  userData.id = id;
  userData.cuid = cuid;

  if (cuid && userDetails && userDetails.length > 0) {
    userDetails.forEach(user => {
      console.log(userDetails, 'userDetails1');
      if (user.cuid === cuid) {
        const newEntityOfficers = user.entityOfficers.sort(
          (a, b) => b.id - a.id,
        );
        userData.entityOfficers = newEntityOfficers[0] || {};
      }
    });
  }
  userData.type = details && details.type ? details.type : '';
  return userData;
};

const getUserDetails = state => {
  const creditData = {
    entity: {},
    applicantsData: [],
    isNoEntity: false,
  };
  const {
    applicantDetails: {
      entity = {},
      citiesByPincodes = {},
      userDetails = {},
    } = {},
  } = state;
  if (Object.keys(entity).length === 0) {
    return {};
  }
  const {
    users = [],
    appGsts = [],
    additionalData = {},
    loanOffers = [],
    appReferences = [],
  } = entity;
  // filter users array

  const { data: { addressChangesFlages = [] } = {} } = additionalData;
  const newAddressFlags = {};
  if (addressChangesFlages.length > 0) {
    addressChangesFlages.forEach(flag => {
      newAddressFlags[flag.cuid] = flag.addresschanged;
    });
  }

  // console.log('newAddressFlags', newAddressFlags);

  let newUsersCompany = [...users];
  if (users && users.length > 0 && users[0].type) {
    newUsersCompany = _.filter(users, { type: 'COMPANY' });
    if (newUsersCompany && newUsersCompany.length > 1)
      newUsersCompany = _.filter(
        users,
        user =>
          (_.get(user, 'appLMS.role') === 'Applicant' ||
            !_.get(user, 'appLMS.role')) &&
          user.type === 'COMPANY',
      );
  }

  const newUsersIndividual = users.filter(
    user =>
      user.type && user.type.toUpperCase() === 'INDIVIDUAL' && user.userLinked,
  );

  const newUsersGuarantor = users.filter(
    user =>
      user.type && user.type.toUpperCase() === 'GUARANTOR' && user.userLinked,
  );

  if (newUsersCompany && newUsersCompany.length > 0) {
    creditData.entity = getDetails(
      newUsersCompany[0],
      newUsersCompany[0].appGsts,
    );
    creditData.entity.cuid = newUsersCompany[0].cuid || '';
    creditData.entity.additionalData = additionalData;
    creditData.entity.addressFlags = newAddressFlags;
    creditData.entity.loanOffers = loanOffers;
    creditData.entity.appReferences = appReferences;
    creditData.isNoEntity = false;
  } else if (users && users.length > 0) {
    creditData.isNoEntity = true;
    creditData.entity = getDetails(users[0], users[0].appGsts);
    creditData.entity.cuid = users[0].cuid || '';
    creditData.entity.addressFlags = newAddressFlags;
    creditData.entity.additionalData = additionalData;
    creditData.entity.loanOffers = loanOffers;
    creditData.entity.appReferences = appReferences;
  }

  const individualData = [];
  if (newUsersIndividual && newUsersIndividual.length > 0) {
    newUsersIndividual.forEach((e, index) => {
      const data = getDetails(e, e.appGsts || [], userDetails);
      if (Object.keys(e).length > 0) {
        data.pincodeData = citiesByPincodes[index];
        data.id = e.id;
        data.cuid = e.cuid || '';
        data.addressFlags = newAddressFlags;
        data.localities = [];
        data.userLinked = e.userLinked;
        individualData.push(data);
      }
    });
  }

  const guarantorData = [];
  if (newUsersGuarantor && newUsersGuarantor.length > 0) {
    newUsersGuarantor.forEach((e, index) => {
      const data = getDetails(e, appGsts, userDetails);
      if (Object.keys(e).length > 0) {
        data.pincodeData = citiesByPincodes[index];
        data.id = e.id;
        data.cuid = e.cuid || '';
        data.addressFlags = newAddressFlags;
        data.localities = [];
        data.userLinked = e.userLinked;
        guarantorData.push(data);
      }
    });
  }

  // if (newUsersIndividual.length === 0) {
  //   // const data = getDetails([], appGsts);
  //   // data.id = 0;
  //   // individualData.push(data);
  //   // just for checking
  //   users.forEach((e, index) => {
  //     const data = getDetails(e, appGsts);
  //     data.id = e.id;
  //     data.cuid = e.cuid || '';
  //     data.localities = [];
  //     data.pincodeData = citiesByPincodes[index];
  //     individualData.push(data);
  //   });
  // }

  creditData.applicantsData = _.map(
    _.filter(users, { type: 'INDIVIDUAL' }),
    user => ({
      ...user,
      dateOfBirthIncorporation: moment(
        user.dateOfBirthIncorporation,
        'DD-MM-YYYY',
      ).isValid()
        ? moment(user.dateOfBirthIncorporation, 'DD-MM-YYYY')
        : '',
    }),
  );

  creditData.guarantorsData = _.map(
    _.filter(users, { type: 'GUARANTOR' }),
    user => ({
      ...user,
      dateOfBirthIncorporation: moment(
        user.dateOfBirthIncorporation,
        'DD-MM-YYYY',
      ).isValid()
        ? moment(user.dateOfBirthIncorporation, 'DD-MM-YYYY')
        : '',
    }),
  );

  console.log('creditData', creditData);
  return creditData;
};

const getAppDocumentDetails = state => {
  const docsData = [];
  const { entity } = state.applicantDetails;
  if (Object.keys(entity).length === 0) {
    return [];
  }
  docsData.push({ key: 0, appId: entity.appId });
  if (entity.users.length > 0) {
    _.map(
      entity.users,
      (user, index) =>
        user.userLinked &&
        docsData.push({
          key: index + 1,
          cuid: user.cuid,
          name: `${user.registeredName ||
            `${user.firstName || ''} ${user.lastName || ''}`}`,
          userLinked: user.userLinked,
        }),
    );
  }
  return docsData;
};

const selectApplicantDetailsDomain = state =>
  state.applicantDetails || initialState;

/**
 * Default selector used by Applicant Details
 */

export const makeSelectApplicantDetails = () =>
  createSelector(
    [getUserDetails],
    substate => substate,
  );

const makeSelectApplicantDetails2 = () =>
  createSelector(
    selectApplicantDetailsDomain,
    substate => substate,
  );

const makeSelectGstArray = () =>
  createSelector(
    selectApplicantDetailsDomain,
    substate => substate.gstArray,
  );

const makeAppDocumentDetails = () =>
  createSelector(
    [getAppDocumentDetails],
    substate => substate,
  );

const makeAppDocumentDetailsData = () =>
  createSelector(
    selectApplicantDetailsDomain,
    substate => substate,
  );

export default makeSelectApplicantDetails;
export {
  makeSelectApplicantDetails2,
  makeAppDocumentDetails,
  makeAppDocumentDetailsData,
  makeSelectGstArray,
};
// import { createSelector } from 'reselect';
// import { initialState } from './reducer';

// /**
//  * Direct selector to the loanDetails state domain
//  */

// const selectApplicantDetailsDomain = state =>
//   state.applicantDetails || initialState;

// /**
//  * Other specific selectors
//  */

// /**
//  * Default selector used by LoanDetails
//  */

// const makeSelectApplicantDetails = () =>
//   createSelector(
//     selectApplicantDetailsDomain,
//     substate => substate,
//   );

// export default makeSelectApplicantDetails;
// export { selectApplicantDetailsDomain };
