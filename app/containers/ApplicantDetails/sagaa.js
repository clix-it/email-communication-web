import _ from 'lodash';
import { call, select } from 'redux-saga/effects';
import { message } from 'antd';
import moment from 'moment';
import request from 'utils/request';
import { revertFormatDate } from 'utils/helpers';
import env from '../../../environment';

import { makeAppDocumentDetailsData } from './selectors';
function* updateunLinkApplicants() {
  const { unlinkedUsers } = yield select(makeAppDocumentDetailsData());
  if (!unlinkedUsers) {
    return;
  }
  for (let i = 0; i < unlinkedUsers.length; i++) {
    const newUser = { ...unlinkedUsers[i] };
    unlinkedUsers[i] = newUser;
    // yield call(updateUser, newUser);
  }
  return unlinkedUsers;
}

function* updateEntity(applicants) {
  const {
    entity: { users, entityCuid },
  } = yield select(makeAppDocumentDetailsData());
  const entityUser = _.find(users, { cuid: entityCuid });
  const entityOfficers = _.flatten(
    _.compact(
      _.map(_.filter(applicants, user => user), applicant => ({
        ...applicant.entityOfficers,
        belongsTo: applicant.cuid,
      })),
    ),
  );
  if (entityUser) {
    const body = {
      id: entityUser.id,
      cuid: entityUser.cuid,
      entityOfficers,
      registeredName: entityUser.registeredName,
    };
    yield call(updateUser, body);
  }
}

export function* createUsers(applicants) {
  const newUsers = _.filter(
    applicants,
    applicant => applicant && String(applicant.id).includes('NEW'),
  );

  for (let i = 0; i < newUsers.length; i++) {
    const newUser = {
      ...newUsers[i],
      identities: newUsers[i].userIdentities,
      id: '',
      dateOfBirthIncorporation: revertFormatDate(
        newUsers[i].dateOfBirthIncorporation,
      ),
      userDetails: {
        ...newUsers[i].userDetails,
        id: '',
      },
    };
    const response = yield call(createUser, newUser);
    newUsers[i].cuid = response.data.cuid;
    newUsers[i].entityOfficers = {
      designation: newUsers[i].entityOfficers
        ? newUsers[i].entityOfficers.designation
        : '',
      belongsTo: response.data.cuid,
    };
  }
  return newUsers;
}

export function* updateUsers(users) {
  for (let i = 0; i < users.length; i++) {
    const newUser = {
      ...users[i],
      dateOfBirthIncorporation: revertFormatDate(
        users[i].dateOfBirthIncorporation,
      ),
      entityOfficers: [],
    };
    yield call(updateUser, newUser);
  }
  return users;
}

export function* dedupeApplicants(applicants) {
  const oldApplicants = _.filter(
    applicants,
    applicant => applicant && !String(applicant.id).includes('NEW'),
  );
  for (let i = 0; i < oldApplicants.length; i++) {
    const newUser = {
      ...oldApplicants[i],
      id: '',
      dateOfBirthIncorporation: revertFormatDate(
        oldApplicants[i].dateOfBirthIncorporation,
      ),
    };
    yield call(dedupeUser, newUser);
  }
  return oldApplicants;
}

export function* dedupeUser(user) {
  try {
    const response = yield call(
      request,
      `${env.DEDUPE_URL}/individual/${user.cuid}`,
      {
        method: 'PUT',
        headers: env.headers,
        body: JSON.stringify(user),
      },
    );
    message.success(
      `${user.firstName || user.registeredName} deduped successfully`,
    );
    return response;
  } catch (err) {
    message.success(`${user.firstName || user.registeredName} deduped failed`);
  }
}
export function* updateUser(user) {
  const response = yield call(
    request,
    `${env.USER_SERVICE_API_URL}/${user.cuid}`,
    {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify(user),
    },
  );
  message.success(
    `${user.firstName || user.registeredName || ''} updated successfully`,
  );
  return response;
}

export function* createUser(user) {
  const response = yield call(request, `${env.DEDUPE_URL}`, {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify({ ...user }),
  });
  message.success(
    `${user.firstName || user.registeredName} created successfully`,
  );
  return response;
}

export function* updateAplication(body, appid, consumerCibilScore, foir) {
  const {
    entity: { applicants, additionalData = {} },
  } = yield select(makeAppDocumentDetailsData());

  const cibilScores =
    additionalData &&
    additionalData.data &&
    additionalData.data.extraDataField &&
    additionalData.data.extraDataField.consumerCibilScore
      ? JSON.parse(additionalData.data.extraDataField.consumerCibilScore)
      : {};
  body.forEach(item => {
    cibilScores[item.cuid] = consumerCibilScore;
  });
  const users = _.map(_.filter(body, applicant => applicant), user => ({
    ...user,
    type: 'INDIVIDUAL',
    id: String(user.id).includes('NEW') ? '' : user.id,
    userLinked: _.find(applicants, { id: user.id }).userLinked,
    dateOfBirthIncorporation: moment(user.dateOfBirthIncorporation).format(
      'YYYY-MM-DD',
    ),
  }));
  const response = yield call(request, `${env.APP_SERVICE_API_URL}/${appid}`, {
    method: 'PUT',
    headers: env.headers,
    body: JSON.stringify({
      users,
      additionalData: {
        data: {
          extraDataField: {
            consumerCibilScore: JSON.stringify({ ...cibilScores }),
            foir: foir
          },
        },
      },
    }),
  });
  message.success('Application updated successfully');
  return response;
}
export function* updateAllApplicants(applicants, appid, consumerCibilScore, foir) {
  // const uunlinkedUsers = yield call(updateunLinkApplicants, applicants);
  const createNewUsers = yield call(createUsers, applicants);
  const updatedUsers = yield call(dedupeApplicants, applicants);
  yield call(updateUsers, _.flatten([...updatedUsers]));
  yield call(updateEntity, applicants);

  return yield call(
    updateAplication,
    [...applicants],
    appid,
    consumerCibilScore,
    foir,
  );
}
