import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useLocation } from 'react-router-dom';
import FinancialUpload from 'containers/FinancialUpload';
import FinancialStatementUpload from 'containers/FinancialStatementUpload';
import makeSelectApplicantDetails, {
  makeSelectApplicantDetails2,
  makeAppDocumentDetails,
  makeAppDocumentDetailsData,
} from './selectors';
import { makeSelectApplications } from '../CreditPortal/selectors';
import { makeSelectKycDetails } from '../KycDetails/selectors';

import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';

import EntityDetails from '../../components/EntityDetails';
import PersonalDetails from '../../components/PersonalDetails';
import GuarantorDetails from '../../components/GuarantorDetails';
import Tabs from '../../components/Tabs';
import LoanDetails from '../LoanDetails';
import OtherDetails from '../OtherDetails';
import LeadDocuments from '../LeadDocuments';
//import BankDetails from '../BankDetails';
import DelphiOutput from '../DelphiOutput';
import HunterOutput from '../HunterOutput';
import BankAnalysisReport from '../BankAnalysisReport';
import InsuranceDetails from '../InsuranceDetails';
import {
  loadEntityDetails,
  setFormValidationError,
  addApplicantEntity,
  fetchMasterDataGeneric,
  loadDelphiOutput,
} from './actions';
import PaymentSchedule from '../PaymentSchedule';
import KycTabComponent from '../../components/KycTabComponent';
import Remarks from '../Remarks';
import EntityTab from '../../components/EntityTab';
import FinancialAnalysisReport from '../FinancialAnalysisReport';
import AddEmailDetails from '../AddEmailDetails';

export function ApplicantDetails({
  dispatch,
  match,
  creditData,
  applicantDetails,
  appDocumentDetails,
  applicationData,
  requestType,
  activities,
  kycDetails,
  activity,
}) {
  const { appId } = match.params;
  const { loanOffers = [] } = applicantDetails;
  //  const { currentApplication = {} } = activities;
  const [tabErrors, setTabErrors] = useState({});
  const { dkycDetails = {}, navigateToDocumentTab = false } = kycDetails || {
    dkycDetails: {},
    navigateToDocumentTab: false,
  };
  const loc = useLocation();

  // useEffect(() => {
  //   dispatch(setFormValidationError('all', {}));
  //   dispatch(loadEntityDetails(appId));
  // }, [appId]);

  useEffect(() => {
    setTabErrors(applicantDetails.formValidationErrors);
  }, [applicantDetails.formValidationErrors]);

  // useEffect(() => {
  //   dispatch(
  //     fetchMasterDataGeneric(
  //       _.get(applicantDetails, 'entity.partner', ''),
  //       _.get(applicantDetails, 'entity.product', ''),
  //       `MANDATORY_DOCS_${currentApplication.userActivity}`,
  //     ),
  //   );
  //   dispatch(
  //     fetchMasterDataGeneric(
  //       _.get(applicantDetails, 'entity.partner', ''),
  //       _.get(applicantDetails, 'entity.product', ''),
  //       'APPLICANTTYPE',
  //     ),
  //   );
  //   if (_.get(applicantDetails, 'entity.partner', '') === 'HFSAPP') {
  //     dispatch(
  //       fetchMasterDataGeneric(
  //         _.get(applicantDetails, 'entity.partner', ''),
  //         _.get(applicantDetails, 'entity.product', ''),
  //         'BUSINESSTYPE',
  //       ),
  //     );
  //     dispatch(
  //       fetchMasterDataGeneric(
  //         _.get(applicantDetails, 'entity.partner', ''),
  //         _.get(applicantDetails, 'entity.product', ''),
  //         'QUALIFICATION',
  //       ),
  //     );
  //     dispatch(
  //       fetchMasterDataGeneric(
  //         _.get(applicantDetails, 'entity.partner', ''),
  //         _.get(applicantDetails, 'entity.product', ''),
  //         'COMPANYCATEGORY',
  //       ),
  //     );
  //   }
  // }, [
  //   _.get(applicantDetails, 'entity.partner', ''),
  //   _.get(applicantDetails, 'entity.product', ''),
  // ]);

  // useEffect(() => {
  //   if (activity === 'INCOME_REVIEW') dispatch(loadDelphiOutput());
  // }, []);

  console.log(applicantDetails, 'applicant data');
  const tabsToShow = [
    {
      id: 'applicantDetails',
      name: 'Applicant Details',
      component: (
        <PersonalDetails
          dispatch={dispatch}
          appId={appId}
          applicationData={applicationData}
          applicantsData={creditData.applicantsData}
          requestType={requestType}
          applicantDetails={applicantDetails}
          isNoEntity={creditData.isNoEntity}
          tabName="Applicant Details"
          activity={activity}
        />
      ),
      requestType,
      tabErrors,
    },
    {
      id: 'loanDetails',
      name: 'Loan Details',
      component: (
        <Tabs
          mode="top"
          tabPanel={[
            {
              id: 'loanDetails',
              name: 'Loan Details',
              component: (
                <LoanDetails
                  dispatch={dispatch}
                  appId={appId}
                  requestType={requestType}
                  isNoEntity={creditData.isNoEntity}
                  activity={activity}
                />
              ),
              requestType,
              tabErrors,
            },
            {
              id: 'paymentSchedule',
              name: 'Payment Schedule',
              component: <PaymentSchedule />,
            },
          ]}
        />
      ),
      requestType,
      tabErrors,
    },

    {
      id: 'bankDetails',
      name: 'Bank Details',
      component: (
        <Tabs
          mode="top"
          ifSendBack={false}
          tabPanel={[
            {
              id: 'bankDetails',
              name: 'Bank Details',
              component: (
                <AddEmailDetails
                  requestType={requestType}
                  dispatch={dispatch}
                  appId={appId}
                  isNoEntity={creditData.isNoEntity}
                  activity={activity}
                  applicantDetails={applicantDetails}
                />
              ),
            },
            {
              id: 'bankStatementDetails',
              name: 'Bank Statement Analyser',
              component: (
                <FinancialUpload
                  loanOffers={loanOffers}
                  isNoEntity={creditData.isNoEntity}
                  appId={appId}
                />
              ),
            },
            {
              id: 'perfiosDetails',
              name: 'Bank Anaysis Report',
              component: <BankAnalysisReport appId={appId} />,
            },
          ]}
        />
      ),
      requestType,
      tabErrors,
    },
    {
      id: 'otherDetails',
      name: 'Other Details',
      component: (
        <OtherDetails
          requestType={requestType}
          dispatch={dispatch}
          appId={appId}
          isNoEntity={creditData.isNoEntity}
          activity={activity}
        />
      ),
      requestType,
      tabErrors,
    },
    {
      id: 'kycDetails',
      name: 'KYC Details',
      component: (
        <KycTabComponent
          applicantDetails={applicantDetails}
          requestType={requestType}
          appId={appId}
          creditData={creditData}
          isDkycAvailable={!!dkycDetails.leadId}
        />
      ),
      requestType,
      tabErrors,
    },
    {
      id: 'delphiOutput',
      name: 'Delphi Output',
      component: (
        <DelphiOutput
          activity={activity}
          applicantDetails={applicantDetails}
          requestType={requestType}
        />
      ),
      requestType,
      tabErrors,
    },
    {
      id: 'hunterOutput',
      name: 'Hunter Output',
      component: (
        <Tabs
          mode="top"
          ifSendBack={false}
          tabPanel={[
            {
              id: 'hunter1',
              name: 'Hunter 1',
              component: (
                <HunterOutput requestType={requestType} appId={appId} />
              ),
            },
          ]}
        />
      ),
      requestType,
      tabErrors,
    },
    {
      id: 'documents',
      name: 'Documents',
      component: (
        <LeadDocuments
          dispatch={dispatch}
          appId={appId}
          appDocumentDetails={appDocumentDetails}
          applicantDetails={applicantDetails}
          requestType={requestType}
          activityType={activity}
        />
      ),
      requestType,
      tabErrors,
    },
    {
      id: 'remarksDetails',
      name: 'History',
      component: <Remarks appId={appId} />,
    },
  ];

  if (loc.pathname.toLowerCase().includes('postsanction')) {
    tabsToShow.splice(5, 0, {
      id: 'insuranceDetails',
      name: 'Insurance Details',
      component: (
        <InsuranceDetails
          appId={appId}
          requestType={requestType}
          applicantDetails={applicantDetails}
          tabName="Insurance Details"
        />
      ),
      requestType,
      tabErrors,
    });
  }

  // if (currentApplication && currentApplication.product != 'PL') {
  //   tabsToShow.unshift({
  //     id: 'entityDetails',
  //     name: 'Entity Details',
  //     component: (
  //       <EntityTab
  //         dispatch={dispatch}
  //         appId={appId}
  //         requestType={requestType}
  //         creditData={creditData}
  //         applicantDetails={applicantDetails}
  //         applicationData={applicationData}
  //         tabName="Entity Details"
  //         addApplicantEntity={addApplicantEntity}
  //         // activityType={activityType}
  //         activity={activity}
  //       />
  //     ),
  //     requestType,
  //     tabErrors,
  //   });
  // }

  // if (
  //   currentApplication &&
  //   (currentApplication.product == 'BL' || currentApplication.product == 'LAEP')
  // ) {
  //   tabsToShow.splice(5, 0, {
  //     id: 'financials',
  //     name: 'Financials',
  //     component: (
  //       <Tabs
  //         mode="top"
  //         ifSendBack={false}
  //         tabPanel={[
  //           {
  //             id: 'financialStatementAnalyser',
  //             name: 'Financial Statement Analyser',
  //             component: (
  //               <FinancialStatementUpload
  //                 loanOffers={loanOffers}
  //                 appId={appId}
  //                 // activityType={activityType}
  //                 // isNoEntity={creditData.isNoEntity}
  //               />
  //             ),
  //           },
  //           {
  //             id: 'financialStatementReport',
  //             name: 'Financial Statement Report',
  //             component: <FinancialAnalysisReport appId={appId} />,
  //           },
  //         ]}
  //       />
  //     ),
  //     requestType,
  //     tabErrors,
  //   });
  // }

  // if (currentApplication && currentApplication.product == 'HFS') {
  //   tabsToShow.splice(2, 0, {
  //     id: 'guarantorDetails',
  //     name: 'Guarantor Details',
  //     component: (
  //       <GuarantorDetails
  //         dispatch={dispatch}
  //         appId={appId}
  //         applicationData={applicationData}
  //         applicantsData={creditData.guarantorsData}
  //         requestType={requestType}
  //         applicantDetails={applicantDetails}
  //         isNoEntity={creditData.isNoEntity}
  //         tabName="Guarantor Details"
  //         activity={activity}
  //       />
  //     ),
  //     requestType,
  //     tabErrors,
  //   });
  // }

  return (
    // <Tabs
    //   formDirty={applicantDetails.dirty}
    //   tabPanel={tabsToShow}
    //   hideEntity={creditData.isNoEntity}
    //   ifSendBack
    //   navigateToDocumentTab={navigateToDocumentTab}
    // />
    <AddEmailDetails
      requestType={requestType}
      dispatch={dispatch}
      appId={appId}
      isNoEntity={creditData.isNoEntity}
      activity={activity}
      applicantDetails={applicantDetails}
    />
  );
}

ApplicantDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.object,
  creditData: PropTypes.object,
  appDocumentDetails: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  creditData: makeSelectApplicantDetails(),
  applicantDetails: makeSelectApplicantDetails2(),
  appDocumentDetails: makeAppDocumentDetails(),
  applicationData: makeAppDocumentDetailsData(),
  activities: makeSelectApplications(),
  kycDetails: makeSelectKycDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

ApplicantDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'applicantDetails', reducer });

//const withSaga = useInjectSaga({ key: 'applicantDetails', saga });
export default compose(
  withReducer,
//  withSaga,
  withConnect,
  memo,
)(ApplicantDetails);
