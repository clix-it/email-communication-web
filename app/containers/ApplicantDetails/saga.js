import _ from 'lodash';
import { takeEvery, put, select, all, call } from 'redux-saga/effects';
import request from 'utils/request';
import moment from 'moment';
import { message } from 'antd';
import { revertFormatDate } from 'utils/helpers';
import { fetchProgram } from 'containers/FetchProgram/saga';
import * as actions from './constants';
import env from '../../../environment';
import {
  loadApplicantDetailsInit,
  loadEntityDetailsSuccess,
  loadEntityDetailsError,
  loadIndustrySuccess,
  loadCityByPinCodeSuccess,
  loadCitiesByPinCodesSuccess,
  loadUpdateAppSuccess,
  updateApplicantDetailsInit,
  loadUpdateUserSuccess,
  loadUserDetailsSuccess,
  startPerfiosSuccess,
  startPerfiosError,
  getPerfiosReportSuccess,
  getPerfiosReportError,
  loadGstByPanSuccess,
  loadApplicantDetailsEnd,
  fetchPrioritySuccess,
  fetchPriorityError,
  fetchRiskLevelSuccess,
  fetchRiskLevelError,
  fetchMasterDataGenericSuccess,
  fetchMasterDataGenericError,
  fetchCategorySuccess,
  fetchCategoryError,
  callCommercialBureau,
  loadDeplhiOutputError,
  loadDelphiOutputSuccess,
} from './actions';
import {
  checkNSDL,
  updateApplication,
  updateUser,
  updateCompositeApplication,
} from '../App/saga';
import makeSelectApplicantDetails, {
  makeSelectApplicantDetails2,
  makeAppDocumentDetailsData,
} from './selectors';
import { updateAllApplicants, createUsers } from './sagaa';

import { uploadDoc } from '../LeadDocuments/saga';
import { fetchApplicant } from '../ApplicantPage/saga';
import data from './data.json';
import delphiOp from './delphiOp.json';

function* fetchUserDetailsSaga() {
  yield put(loadApplicantDetailsInit());
  const creditData = yield select(makeSelectApplicantDetails());
  // console.log('creditData', creditData);
  const { applicantsData } = creditData;
  try {
    const results = yield all(
      applicantsData.map(applicant =>
        call(fetch, `${env.USER_SERVICE_API_URL}/${applicant.cuid}`, {
          headers: env.headers,
          method: 'GET',
        }),
      ),
    );
    const validResults = yield all(
      results.map(result => {
        if (result.status === 200) {
          return call([result, 'json']);
        }
        return '';
      }),
    );

    const resToSend = [];
    validResults.forEach(responseBody => {
      if (responseBody.success || responseBody.status) {
        resToSend.push(responseBody.user);
      }
    });
    yield put(loadUserDetailsSuccess(resToSend));
    // console.log(resToSend, 'resToSend');
  } catch (err) {
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
  }
}

export function* fetchEntityDetailsSaga(action) {
  // yield put(loadApplicantDetailsInit());
  const { payload: { appId = '' } = {} } = action;
  const appIdFromSession = sessionStorage
    .getItem('id')
    .replace(/^"(.*)"$/, '$1');
  try {
    const result = yield fetch(
      `${env.APP_SERVICE_API_URL}/${appId || appIdFromSession}`,
      {
        headers: env.headers,
        method: 'GET',
      },
    );

    if (result.status === 200) {
      const responseBody = yield result.json();
      // const responseBody = data;
      if (responseBody.success || responseBody.status) {
        yield put(loadEntityDetailsSuccess(responseBody.app));
        const isTypeCompanyPresent = _.find(responseBody.app.users, {
          type: 'COMPANY',
        });
        if (isTypeCompanyPresent) {
          yield call(fetchApplicant, {
            cuid: _.find(
              responseBody.app.users,
              user =>
                (_.get(user, 'appLMS.role') === 'Applicant' ||
                  !_.get(user, 'appLMS.role')) &&
                user.type === 'COMPANY',
            ).cuid,
          });
        }
      } else {
        yield put(
          loadEntityDetailsError(
            'Oops! No pending entity is found for the given app Id:( ',
            'info',
          ),
        );
      }
    } else {
      const errorResponse = yield result.json();
      yield put(
        loadEntityDetailsError(
          errorResponse.message ||
            `HTTP Status${result.status} occurred.Try Again`,
        ),
      );
    }
  } catch (err) {
    yield put(
      loadEntityDetailsError(
        `Network error occurred! Check connection! ${err}`,
      ),
    );
  }
}

function* fetchIndustrySaga() {
  // yield put(loadApplicantDetailsInit());
  yield put(loadIndustrySuccess(actions.INDUSTRY_RES.data));

  // try {
  //   const result = yield fetch(
  //     // `https://api-uat.clix.capital/master/v1/fetch?key=Constitution`,
  //     `https://api-uat.clix.capital/master/v1/fetch?key=SubIndustry`,
  //     {
  //       headers: env.headers,
  //       method: 'GET',
  //     },
  //   );

  //   if (result.status === 200) {
  //     const responseBody = yield result.json();
  //     if (responseBody.success || responseBody.status) {
  //       console.log('industry', responseBody);
  //       // yield put(loadEntityDetailsSuccess(responseBody.app));
  //     } else {
  //       yield put(
  //         loadEntityDetailsError(
  //           'Oops! No pending entity is found for the given app Id:( ',
  //           'info',
  //         ),
  //       );
  //     }
  //   } else {
  //     yield put(
  //       loadEntityDetailsError(
  //         `HTTP Status${result.status} occurred.Try Again`,
  //       ),
  //     );
  //   }
  // } catch (err) {
  //   yield put(
  //     loadEntityDetailsError('Network error occurred! Check connection!'),
  //   );
  // }
}

function* uploadCibilReport(action) {
  const uploadCibilResult = yield uploadDoc(action);
  const uploadCibilResponse = yield uploadCibilResult.json();
  // console.log('uploadCibilResponse', uploadCibilResponse);
  return uploadCibilResponse;
}

function* generateCibilReport(action) {
  const { entity, file, type, id, docType } = action.payload;
  yield put(updateApplicantDetailsInit());
  const mbStateName = _.get(
    yield fetchProgram({
      programcode: 'MBSTATECODES',
      cmCode: _.get(entity, 'applicantSegment.addresses[0].state'),
    }),
    'masterData[0].cmValue',
  );
  entity.applicantSegment.addresses[0].state = mbStateName; // _.update(entity, 'applicantSegment.addresses[0].state', mbStateName);
  try {
    const generateCibilResult = yield fetch(`${env.MULTI_BUREAU_API_URL}`, {
      headers: env.headers,
      method: 'POST',
      body: JSON.stringify({ ...entity }),
    });
    if (generateCibilResult.status === 200) {
      const generateCibilresponseBody = yield generateCibilResult.json();
      if (
        generateCibilresponseBody.success ||
        generateCibilresponseBody.status
      ) {
        const actionForUpload = {
          payload: {
            file,
            type,
            id,
            docType,
            pdfFile: generateCibilresponseBody.bureauResponse[0].pdfResponse,
            callFrom: 'entityDetails',
          },
        };
        const uploadCibilResponse = yield* uploadCibilReport(actionForUpload);
        if (uploadCibilResponse.success || uploadCibilResponse.status) {
          yield put(loadUpdateAppSuccess({ cibilDocUploaded: true }));
        } else {
          yield put(
            loadEntityDetailsError(
              'Oops! Could not upload CIBIL Report for the particular user. :(',
              'info',
            ),
          );
        }
      } else {
        yield put(
          loadEntityDetailsError(
            'Oops! Could not generate CIBIL Report for the particular user. :(',
            'info',
          ),
        );
      }
    } else {
      const errorResponse = yield generateCibilResult.json();
      yield put(
        loadEntityDetailsError(
          errorResponse.message ||
            `HTTP Status${generateCibilResult.status} occurred.Try Again`,
        ),
      );
    }
  } catch (err) {
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
  }
}

function* generateCommercialBureau({ payload }) {
  const reqUrl = `${env.COMMERCIAL_BUREAU_API_URL}/?cuid=${_.get(
    payload,
    'cuid',
    '',
  )}&applicationId=${JSON.parse(sessionStorage.getItem('id'))}`;
  try {
    const response = yield call(request, reqUrl, {
      headers: env.headers,
    });
  } catch (err) {
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
    // message.error('Error while calling Commercial Bureau');
  }
}

function* generateDelphiReport(action) {
  const { appId, cuid } = action.payload;
  yield put(updateApplicantDetailsInit());
  try {
    const generateDelphiResult = yield fetch(`${env.BUREAU_DELPHI_API_URL}`, {
      headers: env.headers,
      method: 'POST',
      body: JSON.stringify({ applicationId: appId, cuid }),
    });
    if (generateDelphiResult.status === 200) {
      const generateDelphiresponseBody = yield generateDelphiResult.json();
      if (
        generateDelphiresponseBody.success ||
        generateDelphiresponseBody.status
      ) {
        yield put(loadUpdateAppSuccess({ delphiGenerated: true }));
      } else {
        yield put(
          loadEntityDetailsError(
            'Oops! Could not generate DELPHI Report for the particular user. :(',
            'info',
          ),
        );
      }
    } else {
      const errorResponse = yield generateDelphiResult.json();
      yield put(
        loadEntityDetailsError(
          errorResponse.message ||
            `HTTP Status${generateDelphiResult.status} occurred.Try Again`,
        ),
      );
    }
  } catch (err) {
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
  }
}

function* updateEntityDetails(action) {
  yield put(updateApplicantDetailsInit());
  try {
    const result = yield updateApplication(
      action.payload.data,
      JSON.parse(sessionStorage.getItem('id')),
    );
    if (result.status === 200) {
      action.payload.reset(action.payload.values);
      const responseBody = yield result.json();
      if (responseBody.success || responseBody.status) {
        message.success('Entity updated!!');
        yield put(loadUpdateAppSuccess(responseBody));
      } else {
        yield put(
          loadEntityDetailsError(
            'Oops! No pending entity is found for the given app Id:( ',
            'info',
          ),
        );
      }
    } else {
      const errorResponse = yield result.json();
      yield put(
        loadEntityDetailsError(
          errorResponse.message ||
            `HTTP Status${result.status} occurred.Try Again`,
        ),
      );
    }
  } catch (err) {
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
  }
}

function* updateUserDetails(action) {
  try {
    const result = yield updateUser(
      action.payload.data,
      action.payload.cuid,
      action.payload.type,
    );
    if (result.status === 200) {
      const responseBody = yield result.json();
      // console.log('responseBody', responseBody);
      if (responseBody.responseCode) {
        if (action.payload.type === 'COMPANY')
          yield put(loadUpdateUserSuccess(responseBody.responseMessage));
      } else {
        yield put(
          loadEntityDetailsError(
            'Oops! No pending entity is found for the given app Id:( ',
            'info',
          ),
        );
      }
    } else {
      const errorResponse = yield result.json();
      yield put(
        loadEntityDetailsError(
          errorResponse.message ||
            `HTTP Status${result.status} occurred.Try Again`,
        ),
      );
    }
  } catch (err) {
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
  }
}

export function* updateApplicantDetails(action) {
  yield put(loadApplicantDetailsInit());
  const response = yield call(
    updateAllApplicants,
    action.payload.data.applicants,
    JSON.parse(sessionStorage.getItem('id')),
    action.payload.data.consumerCibilScore,
    action.payload.data.foir,
  );

  if (response.success || response.status) {
    // yield put(loadUpdateAppSuccess(responseBody));
    action.payload.reset(action.payload.values);
    yield put(loadUpdateUserSuccess('Profile Updated Successfully!'));
  } else {
    yield put(
      loadEntityDetailsError(
        'Oops! No pending entity is found for the given app Id:( ',
        'info',
      ),
    );
  }
}

export function* generateCuid(user, result) {
  // yield put(updateApplicantDetailsInit());
  const userBody = {
    // registeredName: user.registeredName || '',
    // displayName: user.registeredName || '',
    firstName: user.firstName || '',
    lastName: user.lastName || '',
    dateOfBirthIncorporation: user.dateOfBirthIncorporation || '',
    preferredEmail: user.preferredEmail || '',
    preferredPhone: user.preferredPhone || '',
    type: user.type,
  };
  userBody.identities = { ...user.userIdentities };
  userBody.contactibilities = [];
  const userDataEO = {};
  if (user.entityOfficers && user.entityOfficers.length > 0) {
    userDataEO.entityOfficers = [];
    userDataEO.entityOfficers[0] = {
      designation: user.entityOfficers[0].designation || '',
    };
    if (user.entityOfficers[0].id) {
      userDataEO.entityOfficers[0].id = user.entityOfficers[0].id;
    }
  }
  userBody.contactibilities[0] = {
    addressLine1: user.contactibilities[0].addressLine1 || '',
    addressLine2: user.contactibilities[0].addressLine2 || '',
    addressLine3: user.contactibilities[0].addressLine3 || '',
    locality: user.contactibilities[0].locality || '',
    pincode: user.contactibilities[0].pincode || '',
    city: user.contactibilities[0].city || '',
    phoneNumber: user.preferredPhone || '',
    email: user.preferredEmail || '',
  };
  try {
    if (user.cuid === '' || user.cuid === undefined) {
      const body = { ...userBody };
      body.cuid = '';
      const apiresult = yield fetch(`${env.DEDUPE_URL}`, {
        headers: env.headers,
        method: 'POST',
        body: JSON.stringify(body),
      });
      const responseBody = yield apiresult.json();
      if (responseBody.responseCode === 200 && responseBody.data.cuid) {
        result.users.push({ ...user, cuid: responseBody.data.cuid });
      }
    } else {
      if (user.entityOfficers && user.entityOfficers.length > 0) {
        yield* updateUsers(userDataEO, user.cuid);
      }

      yield* updateUserDetails({
        payload: {
          data: userBody,
          cuid: user.cuid,
          type: userBody.type.toLowerCase(),
        },
      });
      result.users.push({ ...user });
    }
  } catch (err) {}
}

function* fetchPanNsdl(action) {
  let panDataChange = true;
  const { payload: { pan = '', type = '' } = {} } = action;
  let formData = [];
  const panData2 = yield select(makeSelectApplicantDetails2());

  for (const i in panData2.nsdlResponse) {
    if (panData2.nsdlResponse[i].pan == pan) {
      panDataChange = false;
      return;
    }
  }
  if (panDataChange) {
    formData = [...panData2.nsdlResponse, { type, pan, nsdlResponse: {} }];
  }

  yield checkNSDL({ formData });
}
export function* updateUsers(userData, cuid) {
  // yield put(updateApplicantDetailsInit());
  try {
    const result = yield fetch(`${env.USER_SERVICE_API_URL}/${cuid}`, {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify(userData),
    });

    return result;
  } catch (err) {}
}

function* fetchGstByPan(action) {
  let panDataChange = true;
  const { payload: { pan = '', type = '' } = {} } = action;
  const creditData = yield select(makeSelectApplicantDetails());
  const gst = creditData.entity.gstn;
  let panToUse = creditData.entity.contactDetails.pan;

  let formData = [];
  const panData = yield select(makeSelectApplicantDetails2());

  for (const i in panData.nsdlResponse) {
    if (panData.nsdlResponse[i].pan == pan) {
      panDataChange = false;
      return;
    }
  }
  if (panDataChange) {
    formData = [...panData.nsdlResponse, { type, pan, nsdlResponse: {} }];
  }

  yield checkNSDL({ formData });
  if (gst && !pan) return;
  // const gst = creditData.entity.gstn;
  // if (gst && !pan) return;
  // return yield put(loadGstByPanSuccess(actions.GST_RES.result || []));
  yield put(loadApplicantDetailsInit());
  if (pan) panToUse = pan || panToUse;
  try {
    // const result = yield fetch(`https:/search`, {
    const result = yield fetch('https://gst.karza.in/uat/v1/search', {
      headers: { ...env.karzaKey },
      method: 'POST',
      body: JSON.stringify({
        consent: 'Y',
        pan: panToUse,
      }),
    });
    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.result && responseBody.result.length > 0) {
        yield put(loadGstByPanSuccess(responseBody.result));
      } else {
        yield put(
          loadEntityDetailsError(
            'Oops! No Gstn values available for the PAN :( ',
            'info',
          ),
        );
        yield put(loadGstByPanSuccess([]));
      }
    } else {
      const errorResponse = yield result.json();
      yield put(
        loadEntityDetailsError(
          errorResponse.message ||
            // `HTTP Status${result.status} occurred.Try Again`,
            'The Karza Api is Not Working! Thanks for the patience!',
          'info',
        ),
      );
    }
  } catch (err) {
    yield put(
      // loadEntityDetailsError('Network error occurred! Check connection!'),
      loadEntityDetailsError(
        'The Karza Api is Not Working! Thanks for the patience!',
        'info',
      ),
    );
  }
}

function* fetchCityByPinCode(action) {
  yield put(loadApplicantDetailsInit());
  const { pinCode, index } = action.payload;
  const creditData = yield select(makeSelectApplicantDetails());
  const applicantsData = yield select(makeSelectApplicantDetails2());
  const { citiesByPincodes } = applicantsData;
  let pincodeToSearch = creditData.entity.contactDetails.pincode;
  if (action && action.payload && action.payload.pinCode) {
    pincodeToSearch = pinCode;
  }
  try {
    const result = yield fetch(
      `https://pl-api.clix.capital/master/location/zipcode/v1/search/${pincodeToSearch}`,
      {
        headers: env.headers,
        method: 'GET',
      },
    );
    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.success || responseBody.status) {
        if (index) {
          const newCitiesByPincodes = [...citiesByPincodes];
          newCitiesByPincodes[index] = responseBody.data;
          yield put(loadCitiesByPinCodesSuccess(newCitiesByPincodes));
        } else {
          yield put(loadCityByPinCodeSuccess(responseBody.data));
        }
      } else {
        yield put(
          loadEntityDetailsError(
            'Oops! No pending entity is found for the given app Id:( ',
            'info',
          ),
        );
      }
    } else {
      yield put(
        loadEntityDetailsError(
          // `HTTP Status${result.status} occurred.Try Again`,
          'City could not be fetched by pincode due to some API Issue, Thanks for the patience!',
          'info',
        ),
      );
    }
  } catch (err) {
    yield put(
      loadEntityDetailsError(
        'City could not be fetched by pincode due to some API Issue, Thanks for the patience!',
        'info',
      ),
    );
  }
}

function* fetchMultipleCitiesByPinCodes() {
  yield put(loadApplicantDetailsInit());
  const creditData = yield select(makeSelectApplicantDetails());
  const applicantsDataArray = creditData.applicantsData;
  const pincodes = [];
  applicantsDataArray.forEach((element, i) => {
    if (element.contactDetails && element.contactDetails.pincode) {
      pincodes[i] = element.contactDetails.pincode;
    } else {
      pincodes[i] = 0;
    }
  });

  try {
    const results = yield all(
      pincodes.map(pincode =>
        call(
          fetch,
          `https://pl-api.clix.capital/master/location/zipcode/v1/search/${pincode}`,
          {
            headers: env.headers,
            method: 'GET',
          },
        ),
      ),
    );

    const validResults = yield all(
      results.map(result => {
        if (result.status === 200) {
          return call([result, 'json']);
        }
        return '';
      }),
    );

    const resToSend = [];
    validResults.forEach(responseBody => {
      if (responseBody.success || responseBody.status) {
        resToSend.push(responseBody.data);
      }
    });
    yield put(loadCitiesByPinCodesSuccess(resToSend));
  } catch (err) {
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
  }
}

function* unlinkGuarantor(action) {
  // if the applicant added is new
  if (String(action.payload.id).includes('NEW')) return;
  yield put(loadApplicantDetailsInit());
  const appData = yield select(makeSelectApplicantDetails2());
  const { entity: { users = [] } = {} } = appData;
  const user = _.find(users, { id: action.payload.id });
  const userToUpdate = [
    {
      id: action.payload.id,
      cuid: user.cuid,
      userLinked: action.payload.link,
    },
  ];
  try {
    const result = yield updateCompositeApplication({
      users: userToUpdate,
      appId: JSON.parse(sessionStorage.getItem('id')),
    });
    const appServiceResponse = _.get(result, 'data[1].appServiceResponse');
    if (_.get(appServiceResponse, 'body.success')) {
      yield put(loadUpdateAppSuccess('success'));
      message.info(
        `Applicant ${
          action.payload.link ? 'Linked' : 'Unlinked'
        } Successfully!`,
      );
    } else {
      const errorResponse = yield result.json();
      message.error(
        errorResponse.message ||
          `HTTP Status${result.status} occurred.Try Again`,
      );
    }
  } catch (err) {
    message.error('Network error occurred! Check connection!');
  }
  yield put(loadApplicantDetailsEnd());
}

function* unlinkApplicant(action) {
  // if the applicant added is new
  if (String(action.payload.id).includes('NEW')) return;
  yield put(loadApplicantDetailsInit());
  const appData = yield select(makeSelectApplicantDetails2());
  const { entity: { users = [] } = {} } = appData;
  const user = _.find(users, { id: action.payload.id });
  const userToUpdate = [
    {
      id: action.payload.id,
      cuid: user.cuid,
      userLinked: action.payload.link,
    },
  ];
  try {
    const result = yield updateCompositeApplication({
      users: userToUpdate,
      appId: JSON.parse(sessionStorage.getItem('id')),
    });
    const appServiceResponse = _.get(result, 'data[1].appServiceResponse');
    if (_.get(appServiceResponse, 'body.success')) {
      yield put(loadUpdateAppSuccess('success'));
      message.info(
        `Applicant ${
          action.payload.link ? 'Linked' : 'Unlinked'
        } Successfully!`,
      );
    } else {
      const errorResponse = yield result.json();
      message.error(
        errorResponse.message ||
          `HTTP Status${result.status} occurred.Try Again`,
      );
    }
  } catch (err) {
    message.error('Network error occurred! Check connection!');
  }
  yield put(loadApplicantDetailsEnd());
}

export function* updateEntityOff(users, entityOfficers, cuid, id, data) {
  const usersArr = users.concat({ cuid, entityOfficers, id });
  let additionData = {};
  const {
    entity: { additionalData = {} },
  } = yield select(makeAppDocumentDetailsData());
  const cibilScores =
    additionalData &&
    additionalData.data &&
    additionalData.data.extraDataField &&
    additionalData.data.extraDataField.consumerCibilScore
      ? JSON.parse(additionalData.data.extraDataField.consumerCibilScore)
      : {};
  usersArr.forEach(item => {
    cibilScores[item.cuid] = data.consumerCibilScore;
  });
  additionData = {
    data: {
      extraDataField: {
        consumerCibilScore: JSON.stringify({ ...cibilScores }),
        foir: data.foir,
      },
    },
  };
  const response = yield call(
    request,
    `${env.API_URL}/profiles/update/details`,
    {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify({
        users: usersArr,
        appId: JSON.parse(sessionStorage.getItem('id')),
        additionalData: additionData,
      }),
    },
  );
  return response;
}

export function* updateApp(data, isGuarantor) {
  const users = [
    {
      ...data.applicants[0],
      type: 'INDIVIDUAL',
      dateOfBirthIncorporation: revertFormatDate(
        data.applicants[0].dateOfBirthIncorporation,
      ),
      entityOfficers: [],
      id: String(data.applicants[0].id).includes('NEW')
        ? ''
        : data.applicants[0].id,
      cuid: String(data.applicants[0].id).includes('NEW')
        ? ''
        : data.applicants[0].cuid,
    },
  ];
  debugger;
  const response = yield call(
    request,
    `${env.API_URL}/profiles/update/details`,
    {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify({
        users,
        appId: JSON.parse(sessionStorage.getItem('id')),
        // additionalData: additionData,
      }),
    },
  );
  return response;
}

export function* updateApplicantEntity(entityUsers, cuid, actionToDo) {
  let users = [];
  if (actionToDo === 'UpdateEntityOff') {
    users = _.map(entityUsers, user => ({
      entityOfficers: user.entityOfficers,
      cuid: user.cuid,
      id: user.id,
    }));
  } else {
    users = _.map(_.get(entityUsers, 'users', []), user => ({
      ...user,
      type: 'COMPANY',
      id: String(user.id).includes('NEW') ? '' : user.id,
      dateOfBirthIncorporation: moment(user.dateOfBirthIncorporation).format(
        'YYYY-MM-DD',
      ),
      // appLMS: { role: 'Applicant' },
      cuid: String(user.id).includes('NEW') ? '' : cuid,
    }));
    if (String(users[0].id).includes('NEW') || !users[0].id) {
      users[0].userDetails = {
        ...users[0].userDetails,
      };
      users[0].appLMS = {};
      users[0].appLMS.role = 'Co-Applicant';
    }
  }
  const response = yield call(
    request,
    `${env.API_URL}/profiles/update/details`,
    {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify({
        // appGsts: _.get(entityUsers, 'appGsts', []),
        additionalData: _.get(entityUsers, 'additionalData', {}),
        users,
        appId: JSON.parse(sessionStorage.getItem('id')),
      }),
    },
  );
  return response;
}

export function* updateCoApplicants(action) {
  const { data, entityCuid, isGuarantor } = action.payload;
  yield put(loadApplicantDetailsInit());
  try {
    let result = {};
    result = yield call(updateApp, data, isGuarantor);
    const appServiceResponse = _.get(result, 'data[1].appServiceResponse');
    const userServiceResponse = _.get(result, 'data[0].userServiceResponse');
    if (_.get(appServiceResponse, 'body.success')) {
      const newCuid = _.get(userServiceResponse, '[0].body.cuid')
        ? _.get(userServiceResponse, '[0].body.cuid')
        : data.applicants[0].cuid;
      const newEntity = _.get(appServiceResponse, 'body.app.users', []).find(
        user => user.cuid === newCuid,
      );
      const primaryEntity = _.get(
        appServiceResponse,
        'body.app.users',
        [],
      ).find(user => user.cuid === entityCuid);
      if (entityCuid && primaryEntity) {
        debugger;
        const users = [];
        users.push({
          id: primaryEntity.id,
          cuid: primaryEntity.cuid,
          entityOfficers: [
            {
              designation:
                data.applicants[0].entityOfficers &&
                data.applicants[0].entityOfficers.designation
                  ? data.applicants[0].entityOfficers.designation
                  : '',
              belongsTo: newCuid,
            },
          ],
        });
        const entityOfficers = [
          {
            designation:
              data.applicants[0].entityOfficers &&
              data.applicants[0].entityOfficers.designation
                ? data.applicants[0].entityOfficers.designation
                : '',
            belongsTo: entityCuid,
          },
        ];
        const cuid = newCuid;
        const id = String(data.applicants[0].id).includes('NEW')
          ? newEntity.id || ''
          : data.applicants[0].id;
        const result = yield call(
          updateEntityOff,
          users,
          entityOfficers,
          cuid,
          id,
          data,
        );
        const appServiceResponse = _.get(result, 'data[1].appServiceResponse');
        if (_.get(appServiceResponse, 'body.success')) {
          action.payload.reset(action.payload.values);
          yield put(loadUpdateAppSuccess('Success'));
        } else {
          yield put(
            loadEntityDetailsError(
              'Oops! No pending entity is found for the given app Id:( ',
              'info',
            ),
          );
        }
      } else {
        const entityOfficers = [];
        const cuid = newCuid;
        const id = String(data.applicants[0].id).includes('NEW')
          ? newEntity.id || ''
          : data.applicants[0].id;
        const result = yield call(
          updateEntityOff,
          [],
          entityOfficers,
          cuid,
          id,
          data,
        );
        const appServiceResponse = _.get(result, 'data[1].appServiceResponse');
        if (_.get(appServiceResponse, 'body.success')) {
          action.payload.reset(action.payload.values);
          yield put(loadUpdateAppSuccess('Success'));
        } else {
          yield put(
            loadEntityDetailsError(
              'Oops! No pending entity is found for the given app Id:( ',
              'info',
            ),
          );
        }
      }
    }
  } catch (err) {}
}

export function* updateUserService(action) {
  // yield put(updateApplicantDetailsInit());
  const { data, userData, cuid } = action;
  try {
    // let newUsers = [{ cuid: 1000014423 }];
    let newCuid = '';
    let createResult = {};
    let primaryEntity = {};
    // newUsers = yield call(createUsers, data.users);
    createResult = yield call(
      updateApplicantEntity,
      data,
      userData.cuid,
      'CreateCoEntity',
    );
    const serviceResponses = {};
    _.get(createResult, 'data', []).forEach(key => {
      Object.keys(key).forEach(innerKey => {
        serviceResponses[innerKey] = key[innerKey];
      });
    });
    const appServiceResponse = _.get(serviceResponses, 'appServiceResponse');
    const userServiceResponse = _.get(serviceResponses, 'userServiceResponse');
    if (_.get(appServiceResponse, 'body.success')) {
      newCuid = _.get(userServiceResponse, '[0].body.cuid');
      const newUser = _.get(appServiceResponse, 'body.app.users', []).find(
        user => user.cuid === newCuid,
      );
      primaryEntity = _.get(appServiceResponse, 'body.app.users', []).find(
        user => user.cuid === cuid,
      );
      if (_.get(newUser, 'id')) data.users[0].id = _.get(newUser, 'id');
      else if (!_.get(newUser, 'id') && String(userData.id).includes('NEW')) {
        message.info('Application not updated Successfully!');
        yield put(loadUpdateAppSuccess('Success'));
        return;
      }
      message.info('Application updated Successfully!');
      yield put(loadUpdateAppSuccess('Success'));
    } else {
      message.info('Application could not be updated! :(');
      yield put(loadEntityDetailsError('Application could not be updated! :('));
      return;
    }

    if (String(userData.id).includes('NEW')) {
      if (cuid) {
        data.users[0].entityOfficers = [
          {
            designation:
              userData.entityOfficers && userData.entityOfficers.length > 0
                ? userData.entityOfficers[0].designation
                : '',
            belongsTo: cuid,
          },
        ];
        data.users[0].cuid = newCuid;
      } else {
        data.users[0].entityOfficers = [
          {
            designation:
              userData.entityOfficers && userData.entityOfficers.length > 0
                ? userData.entityOfficers[0].designation
                : '',
            belongsTo: newCuid,
          },
        ];
        data.users[0].cuid = newCuid;
      }
    } else {
      data.users[0].entityOfficers = [
        {
          designation:
            userData.entityOfficers && userData.entityOfficers.length > 0
              ? userData.entityOfficers[0].designation
              : '',
          belongsTo: cuid,
        },
      ];
      data.users[0].cuid = userData.cuid;
    }
    if (_.get(userData, 'id', '') !== _.get(primaryEntity, 'id', ''))
      data.users.push({
        cuid,
        id: primaryEntity.id,
        entityOfficers: [
          {
            designation:
              userData.entityOfficers && userData.entityOfficers.length > 0
                ? userData.entityOfficers[0].designation
                : '',
            belongsTo: userData.cuid || newCuid,
            id:
              userData.entityOfficers && userData.entityOfficers.length > 0
                ? userData.entityOfficers[0].id
                : '',
          },
        ],
      });

    data.users.push({
      cuid,
      id: primaryEntity.id,
      entityOfficers: [
        {
          designation:
            userData.entityOfficers && userData.entityOfficers.length > 0
              ? userData.entityOfficers[0].designation
              : '',
          belongsTo: userData.cuid || newCuid,
          id:
            userData.entityOfficers && userData.entityOfficers.length > 0
              ? userData.entityOfficers[0].id
              : '',
        },
      ],
    });

    yield call(updateApplicantEntity, data.users, cuid, 'UpdateEntityOff');
    yield put(loadUpdateAppSuccess('Success'));
  } catch (error) {
    message.error('Network error occurred! Check connection!');
    yield put(
      loadEntityDetailsError('Network error occurred! Check connection!'),
    );
  }
}

export function* fetchRisk({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=RISKLEVEL_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchRiskLevelSuccess(response.masterData));
    } else {
      yield put(fetchRiskLevelError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchRiskLevelError('Master Data not found'));
  }
}

export function* fetchPriority({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=CUSTPRIORITY_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchPrioritySuccess(response.masterData));
    } else {
      yield put(fetchPriorityError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchPriorityError('Master Data not found'));
  }
}

export function* fetchCategory({ partner, product }) {
  const requestURL = `${env.MASTER_URL}?masterType=CIBIL_ACTIVITY_CODE_MASTER`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchCategorySuccess(response.masterData));
    } else {
      yield put(fetchCategoryError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchCategoryError('Master Data not found'));
  }
}

export function* fetchMasterData(partner, product, masterType) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=${masterType}_${partner}_${product}`;
  return yield call(request, requestURL, {
    headers: env.headers,
  });
}

export function* fetchMasterDataGeneric({ partner, product, masterType }) {
  try {
    const response = yield call(fetchMasterData, partner, product, masterType);
    if (response && response.success) {
      yield put(fetchMasterDataGenericSuccess(response.masterData, masterType));
    } else {
      yield put(
        fetchMasterDataGenericError('Master Data not found', masterType),
      );
    }
  } catch (err) {
    yield put(fetchMasterDataGenericError('Master Data not found', masterType));
  }
}

function* fetchDelphiOutputSaga() {
  try {
    const url = `${env.DELPHI_URL}/v2/latest/response?appid=${JSON.parse(
      sessionStorage.getItem('id'),
    )}`;
    const response = yield call(request, url, {
      method: 'GET',
    });

    // const response = delphiOp;
    if (response.status) {
      yield put(loadDelphiOutputSuccess(response));
    } else {
      yield put(
        loadDeplhiOutputError(
          response.reason ||
            `No Delphi Output data for ${JSON.parse(
              sessionStorage.getItem('id'),
            )} `,
        ),
      );
      message.info(
        response.reason ||
          `No Delphi Output data for ${JSON.parse(
            sessionStorage.getItem('id'),
          )}!`,
      );
    }
  } catch (err) {
    yield put(
      loadDeplhiOutputError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

export default function* creditPortalSaga() {
  yield takeEvery(actions.LOAD_ENTITY_DETAILS, fetchEntityDetailsSaga);
  yield takeEvery(actions.LOAD_ENTITY_DETAILS, fetchIndustrySaga);
  yield takeEvery(actions.LOAD_UPDATE_APP_SUCCESS, fetchEntityDetailsSaga);
  // yield takeEvery(actions.LOAD_ENTITY_DETAILS_SUCCESS, fetchCityByPinCode);
  // yield takeEvery(actions.LOAD_ENTITY_DETAILS_SUCCESS, fetchGstByPan);

  // yield takeEvery(actions.LOAD_CITY_BY_PINCODE, fetchCityByPinCode);
  // yield takeEvery(actions.UPDATE_MULTIPLE_ENTITY_DETAILS, updateMultipleEntities);
  yield takeEvery(actions.UPDATE_ENTITY_DETAILS, updateEntityDetails);
  yield takeEvery(actions.GENERATE_CIBIL_REPORT, generateCibilReport);
  yield takeEvery(actions.GENERATE_DELPHI_REPORT, generateDelphiReport);
  yield takeEvery(actions.UPDATE_CO_APPLICANTS, updateCoApplicants);
  yield takeEvery(actions.UPDATE_USER_REQUEST, updateUserDetails);
  yield takeEvery(actions.UNLINK_APPLICANT, unlinkApplicant);
  yield takeEvery(actions.UNLINK_GUARANTOR, unlinkGuarantor);
  yield takeEvery(actions.UPDATE_USERS, updateUserService);
  yield takeEvery(actions.FETCH_PRIORITY, fetchPriority);
  yield takeEvery(actions.FETCH_CATEGORY, fetchCategory);
  yield takeEvery(actions.FETCH_RISK_LEVEL, fetchRisk);
  yield takeEvery(actions.FETCH_MASTER_DATA_GENERIC, fetchMasterDataGeneric);
  yield takeEvery(actions.CALL_COMMERCIAL_BUREAU, generateCommercialBureau);
  yield takeEvery(actions.LOAD_DELPHI_OUTPUT, fetchDelphiOutputSaga);
}
