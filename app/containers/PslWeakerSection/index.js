/**
 *
 * PslEndUse
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import InputField from 'components/InputField';
import { Controller } from 'react-hook-form';
import { Select, Form } from 'antd';
import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPslWeakerSection from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchPslWeakerSection } from './actions';

const { Option } = Select;

export function PslWeakerSection({
  pslWeakerSection,
  getPslWeakerSection,
  values,
  errors,
  setError,
  clearError,
  setValue,
  name,
  ...rest
}) {
  useInjectReducer({ key: 'pslWeakerSection', reducer });
  useInjectSaga({ key: 'pslWeakerSection', saga });

  useEffect(() => {
    getPslWeakerSection();
  }, []);

  const { response, error } = pslWeakerSection;

  return (
    <Form.Item label="PSL Weaker Section">
      <Controller
        name={name}
        rules={{
          required: {
            value: false,
            message: 'This field cannot be left empty',
          },
        }}
        showSearch
        onChange={([value]) => value}
        {...rest}
        as={
          <Select size="large" placeholder="PSL Weaker Section">
            {response &&
              response.length > 0 &&
              response.map(item => (
                <Option value={item.code}>{item.value}</Option>
              ))}
          </Select>
        }
      />
      <ErrorMessage name={name} errors={errors} />
    </Form.Item>
  );
}

PslWeakerSection.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pslWeakerSection: makeSelectPslWeakerSection(),
});

function mapDispatchToProps(dispatch) {
  return {
    getPslWeakerSection: () => dispatch(fetchPslWeakerSection()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PslWeakerSection);
