/*
 *
 * PslWeakerSection actions
 *
 */

import {
  FETCH_PSL_WEAKER_SECTION,
  FETCH_PSL_WEAKER_SECTION_SUCCESS,
  FETCH_PSL_WEAKER_SECTION_FAILED,
} from './constants';

export function fetchPslWeakerSection() {
  return {
    type: FETCH_PSL_WEAKER_SECTION,
  };
}

export function pslWeakerSectionFetched(response) {
  return {
    type: FETCH_PSL_WEAKER_SECTION_SUCCESS,
    response,
  };
}

export function pslWeakerSectionError(error) {
  return {
    type: FETCH_PSL_WEAKER_SECTION_FAILED,
    error,
  };
}
