/*
 *
 * PslWeakerSection constants
 *
 */

export const FETCH_PSL_WEAKER_SECTION =
  'app/PslWeakerSection/FETCH_PSL_WEAKER_SECTION';
export const FETCH_PSL_WEAKER_SECTION_SUCCESS =
  'app/PslWeakerSection/FETCH_PSL_WEAKER_SECTION_SUCCESS';
export const FETCH_PSL_WEAKER_SECTION_FAILED =
  'app/PslWeakerSection/FETCH_PSL_WEAKER_SECTION_FAILED';
