import { call, put, takeEvery } from 'redux-saga/effects';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import { FETCH_PSL_WEAKER_SECTION } from './constants';
import { pslWeakerSectionFetched, pslWeakerSectionError } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* fetchPslWeakerSection() {
  const requestURL = `${env.MASTER_DATA_URL}/pslweakersection`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });

    if (response && response.length > 0) {
      yield put(pslWeakerSectionFetched(response));
    } else {
      yield put(pslWeakerSectionError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(pslWeakerSectionError('Master Data not found'));
    message.error(
      (error && error.message) || 'Master Data for PSL End Use not found',
    );
  }
}

export default function* pslWeakerSectionSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_PSL_WEAKER_SECTION, fetchPslWeakerSection);
}
