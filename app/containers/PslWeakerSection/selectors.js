import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pslWeakerSection state domain
 */

const selectPslWeakerSectionDomain = state =>
  state.pslWeakerSection || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by pslWeakerSection
 */

const makeSelectPslWeakerSectionInput = () =>
  createSelector(
    selectPslWeakerSectionDomain,
    substate => substate,
  );

export default makeSelectPslWeakerSectionInput;
export { selectPslWeakerSectionDomain };
