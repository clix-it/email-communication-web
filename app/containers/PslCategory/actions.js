/*
 *
 * PslCategory actions
 *
 */

import {
  FETCH_PSL_CATEGORY,
  FETCH_PSL_CATEGORY_FAILED,
  FETCH_PSL_CATEGORY_SUCCESS,
} from './constants';

export function fetchPslCategory() {
  return {
    type: FETCH_PSL_CATEGORY,
  };
}

export function pslCategoryFetched(response) {
  return {
    type: FETCH_PSL_CATEGORY_SUCCESS,
    response,
  };
}

export function pslCategoryError(error) {
  return {
    type: FETCH_PSL_CATEGORY_FAILED,
    error,
  };
}
