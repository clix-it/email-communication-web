/*
 *
 * PslCategory constants
 *
 */

export const FETCH_PSL_CATEGORY = 'app/PslCategory/FETCH_PSL_CATEGORY';
export const FETCH_PSL_CATEGORY_SUCCESS =
  'app/PslCategory/FETCH_PSL_CATEGORY_SUCCESS';
export const FETCH_PSL_CATEGORY_FAILED =
  'app/PslCategory/FETCH_PSL_CATEGORY_FAILED';
