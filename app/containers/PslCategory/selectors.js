import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pslCategory state domain
 */

const selectPslCategoryDomain = state => state.pslCategory || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by pslCategory
 */

const makeSelectPslCategoryInput = () =>
  createSelector(
    selectPslCategoryDomain,
    substate => substate,
  );

export default makeSelectPslCategoryInput;
export { selectPslCategoryDomain };
