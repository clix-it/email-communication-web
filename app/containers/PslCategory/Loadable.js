/**
 *
 * Asynchronously loads the component for PslCategory
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
