/**
 *
 * PslCategory
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import InputField from 'components/InputField';
import { Controller } from 'react-hook-form';
import { Select, Form } from 'antd';
import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPslCategory from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { fetchPslCategory } from './actions';

const { Option } = Select;

export function PslCategory({
  pslCategory,
  getPslCategory,
  values,
  errors,
  setError,
  clearError,
  setValue,
  name,
  ...rest
}) {
  useInjectReducer({ key: 'pslCategory', reducer });
  useInjectSaga({ key: 'pslCategory', saga });

  useEffect(() => {
    getPslCategory();
  }, []);

  const { response = [], error } = pslCategory;

  return (
    <Form.Item label="PSL Category">
      <Controller
        name={name}
        rules={{
          required: {
            value: false,
            message: 'This field cannot be left empty',
          },
        }}
        showSearch
        onChange={([value]) => value}
        {...rest}
        as={
          <Select size="large" placeholder="PSL Category">
            {response &&
              response.length > 0 &&
              response.map(item => (
                <Option value={item.code}>{item.value}</Option>
              ))}
          </Select>
        }
      />
      <ErrorMessage name={name} errors={errors} />
    </Form.Item>
  );
}

PslCategory.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pslCategory: makeSelectPslCategory(),
});

function mapDispatchToProps(dispatch) {
  return {
    getPslCategory: () => dispatch(fetchPslCategory()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PslCategory);
