import * as actions from './constants';
export function searchCustomer(value) {
  return {
    type: actions.SEARCH_CUSTOMER,
    value,
  };
}

export function searchCustomerSuccess(users) {
  return {
    type: actions.SEARCH_CUSTOMER_SUCCESS,
    users,
  };
}

export function searchCustomerError(message) {
  return {
    type: actions.SEARCH_CUSTOMER_ERROR,
    message,
  };
}

export function searchCustomerInit() {
  return {
    type: actions.SEARCH_CUSTOMER_INIT,
  };
}

export function setCurrentUser(app) {
  return {
    type: actions.SET_CURRENT_USER,
    app,
  };
}
