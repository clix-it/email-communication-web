/**
 *Credit Portal
 */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectCustomerDetails } from './selectors';

import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import { searchCustomer, setCurrentUser } from './actions';

import CustomerSearchPage from '../../components/CustomerSearchPage';

export function CustomerDetailsPortal({ dispatch, CustomerDetails }) {
  const { loading = false, error, users } = CustomerDetails;
  return (
    <CustomerSearchPage
      users={users}
      error={error}
      dispatch={dispatch}
      loading={loading}
      setCurrentUser={setCurrentUser}
      searchCustomer={searchCustomer}
    />
  );
}

CustomerDetailsPortal.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  CustomerDetails: makeSelectCustomerDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

CustomerDetailsPortal.propTypes = {
  CustomerDetails: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
};

CustomerDetailsPortal.defaultProps = {
  CustomerDetailsPortal: {},
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'customerDetailsPortal', reducer });

const withSaga = useInjectSaga({ key: 'customerDetailsPortal', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(CustomerDetailsPortal);
