/*
 *
 * LeadDocuments reducer
 *
 */
import produce from 'immer';
import * as actions from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';

export const initialState = {
  loading: false,
  users: [],
  error: false,
};

const customerDetailsPortalReducer = (state = initialState, action) =>
  /* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["draft"] }] */
  produce(state, draft => {
    switch (action.type) {
      case actions.SEARCH_CUSTOMER_INIT:
        draft.loading = true;
        draft.error = false;
        break;
      case actions.SEARCH_CUSTOMER_SUCCESS:
        draft.loading = false;
        draft.users = action.users;
        draft.error = false;
        break;
      case actions.SEARCH_CUSTOMER_ERROR:
        draft.loading = false;
        draft.users = [];
        draft.error = action.message;
        break;
      case LOCATION_CHANGE:
        draft.users = [];
        draft.loading = false;
        break;
      default: {
        return state;
      }
    }
  });

export default customerDetailsPortalReducer;
