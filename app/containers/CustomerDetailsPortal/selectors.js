import { createSelector } from 'reselect';

const selectCustomerDetailsPortal = state => state.customerDetailsPortal;

const makeSelectCustomerDetails = () =>
  createSelector(
    selectCustomerDetailsPortal,
    customerDetailsPortal => customerDetailsPortal,
  );

export { makeSelectCustomerDetails };
