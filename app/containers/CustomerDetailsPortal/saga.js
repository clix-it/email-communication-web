import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import {
  searchCustomerInit,
  searchCustomerSuccess,
  searchCustomerError,
} from './actions';

function* fetchUsersSaga({ value }) {
  yield put(searchCustomerInit());
  try {
    debugger;
    if (value != '') {
      const reqUrl = `${env.DEDUPE_SEARCH}/dedupe/profiles/?identity=${value}`;
      const options = {
        method: 'GET',
      };

      const response = yield call(request, reqUrl, options);
      if (response.status) {
        let data = [];
        response.data.forEach(item => {
          if (item && item.partialMatches && item.partialMatches.length > 0) {
            data = data.concat(item.partialMatches);
          }
          if (item && item.exactMatches && item.exactMatches.length > 0) {
            data = data.concat(item.exactMatches);
          }
          if (item && item.otherMatches && item.otherMatches.length > 0) {
            data = data.concat(item.otherMatches);
          }
        });        
        data = data.map((item, index) => {
          return {
            index: index + 1,
            cuid: item.profile.cuid,
            pan: item.profile.pan,
            DOB: item.profile.dateOfBirthIncorporation,
          };
        });
        yield put(searchCustomerSuccess(data));
      } else {
        yield put(searchCustomerError('No records available.'));
        message.error('No records available.');
      }
    } else {
      yield put(searchCustomerError('No records available.'));
    }
  } catch (err) {
    yield put(searchCustomerError('No records available.'));
    message.error('No records available.');
  }
}

export default function* customerDetailsPortalSaga() {
  yield takeEvery(actions.SEARCH_CUSTOMER, fetchUsersSaga);
}
