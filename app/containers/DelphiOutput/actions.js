import * as actions from './constants';
export function loadDelphiOutput() {
  return {
    type: actions.LOAD_DELPHI_OUTPUT,
  };
}

export function loadDelphiOutputSuccess(data) {
  return {
    type: actions.LOAD_DELPHI_OUTPUT_SUCCESS,
    payload: {
      data,
    },
  };
}

export function loadDeplhiOutputError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_DELPHI_OUTPUT_ERROR,
    payload: {
      errorMessage,
      severity,
    },
  };
}

export function reRunDelphi(appId) {
  return {
    type: actions.RERUN_DELPHI,
    appId,
  };
}

export function reRunDelphiSuccess(data) {
  return {
    type: actions.RERUN_DELPHI_SUCCESS,
    data,
  };
}

export function reRunDelphiError(message) {
  return {
    type: actions.RERUN_DELPHI_ERROR,
    message,
  };
}
