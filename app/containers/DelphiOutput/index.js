import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';
import { createStructuredSelector } from 'reselect';
import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import styled from 'styled-components';
import { Spin, Button } from 'antd';
import { loadDelphiOutput, reRunDelphi } from './actions';
import reducer from './reducer';
import saga from './saga';
import { makeSelectDelphiOutput } from './selectors';
import Tabs from '../../components/Tabs';
import DelphiWithPrime from '../../components/DelphiWithPrime';

function DelphiOutput({
  dispatch,
  delphiOutput,
  activity,
  applicantDetails,
  requestType,
}) {
  const [tabPanel, setTabPanel] = useState([]);
  const { delphiData = {}, loading = false } = delphiOutput;

  useEffect(() => {
    const appId = JSON.parse(sessionStorage.getItem('id')) || '';
    const displayData = delphiData[appId];
    const panel = [];
    if (displayData) {
      displayData.map(data =>
        panel.push({
          id: `${data.stage}`,
          name: `${_.startCase(_.lowerCase(data.stage))}`,
          component: <DelphiWithPrime loading={loading} data={data} />,
        }),
      );
      setTabPanel(panel);
    }
  }, [delphiOutput]);

  useEffect(() => {
    dispatch(loadDelphiOutput());
  }, []);

  const reRun = () => {
    dispatch(reRunDelphi(JSON.parse(sessionStorage.getItem('id'))));
  };

  return (
    <Spin spinning={loading} tip="Loading...">
      {_.get(applicantDetails, 'entity.partner', '') == 'HFSAPP' &&
        (activity == 'CREDIT_PSV' || activity == 'CREDIT_REVIEW') && (
          <Button
            type="primary"
            onClick={() => reRun()}
            disabled={requestType != 'PMA'}
          >
            Re-run Delphi
          </Button>
        )}
      {tabPanel.length > 0 ? (
        <Tabs mode="top" tabPanel={tabPanel} />
      ) : (
        <NoData>No Deplhi Output Data Available!</NoData>
      )}
    </Spin>
  );
}

const NoData = styled.div`
  text-align: center;
  font-size: large;
  font-weight: bold;
`;

DelphiOutput.propTypes = {
  dispatch: PropTypes.func.isRequired,
  delphiOutput: PropTypes.object,
};

DelphiOutput.defaultProps = {
  delphiOutput: {},
};

const withReducer = useInjectReducer({
  key: 'delphiOutput',
  reducer,
});
const withSaga = useInjectSaga({ key: 'delphiOutput', saga });

const mapStateToProps = createStructuredSelector({
  delphiOutput: makeSelectDelphiOutput(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(DelphiOutput);
