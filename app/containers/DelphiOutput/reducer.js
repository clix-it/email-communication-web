/**
 * Credit Portal Reducer
 */
import * as actions from './constants';

export const initialState = {
  loading: true,
  delphiData: [],
  error: {},
  rerunStatus: false,
};

const delphiReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOAD_DELPHI_OUTPUT:
      return { ...state, loading: true };

    case actions.LOAD_DELPHI_OUTPUT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: { status: false, message: '' },
        delphiData: action.payload.data,
      };

    case actions.LOAD_DELPHI_OUTPUT_ERROR: {
      return {
        ...state,
        delphiData: {},
        loading: false,
        error: {
          status: true,
          message: action.payload.errorMessage,
          severity: action.severity,
        },
      };
    }
    case actions.RERUN_DELPHI:
      return { ...state, loading: true, rerunStatus: false };
    case actions.RERUN_DELPHI_SUCCESS:
      return {
        ...state,
        loading: false,
        error: { status: false, message: '' },
        rerunStatus: true,
      };
    case actions.RERUN_DELPHI_ERROR: {
      return {
        ...state,
        rerunStatus: false,
        loading: false,
      };
    }
    default: {
      return state;
    }
  }
};

export default delphiReducer;
