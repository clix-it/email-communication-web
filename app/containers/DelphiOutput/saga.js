import { takeEvery, put, call, takeLatest } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import {
  loadDelphiOutputSuccess,
  loadDeplhiOutputError,
  reRunDelphiSuccess,
  reRunDelphiError,
} from './actions';
import data from './data.json';

function* fetchDelphiOutputSaga() {
  try {
    const url = `${env.DELPHI_URL}/v2/latest/response?appid=${JSON.parse(
      sessionStorage.getItem('id'),
    )}`;
    const response = yield call(request, url, {
      method: 'GET',
    });
    // const response = data;
    if (response.status) {
      yield put(loadDelphiOutputSuccess(response));
    } else {
      yield put(
        loadDeplhiOutputError(
          response.reason ||
            `No Delphi Output data for ${JSON.parse(
              sessionStorage.getItem('id'),
            )} `,
        ),
      );
      message.info(
        response.reason ||
          `No Delphi Output data for ${JSON.parse(
            sessionStorage.getItem('id'),
          )}!`,
      );
    }
  } catch (err) {
    yield put(
      loadDeplhiOutputError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

function* rerunDelphi({ appId }) {
  try {
    const url = `${env.API_URL}/profiles/delphi`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        applicationId: appId,
        perfiosResponse: '',
        callbackUrl: '',
      }),
    };
    const response = yield call(request, url, options);
    // const response = data;
    if (response.mergeResult) {
      yield put(reRunDelphiSuccess());
      message.success('Delphi rerun success');
    }
  } catch (err) {
    yield put(reRunDelphiError(err.message));
    message.error(err.message || 'Some error occured!');
  }
}

export default function* delphiOutputSaga() {
  yield takeEvery(actions.LOAD_DELPHI_OUTPUT, fetchDelphiOutputSaga);
  yield takeEvery(actions.RERUN_DELPHI, rerunDelphi);
}
