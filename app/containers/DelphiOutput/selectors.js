import { createSelector } from 'reselect';

const selectDelphiOutput = state => state.delphiOutput;

const makeSelectDelphiOutput = () =>
  createSelector(
    selectDelphiOutput,
    delphiOutput => delphiOutput,
  );

export { makeSelectDelphiOutput };
