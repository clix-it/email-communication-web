export const UPDATE_ADDRESS = 'UPDATE_ADDRESS';
export const UPDATE_ADDRESS_SUCCESS = 'UPDATE_ADDRESS_SUCCESS';
export const UPDATE_ADDRESS_FAILED = 'UPDATE_ADDRESS_FAILED';

export const colOneThird = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 5,
  xl: { span: 15, offset: 20, pull: 2 },
};

export const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};
