import { takeEvery, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import { message } from 'antd';
import env from '../../../environment';
import * as actions from './constants';
import { updateAddressSuccess, updateAddressFailed } from './actions';
import { updateCompositeApplication } from '../App/saga';
import _ from 'lodash';

export function* updateAddressDetails(action) {
  // yield put(updateApplicantDetailsInit());
  try {
    const response = yield updateCompositeApplication({
      ...action.data,
      appId: JSON.parse(sessionStorage.getItem('id'))
    });
    const appServiceResponse = _.get(response, 'data[1].appServiceResponse');
    if (_.get(appServiceResponse, 'body.success')) {
      message.info('Address updated Successfully!');
      yield put(updateAddressSuccess(response));
    } else {
      yield put(updateAddressFailed(response));
      message.info('Address could not be updated! :(');
    }
  } catch (error) {
    yield put(updateAddressFailed('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}

export default function* addressSaga() {
  yield takeEvery(actions.UPDATE_ADDRESS, updateAddressDetails);
}
