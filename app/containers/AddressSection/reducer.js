import produce from 'immer';
import * as actions from './constants';

const initialState = {
  error: {},
  success: false,
  loading: false,
};

/* eslint-disable default-case, no-param-reassign */
const addressReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.UPDATE_ADDRESS:
        draft.loading = true;
        draft.response = false;
        draft.error = false;
        break;
      case actions.UPDATE_ADDRESS_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        draft.error = false;
        draft.success = true;
        break;

      case actions.UPDATE_ADDRESS_FAILED:
        draft.loading = false;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default addressReducer;
