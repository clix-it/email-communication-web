import _ from 'lodash';
import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { Row, Col, Button, Spin, Collapse } from 'antd';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import InputField from 'components/InputField';
import moment from 'moment';
import { EditOutlined } from '@ant-design/icons';
import OwnerShipType from 'containers/OwnerShipType';
import Pincode from 'containers/Pincode';
import { useLocation } from 'react-router-dom';
import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';
import { updateAddress } from './actions';
import makeSelectAddressSection from './selectors';
import { responsiveColumns, colOneThird } from './constants';
import { loadEntityDetails } from '../ApplicantDetails/actions';

const { Panel } = Collapse;

function AddressSection({
  dispatch,
  contactArr,
  contactType,
  index,
  applicantDetails,
  addressSection,
  cuid,
  userId,
  appId,
  isGuarantor,
  partner,
}) {
  const {
    selectedApplicant = {},
    selectedEntity = {},
    selectedGuarantor = {},
  } = applicantDetails;
  const { loading, success = false } = addressSection;

  const defaultValues = {
    'users[0].contactibilities[0].id': _.get(
      contactArr,
      `[${contactType}][0].id`,
      '',
    ),
    'users[0].contactibilities[0].pincode': _.get(
      contactArr,
      `[${contactType}][0].pincode`,
      '',
    ),
    'users[0].contactibilities[0].addressLine1': _.get(
      contactArr,
      `[${contactType}][0].addressLine1`,
      '',
    ),
    'users[0].contactibilities[0].addressLine2': _.get(
      contactArr,
      `[${contactType}][0].addressLine2`,
      '',
    ),
    'users[0].contactibilities[0].addressLine3': _.get(
      contactArr,
      `[${contactType}][0].addressLine3`,
      '',
    ),
    'users[0].contactibilities[0].city': _.get(
      contactArr,
      `[${contactType}][0].city`,
      '',
    ),
    'users[0].contactibilities[0].state': _.get(
      contactArr,
      `[${contactType}][0].state`,
      '',
    ),
    'users[0].contactibilities[0].locality': _.get(
      contactArr,
      `[${contactType}][0].locality`,
      '',
    ),
    'users[0].contactibilities[0].landmark': _.get(
      contactArr,
      `[${contactType}][0].landmark`,
      '',
    ),
    'users[0].contactibilities[0].country': _.get(
      contactArr,
      `[${contactType}][0].country`,
      'India',
    ),
    'users[0].contactibilities[0].durationOfStayInMonths': _.get(
      contactArr,
      `[${contactType}][0].durationOfStayInMonths`,
      '',
    ),
    'users[0].contactibilities[0].ownershipType': _.get(
      contactArr,
      `[${contactType}][0].ownershipType`,
      '',
    ),
    'users[0].id': userId,
  };

  const {
    handleSubmit,
    errors,
    control,
    reset,
    watch,
    setError,
    clearError,
    getValues,
    setValue,
  } = useForm({
    mode: 'onChange',
    defaultValues,
  });

  useEffect(() => {
    reset(defaultValues);
  }, [
    _.get(selectedApplicant, 'id'),
    _.get(selectedGuarantor, 'id'),
    cuid,
    _.get(applicantDetails, 'selectedEntity'),
    userId,
  ]);

  useEffect(() => {
    if (success) {
      dispatch(loadEntityDetails(appId));
    }
  }, [success]);

  const values = watch();

  const onSubmit = data => {
    const updateData = { ...data };
    delete updateData.contactType;
    console.log('update Address', updateData);
    dispatch(updateAddress(updateData));
  };

  const [disabledObj, setDisabledObj] = useState({});

  useEffect(() => {
    if (Object.keys(contactArr) && Object.keys(contactArr).length > 0) {
      const obj = {};
      Object.keys(contactArr).forEach(ele => {
        if (
          contactArr[ele] &&
          Array.isArray(contactArr[ele]) &&
          contactArr[ele].length > 0
        ) {
          contactArr[ele].forEach(e => (obj[e.id] = true));
        }
      });
      setDisabledObj(obj);
      console.log('disabledObj', obj);
    }
  }, []);

  const editAddress = id => {
    const tempObj = { ...disabledObj };
    tempObj[id] = !disabledObj[id];
    setDisabledObj(tempObj);
  };
  const loc = useLocation();

  const isDisabled = () =>
    _.get(disabledObj, _.get(contactArr[contactType], '[0].id', ''), true);

  return (
    <Collapse>
      <Panel
        header={`${
          contactType === 'undefined' || !contactType
            ? 'Other'
            : partner == 'HFSAPP' && contactType == 'CURRENT'
            ? 'Current Residential'
            : contactType
        } Address`}
        key={_.get(contactArr[contactType], '[0].id', index)}
        extra={
          <>
            {contactArr[contactType]
              ? `Address Added On - ${moment(
                  contactArr[contactType][0].createdAt,
                ).format('DD-MM-YYYY')}`
              : ''}
          </>
        }
      >
        <Spin spinning={loading} tip="Loading...">
          <form onSubmit={handleSubmit(onSubmit)} id="saveAddressForm">
            <Row gutter={[16, 16]}>
              <Col xs={0}>
                <InputField
                  id="users[0].contactibilities[0].id"
                  control={control}
                  errors={errors}
                  // defaultValue={_.get(contactArr, `[${contactType}][0].id`, '')}
                  name="users[0].contactibilities[0].id"
                  type="hidden"
                />
                <InputField
                  id="users[0].cuid"
                  control={control}
                  errors={errors}
                  defaultValue={cuid}
                  name="users[0].cuid"
                  type="hidden"
                />
                <InputField
                  id="users[0].id"
                  control={control}
                  errors={errors}
                  name="users[0].id"
                  type="hidden"
                />
                <InputField
                  id="users[0].contactibilities[0].contactType"
                  control={control}
                  errors={errors}
                  defaultValue={contactType}
                  name="users[0].contactibilities[0].contactType"
                  type="hidden"
                />
              </Col>
            </Row>
            <Row gutter={[16, 16]}>
              <Col {...colOneThird}>
                <Button
                  type="primary"
                  onClick={() => editAddress(contactArr[contactType][0].id)}
                  icon={
                    <EditOutlined
                      style={{
                        fontSize: '16px',
                        padding: '0px 10px',
                      }}
                    />
                  }
                  disabled={
                    !loc.pathname.toLowerCase().includes('pma/') ||
                    loc.pathname.toLowerCase().includes('fraudreview') ||
                    loc.pathname.toLowerCase().includes('hunterreview')
                  }
                >
                  {isDisabled() ? 'Edit Address' : 'Cancel'}
                </Button>
                <Button
                  type="primary"
                  htmlType="button"
                  onClick={handleSubmit(onSubmit)}
                  form="saveAddressForm"
                  disabled={isDisabled()}
                  style={{ marginLeft: '10px' }}
                >
                  Save Address
                </Button>
              </Col>
            </Row>

            <Row gutter={[16, 16]}>
              <Col {...responsiveColumns}>
                <InputField
                  id="users[0].contactibilities[0].addressLine1"
                  control={control}
                  name="users[0].contactibilities[0].addressLine1"
                  type="string"
                  rules={{
                    required: {
                      value: true,
                      message: 'This field cannot be left empty',
                    },
                  }}
                  errors={errors}
                  placeholder="Address Line 1"
                  labelHtml="Address Line 1*"
                  disabled={isDisabled()}
                />
              </Col>
              <Col {...responsiveColumns}>
                <InputField
                  id="users[0].contactibilities[0].addressLine2"
                  control={control}
                  name="users[0].contactibilities[0].addressLine2"
                  type="string"
                  rules={{
                    required: {
                      value: true,
                      message: 'This field cannot be left empty',
                    },
                  }}
                  errors={errors}
                  placeholder="Address Line 2"
                  labelHtml="Address Line 2*"
                  disabled={isDisabled()}
                />
              </Col>
              <Col {...responsiveColumns}>
                <InputField
                  id="users[0].contactibilities[0].addressLine3"
                  control={control}
                  name="users[0].contactibilities[0].addressLine3"
                  type="string"
                  rules={{
                    required: {
                      value: false,
                      message: 'This field cannot be left empty',
                    },
                  }}
                  errors={errors}
                  placeholder="Address Line 3"
                  labelHtml="Address Line 3"
                  disabled={isDisabled()}
                />
              </Col>
              <Pincode
                namePrefix="users[0].contactibilities[0]"
                control={control}
                reset={reset}
                values={values}
                setValue={setValue}
                getValues={getValues}
                setError={setError}
                defaultValue={
                  contactArr[contactType] ? contactArr[contactType][0] : ''
                }
                clearError={clearError}
                pincode={_.get(
                  values,
                  'users[0].contactibilities[0].pincode',
                  '',
                )}
                type="string"
                rules={{
                  required: false,
                  message: 'This field is required',
                  pattern: {
                    value: /[1-9][0-9]{5}/i,
                    message: 'Please enter correct pincode',
                  },
                }}
                errors={errors}
                disabled={isDisabled()}
              />
              <Col {...responsiveColumns}>
                <InputField
                  type="string"
                  className="form-control"
                  control={control}
                  id="users[0].contactibilities[0].landmark"
                  name="users[0].contactibilities[0].landmark"
                  errors={errors}
                  rules={{
                    required: false,
                    message: 'This field is required',
                  }}
                  placeholder="Landmark"
                  labelHtml="Landmark"
                  disabled={isDisabled()}
                />
              </Col>
              <Col {...responsiveColumns}>
                <InputField
                  className="form-control"
                  control={control}
                  id="contactType"
                  name="contactType"
                  errors={errors}
                  type="string"
                  defaultValue={
                    !contactType ||
                    contactType === 'undefined' ||
                    contactType === 'Latest'
                      ? ''
                      : _.get(
                          contactArr,
                          `[${contactType}][0].addressBelongsTo`,
                          '',
                        )
                      ? _.get(
                          contactArr,
                          `[${contactType}][0].addressBelongsTo`,
                          '',
                        )
                      : contactType
                  }
                  rules={{
                    required: false,
                    message: 'This field is required',
                  }}
                  placeholder="Address Type"
                  labelHtml="Address Type"
                  disabled
                />
              </Col>
              <Col {...responsiveColumns}>
                <InputField
                  className="form-control"
                  control={control}
                  id="users[0].contactibilities[0].durationOfStayInMonths"
                  name="users[0].contactibilities[0].durationOfStayInMonths"
                  errors={errors}
                  type="string"
                  rules={{
                    required: false,
                    message: 'This field is required',
                  }}
                  placeholder="Period Of Stay(Months)"
                  labelHtml="Period Of Stay(Months)"
                  disabled={isDisabled()}
                />
              </Col>
              <Col {...responsiveColumns}>
                <OwnerShipType
                  id="users[0].contactibilities[0].ownershipType"
                  control={control}
                  name="users[0].contactibilities[0].ownershipType"
                  type="select"
                  rules={{
                    required: {
                      value: false,
                      message: 'This field cannot be left empty',
                    },
                  }}
                  errors={errors}
                  placeholder="Ownership Type"
                  labelHtml="Ownership Type"
                />
              </Col>
            </Row>
          </form>
        </Spin>
      </Panel>
    </Collapse>
  );
}

const mapStateToProps = createStructuredSelector({
  applicantDetails: makeSelectApplicantDetails2(),
  addressSection: makeSelectAddressSection(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

AddressSection.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'addressSection', reducer });

const withSaga = useInjectSaga({ key: 'addressSection', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(AddressSection);
