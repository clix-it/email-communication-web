import * as actions from './constants';

export function updateAddressSuccess(response) {
  return {
    type: actions.UPDATE_ADDRESS_SUCCESS,
    response,
  };
}

export function updateAddressFailed(error) {
  return {
    type: actions.UPDATE_ADDRESS_FAILED,
    error,
  };
}

export function updateAddress(data) {
  return {
    type: actions.UPDATE_ADDRESS,
    data,
  };
}
