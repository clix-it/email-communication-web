import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the address ection state domain
 */

const selectAddressSection = state => state.addressSection || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Address Section
 */

const makeSelectAddressSection = () =>
  createSelector(
    selectAddressSection,
    substate => substate,
  );

export default makeSelectAddressSection;
export { selectAddressSection };
