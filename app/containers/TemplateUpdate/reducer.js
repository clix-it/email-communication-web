/*
 *
 * BankDetails reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  UPDATE_TEMPLATE_DETAILS,
  UPDATE_TEMPLATE_DETAILS_SUCCESS,
  UPDATE_TEMPLATE_DETAILS_ERROR,
  FETCH_BANKS,
  FETCH_BANKS_FAILURE,
  FETCH_BANKS_SUCCESS,
  SET_SELECTED_BANK,
  FETCH_PENNY_DROP_DETAILS_SUCCESS,
  FETCH_PENNY_DROP_DETAILS_ERROR,
  UPDATE_EXCEL_DETAILS,
  UPDATE_EXCEL_DETAILS_SUCCESS,
  UPDATE_EXCEL_DETAILS_ERROR,
  DOWNLOAD_TEMPLATE,
  DOWNLOAD_TEMPLATE_SUCCESS,
  DOWNLOAD_TEMPLATE_ERROR,
  RESET_STATE,
} from './constants';

export const initialState = {
  response: false,
  error: false,
  payload: false,
  responseExcel: false,
};

/* eslint-disable default-case, no-param-reassign */
const updateTemplateDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case UPDATE_TEMPLATE_DETAILS:
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case UPDATE_TEMPLATE_DETAILS_SUCCESS:
        draft.error = false;
        draft.response = action.data.message;
        break;
      case UPDATE_TEMPLATE_DETAILS_ERROR:
        draft.response = false;
        draft.error = `Data not saved`;
        break;

        case DOWNLOAD_TEMPLATE:
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case DOWNLOAD_TEMPLATE_SUCCESS:
        draft.error = false;
        draft.responseExcel = action.data;
        break;
      case DOWNLOAD_TEMPLATE_ERROR:
        draft.response = false;
        draft.error = `Data not saved`;
        break;
        case RESET_STATE:
          draft.responseExcel = false;
     }  
  });
export default updateTemplateDetailsReducer;
