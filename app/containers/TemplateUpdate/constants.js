/*
 *
 * UpdateTemplate constants
 *
 */

export const DOWNLOAD_TEMPLATE = 'app/BankDetails/DOWNLOAD_TEMPLATE';
export const DOWNLOAD_TEMPLATE_SUCCESS = 'app/BankDetails/DOWNLOAD_TEMPLATE_SUCCESS';
export const DOWNLOAD_TEMPLATE_ERROR = 'app/BankDetails/DOWNLOAD_TEMPLATE_ERROR';

export const UPDATE_TEMPLATE_DETAILS = 'app/TemplateDetails/UPDATE_TEMPLATE_DETAILS';
export const UPDATE_TEMPLATE_DETAILS_SUCCESS =
  'app/TemplateDetails/UPDATE_TEMPLATE_DETAILS_SUCCESS';
export const UPDATE_TEMPLATE_DETAILS_ERROR =
  'app/TemplateDetails/UPDATE_TEMPLATE_DETAILS_ERROR';

  export const RESET_STATE = 'app/TemplateDetails/RESET_STATE';