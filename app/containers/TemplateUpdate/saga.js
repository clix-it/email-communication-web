import _ from 'lodash';
import {
  take,
  call,
  put,
  select,
  takeEvery,
  debounce,
} from 'redux-saga/effects';
import request from 'utils/request';
import {
  UPDATE_TEMPLATE_DETAILS,
  DOWNLOAD_TEMPLATE,
} from './constants';
import {
  updateTemplateDetailsSuccess,
  downloadTemplateSuccess,
} from './actions';
import env from '../../../environment';

export default function* updateTemplateDetailsSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(UPDATE_TEMPLATE_DETAILS, updateTemplate);
  yield takeEvery(DOWNLOAD_TEMPLATE, downloadTemplate);
}

function* updateTemplate({ payload }) {

console.log('Payload', payload)
  const payloadData = {
    templateId: payload.templateId,
    templateType: "Email",
    content: payload.content,
    validationMessage: payload.validationMsg,
    active: payload.active,
    remarks: payload.remarks,
    createdBy: JSON.parse(sessionStorage.getItem('userId'))
  };

  const reqUrl = `${env.ADD_TEMPLATE}`;
  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const appRes = yield call(request, reqUrl, options);
    if (appRes.success) {
      yield put(updateTemplateDetailsSuccess({message : _.get(appRes, 'body.message') || 'Template saved successfully!!'}));
    }
  } catch (error) {
    console.error(error);
  }
}

// Download Template

function* downloadTemplate({ payload }) {

  console.log('Payloadddddd', payload)
    const payloadData = {
      templateId: payload.templateId,
    };
  
    const reqUrl = `${env.DOWNLOAD_TEMPLATE}`;
    const options = {
      method: 'POST',
      body: JSON.stringify(payloadData),
      headers: env.headers,
    };
  
    try {
      const appRes = yield call(request, reqUrl, options);
      if (appRes.success) {
        yield put(downloadTemplateSuccess(appRes.data));
      }
    } catch (error) {
      console.error(error);
    }
  }
  