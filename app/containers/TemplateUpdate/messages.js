/*
 * BankDetails Messages
 *
 * This contains all the text for the BankDetails container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.BankDetails';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the BankDetails container!',
  },
});
