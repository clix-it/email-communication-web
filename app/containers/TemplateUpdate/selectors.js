import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the bankDetails state domain
 */

const selectTemplateDetailsDomain = state => state.updateTemplateDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BankDetails
 */

const makeSelectTemplateDetails = () =>
  createSelector(
    selectTemplateDetailsDomain,
    substate => substate,
  );

export default makeSelectTemplateDetails;
export { selectTemplateDetailsDomain };
