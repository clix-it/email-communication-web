/*
 *
 * UpdateTemplate actions
 *
 */

import {
  DEFAULT_ACTION,
  UPDATE_TEMPLATE_DETAILS,
  UPDATE_TEMPLATE_DETAILS_ERROR,
  UPDATE_TEMPLATE_DETAILS_SUCCESS,
  DOWNLOAD_TEMPLATE,
  DOWNLOAD_TEMPLATE_SUCCESS,
  DOWNLOAD_TEMPLATE_ERROR,
  RESET_STATE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function editTemplateDetails(payload) {
  return {
    type: UPDATE_TEMPLATE_DETAILS,
    payload,
  };
}

export function updateTemplateDetailsSuccess(data) {
  return {
    type: UPDATE_TEMPLATE_DETAILS_SUCCESS,
    data,
  };
}

export function updateTemplateDetailsError(err) {
  return {
    type: UPDATE_TEMPLATE_DETAILS_ERROR,
    data: err,
  };
}

//Download Template

export function downloadTemplate(payload) {
  return {
    type: DOWNLOAD_TEMPLATE,
    payload,
  };
}

export function downloadTemplateSuccess(data) {
  return {
    type: DOWNLOAD_TEMPLATE_SUCCESS,
    data,
  };
}

export function downloadTemplateError(err) {
  return {
    type: DOWNLOAD_TEMPLATE_ERROR,
    data: err,
  };
}

export function resetState() {
  return {
    type: RESET_STATE,
  };
}