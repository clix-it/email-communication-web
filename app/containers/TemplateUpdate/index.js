/**
 *
 * UpdateTemplate
 *
 */
import _ from 'lodash';
import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useForm, Controller } from 'react-hook-form';
import { Form, Row, Col, Button, Radio, message, Divider } from 'antd';
import makeSelectTemplateDetails from './selectors';
import reducer from './reducer';
import saga from './saga';
import { EMAIL_CATEGORY, DEFAULT_MAIL_ID } from '../../utils/constants';
import { useParams } from 'react-router-dom';
import InputField from '../../components/InputField';
import { makeSelectTemplates } from '../TemplatePortal/selectors';
import {
  editTemplateDetails,
  downloadTemplate,
  resetState,
} from './actions';
import {
  setFormValidationError,
  isDirtyForm,
  loadEntityDetails,
} from '../ApplicantDetails/actions';
import { useHistory } from 'react-router-dom';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: { span: 10, offset: 2, pull: 2 },
};
export function TemplateUpdateDetails({
  dispatch,
  updateTemplateDetails,
  activity,
  templateCommunications,
  templatePortal,
}) {

  useInjectReducer({ key: 'updateTemplateDetails', reducer });
  useInjectSaga({ key: 'updateTemplateDetails', saga });

  const emailcommData = templatePortal.templateCommunications.map(item => ({
    id: item.id,
    templateId: item.templateId,
    remarks: item.remarks,
    active: item.active,
    validationMsg: item.validationMessage,
    content: item.content,
  }));
  // var result = arr.map(person => ({ value: person.id, text: person.name }));
  // console.log(result)

  const newEmailDict = {};

  for (let i = 0; i < emailcommData.length; i++) {
    newEmailDict[emailcommData[i].id] = emailcommData[i];
  }

  let { linkTemplateId } = useParams();

  const {
    register,
    handleSubmit,
    getValues,
    errors,
    control,
    watch,
    setError,
    reset,
    clearError,
    formState,
    setValue,
    triggerValidation,
  } = useForm({
    mode: 'onChange',

    defaultValues: {
      templateId: newEmailDict[linkTemplateId].templateId,
      validationMsg: newEmailDict[linkTemplateId].validationMsg,
      remarks: newEmailDict[linkTemplateId].remarks,
      active: newEmailDict[linkTemplateId].active,
      content: newEmailDict[linkTemplateId].content,
    },
  });

  const { dirtyFields, dirtyFieldsSinceLastSubmit } = formState;
  console.log(dirtyFields, 'dirtyFields');
  useEffect(() => {
    dispatch(isDirtyForm({ 5: formState.dirty }));
    console.log(formState.dirty, 'dirty bank', formState.dirtySinceLastSubmit);
  }, [formState.dirty]);

  const values = watch();
  console.log(values, 'values');

  console.log(formState.dirty, 'dirty val');
  

  // console.log(values, 'values bank details', errors);

  const [response, setResponse] = useState(false);

  useEffect(() => {
    // if (Object.keys(errors).length > 0) {
    //   document.getElementById(Object.keys(errors)[0]).focus();
    // }
  }, [errors]);



  useEffect(() => {
    if (updateTemplateDetails.response && response) {
      message.success(updateTemplateDetails.response);
      dispatch(loadEntityDetails());
      setResponse(false);
    }

    if (updateTemplateDetails.error && response) {
      message.error(updateTemplateDetails.error);
    }
  }, [updateTemplateDetails.response, updateTemplateDetails.error]);
  const onSubmit = data => {
    dispatch(editTemplateDetails(data));
    setResponse(true);
    setTimeout(addTemplate, 3000);
  };

  const history = useHistory();
  const addTemplate = () => {
    history.push(`/templateDetails`);
  };

  const downloadTemplateID = () => {
    dispatch(
      downloadTemplate(
        {
          templateId: newEmailDict[linkTemplateId].templateId,
        },
      ),
      //  setDownload(templateDetails.responseExcel),

    );
  }
    // Download Excel
  
  useEffect(() => {
    if (!_.get(updateTemplateDetails,'responseExcel'))
      return;
     var raw = window.atob(updateTemplateDetails.responseExcel);
      var uInt8Array = new Uint8Array(raw.length);
      for (var i = 0; i < raw.length; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
      }

      const link = document.createElement('a')
      const blob = new Blob([uInt8Array], { type: 'application/vnd.ms-excel' })
      link.style.display = 'none'
      link.href = URL.createObjectURL(blob)

      link.setAttribute('download', `${newEmailDict[linkTemplateId].templateId}`)
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)
  }, [updateTemplateDetails.responseExcel]);

  useEffect(()=> {
    dispatch(resetState());
  },[updateTemplateDetails.responseExcel])

  // console.log(applicantDetails, 'applicantDetails');
  return (
    <div className="App">
      <h4>Template Details</h4>
      <Divider />
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={[16, 16]}>
          {/* start */}
          <Col md={24}>
            {/* <h5 style={{ fontWeight: 'bold' }}>Disbursement Bank Details</h5> */}
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="templateId"
              control={control}
              name="templateId"
              type="string"
              values={values}
              disabled
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
                //  pattern: emailPattern,
              }}
              errors={errors}
              placeholder="Template Code"
              labelHtml="Template Code*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="validationMsg"
              control={control}
              name="validationMsg"
              type="string"
              values={values}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
                //  pattern: emailPattern,
              }}
              errors={errors}
              placeholder="Validation Message"
              labelHtml="Validation Message*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="remarks"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              name="remarks"
              type="string"
              values={values}
              errors={errors}
              placeholder="Reamrks"
              labelHtml="Remarks*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="active"
              control={control}
              name="active"
              type="select"
              values={values}
              options={DEFAULT_MAIL_ID}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Active"
              labelHtml="Active*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <Form.Item label="Content">
              <Controller
                name="content"
                control={control}
                value={values}
                style={{ height: '8rem', width: '61rem', marginBottom: '2rem' }}
                as={<ReactQuill theme="snow" />}
              />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            disabled={activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'}
          >
            Save
          </Button>
          {'   '}
          <Button
            type="primary"
            onClick={addTemplate}
          >
            Cancel
          </Button>
          {'   '}
          <Button
            type="primary"
            onClick={downloadTemplateID}
          >
            Download Template
          </Button>
        </Form.Item>
      </form>
    </div>
  );
}

TemplateUpdateDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  updateTemplateDetails: makeSelectTemplateDetails(),
  templatePortal: makeSelectTemplates(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TemplateUpdateDetails);
