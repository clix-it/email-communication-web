/*
 *
 * OtherDetails actions
 *
 */

import {
  DEFAULT_ACTION,
  UPDATE_OTHER_DETAILS,
  UPDATE_OTHER_DETAILS_SUCCESS,
  UPDATE_OTHER_DETAILS_ERROR,
  FETCH_DEVIATIONS,
  FETCH_DEVIATIONS_SUCCESS,
  FETCH_DEVIATIONS_ERROR,
  FETCH_FREE_FIELDS,
  FETCH_FREE_FIELDS_SUCCESS,
  FETCH_FREE_FIELDS_ERROR,
  FETCH_ASSET_CONDITION,
  FETCH_ASSET_CONDITION_SUCCESS,
  FETCH_ASSET_CONDITION_ERROR,
  FETCH_ASSET_TYPE,
  FETCH_ASSET_TYPE_SUCCESS,
  FETCH_ASSET_TYPE_ERROR,
  FETCH_MANUFACTURER,
  FETCH_MANUFACTURER_SUCCESS,
  FETCH_MANUFACTURER_ERROR,
  FETCH_SUPPLIER,
  FETCH_SUPPLIER_SUCCESS,
  FETCH_SUPPLIER_ERROR,
  FETCH_CONVENANT_TYPE,
  FETCH_CONVENANT_TYPE_SUCCESS,
  FETCH_CONVENANT_TYPE_ERROR,
  DELPHI_CALL,
  DELPHI_CALL_SUCCESS,
  DELPHI_CALL_ERROR,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function submitOtherDetails(
  data,
  description,
  deviationObj,
  reset,
  values,
  freeFieldsObj,
  product,
  masterAssetTypeData,
) {
  return {
    type: UPDATE_OTHER_DETAILS,
    data,
    description,
    deviationObj,
    reset,
    values,
    freeFieldsObj,
    product,
    masterAssetTypeData,
  };
}
export function updateOtherDetailsSuccess(data) {
  return {
    type: UPDATE_OTHER_DETAILS_SUCCESS,
    data,
  };
}
export function updateOtherDetailsError(error) {
  return {
    type: UPDATE_OTHER_DETAILS_ERROR,
    data: error,
  };
}

export function fetchDeviations(partner, product) {
  // ;
  return {
    type: FETCH_DEVIATIONS,
    partner,
    product,
  };
}

export function fetchDeviationsSuccess(data) {
  // ;
  return {
    type: FETCH_DEVIATIONS_SUCCESS,
    data,
  };
}

export function fetchDeviationsError(error) {
  // ;
  return {
    type: FETCH_DEVIATIONS_ERROR,
    error,
  };
}

export function fetchFreeFields(partner, product) {
  // ;
  return {
    type: FETCH_FREE_FIELDS,
    partner,
    product,
  };
}

export function fetchFreeFieldsSuccess(data) {
  // ;
  return {
    type: FETCH_FREE_FIELDS_SUCCESS,
    data,
  };
}

export function fetchFreeFieldsError(error) {
  // ;
  return {
    type: FETCH_FREE_FIELDS_ERROR,
    error,
  };
}

export function fetchAssetCondition(partner, product) {
  // ;
  return {
    type: FETCH_ASSET_CONDITION,
    partner,
    product,
  };
}

export function fetchAssetConditionSuccess(data) {
  // ;
  return {
    type: FETCH_ASSET_CONDITION_SUCCESS,
    data,
  };
}

export function fetchAssetConditionError(error) {
  // ;
  return {
    type: FETCH_ASSET_CONDITION_ERROR,
    error,
  };
}

export function fetchAssetType(partner, product) {
  // ;
  return {
    type: FETCH_ASSET_TYPE,
    partner,
    product,
  };
}

export function fetchAssetTypeSuccess(data) {
  // ;
  return {
    type: FETCH_ASSET_TYPE_SUCCESS,
    data,
  };
}

export function fetchAssetTypeError(error) {
  // ;
  return {
    type: FETCH_ASSET_TYPE_ERROR,
    error,
  };
}

export function fetchManufacturer(partner, product) {
  // ;
  return {
    type: FETCH_MANUFACTURER,
    partner,
    product,
  };
}

export function fetchManufacturerSuccess(data) {
  // ;
  return {
    type: FETCH_MANUFACTURER_SUCCESS,
    data,
  };
}

export function fetchManufacturerError(error) {
  // ;
  return {
    type: FETCH_MANUFACTURER_ERROR,
    error,
  };
}

export function fetchSupplier(partner, product) {
  // ;
  return {
    type: FETCH_SUPPLIER,
    partner,
    product,
  };
}

export function fetchSupplierSuccess(data) {
  // ;
  return {
    type: FETCH_SUPPLIER_SUCCESS,
    data,
  };
}

export function fetchSupplierError(error) {
  // ;
  return {
    type: FETCH_SUPPLIER_ERROR,
    error,
  };
}

export function fetchConvenantType() {
  return {
    type: FETCH_CONVENANT_TYPE,
  };
}

export function fetchConvenantTypeSuccess(data) {
  return {
    type: FETCH_CONVENANT_TYPE_SUCCESS,
    data,
  };
}

export function fetchConvenantTypeError(error) {
  return {
    type: FETCH_CONVENANT_TYPE_ERROR,
    error,
  };
}

export function callDelphi(appId) {
  return {
    type: DELPHI_CALL,
    appId,
  };
}

export function callDelphiSuccess(response) {
  return {
    type: DELPHI_CALL_SUCCESS,
    response,
  };
}

export function callDelphiError(error) {
  return {
    type: DELPHI_CALL_ERROR,
    error,
  };
}
