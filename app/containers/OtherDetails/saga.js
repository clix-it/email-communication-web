// import $ from 'jquery';
import { take, call, put, select, takeEvery, all } from 'redux-saga/effects';
// import request from 'utils/request';
import request from 'utils/request';
import _ from 'lodash';
import { message } from 'antd';
import {
  UPDATE_OTHER_DETAILS,
  FETCH_DEVIATIONS,
  FETCH_FREE_FIELDS,
  FETCH_ASSET_CONDITION,
  FETCH_ASSET_TYPE,
  FETCH_MANUFACTURER,
  FETCH_SUPPLIER,
  FETCH_CONVENANT_TYPE,
  DELPHI_CALL,
} from './constants';
// import { CLICK_WRAP_API_URL } from '../App/constants';
import { updateApplication, updateCompositeApplication } from '../App/saga';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';
import {
  updateOtherDetailsSuccess,
  updateOtherDetailsError,
  fetchDeviationsSuccess,
  fetchDeviationsError,
  fetchFreeFieldsSuccess,
  fetchFreeFieldsError,
  fetchAssetConditionSuccess,
  fetchAssetConditionError,
  fetchAssetTypeSuccess,
  fetchAssetTypeError,
  fetchManufacturerSuccess,
  fetchManufacturerError,
  fetchSupplierSuccess,
  fetchSupplierError,
  fetchConvenantTypeSuccess,
  fetchConvenantTypeError,
  callDelphiSuccess,
  callDelphiError,
} from './actions';
import { loadUpdateUserSuccess } from '../ApplicantDetails/actions';
import env from '../../../environment';

export default function* otherDetailsSaga() {
  yield takeEvery(UPDATE_OTHER_DETAILS, updateApplicationFunction);
  yield takeEvery(FETCH_DEVIATIONS, fetchDeviations);
  yield takeEvery(FETCH_FREE_FIELDS, fetchFreeFields);
  yield takeEvery(FETCH_ASSET_CONDITION, fetchAssetConditions);
  yield takeEvery(FETCH_ASSET_TYPE, fetchAssetTypes);
  yield takeEvery(FETCH_MANUFACTURER, fetchManufacturers);
  yield takeEvery(FETCH_SUPPLIER, fetchSupplier);
  yield takeEvery(FETCH_CONVENANT_TYPE, fetchCovenantType);
  yield takeEvery(DELPHI_CALL, callDelphi);
}

export function* callDelphi({ appId }) {
  try {
    const url = `${env.API_URL}/profiles/delphi`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        applicationId: appId,
        perfiosResponse: '',
        callbackUrl: '',
      }),
    };
    const response = yield call(request, url, options);
    // const response = data;
    if (response.mergeResult) {
      yield put(callDelphiSuccess());
      message.success('Delphi success');
    }
  } catch (err) {
    yield put(callDelphiError(err.message));
    message.error(err.message || 'Some error occured!');
  }
}

export function* fetchManufacturers({ partner, product }) {
  const requestURL = `${env.MASTER_URL}?masterType=MANUFACTURE_${product}`;

  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchManufacturerSuccess(response.masterData));
    } else {
      yield put(fetchManufacturerError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(fetchManufacturerError('Master Data not found'));
    message.error((error && error.message) || 'Master Data not found');
  }
}

export function* fetchSupplier({ partner, product }) {
  const requestURL = `${env.MASTER_URL}?masterType=SUPPLIER_HFS`;

  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchSupplierSuccess(response.masterData));
    } else {
      yield put(fetchSupplierError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(fetchSupplierError('Master Data not found'));
    message.error((error && error.message) || 'Master Data not found');
  }
}

export function* fetchAssetTypes({ partner, product }) {
  const requestURL = `${env.MASTER_URL}?masterType=HFS_ASSET_TPE_CATEGORY`;

  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchAssetTypeSuccess(response.masterData));
    } else {
      yield put(fetchAssetTypeError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(fetchAssetTypeError('Master Data not found'));
    message.error((error && error.message) || 'Master Data not found');
  }
}

export function* fetchAssetConditions({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=ASSET_CONDITION_${partner}_${product}`;

  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchAssetConditionSuccess(response.masterData));
    } else {
      yield put(fetchAssetConditionError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(fetchAssetConditionError('Master Data not found'));
    message.error((error && error.message) || 'Master Data not found');
  }
}

export function* fetchFreeFields({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=FREEFIELDS_${partner}_${product}`;

  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchFreeFieldsSuccess(response.masterData));
    } else {
      yield put(fetchFreeFieldsError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(fetchFreeFieldsError('Master Data not found'));
    message.error((error && error.message) || 'Master Data not found');
  }
}

export function* fetchDeviations({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=DEVIATION_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchDeviationsSuccess(response.masterData));
    } else {
      yield put(fetchDeviationsError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchDeviationsError('Master Data not found'));
  }
}

export function* updateApplicationFunction(payload) {
  // yield select(makeSelectAppLayout());
  const appData = yield select(makeSelectApplicantDetails2());

  const appCollateralsVal = [];
  for (const i in payload.description) {
    if (appData.entity.appCollaterals[i]) {
      if (payload.product == 'HFS') {
        appCollateralsVal.push({
          id: appData.entity.appCollaterals[i].id || null,
          brand: payload.description[i].brand,
          assetManufacturer: payload.description[i].brand,
          assetType:
            payload.description[i].assetType.toLowerCase() === 'others'
              ? payload.description[i].assetTypeTextValue
              : payload.description[i].assetType,
          assetPrice: payload.description[i].assetPrice,
          assetCondition: payload.description[i].assetCondition,
          assetCategory: payload.description[i].assetType
            ? payload.masterAssetTypeData[payload.description[i].assetType]
            : '',
          assetUnit: payload.description[i].assetUnit,
          assetMarginMoney: payload.description[i].assetMarginMoney,
          marginMoney: payload.description[i].marginMoney,
          assetInstallationPlace: payload.description[i].assetInstallationPlace,
          collateralLocation: payload.description[i].collateralLocation,
          assetDealer:
            payload.description[i].assetDealer.toLowerCase() === 'others'
              ? payload.description[i].supplierTextValue
              : payload.description[i].assetDealer,
          assetModel: payload.description[i].assetModel,
          assetInvoicePrice: payload.description[i].assetInvoicePrice,
          assetInvoiceNumber: payload.description[i].assetInvoiceNumber,
          assetValuationNRV: payload.description[i].assetValuationNRV,
          assetValuationOLV: payload.description[i].assetValuationOLV,
          assetSpeciality: payload.description[i].assetSpeciality,
          assetMajorOem: payload.description[i].assetMajorOem,
          description: JSON.stringify({
            downPaymentMode: payload.description[i].downPaymentMode,
          }),
        });
      } else {
        appCollateralsVal.push({
          id: appData.entity.appCollaterals[i].id || null,
          description: JSON.stringify({
            address: payload.description[i].address,
            valuation1: payload.description[i].valuation1,
            valuation2: payload.description[i].valuation2,
            collateralType: payload.description[i].collateralType,
            ltv: payload.description[i].ltv,
          }),
        });
      }
    } else if (payload.product == 'HFS') {
      appCollateralsVal.push({
        brand: payload.description[i].brand,
        assetManufacturer: payload.description[i].brand,
        assetType:
          payload.description[i].assetType.toLowerCase() === 'others'
            ? payload.description[i].assetTypeTextValue
            : payload.description[i].assetType,
        assetPrice: payload.description[i].assetPrice,
        assetCondition: payload.description[i].assetCondition,
        assetCategory: payload.description[i].assetType
          ? payload.masterAssetTypeData[payload.description[i].assetType]
          : '',
        assetUnit: payload.description[i].assetUnit,
        assetMarginMoney: payload.description[i].assetMarginMoney,
        marginMoney: payload.description[i].marginMoney,
        assetInstallationPlace: payload.description[i].assetInstallationPlace,
        assetDealer:
          payload.description[i].assetDealer.toLowerCase() === 'others'
            ? payload.description[i].supplierTextValue
            : payload.description[i].assetDealer,
        assetModel: payload.description[i].assetModel,
        assetInvoicePrice: payload.description[i].assetInvoicePrice,
        assetInvoiceNumber: payload.description[i].assetInvoiceNumber,
        assetValuationAmount: payload.description[i].assetValuationAmount,
        assetSpeciality: payload.description[i].assetSpeciality,
        assetMajorOem: payload.description[i].assetMajorOem,
        description: JSON.stringify({
          downPaymentMode: payload.description[i].downPaymentMode,
        }),
      });
    } else {
      appCollateralsVal.push({
        description: JSON.stringify({
          address: payload.description[i].address,
          valuation1: payload.description[i].valuation1,
          valuation2: payload.description[i].valuation2,
          collateralType: payload.description[i].collateralType,
          ltv: payload.description[i].ltv,
        }),
      });
    }
  }
  console.log(appCollateralsVal, 'appCollateralsVal');

  const data = {
    appSourcing: {
      creditManagerId: payload.data.creditManagerId,
      salesManager: payload.data.salesManager,
      clixBranch: payload.data.branch,
      id: appData.entity.appSourcing ? appData.entity.appSourcing.id : '',
    },
    appReferences: payload.data.appReferences,
    appCollaterals: appCollateralsVal,
    appCovenants: _.get(payload, 'data.appCovenants', []),
    // appCollaterals: [
    //   {
    //     id: appData.entity.appCollaterals[0].id || null,
    //     description: JSON.stringify({
    //       address: payload.data.address,
    //       valuation1: payload.data.valuation1,
    //       valuation2: payload.data.valuation2,
    //       collateralType: payload.data.collateralType,
    //     }),
    //   },
    // ],
    appId: appData.entity.appId || '50006879',
    additionalData: {
      data: {
        deviations: payload.deviationObj || {},
        psl: payload.data.psl
          ? { category: payload.data.psl }
          : { category: '' },
        sanctionConditions: {
          condition: payload.data.sanctionCondition,
        },
        recommendNote: payload.data.recommendationNote,
        extraDataField: {
          msme: payload.data.msme || '',
          otcFlag: payload.data.otcFlag,
          remarks: payload.data.remarks || '',
        },
      },
    },
  };
  if (
    _.get(payload, 'data.appPslDetail') &&
    Object.values(_.get(payload, 'data.appPslDetail')).some(val => val)
  ) {
    data.appPslDetail = _.get(payload, 'data.appPslDetail', {});
  }

  if (payload.freeFieldsObj && Object.keys(payload.freeFieldsObj).length > 0) {
    Object.keys(payload.freeFieldsObj).forEach(item => {
      data.additionalData.data.extraDataField[item] =
        payload.freeFieldsObj[item];
    });
  }
  try {
    const response = yield updateCompositeApplication({
      ...data,
      // JSON.parse(sessionStorage.getItem('id')),
    });

    const appServiceResponse = _.get(response, 'data[0].appServiceResponse');
    if (_.get(appServiceResponse, 'body.success')) {
      payload.reset(payload.values);
      yield all([
        put(
          updateOtherDetailsSuccess({
            message:
              _.get(appServiceResponse, 'body.message') ||
              'app updated successfully!!',
          }),
        ),
        put(loadUpdateUserSuccess('Success')),
      ]);
    } else {
      yield put(updateBankDetailsError({ error: 'Data not saved' }));
    }
  } catch (error) {
    yield put(updateOtherDetailsError({ error: 'Data not saved' }));
  }
}

export function* fetchCovenantType() {
  const requestURL = `${env.MASTER_DATA_URL}/covenanttype`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.length > 0) {
      yield put(fetchConvenantTypeSuccess(response));
    } else {
      yield put(fetchConvenantTypeError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(fetchConvenantTypeError('Master Data not found'));
    message.error((error && error.message) || 'Master Data not found');
  }
}
