import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the otherDetails state domain
 */

const selectOtherDetailsDomain = state => state.otherDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by OtherDetails
 */

const makeSelectOtherDetails = () =>
  createSelector(
    selectOtherDetailsDomain,
    substate => substate,
  );

export default makeSelectOtherDetails;
export { selectOtherDetailsDomain };
