/*
 *
 * OtherDetails reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  UPDATE_OTHER_DETAILS,
  UPDATE_OTHER_DETAILS_ERROR,
  UPDATE_OTHER_DETAILS_SUCCESS,
  FETCH_DEVIATIONS_SUCCESS,
  FETCH_DEVIATIONS_ERROR,
  FETCH_FREE_FIELDS_SUCCESS,
  FETCH_FREE_FIELDS_ERROR,
  FETCH_ASSET_CONDITION_SUCCESS,
  FETCH_ASSET_CONDITION_ERROR,
  FETCH_ASSET_TYPE_SUCCESS,
  FETCH_ASSET_TYPE_ERROR,
  FETCH_MANUFACTURER_SUCCESS,
  FETCH_MANUFACTURER_ERROR,
  FETCH_SUPPLIER,
  FETCH_SUPPLIER_SUCCESS,
  FETCH_SUPPLIER_ERROR,
  FETCH_CONVENANT_TYPE_SUCCESS,
  FETCH_CONVENANT_TYPE_ERROR,
} from './constants';

export const initialState = {
  response: false,
  payload: false,
  error: false,
  description: false,
  masterDeviationData: {},
  masterFreeFieldsData: {},
  masterAssetConditionData: {},
  masterAssetTypeData: {},
  masterManufacturerData: {},
  masterSupplierData: {},
  masterConvenantTypeData: {},
};

/* eslint-disable default-case, no-param-reassign */
const otherDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case UPDATE_OTHER_DETAILS:
        draft.payload = action.data;
        draft.description = action.description;
        draft.response = false;
        draft.error = false;
        break;
      case UPDATE_OTHER_DETAILS_SUCCESS:
        draft.response = action.data.message;
        draft.error = false;
        break;
      case UPDATE_OTHER_DETAILS_ERROR:
        draft.response = false;
        draft.error = `Data not saved`;
        break;
      case FETCH_DEVIATIONS_SUCCESS:
        let tempObj = {};
        action.data.forEach(item => {
          tempObj[item.cmCode] = item.cmValue;
        });
        draft.masterDeviationData = tempObj;
        break;
      case FETCH_DEVIATIONS_ERROR:
        draft.masterDeviationData = {};
        break;
      case FETCH_FREE_FIELDS_SUCCESS:
        let tempObj1 = {};
        action.data.forEach(item => {
          tempObj1[item.cmCode] = item.cmValue;
        });
        draft.masterFreeFieldsData = tempObj1;
        break;
      case FETCH_FREE_FIELDS_ERROR:
        draft.masterFreeFieldsData = {};
        break;
      case FETCH_ASSET_CONDITION_SUCCESS:
        let tempObj2 = {};
        action.data.forEach(item => {
          tempObj2[item.cmCode] = item.cmValue;
        });
        draft.masterAssetConditionData = tempObj2;
        break;
      case FETCH_ASSET_CONDITION_ERROR:
        draft.masterAssetConditionData = {};
        break;
      case FETCH_ASSET_TYPE_SUCCESS:
        let tempObj3 = {};
        action.data.forEach(item => {
          tempObj3[item.cmCode] = item.cmValue;
        });
        draft.masterAssetTypeData = tempObj3;
        break;
      case FETCH_ASSET_TYPE_ERROR:
        draft.masterAssetTypeData = {};
        break;
      case FETCH_MANUFACTURER_SUCCESS:
        let tempObj4 = {};
        action.data.forEach(item => {
          tempObj4[item.cmCode] = item.cmValue;
        });
        draft.masterManufacturerData = tempObj4;
        break;
      case FETCH_MANUFACTURER_ERROR:
        draft.masterManufacturerData = {};
        break;
      case FETCH_SUPPLIER_SUCCESS:
        let tempObj5 = {};
        action.data.forEach(item => {
          tempObj5[item.cmCode] = item.cmValue;
        });
        draft.masterSupplierData = tempObj5;
        break;
      case FETCH_SUPPLIER_ERROR:
        draft.masterSupplierData = {};
        break;
      case FETCH_CONVENANT_TYPE_SUCCESS: {
        const temp = [];
        action.data.forEach(item => {
          temp.push({ code: item.doctypecode, value: item.doctypedesc });
        });
        draft.masterConvenantTypeData = temp;
        break;
      }
      case FETCH_CONVENANT_TYPE_ERROR:
        draft.masterConvenantTypeData = {};
        break;
    }
  });

export default otherDetailsReducer;
