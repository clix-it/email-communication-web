/**
 *
 * Asynchronously loads the component for OtherDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
