/**
 *
 * OtherDetails
 *
 */

import _ from 'lodash';
import React, { memo, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useForm, Controller } from 'react-hook-form';
import {
  Form,
  message,
  Input,
  Select,
  Row,
  Col,
  Button,
  AutoComplete,
  Divider,
  Radio,
  Space,
} from 'antd';
import OtcPdd from 'components/OtcPdd';
import makeSelectOtherDetails from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import InputField from '../../components/InputField';
import References from '../../components/References';
import { OtherDetailsDeclarations } from '../../utils/constants';
import {
  submitOtherDetails,
  fetchDeviations,
  fetchFreeFields,
  fetchAssetCondition,
  fetchAssetType,
  fetchManufacturer,
  fetchSupplier,
  fetchConvenantType,
  callDelphi,
} from './actions';
// const {  } = antd;
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';
import {
  setFormValidationError,
  isDirtyForm,
  loadEntityDetails,
} from '../ApplicantDetails/actions';
import PslCategory from '../PslCategory';
import PslEndUse from '../PslEndUse';
import PslPurpose from '../PslPurpose';
import PslWeakerSection from '../PslWeakerSection';
import PslSector from '../PslSector';
import PslSubCategory from '../PslSubCategory';

const { Option } = Select;
const AutoCompleteOption = AutoComplete.Option;
const { TextArea } = Input;
const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: { span: 10, offset: 2, pull: 2 },
};
const formItemLayout = {
  labelCol: {
    span: 24,
  },
  wrapperCol: {
    xs: {
      span: 8,
    },
    sm: {
      span: 8,
    },
    md: {
      span: 24,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 24,
      // offset: 0,
    },
  },
};

const Text = styled.span`
  display: inline-block;
  padding-left: 1.5em;
  flex-grow: 0;
  overflow: hidden;
  white-space: nowrap;
  text-align: right;
  vertical-align: middle;
  color: rgba(0, 0, 0, 0.85);
  font-size: 14px;
`;

export function OtherDetails({
  applicantDetails,
  dispatch,
  otherDetails,
  appId,
  resetValues,
  requestType,
  isNoEntity,
  activity,
}) {
  useInjectReducer({ key: 'otherDetails', reducer });
  useInjectSaga({ key: 'otherDetails', saga });

  const {
    masterDeviationData = {},
    masterFreeFieldsData = {},
    masterAssetConditionData = {},
    masterAssetTypeData = {},
    masterManufacturerData = {},
    masterSupplierData = {},
    masterConvenantTypeData = {},
  } = otherDetails;
  const [freeFields, setFreeFields] = useState([]);
  const [supplierOther, setSupplierOther] = useState({});
  const [assetTypeOther, setAssetTypeOther] = useState({});
  const [thirdPartyAddress, setThirdPartyAddress] = useState({});
  const defautFreeFields = _.get(
    applicantDetails,
    'entity.additionalData.data.extraDataField',
  );

  const MARGIN_MONEY = ['Cash Margin'];

  const ASSET_DEPLOYMENT = [
    'Own Address',
    'Third Party Site arrangement(Revenue Sharing)',
  ];

  const {
    handleSubmit,
    setValue,
    triggerValidation,
    errors,
    control,
    watch,
    formState,
    reset,
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      creditManagerId:
        _.get(applicantDetails, 'entity.appSourcing.creditManagerId') ||
        JSON.parse(sessionStorage.getItem('userId')),
      salesManager: _.get(applicantDetails, 'entity.appSourcing.salesManager'),
      sourcingExecutive: _.get(
        applicantDetails,
        'entity.appSourcing.sourcingExecutive',
        '',
      ),
      sourcing: _.get(
        applicantDetails,
        'entity.appSourcing.channel',
        'Direct Source',
      ),
      branch: _.get(applicantDetails, 'entity.appSourcing.clixBranch') || '',
      psl:
        _.get(applicantDetails, 'entity.additionalData.data.psl.category') ||
        '',
      otcFlag:
        _.get(
          applicantDetails,
          'entity.additionalData.data.extraDataField.otcFlag',
        ) || '',
      msme:
        _.get(
          applicantDetails,
          'entity.additionalData.data.extraDataField.msme',
        ) || '',
      sanctionCondition:
        _.get(
          applicantDetails,
          'entity.additionalData.data.sanctionConditions.condition',
        ) || '',
      recommendationNote:
        _.get(applicantDetails, 'entity.additionalData.data.recommendNote') ||
        '',
      'appPslDetail.categoryCode': _.get(
        applicantDetails,
        'entity.appPslDetail.categoryCode',
        '',
      ),
      'appPslDetail.weakerSection': _.get(
        applicantDetails,
        'entity.appPslDetail.weakerSection',
        '',
      ),
      'appPslDetail.sector': _.get(
        applicantDetails,
        'entity.appPslDetail.sector',
        '',
      ),
      'appPslDetail.investment': _.get(
        applicantDetails,
        'entity.appPslDetail.investment',
        '',
      ),
      'appPslDetail.landHolding': _.get(
        applicantDetails,
        'entity.appPslDetail.landHolding',
        '',
      ),
      'appPslDetail.amount': _.get(
        applicantDetails,
        'entity.appPslDetail.amount',
        '',
      ),
      'appPslDetail.landArea': _.get(
        applicantDetails,
        'entity.appPslDetail.landArea',
        '',
      ),
      'appPslDetail.subCategory': _.get(
        applicantDetails,
        'entity.appPslDetail.subCategory',
        '',
      ),
      'appPslDetail.purpose': _.get(
        applicantDetails,
        'entity.appPslDetail.purpose',
        '',
      ),
      'appPslDetail.endUse': _.get(
        applicantDetails,
        'entity.appPslDetail.endUse',
        '',
      ),
      appCovenants: _.get(applicantDetails, 'entity.appCovenants', ''),
      remarks:
        _.get(
          applicantDetails,
          'entity.additionalData.data.extraDataField.remarks',
        ) || '',
    },
  });

  useEffect(() => {
    if (_.get(applicantDetails, 'entity.partner')) {
      dispatch(
        fetchDeviations(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      dispatch(
        fetchFreeFields(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      if (_.get(applicantDetails, 'entity.product') == 'HFS') {
        dispatch(
          fetchAssetCondition(
            applicantDetails.entity.partner,
            applicantDetails.entity.product,
          ),
        );
        dispatch(
          fetchAssetType(
            applicantDetails.entity.partner,
            applicantDetails.entity.product,
          ),
        );
        dispatch(
          fetchManufacturer(
            applicantDetails.entity.partner,
            applicantDetails.entity.product,
          ),
        );
        dispatch(
          fetchSupplier(
            applicantDetails.entity.partner,
            applicantDetails.entity.product,
          ),
        );
      }
    }
    dispatch(fetchConvenantType());
  }, []);

  useEffect(() => {
    dispatch(isDirtyForm({ 3: formState.dirty }));

    console.log(formState.dirty, 'dirty other');
  }, [formState.dirty]);
  const values = watch();
  console.log(values, 'values', errors);

  const [type, setType] = React.useState([]);
  const [pslRadioValue, setPslRadioValue] = React.useState('No');
  const [msmeRadioValue, setMsmeRadioValue] = _.get(
    applicantDetails,
    'entity.additionalData.data.extraDataField.msme',
  )
    ? React.useState('Yes')
    : React.useState('No');
  const [response, setResponse] = React.useState(false);
  const [collateralLength, setCollateralLength] = React.useState(false);
  const [description, setDescription] = React.useState([]);
  const [collateral, setCollateral] = React.useState({
    address: '',
    valuation1: '',
    valuation2: '',
    collateralType: '',
  });
  const [branchOptions, setBranchOptions] = useState([
    'Delhi',
    'Pune',
    'Bangalore',
    'Jaipur',
    'Chennai',
    'Mumbai',
    'Chandigarh',
    'Hyderabad',
    'Gurugram',
    'Ahmedabad',
  ]);

  const [pslCategory, setPslCategory] = useState([
    'Small',
    'Micro',
    'Medium',
    'Others',
  ]);

  const [msmeCategory, setMsmeCategory] = useState([
    'Small',
    'Micro',
    'Medium',
    'Others',
  ]);

  useEffect(() => {
    if (requestType === 'PMA')
      triggerValidation().then(valid => {
        dispatch(setFormValidationError('otherDetails', valid));
      });
  }, [applicantDetails.entity]);

  useEffect(() => {
    const arr = [];
    const collateralArr = [];
    for (var i in applicantDetails.entity.loanOffers) {
      arr.push(applicantDetails.entity.loanOffers[i].id);
    }
    setType([...arr]);
    const supplierObj = {};
    const assetTypeObj = {};
    const thirdPartyAddressObj = {};
    if (applicantDetails.entity.appCollaterals.length > 0) {
      for (var i in applicantDetails.entity.appCollaterals) {
        const obj = JSON.parse(
          applicantDetails.entity.appCollaterals[i].description,
        );
        if (_.get(applicantDetails, 'entity.product') == 'HFS') {
          supplierObj[`assetDealer${parseInt(i) + 1}`] = !!(
            applicantDetails.entity.appCollaterals[i].assetDealer &&
            applicantDetails.entity.appCollaterals[
              i
            ].assetDealer.toLowerCase() == 'others'
          );
          assetTypeObj[`assetType${parseInt(i) + 1}`] = !!(
            applicantDetails.entity.appCollaterals[i].assetType &&
            applicantDetails.entity.appCollaterals[i].assetType.toLowerCase() ==
              'others'
          );
          thirdPartyAddressObj[`thirdPartyAddress${parseInt(i) + 1}`] = !!(
            applicantDetails.entity.appCollaterals[i].assetInstallationPlace &&
            applicantDetails.entity.appCollaterals[i].assetInstallationPlace ===
              'Third Party Site arrangement(Revenue Sharing)'
          );
          collateralArr.push(
            applicantDetails.entity.appCollaterals[i].description
              ? {
                  ...obj,
                  brand: applicantDetails.entity.appCollaterals[i].brand,
                  assetType:
                    applicantDetails.entity.appCollaterals[i].assetType,
                  assetPrice:
                    applicantDetails.entity.appCollaterals[i].assetPrice,
                  assetCondition:
                    applicantDetails.entity.appCollaterals[i].assetCondition,
                  assetUnit:
                    applicantDetails.entity.appCollaterals[i].assetUnit,
                  assetMarginMoney:
                    applicantDetails.entity.appCollaterals[i].assetMarginMoney,
                  assetInstallationPlace:
                    applicantDetails.entity.appCollaterals[i]
                      .assetInstallationPlace,
                  collateralLocation:
                    applicantDetails.entity.appCollaterals[i]
                      .collateralLocation,
                  assetModel:
                    applicantDetails.entity.appCollaterals[i].assetModel,
                  assetDealer:
                    applicantDetails.entity.appCollaterals[i].assetDealer,
                  assetInvoicePrice:
                    applicantDetails.entity.appCollaterals[i].assetInvoicePrice,
                  assetInvoiceNumber:
                    applicantDetails.entity.appCollaterals[i]
                      .assetInvoiceNumber,
                  assetValuationOLV:
                    applicantDetails.entity.appCollaterals[i]
                    .assetValuationOLV,
                  assetValuationNRV:
                    applicantDetails.entity.appCollaterals[i]
                      .assetValuationNRV,
                  assetSpeciality:
                    applicantDetails.entity.appCollaterals[i].assetSpeciality,
                  marginMoney:
                    applicantDetails.entity.appCollaterals[i].marginMoney,
                  assetMajorOem:
                    applicantDetails.entity.appCollaterals[i].assetMajorOem,
                }
              : collateral,
          );
        } else {
          collateralArr.push(
            applicantDetails.entity.appCollaterals[i].description
              ? obj
              : collateral,
          );
        }
      }
    }
    setCollateralLength([collateralArr.length]);
    setDescription([...collateralArr]);
    setSupplierOther({ ...supplierObj });
    setAssetTypeOther({ ...assetTypeObj });
    setThirdPartyAddress({ ...thirdPartyAddressObj });
  }, []);

  useEffect(() => {
    console.log(description, 'Description');
  }, [description]);

  /**
   * function to Update Collateral
   */
  const updateCollateral = (e, i, dataName) => {
    const supplierObj = { ...supplierOther };
    const assetTypeObj = { ...assetTypeOther };
    const thirdPartyAddressObj = { ...thirdPartyAddressObj };
    if (!e.target) {
      const newArr = [...description];
      if (dataName == 'assetDealer') {
        if (e && e.toLowerCase() != 'others') {
          supplierObj[`assetDealer${parseInt(i) + 1}`] = false;
        } else {
          supplierObj[`assetDealer${parseInt(i) + 1}`] = true;
        }
        setSupplierOther({ ...supplierObj });
      } else if (dataName == 'assetType') {
        if (e && e.toLowerCase() != 'others') {
          assetTypeObj[`assetType${parseInt(i) + 1}`] = false;
        } else {
          assetTypeObj[`assetType${parseInt(i) + 1}`] = true;
        }
        setAssetTypeOther({ ...assetTypeObj });
      } else if (dataName === 'assetInstallationPlace') {
        if (e === 'Third Party Site arrangement(Revenue Sharing)') {
          thirdPartyAddressObj[
            `thirdPartyAddress${parseInt(i, 10) + 1}`
          ] = true;
        } else {
          thirdPartyAddressObj[
            `thirdPartyAddress${parseInt(i, 10) + 1}`
          ] = false;
          newArr[i].collateralLocation = '';
        }
        setThirdPartyAddress({ ...thirdPartyAddressObj });
      }
      newArr[i][dataName] = e;
      setDescription(newArr);
    } else {
      const newArr = [...description];
      if (dataName == 'assetDealer') {
        if (e.target.value.toLowerCase() != 'others') {
          supplierObj[`assetDealer${parseInt(i) + 1}`] = false;
        } else {
          supplierObj[`assetDealer${parseInt(i) + 1}`] = true;
        }
        setSupplierOther({ ...supplierObj });
      } else if (dataName == 'assetType') {
        if (e.target.value.toLowerCase() != 'others') {
          assetTypeObj[`assetType${parseInt(i) + 1}`] = false;
        } else {
          assetTypeObj[`assetType${parseInt(i) + 1}`] = true;
        }
        setAssetTypeOther({ ...assetTypeObj });
      } else if (dataName === 'assetInstallationPlace') {
        if (
          e.target.value === 'Third Party Site arrangement(Revenue Sharing)'
        ) {
          thirdPartyAddressObj[
            `thirdPartyAddress${parseInt(i, 10) + 1}`
          ] = true;
        } else {
          thirdPartyAddressObj[
            `thirdPartyAddress${parseInt(i, 10) + 1}`
          ] = false;
          newArr[i].collateralLocation = '';
        }
        setThirdPartyAddress({ ...thirdPartyAddressObj });
      }
      newArr[i][e.target.getAttribute('data-name')] = e.target.value;
      setDescription(newArr);
    }
  };

  const onPslChange = e => {
    setPslRadioValue(e.target.value);
  };

  const onMsmeChange = e => {
    setMsmeRadioValue(e.target.value);
  };

  /**
   * function to Delete Collateral
   */
  const deleteCollateral = (e, index) => {
    const arr = [...description];

    if (index > -1) {
      arr.splice(index, 1);
    }
    setDescription(arr);
    console.log(arr, description);
  };
  /**
   * Function to Add a Collateral
   */
  const addCollateral = () => {
    if (_.get(applicantDetails, 'entity.product') == 'HFS') {
      setDescription(
        description.concat({
          assetType: '',
          assetPrice: '',
          assetCondition: '',
          assetUnit: '',
          assetMarginMoney: '',
          assetInstallationPlace: '',
          collateralLocation: '',
          assetDealer: '',
          brand: '',
          assetModel: '',
          assetInvoicePrice: '',
          assetInvoiceNumber: '',
          assetValuationNRV: '',
          assetValuationOLV: '',
          assetSpeciality: '',
          marginMoney: '',
          assetMajorOem: '',
        }),
      );
    } else {
      setDescription(
        description.concat({
          address: '',
          valuation1: '',
          valuation2: '',
          collateralType: '',
        }),
      );
    }
  };

  const setFreeFieldsDefaultValue = () => {
    const freeFieldsArr = [];
    if (_.get(applicantDetails, 'entity.additionalData.data.extraDataField')) {
      for (const x in applicantDetails.entity.additionalData.data
        .extraDataField) {
        if (
          applicantDetails.entity.additionalData.data.extraDataField[x] &&
          masterFreeFieldsData[x]
        ) {
          freeFieldsArr.push(masterFreeFieldsData[x]);
        }
      }
      setFreeFields(freeFieldsArr);
      return freeFieldsArr;
    }
    return freeFieldsArr;
  };

  /**
   * Function to extract key & value from deviation data
   */
  const setDeviationDefaultValue = () => {
    const deviationArr = [];
    if (
      applicantDetails &&
      applicantDetails.entity &&
      applicantDetails.entity.additionalData &&
      applicantDetails.entity.additionalData.data &&
      applicantDetails.entity.additionalData.data.deviations
    ) {
      for (const x in applicantDetails.entity.additionalData.data.deviations) {
        if (
          x !== 'Eligible Amount' &&
          applicantDetails.entity.additionalData.data.deviations[x] !== ''
        ) {
          deviationArr.push(
            `${x}: ${
              applicantDetails.entity.additionalData.data.deviations[x]
            }`,
          );
        }
      }
      return deviationArr;
    }
    return deviationArr;
  };

  const setDeviationOptions = () => {
    const deviationArr = [];
    for (const x in masterDeviationData) {
      if (x !== 'Eligible Amount' && masterDeviationData[x] !== '') {
        deviationArr.push(`${x}: ${masterDeviationData[x]}`);
      }
    }
    return deviationArr;
  };

  const fillObject = (from, to) => {
    for (const key in from) {
      if (from.hasOwnProperty(key)) {
        if (Object.prototype.toString.call(from[key]) === '[object Object]') {
          if (!to.hasOwnProperty(key)) {
            to[key] = {};
          }
          fillObject(from[key], to[key]);
        } else if (!to.hasOwnProperty(key)) {
          to[key] = '';
        }
      }
    }
  };

  useEffect(() => {}, [type]);
  const onSubmit = data => {
    console.log(formState.dirty, 'dirte');
    const deviation = data.deviation ? data.deviation.slice() : [];
    const { product } = applicantDetails.entity;

    const allDeviations = masterDeviationData;
    const deviationObj = {};
    for (let i = 0; i < deviation.length; i++) {
      const split = deviation[i].split(':');
      deviationObj[split[0].trim()] = split[1].trim();
    }
    fillObject(allDeviations, deviationObj);
    const freeFieldsObj = {};
    Object.keys(masterFreeFieldsData).forEach(item => {
      if (data[item]) {
        freeFieldsObj[item] = data[item];
      } else {
        freeFieldsObj[item] = '';
      }
    });
    dispatch(
      submitOtherDetails(
        data,
        description,
        deviationObj,
        reset,
        values,
        freeFieldsObj,
        _.get(applicantDetails, 'entity.product'),
        masterAssetTypeData,
      ),
    );
    if(deviationObj['Debitable ABB']){
      setTimeout(() => {
        dispatch(callDelphi(appId));
      }, 1000);      
    }
    console.log(data, 'data in form submit');
    setResponse(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length > 0) {
      document.getElementById(Object.keys(errors)[0]).focus();
    }
  }, [errors]);

  useEffect(() => {
    if (
      applicantDetails.entity.additionalData &&
      applicantDetails.entity.additionalData.data &&
      applicantDetails.entity.additionalData.data.psl &&
      applicantDetails.entity.additionalData.data.psl.category !== ''
    ) {
      setPslRadioValue('Yes');
    }
  }, [applicantDetails]);

  useEffect(() => {
    if (otherDetails.response && response) {
      message.success(otherDetails.response);
      dispatch(loadEntityDetails());
      setResponse(false);
    }

    if (otherDetails.error && response) {
      message.error(otherDetails.error);
    }
  }, [otherDetails.response, otherDetails.error]);

  const appReferences = _.get(applicantDetails.entity, 'appReferences', []);
  // const appReferences = [{ firstName: 'Priya' }, { firstName: 'Rahul' }];

  useEffect(() => reset(), [applicantDetails.entity]);

  const onFreeFieldChange = value => {
    setFreeFields(value);
  };

  const setDefaultField = field => {
    const key = _.findKey(masterFreeFieldsData, function(o) {
      return o == field;
    });
    if (key && defautFreeFields) {
      return defautFreeFields[key] || '';
    }
    return '';
  };

  return (
    <div className="App">
      <h4>Other Details</h4>

      <Divider />
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* start */}
        {/* <Card> */}
        <Row gutter={[16, 16]}>
          <Col xs={0}>
            <InputField
              id="appCovenants"
              control={control}
              name="appCovenants"
              type="hidden"
              errors={errors}
            />
          </Col>
        </Row>
        <Row>
          <Col md={24}>
            <h5 style={{ fontWeight: 'bold' }}>Sourcing Details</h5>
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          <Col md={12}>
            <InputField
              id="creditManagerId"
              disabled
              control={control}
              name="creditManagerId"
              type="string"
              rules={{
                required: {
                  value: true,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Credit Manager"
              labelHtml="Credit Manager*"
            />
          </Col>
          <Col md={12}>
            <InputField
              disabled
              id="salesManager"
              control={control}
              name="salesManager"
              type="string"
              rules={{
                required: {
                  value: false,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Sales Manager"
              labelHtml="Sales Manager"
            />
          </Col>
          <Col md={12}>
            <InputField
              disabled
              id="sourcing"
              control={control}
              name="sourcing"
              type="string"
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="Sourcing"
              labelHtml="Sourcing*"
            />
          </Col>
          <Col md={12}>
            <InputField
              id="branch"
              control={control}
              name="branch"
              type="select"
              rules={{
                required: {
                  value: !isNoEntity,
                  message: 'This field cannot be left empty',
                },
              }}
              options={branchOptions}
              errors={errors}
              placeholder="Branch"
              labelHtml={!isNoEntity ? 'Branch*' : 'Branch'}
            />
          </Col>
          {_.get(applicantDetails, 'entity.partner') == 'DL' && (
            <Col md={12}>
              <InputField
                id="sourcingExecutive"
                control={control}
                name="sourcingExecutive"
                type="string"
                rules={{
                  required: {
                    value: false,
                    message: 'This field cannot be left empty',
                  },
                }}
                errors={errors}
                placeholder="DSA Name"
                labelHtml="DSA Name*"
                disabled
              />
            </Col>
          )}
        </Row>
        <Divider />
        {Object.keys(masterDeviationData).length > 0 && (
          <Row gutter={[16, 16]}>
            <Col md={12}>
              <Space>
                <Text>PSL*</Text>
                <Radio.Group
                  onChange={e => onPslChange(e)}
                  value={pslRadioValue}
                >
                  <Radio value="Yes">Yes</Radio>
                  <Radio value="No">No</Radio>
                </Radio.Group>
              </Space>
              {pslRadioValue === 'Yes' ? (
                <InputField
                  id="psl"
                  control={control}
                  name="psl"
                  type="select"
                  rules={{
                    required: {
                      value: true,
                      message: 'This Field should not be empty!',
                    },
                  }}
                  options={pslCategory}
                  errors={errors}
                />
              ) : null}
            </Col>
            <Col md={12}>
              <InputField
                mode="multiple"
                id="deviation"
                control={control}
                name="deviation"
                type="select"
                defaultValue={setDeviationDefaultValue}
                options={
                  _.get(applicantDetails, 'entity.partner') &&
                  setDeviationOptions()
                }
                rules={{
                  required: {
                    value: true,
                    message: 'This Field should not be empty!',
                  },
                }}
                errors={errors}
                placeholder="Deviation"
                labelHtml="Deviation*"
              />
            </Col>
          </Row>
        )}
        <Row gutter={[16, 16]}>
          <Col md={12}>
            <Space>
              <Text>Whether entity fall under MSME?</Text>
              <Radio.Group
                onChange={e => onMsmeChange(e)}
                value={msmeRadioValue}
              >
                <Radio value="Yes">Yes</Radio>
                <Radio value="No">No</Radio>
              </Radio.Group>
            </Space>
            {msmeRadioValue === 'Yes' ? (
              <InputField
                id="msme"
                control={control}
                name="msme"
                type="select"
                rules={{
                  required: {
                    value: true,
                    message: 'This Field should not be empty!',
                  },
                }}
                options={msmeCategory}
                errors={errors}
              />
            ) : null}
          </Col>
        </Row>
        <Row gutter={[16, 16]}>
          {_.get(applicantDetails, 'entity.product') === 'HFS' && (
            <Col md={12}>
              <InputField
                id="remarks"
                control={control}
                name="remarks"
                type="textarea"
                rules={{
                  required: {
                    value: false,
                    message: 'This Field should not be empty!',
                  },
                }}
                errors={errors}
                placeholder="Remarks"
                labelHtml="Remarks"
              />
            </Col>
          )}
          <Col md={12}>
            <InputField
              id="sanctionCondition"
              control={control}
              name="sanctionCondition"
              type="textarea"
              rules={{
                required: {
                  value: !isNoEntity,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Sanction Condition"
              labelHtml={
                !isNoEntity ? 'Sanction Condition*' : 'Sanction Condition'
              }
            />
          </Col>
          <Col md={12}>
            <InputField
              id="recommendationNote"
              control={control}
              name="recommendationNote"
              type="textarea"
              rules={{
                required: {
                  value: !isNoEntity,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Recommendation Note"
              labelHtml={
                !isNoEntity ? 'Recommendation Note*' : 'Recommendation Note'
              }
            />
          </Col>
          {activity === 'CREDIT_PSV' && (
            <Col md={12}>
              <InputField
                id="otcFlag"
                control={control}
                name="otcFlag"
                type="select"
                options={['true', 'false']}
                errors={errors}
                placeholder="OTC Flag"
                labelHtml="OTC Flag"
              />
            </Col>
          )}
        </Row>
        <Divider />
        {appReferences && appReferences.length > 0 && (
          <>
            <Col md={20}>
              <h5 style={{ fontWeight: 'bold' }}>References</h5>
            </Col>
            <References
              control={control}
              type="string"
              errors={errors}
              values={values}
              appReferences={appReferences}
            />
          </>
        )}
        {/* <Card> */}
        <Divider />
        <Row gutter={[16, 16]}>
          <Col md={20}>
            <h5 style={{ fontWeight: 'bold' }}>Collateral Details</h5>
          </Col>
          <Col md={4}>
            <Button
              onClick={() => {
                addCollateral();
              }}
              type="primary"
            >
              Add Collateral
            </Button>
          </Col>
        </Row>
        <Divider />
        {description.length > 0 &&
          description.map((item, index) => (
            <div key={index}>
              <Row gutter={[16, 16]}>
                <Col md={12}>
                  <h6 style={{ fontWeight: 'bold' }}>Collateral {index + 1}</h6>
                </Col>
                <Col md={12}>
                  <Button
                    onClick={e => {
                      deleteCollateral(e, index);
                    }}
                    // type="primary"
                    style={{ float: 'right' }}
                  >
                    Delete Collateral
                  </Button>
                </Col>
                <Divider />
                {/* <h6>Bank Details</h6> */}
                {_.get(applicantDetails, 'entity.product') == 'HFS' ? (
                  <>
                    <Col md={12}>
                      <InputField
                        handleChange={e => updateCollateral(e, index, 'brand')}
                        id={`brand${index + 1}`}
                        control={control}
                        name={`brand${index + 1}`}
                        data-name="brand"
                        type="select"
                        defaultValue={item.brand}
                        rules={{
                          required: {
                            value: true,
                            message: 'This Field should not be empty!',
                          },
                        }}
                        options={Object.values(masterManufacturerData) || []}
                        errors={errors}
                        placeholder="Select Manufacturer"
                        labelHtml="Select Manufacturer*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        handleChange={e =>
                          updateCollateral(e, index, 'assetType')
                        }
                        id={`assetType${index + 1}`}
                        control={control}
                        name={`assetType${index + 1}`}
                        data-name="assetType"
                        type="select"
                        defaultValue={item.assetType}
                        rules={{
                          required: {
                            value: true,
                            message: 'This Field should not be empty!',
                          },
                        }}
                        options={Object.keys(masterAssetTypeData) || []}
                        errors={errors}
                        placeholder="Asset Type"
                        labelHtml="Asset Type*"
                      />
                    </Col>
                    {assetTypeOther && assetTypeOther[`assetType${index + 1}`] && (
                      <Col md={12}>
                        <InputField
                          id={`assetTypeTextValue${index + 1}`}
                          control={control}
                          name={`assetTypeTextValue${index + 1}`}
                          type="string"
                          data-name="assetTypeTextValue"
                          handleChange={e => {
                            updateCollateral(e, index);
                          }}
                          defaultValue={item.assetTypeTextValue}
                          rules={{
                            required: {
                              value: true,
                              message: 'This field cannot be left empty',
                            },
                          }}
                          errors={errors}
                          placeholder="Asset Type"
                          labelHtml="Asset Type*"
                        />
                      </Col>
                    )}
                    <Col md={12}>
                      <InputField
                        id={`assetPrice${index + 1}`}
                        defaultValue={item.assetPrice}
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        control={control}
                        data-name="assetPrice"
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        name={`assetPrice${index + 1}`}
                        type="string"
                        errors={errors}
                        placeholder="Unit Price"
                        labelHtml="Unit Price*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetCondition${index + 1}`}
                        control={control}
                        name={`assetCondition${index + 1}`}
                        type="select"
                        data-name="assetCondition"
                        handleChange={e =>
                          updateCollateral(e, index, 'assetCondition')
                        }
                        defaultValue={item.assetCondition}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        options={Object.keys(masterAssetConditionData) || []}
                        errors={errors}
                        placeholder="Asset Condition"
                        labelHtml="Asset Condition*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetUnit${index + 1}`}
                        control={control}
                        name={`assetUnit${index + 1}`}
                        type="string"
                        data-name="assetUnit"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.assetUnit}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Units Required"
                        labelHtml="Units Required*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetMarginMoney${index + 1}`}
                        control={control}
                        name={`assetMarginMoney${index + 1}`}
                        type="select"
                        data-name="assetMarginMoney"
                        handleChange={e =>
                          updateCollateral(e, index, 'assetMarginMoney')
                        }
                        defaultValue={item.assetMarginMoney}
                        options={MARGIN_MONEY}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Margin Money"
                        labelHtml="Margin Money*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`marginMoney${index + 1}`}
                        control={control}
                        name={`marginMoney${index + 1}`}
                        type="string"
                        data-name="marginMoney"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.marginMoney}
                        rules={{
                          required: {
                            value: false,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Margin Money Amount"
                        labelHtml="Margin Money Amount"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetInstallationPlace${index + 1}`}
                        control={control}
                        name={`assetInstallationPlace${index + 1}`}
                        type="select"
                        data-name="assetInstallationPlace"
                        handleChange={e =>
                          updateCollateral(e, index, 'assetInstallationPlace')
                        }
                        defaultValue={item.assetInstallationPlace}
                        options={ASSET_DEPLOYMENT}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Asset Deployment"
                        labelHtml="Asset Deployment*"
                      />
                    </Col>
                    {_.get(applicantDetails, 'entity.product') === 'HFS' &&
                      _.get(
                        thirdPartyAddress,
                        `[thirdPartyAddress${index + 1}]`,
                        false,
                      ) && (
                        <Col md={12}>
                          <InputField
                            id={`collateralLocation${index + 1}`}
                            control={control}
                            name={`collateralLocation${index + 1}`}
                            type="textarea"
                            data-name="collateralLocation"
                            handleChange={e =>
                              updateCollateral(e, index, 'collateralLocation')
                            }
                            defaultValue={item.collateralLocation}
                            rules={{
                              required: {
                                value: false,
                                message: 'This field cannot be left empty',
                              },
                            }}
                            errors={errors}
                            placeholder="Third Party Address"
                            labelHtml="Third Party Address*"
                          />
                        </Col>
                      )}
                    {/* <Col md={12}>
                      <InputField
                        id={`downPaymentMode${index + 1}`}
                        control={control}
                        name={`downPaymentMode${index + 1}`}
                        type="string"
                        data-name="downPaymentMode"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.downPaymentMode}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Down Payment Mode"
                        labelHtml="Down Payment Mode*"
                      />
                    </Col> */}
                    <Col md={12}>
                      <InputField
                        id={`assetDealer${index + 1}`}
                        control={control}
                        name={`assetDealer${index + 1}`}
                        type="select"
                        data-name="assetDealer"
                        handleChange={e => {
                          updateCollateral(e, index, 'assetDealer');
                        }}
                        options={Object.values(masterSupplierData) || []}
                        defaultValue={item.assetDealer}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Name of Supplier"
                        labelHtml="Name of Supplier*"
                      />
                    </Col>
                    {supplierOther && supplierOther[`assetDealer${index + 1}`] && (
                      <Col md={12}>
                        <InputField
                          id={`supplierTextValue${index + 1}`}
                          control={control}
                          name={`supplierTextValue${index + 1}`}
                          type="string"
                          data-name="supplierTextValue"
                          handleChange={e => {
                            updateCollateral(e, index);
                          }}
                          defaultValue={item.supplierTextValue}
                          rules={{
                            required: {
                              value: true,
                              message: 'This field cannot be left empty',
                            },
                          }}
                          errors={errors}
                          placeholder="Other Supplier"
                          labelHtml="Other Supplier*"
                        />
                      </Col>
                    )}
                    <Col md={12}>
                      <InputField
                        id={`assetModel${index + 1}`}
                        control={control}
                        name={`assetModel${index + 1}`}
                        type="string"
                        data-name="assetModel"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.assetModel}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Machine Model"
                        labelHtml="Machine Model*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetInvoicePrice${index + 1}`}
                        control={control}
                        name={`assetInvoicePrice${index + 1}`}
                        type="number"
                        data-name="assetInvoicePrice"
                        handleChange={e => {
                          updateCollateral(e, index, 'assetInvoicePrice');
                        }}
                        defaultValue={item.assetInvoicePrice}
                        rules={{
                          required: {
                            value: activity === 'GAM_REVIEW',
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Asset Invoice Price"
                        labelHtml={
                          activity === 'GAM_REVIEW'
                            ? `Asset Invoice Price*`
                            : `Asset Invoice Price`
                        }
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetInvoiceNumber${index + 1}`}
                        control={control}
                        name={`assetInvoiceNumber${index + 1}`}
                        type="string"
                        data-name="assetInvoiceNumber"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.assetInvoiceNumber}
                        rules={{
                          required: {
                            value: activity === 'GAM_REVIEW',
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Asset Invoice Number"
                        labelHtml={
                          activity === 'GAM_REVIEW'
                            ? `Asset Invoice Number*`
                            : `Asset Invoice Number`
                        }
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetValuationNRV${index + 1}`}
                        control={control}
                        name={`assetValuationNRV${index + 1}`}
                        type="number"
                        data-name="assetValuationNRV"
                        handleChange={e => {
                          updateCollateral(e, index, 'assetValuationNRV');
                        }}
                        defaultValue={item.assetValuationNRV}
                        rules={{
                          required: {
                            value: activity === 'GAM_REVIEW',
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Asset Valuation Day1 NRV"
                        labelHtml={
                          activity === 'GAM_REVIEW'
                            ? `Asset Valuation Day1 NRV*`
                            : `Asset Valuation Day1 NRV`
                        }
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetValuationOLV${index + 1}`}
                        control={control}
                        name={`assetValuationOLV${index + 1}`}
                        type="number"
                        data-name="assetValuationOLV"
                        handleChange={e => {
                          updateCollateral(e, index, 'assetValuationOLV');
                        }}
                        defaultValue={item.assetValuationOLV}
                        rules={{
                          required: {
                            value: activity === 'GAM_REVIEW',
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Asset Valuation Day1 OLV"
                        labelHtml={
                          activity === 'GAM_REVIEW'
                            ? `Asset Valuation Day1 OLV*`
                            : `Asset Valuation Day1 OLV`
                        }
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetSpeciality${index + 1}`}
                        control={control}
                        name={`assetSpeciality${index + 1}`}
                        type="string"
                        data-name="assetSpeciality"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.assetSpeciality}
                        rules={{
                          required: {
                            value: activity === 'GAM_REVIEW',
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Asset Description"
                        labelHtml={
                          activity === 'GAM_REVIEW'
                            ? `Asset Description*`
                            : `Asset Description`
                        }
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`assetMajorOem${index + 1}`}
                        control={control}
                        name={`assetMajorOem${index + 1}`}
                        type="string"
                        data-name="assetMajorOem"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.assetMajorOem}
                        rules={{
                          required: {
                            value: activity === 'GAM_REVIEW',
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Asset Major OEM"
                        labelHtml={
                          activity === 'GAM_REVIEW'
                            ? `Asset Major OEM*`
                            : `Asset Major OEM`
                        }
                      />
                    </Col>
                  </>
                ) : (
                  <>
                    <Col md={12}>
                      <InputField
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        id={`address${index + 1}`}
                        control={control}
                        name={`address${index + 1}`}
                        data-name="address"
                        type="text"
                        defaultValue={item.address}
                        rules={{
                          required: {
                            value: true,
                            message: 'This Field should not be empty!',
                          },
                        }}
                        errors={errors}
                        placeholder="Address"
                        labelHtml="Address*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`valuation1${index + 1}`}
                        defaultValue={item.valuation1}
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        control={control}
                        data-name="valuation1"
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        name={`valuation1${index + 1}`}
                        type="string"
                        errors={errors}
                        placeholder="Valuation 1"
                        labelHtml="Valuation 1*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`valuation2${index + 1}`}
                        control={control}
                        name={`valuation2${index + 1}`}
                        type="string"
                        data-name="valuation2"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.valuation2}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Valuation 2"
                        labelHtml="Valuation 2*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`collateralType${index + 1}`}
                        control={control}
                        name={`collateralType${index + 1}`}
                        type="string"
                        data-name="collateralType"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.collateralType}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        errors={errors}
                        placeholder="Collateral Type"
                        labelHtml="collateral Type*"
                      />
                    </Col>
                    <Col md={12}>
                      <InputField
                        id={`ltv${index + 1}`}
                        control={control}
                        name={`ltv${index + 1}`}
                        type="string"
                        data-name="ltv"
                        handleChange={e => {
                          updateCollateral(e, index);
                        }}
                        defaultValue={item.ltv}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                          pattern: /^([0-9]){1,}$/,
                        }}
                        errors={errors}
                        placeholder="LTV"
                        labelHtml="LTV*"
                      />
                    </Col>
                  </>
                )}
                <Divider />
              </Row>
            </div>
          ))}
        {/* D2CP2-1303: add otc pdd start */}
        {activity === 'CREDIT_PSV' && (
          <>
            <Divider />
            <Row gutter={[16, 16]}>
              <Col md={20}>
                <h5 style={{ fontWeight: 'bold' }}>OTC/PDD</h5>
              </Col>
              <OtcPdd
                responsiveColumns={responsiveColumns}
                values={values}
                setValue={setValue}
                masterConvenantTypeData={masterConvenantTypeData}
                initDataSource={_.get(applicantDetails, 'entity.appCovenants')}
                disabled={requestType !== 'PMA'}
              />
            </Row>
          </>
        )}
        {/* D2CP2-1303: add otc pdd ending */}
        <Divider />
        <Col md={20}>
          <h5 style={{ fontWeight: 'bold' }}>PSL Details</h5>
        </Col>

        <Row gutter={[16, 16]}>
          <Col {...responsiveColumns}>
            <PslCategory
              id="appPslDetail.categoryCode"
              control={control}
              name="appPslDetail.categoryCode"
              type="select"
              setValue={setValue}
              errors={errors}
              values={values}
            />
          </Col>
          <Col {...responsiveColumns}>
            <PslWeakerSection
              id="appPslDetail.weakerSection"
              control={control}
              name="appPslDetail.weakerSection"
              type="select"
              setValue={setValue}
              errors={errors}
              values={values}
            />
          </Col>
          <Col {...responsiveColumns}>
            <PslSector
              id="appPslDetail.sector"
              control={control}
              name="appPslDetail.sector"
              type="select"
              setValue={setValue}
              errors={errors}
              values={values}
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="appPslDetail.investment"
              control={control}
              name="appPslDetail.investment"
              type="string"
              rules={{
                required: {
                  value: false,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Investment"
              labelHtml="Investment*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="appPslDetail.landHolding"
              control={control}
              name="appPslDetail.landHolding"
              type="string"
              rules={{
                required: {
                  value: false,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Land Holding"
              labelHtml="Land Holding*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="appPslDetail.amount"
              control={control}
              name="appPslDetail.amount"
              type="string"
              rules={{
                required: {
                  value: false,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Amount"
              labelHtml="Amount*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="appPslDetail.landArea"
              control={control}
              name="appPslDetail.landArea"
              type="string"
              rules={{
                required: {
                  value: false,
                  message: 'This Field should not be empty!',
                },
              }}
              errors={errors}
              placeholder="Land Area"
              labelHtml="Land Area"
            />
          </Col>
          <Col {...responsiveColumns}>
            <PslSubCategory
              id="appPslDetail.subCategory"
              control={control}
              name="appPslDetail.subCategory"
              type="select"
              setValue={setValue}
              errors={errors}
              values={values}
            />
          </Col>
          <Col {...responsiveColumns}>
            <PslPurpose
              id="appPslDetail.purpose"
              control={control}
              name="appPslDetail.purpose"
              type="select"
              setValue={setValue}
              errors={errors}
              values={values}
            />
          </Col>
          <Col {...responsiveColumns}>
            <PslEndUse
              id="appPslDetail.endUse"
              control={control}
              name="appPslDetail.endUse"
              type="select"
              setValue={setValue}
              errors={errors}
              values={values}
            />
          </Col>
        </Row>

        {Object.keys(masterFreeFieldsData).length > 0 && (
          <>
            <Divider />
            <Col md={20}>
              <h5 style={{ fontWeight: 'bold' }}>Additional Fields</h5>
            </Col>
            <Row gutter={[16, 16]}>
              <Col md={12}>
                <InputField
                  mode="multiple"
                  id="addFields"
                  control={control}
                  name="addFields"
                  type="select"
                  defaultValue={setFreeFieldsDefaultValue}
                  options={Object.values(masterFreeFieldsData)}
                  errors={errors}
                  handleChange={onFreeFieldChange}
                  placeholder="Add Fields"
                  labelHtml="Add Fields"
                />
              </Col>
            </Row>
            <Row gutter={[16, 16]}>
              {freeFields &&
                freeFields.length > 0 &&
                freeFields.map(item => (
                  <Col md={12}>
                    <InputField
                      id={_.findKey(masterFreeFieldsData, function(o) {
                        return o == item;
                      })}
                      control={control}
                      name={_.findKey(masterFreeFieldsData, function(o) {
                        return o == item;
                      })}
                      rules={{
                        required: {
                          value: true,
                          message: 'This field cannot be left empty',
                        },
                      }}
                      type="string"
                      defaultValue={setDefaultField(item)}
                      errors={errors}
                      placeholder={item}
                      labelHtml={item}
                    />
                  </Col>
                ))}
            </Row>
          </>
        )}

        <Form.Item {...tailFormItemLayout}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'}
          >
            Save
          </Button>
        </Form.Item>
      </form>
    </div>
  );
}

OtherDetails.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  applicantDetails: makeSelectApplicantDetails2(),
  otherDetails: makeSelectOtherDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(OtherDetails);
