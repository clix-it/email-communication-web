/*
 * OtherDetails Messages
 *
 * This contains all the text for the OtherDetails container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.OtherDetails';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the OtherDetails container!',
  },
});
