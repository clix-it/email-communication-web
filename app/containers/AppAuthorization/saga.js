import { put, takeEvery, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import env from '../../../environment';
import * as actions from './constants';
import { authorizationSuccess, authorizationFailed } from './actions';
import { registerUser, logUserIn } from '../App/actions';

export function* checkAuthorize(action) {
  const { activity } = action.payload;
  const userId = JSON.parse(sessionStorage.getItem('userId'));
  const requestURL = `${env.API_URL}/${
    env.MAKER_CHECKER_API_URL
  }/v1/userRole/fetch?userId=${userId}&activity=${activity}`;
  try {
    const options = {
      method: 'GET',
      headers: env.headers,
    };
    const response = yield call(request, requestURL, options);
    if (response.status) {
      yield put(
        authorizationSuccess(activity, response.data[0].role, response.data),
      );
      yield put(registerUser(userId, response.data[0].role));
      yield put(logUserIn(userId, response.data[0].role));
      sessionStorage.setItem('userRole', JSON.stringify(response.data[0].role));
    } else {
      yield put(authorizationFailed(response.message));
      message.info(`${response.message}!`);
    }
  } catch (error) {
    yield put(authorizationFailed(error));
    message.error('Network error occurred! Check connection!');
  }
}

export default function* appAuthorizationSaga() {
  yield takeEvery(actions.CHECK_AUTHORIZATION, checkAuthorize);
}
