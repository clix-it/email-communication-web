import * as actions from './constants';

export function loadCustomerDetailsInit() {
  return {
    type: actions.LOAD_CUSTOMER_DETAILS_INIT,
  };
}

export function loadCustomerDetails(cuid) {
  return {
    type: actions.LOAD_CUSTOMER_DETAILS,
    cuid,
  };
}

export function loadCustomerDetailsSuccess(user) {
  return {
    type: actions.LOAD_CUSTOMER_DETAILS_SUCCESS,
    user
  };
}

export function loadCustomerDetailsError(message) {
  return {
    type: actions.LOAD_CUSTOMER_DETAILS_ERROR,
    message,
  };
}
