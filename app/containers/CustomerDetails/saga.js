import _ from 'lodash';
import { takeEvery, put, select, all, call } from 'redux-saga/effects';
import request from 'utils/request';
import { message } from 'antd';
import * as actions from './constants';
import env from '../../../environment';
import {
  loadCustomerDetailsInit,
  loadCustomerDetailsSuccess,
  loadCustomerDetailsError
} from './actions';

function* fetchCustomerDetailsSaga({cuid}) {
  yield put(loadCustomerDetailsInit());
  try{
    const reqUrl = `${env.USER_SERVICE_API_URL}/${cuid}`;
    const options = {
      method: 'GET',
      headers: env.headers,
    };
    const response = yield call(request, reqUrl, options);
    if (response.success || response.status) {
      yield put(loadCustomerDetailsSuccess(response.user));
    } else {
      yield put(loadCustomerDetailsError(response.message || 'user not found'));
      message.info(response.message || 'user not found');
    }
  } catch(err){
    yield put(loadCustomerDetailsError('User not found'));
    message.info('User not found');
  }
}

export default function* creditPortalSaga() {
  yield takeEvery(actions.LOAD_CUSTOMER_DETAILS, fetchCustomerDetailsSaga);
}
