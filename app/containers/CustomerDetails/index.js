import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';

import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import Tabs from '../../components/Tabs';
import CustomerInfo from '../../components/CustomerInfo';
import CustomerKycTabComponent from '../../components/CustomerKycTabComponent';
import CustomerDetailsPageHeader from '../../components/CustomerDetailsPageHeader';
import CustomerDocuments from '../../containers/CustomerDocuments';
import CustomerLoanDetails from '../../containers/CustomerLoanDetails';
import RelationshipDetails from '../../containers/RelationshipDetails';

import { loadCustomerDetails } from './actions';
import { makeSelectCustomerDetails } from './selectors';

export function CustomerDetails({
  dispatch,
  match,
  customerDetails,
}) {
  const { cuid } = match.params;
  const {
    userDetails = {},
    userArray = [],
    loading = false,
    isUserChanged = false,
    newUser = false,
    mainCuid = false,
  } = customerDetails;

  useEffect(() => {
    if(newUser){
      dispatch(loadCustomerDetails(newUser));
    } else {
      const user = _.find(userArray, { cuid: cuid });
      if(!user){
        dispatch(loadCustomerDetails(cuid));
      }      
    }
  }, [cuid, newUser]);

  const tabsToShow = [
    {
      id: 'entityDetails',
      name: 'Customer Details',
      component: <CustomerInfo user={userDetails} loading={loading} />,
    },
    {
      id: 'kycDetails',
      name: 'KYC Details',
      component: (
        <CustomerKycTabComponent
          user={userDetails}
          cuid={mainCuid}
        />
      ),
    },
    {
      id: 'documents',
      name: 'Documents',
      component: <CustomerDocuments user={userDetails} cuid={mainCuid}/>,
    },
    {
      id: 'applicationDetails',
      name: 'Application Details',
      component: <CustomerLoanDetails user={userDetails} cuid={mainCuid}/>,
    },
    {
      id: 'relationships',
      name: 'Relationships',
      component: <RelationshipDetails user={userDetails} cuid={mainCuid}/>,
    },
  ];

  return (
    <>
      <CustomerDetailsPageHeader
        userDetails={userDetails}
        userArray={userArray}
        dispatch={dispatch}
      />
      <Tabs tabPanel={tabsToShow} isUserChanged={isUserChanged}/>
    </>
  );
}

CustomerDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
  match: PropTypes.object,
  creditData: PropTypes.object,
  appDocumentDetails: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  customerDetails: makeSelectCustomerDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

CustomerDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'customerDetails', reducer });

const withSaga = useInjectSaga({ key: 'customerDetails', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(CustomerDetails);
