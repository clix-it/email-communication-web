import _ from 'lodash';
import moment from 'moment';
import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectCustomerDetailsDomain = state =>
  state.customerDetails || initialState;

const makeSelectCustomerDetails = () =>
  createSelector(
    selectCustomerDetailsDomain,
    substate => substate,
  );

export {
  makeSelectCustomerDetails
};