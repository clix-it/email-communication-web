/**
 * Reducer
 */

import _ from 'lodash';
import produce from 'immer';
import * as actions from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';
import { SET_MAIN_USER } from '../RelationshipDetails/constants';

export const initialState = {
  loading: false,
  error: {},
  userDetails: {},
  userArray: [],
  isUserChanged: false,
  newUser: false,
  mainCuid: false,
};

const customerDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.LOAD_CUSTOMER_DETAILS_INIT:
        draft.loading = true;
        break;
      case actions.LOAD_CUSTOMER_DETAILS_SUCCESS:
        draft.loading = false;
        draft.userDetails = action.user;
        draft.mainCuid = action.user.cuid;
        draft.userArray = draft.userArray.length
          ? draft.userArray.concat(action.user)
          : [action.user];
        draft.isUserChanged = false;
        draft.newUser = false;
        break;
      case actions.LOAD_CUSTOMER_DETAILS_ERROR:
        draft.loading = false;
        draft.error = action.message;
        break;
      case SET_MAIN_USER:
        draft.isUserChanged = true;
        draft.newUser = action.cuid;
        draft.userDetails = {};
        debugger;
        draft.userArray.length = action.index ? action.index - 1 : draft.userArray.length
        break;
      case LOCATION_CHANGE:
        draft.loading = false;
        draft.userDetails = {};
        draft.mainCuid = false;
        draft.userArray = [];
        draft.isUserChanged = false;
        draft.newUser = false;
        break;
      default: {
        return state;
      }
    }
  });

export default customerDetailsReducer;
