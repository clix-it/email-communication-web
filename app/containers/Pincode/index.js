/**
 *
 * Pincode
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { Col } from 'antd';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import InputField from 'components/InputField';
import makeSelectPincode, { makeSelectLocation } from './selectors';
import reducer from './reducer';
import saga from './saga';
import { getLocation } from './actions';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

export function Pincode({
  getLocation,
  errors,
  pincodeData,
  pincode,
  error,
  setError,
  clearError,
  setLoad,
  values,
  setValue,
  namePrefix,
  reset,
  rules,
  defaultValue,
  ...rest
}) {
  const location = pincodeData[_.get(values, `${namePrefix}.pincode`)];
  useInjectReducer({ key: 'pincode', reducer });
  useInjectSaga({ key: 'pincode', saga });

  // Here's where the API call happens
  // We use useEffect since this is an asynchronous action
  useEffect(() => {
    if ((_.get(values, `${namePrefix}.pincode`) || '').length === 6) {
      getLocation(_.get(values, `${namePrefix}.pincode`));
    }
  }, [_.get(values, `${namePrefix}.pincode`)]);

  useEffect(
    () => () => {
      clearError(`${namePrefix}.locality`);
    },
    [],
  );

  useEffect(() => {
    if (pincode.error) {
      setError(
        `${namePrefix}.pincode`,
        'pattern',
        'No location for this zipcode',
      );
    }
    return () => {
      clearError(`${namePrefix}.locality`);
    };
  }, [pincode.error]);

  useEffect(() => {
    setValue(`${namePrefix}.country`, 'India');
    if (location) {
      const localityChanged = _.includes(
        _.get(location, 'states[0].cities[0].localities'),
        _.get(values, `${namePrefix}.locality`),
      );
      setValue(`${namePrefix}.city`, location.states[0].cities[0].name);
      setValue(`${namePrefix}.state`, location.states[0].name);
      if (!localityChanged) {
        //setValue(`${namePrefix}.locality`, '');
      }
    } else {
      setValue(`${namePrefix}.city`, '');
      setValue(`${namePrefix}.state`, '');
      //setValue(`${namePrefix}.locality`, '');
    }
  }, [location]);

  console.log('pincode', pincode);
  return (
    <>
      <Col {...responsiveColumns}>
        <InputField
          type="text"
          className="form-control"
          id={`${namePrefix}.pincode`}
          name={`${namePrefix}.pincode`}
          placeholder="Pincode"
          defaultValue={defaultValue ? defaultValue.pincode : ''}
          errors={errors}
          rules={rules}
          {...rest}
          labelHtml={
            <label htmlFor="pin">{`Pincode ${
              location
                ? `(${location.states[0].cities[0].name}, ${
                    location.states[0].name
                  })`
                : 'Current Address'
            }*`}</label>
          }
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          {...rest}
          type="select"
          rules={{
            required: true,
            message: 'This field is required',
          }}
          className="form-control"
          id={`${namePrefix}.locality`}
          handleChange={() => clearError(`${namePrefix}.locality`)}
          name={`${namePrefix}.locality`}
          defaultValue={defaultValue ? defaultValue.locality : ''}
          placeholder="Locality"
          options={_.get(location, 'states[0].cities[0].localities')}
          errors={errors}
          labelHtml={<label htmlFor="locality">Locality*</label>}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          {...rest}
          rules={{
            required: true,
            message: 'This field is required',
          }}
          type="string"
          className="form-control"
          id={`${namePrefix}.city`}
          name={`${namePrefix}.city`}
          disabled
          placeholder="City"
          errors={errors}
          labelHtml={<label htmlFor="city">City</label>}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          {...rest}
          rules={{
            required: true,
            message: 'This field is required',
          }}
          type="string"
          className="form-control"
          id={`${namePrefix}.state`}
          name={`${namePrefix}.state`}
          disabled
          placeholder="State"
          errors={errors}
          labelHtml={<label htmlFor="State">State</label>}
        />
      </Col>
      <Col {...responsiveColumns}>
        <InputField
          {...rest}
          rules={{
            required: true,
            message: 'This field is required',
          }}
          type="string"
          className="form-control"
          id={`${namePrefix}.country`}
          name={`${namePrefix}.country`}
          disabled
          placeholder="Country"
          errors={errors}
          labelHtml={<label htmlFor="Country">Country</label>}
        />
      </Col>
    </>
  );
}

Pincode.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  register: PropTypes.any.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pincode: makeSelectPincode(),
  pincodeData: makeSelectLocation(),
});

function mapDispatchToProps(dispatch) {
  return {
    getLocation: search => dispatch(getLocation(search)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Pincode);
