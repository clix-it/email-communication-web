/*
 *
 * Pincode constants
 *
 */

export const GET_LOCATION = 'app/Pincode/GET_LOCATION';
export const GET_LOCATION_SUCCESS = 'app/Pincode/GET_LOCATION_SUCCESS';
export const GET_LOCATION_ERROR = 'app/Pincode/GET_LOCATION_ERROR';
