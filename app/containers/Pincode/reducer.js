/*
 *
 * Pincode reducer
 *
 */
import produce from 'immer';
import {
  GET_LOCATION,
  GET_LOCATION_ERROR,
  GET_LOCATION_SUCCESS,
  SET_LOADING,
} from './constants';

export const initialState = {
  pincode: false,
  location: {},
  loading: true,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const pincodeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_LOCATION:
        draft.pincode = action.pincode;
        draft.error = false;
        draft.loading = true;
        break;
      case GET_LOCATION_SUCCESS:
        draft.location = {
          ...draft.location,
          [action.pincode]: action.response,
        };
        draft.loading = false;
        draft.error = false;
        break;
      case GET_LOCATION_ERROR:
        draft.loading = false;
        draft.error = action.error;
        break;
    }
  });

export default pincodeReducer;
