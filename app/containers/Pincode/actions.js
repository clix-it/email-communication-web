/*
 *
 * Pincode actions
 *
 */

import {
  GET_LOCATION,
  GET_LOCATION_ERROR,
  GET_LOCATION_SUCCESS,
} from './constants';

export function getLocation(pincode) {
  return {
    type: GET_LOCATION,
    pincode,
  };
}

export function getLocationSuccess(data, pincode) {
  return {
    type: GET_LOCATION_SUCCESS,
    response: data,
    pincode,
  };
}

export function getLocationError(error) {
  return {
    type: GET_LOCATION_ERROR,
    error,
  };
}
