import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import { PINCODE_API_URL, HEADERS } from 'containers/App/constants';
import request from 'utils/request';
import { GET_LOCATION } from './constants';
import env from '../../../environment';
import { getLocationSuccess, getLocationError } from './actions';
// Individual exports for testing

export function* getLocationByPincode({ pincode }) {
  const requestURL = `${env.API_URL}/master/v1/zipcode?zipCode=${pincode}`;

  try {
    // Call our request helper (see 'utils/request')
    const options = {
      headers: env.headers,
    };
    const response = yield call(request, requestURL, options);
    if (response.data) {
      yield put(getLocationSuccess(response.data, pincode));
    } else {
      yield put(getLocationError(response.message));
    }
  } catch (err) {
    yield put(getLocationError(err));
  }
}

export default function* pincodeSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(GET_LOCATION, getLocationByPincode);
}
