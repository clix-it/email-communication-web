/**
 *
 * GuarantorPage
 *
 */

import _ from 'lodash';
import moment from 'moment';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { useForm } from 'react-hook-form';
import { compose } from 'redux';
import { Button, Col, Row, message } from 'antd';
import GuarantorForm from 'components/GuarantorForm';
import makeSelectPincode from 'containers/Pincode/selectors';

import { revertFormatDate } from 'utils/helpers';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import {
  generateCibilReport,
  setFormValidationError,
  generateDelphiReport,
} from 'containers/ApplicantDetails/actions';
import makeSelectGuarantorPage from './selectors';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';

import reducer from './reducer';
import saga from './saga';
import { fetchApplicant } from './actions';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 10,
};
export function GuarantorPage({
  appId,
  cuid,
  fetchApplicant,
  generateCibilReport,
  generateDelphiReport,
  guarantorPage,
  pincodeDetails,
  triggerValidation,
  id,
  entity,
  applicant,
  errors,
  dispatch,
  requestType,
  reset,
  applicantDetails,
  contactArr,
  isNoEntity,
  activity,
  product,
  partner,
  defaultValues,
  values,
  setValue,
  clearError,
  APPLICANTTYPE,
  ...rest
}) {
  const { formValidationErrors } = applicantDetails;
  const validationObj = _.get(formValidationErrors, 'guarantorDetails', {});

  useEffect(() => {
    if (
      requestType === 'PMA' &&
      applicant.id &&
      !String(applicant.id).includes('NEW')
    ) {
      if (
        !validationObj ||
        (validationObj &&
          Object.keys(validationObj).length !== entity.guarantors.length)
      ) {
        entity.guarantors.forEach(app => {
          validationObj[app.id] = false;
        });
      }
      debugger;
      triggerValidation().then(valid => {
        dispatch(
          setFormValidationError('guarantorDetails', {
            ...validationObj,
            [applicant.id]: valid,
          }),
        );
      });
    }
    reset(defaultValues);
  }, [applicant]);

  const handleGenerateCibilReport = applicant => {
    const contactibility =
      _.find(applicant.contactibilities, function(o) {
        return o.pincode && o.state && o.city;
      }) || {};
    const { pincode, city, state } = contactibility;
    const gender = _.get(applicant, 'userDetails.gender');
    const CIBIL_DATA = {
      applicantSegment: {
        applicantName: {
          name1: applicant.firstName,
          name3: applicant.lastName,
        },
        dob: {
          dobDate: revertFormatDate(
            applicant.dateOfBirthIncorporation,
            'DD-MM-YYYY',
            '',
          ),
        },
        gender,
        ids: [{ type: 'PAN', value: applicant.userIdentities.pan }],
        addresses: [
          {
            type: 'Residence',
            address1: `${_.get(
              applicant,
              'contactibilities[0].addressLine1',
            )} ${_.get(applicant, 'contactibilities[0].addressLine2')}`,
            city,
            state,
            pincode,
          },
        ],
        entityId: 'eNTITY23',
        phones: [
          {
            type: 'MOBILE',
            value: applicant.preferredPhone,
          },
        ],
        emails: [
          {
            value: applicant.preferredEmail,
          },
        ],
      },
      applicationSegment: {
        loanType: isNoEntity ? 'PB STOCK' : 'BUSINESSL',
        consumerId: applicant.cuid,
        applicationId: applicant.cuid,
        applicationDate: revertFormatDate(moment(), 'DD-MM-YYYY'),
        loanAmount:
          (
            _.find(_.get(entity, 'loanOffers'), {
              type: 'applied_amount',
            }) || {}
          ).loanAmount || 1,
      },
    };
    if (!gender) {
      return message.error('Please save mandatory fields');
    }
    generateCibilReport(
      CIBIL_DATA,
      { name: 'cibil_report.pdf' },
      'user',
      cuid,
      'CIBIL Bureau Report',
    );
  };

  useEffect(() => {
    if (!_.get(errors, 'guarantors[0]')) {
      if (
        !validationObj ||
        (validationObj &&
          Object.keys(validationObj).length !== entity.guarantors.length)
      ) {
        entity.guarantors.forEach(app => {
          validationObj[app.id] = false;
        });
      }
      debugger;
      triggerValidation().then(valid => {
        dispatch(
          setFormValidationError('guarantorDetails', {
            ...validationObj,
            [applicant.id]: valid,
          }),
        );
      });
    }
  }, [_.get(errors, 'guarantors[0]')]);

  const checkField = fieldName => {
    const fValue = _.get(values, `${fieldName}`);
    setValue(`${fieldName}`, fValue);
    if (fValue && _.get(errors, fieldName)) {
      clearError(`${fieldName}`);
    }
  };

  useEffect(() => {
    checkField(`guarantors[0].salutation`);
    checkField(`guarantors[0].preferredPhone`);
    checkField('guarantors[0].firstName');
    checkField('guarantors[0].lastName');
    checkField('guarantors[0].preferredEmail');
    checkField('guarantors[0].dateOfBirthIncorporation');
    checkField('guarantors[0].userDetails.gender');
    setValue(
      'guarantors[0].userIdentities.pan',
      _.get(values, 'guarantors[0].userIdentities.pan'),
    );
    if (
      _.get(values, 'guarantors[0].userIdentities.pan') &&
      _.get(errors, 'guarantors[0].userIdentities.pan.type') === 'required'
    ) {
      clearError('guarantors[0].userIdentities.pan');
    }
  }, [
    _.get(errors, `guarantors[0].preferredPhone`),
    _.get(errors, `guarantors[0].firstName`),
    _.get(errors, `guarantors[0].lastName`),
    _.get(errors, `guarantors[0].preferredEmail`),
    _.get(errors, `guarantors[0].dateOfBirthIncorporation`),
    _.get(errors, 'guarantors[0].userDetails.gender'),
    // _.get(errors, 'guarantors[0].userIdentities.pan.type') === 'required',
  ]);

  const handleGenerateDelphiReport = applicant => {
    generateDelphiReport(appId, cuid);
  };

  useInjectReducer({ key: 'guarantorPage', reducer });
  useInjectSaga({ key: 'guarantorPage', saga });

  return (
    <>
      <GuarantorForm
        cuid={cuid}
        key={id}
        applicant={applicant}
        triggerValidation={triggerValidation}
        errors={errors}
        contactArr={contactArr}
        reset={reset}
        entity={entity}
        requestType={requestType}
        isNoEntity={isNoEntity}
        handleGenerateCibilReport={handleGenerateCibilReport}
        handleGenerateDelphiReport={handleGenerateDelphiReport}
        applicantId={applicant.id}
        product={product}
        partner={partner}
        values={values}
        setValue={setValue}
        clearError={clearError}
        APPLICANTTYPE={APPLICANTTYPE}
        applicantDetails={applicantDetails}
        {...rest}
      />
      <Row justify="space-between">
        <Button
          type="primary"
          htmlType="submit"
          disabled={
            !applicant.userLinked ||
            activity === 'CREDIT_FCU' ||
            activity === 'HUNTER_REVIEW'
          }
        >
          Save
        </Button>
      </Row>
    </>
  );
}

GuarantorPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  guarantorPage: makeSelectGuarantorPage(),
  pincodeDetails: makeSelectPincode(),
  applicantDetails: makeSelectApplicantDetails2(),
});

function mapDispatchToProps(dispatch) {
  return {
    generateCibilReport: (data, file, type, id, docType) =>
      dispatch(generateCibilReport(data, file, type, id, docType)),
    generateDelphiReport: (appId, cuid) =>
      dispatch(generateDelphiReport(appId, cuid)),
    fetchApplicant: cuid => dispatch(fetchApplicant(cuid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(GuarantorPage);
