/*
 *
 * ApplicantPage actions
 *
 */

import {
  FETCH_APPLICANT,
  FETCH_APPLICANT_SUCCESS,
  FETCH_APPLICANT_FAILED,
  UPDATE_APPLICATION,
  UPDATE_APPLICATION_SUCCESS,
  UPDATE_APPLICATION_FAILED,
} from './constants';

export function fetchApplicant(cuid) {
  return {
    type: FETCH_APPLICANT,
    cuid,
  };
}

export function applicantFetched(response, cuid) {
  return {
    type: FETCH_APPLICANT_SUCCESS,
    response,
    cuid,
  };
}

export function applicantFetchingFailed(error) {
  return {
    type: FETCH_APPLICANT_FAILED,
    error,
  };
}

export function updateApplicants(payload) {
  return {
    type: UPDATE_APPLICATION,
    payload,
  };
}

export function applicantsUpdated(response, cuid) {
  return {
    type: UPDATE_APPLICATION_SUCCESS,
    response,
    cuid,
  };
}

export function updateAppliantsError(error) {
  return {
    type: UPDATE_APPLICATION_FAILED,
    error,
  };
}
