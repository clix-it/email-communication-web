import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the applicantPage state domain
 */

const selectGuarantorPageDomain = state => state.guarantorPage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ApplicantPage
 */

const makeSelectGuarantorPage = () =>
  createSelector(
    selectGuarantorPageDomain,
    substate => substate,
  );

export default makeSelectGuarantorPage;
export { selectGuarantorPageDomain };
