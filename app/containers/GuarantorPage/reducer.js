/*
 *
 * ApplicantPage reducer
 *
 */
import produce from 'immer';
import { LOAD_ENTITY_DETAILS } from 'containers/ApplicantDetails/constants';
import {
  FETCH_APPLICANT,
  FETCH_APPLICANT_SUCCESS,
  FETCH_APPLICANT_FAILED,
} from './constants';

export const initialState = {
  loading: false,
  response: {},
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const guarantorPageReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOAD_ENTITY_DETAILS:
      case FETCH_APPLICANT:
        draft.loading = true;
        draft.error = false;
        break;
      case FETCH_APPLICANT_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.response = {
          ...draft.response,
          [action.response.cuid]: action.response,
        };
        break;
      case FETCH_APPLICANT_FAILED:
        draft.loading = false;
        draft.error = action.error;
        break;
    }
  });

export default guarantorPageReducer;
