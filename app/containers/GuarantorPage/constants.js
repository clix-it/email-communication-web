/*
 *
 * ApplicantPage constants
 *
 */

export const FETCH_APPLICANT = 'app/GuarantorPage/FETCH_APPLICANT';
export const FETCH_APPLICANT_SUCCESS =
  'app/GuarantorPage/FETCH_APPLICANT_SUCCESS';
export const FETCH_APPLICANT_FAILED =
  'app/GuarantorPage/FETCH_APPLICANT_FAILED';

export const UPDATE_APPLICATION = 'app/GuarantorPage/UPDATE_APPLICATION';
export const UPDATE_APPLICATION_SUCCESS =
  'app/GuarantorPage/UPDATE_APPLICATION_SUCCESS';
export const UPDATE_APPLICATION_FAILED =
  'app/GuarantorPage/UPDATE_APPLICATION_FAILED';
