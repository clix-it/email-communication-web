/*
 *
 * PslPurpose reducer
 *
 */
import _ from 'lodash';
import produce from 'immer';
import {
  FETCH_PSL_SECTOR,
  FETCH_PSL_SECTOR_FAILED,
  FETCH_PSL_SECTOR_SUCCESS,
} from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const pslSectorReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_PSL_SECTOR:
        draft.loading = true;
        draft.error = false;
        break;
      case FETCH_PSL_SECTOR_SUCCESS: {
        draft.loading = true;
        const temp = [];
        action.response.forEach(item => {
          temp.push({ code: item.cmCode, value: item.cmValue });
        });
        draft.response = temp;
        break;
      }
      case FETCH_PSL_SECTOR_FAILED:
        draft.loading = true;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default pslSectorReducer;
