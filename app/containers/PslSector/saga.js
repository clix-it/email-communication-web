import { call, put, takeEvery } from 'redux-saga/effects';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import { FETCH_PSL_SECTOR } from './constants';
import { pslSectorFetched, pslSectorError } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* fetchPslSector() {
  const requestURL = `${env.MASTER_URL}?masterType=PSL_SECTOR`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });

    if (response && response.success) {
      yield put(pslSectorFetched(response.masterData));
    } else {
      yield put(pslSectorError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(pslSectorError('Master Data not found'));
    message.error(
      (error && error.message) || 'Master Data for PSL Sector not found',
    );
  }
}

export default function* pslSectorSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_PSL_SECTOR, fetchPslSector);
}
