/*
 *
 * PslSector constants
 *
 */

export const FETCH_PSL_SECTOR = 'app/PslSector/FETCH_PSL_SECTOR';
export const FETCH_PSL_SECTOR_SUCCESS =
  'app/PslSector/FETCH_PSL_SECTOR_SUCCESS';
export const FETCH_PSL_SECTOR_FAILED = 'app/PslSector/FETCH_PSL_SECTOR_FAILED';
