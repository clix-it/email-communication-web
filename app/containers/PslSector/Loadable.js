/**
 *
 * Asynchronously loads the component for PslSector
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
