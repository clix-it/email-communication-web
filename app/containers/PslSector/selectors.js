import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pslSector state domain
 */

const selectPslSectorDomain = state => state.pslSector || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by pslSector
 */

const makeSelectPslSectorInput = () =>
  createSelector(
    selectPslSectorDomain,
    substate => substate,
  );

export default makeSelectPslSectorInput;
export { selectPslSectorDomain };
