/*
 *
 * PslSector actions
 *
 */

import {
  FETCH_PSL_SECTOR,
  FETCH_PSL_SECTOR_FAILED,
  FETCH_PSL_SECTOR_SUCCESS,
} from './constants';

export function fetchPslSector() {
  return {
    type: FETCH_PSL_SECTOR,
  };
}

export function pslSectorFetched(response) {
  return {
    type: FETCH_PSL_SECTOR_SUCCESS,
    response,
  };
}

export function pslSectorError(error) {
  return {
    type: FETCH_PSL_SECTOR_FAILED,
    error,
  };
}
