/**
 *
 * PslEndUse
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import InputField from 'components/InputField';
import { Controller } from 'react-hook-form';
import { Select, Form } from 'antd';
import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPslSector from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchPslSector } from './actions';

const { Option } = Select;

export function PslSector({
  pslSector,
  getPslSector,
  values,
  errors,
  setError,
  clearError,
  setValue,
  name,
  ...rest
}) {
  useInjectReducer({ key: 'pslSector', reducer });
  useInjectSaga({ key: 'pslSector', saga });

  useEffect(() => {
    getPslSector();
  }, []);

  const { response = [], error } = pslSector;

  return (
    <Form.Item label="PSL Sector">
      <Controller
        name={name}
        rules={{
          required: {
            value: false,
            message: 'This field cannot be left empty',
          },
        }}
        showSearch
        onChange={([value]) => value}
        {...rest}
        as={
          <Select size="large" placeholder="PSL Sector">
            {response &&
              response.length > 0 &&
              response.map(item => (
                <Option value={item.code}>{item.value}</Option>
              ))}
          </Select>
        }
      />
      <ErrorMessage name={name} errors={errors} />
    </Form.Item>
  );
}

PslSector.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pslSector: makeSelectPslSector(),
});

function mapDispatchToProps(dispatch) {
  return {
    getPslSector: () => dispatch(fetchPslSector()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PslSector);
