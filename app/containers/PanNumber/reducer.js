/*
 *
 * PanNumber reducer
 *
 */
import produce from 'immer';

import {
  VERIFY_PAN_WITH_NSDL,
  VERIFY_PAN_WITH_NSDL_SUCCESS,
  VERIFY_PAN_WITH_NSDL_FAILED,
} from './constants';
export const initialState = {
  pan: false,
  response: {},
  loading: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const panNumberReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case VERIFY_PAN_WITH_NSDL:
        draft.loading = true;
        // draft.response = {};
        draft.error = false;
        draft.pan = action.pan;
        break;
      case VERIFY_PAN_WITH_NSDL_SUCCESS:
        draft.loading = false;
        draft.response = {
          ...draft.response,
          [action.pan]: { ...action.response, error: '' },
        };
        draft.error = false;
        break;
      case VERIFY_PAN_WITH_NSDL_FAILED:
        draft.loading = false;
        draft.response = {
          ...draft.response,
          [action.pan]: { error: 'Invalid Pan' },
        };
        draft.error = action.error;
        break;
    }
  });

export default panNumberReducer;
