/*
 *
 * PanNumber actions
 *
 */

import {
  VERIFY_PAN_WITH_NSDL,
  VERIFY_PAN_WITH_NSDL_SUCCESS,
  VERIFY_PAN_WITH_NSDL_FAILED,
} from './constants';

export function verifyPan(pan) {
  return {
    type: VERIFY_PAN_WITH_NSDL,
    pan,
  };
}

export function panVerified(response, pan) {
  return {
    type: VERIFY_PAN_WITH_NSDL_SUCCESS,
    response,
    pan,
  };
}

export function verifyPanFailed(error, pan) {
  return {
    type: VERIFY_PAN_WITH_NSDL_FAILED,
    error,
    pan,
  };
}
