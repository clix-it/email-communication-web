import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the panNumber state domain
 */

const selectPanNumberDomain = state => state.panNumber || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by PanNumber
 */

const makeSelectPanNumber = () =>
  createSelector(
    selectPanNumberDomain,
    substate => substate,
  );

export default makeSelectPanNumber;
export { selectPanNumberDomain };
