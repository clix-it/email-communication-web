/*
 *
 * PanNumber constants
 *
 */

export const VERIFY_PAN_WITH_NSDL = 'app/PanNumber/VERIFY_PAN_WITH_NSDL';
export const VERIFY_PAN_WITH_NSDL_SUCCESS =
  'app/PanNumber/VERIFY_PAN_WITH_NSDL_SUCCESS';
export const VERIFY_PAN_WITH_NSDL_FAILED =
  'app/PanNumber/VERIFY_PAN_WITH_NSDL_FAILED';
