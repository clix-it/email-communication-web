import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';
import { VERIFY_PAN_WITH_NSDL } from './constants';
import { panVerified, verifyPanFailed } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* verifyPan({ pan }) {
  const requestURL = `${env.API_URL}${env.NSDL_API_URI}`;

  try {
    // Call our request helper (see 'utils/request')
    const nsdlData = {
      pan: pan.toUpperCase(),
      cuid: '',
      firstName: '',
      middleName: '',
      lastName: '',
    };
    const options = {
      body: JSON.stringify(nsdlData),
      method: 'POST',
      headers: env.headers,
    };
    const response = yield call(request, requestURL, options);
    if (
      response.panStatus == 'Record (PAN) Not Found in ITD Database/Invalid PAN'
    ) {
      yield put(verifyPanFailed(response, pan));
    } else {
      yield put(panVerified(response, pan));
    }
  } catch (err) {
    yield put(verifyPanFailed(err, pan));
  }
}

export default function* panNumberSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(VERIFY_PAN_WITH_NSDL, verifyPan);
}
