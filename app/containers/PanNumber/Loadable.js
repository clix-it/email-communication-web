/**
 *
 * Asynchronously loads the component for PanNumber
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
