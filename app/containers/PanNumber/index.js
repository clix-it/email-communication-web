/**
 *
 * PanNumber
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';

import InputField from 'components/InputField';
import { Spin } from 'antd';
import makeSelectPanNumber from './selectors';
import reducer from './reducer';
import saga from './saga';
import { verifyPan } from './actions';
import { stringSimilar } from '../../utils/helpers';
export function PanNumber({
  panNumberDetails,
  verifyPan,
  pan,
  errors,
  setError,
  name,
  panName,
  entityName,
  ...rest
}) {
  useInjectReducer({ key: 'panNumber', reducer });
  useInjectSaga({ key: 'panNumber', saga });

  const { loading } = panNumberDetails;
  const response = _.get(panNumberDetails, `response.${pan}`);
  const error = _.get(panNumberDetails, `response.${pan}.error`);

  // Here's where the API call happens
  // We use useEffect since this is an asynchronous action
  useEffect(() => {
    if ((pan || '').length === 10) {
      verifyPan(pan);
    }
  }, [pan]);

  useEffect(() => {
    if (response && response.error && _.get(errors, name)) {
      setError(name, 'pattern', 'Invalid PAN');
      return;
    }

    if (response && panName) {
      panName(
        `${response.firstName || ''} ${response.middleName || ''} ${
          response.lastName
        }`,
      );
    }
    if (panName) {
      const name1 = response
        ? `${response.firstName || ''} ${response.middleName ||
            ''} ${response.lastName || ''}`
        : '';
      const similarity = stringSimilar(entityName || '', name1);

      console.log(similarity, 'similar');
      if (response && !response.error) {
        if (similarity * 100 < 90) {
          console.log('errors', pan, entityName, name1, 'panEntity');
          setError(name, 'pattern', 'PAN name doesnt match with Name entered.');
        }
      }
    }
    // }
  }, [response, name, error]);

  if (response && response.error && !_.get(errors, name)) {
    setError(name, 'pattern', 'Invalid PAN');
  }

  return (
    // <Spin spinning={loading} style={{ margin: '25% auto', width: '100%' }}>
    <InputField
      errors={errors}
      name={name}
      {...rest}
      labelHtml={
        <label htmlFor="pin">{`PAN ${
          response
            ? `${response.firstName || ''} ${response.middleName ||
                ''} ${response.lastName || ''}`
            : ''
        }*`}</label>
      }
    />
    // </Spin>
  );
}

PanNumber.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  panNumberDetails: makeSelectPanNumber(),
});

function mapDispatchToProps(dispatch) {
  return {
    verifyPan: pan => dispatch(verifyPan(pan)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PanNumber);
