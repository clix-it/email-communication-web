/**
 *
 * PslEndUse
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import InputField from 'components/InputField';
import { Controller } from 'react-hook-form';
import { Select, Form } from 'antd';
import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPslEndUse from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchPslEndUse } from './actions';

const { Option } = Select;

export function PslEndUse({
  pslEndUse,
  getPslEndUse,
  values,
  errors,
  setError,
  clearError,
  setValue,
  name,
  ...rest
}) {
  useInjectReducer({ key: 'pslEndUse', reducer });
  useInjectSaga({ key: 'pslEndUse', saga });

  useEffect(() => {
    getPslEndUse();
  }, []);

  const { response, error } = pslEndUse;

  return (
    <Form.Item label="PSL End Use">
      <Controller
        name={name}
        rules={{
          required: {
            value: false,
            message: 'This field cannot be left empty',
          },
        }}
        showSearch
        onChange={([value]) => value}
        {...rest}
        as={
          <Select size="large" placeholder="PSL End Use">
            {response &&
              response.length > 0 &&
              response.map(item => (
                <Option value={item.code}>{item.value}</Option>
              ))}
          </Select>
        }
      />
      <ErrorMessage name={name} errors={errors} />
    </Form.Item>
  );
}

PslEndUse.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pslEndUse: makeSelectPslEndUse(),
});

function mapDispatchToProps(dispatch) {
  return {
    getPslEndUse: () => dispatch(fetchPslEndUse()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PslEndUse);
