import { call, put, takeEvery } from 'redux-saga/effects';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import { FETCH_PSL_END_USE } from './constants';
import { pslEndUseFetched, pslEndUseError } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* fetchPslEndUse() {
  const requestURL = `${env.MASTER_DATA_URL}/pslenduse`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });

    if (response && response.length > 0) {
      yield put(pslEndUseFetched(response));
    } else {
      yield put(pslEndUseError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(pslEndUseError('Master Data not found'));
    message.error(
      (error && error.message) || 'Master Data for PSL End Use not found',
    );
  }
}

export default function* pslCategorySaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_PSL_END_USE, fetchPslEndUse);
}
