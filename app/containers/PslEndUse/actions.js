/*
 *
 * PslCategory actions
 *
 */

import {
  FETCH_PSL_END_USE,
  FETCH_PSL_END_USE_SUCCESS,
  FETCH_PSL_END_USE_FAILED,
} from './constants';

export function fetchPslEndUse() {
  return {
    type: FETCH_PSL_END_USE,
  };
}

export function pslEndUseFetched(response) {
  return {
    type: FETCH_PSL_END_USE_SUCCESS,
    response,
  };
}

export function pslEndUseError(error) {
  return {
    type: FETCH_PSL_END_USE_FAILED,
    error,
  };
}
