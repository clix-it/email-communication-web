/*
 *
 * PslEndUse constants
 *
 */

export const FETCH_PSL_END_USE = 'app/PslEndUse/FETCH_PSL_END_USE';
export const FETCH_PSL_END_USE_SUCCESS =
  'app/PslEndUse/FETCH_PSL_END_USE_SUCCESS';
export const FETCH_PSL_END_USE_FAILED =
  'app/PslEndUse/FETCH_PSL_END_USE_FAILED';
