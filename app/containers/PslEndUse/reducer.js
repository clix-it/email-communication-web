/*
 *
 * PslCategory reducer
 *
 */
import _ from 'lodash';
import produce from 'immer';
import {
  FETCH_PSL_END_USE,
  FETCH_PSL_END_USE_SUCCESS,
  FETCH_PSL_END_USE_FAILED,
} from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const pslEndUseReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_PSL_END_USE:
        draft.loading = true;
        draft.error = false;
        break;
      case FETCH_PSL_END_USE_SUCCESS: {
        draft.loading = true;
        const temp = [];
        action.response.forEach(item => {
          temp.push({ code: item.code, value: item.description });
        });
        draft.response = temp;
        break;
      }
      case FETCH_PSL_END_USE_FAILED:
        draft.loading = true;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default pslEndUseReducer;
