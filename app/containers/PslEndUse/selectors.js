import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pslEndUse state domain
 */

const selectPslEndUseDomain = state => state.pslEndUse || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by pslEndUse
 */

const makeSelectPslEndUseInput = () =>
  createSelector(
    selectPslEndUseDomain,
    substate => substate,
  );

export default makeSelectPslEndUseInput;
export { selectPslEndUseDomain };
