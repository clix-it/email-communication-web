export const FETCH_HEALTH_QUESTIONS = 'FETCH_HEALTH_QUESTIONS';
export const SET_HEALTH_QUESTIONS = 'SET_HEALTH_QUESTIONS';
export const SET_HEALTH_QUESTIONS_ERROR = 'SET_HEALTH_QUESTIONS_ERROR';
export const SET_HEALTH_QUESTIONS_SUCCESS = 'SET_HEALTH_QUESTIONS_SUCCESS';
export const SAVE_HEALTH_ANSWERS = 'SAVE_HEALTH_ANSWERS';
export const GET_ANSWERS = 'GET_ANSWERS';
export const GET_ANSWERS_SUCCESS = 'GET_ANSWERS_SUCCESS';

export const colOneThird = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 5,
  xl: { span: 15, offset: 20, pull: 2 },
};

export const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

export const colSingle = { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 };
export const questionsArray = [
  {
    label:
      'Do you consume or have consumed any of the following? <br/> i. smoke more than 10 cigarettes/beedis a day <br/> ii. consume more than 60ml of alcohol in a day <br/> iii. consume any narcotics <br/> iv. chew more than 30 gms of Tobacco (Gutka) per day',
    id: 1,
    type: '',
    answer: '',
  },
  {
    label:
      'Do you have any congenital defect/abnormality/physical deformity/handicap?',
    id: 2,
    type: 'COVID',
    answer: '',
  },
  {
    label:
      'Have you undergone or been advised to undergo any tests/investigations or any surgery or hospitalized for observation or treatment in past?',
    id: 3,
    type: '',
    answer: '',
  },
  {
    label:
      'Do you consume or have consumed any of the following? <br/> i. smoke more than 10 cigarettes/beedis a day <br/> ii. consume more than 60ml of alcohol in a day <br/> iii. consume any narcotics <br/> iv. chew more than 30 gms of Tobacco (Gutka) per day',
    id: 4,
    type: 'COVID',
    answer: '',
  },
];
