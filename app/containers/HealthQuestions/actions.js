import {
  FETCH_HEALTH_QUESTIONS,
  SET_HEALTH_QUESTIONS_ERROR,
  SET_HEALTH_QUESTIONS,
  SET_HEALTH_QUESTIONS_SUCCESS,
  SAVE_HEALTH_ANSWERS,
  GET_ANSWERS,
  GET_ANSWERS_SUCCESS,
} from './constants';

export function fetchHealthQuestions(data) {
  return {
    type: FETCH_HEALTH_QUESTIONS,
    data,
  };
}

export function setHealthQuestionsError(error) {
  return {
    type: SET_HEALTH_QUESTIONS_ERROR,
    error,
  };
}

export function setHealthQuestions(questionsData) {
  return {
    type: SET_HEALTH_QUESTIONS,
    questionsData,
  };
}

export function setHealthQuestionsSuccess(success) {
  return {
    type: SET_HEALTH_QUESTIONS_SUCCESS,
    success,
  };
}

export function saveHealthAnswers(data) {
  return {
    type: SAVE_HEALTH_ANSWERS,
    data,
  };
}

export function getAnswers() {
  return {
    type: GET_ANSWERS,
  };
}

export function getAnswersSuccess(data) {
  return {
    type: GET_ANSWERS_SUCCESS,
    data,
  };
}
