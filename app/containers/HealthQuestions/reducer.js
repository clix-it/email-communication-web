/*
 *
 * healthQuestionsReducer reducer
 *
 */
import produce from 'immer';
import _ from 'lodash';
import {
  FETCH_HEALTH_QUESTIONS,
  SET_HEALTH_QUESTIONS,
  SET_HEALTH_QUESTIONS_ERROR,
  GET_ANSWERS_SUCCESS,
} from './constants';

export const initialState = {
  loading: false,
  error: false,
  questions: [],
  answers: [],
  allAnswers: [],
};

const insuranceDetailsReducer = (state = initialState, action) =>
  /* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["draft"] }] */
  produce(state, draft => {
    switch (action.type) {
      case FETCH_HEALTH_QUESTIONS:
        draft.loading = true;
        draft.error = false;
        draft.success = false;
        break;
      case SET_HEALTH_QUESTIONS: {
        draft.questions = action.questionsData;
        draft.loading = false;
        draft.success = true;
        draft.error = false;
        // debugger;
        draft.answers = action.questionsData;
        draft.answers = draft.answers.map(answer =>
          _.omit(answer, ['createdAt', 'updatedAt']),
        );
        break;
      }
      case SET_HEALTH_QUESTIONS_ERROR: {
        draft.loading = false;
        draft.success = false;
        draft.error = action.error;
        break;
      }
      case GET_ANSWERS_SUCCESS: {
        draft.allAnswers = action.data;
        break;
      }
      default: {
        return state;
      }
    }
  });

export default insuranceDetailsReducer;
