import { put, takeLatest, all, takeEvery, call } from 'redux-saga/effects';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import {
  FETCH_HEALTH_QUESTIONS,
  SAVE_HEALTH_ANSWERS,
  GET_ANSWERS,
  SET_HEALTH_QUESTIONS_SUCCESS,
} from './constants';
import {
  setHealthQuestionsError,
  setHealthQuestions,
  setHealthQuestionsSuccess,
  getAnswersSuccess,
} from './actions';
import env from '../../../environment';
import dataJSON from './data.json';

export function* getHealthQuestions(action) {
  const { data: { productpartner = '', amount = 0 } = {} } = action;
  try {
    const requestURL = `${env.API_URL}/insurance/v1/getquestions`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        productpartner,
        productpartnerId: productpartner,
        amount,
      }),
    };
    const response = yield call(request, requestURL, options);
    // const response = dataJSON;
    if (response.success || response.status) {
      let questions = _.get(response, 'productpartnerobj.questions', []);
      questions = _.sortBy(questions, 'priority');
      yield put(setHealthQuestions(questions || []));
    } else {
      message.info(
        response.message || 'Oops! Health Questions could not be fetched! :(',
      );
      yield put(
        setHealthQuestionsError(
          'Oops! Health Questions could not be fetched! :(',
        ),
      );
    }
  } catch (err) {
    yield put(setHealthQuestionsError(err));
    message.error('Network error occurred! Check connection!');
  }
}

export function* getAnswers() {
  try {
    const requestURL = `${env.API_URL}/insurance/v1/getanswer`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        appid: JSON.parse(sessionStorage.getItem('id')),
      }),
    };
    const response = yield call(request, requestURL, options);
    // const response = dataJSON;
    if (response.success || response.status) {
      let answers = _.get(response, 'answerList', []);
      answers = _.sortBy(answers, 'priority');
      yield put(getAnswersSuccess(answers));
    }
  } catch (err) {
    message.error('Network error occurred! Check connection!');
  }
}

export function* saveHealthAnswers(action) {
  const {
    data: { cuid = '', answers = [], productpartner = '' } = {},
  } = action;
  try {
    const requestURL = `${env.API_URL}/insurance/v1/answers`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        appid: JSON.parse(sessionStorage.getItem('id')),
        cuid,
        productpartner,
        answers,
      }),
    };
    const response = yield call(request, requestURL, options);
    // const response = dataJSON;
    if (response.success || response.status) {
      yield put(setHealthQuestionsSuccess(response.message || true));
      message.info(
        response.message || 'Health Questions are saved successfully! :)',
      );
    } else {
      message.info(
        response.message || 'Oops! Health Questions could not be fetched! :(',
      );
      yield put(
        setHealthQuestionsError(
          'Oops! Health Questions could not be fetched! :(',
        ),
      );
    }
  } catch (err) {
    yield put(setHealthQuestionsError(err));
    message.error('Network error occurred! Check connection!');
  }
}
export default function* healthQuestionsSaga() {
  yield takeLatest(FETCH_HEALTH_QUESTIONS, getHealthQuestions);
  yield takeLatest(SAVE_HEALTH_ANSWERS, saveHealthAnswers);
  yield takeLatest(GET_ANSWERS, getAnswers);
  yield takeLatest(SET_HEALTH_QUESTIONS_SUCCESS, getAnswers);
}
