import _ from 'lodash';
import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { useForm, Controller } from 'react-hook-form';
import moment from 'moment';
import {
  Row,
  Col,
  Button,
  Spin,
  Collapse,
  Radio,
  Form,
  Divider,
  message,
} from 'antd';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import InputField from 'components/InputField';
import { EditOutlined } from '@ant-design/icons';
import { useLocation } from 'react-router-dom';
import ErrorMessage from 'components/ErrorMessage';
import StyledTitle from 'components/StyledTitle';
import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';
import {
  fetchHealthQuestions,
  setHealthQuestions,
  saveHealthAnswers,
} from './actions';
import { makeSelectHealthQuestions } from './selectors';
import { colOneThird, colSingle } from './constants';
import { loadEntityDetails } from '../ApplicantDetails/actions';

const { Panel } = Collapse;

function HealthQuestions({
  dispatch,
  applicantDetails,
  selectedInsurance,
  healthQuestions,
}) {
  const {
    entity: { product = '', partner = '', entityCuid = '', appId = '' } = {},
  } = applicantDetails;
  const [questionsArray, setQuestionsArray] = useState([]);
  const [disabled, setDisabled] = useState(true);
  const {
    loading = false,
    answers = [],
    questions = [],
    allAnswers = [],
  } = healthQuestions;

  const defaultValues = {
    questions,
  };

  const {
    handleSubmit,
    errors,
    control,
    reset,
    watch,
    setError,
    clearError,
    getValues,
    setValue,
  } = useForm({
    mode: 'onChange',
    defaultValues,
  });

  useEffect(() => {
    const eligibleLoanAmount = _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].loanAmount',
      0,
    );
    dispatch(
      fetchHealthQuestions({
        productpartner: `${product}${partner}`,
        amount: eligibleLoanAmount,
      }),
    );
  }, []);

  useEffect(() => {
    reset(defaultValues);
    setQuestionsArray(questions);
    const cuid = _.get(selectedInsurance, 'cuid', '');
    const answersPerCuid = allAnswers.filter(ans => ans.cuid === cuid);
    if (answersPerCuid && answersPerCuid.length > 0) {
      setQuestionsArray(answersPerCuid);
    }
    if (product === 'PL' && (!answersPerCuid || answersPerCuid.length === 0)) {
      setQuestionsArray([]);
    }
  }, [questions, selectedInsurance, applicantDetails, allAnswers]);

  const values = watch();

  const onSubmit = data => {
    if (String(_.get(selectedInsurance, 'id', '')).includes('NEW')) {
      message.info('First create the Insurance!');
      return;
    }
    console.log(questionsArray, 'questionsArray');
    let newAnswers = questionsArray.map(answer =>
      _.omit(answer, ['createdAt', 'updatedAt']),
    );
    newAnswers = questionsArray.map(answer =>
      _.extend({}, answer, {
        appid: appId,
        cuid: _.get(selectedInsurance, 'cuid', ''),
        answer_date: moment().format('DD-MM-YYYY'),
      }),
    );
    console.log('answers to save health questions', newAnswers);
    dispatch(
      saveHealthAnswers({
        answers: newAnswers,
        productpartner: `${product}${partner}`,
        cuid: _.get(selectedInsurance, 'cuid', ''),
      }),
    );
  };

  const editQuestions = () => {
    setDisabled(!disabled);
  };

  const isEditDisabled = () => {
    const cuid = _.get(selectedInsurance, 'cuid', '');
    const answersPerCuid = allAnswers.filter(ans => ans.cuid === cuid);
    return (
      (answersPerCuid && answersPerCuid.length > 0) ||
      !loc.pathname.toLowerCase().includes('pma/') ||
      product === 'PL'
    );
  };

  const loc = useLocation();

  const handleQuestion = (type, value, index) => {
    if (value === 'Yes' && type === 'COVID') {
      message.info(
        "Insurance won't be given if the Covid question is answered 'Yes'!",
      );
      return '';
    }
    if (questionsArray[index]) {
      questionsArray[index].answer = value;
    }
    dispatch(setHealthQuestions(questionsArray));
    return value;
  };

  const getframedQuestion = questionEle => {
    if (!questionEle.question) return '';
    const quest = questionEle.question.replace(
      'Is the answer to any of the below mentioned medical questions (Q.No.1 to 8) Yes?',
      '',
    );
    const firstDigit = quest.match(/\d/);
    const indexed = quest.indexOf(firstDigit);
    const questString = quest
      .substring(indexed + 3)
      .replaceAll('<b>', '')
      .replaceAll('</b>', '')
      .split('<br>');
    if (questString[0])
      questString[0] = `${questionEle.priority}. ${questString[0]}`;
    return questString;
  };

  if (questionsArray.length === 0) return <div />;
  return (
    <Collapse style={{ margin: '20px 0' }}>
      <Panel header="Detailed Medical Questionnaire">
        <Spin spinning={loading} tip="Loading...">
          <form onSubmit={handleSubmit(onSubmit)} id="saveAddressForm">
            {!isEditDisabled() && false && (
              <Row gutter={[16, 16]}>
                <Col {...colOneThird}>
                  <Button
                    type="primary"
                    onClick={() => editQuestions()}
                    icon={
                      <EditOutlined
                        style={{
                          fontSize: '16px',
                          padding: '0px 10px',
                        }}
                      />
                    }
                    // disabled={isEditDisabled()}
                  >
                    {disabled ? 'Edit Answers' : 'Cancel'}
                  </Button>
                  <Button
                    type="primary"
                    htmlType="button"
                    onClick={handleSubmit(onSubmit)}
                    form="saveAddressForm"
                    disabled={disabled}
                    style={{ marginLeft: '10px' }}
                  >
                    Save Answers
                  </Button>
                </Col>
              </Row>
            )}
            <StyledTitle>Generic Health Questions</StyledTitle>
            {questionsArray.length > 0 &&
              questionsArray.map(
                (questionEle, index) =>
                  questionEle.type !== 'C' && (
                    <>
                      <Row gutter={[16, 16]}>
                        <Col xs={0}>
                          <InputField
                            id={`question[${index}].questionId`}
                            control={control}
                            errors={errors}
                            name={`question[${index}].questionId`}
                            type="hidden"
                            defaultValue={questionEle.questionId}
                          />
                        </Col>
                      </Row>

                      <Row gutter={[16, 16]}>
                        <Col {...colSingle}>
                          {getframedQuestion(questionEle).map((i, key) => (
                            <div key={key}>{i}</div>
                          ))}
                          <Form.Item
                          //   label={question.label}
                          >
                            <Controller
                              name={`question[${index}].question`}
                              value={questionsArray[index].answer}
                              defaultValue={questionsArray[index].answer}
                              control={control}
                              onChange={([event]) =>
                                handleQuestion('I', event.target.value, index)
                              }
                              rules={{
                                required: {
                                  value: true,
                                  message: 'Please select either option!',
                                },
                              }}
                              as={
                                <Radio.Group
                                  disabled={disabled}
                                  name={`question[${index}].question`}
                                  optionType="button"
                                  buttonStyle="outline"
                                  options={['No', 'Yes']}
                                />
                              }
                            />
                            <ErrorMessage
                              name={`question[${index}].question`}
                              errors={errors}
                            />
                          </Form.Item>
                        </Col>
                      </Row>
                    </>
                  ),
              )}
            <Divider />
            {questionsArray.length > 0 &&
              questionsArray.some(ques => ques.type === 'C') && (
                <>
                  <StyledTitle>COVID Questions</StyledTitle>
                  {questionsArray.map(
                    (questionEle, index) =>
                      questionEle.type === 'C' && (
                        <>
                          <Row gutter={[16, 16]}>
                            <Col xs={0}>
                              <InputField
                                id={`question[${index}].questionId`}
                                control={control}
                                errors={errors}
                                name={`question[${index}].questionId`}
                                type="hidden"
                                defaultValue={questionEle.questionId}
                              />
                            </Col>
                          </Row>

                          <Row gutter={[16, 16]}>
                            <Col {...colSingle}>
                              {getframedQuestion(questionEle).map((i, key) => (
                                <div key={key}>{i}</div>
                              ))}
                              <Form.Item>
                                <Controller
                                  name={`question[${index}].question`}
                                  value={questionsArray[index].answer}
                                  defaultValue={questionsArray[index].answer}
                                  control={control}
                                  onChange={([event]) =>
                                    handleQuestion(
                                      'COVID',
                                      event.target.value,
                                      index,
                                    )
                                  }
                                  rules={{
                                    required: {
                                      value: true,
                                      message: 'Please select either option!',
                                    },
                                  }}
                                  as={
                                    <Radio.Group
                                      disabled={disabled}
                                      name={`question[${index}].question`}
                                      optionType="button"
                                      buttonStyle="outline"
                                      options={['No', 'Yes']}
                                    />
                                  }
                                />
                                <ErrorMessage
                                  name={`question[${index}].question`}
                                  errors={errors}
                                />
                              </Form.Item>
                            </Col>
                          </Row>
                        </>
                      ),
                  )}
                </>
            )}
          </form>
        </Spin>
      </Panel>
    </Collapse>
  );
}

const mapStateToProps = createStructuredSelector({
  applicantDetails: makeSelectApplicantDetails2(),
  healthQuestions: makeSelectHealthQuestions(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

HealthQuestions.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'healthQuestions', reducer });

const withSaga = useInjectSaga({ key: 'healthQuestions', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(HealthQuestions);
