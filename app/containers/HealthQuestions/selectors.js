import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the healthQuestions state domain
 */

const selectHealthQuestionsDomain = state =>
  state.healthQuestions || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by healthQuestions
 */

const makeSelectHealthQuestions = () =>
  createSelector(
    selectHealthQuestionsDomain,
    substate => substate,
  );

export { makeSelectHealthQuestions };
