/*
 *
 * CustomerLoanDetails actions
 *
 */

import * as actions from './constants';

//Loan details actions
export function fetchLoanDetailsInit() {
  return {
    type: actions.FETCH_CUSTOMER_LOAN_DETAILS_INIT,
  };
}

export function fetchLoanDetails(cuid) {
  return {
    type: actions.FETCH_CUSTOMER_LOAN_DETAILS,
    cuid,
  };
}

export function fetchLoanDetailsSuccess(data) {
  return {
    type: actions.FETCH_CUSTOMER_LOAN_DETAILS_SUCCESS,
    data,
  };
}

export function fetchLoanDetailsError(message) {
  return {
    type: actions.FETCH_CUSTOMER_LOAN_DETAILS_ERROR,
    message,
  };
}

export function setSelectedLoan(loan) {
  return {
    type: actions.SET_CUSTOMER_CURRENT_LOAN,
    loan,
  };
}

export function setSelectedLoanStatus(status) {
  return {
    type: actions.SET_CUSTOMER_CURRENT_LOAN_STATUS,
    status,
  };
}

//App details actions

export function fetchAppDetailsInit() {
  return {
    type: actions.FETCH_APP_DETAILS_LOAN_INIT,
  };
}

export function fetchAppDetails(appId) {
  return {
    type: actions.FETCH_APP_DETAILS_LOAN,
    appId,
  };
}

export function fetchAppDetailsSuccess(data) {
  return {
    type: actions.FETCH_APP_DETAILS_LOAN_SUCCESS,
    data,
  };
}

export function fetchAppDetailsError(message) {
  return {
    type: actions.FETCH_APP_DETAILS_LOAN_ERROR,
    message,
  };
}

//Penny Drop actions

export function fetchPennyDropDetails(appId) {
  return {
    type: actions.FETCH_APP_PENNY_DROP,
    appId,
  };
}

export function fetchPennyDropDetailsSuccess(data) {
  return {
    type: actions.FETCH_APP_PENNY_DROP_SUCCESS,
    data,
  };
}

export function fetchPennyDropDetailsError(message) {
  return {
    type: actions.FETCH_APP_PENNY_DROP_ERROR,
    message,
  };
}

//documents fetch actions
export function fetchDocs(appId) {
  return {
    type: actions.FETCH_CUSTOMER_APP_DOCS,
    appId,
  };
}

export function fetchDocsSuccess(data) {
  return {
    type: actions.FETCH_CUSTOMER_APP_DOCS_SUCCESS,
    data,
  };
}

export function fetchDocsError(message) {
  return {
    type: actions.FETCH_CUSTOMER_APP_DOCS_ERROR,
    message,
  };
}

export function fetchDocsV1(appDocuments) {
  return {
    type: actions.FETCH_CUSTOMER_APP_DOCS_V1,
    appDocuments,
  };
}

export function fetchDocsV1Success(data) {
  return {
    type: actions.FETCH_CUSTOMER_APP_DOCS_V1_SUCCESS,
    data,
  };
}

export function fetchDocsV1Error(message) {
  return {
    type: actions.FETCH_CUSTOMER_APP_DOCS_V1_ERROR,
    message,
  };
}

//DKYC actions
export function loadDKYCDocs(appId) {
  return {
    type: actions.LOAD_CUSTOMER_DKYC_DETAILS,
    appId,
  };
}

export function loadDKYCDocsSuccess(dkycDocs) {
  return {
    type: actions.LOAD_CUSTOMER_DKYC_DETAILS_SUCCESS,
    dkycDocs,
  };
}

export function loadDKYCDetails(data) {
  return {
    type: actions.CUSTOMER_DKYC_DETAILS,
    data,
  };
}

export function loadDKYCDocsError() {
  return {
    type: actions.LOAD_CUSTOMER_DKYC_DETAILS_ERROR,
  };
}

export function fetchDkycDocuments(docs) {
  return {
    type: actions.FETCH_CUSTOMER_DKYC_DOCUMENTS,
    docs,
  };
}

export function fetchDkycDocumentsSuccess(data) {
  return {
    type: actions.FETCH_CUSTOMER_DKYC_DOCUMENTS_SUCCESS,
    data,
  };
}

export function fetchDkycDocumentsError() {
  return {
    type: actions.FETCH_CUSTOMER_DKYC_DOCUMENTS_ERROR,    
  };
}

//Coapplicant actions
export function setCoApplicant(user) {
  return {
    type: actions.SET_CO_APPLICANT,
    user,
  };
}