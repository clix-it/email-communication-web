import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the customerLoanDetails state domain
 */

const selectCustomerLoanDetailsDomain = state =>
  state.customerLoanDetails || initialState;

const makeSelectCustomerLoanDetails = () =>
  createSelector(
    selectCustomerLoanDetailsDomain,
    substate => substate,
  );

export { makeSelectCustomerLoanDetails };
