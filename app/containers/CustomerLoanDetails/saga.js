import { takeLatest, put, select, all, call } from 'redux-saga/effects';
import request from 'utils/request';
import _ from 'lodash';
import { message } from 'antd';
import {
  fetchLoanDetailsInit,
  fetchLoanDetailsSuccess,
  fetchLoanDetailsError,
  fetchAppDetailsInit,
  fetchAppDetailsSuccess,
  fetchAppDetailsError,
  fetchPennyDropDetailsSuccess,
  fetchPennyDropDetailsError,
  fetchDocsSuccess,
  fetchDocsError,
  fetchDocsV1Success,
  fetchDocsV1Error,
  loadDKYCDocsSuccess,
  loadDKYCDocsError,
  loadDKYCDetails,
  fetchDkycDocumentsSuccess,
  fetchDkycDocumentsError,
} from './actions';
import * as actions from './constants';

import env from '../../../environment';

function* fetchLoanDetailsSaga({ cuid }) {
  yield put(fetchLoanDetailsInit());
  try {
    const reqUrl = `${env.SELF_SERVICE}/myaccount/${cuid}`;
    const options = {
      method: 'GET',
      headers: env.headers,
    };
    const response = yield call(request, reqUrl, options);
    if (response.success === true) {
      response.commonLmsResponseList &&
        response.commonLmsResponseList.length > 0 &&
        response.commonLmsResponseList.forEach(item => {
          if (item.status === '') {
            item.status = 'In-Progress';
          }
        });
      yield put(fetchLoanDetailsSuccess(response.commonLmsResponseList));
    } else {
      yield put(fetchLoanDetailsError('Loan details not available!'));
      message.error('Loan details not available!');
    }
  } catch (err) {
    yield put(
      fetchLoanDetailsError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

function* fetchAppDetailsSaga({ appId }) {
  yield put(fetchAppDetailsInit());
  try {
    const reqUrl = `${env.APP_SERVICE_API_URL}/${appId}`;
    const options = {
      method: 'GET',
      headers: env.headers,
    };
    const response = yield call(request, reqUrl, options);
    if (response.success === true) {
      yield put(fetchAppDetailsSuccess(response.app));
    } else {
      yield put(fetchAppDetailsError('Details not found'));
      message.error('Details not found');
    }
  } catch (err) {
    yield put(fetchAppDetailsError('Details not found'));
    message.error('Details not found');
  }
}

export function* fetchPennyDropDetails(action) {
  const requestURL = `${env.API_URL}${
    env.PENNY_DROP_URL
  }/getById?type=APP_ID&value=${action.appId}`;
  //const requestURL = `${env.API_URL}${env.PENNY_DROP_URL}/getById?type=APP_ID&value=50002356`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response.success) {
      yield put(fetchPennyDropDetailsSuccess(response.account_details));
    } else {
      yield put(fetchPennyDropDetailsError([]));
    }
  } catch (err) {
    yield put(fetchPennyDropDetailsError([]));
  }
}

export function* getDocs({ appId }) {
  try {
    const result = [];
    const reqUrl = `${env.DMS_API_URL}/download`;
    let data = {
      objId: appId,
      objType: 'app',
      docType: '',
    };
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(data),
    };
    const response = yield call(request, reqUrl, options);
    if (response.status === true) {
      response.data.forEach(obj => {
        if (obj.asset.filename.split('.')[1] !== undefined) {
          result.push({
            obj_type: obj.asset.obj_type,
            obj_id: obj.asset.obj_id,
            type: obj.asset.type,
            signedUrl: obj.signed_url.url,
            fileextension: obj.asset.filename.split('.')[1].toLowerCase(),
          });
        }
      });
    }
    const docsData = result;
    yield put(fetchDocsSuccess(docsData));
  } catch (err) {
    yield put(fetchDocsError(''));
  }
}

export function* fetchAppDocumentsV1({ appDocuments }) {
  try {
    if (appDocuments && appDocuments.length > 0) {
      //GET FILESTREAM FROM APP_DOCS v1 api
      let responses = appDocuments
        .filter(data => data.dmsId != null)
        .map(item => {
          let promise = request(
            `${env.APP_DOCS_V1_API_URL}/download/${item.dmsId}`,
            {
              method: 'GET',
              headers: env.headers,
            },
          );
          return promise;
        });
      let responeBodies = yield Promise.all(responses);
      yield put(fetchDocsV1Success(responeBodies));
    } else {
      yield put(fetchDocsV1Error(''));
    }
  } catch (error) {
    yield put(fetchDocsV1Error(error));
  }
}

export function* fetchDkycDetailsSaga({ appId }) {
  debugger;
  const reqUrl = `${env.KYC_LEAD_API_URL}/search`;
  let data = {
    id: appId,
    isKycDocsRequired: true,
    isAppDocsRequired: true,
  };
  const options = {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify(data),
  };
  try {
    const response = yield call(request, reqUrl, options);
    let responeBodiesKycDocs = [];
    let responeBodiesAppDocs = [];
    if (response.status && response.data.length > 0) {
      yield put(loadDKYCDetails(response.data[0]));
      if (response.data[0].kycDocs && response.data[0].kycDocs.length > 0) {
        responeBodiesKycDocs = response.data[0].kycDocs;
      }
      if (response.data[0].appDocs && response.data[0].appDocs.length > 0) {
        responeBodiesAppDocs = response.data[0].appDocs;
      }
      if (responeBodiesKycDocs.length > 0 || responeBodiesAppDocs.length > 0) {
        let concatedArray = responeBodiesKycDocs.concat(responeBodiesAppDocs);
        yield put(loadDKYCDocsSuccess(concatedArray));
      } else {
        yield put(loadDKYCDocsError());
        yield put(loadDKYCDetails(response.data[0]));
      }
    } else {
      yield put(loadDKYCDocsError());
    }
  } catch (error) {
    debugger;
    yield put(loadDKYCDocsError());
  }
}

export function* fetchDkycDocuments(action) {
  try {
    if (action.docs.length > 0) {
      //GET FILESTREAM FROM APP_DOCS v1 api
      let responses = action.docs
        .filter(data => data.docId != null)
        .map(item => {
          let promise = request(
            `${env.APP_DOCS_V1_API_URL}/download/${item.docId}`,
            {
              method: 'GET',
              headers: env.headers,
            },
          );
          return promise;
        });
        let responeBodies = yield Promise.all(responses);
        debugger;
        yield put(fetchDkycDocumentsSuccess(responeBodies));
    }
  } catch (error) {
    yield put(fetchDkycDocumentsError(''));
  }
}

export default function* customerLoanDetailsSaga() {
  yield takeLatest(actions.FETCH_CUSTOMER_LOAN_DETAILS, fetchLoanDetailsSaga);
  yield takeLatest(actions.FETCH_APP_DETAILS_LOAN, fetchAppDetailsSaga);
  yield takeLatest(actions.FETCH_APP_PENNY_DROP, fetchPennyDropDetails);
  yield takeLatest(actions.FETCH_CUSTOMER_APP_DOCS, getDocs);
  yield takeLatest(actions.FETCH_CUSTOMER_APP_DOCS_V1, fetchAppDocumentsV1);
  yield takeLatest(actions.LOAD_CUSTOMER_DKYC_DETAILS, fetchDkycDetailsSaga);
  yield takeLatest(actions.FETCH_CUSTOMER_DKYC_DOCUMENTS, fetchDkycDocuments);
}
