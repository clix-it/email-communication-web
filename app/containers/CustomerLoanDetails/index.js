/**
 *
 * CustomerLoanDetails
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import _ from 'lodash';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import { makeSelectCustomerLoanDetails } from './selectors';
import { Empty, Menu, Spin } from 'antd';
import { fetchLoanDetails, setSelectedLoan, setSelectedLoanStatus } from './actions';
import CustomerLoanComponent from '../../components/CustomerLoanComponent';

export function CustomerLoanDetails({ user, dispatch, cuid, customerLoanDetails }) {
  const {
    loading = false,
    error = false,
    loans = [],
    loanStatus = false,
    selectedLoanStatus = false,
    selectedLoan = false,
    appDetails = false,
    pennyDropDetails = [],
    docsLoading = false,
    docsData = [],
    appV1Docs = [],
    dkycDocs = [],
    dkycDetails = {},
    kycDocsLoading = false,
    dkycDocsData = [],
    selectedCoApplicant = {},
  } = customerLoanDetails;

  const [filteredLoans, setFilteredLoans] = useState([]);

  useEffect(() => {
    if(cuid){
      dispatch(fetchLoanDetails(cuid));
    }
  }, [cuid]);

  useEffect(() => {
    if(selectedLoanStatus){
      const loansArr = loans && loans.filter(item => item.status === selectedLoanStatus);
      setFilteredLoans(loansArr);
      dispatch(setSelectedLoan(loansArr[0]));
    }
  }, [selectedLoanStatus])

  const setApplication = id => {
    const loan =
      _.find(loans, { loanAccountNumber: id.key }) ||
      _.find(loans, { loanAccountNumber: parseInt(id.key) });
    if (loan) dispatch(setSelectedLoan(loan));
  };

  const setloanStatus = id => {
    dispatch(setSelectedLoanStatus(id.key));
  };  
  return (
    <Spin spinning={loading} tip="Loading...">
      <Menu
        onClick={setloanStatus}
        defaultSelectedKeys={[selectedLoanStatus]}
        selectedKeys={[selectedLoanStatus]}
        mode="horizontal"
        style={{ width: '95%' }}
      >
        {_.map(loanStatus, (status) => (
          <Menu.Item key={String(status)}>{status}</Menu.Item>
        ))}
      </Menu>
      <Menu
        onClick={setApplication}
        defaultSelectedKeys={[(selectedLoan || {}).loanAccountNumber]}
        selectedKeys={[String((selectedLoan || {}).loanAccountNumber)]}
        mode="horizontal"
        style={{ width: '95%' }}
      >
        {_.map(filteredLoans, (loan, index) => (
          <Menu.Item key={String(loan.loanAccountNumber)}>{`Application ${index +
            1}`}</Menu.Item>
        ))}
      </Menu>
      {selectedLoan ? (
        <>
          <CustomerLoanComponent
            selectedLoan={selectedLoan}
            dispatch={dispatch}
            appDetails={appDetails}
            pennyDropDetails={pennyDropDetails}
            docsLoading={docsLoading}
            docsData={docsData}
            appV1Docs={appV1Docs}
            dkycDocs={dkycDocs}
            dkycDetails={dkycDetails}
            kycDocsLoading={kycDocsLoading}
            dkycDocsData={dkycDocsData}
            primaryUser={user.cuid}
            selectedCoApplicant={selectedCoApplicant}
          />
        </>
      ) : (
        <Empty />
      )}
    </Spin>
  );
}

CustomerLoanDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  customerLoanDetails: makeSelectCustomerLoanDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'customerLoanDetails', reducer });
const withSaga = useInjectSaga({ key: 'customerLoanDetails', saga });

export default compose(
  withConnect,
  withReducer,
  withSaga,
  memo,
)(CustomerLoanDetails);
