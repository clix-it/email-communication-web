import produce from 'immer';
import _ from 'lodash';
import { LOCATION_CHANGE } from 'react-router-redux';
import { SET_MAIN_USER } from '../RelationshipDetails/constants';

import * as actions from './constants';

export const initialState = {
  loading: false,
  docsLoading: false,
  error: false,
  loans: [],
  loanStatus: false,
  selectedLoanStatus: false,
  selectedLoan: false,
  appDetails: false,
  pennyDropDetails: [],
  docsData: [],
  appV1Docs: [],
  dkycDocs: [],
  dkycDetails: {},
  dkycDocsData: [],
  kycDocsLoading: false,
  selectedCoApplicant: {},
};

/* eslint-disable default-case, no-param-reassign */
const customerLoanDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      //Loan details actions
      case actions.FETCH_CUSTOMER_LOAN_DETAILS_INIT:
        draft.loading = true;
        break;
      case actions.FETCH_CUSTOMER_LOAN_DETAILS_SUCCESS:
        draft.loading = false;
        draft.loans = [...action.data];
        draft.loanStatus =
          draft.loans &&
          _.mapValues(_.groupBy(draft.loans, 'status'), status =>
            status.map(status => _.omit(status, 'status')),
          ) &&
          Object.keys(
            _.mapValues(_.groupBy(draft.loans, 'status'), status =>
              status.map(status => _.omit(status, 'status')),
            ),
          )
            .sort()
            .reverse();
        draft.selectedLoanStatus = draft.loanStatus && draft.loanStatus[0];
        draft.selectedLoan = draft.selectedLoan
          ? _.find(draft.loans, {
              loanAccountNumber: draft.selectedLoan.loanAccountNumber,
            })
          : draft.loans && draft.loanStatus && draft.loanStatus.length > 0
          ? draft.loans.filter(item => item.status == draft.loanStatus[0])[0]
          : draft.loans[0];
        break;
      case actions.FETCH_CUSTOMER_LOAN_DETAILS_ERROR:
        draft.loading = false;
        break;
      case actions.SET_CUSTOMER_CURRENT_LOAN:
        draft.selectedLoan = action.loan;
        break;
      case actions.SET_CUSTOMER_CURRENT_LOAN_STATUS:
        draft.selectedLoanStatus = action.status;
        break;

      //App details actions
      case actions.FETCH_APP_DETAILS_LOAN_INIT:
        draft.loading = true;
        break;
      case actions.FETCH_APP_DETAILS_LOAN_SUCCESS:
        draft.loading = false;
        draft.appDetails = action.data;
        draft.selectedCoApplicant = {};
        break;
      case actions.FETCH_APP_DETAILS_LOAN_ERROR:
        draft.loading = false;
        draft.error = false;
        draft.loans = [];
        draft.selectedLoan = false;
        draft.appDetails = false;
        draft.docsLoading = false;
        draft.pennyDropDetails = [];
        draft.docsData = [];
        draft.appV1Docs = [];
        draft.error = action.message;
        break;
      case actions.FETCH_APP_PENNY_DROP_SUCCESS:
        draft.pennyDropDetails = action.data;
        break;
      case actions.FETCH_APP_PENNY_DROP_ERROR:
        draft.pennyDropDetails = [];
        break;
      case SET_MAIN_USER:
        draft.loading = false;
        draft.error = false;
        draft.loans = [];
        draft.selectedLoan = false;
        draft.appDetails = false;
        draft.docsLoading = false;
        draft.pennyDropDetails = [];
        draft.docsData = [];
        draft.appV1Docs = [];
        draft.kycDocsLoading = false;
        draft.dkycDocs = [];
        draft.dkycDetails = {};
        draft.dkycDocsData = [];
        draft.selectedCoApplicant = {};
        break;
      case LOCATION_CHANGE:
        draft.loading = false;
        draft.error = false;
        draft.loans = [];
        draft.selectedLoan = false;
        draft.loanStatus = false;
        draft.selectedLoanStatus = false;
        draft.appDetails = false;
        draft.docsLoading = false;
        draft.pennyDropDetails = [];
        draft.docsData = [];
        draft.appV1Docs = [];
        draft.kycDocsLoading = false;
        draft.dkycDocs = [];
        draft.dkycDetails = {};
        draft.dkycDocsData = [];
        draft.selectedCoApplicant = {};
        break;

      // Documents section
      case actions.FETCH_CUSTOMER_APP_DOCS:
        draft.docsLoading = true;
        break;
      case actions.FETCH_CUSTOMER_APP_DOCS_SUCCESS:
        draft.docsLoading = false;
        draft.docsData = action.data;
        break;
      case actions.FETCH_CUSTOMER_APP_DOCS_ERROR:
        draft.docsLoading = false;
        draft.docsData = [];
        draft.error = action.message;
        break;
      case actions.FETCH_CUSTOMER_APP_DOCS_V1:
        draft.docsLoading = true;
        break;
      case actions.FETCH_CUSTOMER_APP_DOCS_V1_SUCCESS:
        draft.docsLoading = false;
        draft.appV1Docs = action.data;
        break;
      case actions.FETCH_CUSTOMER_APP_DOCS_V1_ERROR:
        draft.docsLoading = false;
        draft.appV1Docs = [];
        break;

      //DKYC Details
      case actions.LOAD_CUSTOMER_DKYC_DETAILS:
        draft.kycDocsLoading = true;
        draft.dkycDocs = [];
        break;
      case actions.LOAD_CUSTOMER_DKYC_DETAILS_SUCCESS:
        draft.kycDocsLoading = false;
        draft.error = { status: false, message: '' };
        draft.dkycDocs = action.dkycDocs;
        break;
      case actions.CUSTOMER_DKYC_DETAILS:
        draft.dkycDetails = action.data;
        break;
      case actions.LOAD_CUSTOMER_DKYC_DETAILS_ERROR:
        draft.kycDocsLoading = false;
        draft.dkycDocs = [];
        draft.dkycDocsData = [];
        draft.dkycDetails = {};
        break;
      case actions.FETCH_CUSTOMER_DKYC_DOCUMENTS:
        draft.kycDocsLoading = true;
        draft.dkycDocsData = [];
      case actions.FETCH_CUSTOMER_DKYC_DOCUMENTS_SUCCESS:
        draft.kycDocsLoading = false;
        draft.dkycDocsData = action.data;
        break;
      case actions.FETCH_CUSTOMER_DKYC_DOCUMENTS_ERROR:
        draft.kycDocsLoading = false;
        draft.error = { status: false, message: '' };
        draft.dkycDocsData = [];
        break;
      case actions.SET_CO_APPLICANT:
        draft.selectedCoApplicant = action.user;
        break;
      default: {
        return state;
      }
    }
  });

export default customerLoanDetailsReducer;
