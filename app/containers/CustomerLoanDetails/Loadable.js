/**
 *
 * Asynchronously loads the component for CustomerLoanDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
