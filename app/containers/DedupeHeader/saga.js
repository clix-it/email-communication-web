import { takeEvery, put, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import * as actions from './constants';
import { setError, setSuccess, submitDecision } from './actions';
import { message } from 'antd';
import env from '../../../environment';
import { makeSelectApplications } from '../CreditPortal/selectors';

export function* approveRejectDedupeUser(action) {
  const { data, detailsData } = action.payload;
  yield put(submitDecision());
  const leadList = yield select(makeSelectApplications());
  const { applications = [], currentApplication } = leadList; // activitySrlNo
  const appId = sessionStorage.getItem('id') || '';
  const str = appId.replace(/^"(.*)"$/, '$1');
  if (applications.length === 0 || !appId) {
    yield put(setError('Activity Serial Number Not found! :( '));
    return;
  }
  let selectedLead = {};
  applications.forEach(lead => {
    const { applicationId } = lead;
    if (applicationId === str) {
      selectedLead = lead;
    }
  });
  let body = {};
  if (detailsData.matchType === 'EXACT_MATCH') {
    body = {
      matchType: 'EXACT_MATCH',
      decisionParams: {
        activitySrlNo: selectedLead.activitySrlNo,
        employeeId: parseInt(JSON.parse(sessionStorage.getItem('userId'))),
        status: data.status,
        remarks: data.remarks,
        nextApprovalUserId: '',
      },
    };
  } else if (detailsData.matchType === 'PARTIAL_MATCH') {
    body = {
      matchType: 'PARTIAL_MATCH',
      profileMergeParams: {
        requestId: detailsData.requestId,
        mergeCuids:
          detailsData.checkedUsers.length > 0
            ? [detailsData.userDetails.cuid].concat(detailsData.checkedUsers)
            : [detailsData.userDetails.cuid],
        noMerge: detailsData.checkedUsers.length > 0 ? false : true,
      },
      decisionParams: {
        activitySrlNo: selectedLead.activitySrlNo,
        employeeId: parseInt(JSON.parse(sessionStorage.getItem('userId'))),
        status: data.status,
        remarks: data.remarks,
        nextApprovalUserId: '',
      },
    };
    if (detailsData.userDetails.type === 'INDIVIDUAL') {
      body.profileMergeParams.mergeAttributes = {};
      if(detailsData.userDetails.dateOfBirthIncorporation){
        body.profileMergeParams.mergeAttributes.dateOfBirthIncorporation = detailsData.userDetails.dateOfBirthIncorporation;
      }
      if(detailsData.userDetails.identities && detailsData.userDetails.identities.pan){
        body.profileMergeParams.mergeAttributes.pan = detailsData.userDetails.identities.pan;
      }
      if(detailsData.userDetails.identities && detailsData.userDetails.identities.aadhar){
        body.profileMergeParams.mergeAttributes.aadhar = detailsData.userDetails.identities.aadhar;
      }
      if(detailsData.userDetails.identities && detailsData.userDetails.identities.voterId){
        body.profileMergeParams.mergeAttributes.voterId = detailsData.userDetails.identities.voterId;
      }
      if(detailsData.userDetails.identities && detailsData.userDetails.identities.passport){
        body.profileMergeParams.mergeAttributes.passport = detailsData.userDetails.identities.passport;
      }
    } else if (detailsData.userDetails.type === 'COMPANY') {
      body.profileMergeParams.mergeAttributes = {};
      if(detailsData.userDetails.dateOfBirthIncorporation){
        body.profileMergeParams.mergeAttributes.dateOfBirthIncorporation = detailsData.userDetails.dateOfBirthIncorporation;
      }
      if(detailsData.userDetails.identities && detailsData.userDetails.identities.pan){
        body.profileMergeParams.mergeAttributes.pan = detailsData.userDetails.identities.pan;
      }
      if(detailsData.userDetails.identities && detailsData.userDetails.identities.cin){
        body.profileMergeParams.mergeAttributes.cin = detailsData.userDetails.identities.cin;
      }
      if(detailsData.userDetails.identities && detailsData.userDetails.identities.tan){
        body.profileMergeParams.mergeAttributes.tan = detailsData.userDetails.identities.tan;
      }
    }
  }
  try {
    const result = yield fetch(
      `${env.API_URL}${env.APPROVE_REJECT_DEDUPE}/${detailsData.userDetails.type.toLowerCase()}`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify(body),
      },
    );
    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.executionStatus === 'MERGE_AND_USER_DECISION_SUCCESS') {
        if (data.status === 'APPROVED') {
          yield put(
            setSuccess('User Approved Successfully'),
          );
          yield* notifyApplicationService('APPROVED');
        } else if (data.status === 'REJECTED') {
          yield* notifyApplicationService(data.status);
        }
        yield put(push(`/${location.pathname.split('/')[1]}`));
      } else {
        yield put(
          setError(
            responseBody.message || 'Oops! User Could Not be approved :(',
          ),
        );
      }
    } else {
      yield put(setError(`HTTP Status${result.status} occurred.Try Again`));
    }
  } catch (err) {
    yield put(setError('Network error occurred! Check connection!'));
  }
}

export function* notifyApplicationService(status) {
  const appId = sessionStorage.getItem('id');
  const str = appId.replace(/^"(.*)"$/, '$1');
  try {
    const result = yield fetch(`${env.APP_SERVICE_API_URL}/${str}`, {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify({
        appSourcing: {
          creditManagerId: JSON.parse(sessionStorage.getItem('userId')),
        },
      }),
    });
    if (result.status === 200) {
      const responseBody = yield result.json();
      if (!responseBody.success && !responseBody.status) {
        yield put(
          setError(
            responseBody.message || 'Oops! User Could Not be Updated :(',
          ),
        );
      }
      if (
        (responseBody.success || responseBody.status) &&
        status === 'REJECTED'
      ) {
        yield put(
          setSuccess(responseBody.message || 'User Rejected Successfully'),
        );
      }
    } else {
      yield put(setError(`HTTP Status${result.status} occurred.Try Again`));
    }
  } catch (err) {
    yield put(setError('Network error occurred! Check connection!'));
  }
}

export default function* appSaga() {
  yield takeEvery(actions.APPROVE_REJECT_DEDUPE_USER, approveRejectDedupeUser);
}
