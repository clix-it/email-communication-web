import * as actions from './constants';

const initialState = {
  error: {},
  success: {},
  loading: false,
};

export default function dedupeHeaderReducer(state = initialState, action) {
  switch (action.type) {
    case actions.CLEAR_MESSAGES: {
      return {
        ...state,
        error: {},
        success: {},
        loading: false
      };
    }
    case actions.SET_ERROR: {
      return {
        ...state,
        error: { status: true, message: action.payload.error },
        loading: false
      };
    }
    case actions.SET_SUCCESS: {
      return {
        ...state,
        success: { status: true, message: action.payload.success },
        loading: false
      };
    }
    case actions.SUBMIT_DECISION: {
      return {
        ...state,
        loading: true
      };
    }
    default:
      return state;
  }
}
