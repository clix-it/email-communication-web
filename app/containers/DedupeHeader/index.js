import React, { useState, memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { Button, message, Spin, Input, Modal } from 'antd';

import { approveRejectDedupeUser, clearMessages } from './actions';
import reducer from './reducer';
import saga from './saga';

const { TextArea } = Input;

function DedupeHeader({ dispatch, success, error, detailsData, loading }) {
  const [visibleApp, setVisibleApp] = useState(false);
  const [visibleRej, setVisibleRej] = useState(false);
  const [remarks, setRemarks] = useState('');
  const { checkedUsers, partialUsers } = detailsData
    ? detailsData
    : { checkedUsers: [], partialUsers: [] };

  const handleOkApp = () => {
    const approveData = {
      status: 'APPROVED',
      remarks,
    };
    dispatch(approveRejectDedupeUser(approveData, detailsData));
  };

  const handleOkRej = () => {
    const rejData = {
      status: 'REJECTED',
      remarks,
    };
    dispatch(approveRejectDedupeUser(rejData, detailsData));
  };

  const setButtons = () => {
    if (detailsData && detailsData.matchType === 'EXACT_MATCH') {
      return (
        <>
          <Button type="primary" onClick={() => setVisibleApp(true)}>
            Approve
          </Button>
          <Button type="primary" onClick={() => setVisibleRej(true)}>
            Reject
          </Button>
        </>
      );
    } else if(detailsData && detailsData.matchType === 'PARTIAL_MATCH'){
      return (
        <>
          <Button type="primary" onClick={() => setVisibleApp(true)}>
            {checkedUsers.length == 0 ? `Approve as New` : `Merge and Approve`}
          </Button>
          <Button type="primary" onClick={() => setVisibleRej(true)}>
            {checkedUsers.length == 0 ? `Reject as New` : `Merge and Reject`}
          </Button>
        </>
      );
    }
  };

  useEffect(() => {
    if (success.status && success.message) {
      message.success(success.message, 4);
    }
    if (error.status && error.message) {
      message.error(error.message, 4);
    }
    setVisibleApp(false);
    setVisibleRej(false);
    setRemarks('');
    dispatch(clearMessages());
  }, [success.status, error.status]);

  const handleRemarks = e => {
    const { value } = e.target;
    setRemarks(value);
  };

  const handleCancel = () => {
    setVisibleApp(false);
    setVisibleRej(false);
    setRemarks('');
  };

  return (
    <>
      {setButtons()}
      <Modal
        title="Approve Application"
        visible={visibleApp}
        onOk={!loading ? handleOkApp : () => message.warning('Request is already in process')}
        onCancel={!loading ? handleCancel : () => message.warning('Request is already in process')}
      >
        <Spin spinning={loading}>
          <TextArea onChange={handleRemarks} value={remarks} />
        </Spin>
      </Modal>
      <Modal
        title="Reject Application"
        visible={visibleRej}
        onOk={!loading ? handleOkRej : () => message.warning('Request is already in process')}
        onCancel={!loading ? handleCancel : () => message.warning('Request is already in process')}
      >
        <Spin spinning={loading}>
          <TextArea onChange={handleRemarks} value={remarks} />
        </Spin>
      </Modal>
    </>
  );
}

DedupeHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  success: PropTypes.object,
  error: PropTypes.object,
};

DedupeHeader.defaultProps = {
  success: {},
  error: {},
};

const withReducer = useInjectReducer({ key: 'dedupeHeader', reducer });
const withSaga = useInjectSaga({ key: 'dedupeHeader', saga });

const mapStateToProps = state => ({
  success: state.dedupeHeader.success,
  error: state.dedupeHeader.error,
  loading: state.dedupeHeader.loading,
  detailsData: state.leadDetails,
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(DedupeHeader);
