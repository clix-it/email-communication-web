import * as actions from './constants';

export function approveRejectDedupeUser(data, detailsData) {
  return {
    type: actions.APPROVE_REJECT_DEDUPE_USER,
    payload: {
      data,
      detailsData
    },
  };
}

export function setError(error) {
  return {
    type: actions.SET_ERROR,
    payload: {
      error,
    },
  };
}

export function setSuccess(success) {
  return {
    type: actions.SET_SUCCESS,
    payload: {
      success,
    },
  };
}

export function clearMessages() {
  return {
    type: actions.CLEAR_MESSAGES,
  };
}

export function submitDecision() {
  return {
    type: actions.SUBMIT_DECISION,
  };
}
