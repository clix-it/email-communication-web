import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectUserDecision = state => state.userDecision || initialState;

const makeSelectUserDecision = () =>
  createSelector(
    selectUserDecision,
    userDecision => userDecision,
  );

export { makeSelectUserDecision };
