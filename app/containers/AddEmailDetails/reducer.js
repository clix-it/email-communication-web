/*
 *
 * AddEmail reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  ADD_EMAIL_DETAILS,
  ADD_EMAIL_DETAILS_ERROR,
  ADD_EMAIL_DETAILS_SUCCESS,
} from './constants';

export const initialState = {
  response: false,
  error: false,
  payload: false,
};

/* eslint-disable default-case, no-param-reassign */
const addEmailReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case ADD_EMAIL_DETAILS:
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case ADD_EMAIL_DETAILS_SUCCESS:
        draft.error = false;
        draft.response = action.data.message;
        break;
      case ADD_EMAIL_DETAILS_ERROR:
        draft.response = false;
        draft.error = action.data.error.message;
        break;
     }
  });

export default addEmailReducer;
