/**
 *
 * BankDetails
 *
 */
import _ from 'lodash';
import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useForm, Controller } from 'react-hook-form';
import { Form, Row, Col, Button, Radio, message, Divider } from 'antd';
import makeSelectAddEmailDetails from './selectors';
import reducer from './reducer';
import saga from './saga';
import {
  emailPattern,
} from '../../containers/App/constants';
import { EMAIL_CATEGORY, DEFAULT_MAIL_ID} from '../../utils/constants';
// const {  } = antd;
import InputField from '../../components/InputField';
import {
 updateEmailDetails
} from './actions';
import {
  setFormValidationError,
  isDirtyForm,
  loadEntityDetails,
} from '../ApplicantDetails/actions';
import { useHistory } from 'react-router-dom';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: { span: 10, offset: 2, pull: 2 },
};
export function AddEmailDetails({
  dispatch,
  applicantDetails,
  addEmailDetails,
  activity,
}) {

  useInjectReducer({ key: 'addEmailDetails', reducer });
  useInjectSaga({ key: 'addEmailDetails', saga });

  const {
    register,
    handleSubmit,
    getValues,
    errors,
    control,
    watch,
    setError,
    reset,
    clearError,
    formState,
    setValue,
    triggerValidation,
  } = useForm({
    mode: 'onChange',
  });

  const { dirtyFields, dirtyFieldsSinceLastSubmit } = formState;
  console.log(dirtyFields, 'dirtyFields');
  useEffect(() => {
    dispatch(isDirtyForm({ 5: formState.dirty }));
    console.log(formState.dirty, 'dirty bank', formState.dirtySinceLastSubmit);
  }, [formState.dirty]);

  const values = watch();
  console.log(values, 'values');

  console.log(formState.dirty, 'dirty val');
 

  const [response, setResponse] = useState(false);

  useEffect(() => {
    // if (Object.keys(errors).length > 0) {
    //   document.getElementById(Object.keys(errors)[0]).focus();
    // }
  }, [errors]);

  useEffect(() => {
    if (addEmailDetails.response && response) {
      message.success(addEmailDetails.response);
      dispatch(loadEntityDetails());
      setResponse(false);
    }

    if (addEmailDetails.error && response) {
      message.error(addEmailDetails.error);
    }
  }, [addEmailDetails.response, addEmailDetails.error]);

  const onSubmit = data => {
    
    dispatch(updateEmailDetails(data));
    setResponse(true);
  setTimeout(addEmail, 3000);
  };

  const history = useHistory();
  const addEmail = () => {
    history.push(`/emailDetails`)
  };

  // console.log(applicantDetails, 'applicantDetails');
  return (
    <div className="App">
      <h4>Email Details</h4>
      <Divider />
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={[16, 16]}>
          {/* start */}
          <Col md={24}>
            {/* <h5 style={{ fontWeight: 'bold' }}>Disbursement Bank Details</h5> */}
          </Col>
          <Col {...responsiveColumns}>
            <InputField
            id="emailId"
            control={control}
            name="emailId"
            type="string"
            values={values}
            rules={{
              required: {
                value: true,
                message: 'This field cannot be left empty',
              },
              pattern: emailPattern,
            }}
            errors={errors}
            placeholder="Email"
            labelHtml=" Email ID*"
          />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="category"
              control={control}
              name="category"
              type="select"
              values={values}
              options={EMAIL_CATEGORY}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Category"
              labelHtml="Category*"
            />
          </Col>
          {/* <Col {...responsiveColumns}>
            <InputField
              id="defaultMailId"
              control={control}
              name="defaultMailId"
              type="select"
              values={values}
              options={DEFAULT_MAIL_ID}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Default Mail ID"
              labelHtml="Default Mail ID*"
            />
          </Col> */}
          <Col {...responsiveColumns}>
            <InputField
              id="remarks"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              name="remarks"
              type="string"
              values={values}
              errors={errors}
              placeholder="Reamrks"
              labelHtml="Remarks*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="active"
              control={control}
              name="active"
              type="select"
              values={values}
              options={DEFAULT_MAIL_ID}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Active"
              labelHtml="Active*"
            />
          </Col>
          {applicantDetails &&
            applicantDetails.entity &&
            applicantDetails.entity.mandate && (
              <>
                <Col md={24}>
                  <h5 style={{ fontWeight: 'bold' }}>Mandate Details</h5>
                </Col>
                <Col {...responsiveColumns}>
                  <InputField
                    id="status"
                    control={control}
                    name="mandateDetail.status"
                    // disabled
                    rules={{
                      required: false,
                      message: 'This Field should not be empty!',
                    }}
                    type="string"
                    errors={errors}
                    placeholder="Registration Status"
                    labelHtml="Registration Status*"
                  />
                </Col>
                <Col {...responsiveColumns}>
                  <InputField
                    id="emandateId"
                    control={control}
                    name="mandateDetail.emandateId"
                    // disabled
                    type="string"
                    rules={{
                      required: false,
                      message: 'This Field should not be empty!',
                    }}
                    errors={errors}
                    placeholder="E-mandate ID"
                    labelHtml="E-mandate ID*"
                  />
                </Col>
                <Col {...responsiveColumns}>
                  <InputField
                    id="umrn"
                    control={control}
                    name="mandateDetail.umrn"
                    // disabled
                    type="string"
                    rules={{
                      required: false,
                      message: 'This Field should not be empty!',
                    }}
                    errors={errors}
                    placeholder="UMRN"
                    labelHtml="UMRN*"
                  />
                </Col>
              </>
            )}
        </Row>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            disabled={activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'}
          >
            Save
          </Button>
          {'   '}
          <Button
            type="primary"
            onClick={addEmail}
          >
            Cancel
          </Button>
        </Form.Item>
      </form>
    </div>
  );
}

AddEmailDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  addEmailDetails: makeSelectAddEmailDetails(),
});

const withConnect = connect(
  mapStateToProps,
//  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(AddEmailDetails);
