import _ from 'lodash';
import {
  take,
  call,
  put,
  select,
  takeEvery,
  debounce,
} from 'redux-saga/effects';
import request from 'utils/request';
import {
  ADD_EMAIL_DETAILS,
} from './constants';
import {
  addEmailDetailsSuccess,
  addEmailDetailsError,
} from './actions';
import env from '../../../environment';

export default function* addEmailDetailsSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(ADD_EMAIL_DETAILS, addEmailDetails);
}

function* addEmailDetails({ payload }) {

//console.log('Payload', payload)
  const payloadData = {
    emailId: payload.emailId,
    category: payload.category,
    defaultMailId: 'N',
    remarks: payload.remarks,
    active: payload.active,
    createdBy: JSON.parse(sessionStorage.getItem('userId')),
  };

  const reqUrl = `${env.EMAIL_COMMUNICATION_URL}`;
  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const appRes = yield call(request, reqUrl, options);
    if (appRes.success) {
      yield put(addEmailDetailsSuccess({message : _.get(appRes, 'body.message') || 'Emails saved successfully!!'}));
    } else yield put(addEmailDetailsError({message : _.get(appRes, 'body.message') || 'Emails Not saved successfully!!'}));
  } catch (error) {
    yield put(addEmailDetailsError(error));
    console.error(error);
  }
}
