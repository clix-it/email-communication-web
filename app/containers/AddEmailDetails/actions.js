/*
 *
 * AddEmail actions
 *
 */

import {
  DEFAULT_ACTION,
  ADD_EMAIL_DETAILS,
  ADD_EMAIL_DETAILS_ERROR,
  ADD_EMAIL_DETAILS_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function updateEmailDetails(payload) {
  return {
    type: ADD_EMAIL_DETAILS,
    payload,
  };
}

export function addEmailDetailsSuccess(data) {
  return {
    type: ADD_EMAIL_DETAILS_SUCCESS,
    data,
  };
}

export function addEmailDetailsError(err) {
  return {
    type: ADD_EMAIL_DETAILS_ERROR,
    data: err,
  };
}
