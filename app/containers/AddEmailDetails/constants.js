/*
 *
 * AddEmail constants
 *
 */
export const DEFAULT_ACTION = 'app/AddEmail/DEFAULT_ACTION';
export const ADD_EMAIL_DETAILS = 'app/AddEmail/ADD_EMAIL_DETAILS';
export const ADD_EMAIL_DETAILS_SUCCESS =
  'app/AddEmail/ADD_EMAIL_DETAILS_SUCCESS';
export const ADD_EMAIL_DETAILS_ERROR =
  'app/AddEmail/ADD_EMAIL_DETAILS_ERROR';