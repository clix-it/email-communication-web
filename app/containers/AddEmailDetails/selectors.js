import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the bankDetails state domain
 */

const selectAddEmailDetailsDomain = state => state.addEmailDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BankDetails
 */

const makeSelectAddEmailDetails = () =>
  createSelector(
    selectAddEmailDetailsDomain,
    substate => substate,
  );

export default makeSelectAddEmailDetails;
export { selectAddEmailDetailsDomain };
