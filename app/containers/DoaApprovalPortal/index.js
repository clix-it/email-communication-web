/**
 *PSV Portal
 */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';

//import CreditListingPage from '../../components/CreditListingPage';
import { loadApplications, setCurrentApp } from './actions';

export function DoaApprovalPortal({
  user,
  isLoading,
  error,
  dispatch,
  applications,
  requestType,
}) {
  // console.log('requestType', requestType);
  return (
    // <CreditListingPage
    //   applications={applications}
    //   error={error}
    //   user={user}
    //   dispatch={dispatch}
    //   requestType={requestType}
    //   isLoading={isLoading}
    //   loadApplications={loadApplications}
    //   setCurrentApp={setCurrentApp}
    //   activityType="SALES_DOA"
    // />
    <div></div>
  );
}

DoaApprovalPortal.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  user: state.global.user,
  isLoading: state.doaApprovalPortal.loading,
  error: state.doaApprovalPortal.error,
  applications: state.doaApprovalPortal.applications,
});
function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

DoaApprovalPortal.propTypes = {
  user: PropTypes.object,
  applications: PropTypes.arrayOf(PropTypes.object),
  isLoading: PropTypes.bool,
  error: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  requestType: PropTypes.string,
};

DoaApprovalPortal.defaultProps = {
  user: {},
  applications: [],
  isLoading: false,
  error: {},
  requestType: '',
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'doaApprovalPortal', reducer });

const withSaga = useInjectSaga({ key: 'doaApprovalPortal', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(DoaApprovalPortal);
