import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the creditPortal state domain
 */

const selectDoaApprovalDomain = state =>
  state.doaApprovalPortal || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by CreditPortal
 */

const makeSelectDoaApprovalPortal = () =>
  createSelector(
    selectDoaApprovalDomain,
    substate => substate,
  );

export default makeSelectDoaApprovalPortal;
export { selectDoaApprovalDomain };
