/**
 * DOA Approval Portal Reducer
 */
import * as actions from './constants';

export const initialState = {
  loading: false,
  applications: [],
  error: {},
};

const DoaApprovalReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.DOA_LOAD_APPLICATIONS_INIT:
      return { ...state, loading: true };

    case actions.DOA_LOAD_APPLICATIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: { status: false, message: '' },
        applications: action.payload.applications,
      };

    case actions.DOA_LOAD_APPLICATIONS_ERROR: {
      return {
        ...state,
        loading: false,
        error: {
          status: true,
          message: action.payload.errorMessage,
          severity: action.severity,
        },
      };
    }

    case actions.DOA_SET_CURRENT_APPLICATION: {
      return {
        ...state,
        currentApplication: action.app,
      };
    }
    default: {
      return state;
    }
  }
};

export default DoaApprovalReducer;
