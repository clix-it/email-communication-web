import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import {
  loadApplicationsInit,
  loadApplicationsSuccess,
  loadApplicationsError,
} from './actions';

function* fetchApplicationsSaga(action) {
  yield put(loadApplicationsInit());
  try {
    const response = yield call(
      request,
      `${env.API_URL}/makerChecker/v2/fetch/pending/activities`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({
          userId: JSON.parse(sessionStorage.getItem('userId')),
          // userRole: JSON.parse(sessionStorage.getItem('userRole')),
          userRole: '',
          startDate: action.payload.startDate,
          endDate: action.payload.endDate,
          appId: action.payload.appId,
          requestType: action.payload.requestType,
          panNumber: action.payload.panNumber,
          customerName: action.payload.customerName,
          userActivity: [action.payload.activityType],
        }),
      },
    );

    if (response.success || response.status) {
      yield put(loadApplicationsSuccess(response.data));
    } else {
      yield put(loadApplicationsSuccess([]));
      message.info(
        'Oops! No pending applications found for the searched criteria :( ',
      );
    }
  } catch (err) {
    yield put(
      loadApplicationsError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

export default function* doaApprovalSaga() {
  yield takeEvery(actions.DOA_LOAD_APPLICATIONS, fetchApplicationsSaga);
}
