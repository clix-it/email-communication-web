/*
 *
 * BankDetails constants
 *
 */

export const SEND_EMAIL = 'app/SendEmail/SEND_EMAIL';
export const SEND_EMAIL_SUCCESS =
  'app/SendEmail/SEND_EMAIL_SUCCESS';
export const SEND_EMAIL_ERROR =
  'app/SendEmail/SEND_EMAIL_ERROR';

  export const FETCH_SENDER_EMAIL = 'app/SendEmail/FETCH_SENDER_EMAIL';
  export const FETCH_SENDER_EMAIL_SUCCESS =
    'app/SendEmail/FETCH_SENDER_EMAIL_SUCCESS';
  export const FETCH_SENDER_EMAIL_ERROR =
    'app/SendEmail/FETCH_SENDER_EMAIL_ERROR';

  export const FETCH_CC_EMAIL = 'app/SendEmail/FETCH_CC_EMAIL';
  export const FETCH_CC_EMAIL_SUCCESS =
    'app/SendEmail/FETCH_CC_EMAIL_SUCCESS';
  export const FETCH_CC_EMAIL_ERROR =
    'app/SendEmail/FETCH_CC_EMAIL_ERROR';

  export const FETCH_BCC_EMAIL = 'app/SendEmail/FETCH_BCC_EMAIL';
  export const FETCH_BCC_EMAIL_SUCCESS =
    'app/SendEmail/FETCH_BCC_EMAIL_SUCCESS';
  export const FETCH_BCC_EMAIL_ERROR =
    'app/SendEmail/FETCH_BCC_EMAIL_ERROR';

  export const FETCH_TEMPLATE_CODE = 'app/SendEmail/FETCH_TEMPLATE_CODE';
  export const FETCH_TEMPLATE_CODE_SUCCESS =
    'app/SendEmail/FETCH_TEMPLATE_CODE_SUCCESS';
  export const FETCH_TEMPLATE_CODE_ERROR =
    'app/SendEmail/FETCH_TEMPLATE_CODE_ERROR';