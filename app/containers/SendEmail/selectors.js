import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the bankDetails state domain
 */

const selectSendEmailDomain = state => state.sendEmail || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BankDetails
 */

const makeSelectSendEmail = () =>
  createSelector(
    selectSendEmailDomain,
    substate => substate,
  );

export default makeSelectSendEmail;
export { selectSendEmailDomain };
