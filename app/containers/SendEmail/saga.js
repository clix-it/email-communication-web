import _ from 'lodash';
import {
  take,
  call,
  put,
  select,
  takeEvery,
  debounce,
} from 'redux-saga/effects';
import request from 'utils/request';
import {
  SEND_EMAIL,
  FETCH_SENDER_EMAIL,
  FETCH_CC_EMAIL,
  FETCH_BCC_EMAIL,
  FETCH_TEMPLATE_CODE,
} from './constants';
import {
  sendEmailSuccess,
  sendEmailError,
  fetchSenderEmailSuccess,
  fetchSenderEmailError,
  fetchCcEmailSuccess,
  fetchCcEmailError,
  fetchBccEmailSuccess,
  fetchBccEmailError,
  fetchTemplateCodesSuccess,
  fetchTemplateCodesError,
} from './actions';
import env from '../../../environment';

export default function* bankDetailsSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(SEND_EMAIL, sendEmails);
  yield takeEvery(FETCH_SENDER_EMAIL, fetchSenderEmails);
  yield takeEvery(FETCH_CC_EMAIL, fetchCcEmails);
  yield takeEvery(FETCH_BCC_EMAIL, fetchBccEmails);
  yield takeEvery(FETCH_TEMPLATE_CODE, fetchTemplateCodes);
}

function* sendEmails({ payload }) {
  console.log('Payload', payload);
  const payloadData = {
    from: payload.senderMailId,
    fromName: payload.fromName,
    excelFileStream: payload.upload,
    ccEmailIds: payload.ccMailId,
    bccEmailIds: payload.bccMailId,
    subject: payload.subject,
    templateId: payload.templateId,
  };

  const reqUrl = `${env.SEND_EMAIL}`;
  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const appRes = yield call(request, reqUrl, options);
    if (appRes.success) {
      yield put(
        sendEmailSuccess({
          message: _.get(appRes, 'message') || 'Email Sent successfully!!',
        }),
      );
    } else
      yield put(
        sendEmailError({
          message:
            _.get(appRes, 'error.message') || 'Email Not sent successfully!!',
        }),
      );
  } catch (error) {
    yield put(sendEmailError(error));
    console.error(error);
  }
}

//Fetch Sender

function* fetchSenderEmails(payload) {
  // Select username from store
  const requestURL = `${env.GET_EMAIL_SEARCH}`;

  const payloadData = {
    category: 'SENDER_EMAIL_ID',
  };

  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const response = yield call(request, requestURL, options);

    if (response.success) {
      yield put(fetchSenderEmailSuccess(response));
    } else yield put(fetchSenderEmailError(response));
  } catch (err) {
    yield put(fetchSenderEmailError(err));
  }
}

//Fetch CC

function* fetchCcEmails(payload) {
  const requestURL = `${env.GET_EMAIL_SEARCH}`;

  const payloadData = {
    category: 'CC_EMAIL_ID',
  };

  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const response = yield call(request, requestURL, options);

    if (response.success) {
      yield put(fetchCcEmailSuccess(response));
    } else yield put(fetchCcEmailError(response));
  } catch (err) {
    yield put(fetchCcEmailError(err));
  }
}

//Fetch CC

function* fetchBccEmails(payload) {
  const requestURL = `${env.GET_EMAIL_SEARCH}`;

  const payloadData = {
    category: 'BCC_EMAIL_ID',
  };

  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const response = yield call(request, requestURL, options);

    if (response.success) {
      yield put(fetchBccEmailSuccess(response));
    } else yield put(fetchBccEmailError(response));
  } catch (err) {
    yield put(fetchBccEmailError(err));
  }
}

//Fetch Template Codes

function* fetchTemplateCodes(payload) {
  const requestURL = `${env.GET_TEMPLATE}`;

  const options = {
    method: 'GET',
    headers: env.headers,
  };

  try {
    const response = yield call(request, requestURL, options);

    if (response.success) {
      yield put(fetchTemplateCodesSuccess(response));
    } else yield put(fetchTemplateCodesError(response));
  } catch (err) {
    yield put(fetchTemplateCodesError(err));
  }
}
