/*
 *
 * BankDetails reducer
 *
 */
import produce from 'immer';
import {
  SEND_EMAIL,
  SEND_EMAIL_SUCCESS,
  SEND_EMAIL_ERROR,
  FETCH_SENDER_EMAIL,
  FETCH_SENDER_EMAIL_SUCCESS,
  FETCH_SENDER_EMAIL_ERROR,
  FETCH_CC_EMAIL,
  FETCH_CC_EMAIL_SUCCESS,
  FETCH_CC_EMAIL_ERROR,
  FETCH_BCC_EMAIL,
  FETCH_BCC_EMAIL_SUCCESS,
  FETCH_BCC_EMAIL_ERROR,
  FETCH_TEMPLATE_CODE,
  FETCH_TEMPLATE_CODE_SUCCESS,
  FETCH_TEMPLATE_CODE_ERROR,
} from './constants';

export const initialState = {
  response: false,
  error: false,
  payload: false,
  pennyDropDetails: [],
  senderEmails: [],
  ccEmails: [],
  bccEmails: [],
  templateCodes: [],
};

/* eslint-disable default-case, no-param-reassign */
const sendEmailReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      // case DEFAULT_ACTION:
      //   break;
      case SEND_EMAIL:
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case SEND_EMAIL_SUCCESS:
        draft.error = false;
        draft.response = action.data.message;
        break;
      case SEND_EMAIL_ERROR:
        draft.response = false;
        draft.error = action.data.message;
        break;

        //Fetch Sender
        case FETCH_SENDER_EMAIL:
        draft.loading = true;
       
        break;
      case FETCH_SENDER_EMAIL_SUCCESS:
        draft.loading = false;
        draft.error = false;
        
        draft.senderEmails =  action.response &&
        action.response.data.map((item, index) => {
          return { ...item, index: index + 1 };
        })  
      
        break;
      case FETCH_SENDER_EMAIL_ERROR:
        draft.loading = false;
        draft.error = action.error;
        break;

        //Fetch CC
        case FETCH_CC_EMAIL:
        draft.loading = true;
        break;
      case FETCH_CC_EMAIL_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.ccEmails =  action.response &&
        action.response.data.map((item, index) => {
          return { ...item, index: index + 1 };
        })  
        break;
      case FETCH_CC_EMAIL_ERROR:
        draft.loading = false;
        draft.error = action.error;
        break;
          //Fetch BCC
        case FETCH_BCC_EMAIL:
          draft.loading = true;
          break;
        case FETCH_BCC_EMAIL_SUCCESS:
          draft.loading = false;
          draft.error = false;
          draft.bccEmails =  action.response &&
          action.response.data.map((item, index) => {
            return { ...item, index: index + 1 };
          })  
          break;
        case FETCH_BCC_EMAIL_ERROR:
          draft.loading = false;
          draft.error = action.error;
          break;

         //Fetch Template Codes
        case FETCH_TEMPLATE_CODE:
          draft.loading = true;
          break;
        case FETCH_TEMPLATE_CODE_SUCCESS:
          draft.loading = false;
          draft.error = false;
          
          draft.templateCodes =  action.response &&
          action.response.data.map((item, index) => {
            return { ...item, index: index + 1 };
          })  
        
          break;
        case FETCH_TEMPLATE_CODE_ERROR:
          draft.loading = false;
          draft.error = action.error;
          break;
  
     }
  });

export default sendEmailReducer;
