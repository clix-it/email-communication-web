/**
 *
 * SendEmail
 *
 */
import _, { constant } from 'lodash';
import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useForm, Controller } from 'react-hook-form';
import {
  Form,
  Row,
  Col,
  Button,
  Radio,
  message,
  Divider,
  Upload,
  Card,
} from 'antd';
import { UploadOutlined, showPreviewIcon, removeIcon } from '@ant-design/icons';
import reducer from './reducer';
import saga from './saga';
import ErrorMessage from '../../components/ErrorMessage/index';
import InputField from '../../components/InputField';
import {
  sendEmailAction,
  fetchSenderEmail,
  fetchCcEmail,
  fetchBccEmail,
  fetchTemplateCodes,
} from './actions';
import {
  setFormValidationError,
  isDirtyForm,
  loadEntityDetails,
} from '../ApplicantDetails/actions';
import { useHistory } from 'react-router-dom';
import 'react-quill/dist/quill.snow.css';
import makeSelectSendEmail from './selectors';

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: { span: 10, offset: 2, pull: 2 },
};
export function SendEmailFunction({
  dispatch,
  applicantDetails,
  activity,
  sendEmail,
}) {
  const {
    senderEmails = [],
    ccEmails = [],
    bccEmails = [],
    templateCodes = [],
  } = sendEmail;

  useInjectReducer({ key: 'sendEmail', reducer });
  useInjectSaga({ key: 'sendEmail', saga });


  const [tempId, setTempIdOptions] = useState([]);

  const [errorMeesage, setErrorMessage] = useState(false);

  const [senderEmail, setSenderEmail] = useState([]);
  const [ccEmail, setCcEmail] = useState([]);
  const [bccEmail, setBccEmail] = useState([]);

 

  const {
    register,
    handleSubmit,
    getValues,
    errors,
    control,
    watch,
    setError,
    reset,
    clearError,
    formState,
    setValue,
    triggerValidation,
  } = useForm({
    mode: 'onChange',
  });

  const { dirtyFields, dirtyFieldsSinceLastSubmit } = formState;
  //  console.log(dirtyFields, 'dirtyFields');
  useEffect(() => {
    dispatch(isDirtyForm({ 5: formState.dirty }));
    console.log(formState.dirty, 'dirty bank', formState.dirtySinceLastSubmit);
  }, [formState.dirty]);

  const values = watch();
  // console.log(values, 'values');

  // console.log(formState.dirty, 'dirty val');

  // console.log(values, 'values bank details', errors);
  const senderMailId = senderEmails.map(data => data.emailId);
  const ccMailId = ccEmails.map(data => data.emailId);
  const bccMailId = bccEmails.map(data => data.emailId);
  const tempIdsData = templateCodes.map(data => data.templateId);
  // const defaultSender = senderEmails.find(data => data.defaultMailId === 'Y');
  // const defSender = defaultSender.emailId

  // useEffect(() => {
  //   setDefaultSenderEmail(defSender)
  // }, [defSender, defaultSender]);

  useEffect(() => {
    setTempIdOptions(tempIdsData);
  }, [tempIdsData]);

  useEffect(() => {
    setSenderEmail(senderMailId);
  }, [senderMailId]);

  useEffect(() => {
    setCcEmail(ccMailId);
  }, [ccMailId]);

  useEffect(() => {
    setBccEmail(bccMailId);
  }, [bccMailId]);

  useEffect(() => {
    dispatch(fetchSenderEmail());
    dispatch(fetchCcEmail());
    dispatch(fetchBccEmail());
    dispatch(fetchTemplateCodes());
  }, []);

  const [response, setResponse] = useState(false);

  useEffect(() => {
    if (sendEmail.response && response) {
      message.success(sendEmail.response);
      setErrorMessage(false);
      dispatch(loadEntityDetails());
      setResponse(false);
    }

    if (sendEmail.error && response) {
      message.error(sendEmail.error);
      setErrorMessage(true);
    }
  }, [sendEmail.response, sendEmail.error]);

  //Submit
  const onSubmit = data => {
    dispatch(sendEmailAction(data));
    setResponse(true);
  };

  //ONchange

  const handleFileUpload = async file => {
    const fileUpload = file.file;
    const base64 = await convertBase64(fileUpload);
    //  setBaseImage(base64);
    const splitBase64 = base64.split('base64,');
    const splitOutput = splitBase64[1];
    //  dispatch(excelUpload({base: splitOutput}));
    // setResponse(true);
    //  console.log('Excel Upload', splitOutput)
    setValue('upload', splitOutput, { shouldValidate: true });
    message.success(`${file.file.name} Uploaded Successfully`);
  };

  const convertBase64 = file => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = error => {
        reject(error);
      };
    });
  };
  return (
    <div className="App">
      <h4>Send Email</h4>
      <Divider />
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={[16, 16]}>
          {/* start */}
          <Col md={24}>
            {/* <h5 style={{ fontWeight: 'bold' }}>Disbursement Bank Details</h5> */}
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="fromName"
              control={control}
              name="fromName"
              type="string"
              values={values}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="From Name"
              labelHtml="From Name*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="senderMailId"
              control={control}
              name="senderMailId"
              values={values}
              type="select"
              //  defaultValue="priyanka.tadsare@clix.capital"
              // mode="multiple"
              options={senderEmail}
              //  showSearch
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="Sender Mail ID"
              labelHtml="Sender Mail ID*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="ccMailId"
              control={control}
              name="ccMailId"
              values={values}
              type="select"
              //  defaultValue="priyanka.tadsare@clix.capital"
              mode="multiple"
              options={ccEmail}
              showSearch
              // rules={{
              //   required: {
              //     value: true,
              //     message: 'This field cannot be left empty',
              //   },
              // }}
              errors={errors}
              placeholder="CC Mail ID"
              labelHtml="CC Mail ID"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="bccMailId"
              control={control}
              name="bccMailId"
              //  defaultValue="priyanka.tadsare@clix.capital"
              values={values}
              type="select"
              mode="multiple"
              options={bccEmail}
              showSearch
              // rules={{
              //   required: {
              //     value: true,
              //     message: 'This field cannot be left empty',
              //   },
              // }}
              errors={errors}
              placeholder="BCC Mail ID"
              labelHtml="BCC Mail ID"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="subject"
              control={control}
              name="subject"
              type="string"
              values={values}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="Subject"
              labelHtml="Subject*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="templateId"
              control={control}
              name="templateId"
              type="select"
              options={tempIdsData}
              values={values}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              errors={errors}
              placeholder="Template Code"
              labelHtml="Template Code*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <Form.Item label="Upload Excel">
              <Controller
                name="upload"
                control={control}
                errors={errors}
                values={values}
                //defaultValue=""
                rules={{
                  required: {
                    value: true,
                    message: 'This field cannot be left empty',
                  },
                }}
                style={{ marginBottom: '2rem' }}
                as={
                  <Upload
                    accept=".xlsx, .xls"
                    showUploadList={false}
                    customRequest={handleFileUpload}
                  >
                    <Button>
                      <UploadOutlined /> Upload Excel
                    </Button>
                  </Upload>
                }
              />
              <ErrorMessage name="upload" errors={errors} />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            disabled={activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'}
          >
            Send Email
          </Button>
          {errorMeesage == true && (
            <Card
              title="Email Not sent successfully!!"
              bordered={true}
              style={{ width: 400, marginTop: '2rem', color: 'red' }}
            >
              {sendEmail.error}
            </Card>
          )}
          {'   '}
        </Form.Item>
      </form>
    </div>
  );
}

SendEmailFunction.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  sendEmail: makeSelectSendEmail(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(SendEmailFunction);
