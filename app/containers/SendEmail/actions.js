/*
 *
 * BankDetails actions
 *
 */

import {
  SEND_EMAIL,
  SEND_EMAIL_SUCCESS,
  SEND_EMAIL_ERROR,
  FETCH_SENDER_EMAIL,
  FETCH_SENDER_EMAIL_SUCCESS,
  FETCH_SENDER_EMAIL_ERROR,
  FETCH_CC_EMAIL,
  FETCH_CC_EMAIL_SUCCESS,
  FETCH_CC_EMAIL_ERROR,
  FETCH_BCC_EMAIL,
  FETCH_BCC_EMAIL_SUCCESS,
  FETCH_BCC_EMAIL_ERROR,
  FETCH_TEMPLATE_CODE,
  FETCH_TEMPLATE_CODE_SUCCESS,
  FETCH_TEMPLATE_CODE_ERROR,
} from './constants';

export function sendEmailAction(payload) {
  return {
    type: SEND_EMAIL,
    payload,
  };
}

export function sendEmailSuccess(data) {
  return {
    type: SEND_EMAIL_SUCCESS,
    data,
  };
}

export function sendEmailError(err) {
  return {
    type: SEND_EMAIL_ERROR,
    data: err,
  };
}

// Sender

export function fetchSenderEmail() {
  return {
    type: FETCH_SENDER_EMAIL,
  };
}

export function fetchSenderEmailSuccess(response) {
  return {
    type: FETCH_SENDER_EMAIL_SUCCESS,
    response,
  };
}

export function fetchSenderEmailError(error) {
  return {
    type: FETCH_SENDER_EMAIL_ERROR,
    error
  };
}

//CC 

export function fetchCcEmail() {
  return {
    type: FETCH_CC_EMAIL,
  };
}

export function fetchCcEmailSuccess(response) {
  return {
    type: FETCH_CC_EMAIL_SUCCESS,
    response,
  };
}

export function fetchCcEmailError(error) {
  return {
    type: FETCH_CC_EMAIL_ERROR,
    error
  };
}


//BCC 

export function fetchBccEmail() {
  return {
    type: FETCH_BCC_EMAIL,
  };
}

export function fetchBccEmailSuccess(response) {
  return {
    type: FETCH_BCC_EMAIL_SUCCESS,
    response,
  };
}

export function fetchBccEmailError(error) {
  return {
    type: FETCH_BCC_EMAIL_ERROR,
    error
  };
}

//Template Codes

export function fetchTemplateCodes() {
  return {
    type: FETCH_TEMPLATE_CODE,
  };
}

export function fetchTemplateCodesSuccess(response) {
  return {
    type: FETCH_TEMPLATE_CODE_SUCCESS,
    response,
  };
}

export function fetchTemplateCodesError(error) {
  return {
    type: FETCH_TEMPLATE_CODE_ERROR,
    error
  };
}