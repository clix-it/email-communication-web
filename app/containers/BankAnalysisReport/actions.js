/*
 *
 * BankAnalysisReport actions
 *
 */

import {
  FETCH_BANK_REPORT,
  FETCH_BANK_REPORT_SUCCESS,
  FETCH_BANK_REPORT_ERROR,
  SET_CURRENT_REPORT,
} from './constants';

export function fetchBankAnalysisReport(appId) {
  return {
    type: FETCH_BANK_REPORT,
    appId,
  };
}

export function fetchBankAnalysisReportSuccess(data) {
  return {
    type: FETCH_BANK_REPORT_SUCCESS,
    data,
  };
}

export function fetchBankAnalysisReportError(message) {
  return {
    type: FETCH_BANK_REPORT_ERROR,
    message,
  };
}

export function setSelectedReport(report) {
  return {
    type: SET_CURRENT_REPORT,
    report,
  };
}
