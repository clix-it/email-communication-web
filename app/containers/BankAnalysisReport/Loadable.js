/**
 *
 * Asynchronously loads the component for BankAnalysisReport
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
