import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the bankAnalysisReport state domain
 */

const selectBankAnalysisReportDomain = state =>
  state.bankAnalysisReport || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BankAnalysisReport
 */

const makeSelectBankAnalysisReport = () =>
  createSelector(
    selectBankAnalysisReportDomain,
    substate => substate,
  );

export default makeSelectBankAnalysisReport;
export { selectBankAnalysisReportDomain };
