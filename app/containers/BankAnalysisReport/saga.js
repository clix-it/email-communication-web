import _ from 'lodash';
import { takeEvery, put, select, all, call } from 'redux-saga/effects';
import request from 'utils/request';
import { message } from 'antd';
import * as actions from './constants';
import env from '../../../environment';
import data from './data.json';
import {
  fetchBankAnalysisReportSuccess,
  fetchBankAnalysisReportError,
} from './actions';

function* fetchEntityDetailsSaga({ appId }) {
  try {
    const url = `${
      env.BANK_ANALYSIS_REPORT_URL
    }/getanalysisdata?type=APPID&value=${appId}`;
    const response = yield call(request, url, {
      method: 'GET',
      headers: env.headers,
    });
    if (response && response.length > 0) {
      yield put(fetchBankAnalysisReportSuccess(response));
    } else {
      yield put(fetchBankAnalysisReportError('No data found!'));
      message.info('No data found!');
    }
  } catch (err) {
    yield put(fetchBankAnalysisReportError('No data found!'));
    message.info('No data found!');
  }
}

export default function* bankAnalysisReportSaga() {
  yield takeEvery(actions.FETCH_BANK_REPORT, fetchEntityDetailsSaga);
}
