/**
 * Bank Analysis Report Reducer
 */
import * as actions from './constants';
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';

export const initialState = {
  loading: false,
  error: '',
  reportData: [],
  selectedReport: {},
};

const bankAnalysisReportReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.FETCH_BANK_REPORT:
        draft.loading = true;
        break;
      case actions.FETCH_BANK_REPORT_SUCCESS:
        draft.loading = false;
        draft.reportData = action.data
          .filter(item => !item.analysisData['FinancialStatement'])
          .map((item, index) => {
            return { ...item, index: index };
          });
        draft.selectedReport = draft.reportData[0];
        break;
      case actions.FETCH_BANK_REPORT_ERROR:
        draft.loading = false;
        draft.error = action.message;
        draft.reportData = [];
        draft.selectedReport = {};
        break;
      case actions.SET_CURRENT_REPORT:
        draft.selectedReport = action.report;
        break;
      case LOCATION_CHANGE:
        draft.reportData = [];
        draft.loading = false;
        draft.selectedReport = {};
        draft.error = '';
        break;
      default: {
        return state;
      }
    }
  });

export default bankAnalysisReportReducer;
