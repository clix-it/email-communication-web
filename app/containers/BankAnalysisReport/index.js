/**
 *
 * BankAnalysisReport
 *
 */
import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import styled from 'styled-components';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import makeSelectBankAnalysisReport from './selectors';

import { fetchBankAnalysisReport, setSelectedReport } from './actions';
import BankAnalysisComponent from '../../components/BankAnalysisComponent';
import { Menu, Button } from 'antd';
//import KycComponent from '../../components/KycComponent';

export function BankAnalysisReport({ bankAnalysisReport, dispatch, appId }) {
  const {
    loading = false,
    error = '',
    selectedReport = {},
    reportData = [],
  } = bankAnalysisReport;

  useEffect(() => {
    if (appId) {
      dispatch(fetchBankAnalysisReport(appId));
    }
  }, [appId]);

  const setReport = id => {
    const report =
      _.find(reportData, { index: id.key }) ||
      _.find(reportData, { index: parseInt(id.key) });
    dispatch(setSelectedReport(report));
  };

  const refreshData = () => {
    dispatch(fetchBankAnalysisReport(appId));
  };

  return (
    <>
      <Button type="primary" onClick={refreshData}>
        Refresh
      </Button>
      <Menu
        onClick={setReport}
        defaultSelectedKeys={[(selectedReport || {}).index]}
        selectedKeys={[String((selectedReport || {}).index)]}
        mode="horizontal"
        style={{ width: '95%' }}
      >
        {_.map(reportData, (report, index) => (
          <Menu.Item key={String(report.index)}>{`Report ${index +
            1}`}</Menu.Item>
        ))}
      </Menu>
      <BankAnalysisComponent
        selectedReport={selectedReport}
        loading={loading}
        error={error}
      />
    </>
  );
}

BankAnalysisReport.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  bankAnalysisReport: makeSelectBankAnalysisReport(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = useInjectReducer({ key: 'bankAnalysisReport', reducer });
const withSaga = useInjectSaga({ key: 'bankAnalysisReport', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(BankAnalysisReport);
