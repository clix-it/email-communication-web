/**
 *
 * CustomerKycDetails
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import { makeSelectCustomerKycDetails } from './selectors';

import { loadOkycDetails, loadCkycDetails } from './actions';
import { Spin } from 'antd';
import CustomerKycComponent from '../../components/CustomerKycComponent';

export function CustomerKycDetails({ user, kycType, cuid, customerKycDetails, dispatch }) {
  const {
    okycData = {},
    ckycData = {},
    loading = false,
    okycDocsData = {},
    ckycDocsData = {},
    dkycDocs = [],
    dkycDetails = {},
  } = customerKycDetails;

  useEffect(() => {     
    if (
      user &&
      user.cuid &&
      kycType == 'OKYC'
    ) {
      dispatch(loadOkycDetails(user));
    } else if (
      user &&
      user.cuid &&
      kycType == 'CKYC'
    ) {
      dispatch(loadCkycDetails(user));
    }
  }, [cuid]);

  return (
    <Spin spinning={loading} tip="Loading...">
      {kycType === 'CKYC' ? (
        <CustomerKycComponent
          kycType={kycType}
          data={ckycData}
          ckycDocsData={ckycDocsData}
          dispatch={dispatch}
        />
      ) : kycType === 'OKYC' ? (
        <CustomerKycComponent
          kycType={kycType}
          data={okycData}
          okycDocsData={okycDocsData}
          dispatch={dispatch}
        />
      ) : (
        <CustomerKycComponent
          kycType={kycType}
          data={okycData}
          okycDocsData={okycDocsData}
          dispatch={dispatch}
          dkycDocs={dkycDocs}
          dkycDetails={dkycDetails}
        />
      )}
    </Spin>
  );
}

CustomerKycDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  customerKycDetails: makeSelectCustomerKycDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = useInjectReducer({ key: 'customerKycDetails', reducer });
const withSaga = useInjectSaga({ key: 'customerKycDetails', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(CustomerKycDetails);
