import { takeEvery, put, all, takeLatest, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import {
  loadOKYCSuccess,
  loadOKYCError,
  loadOKYCDocsSuccess,
  loadCKYCSuccess,
  loadCKYCDocsSuccess,
  loadDKYCDocsSuccess,
  loadDKYCDocsError,
  loadDKYCDetails,
} from './actions';
import _ from 'lodash';

function* fetchOkycDetailsSaga({ user }) {
  const result = [];
  const okycDocs = [];
  const apiresult = yield fetch(
    `${env.OKYC_URL}/getById?type=CUID&value=${user.cuid}`,
    {
      headers: env.headers,
      method: 'GET',
    },
  );
  const responseBody = yield apiresult.json();
  if (responseBody.success === true) {
    responseBody.details.forEach(item => {
      result.push(item);
    });
  }
  debugger;
  if (result.length > 0) {
    yield all(
      result.map(function*(item) {
        yield* fetchOkycDocs(item, okycDocs);
      }),
    );
  }
  const okycData = result.length > 0 ? result : [];
  const okycDocsData = okycDocs.length > 0 ? okycDocs : [];
  yield put(loadOKYCSuccess(okycData));
  yield put(loadOKYCDocsSuccess(okycDocsData));
}

function* fetchOkycDocs(item, okycDocs) {
  let responses = item.details.dmsIds
    .filter(item => item.documentId != '' && item.documentId != null)
    .map(item => {
      let promise = request(
        `${env.APP_DOCS_V1_API_URL}/download/${item.documentId}`,
        {
          method: 'GET',
          headers: env.headers,
        },
      );
      return promise;
    });
  let responeBodies = yield Promise.all(responses);
  if (responeBodies && responeBodies.length > 0) {
    responeBodies[0].cuid = item.cuid;
    okycDocs.push(responeBodies[0]);
  }
}

function* fetchCkycDetailsSaga({ user }) {
  const result = [];
  const ckycDocs = [];
  const apiresult = yield fetch(
    `${env.CKYC_URL}/find?type=CKYCNUMBER&value=${
      user.identities && user.identities.ckycNumber
        ? user.identities.ckycNumber
        : '0'
    }`,
    {
      headers: env.headers,
      method: 'GET',
    },
  );
  const responseBody = yield apiresult.json();
  if (responseBody && responseBody.length > 0) {
    responseBody.forEach(item => {
      result.push(item);
    });
  }
  debugger;
  if (result.length > 0) {
    yield all(
      result.map(function*(item) {
        yield* fetchCkycDocs(item, ckycDocs);
      }),
    );
  }
  const ckycData = result.length > 0 ? result : [];
  const ckycDocsData = ckycDocs.length > 0 ? ckycDocs : [];
  yield put(loadCKYCSuccess(ckycData));
  yield put(loadCKYCDocsSuccess(ckycDocsData));
}

function* fetchCkycDocs(item, ckycDocs) {
  let responses = item.kycDetails.details
    .filter(item => item.xmlDocumentId != '' && item.xmlDocumentId != null)
    .map(item => {
      let promise = request(
        `${env.APP_DOCS_V1_API_URL}/download/${item.xmlDocumentId}`,
        {
          method: 'GET',
          headers: env.headers,
        },
      );
      return promise;
    });
  let responeBodies = yield Promise.all(responses);
  if(responeBodies && responeBodies.length > 0){
    responeBodies[0].kycRefId = item.kycRefId;
    ckycDocs.push(responeBodies[0]);
  }
}

export default function* customerKycDetailsSaga() {
  yield takeLatest(actions.LOAD_CUSTOMER_OKYC, fetchOkycDetailsSaga);
  yield takeLatest(actions.LOAD_CUSTOMER_CKYC, fetchCkycDetailsSaga);
}
