/**
 * Hunter O/P Reducer
 */
import * as actions from './constants';
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';
import { SET_MAIN_USER } from '../RelationshipDetails/constants';

export const initialState = {
  loading: false,
  okycData: [],
  ckycData: [],
  dkycDocs: [],
  error: {},
  okycDocsData: [],
  ckycDocsData: [],
  dkycDetails: {},
  navigateToDocumentTab: false,
};

const customerKycDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.LOAD_CUSTOMER_OKYC:
        draft.loading = true;
        break;
      case actions.LOAD_CUSTOMER_CKYC:
        draft.loading = true;
        break;
      case actions.LOAD_CUSTOMER_DKYC:
        draft.loading = true;
        break;        
      case actions.LOAD_CUSTOMER_OKYC_SUCCESS:
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.okycData = action.okycdata;
        break;
      case actions.LOAD_CUSTOMER_CKYC_SUCCESS:
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.ckycData = action.ckycdata;
        break;
      case actions.LOAD_CUSTOMER_DKYC_SUCCESS:
        draft.loading = false;
        draft.error = { status: false, message: '' };
        draft.dkycDocs = action.dkycDocs;
        break;
      case actions.CUSTOMER_DKYC_DETAILS:
        draft.dkycDetails = action.data;
        break;
      case actions.LOAD_CUSTOMER_DKYC_ERROR:
        draft.loading = false;
        draft.dkycDocs = [];
        break;
      case actions.LOAD_CUSTOMER_OKYC_ERROR: {
        draft.loading = false;
        draft.okycData = {};
        draft.okycDocsData = {};
        draft.error = {
          status: true,
          message: action.errorMessage,
        };
        break;
      }
      case actions.FETCH_CUSTOMER_OKYC_DOCS_SUCCESS: {
        draft.okycDocsData = action.data;
        break;
      }
      case actions.FETCH_CUSTOMER_CKYC_DOCS_SUCCESS: {
        draft.ckycDocsData = action.data;
        break;
      }
      case actions.NAVIGATE_TO_DOCUMENTS: {
        draft.navigateToDocumentTab = action.flag;
        break;
      }
      case SET_MAIN_USER:
        draft.loading = false;
        draft.okycData = [];
        draft.ckycData = [];
        draft.okycDocsData = [];
        draft.ckycDocsData = [];
        draft.dkycDocs = [];
        draft.dkycDetails = {};
        draft.navigateToDocumentTab = false;
        break;
      case LOCATION_CHANGE:
        draft.loading = false;
        draft.okycData = [];
        draft.ckycData = [];
        draft.okycDocsData = [];
        draft.ckycDocsData = [];
        draft.dkycDocs = [];
        draft.dkycDetails = {};
        draft.navigateToDocumentTab = false;
        break;
      default: {
        return state;
      }
    }
  });

export default customerKycDetailsReducer;
