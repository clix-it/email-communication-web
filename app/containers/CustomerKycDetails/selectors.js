import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the customerKycDetails state domain
 */

const selectCustomerKycDetailsDomain = state =>
  state.customerKycDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by CustomerKycDetails
 */

const makeSelectCustomerKycDetails = () =>
  createSelector(
    selectCustomerKycDetailsDomain,
    substate => substate,
  );

export { makeSelectCustomerKycDetails };
