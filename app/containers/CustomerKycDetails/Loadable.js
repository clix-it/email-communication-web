/**
 *
 * Asynchronously loads the component for CustomerKycDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
