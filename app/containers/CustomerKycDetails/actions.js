/*
 *
 * CustomerKycDetails actions
 *
 */

import * as actions from './constants';

export function loadOkycDetails(user) {
  return {
    type: actions.LOAD_CUSTOMER_OKYC,
    user,
  };
}

export function loadOKYCSuccess(okycdata) {
  return {
    type: actions.LOAD_CUSTOMER_OKYC_SUCCESS,
    okycdata,
  };
}

export function loadOKYCError(errorMessage) {
  return {
    type: actions.LOAD_CUSTOMER_OKYC_ERROR,
    errorMessage
  };
}

export function loadOKYCDocsSuccess(data) {
  return {
    type: actions.FETCH_CUSTOMER_OKYC_DOCS_SUCCESS,
    data,
  };
}

//CKYC actions

export function loadCkycDetails(user) {
  return {
    type: actions.LOAD_CUSTOMER_CKYC,
    user,
  };
}

export function loadCKYCSuccess(ckycdata) {
  return {
    type: actions.LOAD_CUSTOMER_CKYC_SUCCESS,
    ckycdata,
  };
}

export function loadCKYCDocsSuccess(data) {
  return {
    type: actions.FETCH_CUSTOMER_CKYC_DOCS_SUCCESS,
    data,
  };
}

//DKYC actions

export function loadDKYCDocs() {
  return {
    type: actions.LOAD_CUSTOMER_DKYC,
  };
}

export function loadDKYCDocsSuccess(dkycDocs) {
  return {
    type: actions.LOAD_CUSTOMER_DKYC_SUCCESS,
    dkycDocs,
  };
}

export function loadDKYCDetails(data) {
  return {
    type: actions.CUSTOMER_DKYC_DETAILS,
    data,
  };
}

export function loadDKYCDocsError() {
  return {
    type: actions.LOAD_CUSTOMER_DKYC_ERROR,
  };
}

export function navigateToDocumentsTab(flag) {
  return {
    type: actions.NAVIGATE_TO_DOCUMENTS,
    flag,
  };
}
