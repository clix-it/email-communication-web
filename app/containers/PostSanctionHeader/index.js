import React, { useState, memo, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import { PageHeader, Button, message } from 'antd';
import { makeDocsData } from 'containers/LeadDocuments/selectors';

import { createStructuredSelector } from 'reselect';
import { makeSelectPostSanctionHeader } from './selectors';
import {
  makeSelectApplicantDetails,
  makeSelectApplicantDetails2,
} from '../ApplicantDetails/selectors';
import { makeSelectApplications } from '../CreditPortal/selectors';
import { approvePostSanctionUser, rejectPostSanctionUser, clearMessages, referPostSanctionUser } from './actions';
import reducer from './reducer';
import saga from './saga';

import PopUp from '../../components/PopUp';
import { FORM_SCREENS } from './constants';

import { useLocation } from 'react-router-dom';

function PostSanctionHeader({
  dispatch,
  postSanctionHeader,
  creditPortal,
  docsData,
  applicantDetails,
  applicationsData,
}) {
  const { success, error } = postSanctionHeader;
  const { formValidationErrors = {} } = applicantDetails;
  const { currentApplication } = applicationsData;

  const [info, setInfo] = useState('');
  const [visibleApp, setVisibleApp] = useState(false);
  const [visibleRej, setVisibleRej] = useState(false);
  const [visibleRefer, setVisibleRefer] = useState(false);
  const [remarks, setRemarks] = useState('');
  const loc = useLocation();

  const handleOkApp = () => {
    const approveData = {
      status: 'APPROVED',
      remarks,
    };
    dispatch(approvePostSanctionUser(approveData, applicantDetails));
    setVisibleApp(false);
    setRemarks('');
  };

  const handleOkRej = () => {
    const rejData = {
      status: 'REJECTED',
      remarks,
    };
    dispatch(rejectPostSanctionUser(rejData));
    setVisibleRej(false);
    setRemarks('');
  };

  const handleOkRefer = () => {
    const referData = {
      status: 'REFER',
      remarks,
    };
    dispatch(referPostSanctionUser(referData));
    setVisibleRefer(false);
    setRemarks('');
  };

  useEffect(() => {
    if (success.status && success.message) {
      message.success(success.message, 4);
    }
    if (error.status && error.message) {
      message.error(error.message, 4);
    }
    dispatch(clearMessages());
  }, [success.status, error.status]);

  useEffect(() => {
    if (info) {
      message.info(info, 10);
    }
    setInfo('');
  }, [info]);

  const handleRemarks = e => {
    const { value } = e.target;
    setRemarks(value);
  };

  const handleCancel = () => {
    setVisibleApp(false);
    setVisibleRej(false);
    setVisibleRefer(false)
  };

  const setApprove = () => {
    const {
      entity: { additionalData: { data: { deviations = [] } = {} } = {} } = {},
    } = creditPortal;
    // console.log('deviations', deviations);

    if (formValidationErrors) {
      const allFormChecked = FORM_SCREENS.every(key =>
        Object.keys(formValidationErrors).includes(key),
      );
      // debugger;
      if (!allFormChecked) {
        message.info(
          'Please go to all the tabs (all Co-Applicants as well) and check for mandatory fields before Approving!',
        );
        return;
      }
      const noErrorsInAllForms = Object.keys(formValidationErrors).every(
        key => {
          if (key === 'applicantDetails') {
            return Object.values(formValidationErrors[key]).every(
              applicantError => applicantError === true,
            );
          }
          return formValidationErrors[key] === true;
        },
      );
      if (!noErrorsInAllForms) {
        message.info('Please fill all the mandatory fields!');
        return;
      }
    }

    if (!docsData.length) {
      return setInfo('Please review/upload all mandatory documents');
    }
    const mandatoryDocList = [
      'bank statement',
      'cibil bureau report',
      'commercial cibil bureau report',
      'pd questionnaire',
    ];
    const uploadedDocsList = _.map(
      _.uniqBy(
        _.filter(docsData, doc =>
          mandatoryDocList.includes(doc.type.toLowerCase()),
        ),
        'type',
      ),
      item => item.type.toLowerCase(),
      // ),
    );

    const pendingDocs = _.join(
      _.filter(mandatoryDocList, doc => !uploadedDocsList.includes(doc)),
      ', ',
    );
    if (uploadedDocsList.length < 4) {
      return setInfo(`Please upload ${pendingDocs} documents`);
    }

    if (deviations.length === 0) {
      setInfo(
        'Application cannot be approved without at least one deviation, either fill Loan Amount in Loan Details Section or fill Deviation in the Other Details Section!',
      );
      return;
    }
    setVisibleApp(true);
  };

  return (
    <>
      <PageHeader
        className="site-page-header"
        title={
          sessionStorage.getItem('id') ? 'Details Viewed for App Id : ' : ''
        }
        // breadcrumb={{ routes }}
        subTitle={
          sessionStorage.getItem('id')
            ? JSON.parse(sessionStorage.getItem('id'))
            : ''
        }
        extra={
          sessionStorage.getItem('id') && loc.pathname.indexOf('postSanctionpma/') >= 0 ? [
            <Button type="primary" onClick={setApprove}>
              Approve
            </Button>,
            <Button type="primary" onClick={() => setVisibleRej(true)}>
              Reject
            </Button>,
          ] : 
          sessionStorage.getItem('id') &&
          loc.pathname.indexOf('postSanctionclo/') >= 0 &&
          currentApplication &&
          (currentApplication.state === 'REJECTED' ||
            currentApplication.state === 'AUTO_REJECTED') ? [
              <Button type="primary" onClick={() => setVisibleRefer(true)}>
                REFER
              </Button>,
            ] : null
        }
      />
      <PopUp
        handleOk={handleOkApp}
        handleCancel={handleCancel}
        visible={visibleApp}
        value={remarks}
        handleChange={handleRemarks}
        textAreaLabel="Remarks"
        title="Approve Application"
      />
      <PopUp
        handleOk={handleOkRej}
        handleCancel={handleCancel}
        visible={visibleRej}
        value={remarks}
        handleChange={handleRemarks}
        textAreaLabel="Remarks"
        title="Reject Application"
      />
      <PopUp
        handleOk={handleOkRefer}
        handleCancel={handleCancel}
        visible={visibleRefer}
        value={remarks}
        handleChange={handleRemarks}
        textAreaLabel="Remarks"
        title="Refer Application"
      />
    </>
  );
}

PostSanctionHeader.propTypes = {
  dispatch: PropTypes.func.isRequired,
  postSanctionHeader: PropTypes.object,
  creditPortal: PropTypes.object,
  docsData: PropTypes.array,
};

PostSanctionHeader.defaultProps = {
  postSanctionHeader: {},
  creditPortal: {},
};

const withReducer = useInjectReducer({ key: 'postSanctionHeader', reducer });
const withSaga = useInjectSaga({ key: 'postSanctionHeader', saga });

const mapStateToProps = createStructuredSelector({
  postSanctionHeader: makeSelectPostSanctionHeader(),
  creditPortal: makeSelectApplicantDetails(),
  docsData: makeDocsData(),
  applicantDetails: makeSelectApplicantDetails2(),
  applicationsData: makeSelectApplications(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(PostSanctionHeader);
