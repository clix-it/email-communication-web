/*
 *
 * PostSanctionHeader reducer
 *
 */
import produce from 'immer';
import * as actions from './constants';

const initialState = {
  error: {},
  success: {},
};


/* eslint-disable default-case, no-param-reassign */
const postSanctionHeaderReducer = (state = initialState, action) =>
  produce(state, (/* draft */) => {
    switch (action.type) {
      case actions.CLEAR_MESSAGES: {
        return {
          ...state,
          error: {},
          success: {},
          info: {},
        };
      }
      case actions.SET_ERROR: {
        return {
          ...state,
          error: { status: true, message: action.payload.error },
        };
      }
      case actions.SET_SUCCESS: {
        return {
          ...state,
          success: { status: true, message: action.payload.success },
        };
      }
      case actions.SET_INFO: {
        return {
          ...state,
          info: { status: true, message: action.payload.info },
        };
      }
      default:
        return state;
    }
  });

export default postSanctionHeaderReducer;
