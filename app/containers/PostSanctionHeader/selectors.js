import { createSelector } from 'reselect';

const selectPostSanctionHeaderDomain = state => state.postSanctionHeader;

const makeSelectPostSanctionHeader = () =>
  createSelector(
    selectPostSanctionHeaderDomain,
    postSanctionHeader => postSanctionHeader,
  );

export { makeSelectPostSanctionHeader };



