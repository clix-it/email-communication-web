import * as actions from './constants';

export function approvePostSanctionUser(data, applicantDetails) {
  return {
    type: actions.APPROVE_POST_SANCTION_USER,
    payload: {
      data,
      applicantDetails
    },
  };
}

export function referPostSanctionUser(data) {
  return {
    type: actions.REFER_POST_SANCTION_USER,
    payload: {
      data,
    },
  };
}

export function rejectPostSanctionUser(data) {
  return {
    type: actions.REJECT_POST_SANCTION_USER,
    payload: {
      data
    },
  };
}

export function setError(error) {
  return {
    type: actions.SET_ERROR,
    payload: {
      error,
    },
  };
}

export function setInfo(info) {
  return {
    type: actions.SET_INFO,
    payload: {
      info,
    },
  };
}

export function setSuccess(success) {
  return {
    type: actions.SET_SUCCESS,
    payload: {
      success,
    },
  };
}

export function clearMessages() {
  return {
    type: actions.CLEAR_MESSAGES,
  };
}
