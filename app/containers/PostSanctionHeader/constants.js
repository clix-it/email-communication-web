export const APPROVE_POST_SANCTION_USER = 'APPROVE_POST_SANCTION_USER';
export const REJECT_POST_SANCTION_USER = 'REJECT_POST_SANCTION_USER';
export const REFER_POST_SANCTION_USER = 'REFER_POST_SANCTION_USER';
export const SET_ERROR = 'SET_ERROR';
export const SET_SUCCESS = 'SET_SUCCESS';
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';
export const SET_INFO = 'SET_INFO';

export const FORM_SCREENS = [
  'entityDetails',
  'applicantDetails',
  'loanDetails',
  'bankDetails',
  'otherDetails',
];
