/**
 *
 * Asynchronously loads the component for PostSanctionHeader
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
