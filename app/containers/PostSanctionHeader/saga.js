import { takeEvery, put, select, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import { setError } from './actions';
import env from '../../../environment';
import { makeSelectApplications } from '../PostSanction/selectors';

export function* createTransaction(data, selectedApplication, entity) {
  const {
    applicationId,
    partner,
    product,
    activityRequestId,
    source,
    followHierarchy,
  } = selectedApplication;
  try {
    yield call(
      request,
      `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v1/transaction/create`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({
          userActivity: 'SALES_PSV',
          source: 'CREDIT_PSV',
          applicationId,
          product,
          partner,
          activityRequestId,
          ruleDesc: `1-SM`,
          nextActor: entity.appSourcing.salesManager,
          panNumber: selectedApplication.panNumber,
          customerName: selectedApplication.customerName,
          followHierarchy,
        }),
      },
    );
  } catch (err) {
    yield put(setError(`HTTP error occurred.Try Again`));
  }
}

export function* approvePostSanctionUser(action) {
  const { data, applicantDetails } = action.payload;
  const { applications = [] } = yield select(makeSelectApplications());
  const appId = sessionStorage.getItem('id') || '';
  const str = appId.replace(/^"(.*)"$/, '$1');
  if (applications.length === 0 || !appId) {
    yield put(setError('Activity Serial Number Not found! :( '));
    return;
  }
  let selectedApplication = {};
  applications.forEach(application => {
    const { applicationId } = application;
    if (applicationId === str) {
      selectedApplication = application;
    }
  });
  const { entity } = applicantDetails;
  try {
    if (data.status === 'APPROVED') {
      yield call(createTransaction, data, selectedApplication, entity);
      const result = yield fetch(
        `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v1/user/decision`,
        {
          method: 'POST',
          headers: env.headers,
          body: JSON.stringify({
            activitySrlNo: selectedApplication.activitySrlNo,
            employeeId: JSON.parse(sessionStorage.getItem('userId')),
            status: data.status,
            remarks: data.remarks,
            nextApprovalUserId: '',
          }),
        },
      );
      if (result.status === 200) {
        const responseBody = yield result.json();
        if (responseBody.success || responseBody.status) {
          message.info(
            responseBody.message || `User ${data.status} Successfully!`,
          );
          yield put(push(`/${location.pathname.split('/')[1]}`));
        } else {
          message.info(
            responseBody.message || 'Oops! User Could Not be approved :(',
          );
        }
      } else {
        message.error(
          setError(`HTTP Status${result.status} occurred.Try Again`),
        );
      }
    }
  } catch (err) {
    message.error('Network error occurred! Check connection!');
  }
}

export function* rejectPostSanctionUser(action) {
  const { data } = action.payload;
  const { applications = [] } = yield select(makeSelectApplications());
  const appId = sessionStorage.getItem('id') || '';
  const str = appId.replace(/^"(.*)"$/, '$1');
  if (applications.length === 0 || !appId) {
    yield put(setError('Activity Serial Number Not found! :( '));
    return;
  }
  let selectedApplication = {};
  applications.forEach(application => {
    const { applicationId } = application;
    if (applicationId === str) {
      selectedApplication = application;
    }
  });
  try {
    const result = yield fetch(
      `${env.API_URL}/${env.MAKER_CHECKER_API_URL}/v1/user/decision`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({
          activitySrlNo: selectedApplication.activitySrlNo,
          employeeId: JSON.parse(sessionStorage.getItem('userId')),
          status: data.status,
          remarks: data.remarks,
          nextApprovalUserId: '',
        }),
      },
    );
    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.success || responseBody.status) {
        message.info(
          responseBody.message || `User ${data.status} Successfully!`,
        );
        yield put(push(`/${location.pathname.split('/')[1]}`));
      } else {
        message.info(
          responseBody.message || 'Oops! User Could Not be approved :(',
        );
      }
    } else {
      message.error(setError(`HTTP Status${result.status} occurred.Try Again`));
    }
  } catch (err) {
    message.error('Network error occurred! Check connection!');
  }
}

export default function* appSaga() {
  yield takeEvery(actions.APPROVE_POST_SANCTION_USER, approvePostSanctionUser);
  yield takeEvery(actions.REJECT_POST_SANCTION_USER, rejectPostSanctionUser);
}
