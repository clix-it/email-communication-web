import _ from 'lodash';

const regexLevel = /(^l|L){1}([0-9]+)/;

export const checkIfSameLevelRoleAndDevi = (role, deviations) => {
  const sortedDev =
    Object.values(deviations)
      .sort()
      .reverse() || [];
  if (sortedDev.length === 0) return true;
  if (!regexLevel.test(role)) return true;
  if (_.head(role.match(regexLevel)) === sortedDev[0]) return true;
  return false;
};
