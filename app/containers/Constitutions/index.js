/**
 *
 * Constitution
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Spin } from 'antd';

import InputField from 'components/InputField';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectConstitution from './selectors';
import reducer from './reducer';
import saga from './saga';

import { fetchConstitution } from './actions';
export function Constitution({
  cuid,
  existingLAN,
  constitutions,
  clearError,
  setError,
  entityData,
  setValue,
  errors,
  partner,
  product,
  fetchConstitution,
  ...rest
}) {
  useInjectReducer({ key: 'constitutions', reducer });
  useInjectSaga({ key: 'constitutions', saga });

  const { loading, masterConstitutionData = {} } = constitutions;

  const corporateStructure = _.get(entityData, 'corporateStructure');

  useEffect(() => {
    setValue('users[0].corporateStructure', corporateStructure);
    if (corporateStructure) {
      clearError('users[0].corporateStructure');
    } else {
      setError('users[0].corporateStructure');
    }
  }, [entityData]);

  useEffect(() => {
    if (partner && product) {
      fetchConstitution(partner, product);
    }
  }, []);
  return (
    <Spin spinning={loading}>
      <InputField
        errors={errors}
        {...rest}
        showSearch
        // defaultValue={existingLAN}
        type={
          Object.keys(masterConstitutionData) &&
          Object.keys(masterConstitutionData).length
            ? 'select'
            : 'string'
        }
        options={Object.keys(masterConstitutionData)}
        placeholder="Select Constitution"
        labelHtml="Existing Constitution*"
      />
    </Spin>
  );
}

Constitution.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  constitutions: makeSelectConstitution(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchConstitution: (partner, product) =>
      dispatch(fetchConstitution(partner, product)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Constitution);
