/**
 *
 * Asynchronously loads the component for ExistingLans
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
