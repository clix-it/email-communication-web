import { call, put, takeLatest } from 'redux-saga/effects';
import { FETCH_CONSTITUTIONS } from './constants';
import { fetchConstitutionSuccess, fetchConstitutionError } from './actions';
import request from 'utils/request';
import env from '../../../environment';
// Individual exports for testing

export function* fetchConst({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=CONSTITUTION_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchConstitutionSuccess(response.masterData));
    } else {
      yield put(fetchConstitutionError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchConstitutionError('Master Data not found'));
  }
}

export default function* existingLansSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(FETCH_CONSTITUTIONS, fetchConst);
}
