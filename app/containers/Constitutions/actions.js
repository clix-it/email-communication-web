/*
 *
 * ExistingLans actions
 *
 */

import {
  FETCH_CONSTITUTIONS,
  FETCH_CONSTITUTIONS_FETCHED,
  FETCH_CONSTITUTIONS_FAILED,
} from './constants';

export function fetchConstitution(partner, product) {
  return {
    type: FETCH_CONSTITUTIONS,
    partner,
    product,
  };
}

export function fetchConstitutionSuccess(data) {
  return {
    type: FETCH_CONSTITUTIONS_FETCHED,
    data,
  };
}

export function fetchConstitutionError() {
  return {
    type: FETCH_CONSTITUTIONS_FAILED,
  };
}
