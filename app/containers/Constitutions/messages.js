/*
 * ExistingLans Messages
 *
 * This contains all the text for the ExistingLans container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ExistingLans';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ExistingLans container!',
  },
});
