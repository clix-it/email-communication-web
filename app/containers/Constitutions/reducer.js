/*
 *
 * ExistingLans reducer
 *
 */
import produce from 'immer';
import _ from 'lodash';
import {
  FETCH_CONSTITUTIONS,
  FETCH_CONSTITUTIONS_FETCHED,
  FETCH_CONSTITUTIONS_FAILED,
} from './constants';
export const initialState = {
  cuid: false,
  masterConstitutionData: {},
  response: [
    'Proprietorship',
    'Partnership firm',
    'Private Limited Co',
    'Limited Liability Partnership',
    'Limited Company',
    'Trust',
    'Society',
    'HINDU UNDEVIDED FAMILY',
    'INDIVIDUAL',
    'PUBLIC LIMITED COMPANY',
    // 'Others',
  ],
  loading: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const existingLansReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_CONSTITUTIONS:
        draft.loading = true;
        draft.cuid = action.cuid;
        break;
      case FETCH_CONSTITUTIONS_FETCHED:
        draft.loading = false;
        let tempObj2 = {};
        action.data.forEach(item => {
          tempObj2[item.cmValue] = item.cmCode;
        });
        draft.masterConstitutionData = tempObj2;
        break;
      case FETCH_CONSTITUTIONS_FAILED:
        draft.loading = false;
        draft.masterConstitutionData = {};
        break;
    }
  });

export default existingLansReducer;
