import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the constitutions state domain
 */

const selectConstitutionsDomain = state => state.constitutions || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by constitutions
 */

const makeSelectConstitution = () =>
  createSelector(
    selectConstitutionsDomain,
    substate => substate,
  );

export default makeSelectConstitution;
export { selectConstitutionsDomain };
