/*
 *
 * ExistingLans constants
 *
 */

export const FETCH_CONSTITUTIONS = 'app/ExistingLans/FETCH_CONSTITUTIONS';
export const FETCH_CONSTITUTIONS_FETCHED =
  'app/ExistingLans/FETCH_CONSTITUTIONS_FETCHED';
export const FETCH_CONSTITUTIONS_FAILED =
  'app/ExistingLans/FETCH_CONSTITUTIONS_FAILED';
