import _ from 'lodash';
import {
  take,
  call,
  put,
  select,
  takeEvery,
  debounce,
} from 'redux-saga/effects';
import request from 'utils/request';
// import { banksFetched, banksFetchingError } from './actions';
// import { FETCH_BANKS } from './constants';
import {
  UPDATE_EMAIL_DETAILS,
} from './constants';
import {
  updateEmailDetailsSuccess,
  updateEmailDetailsError,
} from './actions';
import env from '../../../environment';

export default function* updateEmailDetailsSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(UPDATE_EMAIL_DETAILS, updateEmail);
}

function* updateEmail({ payload }) {

//console.log('Payload', payload)
  const payloadData = {
    emailId: payload.emailId,
    category: payload.category,
    defaultMailId: 'N',
    remarks: payload.remarks,
    active: payload.active,
    createdBy: JSON.parse(sessionStorage.getItem('userId')),
  };

  const reqUrl = `${env.EMAIL_COMMUNICATION_URL}`;
  const options = {
    method: 'POST',
    body: JSON.stringify(payloadData),
    headers: env.headers,
  };

  try {
    const appRes = yield call(request, reqUrl, options);
    if (appRes.success) {
      yield put(updateEmailDetailsSuccess({message : _.get(appRes, 'body.message') || 'Emails saved successfully!!'}));
    }
  } catch (error) {
    console.error(error);
  }
}
