/*
 *
 * UpdateEmails constants
 *
 */

export const DEFAULT_ACTION = 'app/EmailDetails/DEFAULT_ACTION';
export const UPDATE_EMAIL_DETAILS = 'app/EmailDetails/UPDATE_EMAIL_DETAILS';
export const UPDATE_EMAIL_DETAILS_SUCCESS =
  'app/EmailDetails/UPDATE_EMAIL_DETAILS_SUCCESS';
export const UPDATE_EMAIL_DETAILS_ERROR =
  'app/EmailDetails/UPDATE_EMAIL_DETAILS_ERROR';
