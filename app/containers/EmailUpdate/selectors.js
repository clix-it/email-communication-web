import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the bankDetails state domain
 */

const selectUpdateEmailDetailsDomain = state => state.updateEmailDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by BankDetails
 */

const makeSelectUpdateEmailDetails = () =>
  createSelector(
    selectUpdateEmailDetailsDomain,
    substate => substate,
  );

export default makeSelectUpdateEmailDetails;
export { selectUpdateEmailDetailsDomain };
