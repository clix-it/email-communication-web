/*
 *
 * UpdateEmails reducer
 *
 */
import produce from 'immer';
import {
  DEFAULT_ACTION,
  UPDATE_EMAIL_DETAILS,
  UPDATE_EMAIL_DETAILS_ERROR,
  UPDATE_EMAIL_DETAILS_SUCCESS,
} from './constants';

export const initialState = {
  response: false,
  error: false,
  payload: false,
};

/* eslint-disable default-case, no-param-reassign */
const emailUpdateReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case DEFAULT_ACTION:
        break;
      case UPDATE_EMAIL_DETAILS:
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case UPDATE_EMAIL_DETAILS_SUCCESS:
        draft.error = false;
        draft.response = action.data.message;
        break;
      case UPDATE_EMAIL_DETAILS_ERROR:
        draft.response = false;
        draft.error = `Data not saved`;
        break;
     }
  });

export default emailUpdateReducer;
