/*
 *
 * UpdateEmails actions
 *
 */

import {
  DEFAULT_ACTION,
  UPDATE_EMAIL_DETAILS,
  UPDATE_EMAIL_DETAILS_ERROR,
  UPDATE_EMAIL_DETAILS_SUCCESS,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function editEmailDetails(payload) {
  return {
    type: UPDATE_EMAIL_DETAILS,
    payload,
  };
}

export function updateEmailDetailsSuccess(data) {
  return {
    type: UPDATE_EMAIL_DETAILS_SUCCESS,
    data,
  };
}

export function updateEmailDetailsError(err) {
  return {
    type: UPDATE_EMAIL_DETAILS_ERROR,
    data: err,
  };
}
