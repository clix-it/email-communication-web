/**
 *
 * EmailUpdate
 *
 */
import _ from 'lodash';
import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { useForm, Controller } from 'react-hook-form';
import { Form, Row, Col, Button, Radio, message, Divider } from 'antd';
import makeSelectUpdateEmailDetails from './selectors';
import reducer from './reducer';
import saga from './saga';
import { emailPattern } from '../../containers/App/constants';
import { EMAIL_CATEGORY, DEFAULT_MAIL_ID } from '../../utils/constants';
import { useParams } from 'react-router-dom';
import InputField from '../../components/InputField';
import { makeSelectApplications } from '../EmailPortal/selectors';
import {
  editEmailDetails,
} from './actions';
import {
  setFormValidationError,
  isDirtyForm,
  loadEntityDetails,
} from '../ApplicantDetails/actions';
import { useHistory } from 'react-router-dom';
const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: { span: 10, offset: 2, pull: 2 },
};
export function UpdateEmailDetails({
  dispatch,
  activity,
  emailPortal,
  updateEmailDetails,
}) {
  useInjectReducer({ key: 'updateEmailDetails', reducer });
  useInjectSaga({ key: 'updateEmailDetails', saga });

  const emailcommData = emailPortal.emailCommunications.map(item => ({
    id: item.id,
    emailId: item.emailId,
    remarks: item.remarks,
    active: item.active,
    defaultMailId: item.defaultMailId,
    category: item.category,
  }));

  const newEmailDict = {};

  for (let i = 0; i < emailcommData.length; i++) {
    newEmailDict[emailcommData[i].id] = emailcommData[i];
  }

  let { appIdEmail } = useParams();

  // useEffect(() => {
  //   console.log('Emails', creditPortal.emailCommunications);
  //   console.log('Email Updates', emailcommData);
  //   console.log('Dict', newEmailDict);
  // }, []);

  const {
    register,
    handleSubmit,
    getValues,
    errors,
    control,
    watch,
    setError,
    reset,
    clearError,
    formState,
    setValue,
    triggerValidation,
  } = useForm({
    mode: 'onChange',

    defaultValues: {
      emailId: newEmailDict[appIdEmail].emailId,
      category: newEmailDict[appIdEmail].category,
      defaultMailId: newEmailDict[appIdEmail].defaultMailId,
      remarks: newEmailDict[appIdEmail].remarks,
      active: newEmailDict[appIdEmail].active,
    },
  });

  const { dirtyFields, dirtyFieldsSinceLastSubmit } = formState;
  console.log(dirtyFields, 'dirtyFields');
  useEffect(() => {
    dispatch(isDirtyForm({ 5: formState.dirty }));
    console.log(formState.dirty, 'dirty bank', formState.dirtySinceLastSubmit);
  }, [formState.dirty]);

  const values = watch();
  console.log(values, 'values');

  console.log(formState.dirty, 'dirty val');

  const [response, setResponse] = useState(false);

  useEffect(() => {
    // if (Object.keys(errors).length > 0) {
    //   document.getElementById(Object.keys(errors)[0]).focus();
    // }
  }, [errors]);

  useEffect(() => {
    if (updateEmailDetails.response && response) {
      message.success(updateEmailDetails.response);
      dispatch(loadEntityDetails());
      setResponse(false);
    }

    if (updateEmailDetails.error && response) {
      message.error(updateEmailDetails.error);
    }
  }, [updateEmailDetails.response, updateEmailDetails.error]);

  const onSubmit = data => {
    dispatch(editEmailDetails(data));
    setResponse(true);
    setTimeout(addEmail, 3000);
  };

  const history = useHistory();
  const addEmail = () => {
    history.push(`/emailDetails`);
  };

  return (
    <div className="App">
      <h4>Email Details</h4>
      <Divider />
      <form onSubmit={handleSubmit(onSubmit)}>
        <Row gutter={[16, 16]}>
          {/* start */}
          <Col md={24} />
          <Col {...responsiveColumns}>
            <InputField
              id="emailId"
              control={control}
              name="emailId"
              type="string"
              values={values}
              disabled
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
                pattern: emailPattern,
              }}
              errors={errors}
              placeholder="Email"
              labelHtml=" Email ID*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="category"
              control={control}
              name="category"
              type="select"
              values={values}
              disabled
              options={EMAIL_CATEGORY}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Category"
              labelHtml="Category*"
            />
          </Col>
          {/* <Col {...responsiveColumns}>
            <InputField
              id="defaultMailId"
              control={control}
              name="defaultMailId"
              type="select"
              values={values}
              options={DEFAULT_MAIL_ID}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Default Mail ID"
              labelHtml="Default Mail ID*"
            />
          </Col> */}
          <Col {...responsiveColumns}>
            <InputField
              id="remarks"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              name="remarks"
              type="string"
              values={values}
              errors={errors}
              placeholder="Reamrks"
              labelHtml="Remarks*"
            />
          </Col>
          <Col {...responsiveColumns}>
            <InputField
              id="active"
              control={control}
              name="active"
              type="select"
              values={values}
              options={DEFAULT_MAIL_ID}
              rules={{
                required: {
                  value: true,
                  message: 'This field cannot be left empty',
                },
              }}
              isAddressSelect
              errors={errors}
              placeholder="Active"
              labelHtml="Active*"
            />
          </Col>
        </Row>
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            disabled={activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'}
          >
            Save
          </Button>
          {'   '}
          <Button
            type="primary"
            onClick={addEmail}
            //  disabled={activity === 'CREDIT_FCU' || activity === 'HUNTER_REVIEW'}
          >
            Cancel
          </Button>
        </Form.Item>
      </form>
    </div>
  );
}

UpdateEmailDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  updateEmailDetails: makeSelectUpdateEmailDetails(),
  emailPortal: makeSelectApplications(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(UpdateEmailDetails);
