/*
 *
 * LeadDocuments reducer
 *
 */
import produce from 'immer';
import * as actions from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';
import moment from 'moment';

export const initialState = {
  loading: false,
  error: false,
  isFileUploading: false,
  isFileUploaded: false,
  FileUploadError: '',
  docsData: [],
  dmsDocsLoading: false,
  appDocsLoading: false,
  dmsV1Docs: [],
  appV1Docs: [],
  masterOtherDocsData: {},
  acceptanceLetterDocs: [],
  allDocs: [],
};

const leadDocumentsReducer = (state = initialState, action) =>
  /* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["draft"] }] */
  produce(state, draft => {
    switch (action.type) {
      case actions.FETCH_LEADSDOC_REQUEST:
        draft.loading = true;
        draft.isFileUploading = false;
        draft.isFileUploaded = false;
        draft.FileUploadError = '';
        break;
      case actions.FETCH_LEADSDOC_SUCCESS:
        draft.loading = false;
        draft.docsData = action.data.sort((a, b) => {
          return moment.utc(b.date).diff(moment.utc(a.date));
        });
        draft.isFileUploading = false;
        draft.isFileUploaded = false;
        draft.FileUploadError = '';
        break;
      case actions.FETCH_LEADSDOC_FAIL:
        draft.loading = false;
        draft.docsData = [];
        draft.isFileUploading = false;
        draft.isFileUploaded = false;
        draft.FileUploadError = '';
        break;
      case actions.UPLOAD_LEADSDOC_REQUEST:
        draft.isFileUploading = true;
        draft.isFileUploaded = false;
        draft.FileUploadError = '';
        break;
      case actions.UPLOAD_LEADSDOC_SUCCESS:
        draft.isFileUploading = false;
        draft.isFileUploaded = true;
        draft.FileUploadError = '';
        break;
      case actions.UPLOAD_LEADSDOC_FAIL:
        draft.isFileUploading = false;
        draft.isFileUploaded = false;
        draft.FileUploadError = action.payload.error;
        break;
      case LOCATION_CHANGE:
        draft.docsData = [];
        draft.allDocs = [];
        draft.isFileUploaded = false;
        draft.appV1Docs = [];
        draft.acceptanceLetterDocs = [];
        break;
      case actions.FETCH_DMS_V1_DOCS:
        draft.dmsDocsLoading = true;
        draft.isFileUploaded = false;
        break;
      case actions.FETCH_DMS_V1_DOCS_SUCCESS:
        draft.dmsDocsLoading = false;
        draft.dmsV1Docs = action.response;
        draft.isFileUploaded = false;
        break;
      case actions.FETCH_DMS_V1_DOCS_ERROR:
        draft.dmsDocsLoading = false;
        break;
      case actions.FETCH_APPDOCUMENTS_V1:
        draft.appDocsLoading = true;
        draft.isFileUploaded = false;
        break;
      case actions.FETCH_APPDOCUMENTS_V1_SUCCESS:
        draft.appDocsLoading = false;
        draft.appV1Docs = action.response;
        draft.isFileUploaded = false;
        break;
      case actions.FETCH_APPDOCUMENTS_V1_ERROR:
        draft.appDocsLoading = false;
        break;
      case actions.FETCH_OTHER_DOCS_SUCCESS:
        let tempObj2 = {};
        action.data.forEach(item => {
          tempObj2[item.cmValue] = item.cmCode;
        });
        draft.masterOtherDocsData = tempObj2;
        break;
      case actions.FETCH_OTHER_DOCS_ERROR:
        draft.masterOtherDocsData = {};
        break;
      case actions.FETCH_ACCEPTANCE_LETTER:
        draft.loading = true;
        //draft.acceptanceLetterDocs = [];
        break;
      case actions.FETCH_ACCEPTANCE_LETTER_SUCCESS:
        draft.loading = false;
        draft.acceptanceLetterDocs = action.data;
        break;
      case actions.FETCH_ACCEPTANCE_LETTER_ERROR:
        draft.loading = false;
        draft.acceptanceLetterDocs = [];
        break;
      case actions.SET_ALL_DOCS:
        draft.allDocs = action.downloadedDocs;
        break;
      default: {
        return state;
      }
    }
  });

export default leadDocumentsReducer;
