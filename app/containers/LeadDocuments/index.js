/**
 *
 * LeadDocuments
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import {
  makeDocsData,
  makeLoading,
  makeDMSLoading,
  makeError,
  makeFileUploaded,
  makeFileUploadError,
  makeFileUploading,
  makeSelectDMSDocs,
  makeSelectAppDocs,
  makeSelectOtherDocs,
  makeSelectAcceptanceLetter,
  makeSelectAllDocs,
} from './selectors';

import LeadDocsList from '../../components/LeadDocsList';
import { makeSelectKycDetails } from '../KycDetails/selectors';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';

export function LeadDocuments({
  docsData,
  dispatch,
  loading,
  error,
  appId,
  appDocumentDetails,
  isFileUploaded,
  isFileUploading,
  FileUploadError,
  dmsV1Docs,
  dmsDocsLoading,
  applicantDetails,
  appV1Docs,
  kycDetails,
  otherDocs,
  activityType,
  requestType,
  acceptanceLetterDocs,
  allDocs,
}) {
  const { navigateToDocumentTab = false } = kycDetails || {
    navigateToDocumentTab: false,
  };
  return (
    <LeadDocsList
      docsData={docsData}
      dispatch={dispatch}
      isLoading={loading}
      error={error}
      appDocumentDetails={appDocumentDetails}
      appId={appId}
      isFileUploaded={isFileUploaded}
      isFileUploading={isFileUploading}
      FileUploadError={FileUploadError}
      dmsV1Docs={dmsV1Docs}
      dmsDocsLoading={dmsDocsLoading}
      applicantDetails={applicantDetails}
      appV1Docs={appV1Docs}
      navigateToDocumentTab={navigateToDocumentTab}
      otherDocs={otherDocs}
      activityType={activityType}
      requestType={requestType}
      acceptanceLetterDocs={acceptanceLetterDocs}
      allDocs={allDocs}
    />
  );
}

LeadDocuments.propTypes = {
  dispatch: PropTypes.func.isRequired,
  docsData: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.object,
  isFileUploaded: PropTypes.bool,
  isFileUploading: PropTypes.bool,
  FileUploadError: PropTypes.string,
  appId: PropTypes.string,
  appDocumentDetails: PropTypes.array,
};

const mapStateToProps = createStructuredSelector({
  docsData: makeDocsData(),
  loading: makeLoading(),
  dmsDocsLoading: makeDMSLoading(),
  error: makeError(),
  isFileUploaded: makeFileUploaded(),
  FileUploadError: makeFileUploadError(),
  isFileUploading: makeFileUploading(),
  dmsV1Docs: makeSelectDMSDocs(),
  appV1Docs: makeSelectAppDocs(),
  kycDetails: makeSelectKycDetails(),
  otherDocs: makeSelectOtherDocs(),
  acceptanceLetterDocs: makeSelectAcceptanceLetter(),
  allDocs: makeSelectAllDocs(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = useInjectReducer({ key: 'leadDocuments', reducer });
const withSaga = useInjectSaga({ key: 'leadDocuments', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(LeadDocuments);
