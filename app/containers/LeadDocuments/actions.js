/*
 *
 * LeadDocuments actions
 *
 */

import {
  FETCH_LEADSDOC_REQUEST,
  FETCH_LEADSDOC_SUCCESS,
  FETCH_LEADSDOC_FAIL,
  UPLOAD_LEADSDOC_REQUEST,
  UPLOAD_LEADSDOC_SUCCESS,
  UPLOAD_LEADSDOC_FAIL,
  FETCH_DMS_V1_DOCS,
  FETCH_DMS_V1_DOCS_SUCCESS,
  FETCH_DMS_V1_DOCS_ERROR,
  FETCH_APPDOCUMENTS_V1,
  FETCH_APPDOCUMENTS_V1_SUCCESS,
  FETCH_APPDOCUMENTS_V1_ERROR,
  FETCH_OTHER_DOCS,
  FETCH_OTHER_DOCS_SUCCESS,
  FETCH_OTHER_DOCS_ERROR,
  DELETE_FILE,
  DELETE_FILE_FAIL,
  DELETE_FILE_SUCCESS,
  FETCH_ACCEPTANCE_LETTER,
  FETCH_ACCEPTANCE_LETTER_SUCCESS,
  FETCH_ACCEPTANCE_LETTER_ERROR,
  SET_ALL_DOCS,
} from './constants';

export function fetchleadsDoc(appDocumentDetails) {
  return {
    type: FETCH_LEADSDOC_REQUEST,
    payload: {
      appDocumentDetails,
    },
  };
}

export function fetchleadsDocSuccess(docsData) {
  return {
    type: FETCH_LEADSDOC_SUCCESS,
    data: docsData,
  };
}

export function fetchleadsDocFail() {
  return {
    type: FETCH_LEADSDOC_FAIL,
  };
}

export function uploadleadsDocRequest(
  file,
  type,
  id,
  docType,
  binaryData,
  filestoBeSend,
  otherDocumentName,
) {
  return {
    type: UPLOAD_LEADSDOC_REQUEST,
    payload: {
      file,
      type,
      id,
      docType,
      binaryData,
      filestoBeSend,
      otherDocumentName,
    },
  };
}

export function uploadleadsDocSuccess() {
  return {
    type: UPLOAD_LEADSDOC_SUCCESS,
  };
}

export function uploadleadsDocFail(error) {
  return {
    type: UPLOAD_LEADSDOC_FAIL,
    payload: {
      error,
    },
  };
}
export function fetchDMSV1Docs(payload) {
  return {
    type: FETCH_DMS_V1_DOCS,
    payload,
  };
}

export function fetchDMSV1DocsSuccess(response) {
  return {
    type: FETCH_DMS_V1_DOCS_SUCCESS,
    response,
  };
}

export function fetchDMSV1DocsError(error) {
  return {
    type: FETCH_DMS_V1_DOCS_ERROR,
    error,
  };
}

export function fetchAppDocumentsV1(appDocuments) {
  return {
    type: FETCH_APPDOCUMENTS_V1,
    appDocuments,
  };
}

export function fetchAppDocumentsV1Success(response) {
  return {
    type: FETCH_APPDOCUMENTS_V1_SUCCESS,
    response,
  };
}

export function fetchAppDocumentsV1Error(error) {
  return {
    type: FETCH_APPDOCUMENTS_V1_ERROR,
    error,
  };
}

export function fetchOtherDocs(partner, product) {
  // ;
  return {
    type: FETCH_OTHER_DOCS,
    partner,
    product,
  };
}

export function fetchOtherDocsSuccess(data) {
  // ;
  return {
    type: FETCH_OTHER_DOCS_SUCCESS,
    data,
  };
}

export function fetchOtherDocsError(error) {
  // ;
  return {
    type: FETCH_OTHER_DOCS_ERROR,
    error,
  };
}

export function deleteFile(data) {
  return {
    type: DELETE_FILE,
    data,
  };
}

export function deleteFileSuccess(message) {
  return {
    type: DELETE_FILE_SUCCESS,
    message,
  };
}

export function deleteFileFail(message) {
  return {
    type: DELETE_FILE_FAIL,
    message,
  };
}

export function fetchAcceptanceLetter(appId) {
  return {
    type: FETCH_ACCEPTANCE_LETTER,
    appId,
  };
}

export function fetchAcceptanceLetterSuccess(data) {
  return {
    type: FETCH_ACCEPTANCE_LETTER_SUCCESS,
    data,
  };
}

export function fetchAcceptanceLetterError(error) {
  return {
    type: FETCH_ACCEPTANCE_LETTER_ERROR,
    error,
  };
}

export function setAllDocs(downloadedDocs) {
  return {
    type: SET_ALL_DOCS,
    downloadedDocs,
  }
}
