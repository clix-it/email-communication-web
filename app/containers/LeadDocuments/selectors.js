import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the leadDocuments state domain
 */

const selectLeadDocumentsDomain = state => state.leadDocuments || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by LeadDocuments
 */

const makeDocsData = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.docsData,
  );

const makeLoading = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.loading,
  );

const makeDMSLoading = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.dmsDocsLoading,
  );

const makeError = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.error,
  );

const makeFileUploaded = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.isFileUploaded,
  );

const makeFileUploadError = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.FileUploadError,
  );

const makeFileUploading = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.isFileUploading,
  );
const makeSelectDMSDocs = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.dmsV1Docs,
  );

const makeSelectAppDocs = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.appV1Docs,
  );

const makeSelectOtherDocs = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.masterOtherDocsData,
  );

const makeSelectAcceptanceLetter = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.acceptanceLetterDocs,
  );

const makeSelectAllDocs = () =>
  createSelector(
    selectLeadDocumentsDomain,
    substate => substate.allDocs,
  );

export {
  selectLeadDocumentsDomain,
  makeDocsData,
  makeLoading,
  makeDMSLoading,
  makeError,
  makeFileUploaded,
  makeFileUploadError,
  makeFileUploading,
  makeSelectDMSDocs,
  makeSelectAppDocs,
  makeSelectOtherDocs,
  makeSelectAcceptanceLetter,
  makeSelectAllDocs,
};
