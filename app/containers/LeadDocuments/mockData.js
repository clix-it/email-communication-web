const data = {
  docType: [
    {
      name: 'loan agreement',
      objType: 'app',
    },
    {
      name: 'Application Docket',
      objType: 'app',
    },
    {
      name: 'Application Docket (Rectified)',
      objType: 'app',
    },
    {
      name: 'Approval Email',
      objType: 'app',
    },
    {
      name: 'Posidex output',
      objType: 'user',
    },
    {
      name: 'caf',
      objType: 'app',
    },
    {
      name: 'soc',
      objType: 'app',
    },
    {
      name: 'noc',
      objType: 'app',
    },
    {
      name: 'welcome letter',
      objType: 'app',
    },
    {
      name: 'foreclosure letter',
      objType: 'app',
    },
    {
      name: 'soa',
      objType: 'app',
    },
    {
      name: 'rps',
      objType: 'app',
    },
    {
      name: 'delivery order',
      objType: 'app',
    },
    {
      name: 'insurance form',
      objType: 'app',
    },
    {
      name: 'vehicle registration certificate',
      objType: 'app',
    },
    {
      name: 'undertaking docket',
      objType: 'app',
    },
    {
      name: 'fi report',
      objType: 'app',
    },
    {
      name: 'fcu report',
      objType: 'app',
    },
    {
      name: 'property',
      objType: 'app',
    },
    {
      name: 'nach',
      objType: 'app',
    },
    {
      name: 'pdc',
      objType: 'app',
    },
    {
      name: 'drd',
      objType: 'app',
    },
    {
      name: 'passport',
      objType: 'user',
    },
    {
      name: 'pan',
      objType: 'user',
    },
    {
      name: 'aadhaar',
      objType: 'user',
    },
    {
      name: 'driving license',
      objType: 'user',
    },
    {
      name: 'voter id card',
      objType: 'user',
    },
    {
      name: 'nrega job card',
      objType: 'user',
    },
    {
      name: 'utility bill',
      objType: 'user',
    },
    {
      name: 'bank statement',
      objType: 'user',
    },
    {
      name: 'rent agreement',
      objType: 'user',
    },
    {
      name: 'experian bureau report',
      objType: 'user',
    },
    {
      name: 'cibil bureau report',
      objType: 'user',
    },
    {
      name: 'crif bureau report',
      objType: 'user',
    },
    {
      name: 'equifax bureau report',
      objType: 'user',
    },
    {
      name: 'ckyc xml',
      objType: 'user',
    },
    {
      name: 'okyc xml',
      objType: 'user',
    },
    {
      name: 'aadhaar front',
      objType: 'user',
    },
    {
      name: 'aadhaar back',
      objType: 'user',
    },
    {
      name: 'driving license front',
      objType: 'user',
    },
    {
      name: 'driving license back',
      objType: 'user',
    },
    {
      name: 'voter id card front',
      objType: 'user',
    },
    {
      name: 'voter id card back',
      objType: 'user',
    },
    {
      name: 'passport front',
      objType: 'user',
    },
    {
      name: 'passport back',
      objType: 'user',
    },
    {
      name: 'photo',
      objType: 'user',
    },
    {
      name: 'address proof',
      objType: 'user',
    },
    {
      name: 'constitution of entity',
      objType: 'user',
    },
    {
      name: 'commercial cibil bureau report',
      objType: 'user',
    },
    {
      name: 'pd questionnaire',
      objType: 'user',
    },
    {
      name: 'student data',
      objType: 'user',
    },
    {
      name: 'ca certificate',
      objType: 'user',
    },
    {
      name: 'audit report',
      objType: 'user',
    },
    {
      name: 'valuation report',
      objType: 'user',
    },
    {
      name: 'legal report',
      objType: 'user',
    },
    {
      name: 'GST',
      objType: 'user',
    },
    {
      name: 'Combined KYC documents',
      objType: 'user',
    },
    {
      name: 'Application form',
      objType: 'user',
    },
    {
      name: 'Electricity Bill',
      objType: 'user',
    },
    {
      name: 'Ownership proof',
      objType: 'user',
    },
    {
      name: 'Others',
      objType: 'app',
    },
  ],
};

export default data;
