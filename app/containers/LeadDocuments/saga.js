/**
 * Gets the leads documents data
 */

import _ from 'lodash';
import { put, takeLatest, all, takeEvery, call } from 'redux-saga/effects';
import request from 'utils/request';
import { message } from 'antd';

import * as actions from './constants';
import {
  fetchleadsDocSuccess,
  fetchleadsDocFail,
  uploadleadsDocSuccess,
  uploadleadsDocFail,
  fetchDMSV1DocsSuccess,
  fetchDMSV1DocsError,
  fetchAppDocumentsV1Success,
  fetchAppDocumentsV1Error,
  fetchOtherDocsSuccess,
  fetchOtherDocsError,
  deleteFileSuccess,
  deleteFileFail,
  fetchAcceptanceLetterSuccess,
  fetchAcceptanceLetterError,
} from './actions';
import env from '../../../environment';
// import data from './mockData';

/**
 * Github repos request/response handler
 */
export function* getDocs(action) {
  const result = [];
  if (action.payload.appDocumentDetails.length > 0) {
    yield all(
      action.payload.appDocumentDetails.map(function*(item) {        
        const body = {
          objId: Object.keys(item).indexOf('appId') >= 0 ? item.appId : item.cuid,
          objType: Object.keys(item).indexOf('appId') >= 0 ? 'app' : 'user',
          docType: item.docType || '',
        };
        yield* downloadApi(body, result);
      }),
    );
  }
  const docsData = result;
  if(docsData.length > 0){
    yield put(fetchleadsDocSuccess(docsData));
  } else {
    yield put(fetchleadsDocFail());
  }  
}

export function* downloadApi(body, result) {
  const apiresult = yield fetch(`${env.API_URL}/app-docs/v2/download`, {
    headers: env.headers,
    method: 'POST',
    body: JSON.stringify(body),
  });
  const responseBody = yield apiresult.json();
  if (responseBody.status === true) {
    responseBody.data.forEach(obj => {
      if (obj.asset.filename.split('.')[1] !== undefined) {
        result.push({
          obj_type: obj.asset.obj_type,
          obj_id: obj.asset.obj_id,
          type: obj.asset.type,
          filename: obj.asset.filename,
          signedUrl: obj.signed_url.url,
          fileextension: obj.asset.filename.split('.')[1].toLowerCase(),
          fileAttrs: obj.asset.attrs,
          date: obj.asset.updated_at,
        });
      }
    });
  }
}

export function* deleteFile(action) {
  const reqUrl = `${env.API_URL}/app-docs/v2/upload`;
  const options = {
    method: 'POST',
    body: JSON.stringify({
      objId: String(action.data.obj_id),
      objType: action.data.obj_type,
      docType: action.data.type,
      fileName:
        action.data.filename && action.data.filename.split('/').length > 0
          ? action.data.filename.split('/').pop()
          : action.data.filename,
      fileAttributes: {
        ...action.data.fileAttrs,
        isDocumentActive: false,
      },
      files: [],
    }),
    headers: env.headers,
  };
  try {
    const response = yield call(request, reqUrl, options);
    if (response.status) {
      message.info('File deleted Successfully!');
      yield put(deleteFileSuccess('File deleted Successfully'));
    } else {
      message.info('File could not be deleted!');
      yield put(deleteFileFail('File could not be deleted!'));
    }
  } catch (err) {
    message.info('File could not be deleted!');
    yield put(deleteFileFail('File could not be deleted!'));
  }
}

export function* fileUpload({ data }) {
  const reqUrl = `${env.API_URL}/app-docs/v2/upload`;
  const {
    fileAttributes,
    objId,
    objType,
    docType,
    fileName,
    filestoBeSend,
  } = data;
  const fInalData = {
    objId,
    objType,
    docType,
    fileName,
    fileAttributes,
    files: [],
  };
  const options = {
    method: 'POST',
    body: JSON.stringify(fInalData),
    headers: env.headers,
  };
  try {
    const response = yield call(request, reqUrl, options);

    if (response.status) {
      const optionsNew = {
        method: 'PUT',
        body: filestoBeSend,
      };
      const doNotCopyHeader = true;
      const response2 = yield fetch(response.data.presignedUrl, {
        ...optionsNew,
        doNotCopyHeader,
      });
      if (response2.status == 200) {
        yield put(uploadleadsDocSuccess());
      } else {
        yield put(uploadleadsDocFail('Doc upload failed'));
      }
    } else {
      yield put(uploadleadsDocFail('Doc upload failed 1'));
    }
  } catch (err) {
    yield put(uploadleadsDocFail('Doc upload failed 3'));
  }
}

export function* uploadDoc(action) {
  const { pdfFile, callFrom, filestoBeSend } = action.payload;
  const file =
    callFrom === 'entityDetails'
      ? [pdfFile]
      : [action.payload.binaryData.split(',')[1]];
  const body = {
    objId: action.payload.id,
    objType: action.payload.type,
    docType: action.payload.docType,
    fileName: action.payload.file.name,
    fileAttributes: {
      isPasswordProtected: false,
      filePassword: '',
      isDocumentActive: true,
    },
    files: file,
  };
  if (action.payload.otherDocumentName) {
    body.fileAttributes.docName = action.payload.otherDocumentName;
  }
  if (true) {
    yield call(fileUpload, { data: { ...body, filestoBeSend } });
    return;
  }
  try {
    const result = yield fetch(`${env.API_URL}/app-docs/v2/upload`, {
      headers: env.headers,
      method: 'POST',
      body: JSON.stringify(body),
    });
    if (callFrom === 'entityDetails') return result;

    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.success || responseBody.status) {
        yield put(uploadleadsDocSuccess());
      }
    } else {
      const errorBody = yield result.json();
      yield put(uploadleadsDocFail(errorBody.message));
    }
  } catch (err) {
    yield put(uploadleadsDocFail(err));
  }
  return {};
}
export function* fetchDMSV1DocsList({ payload }) {
  const reqUrl = `${env.API_URL}/kyc-lead/v1/search`;
  const data = {
    // id: '50010054',
    id: payload,
    isKycDocsRequired: true,
    isAppDocsRequired: true,
  };
  const options = {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify(data),
  };
  try {
    const response = yield call(request, reqUrl, options);
    let responeBodiesKycDocs = [];
    let responeBodiesAppDocs = [];
    if (response.status && response.data.length > 0) {
      if (response.data[0].kycDocs && response.data[0].kycDocs.length > 0) {
        // GET FILESTREAM FROM APP_DOCS v1 api
        const responses = response.data[0].kycDocs
          .filter(data => data.docId != null)
          .map(item => {
            const promise = request(
              `${env.API_URL}/app-docs/v1/download/${item.docId}`,
              {
                method: 'GET',
                headers: env.headers,
              },
            );
            return promise;
          });
        responeBodiesKycDocs = yield Promise.all(responses);
      }
      if (response.data[0].appDocs && response.data[0].appDocs.length > 0) {
        // GET FILESTREAM FROM APP_DOCS v1 api
        const responses = response.data[0].appDocs
          .filter(data => data.docId != null)
          .map(item => {
            const promise = request(
              `${env.API_URL}/app-docs/v1/download/${item.docId}`,
              {
                method: 'GET',
                headers: env.headers,
              },
            );
            return promise;
          });
        responeBodiesAppDocs = yield Promise.all(responses);
      }
      if (responeBodiesKycDocs.length > 0 || responeBodiesAppDocs.length > 0) {
        const concatedArray = responeBodiesKycDocs.concat(responeBodiesAppDocs);
        yield put(fetchDMSV1DocsSuccess(concatedArray));
      } else {
        yield put(fetchDMSV1DocsError(response));
      }
    } else {
      yield put(fetchDMSV1DocsError(response));
    }
  } catch (error) {
    yield put(fetchDMSV1DocsError(error));
  }
}

export function* fetchAppDocumentsV1({ appDocuments }) {
  try {
    if (appDocuments && appDocuments.length > 0) {
      // GET FILESTREAM FROM APP_DOCS v1 api
      const responses = appDocuments
        .filter(data => data.dmsId != null)
        .map(item => {
          const promise = request(
            `${env.APP_DOCS_V1_API_URL}/download/${item.dmsId}`,
            {
              method: 'GET',
              headers: env.headers,
            },
          );
          return promise;
        });
      const responeBodies = yield Promise.all(responses);
      console.log('RESPONSE BODIES OF DMS', responeBodies);
      yield put(fetchAppDocumentsV1Success(responeBodies));
    }
    yield put(fetchAppDocumentsV1Error(''));
  } catch (error) {
    yield put(fetchAppDocumentsV1Error(error));
  }
}

export function* fetchOtherDocs({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=OTHERDOCS_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchOtherDocsSuccess(response.masterData));
    } else {
      yield put(fetchOtherDocsError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchOtherDocsError('Master Data not found'));
  }
}

export function* generateAcceptanceLetter({ appId }) {
  debugger;
  try {
    const reqUrl = `${env.API_URL}/${
      env.ACCEPTANCE_LETTER_GENERATE
    }?appId=${appId}&isRequestLtrRequired=true&isAcceptanceLtrRequired=true&postSanctionedFlow=false`;
    const options = {
      method: 'GET',
      headers: env.headers,
    };
    const response = yield call(request, reqUrl, options);
    debugger;
    if (response.status === true || response.success) {
      yield put(
        fetchAcceptanceLetterSuccess(_.get(response, 'signed_urls', [])),
      );
    } else {
      yield put(fetchAcceptanceLetterError('Error Occured !'));
      message.info('Sorry, Acceptance Letter could not be generated!:(');
    }
  } catch (err) {
    debugger;
    yield put(
      fetchAcceptanceLetterError('Network error occurred! Check connection!'),
    );
    message.info('Network error occurred! Check connection!');
  }
}

// yield takeLatest(actions.FETCH_LEADSDOC_REQUEST, getDocs);
// yield takeLatest(actions.UPLOAD_LEADSDOC_REQUEST, uploadDoc);
function* actionWatcher() {
  yield takeEvery(actions.FETCH_LEADSDOC_REQUEST, getDocs);
  yield takeEvery(actions.UPLOAD_LEADSDOC_REQUEST, uploadDoc);
  yield takeEvery(actions.FETCH_DMS_V1_DOCS, fetchDMSV1DocsList);
  yield takeEvery(actions.FETCH_APPDOCUMENTS_V1, fetchAppDocumentsV1);
  yield takeEvery(actions.FETCH_OTHER_DOCS, fetchOtherDocs);
  yield takeEvery(actions.DELETE_FILE, deleteFile);
  yield takeEvery(actions.FETCH_ACCEPTANCE_LETTER, generateAcceptanceLetter);
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* leadDocumentsSaga() {
  // Watches for all actions
  yield all([actionWatcher()]);
}
