import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import { FETCH_GSTN } from './constants';
import { gstnFetched, gstnFetchingError } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* fetchGstnsByCuid({ pan, cuid }) {
  const requestURL = `${env.GST_API_URL}?pan=${pan}&cuid=${cuid}`;
  // const requestURL = `${env.GST_API_URL}?pan=AAACR5055K&cuid=1378458459`;
  const options = {
    method: 'GET',
    headers: env.headers,
  };

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(request, requestURL, options);
    if ((_.get(response, 'data') || []).length) {
      yield put(gstnFetched(response.data));
      // message.success(`GSTN fetched for ${pan}`);
    } else {
      yield put(gstnFetchingError(response));
      message.success(`GSTN not found`);
    }
  } catch (err) {
    yield put(gstnFetchingError(err));
  }
}

export function* fetchGstns({ pan }) {
  const requestURL = `${env.GSTN_API_URL}`;

  try {
    // Call our request helper (see 'utils/request')
    const options = {
      headers: { ...env.karzaKey },
      method: 'POST',
      body: JSON.stringify({
        consent: 'Y',
        pan,
      }),
    };
    const response = yield call(request, requestURL, options);
    if ((_.get(response, 'result') || []).length) {
      yield put(gstnFetched(response.result));
      // message.success(`GSTN fetched for ${pan}`);
    } else {
      yield put(gstnFetchingError(response));
      message.success(`GSTN not found`);
    }
  } catch (err) {
    yield put(gstnFetchingError(err));
  }
}

export default function* gstnInputSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_GSTN, fetchGstnsByCuid);
}
