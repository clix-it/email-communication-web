/*
 *
 * GstnInput actions
 *
 */

import { FETCH_GSTN, FETCH_GSTN_SUCCESS, FETCH_GSTN_FAILED } from './constants';

export function fetchGstn(pan, cuid) {
  return {
    type: FETCH_GSTN,
    pan,
    cuid,
  };
}

export function gstnFetched(response) {
  return {
    type: FETCH_GSTN_SUCCESS,
    response,
  };
}

export function gstnFetchingError(error) {
  return {
    type: FETCH_GSTN_FAILED,
    error,
  };
}
