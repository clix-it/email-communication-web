/**
 *
 * GstnInput
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import InputField from 'components/InputField';
import { Spin } from 'antd';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectGstnInput from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { fetchGstn } from './actions';

export function GstnInput({
  gstnInput,
  fetchGstn,
  pan,
  values,
  errors,
  setError,
  clearError,
  setValue,
  name,
  cuid,
  ...rest
}) {
  useInjectReducer({ key: 'gstnInput', reducer });
  useInjectSaga({ key: 'gstnInput', saga });

  const { response, error } = gstnInput;

  useEffect(() => {
    if ((pan || '').length === 10) {
      fetchGstn(pan, cuid);
    } else {
      setValue([{ 'users[0].appGsts[0].gstn': '' }]);
    }
  }, [pan]);

  useEffect(() => {
    if (error) setValue(name, '');
    if (response === null) {
      setError(name, 'pattern', 'Invalid Gstn');
    }
    if (response && response.length > 0) {
      setValue([{ 'users[0].appGsts[0].gstn': response[0] }]);
    }
  }, [response, error]);

  console.log('errors', pan);
  return (
    // <Spin spinning={loading} style={{ margin: '25% auto', width: '100%' }}>
    <>
      {response && response.length > 0 && (
        <InputField
          errors={errors}
          name={name}
          // defaultValue={response && response[0]}
          options={response || []}
          {...rest}
          labelHtml={<label htmlFor="gstn">GSTN</label>}
          // disabled={false}
        />
      )}
    </>
    // </Spin>
  );
}

GstnInput.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  gstnInput: makeSelectGstnInput(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchGstn: (pan, cuid) => dispatch(fetchGstn(pan, cuid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(GstnInput);
