/*
 *
 * GstnInput reducer
 *
 */
import _ from 'lodash';
import produce from 'immer';
import { FETCH_GSTN, FETCH_GSTN_SUCCESS, FETCH_GSTN_FAILED } from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const gstnInputReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_GSTN:
        draft.loading = true;
        draft.error = false;
        break;
      case FETCH_GSTN_SUCCESS:
        draft.loading = true;
        draft.response = _.map(
          _.filter(action.response, { authStatus: 'Active' }),
          'gstinId',
        );
        break;
      case FETCH_GSTN_FAILED:
        draft.loading = true;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default gstnInputReducer;
