import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the gstnInput state domain
 */

const selectGstnInputDomain = state => state.gstnInput || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by GstnInput
 */

const makeSelectGstnInput = () =>
  createSelector(
    selectGstnInputDomain,
    substate => substate,
  );

export default makeSelectGstnInput;
export { selectGstnInputDomain };
