/**
 *
 * Asynchronously loads the component for CallCommercialBureau
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
