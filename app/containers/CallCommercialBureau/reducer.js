/*
 *
 * CallCommercialBureau reducer
 *
 */
import produce from 'immer';
import {
  FETCH_BUREAU,
  FETCH_BUREAU_SUCCESS,
  FETCH_BUREAU_ERROR,
} from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const callCommercialBureauReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_BUREAU:
        draft.loading = true;
        draft.response = false;
        draft.error = false;
        break;
      case FETCH_BUREAU_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        draft.error = false;
        break;
      case FETCH_BUREAU_ERROR:
        draft.loading = false;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default callCommercialBureauReducer;
