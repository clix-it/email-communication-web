/*
 *
 * CallCommercialBureau actions
 *
 */

import {
  FETCH_BUREAU,
  FETCH_BUREAU_SUCCESS,
  FETCH_BUREAU_ERROR,
} from './constants';

export function fetchBureau(payload) {
  return {
    type: FETCH_BUREAU,
    ...payload,
  };
}

export function bureauFetched(response) {
  return {
    type: FETCH_BUREAU_SUCCESS,
    response,
  };
}

export function bureauFetchFailed(error) {
  return {
    type: FETCH_BUREAU_ERROR,
    error,
  };
}
