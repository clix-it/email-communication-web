/*
 *
 * CallCommercialBureau constants
 *
 */

export const FETCH_BUREAU = 'app/CallCommercialBureau/FETCH_BUREAU';
export const FETCH_BUREAU_SUCCESS =
  'app/CallCommercialBureau/FETCH_BUREAU_SUCCESS';
export const FETCH_BUREAU_ERROR = 'app/CallCommercialBureau/FETCH_BUREAU_ERROR';
