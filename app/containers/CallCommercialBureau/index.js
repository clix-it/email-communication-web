/**
 *
 * CallCommercialBureau
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Button } from 'antd';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectCallCommercialBureau from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchBureau } from './actions';

export function CallCommercialBureau({
  fetchBureau,
  callCommercialBureau,
  cuid,
}) {
  useInjectReducer({ key: 'callCommercialBureau', reducer });
  useInjectSaga({ key: 'callCommercialBureau', saga });

  return (
    <Button
      onClick={() => fetchBureau({ cuid })}
      loading={callCommercialBureau.loading}
      type="primary"
      className="personalDetailsRemoveButton"
      style={{ marginLeft: '10px' }}
    >
      Generate Cibil Report
    </Button>
  );
}

CallCommercialBureau.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  callCommercialBureau: makeSelectCallCommercialBureau(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    fetchBureau: cuid => dispatch(fetchBureau(cuid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(CallCommercialBureau);
