import { call, put, takeEvery } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import { bureauFetched, bureauFetchFailed } from './actions';
import { FETCH_BUREAU } from './constants';
import env from '../../../environment';

/**
 * Github repos request/response handler
 */
export function* fetchBureau({ cuid }) {
  const reqUrl = `${
    env.COMMERCIAL_BUREAU_API_URL
  }/?cuid=${cuid}&applicationId=${JSON.parse(sessionStorage.getItem('id'))}`;
  try {
    const response = yield call(request, reqUrl, {
      headers: env.headers,
    });
    yield put(bureauFetched(response));
  } catch (err) {
    yield put(
      bureauFetchFailed(err || 'Network error occurred! Check connection!'),
    );
    message.error(err.message || 'Error while calling Commercial Bureau');
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* callCommercialBureauSaga() {
  // Watches for FETCH SOA actions and calls fetchBureau when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeEvery(FETCH_BUREAU, fetchBureau);
}
