import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the callCommercialBureau state domain
 */

const selectCallCommercialBureauDomain = state =>
  state.callCommercialBureau || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by CallCommercialBureau
 */

const makeSelectCallCommercialBureau = () =>
  createSelector(
    selectCallCommercialBureauDomain,
    substate => substate,
  );

export default makeSelectCallCommercialBureau;
export { selectCallCommercialBureauDomain };
