import { createSelector } from 'reselect';

const selectTemplatePortal = state => state.templatePortal;

const makeSelectTemplates = () =>
  createSelector(
    selectTemplatePortal,
    templatePortal => templatePortal,
  );

export { makeSelectTemplates };
