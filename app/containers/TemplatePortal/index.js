/**
 *Credit Portal
 */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectTemplates } from './selectors';

import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import { loadTemplates, setCurrentApp } from './actions';

import TemplateListingPage from '../../components/TemplateListingPage';

export function TemplatePortal({
  dispatch,
  templatePortal,
  requestType,
  activity,
}) {
  const { isLoading = false, error, templateCommunications=[] } = templatePortal;
  if(activity){
    sessionStorage.setItem('activity', activity);
  }  
  return (
    <TemplateListingPage
      error={error}
      dispatch={dispatch}
      requestType={requestType}
      isLoading={isLoading}
      loadTemplates={loadTemplates}
      activityType={activity}
      templateCommunications={templateCommunications}
      templatePortal={templatePortal}
    />
  );
}

TemplatePortal.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

// const mapStateToProps = state => ({
//   isLoading: state.creditPortal.loading,
//   error: state.creditPortal.error,
//   applications: state.creditPortal.applications,
// });

const mapStateToProps = createStructuredSelector({
  templatePortal: makeSelectTemplates(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

TemplatePortal.propTypes = {
  templatePortal: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  requestType: PropTypes.string.isRequired,
};

TemplatePortal.defaultProps = {
  templatePortal: {},
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'templatePortal', reducer });

const withSaga = useInjectSaga({ key: 'templatePortal', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(TemplatePortal);
