/**
 * Template Portal Reducer
 */
import * as actions from './constants';
import produce from 'immer';

export const initialState = {
  loading: true,
  templateCommunications: [],
  //  error: {},
  response: false,
  error: false,
  payload: false,
  searchResponse: false,
};

const templatePortalReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.LOAD_TEMPLATES:
        draft.loading = true;
        break;
      case actions.LOAD_TEMPLATES_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.templateCommunications =
          action.response.data &&
          action.response.data.map((item, index) => {
            return { ...item, index: index + 1 };
          });
        break;
      case actions.LOAD_TEMPLATES_ERROR: {
        draft.error = action.error;
      }
      //Search

      case actions.LOAD_TEMPLATES_SEARCH:
        draft.loading = true;
        draft.error = false;
        draft.payload = action.data;
        console.log('Search Reducer')
        break;
      case actions.LOAD_TEMPLATES_SEARCH_SUCCESS:
        draft.loading = false;
        draft.error = false; 
        draft.templateCommunications =  action.response.data &&
        action.response.data.map((item, index) => {
          return { ...item, index: index + 1 };
        })
        break;
      case actions.LOAD_TEMPLATES_SEARCH_ERROR: {
        draft.loading = false;
        draft.error = action.error
      //  draft.response = false
      }
    }
  });

export default templatePortalReducer;
