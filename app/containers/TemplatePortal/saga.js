import { takeEvery, call, put } from 'redux-saga/effects';
import request from 'utils/request';
import {
  loadTemplatesSearchSuccess,
  loadTemplatesSearchError,
  loadTemplatesSuccess,
} from './actions';
import {
  LOAD_TEMPLATES,
  LOAD_TEMPLATES_SEARCH,
} from './constants';
import env from '../../../environment';

// Individual exports for testing
export default function* getEmailSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(LOAD_TEMPLATES, getTemplateData);
  yield takeEvery(LOAD_TEMPLATES_SEARCH, getTemplateDataSearch);
}

function* getTemplateData(action) {
  const masterUrl = `${env.GET_TEMPLATE}`;
  const options = {
    method: 'GET',
    headers: env.headers,
  };
  try {
    const response = yield call(request, masterUrl, options);
    console.log('response', response);
    if (response.success) yield put(loadTemplatesSuccess(response));
    else yield put(loadTemplatesError(response));
  } catch (error) {
    yield put(loadTemplatesError(error));
  }
}
//Search
function* getTemplateDataSearch({ payload }) {
  //console.log('payload Search', payload);
  const searchData = {
    templateId: payload.templateId,
    searchCriteria: 'partial',
  };
  const masterUrl = `${env.GET_TEMPLATE_SEARCH}`;
  const options = {
    method: 'POST',
    body: JSON.stringify(searchData),
    headers: env.headers,
  };
  try {
    const response = yield call(request, masterUrl, options);
    console.log('Template response', response);
    if (response.success) yield put(loadTemplatesSearchSuccess(response));
    else yield put(loadTemplatesSearchError(response));
  } catch (error) {
    yield put(loadTemplatesSearchError(error));
  }
}
