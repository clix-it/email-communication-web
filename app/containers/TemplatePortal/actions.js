import * as actions from './constants';

export function loadTemplates() {
  return {
    type: actions.LOAD_TEMPLATES,
  };
};

export function loadTemplatesSuccess(response) {
  return {
    type: actions.LOAD_TEMPLATES_SUCCESS,
    response,
  };
}

export function loadTemplatesError(error) {
  return {
    type: actions.LOAD_TEMPLATES_ERROR,
    error
  };
}

// Search

export function loadTemplatesSearch(payload) {
  return {
    type: actions.LOAD_TEMPLATES_SEARCH,
    payload,
  };
};

export function loadTemplatesSearchSuccess(response) {
  return {
    type: actions.LOAD_TEMPLATES_SEARCH_SUCCESS,
    response,
  };
}

export function loadTemplatesSearchError(error) {
  return {
    type: actions.LOAD_TEMPLATES_SEARCH_ERROR,
    error
  };
}