import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the financialUpload state domain
 */

const selectFinancialStatementUploadDomain = state =>
  state.financialStatementUpload || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by FinancialUpload
 */

const makeSelectFinancialStatementUpload = () =>
  createSelector(
    selectFinancialStatementUploadDomain,
    substate => substate,
  );

export default makeSelectFinancialStatementUpload;
export { selectFinancialStatementUploadDomain };
