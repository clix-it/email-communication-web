/*
 *
 * FinancialStatementUpload actions
 *
 */

import {
  START_FINANCIAL,
  START_FINANCIAL_SUCCESS,
  START_FINANCIAL_ERROR,
  GET_FINANCIAL_REPORT,
  GET_FINANCIAL_REPORT_SUCCESS,
  GET_FINANCIAL_REPORT_ERROR,
  GET_FINANCIAL_STATUS,
  GET_FINANCIAL_STATUS_SUCCESS,
  GET_FINANCIAL_STATUS_ERROR,
} from './constants';

export function getPerfiosReport(payload) {
  return {
    type: GET_FINANCIAL_REPORT,
    payload,
  };
}

export function getPerfiosReportSuccess(response) {
  return {
    type: GET_FINANCIAL_REPORT_SUCCESS,
    response,
  };
}

export function getPerfiosReportError(error) {
  return {
    type: GET_FINANCIAL_REPORT_ERROR,
    error,
  };
}

export function startPerfios({ payload, password, appId, fileList, cuid }) {
  return {
    type: START_FINANCIAL,
    payload,
    password,
    appId,
    fileList,
    cuid,
  };
}

export function startPerfiosSuccess(response) {
  return {
    type: START_FINANCIAL_SUCCESS,
    response,
  };
}

export function startPerfiosError(error) {
  return {
    type: START_FINANCIAL_ERROR,
    error,
  };
}

export function getPerfiosStatus(appId) {
  return {
    type: GET_FINANCIAL_STATUS,
    appId,
  };
}

export function getPerfiosStatusSuccess(response) {
  return {
    type: GET_FINANCIAL_STATUS_SUCCESS,
    response,
  };
}

export function getPerfiosStatusError(error) {
  return {
    type: GET_FINANCIAL_STATUS_ERROR,
    error,
  };
}
