/*
 *
 * FinancialStatementUpload reducer
 *
 */
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
  GET_FINANCIAL_REPORT,
  GET_FINANCIAL_REPORT_SUCCESS,
  GET_FINANCIAL_REPORT_ERROR,
  START_FINANCIAL,
  START_FINANCIAL_SUCCESS,
  START_FINANCIAL_ERROR,
  GET_FINANCIAL_STATUS,
  GET_FINANCIAL_STATUS_SUCCESS,
  GET_FINANCIAL_STATUS_ERROR,
} from './constants';
export const initialState = {
  loading: false,
  response: false,
  error: false,
  perfiosStatus: [],
  perfiosAbbreviations: {},
};

const abbreviations = {
  CTD: 'Transaction created',
  FAIL: 'Transaction failed',
  CNCD: 'Connected data for Analysis initiation',
  UPLD: 'All documents uploaded for analysis',
  NBCT: 'Transaction created for netbanking',
  NBF: 'Transaction for netbanking failed',
  NBC: 'Transaction for netbanking completed',
  CNBC: 'Client completed the netbanking',
};

/* eslint-disable default-case, no-param-reassign */
const financialStatementUploadReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_FINANCIAL_REPORT:
      case START_FINANCIAL:
        draft.loading = true;
        break;
      case GET_FINANCIAL_REPORT_SUCCESS:
      case START_FINANCIAL_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        break;
      case GET_FINANCIAL_REPORT_ERROR:
      case START_FINANCIAL_ERROR:
        draft.loading = false;
        draft.error = action.error;
        break;
      case GET_FINANCIAL_STATUS:
        draft.loading = true;
        draft.perfiosAbbreviations = { ...abbreviations };
        break;
      case GET_FINANCIAL_STATUS_SUCCESS:
        draft.loading = false;
        draft.perfiosStatus = action.response;
        break;
      case GET_FINANCIAL_STATUS_ERROR:
        draft.loading = false;
        draft.perfiosStatus = [];
        break;
      case LOCATION_CHANGE:
        draft.perfiosStatus = [];
        break;
    }
  });

export default financialStatementUploadReducer;
