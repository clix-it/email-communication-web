/*
 * FinancialUpload Messages
 *
 * This contains all the text for the FinancialUpload container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.FinancialUpload';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the FinancialUpload container!',
  },
});
