/**
 *
 * FinancialStatementUpload
 *
 */

import _ from 'lodash';
import moment from 'moment';
import React, { memo, useEffect, useState } from 'react';
import cookies from 'react-cookies';
import { useLocation } from 'react-router-dom';

import PropTypes from 'prop-types';

import {
  Row,
  Col,
  Button,
  List,
  Form,
  Upload,
  DatePicker,
  Select,
  Typography,
  Anchor,
  Radio,
  Input,
  Space,
  Divider,
  Tooltip,
  InputNumber,
  Table,
} from 'antd';
const { Option } = Select;
const { Link } = Anchor;
const { Title } = Typography;
import { useForm, Controller } from 'react-hook-form';

import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import {
  UploadOutlined,
  CloseOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from '@ant-design/icons';
import { compose } from 'redux';

import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectFinancialStatementUpload from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { getPerfiosReport, startPerfios, getPerfiosStatus } from './actions';
import InputField from '../../components/InputField';
import makeSelectApplicantDetails, {
  makeSelectApplicantDetails2,
} from '../ApplicantDetails/selectors';
import { isDirtyForm } from '../ApplicantDetails/actions';
//import Title from 'antd/lib/skeleton/Title';

const { RangePicker } = DatePicker;

const columnResponsiveLayout = {
  colSingle: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
  colHalf: { xs: 24, sm: 24, md: 24, lg: 12, xl: 10 },
  colOneThird: { xs: 24, sm: 24, md: 24, lg: 8, xl: 8 },
};
export function FinancialStatementUpload({
  loanOffers,
  appId,
  startPerfios,
  dispatch,
  getPerfiosReport,
  applicantDetails,
  applicantDetails2,
  financialUpload,
}) {
  useInjectReducer({ key: 'financialStatementUpload', reducer });
  useInjectSaga({ key: 'financialStatementUpload', saga });

  const loc = useLocation();

  const { perfiosStatus = [] } = financialUpload;

  const columnData = [
    {
      title: 'Institution Code',
      dataIndex: 'institutionCode',
    },
    {
      title: 'Status',
      dataIndex: 'status',
    },
    {
      title: 'Operation',
      render: (item, record) => {
        if (item.statusCode == 'FAIL' || item.statusCode == 'NBF') {
          return null;
        } else {
          return (
            <Button
              type="primary"
              htmlType="button"
              className="entityDetailsButton"
              onClick={() =>
                getPerfiosReport({
                  applicantCode: item.applicantCode,
                  appId,
                })
              }
            >
              Fetch Report
            </Button>
          );
        }
      },
    },
  ];

  const {
    errors,
    control,
    watch,
    handleSubmit,
    reset,
    setValue,
    formState,
  } = useForm({
    mode: 'onChange',
  });

  const years = function(startYear) {
    var currentYear = new Date().getFullYear(),
      years = [];
    startYear = startYear || 1980;
    while (startYear <= currentYear) {
      years.push(startYear++);
    }
    return years.reverse();
  };

  const [disableButton, setDisableButton] = React.useState(false);
  const role = JSON.parse(sessionStorage.getItem('userRole'));
  useEffect(() => {
    if (
      loc.pathname.toLowerCase().includes('poa/') ||
      loc.pathname.toLowerCase().includes('clo/')
    ) {
      setDisableButton(true);
    }
    dispatch(getPerfiosStatus(appId));
  }, []);
  useEffect(() => {
    dispatch(isDirtyForm({ 4: formState.dirty }));
  }, [formState.dirty]);
  const values = watch();
  console.log('Financial Upload Values', values);

  const [isPassword, setIsPassword] = useState(false);
  const [cuidArr, setCuidArr] = useState([]);

  useEffect(() => {
    if (applicantDetails2 && applicantDetails2.entity) {
      const users = _.get(applicantDetails2, 'entity.users') || [];
      if (users && users.length > 0) {
        let arr = users
          .filter(item => item.userLinked)
          .map(item => {
            return {
              cuid: item.cuid,
              name: item.registeredName
                ? item.registeredName
                : `${item.firstName} ${item.lastName}`,
            };
          });
        setCuidArr(arr);
      } else {
        setCuidArr([]);
      }
    }
  }, []);

  const options = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];

  function removeFile(fileIndex) {
    //If only 1 file is removed then reset the form else splice the array
    if (values.financialStatement.fileList.length === 1) {
      reset({
        financialStatement: null,
      });
      //  document.getElementById('bankStatement')
      return;
    }
    let temp = _.remove(values.financialStatement.fileList, (item, index) => {
      return index !== fileIndex;
    });
    const newFileList = {
      fileList: temp,
    };
    setValue('financialStatement', newFileList);
  }

  const onSubmit = data => {
    console.log('data', data);
    //Send for perfios

    let uploadData = {
      loanApplicationId: appId,
      transactionCode: '',
      source: 'LOS-CLIX',
      loanAmount:
        _.get(
          _.filter(
            loanOffers,
            loanOffer =>
              loanOffer.type === 'credit_amount' ||
              loanOffer.type === 'eligible_amount',
          ),
          '[0].loanAmount',
        ) || '100001',
      loanDuration: '48',
      loanType:
        _.get(applicantDetails, 'entity.type') == 'INDIVIDUAL'
          ? 'PERSONAL_LOAN'
          : 'BUSINESS_LOAN',
      applicantName:
        _.get(applicantDetails, 'entity.type') == 'INDIVIDUAL'
          ? `${_.get(applicantDetails, 'entity.firstName')} ${_.get(
              applicantDetails,
              'entity.lastName',
            )}`
          : _.get(applicantDetails, 'entity.registeredName'),
      scannedStatement: false,
      documentObjIdList: [],
      financialYearList: data.year,
    };
    startPerfios({
      payload: uploadData,
      password: data.password || '',
      appId,
      fileList: data.financialStatement.fileList,
      cuid: data.cuid,
    });
    setTimeout(() => {
      reset({
        year: [],
        financialStatement: '',
        cuid: '',
      });
    }, 1000);
    //uploadStatemnet({ uploadData, password: data.password });
  };

  return (
    <>
      <Title level={4}>Financial Statement Analyser</Title>
      <form>
        <Row gutter={[16, 24]}>
          {/* <form onSubmit={handleSubmit}> */}
          <Col {...columnResponsiveLayout.colOneThird}>
            <InputField
              mode="multiple"
              id="year"
              control={control}
              name="year"
              type="select"
              rules={{
                required: true,
              }}
              options={years()}
              errors={errors}
              placeholder="Select Year(s)"
              labelHtml="Select Year(s)"
            />
          </Col>
          <Col {...columnResponsiveLayout.colHalf}>
            <Form.Item label="Select User*" className="entityFormStyle">
              <Controller
                placeholder="Select User"
                id="cuid"
                name="cuid"
                control={control}
                rules={{
                  required: true,
                }}
                as={
                  <Select
                    showSearch
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                    size="large"
                    placeholder="Select User"
                  >
                    {cuidArr &&
                      cuidArr.map((item, index) => {
                        return (
                          <Option key={index} value={item.cuid}>
                            {item.cuid}-{item.name}
                          </Option>
                        );
                      })}
                  </Select>
                }
              />
              <ErrorMessage name="dateRange" errors={errors} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={[16, 24]}>
          <Col
            {...columnResponsiveLayout.colOneThird}
            className="entityStyleCol"
          >
            <Form.Item
              label="Select Financial Statement"
              className="entityFormStyle"
            >
              <Controller
                disabled={disableButton}
                placeholder="Select Financial Statement"
                type="file"
                id="financialStatement"
                name="financialStatement"
                control={control}
                rules={{
                  required: true,
                }}
                as={
                  <Upload
                    accept=".pdf"
                    showUploadList={false}
                    multiple={true}
                    fileList={
                      values.financialStatement
                        ? [...values.financialStatement.fileList]
                        : []
                    }
                    // customRequest={handleFileUpload}
                  >
                    <Button>
                      <UploadOutlined /> Choose File
                    </Button>
                  </Upload>
                }
              />
              <ErrorMessage name="financialStatement" errors={errors} />
            </Form.Item>
          </Col>
          {values.financialStatement &&
          values.financialStatement.hasOwnProperty('fileList') ? (
            <>
              <Col {...columnResponsiveLayout.colOneThird}>
                <Form.Item label="File Name">
                  {values.financialStatement.fileList.map((item, index) => (
                    <Col
                      {...columnResponsiveLayout.colSingle}
                      style={{ margin: '10px 0' }}
                    >
                      <Tooltip title="Remove file">
                        <a
                          style={{ color: 'black' }}
                          onClick={() => {
                            //Remove file
                            removeFile(index);
                          }}
                        >
                          {' '}
                          {item.name}
                        </a>
                        <CloseOutlined
                          onClick={() => {
                            //Remove file
                            removeFile(index);
                          }}
                          style={{
                            fontSize: '20px',
                            verticalAlign: 0,
                            marginLeft: '0.225em',
                          }}
                        />
                      </Tooltip>
                    </Col>
                  ))}
                </Form.Item>
              </Col>
              <Col {...columnResponsiveLayout.colOneThird}>
                <Form.Item label="Is File Password Protected?">
                  {/* {values.bankStatement.fileList.map((item, index) => ( */}
                  <>
                    <Radio.Group
                      options={options}
                      value={isPassword}
                      onChange={e => {
                        e.preventDefault();
                        setIsPassword(e.target.value);
                      }}
                    />
                    {isPassword ? (
                      <div style={{ padding: '1em', paddingLeft: '0' }}>
                        <Controller
                          name={'password'}
                          control={control}
                          rules={{
                            required: isPassword ? true : false,
                          }}
                          as={
                            <Space direction="vertical">
                              <Input.Password
                                placeholder="Enter Password"
                                iconRender={visible =>
                                  visible ? (
                                    <EyeTwoTone />
                                  ) : (
                                    <EyeInvisibleOutlined />
                                  )
                                }
                              />
                            </Space>
                          }
                        />
                        <ErrorMessage name={'password'} errors={errors} />
                      </div>
                    ) : (
                      <> </>
                    )}
                  </>
                  {/* ))} */}
                </Form.Item>
              </Col>
            </>
          ) : (
            <> </>
          )}
          <Divider />
          <Col {...columnResponsiveLayout.colOneThird}>
            <Button
              disabled={disableButton}
              type="primary"
              htmlType="button"
              className="entityDetailsButton"
              onClick={handleSubmit(onSubmit)}
            >
              Initiate
            </Button>
          </Col>
          <Divider />
        </Row>
      </form>
      {perfiosStatus.length > 0 && (
        <>
          <Row gutter={[16, 24]}>
            <Col
              {...columnResponsiveLayout.colSingle}
              className="personalStyleCol"
            >
              <b>Perfios Analysis Status </b>
            </Col>
          </Row>
          <Table bordered dataSource={perfiosStatus} columns={columnData} />
        </>
      )}
    </>
  );
}

FinancialStatementUpload.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  financialUpload: makeSelectFinancialStatementUpload(),
  applicantDetails: makeSelectApplicantDetails(),
  applicantDetails2: makeSelectApplicantDetails2(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPerfiosReport: payload => dispatch(getPerfiosReport(payload)),
    startPerfios: payload => dispatch(startPerfios(payload)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(FinancialStatementUpload);
