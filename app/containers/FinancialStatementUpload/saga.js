import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import {
  takeEvery,
  put,
  call,
  takeLatest,
  select,
  all,
} from 'redux-saga/effects';
import request from 'utils/request';
import cookies from 'react-cookies';
import { message } from 'antd';
import {
  START_FINANCIAL,
  GET_FINANCIAL_REPORT,
  GET_FINANCIAL_STATUS,
} from './constants';
import env from '../../../environment';
import {
  startPerfiosSuccess,
  startPerfiosError,
  getPerfiosReportSuccess,
  getPerfiosReportError,
  fetchPerfiosBanksSuccess,
  fetchPerfiosBanksError,
  getPerfiosStatusSuccess,
  getPerfiosStatusError,
} from './actions';
import makeSelectFinancialStatementUpload from './selectors';
import makeSelectApplicantDetails from '../ApplicantDetails/selectors';
import _ from 'lodash';

/**
 *
 * @param {File} item
 * @param {String} appId
 * @param {String} password
 * @description Wrappper Function to call upload File to DMS V1 function and provide necessary arguments
 */
function* doSomethingWithItem(item, appId, password, cuid) {
  console.log('doSomethingWithItem', item);
  const content = yield call(readAsText, item.originFileObj);
  console.log('content', content);
  //Make api call
  const stream = content.split('data:application/pdf;base64,')[1];
  const guid = uuidv4();
  debugger;
  const uploadDMSData = {
    objId: cuid,
    objType: 'user',
    docType: 'bank statement',
    fileName: `${cuid}-${item.name}`,
    fileAttributes: {
      isPasswordProtected: password ? true : false,
      filePassword: password,
      cuid: cuid,
      guid: guid,
    },
    files: [stream],
  };
  try {
    const dmsResponse = yield uploadDocumentDMS(uploadDMSData);
    if (dmsResponse.status) {
      return guid;
    }
  } catch (error) {}
}

/**
 *
 * @param {Array<File>} items
 * @param {String} appId
 * @param {String} password
 * @description Function to iterate over List of Files and call wrapper function foe uploading
 */
function* doSomethingWithAllItems(items, appId, password, cuid) {
  console.log('doSomethingWithAllItems');
  return yield all(
    items.map(item => call(doSomethingWithItem, item, appId, password, cuid)),
  );
}

/**
 *
 * @param {Blob} blob
 * @description Function to read data of File blob and return fileStream data
 */
function readAsText(blob) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => resolve(reader.result);
    reader.onerror = reject;
    reader.readAsDataURL(blob);
  });
}

export function* initiatePerfios({ payload, password, appId, fileList, cuid }) {
  console.log('payload for perfios check', payload);
  // Upload fileList to DMS and get DMS ID to sent to perfios payload
  const dmsArray = yield call(
    doSomethingWithAllItems,
    fileList,
    appId,
    password,
    cuid,
  );

  console.log('dmsArray', dmsArray);
  payload.documentObjIdList = dmsArray;
  debugger;
  const reqUrl = `${env.API_URL}/sa/v2/finance/analysis/initiate`;
  const options = {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify(payload),
  };
  try {
    const perfiosRes = yield call(request, reqUrl, options);
    console.log('perfios response', perfiosRes);
    if (perfiosRes.success) {
      // Check for existing running perfios List and update it
      message.success('File sent for Financial Analysis', 4);
      yield put(startPerfiosSuccess(perfiosRes));
      const reqUrl = `${
        env.API_URL
      }/sa/v1/analysis/getanalysisstatus?value=${appId}`;
      const options = {
        method: 'GET',
        headers: env.headers,
      };
      const data = yield select(makeSelectFinancialStatementUpload());
      try {
        const res = yield call(request, reqUrl, options);
        if (res && res.length > 0) {
          let arr = [];
          res
            .filter(item => item.transactionType == 'FINANCESTATEMENT')
            .forEach(item => {
              arr.push({
                institutionCode:
                  item.bankDetails && item.bankDetails.institutionCode,
                yearMonthFrom:
                  item.bankDetails && item.bankDetails.yearMonthFrom,
                yearMonthTo: item.bankDetails && item.bankDetails.yearMonthTo,
                status: data.perfiosAbbreviations[item.status] || '',
              });
            });
          yield put(getPerfiosStatusSuccess(arr));
          return;
        }
        message.error(`no record found for ${appId}`, 4);
        yield put(getPerfiosStatusError(`no record found for ${appId}`));
      } catch (error) {
        message.error(`no record found for ${appId}`, 4);
        yield put(getPerfiosStatusError(`no record found for ${appId}`));
      }
    } else {
      if (
        perfiosRes.errorResponseDto &&
        perfiosRes.errorResponseDto.errorMessage
      ) {
        message.error(perfiosRes.errorResponseDto.errorMessage, 4);
      } else {
        message.error('Something went wrong. Please try again', 4);
      }
      yield put(startPerfiosError(perfiosRes));
    }
  } catch (error) {
    message.error('Something went wrong. Please try again', 4);
    yield put(startPerfiosError(error));
  }
}

/**
 *
 * @param {Map} params
 * @description Function to upload a file to DMS and get DMS id
 * @returns DMS ID of uploaded file
 */
export function* uploadDocumentDMS(params) {
  console.log('PArams for uploading document to DMS', params);
  const reqUrl = `${env.API_URL}/app-docs/v2/upload`;
  const options = {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify(params),
  };
  try {
    const response = yield call(request, reqUrl, options);
    console.log('Response from Upload document to DMS APi', response);
    return response;
  } catch (error) {
    return error;
  }
}

/**
 *
 * @param {String} appId
 * @param {FileStream} fileStream
 * @description Function to upload Xls File to App Docs V2
 */
export function* uploadXlsFile({ appId, fileStream }) {
  const applicantDetails = yield select(makeSelectApplicantDetails());
  const cuid = _.get(applicantDetails, 'entity.cuid');
  const reqUrl = `${env.API_URL}/app-docs/v2/upload`;
  const payload = {
    objId: cuid,
    objType: 'user',
    docType: 'Financial Statement Analysis Report',
    fileName: `Perfios Report ${moment().format('LLLL')}.xlsx`,
    fileAttributes: {},
    files: [fileStream],
  };
  const options = {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify(payload),
  };
  try {
    const xlsRes = yield call(request, reqUrl, options);
    return xlsRes;
  } catch (error) {
    return error;
  }
}

export function* getReport({ payload }) {
  console.log('transactioncode of perfios report', payload);

  const reqUrl = `${env.API_URL}/sa/v1/reportfile?loanApplicationId=${
    payload.appId
  }&reportType=xls&forBusinessLoan=true`;
  const options = {
    method: 'GET',
    headers: env.headers,
  };
  try {
    const report = yield call(request, reqUrl, options);
    console.log('perfios report response', report);
    if (report.success) {
      if (
        report.applicantCodeDocDataMap[payload.applicantCode] &&
        report.applicantCodeDocDataMap[payload.applicantCode].hasOwnProperty(
          'fileStream',
        ) &&
        report.applicantCodeDocDataMap[payload.applicantCode].fileStream
      ) {
        // Upload filestream to App-docs-v2 Service and Remove this applicant code from cookies
        const xlsFileUploadRes = yield uploadXlsFile({
          appId: payload.appId,
          fileStream:
            report.applicantCodeDocDataMap[payload.applicantCode].fileStream,
        });
        if (xlsFileUploadRes.status) {
          message.success(
            'Report fetched and sent to Documents section to view',
            6,
          );
          yield put(getPerfiosReportSuccess(report));
          return;
        }
      } else {
        message.error(
          (report.applicantCodeDocDataMap[payload.applicantCode] &&
            report.applicantCodeDocDataMap[payload.applicantCode].error) ||
            'Error in fetching report',
        );
      }
    }
  } catch (error) {
    if (error.errorResponseDTO && error.errorResponseDTO.errorMessage) {
      message.error(error.errorResponseDTO.errorMessage, 4);
    } else if (error.errorResponseDto && error.errorResponseDto.errorMessage) {
      message.error(error.errorResponseDto.errorMessage, 4);
    } else {
      message.error('Unable to hit perfios', 4);
    }
    yield put(getPerfiosReportError(error));
    //  toast('Still Pending at Perfios');
  }
}

export function* getPerfiosBanks() {
  const reqUrl = `${env.API_URL}/sa/v1/institutions`;
  const options = {
    method: 'GET',
    headers: env.headers,
  };
  try {
    const res = yield call(request, reqUrl, options);
    if (res.success) {
      yield put(fetchPerfiosBanksSuccess(res));
      return;
    }
    yield put(fetchPerfiosBanksError(res));
  } catch (error) {
    yield put(fetchPerfiosBanksError(error));
  }
}

export function* getPerfiosStatus({ appId }) {
  const reqUrl = `${
    env.API_URL
  }/sa/v1/analysis/getanalysisstatus?value=${appId}`;
  const options = {
    method: 'GET',
    headers: env.headers,
  };
  const data = yield select(makeSelectFinancialStatementUpload());
  try {
    const res = yield call(request, reqUrl, options);
    if (res && res.length > 0) {
      let arr = [];
      res
        .filter(item => item.transactionType.includes('FINANCESTATEMENT'))
        .forEach(item => {
          arr.push({
            institutionCode:
              item.bankDetails && item.bankDetails.institutionCode,
            yearMonthFrom: item.bankDetails && item.bankDetails.yearMonthFrom,
            yearMonthTo: item.bankDetails && item.bankDetails.yearMonthTo,
            status: data.perfiosAbbreviations[item.status] || '',
            statusCode: item.status || '',
            applicantCode: item.applicantCode,
          });
        });
      yield put(getPerfiosStatusSuccess(arr));
      return;
    }
    message.error(`no record found for ${appId}`, 4);
    yield put(getPerfiosStatusError(`no record found for ${appId}`));
  } catch (error) {
    message.error(`no record found for ${appId}`, 4);
    yield put(getPerfiosStatusError(`no record found for ${appId}`));
  }
}

// Individual exports for testing
export default function* financialStatementUploadSaga() {
  yield takeEvery(START_FINANCIAL, initiatePerfios);
  yield takeLatest(GET_FINANCIAL_STATUS, getPerfiosStatus);
  yield takeLatest(GET_FINANCIAL_REPORT, getReport);
}
