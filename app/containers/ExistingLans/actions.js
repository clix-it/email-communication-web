/*
 *
 * ExistingLans actions
 *
 */

import {
  FETCH_USER_LANS,
  FETCH_USER_LANS_FETCHED,
  FETCH_USER_LANS_FAILED,
} from './constants';

export function fetchLans(cuid) {
  return {
    type: FETCH_USER_LANS,
    cuid,
  };
}

export function lansFetched(response) {
  return {
    type: FETCH_USER_LANS_FETCHED,
    response,
  };
}

export function lansFetchingError(error) {
  return {
    type: FETCH_USER_LANS_FAILED,
    error,
  };
}
