/**
 *
 * ExistingLans
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Spin, Drawer } from 'antd';
import LanDetails from 'containers/LanDetails';
import { ContainerOutlined } from '@ant-design/icons';

import InputField from 'components/InputField';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import _ from 'lodash';
import makeSelectExistingLans from './selectors';
import reducer from './reducer';
import saga from './saga';
import LanTabComponent from '../../components/LanTabComponent';

import { fetchLans } from './actions';
export function ExistingLans({
  cuid,
  existingLAN,
  fetchLans,
  existingLans,
  applicantDetails,
  ...rest
}) {
  useInjectReducer({ key: 'existingLans', reducer });
  useInjectSaga({ key: 'existingLans', saga });

  useEffect(() => {
    fetchLans(cuid);
  }, [cuid]);
  const [visible, setVisible] = useState(false);

  const { loading, response = [] } = existingLans;

  const optionValues = (response || []).map(lan => lan.loanAccountNumber);

  const lms = (_.find(response || [], { loanAccountNumber: existingLAN }) || {})
    .source_System;
  console.log('lms', lms);

  return (
    <Spin spinning={loading}>
      <InputField
        {...rest}
        // mode="tags"
        defaultValue={existingLAN}
        type={response && response.length ? 'select' : 'string'}
        options={optionValues}
        placeholder="Select Lan"
        labelHtml="Existing Lans"
      />
      {existingLAN && (
        <div onClick={() => setVisible(true)}>
          <ContainerOutlined onClick={() => setVisible(true)} />
          &nbsp;View More
        </div>
      )}
      <Drawer
        placement="right"
        visible={visible}
        onClose={() => setVisible(false)}
        key="remarks"
        width="80%"
      >
        <LanTabComponent
          lan={existingLAN}
          lms={lms}
          applicantDetails={applicantDetails}
          {...rest}
        />
      </Drawer>
    </Spin>
  );
}

ExistingLans.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  existingLans: makeSelectExistingLans(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchLans: cuid => dispatch(fetchLans(cuid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ExistingLans);
