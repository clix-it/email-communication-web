/*
 *
 * ExistingLans constants
 *
 */

export const FETCH_USER_LANS = 'app/ExistingLans/FETCH_USER_LANS';
export const FETCH_USER_LANS_FETCHED =
  'app/ExistingLans/FETCH_USER_LANS_FETCHED';
export const FETCH_USER_LANS_FAILED = 'app/ExistingLans/FETCH_USER_LANS_FAILED';
