/*
 *
 * ExistingLans reducer
 *
 */
import produce from 'immer';
import _ from 'lodash';
import {
  FETCH_USER_LANS,
  FETCH_USER_LANS_FETCHED,
  FETCH_USER_LANS_FAILED,
} from './constants';
export const initialState = {
  cuid: false,
  response: false,
  loading: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const existingLansReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_USER_LANS:
        draft.loading = true;
        draft.cuid = action.cuid;
        break;
      case FETCH_USER_LANS_FETCHED:
        draft.loading = false;
        const users = action.response;
        draft.response = action.response;
        break;
      case FETCH_USER_LANS_FAILED:
        draft.loading = false;
        draft.error = action.error;
        break;
    }
  });

export default existingLansReducer;
