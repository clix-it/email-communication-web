import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the existingLans state domain
 */

const selectExistingLansDomain = state => state.existingLans || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ExistingLans
 */

const makeSelectExistingLans = () =>
  createSelector(
    selectExistingLansDomain,
    substate => substate,
  );

export default makeSelectExistingLans;
export { selectExistingLansDomain };
