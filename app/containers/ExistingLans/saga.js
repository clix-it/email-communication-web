import { call, put, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';
import { FETCH_USER_LANS } from './constants';
import { lansFetched, lansFetchingError } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* fetchLans({ cuid }) {
  const requestURL = `${env.SELF_SERVICE}/myaccount/${cuid}`;

  try {
    // Call our request helper (see 'utils/request')
    const options = {
      headers: env.headers,
    };
    const response = yield call(request, requestURL, options);
    if (response.success) {
      yield put(lansFetched(response.commonLmsResponseList));
    } else {
      yield put(lansFetchingError(response.commonLmsResponseList));
    }
  } catch (err) {
    yield put(lansFetchingError(err));
  }
}

export default function* existingLansSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_USER_LANS, fetchLans);
}
