/*
 *
 * LeadDetails actions
 *
 */

import * as actions from './constants';

export function fetchDetails(appId) {
  return {
    type: actions.FETCH_DETAILS,
    appId,
  };
}

export function resetState() {
  return {
    type: actions.RESET_STATE,
  };
}

export function fetchDetailsInit() {
  return {
    type: actions.FETCH_DETAILS_INIT,
  };
}
export function fetchDetailsSuccess(appDetails) {
  return {
    type: actions.FETCH_DETAILS_SUCCESS,
    appDetails,
  };
}

export function fetchUserDetailsSuccess(userDetails, matchType, requestId) {
  return {
    type: actions.FETCH_USER_DETAILS_SUCCESS,
    userDetails,
    matchType,
    requestId,
  };
}

export function fetchCoApplicantDetailsInit() {
  return {
    type: actions.FETCH_COAPPLICANT_DETAILS_INIT,
  };
}

export function fetchLoanDetailsInit() {
  return {
    type: actions.FETCH_LOAN_DETAILS_INIT,
  };
}

export function fetchLoanDetails(userData) {
  return {
    type: actions.FETCH_LOAN_DETAILS,
    userData,
  };
}

export function fetchCoApplicantDetailsSuccess(coApplicantData) {
  return {
    type: actions.FETCH_COAPPLICANT_DETAILS_SUCCESS,
    coApplicantData,
  };
}

export function fetchCoApplicantDetails(userData) {
  return {
    type: actions.FETCH_COAPPLICANT_DETAILS,
    userData,
  };
}

export function setSelectedCoApplicant(applicant) {
  return {
    type: actions.SET_CURRENT_COAPPLICANT,
    applicant,
  };
}

export function setSelectedLoan(loan) {
  return {
    type: actions.SET_CURRENT_LOAN,
    loan,
  };
}

export function fetchDetailsError(errMessage) {
  return {
    type: actions.FETCH_DETAILS_ERROR,
    errMessage,
  };
}

export function loadPartialMatchError(partialMatchError) {
  return {
    type: actions.PARTIAL_MATCH_ERROR,
    partialMatchError,
  };
}

export function fetchUserDetailsError(errMessage) {
  return {
    type: actions.FETCH_USER_DETAILS_ERROR,
    errMessage,
  };
}

export function partialMatchFetchUserDetailsSuccess(partialMatchUserDetails) {
  return {
    type: actions.PARTIAL_MATCH_USER_DETAILS_SUCCESS,
    partialMatchUserDetails,
  };
}

export function setSelectedMatchedUser(matchedUser) {
  return {
    type: actions.SET_CURRENT_MATCHED_USER,
    matchedUser,
  };
}

export function setCheckedUser(userArr) {
  return {
    type: actions.SET_CHECKED_USER,
    userArr,
  };
}

export function fetchMatchUsersLoanDetails(userData) {
  return {
    type: actions.FETCH_MATCH_USER_LOAN_DETAILS,
    userData,
  };
}

export function partialLoader() {
  return {
    type: actions.PARTIAL_LOADER,
  };
}

export function partialMatchFetchLeadDetailsSuccess(partialMatchLeadDetails) {
  return {
    type: actions.PARTIAL_MATCH_LEAD_DETAILS_SUCCESS,
    partialMatchLeadDetails,
  };
}

export function fetchAppDetails(appId) {
  return {
    type: actions.FETCH_APP_DETAILS,
    appId,
  };
}

export function setSelectedMatchLoan(loan) {
  return {
    type: actions.SET_CURRENT_MATCH_LOAN,
    loan,
  };
}
