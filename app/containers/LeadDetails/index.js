/**
 *
 * LeadDetails
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import { useParams, useLocation, useHistory } from 'react-router-dom';
import reducer from './reducer';
import saga from './saga';
import { fetchDetails, fetchAppDetails, submitDecision, resetState } from './actions';
import makeSelectLeadDetails from './selectors';
import DedupeTabs from '../../components/DedupeTabs';
import DedupeEntityDetails from '../../components/DedupeEntityDetails';
import DedupeLoanDetail from '../../components/DedupeLoanDetail';
import DedupeCoApplicantDetails from '../../components/DedupeCoApplicantDetails';
import DedupeMatchDetails from '../../components/DedupeMatchDetails';
import { Spin } from 'antd'; 

export function LeadDetails({ dispatch, detailsData }) {
  const { appId } = useParams();
  useEffect(() => {
    const address = location.pathname.split('/');
    if(address[1].toLowerCase().includes('clo')) {
      dispatch(fetchAppDetails(appId));
    } else {
      dispatch(fetchDetails(appId));
    }
  }, [appId]);

  const { userDetails = {} } = detailsData;
  let isNoEntity = false;
  if(userDetails && userDetails.type !== 'COMPANY'){
    isNoEntity = true;
  }

  const tabsToShow = [
    {
      id: 'entityDetails',
      name: isNoEntity ? 'Applicant Details' : 'Entity Details',
      component: (
        <DedupeEntityDetails
          dispatch={dispatch}
          appId={appId}
          detailsData={detailsData}
          tabName={isNoEntity ? 'Applicant Details' : 'Entity Details'}
          isNoEntity={isNoEntity}
        />
      ),
    },
    {
      id: 'applicantDetails',
      name: 'Relationships',
      component: (
        <DedupeCoApplicantDetails
          dispatch={dispatch}
          appId={appId}
          detailsData={detailsData}
          tabName="Relationships"
        />
      ),
    },
    {
      id: 'loanDetails',
      name: 'Loan Details',
      component: (
        <DedupeLoanDetail
          dispatch={dispatch}
          appId={appId}
          detailsData={detailsData}
          tabName="Loan Details"
        />
      ),
    },
  ];

  if (detailsData.partialUsers.length > 0) {
    tabsToShow.push({
      id: 3,
      name: 'Match Details',
      component: (
        <DedupeMatchDetails
          dispatch={dispatch}
          appId={appId}
          detailsData={detailsData}
          tabName="Match Details"
        />
      ),
    });
  }

  return (
    <Spin spinning={detailsData.loading || detailsData.loadingPartialMatch} tip={'Loading...'}>
      <DedupeTabs tabPanel={tabsToShow} isNoEntity={isNoEntity}/>
    </Spin>
  );
}

LeadDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};
const withReducer = useInjectReducer({ key: 'leadDetails', reducer });
const withSaga = useInjectSaga({ key: 'leadDetails', saga });

const mapStateToProps = createStructuredSelector({
  detailsData: makeSelectLeadDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(LeadDetails);
