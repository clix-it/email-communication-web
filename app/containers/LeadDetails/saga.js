import { takeEvery, put, takeLatest, select, all } from 'redux-saga/effects';
import _ from 'lodash';
import { push } from 'react-router-redux';
import { message } from 'antd';
import { makeSelectApplications } from '../CreditPortal/selectors';
import * as actions from './constants';
import {
  fetchDetailsError,
  fetchDetailsInit,
  fetchDetailsSuccess,
  fetchUserDetailsSuccess,
  fetchUserDetailsError,
  partialLoader,
  submitDecisionInit,
  submitDecisionError,
  submitDecisionSuccess,
  loadPartialMatchSuccess,
  loadPartialMatchError,
  partialMatchFetchLeadDetailsSuccess,
  partialMatchFetchUserDetailsSuccess,
  fetchCoApplicantDetailsSuccess,
  fetchCoApplicantDetailsInit,
  fetchLoanDetailsInit,
} from './actions';

import env from '../../../environment';
function* fetchDetailsSaga(action) {
  yield put(fetchDetailsInit());
  const { currentApplication } = yield select(makeSelectApplications());

  if (currentApplication) {
    try {
      const result = yield fetch(
        `${env.API_URL}${env.SEARCH_PROFILE}/${
          currentApplication.activityRequestId
        }`,
        {
          method: 'GET',
          headers: env.headers,
        },
      );

      const responseBody = yield result.json();
      // const responseBody = result;

      if (responseBody) {
        if (responseBody.executionStatus === 'SEARCH_PROFILES_SUCCESS') {
          try {
            const userResponse = responseBody.users[0];
            if (userResponse.success) {
              yield put(
                fetchUserDetailsSuccess(
                  userResponse.user,
                  responseBody.matchType,
                  responseBody.requestId,
                ),
              );
              if (responseBody.matchType === 'PARTIAL_MATCH') {
                yield all(
                  responseBody.users.map(function*(userArr, index) {
                    if (index != 0) {
                      yield put(
                        partialMatchFetchUserDetailsSuccess(userArr.user),
                      );
                    }
                  }),
                );
              }
            }
          } catch (err) {
            console.error(err);
            yield put(
              fetchUserDetailsError(
                'Network error occurred! Check connection!',
              ),
            );
            message.error('Network error occurred! Check connection!');
          }
        } else {
          yield put(
            fetchUserDetailsError('Network error occurred! Check connection!'),
          );
          message.error('Network error occurred! Check connection!');
        }
      } else {
        yield put(
          fetchUserDetailsError('Network error occurred! Check connection!'),
        );
        message.error('Network error occurred! Check connection!');
      }
    } catch (err) {
      yield put(
        fetchUserDetailsError('Network error occurred! Check connection!'),
      );
      message.error('Network error occurred! Check connection!');
    }
  } else {
    yield put(push(`/${location.pathname.split('/')[1]}`));
  }
}

function* fetchAppDetailsSaga({ appId }) {
  yield put(fetchDetailsInit());
  const appIdFromSession = sessionStorage
    .getItem('id')
    .replace(/^"(.*)"$/, '$1');
  try {
    const result = yield fetch(
      `${env.APP_SERVICE_API_URL}/${appId || appIdFromSession}`,
      {
        headers: env.headers,
        method: 'GET',
      },
    );

    if (result.status === 200) {
      const responseBody = yield result.json();

      if (responseBody.success || responseBody.status) {
        const company = _.find(
          responseBody.app.users,
          user =>
            (_.get(user, 'appLMS.role') === 'Applicant' ||
              !_.get(user, 'appLMS.role')) &&
            user.type === 'COMPANY',
        );
        const userCuid =
          company && company.cuid
            ? company.cuid
            : responseBody.app.users[0].cuid;
        if (userCuid) {
          const apiresult = yield fetch(
            `${env.USER_SERVICE_API_URL}/${userCuid}`,
            {
              headers: env.headers,
            },
          );
          const responseBody = yield apiresult.json();
          if (responseBody.success === true) {
            yield put(fetchUserDetailsSuccess(responseBody.user, '', ''));
          } else {
            yield put(
              fetchUserDetailsError(
                'Network error occurred! Check connection!',
              ),
            );
            message.error('Network error occurred! Check connection!');
          }
        } else {
          yield put(
            fetchUserDetailsError('Network error occurred! Check connection!'),
          );
          message.error('Network error occurred! Check connection!');
        }
      } else {
        yield put(
          fetchUserDetailsError('Network error occurred! Check connection!'),
        );
        message.error('Network error occurred! Check connection!');
      }
    } else {
      const errorResponse = yield result.json();
      yield put(
        fetchUserDetailsError(
          errorResponse.message || 'Network error occurred! Check connection!',
        ),
      );
      message.error(
        errorResponse.message || 'Network error occurred! Check connection!',
      );
    }
  } catch (err) {
    yield put(
      fetchUserDetailsError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

function* fetchCoApplicantDetailsSaga(action) {
  yield put(fetchCoApplicantDetailsInit());
  const coApplicantDetails = [];
  const userResponseBody = action.userData;
  try {
    if (
      userResponseBody.entityOfficers &&
      userResponseBody.entityOfficers.length > 0
    ) {
      yield all(
        userResponseBody.entityOfficers.map(function*(item) {
          if (item.belongsTo) {
            yield* fetchCoApplicantDetails(item.belongsTo, coApplicantDetails);
          }
        }),
      );
    }
    const coApplicantData = coApplicantDetails.reduce((unique, o) => {
      if (!unique.some(obj => obj.cuid === o.cuid)) {
        unique.push(o);
      }
      return unique;
    }, []);
    yield put(fetchCoApplicantDetailsSuccess(coApplicantData));
  } catch (err) {
    yield put(fetchDetailsError('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}

function* fetchLoanDetailsSaga(action) {
  yield put(fetchLoanDetailsInit());
  const userResponseBody = action.userData;
  try {
    const leadDetails = yield fetch(
      `${env.SELF_SERVICE}/myaccount/${userResponseBody.cuid}`,
      {
        headers: env.headers,
      },
    );
    const responseBody = yield leadDetails.json();
    if (responseBody.success === true) {
      yield put(fetchDetailsSuccess(responseBody.commonLmsResponseList));
    } else {
      yield put(fetchDetailsError('Loan details not available!'));
      message.error('Loan details not available!');
    }
  } catch (err) {
    yield put(fetchDetailsError('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}

export function* fetchCoApplicantDetails(coApplicantCuid, coApplicantDetails) {
  const apiresult = yield fetch(
    `${env.USER_SERVICE_API_URL}/${coApplicantCuid}`,
    {
      headers: env.headers,
    },
  );
  const responseBody = yield apiresult.json();
  if (responseBody.success === true) {
    coApplicantDetails.push(responseBody.user);
  }
}

function* fetchMatchUserLoanDetails(action) {
  const userResponseBody = action.userData;
  try {
    yield all(
      userResponseBody.map(function*(user, index) {
        yield put(partialLoader());
        const apiresult = yield fetch(
          `${env.SELF_SERVICE}/myaccount/${user.cuid}`,
          {
            headers: env.headers,
          },
        );
        const responseBody = yield apiresult.json();
        debugger;
        if (responseBody.success) {
          yield put(
            partialMatchFetchLeadDetailsSuccess(
              responseBody.commonLmsResponseList,
            ),
          );
        } else {
          yield put(
            fetchDetailsError(
              `Loan details not available for ${user.firstName || user.cuid}`,
            ),
          );
          // message.error(`Loan details not available for ${user.firstName || user.cuid}`);
        }
      }),
    );
  } catch (err) {
    yield put(fetchDetailsError('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}

// Individual exports for testing
export default function* leadDetailsSaga() {
  yield takeLatest(actions.FETCH_DETAILS, fetchDetailsSaga);
  yield takeLatest(actions.FETCH_APP_DETAILS, fetchAppDetailsSaga);
  yield takeLatest(
    actions.FETCH_COAPPLICANT_DETAILS,
    fetchCoApplicantDetailsSaga,
  );
  yield takeLatest(actions.FETCH_LOAN_DETAILS, fetchLoanDetailsSaga);
  yield takeLatest(
    actions.FETCH_MATCH_USER_LOAN_DETAILS,
    fetchMatchUserLoanDetails,
  );
}
