import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the leadDetails state domain
 */

const selectLeadDetailsDomain = state => state.leadDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by LeadDetails
 */

const makeSelectLeadDetails = () =>
  createSelector(
    selectLeadDetailsDomain,
    substate => substate,
  );

export default makeSelectLeadDetails;
export { selectLeadDetailsDomain };
