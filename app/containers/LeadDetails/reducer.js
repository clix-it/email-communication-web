import produce from 'immer';
import _ from 'lodash';

import {
  FETCH_DETAILS,
  FETCH_DETAILS_INIT,
  FETCH_DETAILS_SUCCESS,
  FETCH_DETAILS_ERROR,
  FETCH_USER_DETAILS_SUCCESS,
  FETCH_USER_DETAILS_ERROR,
  PARTIAL_MATCH_ERROR,
  PARTIAL_MATCH_SUCCESS,
  PARTIAL_MATCH_USER_DETAILS_SUCCESS,
  PARTIAL_MATCH_LEAD_DETAILS_SUCCESS,
  RESET_STATE,
  PARTIAL_LOADER,
  FETCH_COAPPLICANT_DETAILS_SUCCESS,
  SET_CURRENT_COAPPLICANT,
  SET_CURRENT_LOAN,
  SET_CURRENT_MATCHED_USER,
  SET_CHECKED_USER,
  FETCH_COAPPLICANT_DETAILS_INIT,
  FETCH_LOAN_DETAILS_INIT,
  FETCH_APP_DETAILS,
  SET_CURRENT_MATCH_LOAN,
} from './constants';

export const initialState = {
  loading: false,
  error: false,
  leadId: false,
  response: false,
  dataDetails: [],
  userDetails: {},
  loadingPartialMatch: false,
  partialUsers: [],
  partialLeads: [],
  coApplicantData: [],
  selectedApplicant: false,
  selectedLoan: false,
  selectedMatchedUser: false,
  selectedMatchedUserLoan: false,
  checkedUsers: [],
  matchType: '',
  requestId: '',
};

/* eslint-disable default-case, no-param-reassign */
const appLayoutReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case RESET_STATE: {
        return initialState;
      }
      case FETCH_APP_DETAILS:
        draft.partialUsers = [];
        break;
      case FETCH_DETAILS:
        draft.loading = true;
        draft.errorLoadingData = false;
        draft.leadId = action.appId;
        draft.selectedApplicant = false;
        draft.selectedLoan = false;
        draft.selectedMatchedUserLoan = false;
        draft.selectedMatchedUser = false;
        draft.partialUsers = [];
        draft.partialLeads = [];
        draft.checkedUsers = [];
        break;
      case FETCH_COAPPLICANT_DETAILS_INIT:
        draft.loading = true;
        break;
      case FETCH_LOAN_DETAILS_INIT:
        draft.loading = true;
        break;
      case FETCH_DETAILS_ERROR:
        draft.loadingPartialMatch = false;
        draft.loading = false;
        draft.dataDetails = [];
        draft.errorLoadingData = action.errMessage;

        // 'An Error Occured, Please Try Later';

        // draft.uiStage = action.data.details.uiStage;
        // draft.leadId = action.data.leadId;
        // draft.response = action.data;
        break;

      case PARTIAL_MATCH_ERROR:
        break;

      case PARTIAL_MATCH_SUCCESS:
        break;
      case PARTIAL_LOADER:
        draft.errorLoadingData = false;
        draft.loadingPartialMatch = true;
        break;

      case PARTIAL_MATCH_USER_DETAILS_SUCCESS:
        draft.errorLoadingData = false;
        draft.loadingPartialMatch = false;
        draft.loading = false;
        draft.partialUsers.push(action.partialMatchUserDetails);
        draft.selectedMatchedUser = draft.selectedMatchedUser
          ? _.find(draft.partialUsers, { cuid: draft.selectedMatchedUser.cuid })
          : draft.partialUsers[0];
        break;

      case PARTIAL_MATCH_LEAD_DETAILS_SUCCESS:
        draft.errorLoadingData = false;
        draft.loadingPartialMatch = false;
        draft.loading = false;
        // draft.partialLeadMatch = action.partialMatchLeadDetails;
        draft.partialLeads = draft.partialLeads.length
          ? draft.partialLeads.concat(action.partialMatchLeadDetails)
          : action.partialMatchLeadDetails;
        draft.selectedMatchedUserLoan = draft.selectedMatchedUserLoan
          ? _.find(draft.partialLeads, {
              loanAccountNumber:
                draft.selectedMatchedUserLoan.loanAccountNumber,
            })
          : draft.partialLeads[0];
        break;

      case FETCH_DETAILS_SUCCESS:
        draft.errorLoadingData = false;
        draft.loadingPartialMatch = false;
        draft.loading = false;
        draft.dataDetails = [...action.appDetails];
        draft.selectedLoan = draft.selectedLoan
          ? _.find(draft.dataDetails, {
              loanAccountNumber: draft.selectedLoan.loanAccountNumber,
            })
          : draft.dataDetails[0];
        draft.errorLoadingData = false;
        break;

      case FETCH_USER_DETAILS_ERROR:
        draft.loadingPartialMatch = false;
        draft.loading = false;
        draft.dataDetails = [];
        draft.userDetails = {};
        draft.selectedApplicant = false;
        draft.matchType = '';
        draft.errorLoadingData = action.errMessage;

        break;

      case FETCH_USER_DETAILS_SUCCESS:
        draft.errorLoadingData = false;
        draft.loading = false;
        draft.userDetails = {
          ...action.userDetails,
        };
        draft.matchType = action.matchType;
        draft.requestId = action.requestId;
        draft.errorLoadingData = false;
        break;
      case FETCH_COAPPLICANT_DETAILS_SUCCESS:
        draft.loading = false;
        draft.coApplicantData = action.coApplicantData;
        draft.selectedApplicant = draft.selectedApplicant
          ? _.find(draft.coApplicantData, {
              cuid: draft.selectedApplicant.cuid,
            })
          : draft.coApplicantData[0];
        break;
      case SET_CURRENT_COAPPLICANT:
        draft.selectedApplicant = action.applicant;
        break;
      case SET_CURRENT_LOAN:
        draft.selectedLoan = action.loan;
        break;
      case SET_CURRENT_MATCHED_USER:
        draft.selectedMatchedUser = action.matchedUser;
        break;
      case SET_CHECKED_USER:
        draft.checkedUsers = action.userArr;
        break;
      case SET_CURRENT_MATCH_LOAN:
        draft.selectedMatchedUserLoan = action.loan;
        break;
    }
  });

export default appLayoutReducer;
