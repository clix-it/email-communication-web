/**
 *
 * Asynchronously loads the component for CustomerDocuments
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
