import { put, takeLatest, all, takeEvery, call } from 'redux-saga/effects';
import * as actions from './constants';
import { fetchDocsSuccess, fetchDocsError } from './actions';
import env from '../../../environment';
import request from 'utils/request';

export function* getDocs({ cuid }) {
  try {
    const result = [];
    const reqUrl = `${env.DMS_API_URL}/download`;
    let data = {
      objId: cuid,
      objType: 'user',
      docType: '',
    };
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(data),
    };
    const response = yield call(request, reqUrl, options);
    if (response.status === true) {
      response.data.forEach(obj => {
        if (obj.asset.filename.split('.')[1] !== undefined) {
          result.push({
            obj_type: obj.asset.obj_type,
            obj_id: obj.asset.obj_id,
            type: obj.asset.type,
            signedUrl: obj.signed_url.url,
            fileextension: obj.asset.filename.split('.')[1].toLowerCase(),
          });
        }
      });
    }
    const docsData = result;
    yield put(fetchDocsSuccess(docsData));
  } catch (err) {
    yield put(fetchDocsError(''));
  }
}

export default function* customerDocumentsSaga() {
  yield takeLatest(actions.FETCH_CUSTOMER_DOCS, getDocs);
}
