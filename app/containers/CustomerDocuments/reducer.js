/*
 *
 * LeadDocuments reducer
 *
 */
import produce from 'immer';
import * as actions from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';
import { SET_MAIN_USER } from '../RelationshipDetails/constants';

export const initialState = {
  loading: false,
  error: false,
  docsData: [],
};

const customerDocumentsReducer = (state = initialState, action) =>
  /* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["draft"] }] */
  produce(state, draft => {
    switch (action.type) {
      case actions.FETCH_CUSTOMER_DOCS:
        draft.loading = true;
        break;
      case actions.FETCH_CUSTOMER_DOCS_SUCCESS:
        draft.loading = false;
        draft.docsData = action.data;
        break;
      case actions.FETCH_CUSTOMER_DOCS_ERROR:
        draft.loading = false;
        draft.error = action.message;
        draft.docsData = [];
        break;
      case SET_MAIN_USER:
        draft.docsData = [];
        draft.isFileUploaded = false;
        draft.appV1Docs = [];
        break;
      case LOCATION_CHANGE:
        draft.docsData = [];
        draft.isFileUploaded = false;
        draft.appV1Docs = [];
        break;
      default: {
        return state;
      }
    }
  });

export default customerDocumentsReducer;
