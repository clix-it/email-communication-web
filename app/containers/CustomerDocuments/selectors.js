import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the customerDocuments state domain
 */

const selectCustomerDocumentsDomain = state =>
  state.customerDocuments || initialState;

const makeSelectCustomerDocuments = () =>
  createSelector(
    selectCustomerDocumentsDomain,
    substate => substate,
  );

export { makeSelectCustomerDocuments };
