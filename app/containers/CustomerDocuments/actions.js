/*
 *
 * CustomerDocuments actions
 *
 */

import * as actions from './constants';

export function fetchDocs(cuid) {
  return {
    type: actions.FETCH_CUSTOMER_DOCS,
    cuid,
  };
}

export function fetchDocsSuccess(data) {
  return {
    type: actions.FETCH_CUSTOMER_DOCS_SUCCESS,
    data,
  };
}

export function fetchDocsError(message) {
  return {
    type: actions.FETCH_CUSTOMER_DOCS_ERROR,
    message,
  };
}
