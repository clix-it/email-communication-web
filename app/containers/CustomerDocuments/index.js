/**
 *
 * CustomerDocuments
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import { makeSelectCustomerDocuments } from './selectors';

import CustomerDocList from '../../components/CustomerDocList';

export function CustomerDocuments({
  dispatch,
  customerDocuments,
  user,
  cuid,
}) {

  return (
    <CustomerDocList
      dispatch={dispatch}
      customerDocuments={customerDocuments}
      user={user}
      cuid={cuid}
    />
  );
}

CustomerDocuments.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  customerDocuments: makeSelectCustomerDocuments(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'customerDocuments', reducer });
const withSaga = useInjectSaga({ key: 'customerDocuments', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(CustomerDocuments);
