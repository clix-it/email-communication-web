import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the updateForeclosureAmont state domain
 */

const selectUpdateForeclosureAmontDomain = state =>
  state.updateForeclosureAmont || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by UpdateForeclosureAmont
 */

const makeSelectUpdateForeclosureAmont = () =>
  createSelector(
    selectUpdateForeclosureAmontDomain,
    substate => substate,
  );

export default makeSelectUpdateForeclosureAmont;
export { selectUpdateForeclosureAmontDomain };
