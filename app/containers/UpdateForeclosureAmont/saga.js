import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import env from '../../../environment';
import { UPDATE_FORECLOSURE_AMOUNT } from './constants';
import { amountUppdated, updateAmountFailed } from './actions';

function* updateAmount({applicationId}) {
  try {
    const responseBody = yield call(
      request,
      `${env.DEDUPE_SEARCH}/update/foreclosure/amount/`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({applicationId})
      },
    );

    yield put(amountUppdated(responseBody));
  } catch (err) {
    console.log('err', err)
    yield put(
      updateAmountFailed(err.message),
    );
    message.error(err.message);
  }
}

export default function* updateForeclosureAmontSaga() {
  yield takeEvery(UPDATE_FORECLOSURE_AMOUNT, updateAmount);
}
