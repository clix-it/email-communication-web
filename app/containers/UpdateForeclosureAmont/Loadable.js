/**
 *
 * Asynchronously loads the component for UpdateForeclosureAmont
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
