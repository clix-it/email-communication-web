/*
 *
 * UpdateForeclosureAmont reducer
 *
 */
import produce from 'immer';
import { UPDATE_FORECLOSURE_AMOUNT, UPDATE_FORECLOSURE_AMOUNT_SUCCESS, UPDATE_FORECLOSURE_AMOUNT_FAILURE } from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const updateForeclosureAmontReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case UPDATE_FORECLOSURE_AMOUNT:
        draft.loading = true;
        draft.response = false;
        draft.error = false;
        break;
      case UPDATE_FORECLOSURE_AMOUNT_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        draft.error = false;
        break;
      case UPDATE_FORECLOSURE_AMOUNT_FAILURE:
        draft.loading = false;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default updateForeclosureAmontReducer;
