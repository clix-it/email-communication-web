/*
 *
 * UpdateForeclosureAmont constants
 *
 */

export const UPDATE_FORECLOSURE_AMOUNT = 'app/UpdateForeclosureAmont/UPDATE_FORECLOSURE_AMOUNT';
export const UPDATE_FORECLOSURE_AMOUNT_SUCCESS = 'app/UpdateForeclosureAmont/UPDATE_FORECLOSURE_AMOUNT_SUCCESS';
export const UPDATE_FORECLOSURE_AMOUNT_FAILURE = 'app/UpdateForeclosureAmont/UPDATE_FORECLOSURE_AMOUNT_FAILURE';
