/**
 *
 * UpdateForeclosureAmont
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import {Button} from 'antd'

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectUpdateForeclosureAmont from './selectors';
import reducer from './reducer';
import saga from './saga';
import {updateAmount} from './actions'
export function UpdateForeclosureAmont({appid, updateForeclosureAmont, updateAmount}) {
  useInjectReducer({ key: 'updateForeclosureAmont', reducer });
  useInjectSaga({ key: 'updateForeclosureAmont', saga });

  return <Button
          type="primary"
          onClick={() => updateAmount(appid)}
          loading={updateForeclosureAmont.loading}
        >
          Update Foreclosure Amount
        </Button>;
  }

UpdateForeclosureAmont.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  updateForeclosureAmont: makeSelectUpdateForeclosureAmont(),
});

function mapDispatchToProps(dispatch) {
  return {
    updateAmount: appid => dispatch(updateAmount(appid)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(UpdateForeclosureAmont);
