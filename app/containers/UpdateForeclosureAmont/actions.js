/*
 *
 * UpdateForeclosureAmont actions
 *
 */

import { UPDATE_FORECLOSURE_AMOUNT, UPDATE_FORECLOSURE_AMOUNT_SUCCESS, UPDATE_FORECLOSURE_AMOUNT_FAILURE } from './constants';

export function updateAmount(applicationId) {
  return {
    type: UPDATE_FORECLOSURE_AMOUNT,
    applicationId
  };
}

export function amountUppdated(response) {
  return {
    type: UPDATE_FORECLOSURE_AMOUNT_SUCCESS,
    response,
    payload: {
      entity: response.app
    }
  };
}

export function updateAmountFailed(error) {
  return {
    type: UPDATE_FORECLOSURE_AMOUNT_FAILURE,
    error
  };
}
