/**
 *
 * ForeClosureDetails
 *
 */

import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Typography, Card, Spin } from 'antd';
import { formatToCurrency } from 'utils/helpers';

import UpdateForeclosureAmont from 'containers/UpdateForeclosureAmont/Loadable'

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectForeClosureDetails from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { fetchDetails } from './actions'

const { Text } = Typography;
const gridStyle = {
  width: '25%',
  textAlign: 'left',
};
export function ForeClosureDetails({fetchDetails, foreClosureDetails, lan, appid}) {
  useInjectReducer({ key: 'foreClosureDetails', reducer });
  useInjectSaga({ key: 'foreClosureDetails', saga });
  useEffect(()=> {
    fetchDetails(lan)
  },[])

  const {loading, response, error} = foreClosureDetails;
  if (error) {
    return <></>
  }
  return (
    <Spin spinning={loading} tip="Loading...">
      <h4>{`Foreclosure Details (${lan}) `}</h4>
      <Card title="Loan Overview">
        <Card.Grid style={gridStyle}>
          <Text type="secondary">Customer Name</Text> {response['customer name']}
        </Card.Grid>
        <Card.Grid hoverable={false} style={gridStyle}>
          <Text type="success">PORTFOLIO CODE: </Text>
          {response.portfolio_code}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Amount repaid: </Text>
          {formatToCurrency(response['amount repaid'])}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Foreclosure charges Due: </Text>
          {formatToCurrency(response['foreclosure charges Due'])}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Foreclosure charge wavied: </Text>
          {formatToCurrency(response['foreclosure charge wavied'])}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Date of Foreclosure: </Text> {response['date of foreclosure']}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Remarks for foreclosure: </Text> {response['remarks for foreclosure']}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Account Status: </Text> {response['account status']}
        </Card.Grid>
        <Card.Grid style={gridStyle}>
          <Text type="success">Additional Status: </Text>
          {response['additional status']}
        </Card.Grid>
      </Card>
    </Spin>
  );
}

ForeClosureDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  foreClosureDetails: makeSelectForeClosureDetails(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchDetails: lan => dispatch(fetchDetails(lan)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(ForeClosureDetails);
