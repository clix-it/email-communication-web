/*
 *
 * ForeClosureDetails reducer
 *
 */
import produce from 'immer';
import { FETCH_FORECLOSURE_DETAILS, FETCH_FORECLOSURE_DETAILS_FAILURE, FETCH_FORECLOSURE_DETAILS_SUCCESS } from './constants';

export const initialState = {
  loading: true,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const foreClosureDetailsReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_FORECLOSURE_DETAILS:
        draft.loading = true;
        draft.response = false;
        draft.error = false
        break;
      case FETCH_FORECLOSURE_DETAILS_SUCCESS:
        draft.loading = false;
        draft.response = action.response.foreclosureDetails[0];
        draft.error = false;
        break;
      case FETCH_FORECLOSURE_DETAILS_FAILURE:
        draft.loading = false;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default foreClosureDetailsReducer;
