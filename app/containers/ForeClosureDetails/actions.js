/*
 *
 * ForeClosureDetails actions
 *
 */

import { FETCH_FORECLOSURE_DETAILS, FETCH_FORECLOSURE_DETAILS_SUCCESS, FETCH_FORECLOSURE_DETAILS_FAILURE } from './constants';

export function fetchDetails(lan) {
  return {
    type: FETCH_FORECLOSURE_DETAILS,
    lan
  };
}

export function detailsFetched(response) {
  return {
    type: FETCH_FORECLOSURE_DETAILS_SUCCESS,
    response
  };
}

export function fetchDetailsFailed(error) {
  return {
    type: FETCH_FORECLOSURE_DETAILS_FAILURE,
    error
  };
}
