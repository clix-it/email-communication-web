/*
 * ForeClosureDetails Messages
 *
 * This contains all the text for the ForeClosureDetails container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ForeClosureDetails';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ForeClosureDetails container!',
  },
});
