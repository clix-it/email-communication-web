import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the foreClosureDetails state domain
 */

const selectForeClosureDetailsDomain = state =>
  state.foreClosureDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ForeClosureDetails
 */

const makeSelectForeClosureDetails = () =>
  createSelector(
    selectForeClosureDetailsDomain,
    substate => substate,
  );

export default makeSelectForeClosureDetails;
export { selectForeClosureDetailsDomain };
