import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import env from '../../../environment';
import { FETCH_FORECLOSURE_DETAILS } from './constants';
import { detailsFetched, fetchDetailsFailed } from './actions';

function* fetchLanDetails(action) {
  try {
    const responseBody = yield call(
      request,
      `${env.DEDUPE_SEARCH}/indus/foreclosure/details/?lan=${action.lan}&lms=${
        action.lms
      }`,
      {
        method: 'GET',
        headers: env.headers,
      },
    );

    // const responseBody = yield result.json();
    // const responseBody = data;
    if(responseBody.foreclosureDetails.length) {
      yield put(detailsFetched(responseBody));
    } else {
      message.error(`${action.lan} not closed yet`);
      yield put(fetchDetailsFailed(`${action.lan} not closed yet`))
    }
  } catch (err) {
    console.log("error", err)
    yield put(
      fetchDetailsFailed('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

export default function* foreClosureDetailsSaga() {
  yield takeEvery(FETCH_FORECLOSURE_DETAILS, fetchLanDetails);
}
