/*
 *
 * ForeClosureDetails constants
 *
 */

export const FETCH_FORECLOSURE_DETAILS = 'app/ForeClosureDetails/FETCH_FORECLOSURE_DETAILS';
export const FETCH_FORECLOSURE_DETAILS_SUCCESS = 'app/ForeClosureDetails/FETCH_FORECLOSURE_DETAILS_SUCCESS';
export const FETCH_FORECLOSURE_DETAILS_FAILURE = 'app/ForeClosureDetails/FETCH_FORECLOSURE_DETAILS_FAILURE';
