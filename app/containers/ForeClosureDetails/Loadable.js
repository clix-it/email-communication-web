/**
 *
 * Asynchronously loads the component for ForeClosureDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
