/**
 *
 * FinancialAnalysisReport
 *
 */
import _ from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import styled from 'styled-components';

import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { createStructuredSelector } from 'reselect';
import reducer from './reducer';
import saga from './saga';
import makeSelectFinancialAnalysisReport from './selectors';
import docSvg from '../../images/document.svg';

import { getPerfiosReport, FinancialAnalysisDoc } from './actions';
import { Button, List, Avatar, Space, Spin, Typography, Empty } from 'antd';

const Wrapper = styled.section`
  background: #fff;
  margin: 0 1em;
  margin-top: 2em;
`;

const ListWrapper = styled.div`
  height: 20em;
  overflow: auto;
`;

const { Text } = Typography;

export function FinancialAnalysisReport({
  financialAnalysisReport,
  dispatch,
  appId,
}) {
  const [visible, setVisible] = useState(false);
  const [currentItem, setCurrentItem] = useState({});
  const [existingDocs, setExistingDocs] = useState([]);
  const { docsData = [], loading = true } = financialAnalysisReport;

  useEffect(() => {
    if (appId) {
      dispatch(getPerfiosReport(appId || ''));
      dispatch(FinancialAnalysisDoc());
    }
    return () => {
      setExistingDocs([]);
    };
  }, [appId]);

  useEffect(() => {
    if (docsData.length > 0) {
      setExistingDocs(docsData);
    }
  }, [docsData]);

  const refreshData = () => {
    dispatch(getPerfiosReport(appId || ''));
    dispatch(FinancialAnalysisDoc());
  };

  const setDescription = item => {
    return <Text>App</Text>;
  };

  const onClose = () => {
    setCurrentItem({});
    setVisible(false);
  };

  return (
    <>
      <Button type="primary" onClick={refreshData}>
        Refresh
      </Button>
      {existingDocs.length == 0 && (
        <Spin spinning={loading} tip={'Loading...'}>
          <Empty />
        </Spin>
      )}
      {existingDocs.length > 0 && (
        <Spin spinning={loading} tip={'Loading...'}>
          <Wrapper>
            <Space>
              <h5 style={{ fontWeight: 'bold' }}>Document</h5>
            </Space>
            <ListWrapper>
              <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                size="small"
                bordered
                dataSource={existingDocs}
                renderItem={(item, index) => (
                  <>
                    <List.Item
                      actions={[
                        <a key={index} href={item.signedUrl} target="_blank">
                          Download
                        </a>,
                      ]}
                    >
                      <List.Item.Meta
                        avatar={<Avatar shape="square" src={docSvg} />}
                        title={<Text>{item.type}</Text>}
                        description={setDescription(item)}
                      />
                    </List.Item>
                  </>
                )}
              />
            </ListWrapper>
          </Wrapper>
        </Spin>
      )}
    </>
  );
}

FinancialAnalysisReport.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  financialAnalysisReport: makeSelectFinancialAnalysisReport(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);
const withReducer = useInjectReducer({
  key: 'financialAnalysisReport',
  reducer,
});
const withSaga = useInjectSaga({ key: 'financialAnalysisReport', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(FinancialAnalysisReport);
