import _ from 'lodash';
import moment from 'moment';
import { takeEvery, put, call, select, all } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import {
  FinancialAnalysisDocSuccess,
  FinancialAnalysisDocError,
} from './actions';
import makeSelectApplicantDetails from '../ApplicantDetails/selectors';

export function* getDocs() {
  const result = [];
  const applicantDetails = yield select(makeSelectApplicantDetails());
  try {
    const reqUrl = `${env.APP_DOCS_API_URL}/download`;
    const cuid = _.get(applicantDetails, 'entity.cuid');
    let body = {
      objId: cuid,
      objType: 'user',
      docType: '',
    };
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(body),
    };
    const response = yield call(request, reqUrl, options);
    if (response.status === true) {
      response.data.forEach(obj => {
        if (
          obj.asset.filename.split('.')[1] !== undefined &&
          obj.asset.type.toLowerCase() === 'financial statement analysis report'
        ) {
          result.push({
            obj_type: obj.asset.obj_type,
            obj_id: obj.asset.obj_id,
            type: obj.asset.type,
            signedUrl: obj.signed_url.url,
            fileextension: obj.asset.filename.split('.')[1].toLowerCase(),
          });
        }
      });
    }
    if (result.length > 0) {
      yield put(FinancialAnalysisDocSuccess(result));
    } else {
      yield put(FinancialAnalysisDocError());
    }
  } catch (err) {
    yield put(FinancialAnalysisDocError());
  }
}

export function* getReport({ appId }) {
  const reqUrl = `${
    env.API_URL
  }/sa/v1/reportfile?loanApplicationId=${appId}&reportType=xls&forBusinessLoan=true`;
  const options = {
    method: 'GET',
    headers: env.headers,
  };
  let allResponses = [];
  try {
    const report = yield call(request, reqUrl, options);
    const applicantDetails = yield select(makeSelectApplicantDetails());
    const cuid = _.get(applicantDetails, 'entity.cuid');
    debugger;
    if (report.success && report.applicantCodeDocDataMap) {
      if (Object.keys(report.applicantCodeDocDataMap).length > 0) {
        let responses = Object.keys(report.applicantCodeDocDataMap).map(
          item => {
            if (
              report.applicantCodeDocDataMap[item] &&
              report.applicantCodeDocDataMap[item].fileStream &&
              report.applicantCodeDocDataMap[item].transactionType ==
                'FINANCESTATEMENT'
            ) {
              const payload = {
                objId: cuid,
                objType: 'user',
                docType: 'Financial Statement Analysis Report',
                fileName: `Financial Statement Analysis Report.xlsx`,
                fileAttributes: {},
                files: [report.applicantCodeDocDataMap[item].fileStream],
              };
              const options = {
                method: 'POST',
                headers: env.headers,
                body: JSON.stringify(payload),
              };
              let promise = request(
                `${env.API_URL}/app-docs/v2/upload`,
                options,
              );
              return promise;
            }
          },
        );
        allResponses = yield Promise.all(responses);
        let arr = allResponses.filter(item => item);
        debugger;
        if(arr.length > 0){
          yield* getDocs();
        }
      }
    }
  } catch (error) {
    message.error('No transaction found with the given Application Id!');
  }
}

function* actionWatcher() {
  yield takeEvery(actions.FETCH_FINANCIAL_ANALYSIS_DOC, getDocs);
  yield takeEvery(actions.GET_FINANCIAL_REPORT, getReport);
}

export default function* financialAnalysisReportSaga() {
  yield all([actionWatcher()]);
}
