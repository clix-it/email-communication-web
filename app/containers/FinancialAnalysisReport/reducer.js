/**
 * Financial Analysis Report Reducer
 */
import * as actions from './constants';
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';

export const initialState = {
  loading: true,
  docsData: [],
  error: false,
};

const financialAnalysisReportReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.FETCH_FINANCIAL_ANALYSIS_DOC:
        draft.loading = true;
        draft.error = false;
        break;
      case actions.FETCH_FINANCIAL_ANALYSIS_DOC_SUCCESS:
        draft.loading = false;
        draft.docsData = action.data;
        draft.error = false;
        break;
      case actions.FETCH_FINANCIAL_ANALYSIS_DOC_ERROR:
        draft.loading = false;
        draft.docsData = [];
        break;
      case LOCATION_CHANGE:
        draft.docsData = [];
        draft.loading = false;
        break;
      default: {
        return state;
      }
    }
  });

export default financialAnalysisReportReducer;
