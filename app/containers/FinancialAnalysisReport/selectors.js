import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the FinancialAnalysisReport state domain
 */

const selectFinancialAnalysisReportDomain = state =>
  state.financialAnalysisReport || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by FinancialAnalysisReport
 */

const makeSelectFinancialAnalysisReport = () =>
  createSelector(
    selectFinancialAnalysisReportDomain,
    substate => substate,
  );

export default makeSelectFinancialAnalysisReport;
export { selectFinancialAnalysisReportDomain };
