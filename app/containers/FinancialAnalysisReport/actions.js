/*
 *
 * FinancialAnalysisReport actions
 *
 */

import * as actions from './constants';

export function getPerfiosReport(appId) {
  return {
    type: actions.GET_FINANCIAL_REPORT,
    appId,
  };
}

export function getPerfiosReportSuccess(response) {
  return {
    type: actions.GET_FINANCIAL_REPORT_SUCCESS,
    response,
  };
}

export function getPerfiosReportError(error) {
  return {
    type: actions.GET_FINANCIAL_REPORT_ERROR,
    error,
  };
}

export function FinancialAnalysisDoc(appId) {
  return {
    type: actions.FETCH_FINANCIAL_ANALYSIS_DOC,
    appId,
  };
}

export function FinancialAnalysisDocSuccess(data) {
  return {
    type: actions.FETCH_FINANCIAL_ANALYSIS_DOC_SUCCESS,
    data,
  };
}

export function FinancialAnalysisDocError() {
  return {
    type: actions.FETCH_FINANCIAL_ANALYSIS_DOC_ERROR,
  };
}