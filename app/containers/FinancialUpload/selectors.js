import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the financialUpload state domain
 */

const selectFinancialUploadDomain = state =>
  state.financialUpload || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by FinancialUpload
 */

const makeSelectFinancialUpload = () =>
  createSelector(
    selectFinancialUploadDomain,
    substate => substate,
  );

export default makeSelectFinancialUpload;
export { selectFinancialUploadDomain };
