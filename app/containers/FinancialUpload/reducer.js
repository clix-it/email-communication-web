/*
 *
 * FinancialUpload reducer
 *
 */
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';
import {
  GET_PERFIOS_REPORT,
  GET_PERFIOS_REPORT_SUCCESS,
  GET_PERFIOS_REPORT_ERROR,
  START_PERFIOS,
  START_PERFIOS_SUCCESS,
  START_PERFIOS_ERROR,
  FETCH_PERFIOS_BANKS,
  FETCH_PERFIOS_BANKS_SUCCESS,
  FETCH_PERFIOS_BANKS_ERROR,
  SET_FILEDATA,
  GET_PERFIOS_STATUS,
  GET_PERFIOS_STATUS_SUCCESS,
  GET_PERFIOS_STATUS_ERROR,
} from './constants';
export const initialState = {
  loading: false,
  response: false,
  error: false,
  bankList: false,
  perfiosStatus: [],
  perfiosAbbreviations: {},
};

const abbreviations = {
  CTD: 'Transaction created',
  FAIL: 'Transaction failed',
  CNCD: 'Connected data for Analysis initiation',
  UPLD: 'All documents uploaded for analysis',
  NBCT: 'Transaction created for netbanking',
  NBF: 'Transaction for netbanking failed',
  NBC: 'Transaction for netbanking completed',
  CNBC: 'Client completed the netbanking',
};

/* eslint-disable default-case, no-param-reassign */
const financialUploadReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case GET_PERFIOS_REPORT:
      case START_PERFIOS:
        draft.loading = true;
        break;
      case GET_PERFIOS_REPORT_SUCCESS:
      case START_PERFIOS_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        break;
      case GET_PERFIOS_REPORT_ERROR:
      case START_PERFIOS_ERROR:
        draft.loading = false;
        draft.error = action.error;
        break;
      case FETCH_PERFIOS_BANKS:
        break;
      case FETCH_PERFIOS_BANKS_SUCCESS:
        draft.bankList = action.response.institutionNameCodeMapping;
        draft.error = false;
        break;
      case FETCH_PERFIOS_BANKS_ERROR:
        draft.error = action.error;
        break;
      case SET_FILEDATA:
        draft.fileData = action.payload;
        break;
      case GET_PERFIOS_STATUS:
        draft.loading = true;
        draft.perfiosAbbreviations = { ...abbreviations };
        break;
      case GET_PERFIOS_STATUS_SUCCESS:
        draft.loading = false;
        draft.perfiosStatus = action.response;
        break;
      case GET_PERFIOS_STATUS_ERROR:
        draft.loading = false;
        draft.perfiosStatus = [];
        break;
      case LOCATION_CHANGE:
        draft.perfiosStatus = [];
        break;
    }
  });

export default financialUploadReducer;
