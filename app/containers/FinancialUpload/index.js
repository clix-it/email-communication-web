/**
 *
 * FinancialUpload
 *
 */

import _ from 'lodash';
import moment from 'moment';
import React, { memo, useEffect, useState } from 'react';
import cookies from 'react-cookies';

import PropTypes from 'prop-types';

import {
  Row,
  Col,
  Button,
  List,
  Form,
  Upload,
  DatePicker,
  Select,
  Typography,
  Anchor,
  Radio,
  Input,
  Space,
  Divider,
  Tooltip,
  InputNumber,
  Table,
} from 'antd';
import { useForm, Controller } from 'react-hook-form';

import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import {
  UploadOutlined,
  CloseOutlined,
  EyeInvisibleOutlined,
  EyeTwoTone,
} from '@ant-design/icons';
import { compose } from 'redux';

import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectFinancialUpload from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import {
  getPerfiosReport,
  startPerfios,
  fetchPerfiosBanks,
  setFileData,
  getPerfiosStatus,
} from './actions';
import InputField from '../../components/InputField';
import makeSelectApplicantDetails, {
  makeSelectApplicantDetails2,
} from '../ApplicantDetails/selectors';
// import { isDirtyForm } from '../ApplicantDetails/actions';
// import Title from 'antd/lib/skeleton/Title';
import { isDirtyForm } from '../ApplicantDetails/actions';
const { Option } = Select;
const { Link } = Anchor;
const { Title } = Typography;
// import Title from 'antd/lib/skeleton/Title';

const { RangePicker } = DatePicker;

const columnResponsiveLayout = {
  colSingle: { xs: 24, sm: 24, md: 24, lg: 24, xl: 24 },
  colHalf: { xs: 24, sm: 24, md: 24, lg: 12, xl: 10 },
  colOneThird: { xs: 24, sm: 24, md: 24, lg: 8, xl: 8 },
};
export function FinancialUpload({
  loanOffers,
  appId,
  registeredName,
  getPerfiosReport,
  startPerfios,
  dispatch,
  financialUpload,
  applicantDetails,
  applicantDetails2,
  isNoEntity,
  type,
}) {
  const {
    entity
  } = applicantDetails2

  useInjectReducer({ key: 'financialUpload', reducer });
  useInjectSaga({ key: 'financialUpload', saga });

  const { perfiosStatus = [] } = financialUpload;

  const columnData = [
    {
      title: 'Bank',
      dataIndex: 'institutionCode',
    },
    {
      title: 'From',
      dataIndex: 'yearMonthFrom',
    },
    {
      title: 'To',
      dataIndex: 'yearMonthTo',
    },
    {
      title: 'Status',
      dataIndex: 'status',
    },
    {
      title: 'Operation',
      render: (item, record) => {
        if (item.statusCode == 'FAIL' || item.statusCode == 'NBF') {
          return null;
        } else {
          return (
            <Button
              type="primary"
              htmlType="button"
              className="entityDetailsButton"
              onClick={() =>
                getPerfiosReport({
                  applicantCode: item.applicantCode,
                  appId,
                  product: entity.product,
                })
              }
            >
              Fetch Report
            </Button>
          );
        }
      },
    },
  ];

  const {
    errors,
    control,
    watch,
    handleSubmit,
    reset,
    setValue,
    formState,
  } = useForm({
    mode: 'onChange',
  });
  useEffect(() => {
    dispatch(isDirtyForm({ 4: formState.dirty }));
  }, [formState.dirty]);
  const values = watch();
  console.log('Financial Upload Values', values);

  const [isPassword, setIsPassword] = useState(false);
  const [cuidArr, setCuidArr] = useState([]);

  useEffect(() => {
    if (applicantDetails2 && applicantDetails2.entity) {
      const users = _.get(applicantDetails2, 'entity.users') || [];
      if (users && users.length > 0) {
        let arr = users
          .filter(item => item.userLinked)
          .map(item => {
            return {
              cuid: item.cuid,
              name: item.registeredName
                ? item.registeredName
                : `${item.firstName} ${item.lastName}`,
            };
          });
        setCuidArr(arr);
      } else {
        setCuidArr([]);
      }
    }
  }, []);

  const options = [
    { label: 'Yes', value: true },
    { label: 'No', value: false },
  ];

  useEffect(() => {
    // Fetch Perfios Banks List
    if (!financialUpload.bankList) {
      dispatch(fetchPerfiosBanks());
    }
    dispatch(getPerfiosStatus(appId));
    // dispatch(fetchPerfiosBanks());
  }, []);

  function getBankOptionList() {
    const banksList = [];
    banksList.push(
      <Option disabled value selected key="select your bank">
        Select your Salary Bank
      </Option>,
    );
    for (const [key, value] of Object.entries(financialUpload.bankList)) {
      // console.log(key, value);
      banksList.push(
        <Option key={key} value={value}>
          {key}
        </Option>,
      );
    }
    return banksList;
  }

  function getAccountOptionList() {
    const accountList = [];
    const accountTypes = ['CC', 'OD', 'None'];
    accountList.push(
      <Option disabled value selected key="Select your facility type">
        Select your Facility Type
      </Option>,
    );
    accountTypes.map(item =>
      accountList.push(
        <Option key={item} value={item}>
          {item}
        </Option>,
      ),
    );
    return accountList;
  }

  const handleFileUpload = file => {
    const data = {
      file,
    };
    const reader = new FileReader();
    reader.readAsDataURL(data.file.file);
    reader.onload = () => {
      // console.log('Reader result', reader.result);
      const fileData = {
        name: file.file.name,
        fileStream: reader.result,
      };
      dispatch(setFileData(fileData));
    };
  };

  function removeFile(fileIndex) {
    // If only 1 file is removed then reset the form else splice the array
    if (values.bankStatement.fileList.length === 1) {
      reset({
        bankStatement: null,
        dateRange: values.dateRange,
        bankName: values.bankName,
      });
      //  document.getElementById('bankStatement')
      return;
    }
    const temp = _.remove(
      values.bankStatement.fileList,
      (item, index) => index !== fileIndex,
    );
    const newFileList = {
      fileList: temp,
    };
    setValue('bankStatement', newFileList);
  }

  const onSubmit = data => {
    console.log('data', data);
    // Send for perfios
    const fromDate = moment(data.dateRange[0]).format('YYYY-MM');
    const toDate = moment(data.dateRange[1]).format('YYYY-MM');

    const uploadData = {
      appId,
      transactionCode: '',
      source: 'LOS-CLIX',
      facility: !isNoEntity ? data.accountType.toUpperCase() : '',
      applicant: {
        cuid: _.get(applicantDetails, 'entity.cuid') || '', // Entity cuid ?
        applicantName:
          _.get(applicantDetails, 'entity.type') == 'INDIVIDUAL'
            ? `${_.get(applicantDetails, 'entity.firstName')} ${_.get(
                applicantDetails,
                'entity.lastName',
              )}`
            : _.get(applicantDetails, 'entity.registeredName'), // registered name?
        emailId: _.get(applicantDetails, 'entity.preferredEmail') || '', // entityEmail
        employementType: 'SELF_EMPLOYED', // ? kamlesh ?--
        employerName: '', // ? kamlesh?--
        pan: _.get(applicantDetails, 'entity.identities.pan'),
        sisterCompanyNames: [],
        companyNames: [],
      },
      loanDetails: {
        loanAmount:
          _.get(
            _.filter(
              loanOffers,
              loanOffer =>
                loanOffer.type === 'credit_amount' ||
                loanOffer.type === 'eligible_amount',
            ),
            '[0].loanAmount',
          ) || '100001',
        loanDuration: '48',
        loanType:
          _.get(applicantDetails, 'entity.type') == 'INDIVIDUAL'
            ? 'PERSONAL_LOAN'
            : 'BUSINESS_LOAN',
        sanctionLimitFixed: false,
        sanctionLimitFixedAmount: '',
        sanctionLimitVariableAmounts: data.sanctionLimit
          ? [data.sanctionLimit.toString()]
          : [],

        drawingPowerVariableAmounts:
          data.drawingPower && data.accountType === 'CC'
            ? [data.drawingPower.toString()]
            : [],
      },
      bankDetail: [
        {
          yearMonthFrom: fromDate,
          yearMonthTo: toDate,
          correlationId: '',
          institutionCode: data.bankName,
          scannedStatement: false,
          documentObjIdList: [],
        },
      ],
    };
    startPerfios({
      payload: uploadData,
      password: data.password || '',
      appId,
      fileList: data.bankStatement.fileList,
      cuid: data.cuid,
    });
    // uploadStatemnet({ uploadData, password: data.password });
    setTimeout(() => {
      reset({
        bankName: '',
        dateRange: '',
        accountType: '',
        sanctionLimit: '',
        drawingPower: '',
        bankStatement: '',
        cuid: '',
      });
    }, 1000);
  };

  return (
    <>
      <Title level={4}>Bank Statement Analyser</Title>
      <form>
        <Row gutter={[16, 24]}>
          {/* <form onSubmit={handleSubmit}> */}
          <Col {...columnResponsiveLayout.colHalf}>
            <Form.Item label="Select Bank*">
              <Controller
                // disabled={disabled}
                name="bankName"
                control={control}
                rules={{
                  required: true,
                }}
                // onChange={([value]) => handleChange2(value)}
                as={
                  <Select
                    showSearch
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                    size="large"
                    placeholder="Select Bank"
                  >
                    {getBankOptionList()}
                  </Select>
                }
              />
              <ErrorMessage name="bankName" errors={errors} />
            </Form.Item>
          </Col>
          <Col {...columnResponsiveLayout.colHalf} className="entityStyleCol">
            <Form.Item
              label="Select Financial Year*"
              className="entityFormStyle"
            >
              <Controller
                placeholder="Select Financial Year"
                type="range-picker"
                id="dateRange"
                name="dateRange"
                control={control}
                rules={{
                  required: true,
                }}
                as={<RangePicker size="large" picker="month" />}
              />
              <ErrorMessage name="dateRange" errors={errors} />
            </Form.Item>
          </Col>
          <Col {...columnResponsiveLayout.colHalf} className="entityStyleCol">
            <Form.Item label="Select User*" className="entityFormStyle">
              <Controller
                placeholder="Select User"
                type="range-picker"
                id="cuid"
                name="cuid"
                control={control}
                rules={{
                  required: true,
                }}
                as={
                  <Select
                    showSearch
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                    size="large"
                    placeholder="Select User"
                  >
                    {cuidArr &&
                      cuidArr.map((item, index) => {
                        return (
                          <Option key={index} value={item.cuid}>
                            {item.cuid}-{item.name}
                          </Option>
                        );
                      })}
                  </Select>
                }
              />
              <ErrorMessage name="dateRange" errors={errors} />
            </Form.Item>
          </Col>
          {!isNoEntity && (
            <>
              <Col {...columnResponsiveLayout.colHalf}>
                <Form.Item label="Select Facility Type*">
                  <Controller
                    // disabled={disabled}
                    name="accountType"
                    control={control}
                    rules={{
                      required: true,
                    }}
                    // onChange={([value]) => handleChange2(value)}
                    as={
                      <Select
                        showSearch
                        filterOption={(input, option) =>
                          option.children
                            .toLowerCase()
                            .indexOf(input.toLowerCase()) >= 0
                        }
                        size="large"
                        placeholder="Select Facility Type"
                      >
                        {getAccountOptionList()}
                      </Select>
                    }
                  />
                  <ErrorMessage name="accountType" errors={errors} />
                </Form.Item>
              </Col>
              <Col
                {...columnResponsiveLayout.colHalf}
                // style={
                //   values.accountType === 'OD' || values.accountType === 'CC'
                //     ? {}
                //     : { display: 'none' }
                // }
              >
                <Form.Item label="Enter Sanction Limit Amount">
                  <Controller
                    // disabled={disabled}
                    name="sanctionLimit"
                    control={control}
                    rules={{
                      required: !!(
                        values.accountType === 'OD' ||
                        values.accountType === 'CC'
                      ),
                    }}
                    // onChange={([value]) => handleChange2(value)}
                    as={
                      <InputNumber
                        size="large"
                        min={1}
                        disabled={values.accountType === 'None'}
                        // formatter={value =>
                        //   `₹​ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                        // }
                        // parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        //  onChange={onChange}
                      />
                    }
                  />
                  <ErrorMessage name="sanctionLimit" errors={errors} />
                </Form.Item>
              </Col>
              <Col
                {...columnResponsiveLayout.colHalf}
                // style={values.accountType === 'CC' ? {} : { display: 'none' }}
              >
                <Form.Item label="Enter Drawing Power Amount">
                  <Controller
                    // disabled={disabled}
                    name="drawingPower"
                    control={control}
                    rules={{
                      required: values.accountType === 'CC',
                    }}
                    // onChange={([value]) => handleChange2(value)}
                    as={
                      <InputNumber
                        size="large"
                        min={1}
                        disabled={
                          values.accountType === 'None' ||
                          values.accountType === 'OD'
                        }
                        // formatter={value =>
                        //   `₹​ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                        // }
                        // parser={value => value.replace(/\$\s?|(,*)/g, '')}
                        //  onChange={onChange}
                      />
                    }
                  />
                  <ErrorMessage name="drawingPower" errors={errors} />
                </Form.Item>
              </Col>
            </>
          )}
          <Col
            {...columnResponsiveLayout.colOneThird}
            className="entityStyleCol"
          >
            <Form.Item
              label="Select Bank Statement*"
              className="entityFormStyle"
            >
              <Controller
                placeholder="Select Bank Statement"
                type="file"
                id="bankStatement"
                name="bankStatement"
                control={control}
                rules={{
                  required: true,
                }}
                as={
                  <Upload
                    accept=".pdf"
                    showUploadList={false}
                    multiple
                    fileList={
                      values.bankStatement
                        ? [...values.bankStatement.fileList]
                        : []
                    }
                    // customRequest={handleFileUpload}
                  >
                    <Button>
                      <UploadOutlined /> Choose File
                    </Button>
                  </Upload>
                }
              />
              <ErrorMessage name="bankStatement" errors={errors} />
            </Form.Item>
          </Col>
          {values.bankStatement &&
          values.bankStatement.hasOwnProperty('fileList') ? (
            <>
              <Col {...columnResponsiveLayout.colOneThird}>
                <Form.Item label="File Name">
                  {values.bankStatement.fileList.map((item, index) => (
                    <Col
                      {...columnResponsiveLayout.colSingle}
                      style={{ margin: '10px 0' }}
                    >
                      <Tooltip title="Remove file">
                        <a
                          style={{ color: 'black' }}
                          onClick={() => {
                            // Remove file
                            removeFile(index);
                          }}
                        >
                          {' '}
                          {item.name}
                        </a>
                        <CloseOutlined
                          onClick={() => {
                            // Remove file
                            removeFile(index);
                          }}
                          style={{
                            fontSize: '20px',
                            verticalAlign: 0,
                            marginLeft: '0.225em',
                          }}
                        />
                      </Tooltip>
                    </Col>
                  ))}
                </Form.Item>
              </Col>
              <Col {...columnResponsiveLayout.colOneThird}>
                <Form.Item label="Is File Password Protected?">
                  {/* {values.bankStatement.fileList.map((item, index) => ( */}
                  <>
                    <Radio.Group
                      options={options}
                      value={isPassword}
                      onChange={e => {
                        e.preventDefault();
                        setIsPassword(e.target.value);
                      }}
                    />
                    {isPassword ? (
                      <div style={{ padding: '1em', paddingLeft: '0' }}>
                        <Controller
                          name="password"
                          control={control}
                          rules={{
                            required: !!isPassword,
                          }}
                          as={
                            <Space direction="vertical">
                              <Input.Password
                                placeholder="Enter Password"
                                iconRender={visible =>
                                  visible ? (
                                    <EyeTwoTone />
                                  ) : (
                                    <EyeInvisibleOutlined />
                                  )
                                }
                              />
                            </Space>
                          }
                        />
                        <ErrorMessage name="password" errors={errors} />
                      </div>
                    ) : (
                      <> </>
                    )}
                  </>
                  {/* ))} */}
                </Form.Item>
              </Col>
            </>
          ) : (
            <> </>
          )}
          <Divider />
          <Col {...columnResponsiveLayout.colOneThird}>
            <Button
              type="primary"
              htmlType="button"
              className="entityDetailsButton"
              onClick={handleSubmit(onSubmit)}
            >
              Send to Perfios
            </Button>
          </Col>

          {/* </form> */}
          <Divider />
        </Row>
      </form>
      {perfiosStatus.length > 0 && (
        <>
          <Row gutter={[16, 24]}>
            <Col
              {...columnResponsiveLayout.colSingle}
              className="personalStyleCol"
            >
              <b>Perfios Analysis Status </b>
            </Col>
          </Row>
          <Table bordered dataSource={perfiosStatus} columns={columnData} />
        </>
      )}
    </>
  );
}

FinancialUpload.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  financialUpload: makeSelectFinancialUpload(),
  applicantDetails: makeSelectApplicantDetails(),
  applicantDetails2: makeSelectApplicantDetails2(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getPerfiosReport: payload => dispatch(getPerfiosReport(payload)),
    startPerfios: payload => dispatch(startPerfios(payload)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(FinancialUpload);
