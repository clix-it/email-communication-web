/*
 *
 * FinancialUpload constants
 *
 */

export const GET_PERFIOS_REPORT = 'app/FinancialUpload/GET_PERFIOS_REPORT';
export const GET_PERFIOS_REPORT_SUCCESS =
  'app/FinancialUpload/GET_PERFIOS_REPORT_SUCCESS';
export const GET_PERFIOS_REPORT_ERROR =
  'app/FinancialUpload/GET_PERFIOS_REPORT_ERROR';

export const START_PERFIOS = 'app/FinancialUpload/START_PERFIOS';
export const START_PERFIOS_SUCCESS =
  'app/FinancialUpload/START_PERFIOS_SUCCESS';
export const START_PERFIOS_ERROR = 'app/FinancialUpload/START_PERFIOS_ERROR';

export const FETCH_PERFIOS_BANKS = 'app/FinancialUpload/FETCH_PERFIOS_BANKS';
export const FETCH_PERFIOS_BANKS_SUCCESS =
  'app/FinancialUpload/FETCH_PERFIOS_BANKS_SUCCESS';
export const FETCH_PERFIOS_BANKS_ERROR =
  'app/FinancialUpload/FETCH_PERFIOS_BANKS_ERROR';

export const SET_FILEDATA = 'app/FinancialUpload/SET_FILEDATA';

export const GET_PERFIOS_STATUS = 'GET_PERFIOS_STATUS';
export const GET_PERFIOS_STATUS_SUCCESS = 'GET_PERFIOS_STATUS_SUCCESS';
export const GET_PERFIOS_STATUS_ERROR = 'GET_PERFIOS_STATUS_ERROR';
