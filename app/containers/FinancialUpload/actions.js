/*
 *
 * FinancialUpload actions
 *
 */

import {
  START_PERFIOS,
  START_PERFIOS_SUCCESS,
  START_PERFIOS_ERROR,
  GET_PERFIOS_REPORT,
  GET_PERFIOS_REPORT_SUCCESS,
  GET_PERFIOS_REPORT_ERROR,
  FETCH_PERFIOS_BANKS,
  FETCH_PERFIOS_BANKS_SUCCESS,
  FETCH_PERFIOS_BANKS_ERROR,
  SET_FILEDATA,
  GET_PERFIOS_STATUS,
  GET_PERFIOS_STATUS_SUCCESS,
  GET_PERFIOS_STATUS_ERROR,
} from './constants';

export function getPerfiosReport(payload) {
  return {
    type: GET_PERFIOS_REPORT,
    payload,
  };
}

export function getPerfiosReportSuccess(response) {
  return {
    type: GET_PERFIOS_REPORT_SUCCESS,
    response,
  };
}

export function getPerfiosReportError(error) {
  return {
    type: GET_PERFIOS_REPORT_ERROR,
    error,
  };
}

export function startPerfios({ payload, password, appId, fileList, cuid }) {
  return {
    type: START_PERFIOS,
    payload,
    password,
    appId,
    fileList,
    cuid,
  };
}

export function startPerfiosSuccess(response) {
  return {
    type: START_PERFIOS_SUCCESS,
    response,
  };
}

export function startPerfiosError(error) {
  return {
    type: START_PERFIOS_ERROR,
    error,
  };
}

export function fetchPerfiosBanks() {
  return {
    type: FETCH_PERFIOS_BANKS,
  };
}

export function fetchPerfiosBanksSuccess(response) {
  return {
    type: FETCH_PERFIOS_BANKS_SUCCESS,
    response,
  };
}
export function fetchPerfiosBanksError(error) {
  return {
    type: FETCH_PERFIOS_BANKS_ERROR,
    error,
  };
}

export function setFileData(payload) {
  return {
    type: SET_FILEDATA,
    payload,
  };
}

export function getPerfiosStatus(appId) {
  return {
    type: GET_PERFIOS_STATUS,
    appId,
  };
}

export function getPerfiosStatusSuccess(response) {
  return {
    type: GET_PERFIOS_STATUS_SUCCESS,
    response,
  };
}
export function getPerfiosStatusError(error) {
  return {
    type: GET_PERFIOS_STATUS_ERROR,
    error,
  };
}
