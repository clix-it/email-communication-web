/**
 *
 * Asynchronously loads the component for FinancialUpload
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
