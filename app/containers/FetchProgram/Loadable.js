/**
 *
 * Asynchronously loads the component for FetchProgram
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
