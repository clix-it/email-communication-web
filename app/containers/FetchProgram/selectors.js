import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the fetchProgram state domain
 */

const selectFetchProgramDomain = state => state.fetchProgram || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by FetchProgram
 */

const makeSelectFetchProgram = () =>
  createSelector(
    selectFetchProgramDomain,
    substate => substate,
  );

export default makeSelectFetchProgram;
export { selectFetchProgramDomain };
