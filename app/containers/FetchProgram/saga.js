import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import env from '../../../environment';
import { FETCH_PROGRAM } from './constants';
import { programFetched, programFetchingError } from './actions';

export function* fetchProgram({ partner, product, cmCode, programcode }) {
  let url = '';
  if (partner)
    url = `${
      env.MASTER_URL
    }?masterType=${programcode}_${partner}_${product}&cmCode=${cmCode || ''}`;
  url = `${env.MASTER_URL}?masterType=${programcode}&cmCode=${cmCode || ''}`;
  try {
    const responseBody = yield call(request, url, {
      method: 'GET',
      headers: env.headers,
    });

    // const responseBody = yield result.json();
    // const responseBody = data;
    yield put(programFetched(responseBody));
    return responseBody;
  } catch (err) {
    yield put(
      programFetchingError('Network error occurred! Check connection!'),
    );
    message.error('Network error occurred! Check connection!');
  }
}

export default function* fetchProgramSaga() {
  yield takeEvery(FETCH_PROGRAM, fetchProgram);
}
