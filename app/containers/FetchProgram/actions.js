/*
 *
 * FetchProgram actions
 *
 */

import {
  FETCH_PROGRAM,
  FETCH_PROGRAM_SUCCESS,
  FETCH_PROGRAM_FAILED,
} from './constants';

export function fetchprogram(payload) {
  return {
    type: FETCH_PROGRAM,
    ...payload,
  };
}

export function programFetched(response) {
  return {
    type: FETCH_PROGRAM_SUCCESS,
    response,
  };
}

export function programFetchingError(error) {
  return {
    type: FETCH_PROGRAM_FAILED,
    error,
  };
}
