/*
 *
 * FetchProgram constants
 *
 */

export const FETCH_PROGRAM = 'app/FetchProgram/FETCH_PROGRAM';
export const FETCH_PROGRAM_SUCCESS = 'app/FetchProgram/FETCH_PROGRAM_SUCCESS';
export const FETCH_PROGRAM_FAILED = 'app/FetchProgram/FETCH_PROGRAM_FAILED';
