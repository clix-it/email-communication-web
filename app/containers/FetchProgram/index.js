/**
 *
 * FetchProgram
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectFetchProgram from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchprogram as getProgram } from './actions';
import _ from 'lodash';

export function FetchProgram({
  product,
  partner,
  dispatch,
  children,
  programcode,
  cmCode,
  fetchProgramData,
}) {
  useInjectReducer({ key: 'fetchProgram', reducer });
  useInjectSaga({ key: 'fetchProgram', saga });

  useEffect(() => {
    if (product && partner)
      dispatch(getProgram({ product, partner, cmCode, programcode }));
  }, [product, partner, cmCode, programcode]);

  return React.cloneElement(children, fetchProgramData);
}

FetchProgram.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  fetchProgramData: makeSelectFetchProgram(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(FetchProgram);
