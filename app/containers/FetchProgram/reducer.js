/*
 *
 * FetchProgram reducer
 *
 */
import produce from 'immer';
import {
  FETCH_PROGRAM,
  FETCH_PROGRAM_SUCCESS,
  FETCH_PROGRAM_FAILED,
} from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const fetchProgramReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_PROGRAM:
        draft.loading = true;
        draft.product = action.product;
        draft.partner = action.partner;
        break;
      case FETCH_PROGRAM_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        break;
      case FETCH_PROGRAM_FAILED:
        draft.loading = false;
        draft.error = action.error;
        draft.response = false;
        break;
    }
  });

export default fetchProgramReducer;
