import { createSelector } from 'reselect';

const selectRemarks = state => state.remarks;

const makeSelectRemarks = () =>
  createSelector(
    selectRemarks,
    remarks => remarks,
  );

export { makeSelectRemarks };
