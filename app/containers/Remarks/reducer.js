/**
 * Credit Portal Reducer
 */
import * as actions from './constants';

export const initialState = {
  loading: false,
  remarks: [],
  error: {},
};

const remarksReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOAD_REMARKS:
      return { ...state, loading: true };

    case actions.LOAD_REMARKS_SUCCESS:
      return {
        ...state,
        loading: false,
        error: { status: false, message: '' },
        remarks: action.payload.data,
      };

    case actions.LOAD_REMARKS_ERROR: {
      return {
        ...state,
        loading: false,
        remarks: [],
        error: {
          status: true,
          message: action.payload.errorMessage,
          severity: action.severity,
        },
      };
    }
    default: {
      return state;
    }
  }
};

export default remarksReducer;
