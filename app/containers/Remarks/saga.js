import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import data from './data.json';
import env from '../../../environment';
import * as actions from './constants';
import { loadRemarksSuccess, loadRemarksError } from './actions';

function* fetchRemarks(action) {
  try {
    const responseBody = yield call(
      request,
      `${env.REMARK_URL}/?appid=${action.payload.appId}`,
      {
        method: 'GET',
      },
    );

    // const responseBody = yield result.json();
    // const responseBody = data;
    if (responseBody && responseBody.length > 0) {
      yield put(loadRemarksSuccess(responseBody));
    } else {
      yield put(loadRemarksSuccess([]));
      message.info('Oops! No Remarks found :( ');
    }
  } catch (err) {
    yield put(loadRemarksError('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}

export default function* remarksSaga() {
  yield takeEvery(actions.LOAD_REMARKS, fetchRemarks);
}
