import React, { useEffect, useState, memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import { Table, Spin, Tag } from 'antd';
import reducer from './reducer';
import saga from './saga';

import { loadRemarks } from './actions';
import { makeSelectRemarks } from './selectors';

const STATUS_WISE_COLOR = {
  active: '#108ee9',
  approved: '#87d068',
  rejected: '#f50',
  send_back: '#FFFF66',
};

function Remarks({ remarks, dispatch, appId }) {
  useInjectReducer({ key: 'remarks', reducer });
  useInjectSaga({ key: 'remarks', saga });

  const [dataSource, setDataSource] = useState([]);

  useEffect(() => {
    dispatch(loadRemarks(appId));
  }, [appId]);

  useEffect(() => {
    setDataSource(remarks.remarks);
  }, [remarks]);

  const columns = [
    {
      title: 'Activity',
      dataIndex: 'user_activity',
      key: 'user_activity',
      fixed: 'left',
    },
    {
      title: 'Next Actor',
      dataIndex: 'next_actor',
      key: 'next_actor',
    },
    {
      title: 'State',
      dataIndex: 'state',
      key: 'state',
      render: (text, record) => (
        <Tag color={STATUS_WISE_COLOR[text.toLowerCase()]}>{text}</Tag>
      ),
    },
    {
      title: 'Remarks',
      dataIndex: 'remark',
      key: 'remark',
      width: '30%',
    },
    {
      title: 'Created At',
      dataIndex: 'created_at',
      key: 'created_at',
    },
    {
      title: 'Updated At',
      dataIndex: 'updated_at',
      key: 'updated_at',
    },
    {
      title: 'Approver',
      dataIndex: 'email',
      key: 'email',
      fixed: 'right',
    },
  ];

  const locale = {
    emptyText: 'No Data Found',
  };

  return (
    <Spin spinning={remarks.loading} tip="Loading...">
      <h4>History</h4>
      <Table
        locale={locale}
        dataSource={dataSource || []}
        pagination={{ defaultPageSize: 10, showSizeChanger: true }}
        columns={columns}
        scroll={{ x: 1300 }}
      />
    </Spin>
  );
}

Remarks.propTypes = {
  remarks: PropTypes.arrayOf(PropTypes.object),
  dispatch: PropTypes.func.isRequired,
  appId: PropTypes.string,
};

Remarks.defaultProps = {
  remarks: [],
  appId: '',
};

const mapStateToProps = createStructuredSelector({
  remarks: makeSelectRemarks(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Remarks);
