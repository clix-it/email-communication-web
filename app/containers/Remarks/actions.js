import * as actions from './constants';
export function loadRemarks(appId) {
  return {
    type: actions.LOAD_REMARKS,
    payload: {
      appId,
    },
  };
}

export function loadRemarksSuccess(data) {
  return {
    type: actions.LOAD_REMARKS_SUCCESS,
    payload: {
      data,
    },
  };
}

export function loadRemarksError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_REMARKS_ERROR,
    payload: {
      errorMessage,
      severity,
    },
  };
}
