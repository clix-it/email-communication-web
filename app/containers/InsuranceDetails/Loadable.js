/**
 *
 * Asynchronously loads the component for InsuranceDetails
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
