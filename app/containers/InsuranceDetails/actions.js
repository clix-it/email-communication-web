/*
 *
 * InsuranceDetails actions
 *
 */

import * as actions from './constants';

export function updateInsuranceDetails() {
  return {
    type: actions.DEFAULT_ACTION,
  };
}

export function InsuranceDoc(appId) {
  return {
    type: actions.FETCH_INSURANCE_DOC,
    appId,
  };
}

export function InsuranceDocSuccess(data) {
  return {
    type: actions.FETCH_INSURANCE_DOC_SUCCESS,
    data,
  };
}

export function InsuranceDocError() {
  return {
    type: actions.FETCH_INSURANCE_DOC_ERROR,
  };
}

export function setSelectedInsurance(insurance) {
  return {
    type: actions.SET_SELECTED_INSURANCE,
    insurance,
  };
}
export function setInsurances(insurances) {
  return {
    type: actions.SET_INSURANCES,
    insurances,
  };
}

export function addInsurance(insurance) {
  return {
    type: actions.ADD_INSURANCE,
    insurance,
  };
}

export function saveInsuranceSuccess(success) {
  return {
    type: actions.SAVE_INSURANCE_SUCCESS,
    success,
  };
}

export function saveInsurance(data) {
  return {
    type: actions.SAVE_INSURANCE,
    data,
  };
}

export function generateInsurance(data) {
  return {
    type: actions.GENERATE_INSURANCE,
    data,
  };
}

export function fetchPlan(data) {
  return {
    type: actions.FETCH_PLAN,
    data,
  };
}

export function fetchPlanSuccess(data) {
  return {
    type: actions.FETCH_PLAN_SUCCESS,
    data,
  };
}

export function calculatePremium(data) {
  return {
    type: actions.CALCULATE_PREMIUM,
    data,
  };
}

export function calculatePremiumSuccess(data) {
  return {
    type: actions.CALCULATE_PREMIUM_SUCCESS,
    data,
  };
}

export function resetState() {
  return {
    type: actions.RESET_STATE,
  };
}

export function generateInsuranceFormSuccess(data) {
  return {
    type: actions.GENERATE_INSURANCE_SUCCESS,
    data,
  };
}

export function generateInsuranceFormError(error) {
  return {
    type: actions.GENERATE_INSURANCE_ERROR,
    error,
  };
}
