import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the insuranceDetails state domain
 */

const selectInsuranceDetailsDomain = state =>
  state.insuranceDetails || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by InsuranceDetails
 */

const makeSelectInsuranceDetails = () =>
  createSelector(
    selectInsuranceDetailsDomain,
    substate => substate,
  );

export { makeSelectInsuranceDetails };
