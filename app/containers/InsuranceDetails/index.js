import React, { memo, useEffect } from 'react';
import { Menu, Spin, Button, message, Empty } from 'antd';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectInsuranceDetails } from './selectors';

import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import {
  setSelectedInsurance,
  addInsurance,
  setInsurances,
  generateInsurance,
  saveInsurance,
  fetchPlan,
  calculatePremium,
  resetState,
} from './actions';
import InsuranceForm from '../../components/InsuranceForm';
import { NMLGrid } from './constants';
import { makeSelectHealthQuestions } from '../HealthQuestions/selectors';
import { getAnswers } from '../HealthQuestions/actions';

function InsuranceDetails({
  dispatch,
  appId,
  applicantDetails,
  requestType,
  insuranceDetails,
  healthQuestions,
}) {
  const {
    entity: { entityCuid, appInsurance: appInsurancePL = {} },
    selectedApplicant = {},
    corpInsurances = [],
    loading,
    entity: { users = [] } = {},
  } = applicantDetails;
  const {
    corpAppInsurances = [],
    selectedInsurance,
    docsData,
    insurancePlan,
    insuranceForms = [],
    premiumData = {},
  } = insuranceDetails;

  const { allAnswers = [] } = healthQuestions;

  useEffect(() => {
    dispatch(setInsurances(corpInsurances));
    if (corpInsurances && corpInsurances.length > 0)
      dispatch(setSelectedInsurance(corpInsurances[0]));
    else dispatch(setSelectedInsurance({}));
    dispatch(getAnswers());
  }, [_.get(applicantDetails, 'corpInsurances'), appId]);

  useEffect(() => {
    // dispatch(resetState());
    if (_.get(applicantDetails, 'entity.product', '') !== 'PL') return;
    dispatch(setInsurances(corpInsurances));
    if (Object.keys(appInsurancePL).length > 0) {
      dispatch(setInsurances([appInsurancePL]));
    }
    if (Object.keys(appInsurancePL).length > 0)
      dispatch(setSelectedInsurance(appInsurancePL));
    else dispatch(setSelectedInsurance({}));
    dispatch(getAnswers());
  }, [_.get(applicantDetails, 'entity.appInsurance'), appId]);

  useEffect(() => {
    if (
      _.get(applicantDetails, 'entity.product', '') === 'BL' ||
      _.get(applicantDetails, 'entity.product', '') === 'HFS'
    )
      dispatch(
        fetchPlan({
          product: _.get(applicantDetails, 'entity.product', ''),
          partner: _.get(applicantDetails, 'entity.partner', ''),
        }),
      );
  }, []);
  const setInsurance = id => {
    // reset({ 'applicants[0].middleName': '' });
    const insurance =
      _.find(corpAppInsurances, { id: id.key }) ||
      _.find(corpAppInsurances, { id: parseInt(id.key) });
    if (insurance) dispatch(setSelectedInsurance(insurance));
  };

  const addNewInsurance = () => {
    if (
      selectedInsurance &&
      String(_.get(selectedInsurance, 'id', '')).includes('NEW')
    ) {
      message.info('Please save the current info before moving ahead!');
      return;
    }
    // corpAppInsurances;
    const applicants = users.filter(user => user.type !== 'COMPANY');
    const checker = applicants.filter(
      item =>
        !corpAppInsurances.some(resultItem => resultItem.cuid === item.cuid),
    );
    if (checker && checker.length === 0) {
      message.info('All the applicants have insurances!');
      return;
    }
    dispatch(addInsurance());
  };

  const submitInsurance = data => {
    const finalLoanAmount = _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),

      '[0].loanAmount',
    );
    const applicant = _.find(_.get(applicantDetails, 'entity.applicants'), {
      cuid: _.get(data, 'appInsurances[0].insuranceForApplicant'),
    });
    const age = _.get(applicant, 'dateOfBirthIncorporation');
    const ageDiff = moment().diff(moment(age, 'DD-MM-YYYY'), 'years');
    const medicalRequired = checkNMLLimit(
      parseInt(finalLoanAmount, 10),
      ageDiff,
    );
    if (medicalRequired) {
      message.info('Please ensure medicals are conducted for this user!');
    }
    console.log('data to save in insurance', data);
    console.log(
      'data to save in insurance of health questions',
      healthQuestions.questions,
    );
    const users = [
      {
        ...selectedApplicant,
        cropInsurance: data,
        userDetails: {
          ..._.get(selectedApplicant, 'userDetails'),
          height: _.get(data, 'height'),
          weight: _.get(data, 'weight'),
        },
      },
    ];
    debugger;
    dispatch(saveInsurance({ users }));
  };

  const checkNMLLimit = (finalLoanAmount, ageDiff) => {
    let result = false;
    NMLGrid.forEach(ele => {
      if (
        ageDiff >= ele.start &&
        ageDiff <= ele.end &&
        finalLoanAmount >= ele.amount
      ) {
        result = true;
      }
    });
    return result;
  };

  console.log('selectedInsurance', selectedInsurance);

  return (
    <Spin spinning={loading} tip="Loading...">
      <div className="App">
        <Menu
          onClick={id => setInsurance(id)}
          defaultSelectedKeys={[(selectedInsurance || {}).id]}
          selectedKeys={[String((selectedInsurance || {}).id)]}
          mode="horizontal"
          overflowedIndicator
        >
          {corpAppInsurances.length > 0 &&
            _.map(corpAppInsurances, (appInsurance, index) => (
              <Menu.Item key={String(appInsurance.id)}>
                {`Insurance ${index + 1}`}
              </Menu.Item>
            ))}
          {requestType === 'PMA' &&
            _.get(applicantDetails, 'entity.product', '') === 'BL' &&
            false && (
              <Menu.Item key="add">
                <Button type="link" onClick={() => addNewInsurance()}>
                  + Insurance
                </Button>
              </Menu.Item>
            )}
        </Menu>
        {_.get(selectedInsurance, 'id') ? (
          <InsuranceForm
            dispatch={dispatch}
            appId={appId}
            requestType={requestType}
            selectedInsurance={selectedInsurance}
            applicantDetails={applicantDetails}
            entityCuid={entityCuid}
            applicants={users.filter(user => user.type !== 'COMPANY')}
            submitInsurance={submitInsurance}
            generateInsurance={generateInsurance}
            docsData={docsData}
            corpInsurances={corpInsurances}
            insurancePlan={insurancePlan}
            calculatePremium={calculatePremium}
            allAnswers={allAnswers}
            insuranceForms={insuranceForms}
            premiumData={premiumData}
          />
        ) : (
          <Empty />
        )}
      </div>
    </Spin>
  );
}

InsuranceDetails.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  insuranceDetails: makeSelectInsuranceDetails(),
  healthQuestions: makeSelectHealthQuestions(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'insuranceDetails', reducer });
const withSaga = useInjectSaga({ key: 'insuranceDetails', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(InsuranceDetails);
