import { put, takeLatest, call } from 'redux-saga/effects';
import _ from 'lodash';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import {
  InsuranceDocSuccess,
  InsuranceDocError,
  saveInsuranceSuccess,
  fetchPlanSuccess,
  calculatePremiumSuccess,
  generateInsuranceFormSuccess,
  generateInsuranceFormError,
} from './actions';
import { updateApplication } from '../App/saga';
import { fetchEntityDetailsSaga } from '../ApplicantDetails/saga';
import env from '../../../environment';
import data from './premium.json';

export function* getDocs({ appId }) {
  const result = [];
  try {
    const reqUrl = `${env.APP_DOCS_API_URL}/download`;
    const body = {
      objId: appId,
      objType: 'app',
      docType: '',
    };
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(body),
    };
    const response = yield call(request, reqUrl, options);
    if (response.status === true) {
      response.data.forEach(obj => {
        if (
          obj.asset.filename.split('.')[1] !== undefined &&
          obj.asset.type === 'Insurance Form'
        ) {
          result.push({
            obj_type: obj.asset.obj_type,
            obj_id: obj.asset.obj_id,
            type: obj.asset.type,
            signedUrl: obj.signed_url.url,
            fileextension: obj.asset.filename.split('.')[1].toLowerCase(),
          });
        }
      });
    }
    if (result.length > 0) {
      yield put(InsuranceDocSuccess(result));
    } else {
      yield put(InsuranceDocError());
    }
  } catch (err) {
    yield put(InsuranceDocError());
  }
}

function* generateInsuranceForm() {
  try {
    const reqUrl = `${env.API_URL}/${env.INSURANCE_DOC_GEN}?appId=${JSON.parse(
      sessionStorage.getItem('id'),
    )}`;
    const options = {
      method: 'GET',
      headers: env.headers,
    };
    const response = yield call(request, reqUrl, options);
    if (response.status === true || response.success) {
      yield put(
        generateInsuranceFormSuccess(_.get(response, 'signed_urls', [])),
      );
    } else {
      yield put(generateInsuranceFormError('Error Occured !'));
      message.info('Sorry, Insurance form could not be generated!:(');
    }
  } catch (err) {
    yield put(
      generateInsuranceFormError('Network error occurred! Check connection!'),
    );
    message.info('Network error occurred! Check connection!');
  }
}

function* fetchPlan(action) {
  const { data: { partner, product } = {} } = action;
  const requestURL = `${
    env.MASTER_URL
  }?masterType=INSURANCEPLAN_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchPlanSuccess(response.masterData || []));
    } else {
      yield message.info('Master Data not found');
    }
  } catch (err) {
    yield message.error('Master Data not found');
  }
}

function* saveInsurance(action) {
  try {
    const result = yield updateApplication(
      action.data,
      JSON.parse(sessionStorage.getItem('id')),
    );
    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.success || responseBody.status) {
        message.success('App updated Successfully!!');
        yield put(saveInsuranceSuccess(responseBody));
      } else {
        yield put(InsuranceDocError('App could not be Updated !'));
        message.info('App could not be Updated!');
      }
    } else {
      const errorResponse = yield result.json();
      yield put(
        InsuranceDocError(
          errorResponse.message ||
            `HTTP Status${result.status} occurred.Try Again`,
        ),
      );
      message.info(
        errorResponse.message ||
          `HTTP Status${result.status} occurred.Try Again`,
      );
    }
  } catch (err) {
    yield put(InsuranceDocError('Network error occurred! Check connection!'));
    message.info('Network error occurred! Check connection!');
  }
}

function* calculatePremium(action) {
  const { data: { cuid, term, benefitOption } = {} } = action;
  const requestURL = `${env.API_URL}/${env.INSURANCE_CALC}/insurance`;
  const body = {
    cuid,
    appId: JSON.parse(sessionStorage.getItem('id')),
    term,
    benefitOption,
  };
  const options = {
    method: 'POST',
    headers: env.headers,
    body: JSON.stringify(body),
  };
  try {
    const response = yield call(request, requestURL, options);
    // const response = data;
    if (response && response.premiumSummary) {
      yield put(calculatePremiumSuccess(_.get(response, 'premiumSummary', {})));
    } else {
      yield message.info('Premium could not be calculated!');
    }
  } catch (err) {
    yield message.error('Network error occurred! Check connection!');
  }
}

export default function* insuranceDetailsSaga() {
  yield takeLatest(actions.FETCH_INSURANCE_DOC, getDocs);
  yield takeLatest(actions.GENERATE_INSURANCE, generateInsuranceForm);
  yield takeLatest(actions.SAVE_INSURANCE, saveInsurance);
  yield takeLatest(actions.SAVE_INSURANCE_SUCCESS, fetchEntityDetailsSaga);
  yield takeLatest(actions.FETCH_PLAN, fetchPlan);
  yield takeLatest(actions.CALCULATE_PREMIUM, calculatePremium);
}
