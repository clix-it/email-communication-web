import { BorderAll } from '@material-ui/icons';
/*
 *
 * insuranceDetailsReducer reducer
 *
 */
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';
import * as actions from './constants';

const newInsuranceBluPrint = {
  cuid: '',
  coverageTerm: '',
  nominee1_DateOfBirth: '',
  nominee1_Gender: '',
  nominee1_firstName: '',
  nominee1_mobile: '',
  nominee1_nomineeRelation: '',
  premiumAmount: '',
  sumAssured: '',
};

export const initialState = {
  loading: false,
  docsData: [],
  error: false,
  selectedInsurance: {},
  corpAppInsurances: {},
  insurancePlan: [],
  insuranceForms: [],
  premiumData: {},
};

const insuranceDetailsReducer = (state = initialState, action) =>
  /* eslint no-param-reassign: ["error", { "props": true, "ignorePropertyModificationsFor": ["draft"] }] */
  produce(state, draft => {
    switch (action.type) {
      case actions.FETCH_INSURANCE_DOC:
        draft.loading = true;
        draft.error = false;
        break;
      case actions.FETCH_INSURANCE_DOC_SUCCESS:
        draft.loading = false;
        draft.docsData = action.data;
        draft.error = false;
        break;
      case actions.FETCH_INSURANCE_DOC_ERROR:
        draft.loading = false;
        draft.docsData = [];
        break;
      case LOCATION_CHANGE:
        draft.docsData = [];
        draft.loading = false;
        break;
      case actions.SET_SELECTED_INSURANCE: {
        draft.selectedInsurance = action.insurance;
        draft.premiumData = {};
        break;
      }
      case actions.SET_INSURANCES: {
        draft.corpAppInsurances = action.insurances;
        draft.premiumData = {};
        // draft.selectedInsurance = action.insurances;
        break;
      }
      case actions.ADD_INSURANCE:
        {
          const newInsurance = {
            ...newInsuranceBluPrint,
            id: `NEW-${draft.corpAppInsurances.length}`,
          };
          draft.corpAppInsurances = [...draft.corpAppInsurances, newInsurance];
          draft.selectedInsurance = newInsurance;
          draft.premiumData = {};
        }
        break;
      case actions.ADD_INSURANCE_SUCCESS:
        draft.loading = false;
        draft.error = false;
        break;
      case actions.FETCH_PLAN_SUCCESS:
        draft.insurancePlan = action.data;
        draft.error = false;
        break;
      case actions.CALCULATE_PREMIUM_SUCCESS:
        draft.premiumData = action.data;
        break;
      case actions.GENERATE_INSURANCE_SUCCESS:
        draft.insuranceForms = action.data;
        break;
      case actions.GENERATE_INSURANCE_ERROR:
        draft.insuranceForms = [];
        break;
      case actions.RESET_STATE:
        return initialState;
      default: {
        return state;
      }
    }
  });

export default insuranceDetailsReducer;
