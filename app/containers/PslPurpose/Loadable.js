/**
 *
 * Asynchronously loads the component for PslEndUse
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
