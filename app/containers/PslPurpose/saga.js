import { call, put, takeEvery } from 'redux-saga/effects';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import { FETCH_PSL_PURPOSE } from './constants';
import { pslPurposeFetched, pslPurposeError } from './actions';
import env from '../../../environment';
// Individual exports for testing

export function* fetchPslPurpose() {
  const requestURL = `${env.MASTER_DATA_URL}/pslpurpose`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });

    if (response && response.length > 0) {
      yield put(pslPurposeFetched(response));
    } else {
      yield put(pslPurposeError('Master Data not found'));
    }
  } catch (err) {
    const error = err && err.error;
    yield put(pslPurposeError('Master Data not found'));
    message.error(
      (error && error.message) || 'Master Data for PSL Purpose not found',
    );
  }
}

export default function* pslPurposeSaga() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(FETCH_PSL_PURPOSE, fetchPslPurpose);
}
