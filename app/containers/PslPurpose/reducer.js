/*
 *
 * PslPurpose reducer
 *
 */
import _ from 'lodash';
import produce from 'immer';
import {
  FETCH_PSL_PURPOSE,
  FETCH_PSL_PURPOSE_FAILED,
  FETCH_PSL_PURPOSE_SUCCESS,
} from './constants';

export const initialState = {
  loading: false,
  response: false,
  error: false,
};

/* eslint-disable default-case, no-param-reassign */
const pslPurposeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_PSL_PURPOSE:
        draft.loading = true;
        draft.error = false;
        break;
      case FETCH_PSL_PURPOSE_SUCCESS: {
        draft.loading = true;
        const temp = [];
        action.response.forEach(item => {
          temp.push({ code: item.code, value: item.description });
        });
        draft.response = temp;
        break;
      }
      case FETCH_PSL_PURPOSE_FAILED:
        draft.loading = true;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default pslPurposeReducer;
