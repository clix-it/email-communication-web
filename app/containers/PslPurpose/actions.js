/*
 *
 * PslPurpose actions
 *
 */

import {
  FETCH_PSL_PURPOSE,
  FETCH_PSL_PURPOSE_SUCCESS,
  FETCH_PSL_PURPOSE_FAILED,
} from './constants';

export function fetchPslPurpose() {
  return {
    type: FETCH_PSL_PURPOSE,
  };
}

export function pslPurposeFetched(response) {
  return {
    type: FETCH_PSL_PURPOSE_SUCCESS,
    response,
  };
}

export function pslPurposeError(error) {
  return {
    type: FETCH_PSL_PURPOSE_FAILED,
    error,
  };
}
