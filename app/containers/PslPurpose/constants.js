/*
 *
 * PslPurpose constants
 *
 */

export const FETCH_PSL_PURPOSE = 'app/PslPurpose/FETCH_PSL_PURPOSE';
export const FETCH_PSL_PURPOSE_SUCCESS =
  'app/PslPurpose/FETCH_PSL_PURPOSE_SUCCESS';
export const FETCH_PSL_PURPOSE_FAILED =
  'app/PslPurpose/FETCH_PSL_PURPOSE_FAILED';
