/**
 *
 * PslEndUse
 *
 */

import _ from 'lodash';
import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import InputField from 'components/InputField';
import { Controller } from 'react-hook-form';
import { Select, Form } from 'antd';
import ErrorMessage from 'components/ErrorMessage';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectPslPurpose from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchPslPurpose } from './actions';

const { Option } = Select;

export function PslPurpose({
  pslPurpose,
  getPslPurpose,
  values,
  errors,
  setError,
  clearError,
  setValue,
  name,
  ...rest
}) {
  useInjectReducer({ key: 'pslPurpose', reducer });
  useInjectSaga({ key: 'pslPurpose', saga });

  useEffect(() => {
    getPslPurpose();
  }, []);

  const { response = [], error } = pslPurpose;

  return (
    <Form.Item label="PSL Purpose">
      <Controller
        name={name}
        rules={{
          required: {
            value: false,
            message: 'This field cannot be left empty',
          },
        }}
        showSearch
        onChange={([value]) => value}
        {...rest}
        as={
          <Select size="large" placeholder="PSL Purpose">
            {response &&
              response.length > 0 &&
              response.map(item => (
                <Option value={item.code}>{item.value}</Option>
              ))}
          </Select>
        }
      />
      <ErrorMessage name={name} errors={errors} />
    </Form.Item>
  );
}

PslPurpose.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  pslPurpose: makeSelectPslPurpose(),
});

function mapDispatchToProps(dispatch) {
  return {
    getPslPurpose: () => dispatch(fetchPslPurpose()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(PslPurpose);
