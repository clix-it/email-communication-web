import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pslPurpose state domain
 */

const selectPslPurposeDomain = state => state.pslPurpose || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by pslPurpose
 */

const makeSelectPslPurposeInput = () =>
  createSelector(
    selectPslPurposeDomain,
    substate => substate,
  );

export default makeSelectPslPurposeInput;
export { selectPslPurposeDomain };
