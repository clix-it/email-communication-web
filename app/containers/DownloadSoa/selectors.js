import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the downloadSoa state domain
 */

const selectDownloadSoaDomain = state => state.downloadSoa || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by DownloadSoa
 */

const makeSelectDownloadSoa = () =>
  createSelector(
    selectDownloadSoaDomain,
    substate => substate,
  );

export default makeSelectDownloadSoa;
export { selectDownloadSoaDomain };
