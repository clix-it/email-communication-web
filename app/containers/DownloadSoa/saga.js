import { call, put, select, takeEvery } from 'redux-saga/effects';
import { soaFetched, soaFetchingError } from './actions';
import { FETCH_SOA } from './constants';
import moment from 'moment';
import env from '../../../environment';
import request from 'utils/request';
import { downloadFile } from '../../utils/helpers';

/**
 * Github repos request/response handler
 */
export function* fetchSoa({
  params: { lms, product, partner, ...rest },
  callFrom,
}) {
  const requestURL = `${
    env.API_URL
  }/lms_integrator/v2/getSOA?product=${product}&partner=${partner}&lms=${lms}`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(rest),
    });
    /*yield downloadFile(
      response.data.documentBase64,
      `account-statement-${moment().format('YYYYMMDDkkmm')}.pdf`,
    );*/
    if (response && response.success && callFrom == 'download') {
      var url = response.data.signedUrls[0].url;
      var element = document.createElement('a');
      element.setAttribute('href', url);
      element.setAttribute('target', '_blank');
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
    }
    yield put(soaFetched(response));
    // );
  } catch (err) {
    yield put(soaFetchingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* downloadSoaSaga() {
  // Watches for FETCH SOA actions and calls fetchSoa when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeEvery(FETCH_SOA, fetchSoa);
}
