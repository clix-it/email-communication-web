/*
 * DownloadSoa Messages
 *
 * This contains all the text for the DownloadSoa container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.DownloadSoa';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DownloadSoa container!',
  },
});
