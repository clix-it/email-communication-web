/*
 *
 * DownloadSoa reducer
 *
 */
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';
import { FETCH_SOA, FETCH_SOA_SUCCESS, FETCH_SOA_FAILURE } from './constants';

export const initialState = {
  loading: false,
  error: false,
  response: false,
};

/* eslint-disable default-case, no-param-reassign */
const downloadSoaReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_SOA:
        draft.loading = true;
        draft.params = action.params;
        draft.error = false;
        break;
      case FETCH_SOA_SUCCESS:
        draft.loading = false;
        draft.response = action.response;
        draft.error = false;
        break;
      case FETCH_SOA_FAILURE:
        draft.loading = false;
        draft.error = action.error;
        break;
    }
  });

export default downloadSoaReducer;
