/*
 *
 * DownloadSoa actions
 *
 */

import { FETCH_SOA, FETCH_SOA_SUCCESS, FETCH_SOA_FAILURE } from './constants';

export function fetchSoa(params, callFrom) {
  return {
    type: FETCH_SOA,
    params,
    callFrom,
  };
}

export function soaFetched(response) {
  return {
    type: FETCH_SOA_SUCCESS,
    response,
  };
}

export function soaFetchingError(error) {
  return {
    type: FETCH_SOA_FAILURE,
    error,
  };
}
