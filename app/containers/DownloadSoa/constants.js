/*
 *
 * DownloadSoa constants
 *
 */

export const FETCH_SOA = 'app/DownloadSoa/FETCH_SOA';
export const FETCH_SOA_SUCCESS = 'app/DownloadSoa/FETCH_SOA_SUCCESS';
export const FETCH_SOA_FAILURE = 'app/DownloadSoa/FETCH_SOA_FAILURE';
