/**
 *
 * DownloadSoa
 *
 */

import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { DownloadOutlined } from '@ant-design/icons';
import { Spin, Empty } from 'antd';
import moment from 'moment';
import styled from 'styled-components';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectDownloadSoa from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { fetchSoa } from './actions';
import { formatDate } from 'utils/helpers';
import makeSelectExistingLans from '../ExistingLans/selectors';
import _ from 'lodash';
//import PDFViewer from 'pdf-viewer-reactjs';
import PdfNavigation from '../../components/PdfNavigation';

const SHOW_DOWNLOAD_LMS = ['INDUS', 'PENNANT', 'ORBI'];
const Wrapper = styled.div`
  /* width: 700px; */
  /* height: 700px; */
`;

export function DownloadSoa({
  fetchSoa,
  loan,
  closed,
  existingLans,
  downloadSoa,
}) {
  useInjectReducer({ key: 'downloadSoa', reducer });
  useInjectSaga({ key: 'downloadSoa', saga });

  useEffect(() => {
    if (SHOW_DOWNLOAD_LMS.includes(_.upperCase(sourceSystem))) {
      const params = {
        loanAccountNumber,
        responseFormat: 'PDF',
        lms: sourceSystem,
        product,
        partner,
        appId,
        fromDate: moment(loan_start_date).format('DD-MM-YYYY'),
      };
      fetchSoa(params, 'init');
    }
  }, []);

  const {
    loanAccountNumber,
    // source_System = 'Indus',
    product,
    partner,
    loan_start_date,
    lastPaymentMade,
    appId,
  } = loan;

  const sourceSystem = _.get(
    _.filter(existingLans.response, { loanAccountNumber }),
    '[0].lmsSystem',
  );

  const handleFetchSoa = () => {
    event.preventDefault();
    console.log(moment(loan_start_date).format('DD-MM-YYYY'));
    const params = {
      loanAccountNumber,
      responseFormat: 'PDF',
      lms: sourceSystem,
      product,
      partner,
      appId,
      fromDate: moment(loan_start_date).format('DD-MM-YYYY'),
    };
    fetchSoa(params);
  };

  if (!SHOW_DOWNLOAD_LMS.includes(_.upperCase(sourceSystem))) return <Empty />;
  return (
    <Spin spinning={downloadSoa.loading}>
      <Link to="#" className="blueTag" onClick={handleFetchSoa}>
        <DownloadOutlined />
        Account Statement
      </Link>
      {downloadSoa.response &&
        downloadSoa.response.data &&
        downloadSoa.response.data.signedUrls && (
          <Wrapper>
            {/* <PDFViewer
              document={{
                url: downloadSoa.response.data.signedUrls[0].url,
              }}
              scale={1.6}
              hideZoom
              hideRotation
              scaleStep={0.2}
              maxScale={3}
              minScale={1}
              navigation={PdfNavigation}
              navbarOnTop
            /> */}
          </Wrapper>
        )}
    </Spin>
  );
}

DownloadSoa.propTypes = {
  //dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  downloadSoa: makeSelectDownloadSoa(),
  existingLans: makeSelectExistingLans(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchSoa: (params, callFrom = 'download') =>
      dispatch(fetchSoa(params, callFrom)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(DownloadSoa);
