/**
 *
 * Asynchronously loads the component for DownloadSoa
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
