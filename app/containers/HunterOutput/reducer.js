/**
 * Hunter O/P Reducer
 */
import * as actions from './constants';

export const initialState = {
  loading: true,
  hunterData: [],
  cuid: '',
  error: {},
};

const hunterReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.LOAD_HUNTER_OUTPUT:
      return { ...state, loading: true };

    case actions.LOAD_HUNTER_OUTPUT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: { status: false, message: '' },
        hunterData: action.payload.data,
        cuid: action.payload.cuid || '',
      };

    case actions.LOAD_HUNTER_OUTPUT_ERROR: {
      return {
        ...state,
        loading: false,
        hunterData: {},
        cuid: '',
        error: {
          status: true,
          message: action.payload.errorMessage,
          severity: action.severity,
        },
      };
    }
    default: {
      return state;
    }
  }
};

export default hunterReducer;
