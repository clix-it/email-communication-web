import React, { useEffect, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Descriptions, Table, Spin } from 'antd';
import _ from 'lodash';
import styled from 'styled-components';
import { createStructuredSelector } from 'reselect';
import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { loadHunterOutput } from './actions';
import reducer from './reducer';
import saga from './saga';
import { makeSelectHunterOutput } from './selectors';

import DynamicTable from '../../components/DynamicTable';

function HunterOutput({ dispatch, hunterOutput }) {
  const { hunterData = {}, loading = false, cuid = '' } = hunterOutput;
  const matchSummary = _.get(
    hunterData,
    'matchResponse.matchResult.resultBlock.matchSummary',
    { matchSchemes: { scheme: [] }, rules: { rule: [] } },
  );
  const errorWarnings = _.get(
    hunterData,
    'matchResponse.matchResult.resultBlock.errorWarnings',
  );
  const {
    totalMatchScore = '',
    rules = {},
    matchSchemes = {},
    matches = '',
  } = matchSummary;

  const { rule = [] } = rules || {};
  const { scheme = [] } = matchSchemes || {};

  useEffect(() => {
    dispatch(loadHunterOutput());
  }, []);

  const renderData = (data, title) => {
    const keys = Object.keys(data[0] || {});
    const columns = keys.map((item, index) => {
      return {
        title: item,
        dataIndex: item,
        key: item,
        width: item.length * 12,
        fixed: index == 0 ? 'left' : index == keys.length - 1 ? 'right' : '',
      };
    });
    return (
      <>
        <Table
          columns={columns}
          dataSource={data || []}
          bordered
          scroll={{ x: 1300 }}
          title={() => title}
        />
      </>
    );
  };

  if (Object.keys(hunterOutput.hunterData).length === 0)
    return <NoData>No Hunter Output Data Available!</NoData>;

  return (
    <Spin spinning={loading} tip="Loading...">
      <Descriptions
        title="Match Summary"
        column={{ xxl: 3, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      >
        <Descriptions.Item label="CUID">{cuid}</Descriptions.Item>
        <Descriptions.Item label="Total Match Score">
          {totalMatchScore}
        </Descriptions.Item>
        {/* <Descriptions.Item label="Rules" />
        <Descriptions.Item label="Match Schemes" /> */}
        <Descriptions.Item label="Matches">{matches}</Descriptions.Item>
      </Descriptions>
      {rule.length > 0 ? renderData(rule, 'Rules') : ''}
      {scheme.length > 0 ? renderData(scheme, 'Match Schemes') : ''}
      {errorWarnings && parseInt(errorWarnings.errors.errorCount, 10) > 0 && (
        <>
          <Title>Errors</Title>
          <DynamicTable
            dataSource={errorWarnings.errors.error || []}
            defaultPageSize={5}
          />
        </>
      )}
      {errorWarnings && parseInt(errorWarnings.warnings.warningCount, 10) > 0 && (
        <>
          <Title>Warnings</Title>
          <DynamicTable
            dataSource={errorWarnings.warnings.warning || []}
            defaultPageSize={5}
          />
        </>
      )}
    </Spin>
  );
}

const Title = styled.div`
  font-size: 16px;
  font-weight: bold;
  line-height: 1.5715;
  margin-top: 20px;
  margin-bottom: 20px;
  color: rgba(0, 0, 0, 0.85);
`;

const NoData = styled.div`
  text-align: center;
  font-size: large;
  font-weight: bold;
`;

HunterOutput.propTypes = {
  dispatch: PropTypes.func.isRequired,
  hunterOutput: PropTypes.object,
};

HunterOutput.defaultProps = {
  hunterOutput: {},
};

const withReducer = useInjectReducer({
  key: 'hunterOutput',
  reducer,
});
const withSaga = useInjectSaga({ key: 'hunterOutput', saga });

const mapStateToProps = createStructuredSelector({
  hunterOutput: makeSelectHunterOutput(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(HunterOutput);
