import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import * as actions from './constants';
import env from '../../../environment';
import { loadHunterOutputSuccess, loadHunterOutputError } from './actions';
import data from './data.json';

function* fetchHunterOutputSaga() {
  try {
    const url = `${env.HUNTER_URL}/getById?value=${JSON.parse(
      sessionStorage.getItem('id'),
    )}`;
    const response = yield call(request, url, {
      method: 'GET',
      headers: env.headers,
    });
    //const response = data;
    //debugger;
    if (response.status || response.success) {
      // debugger;
      yield put(loadHunterOutputSuccess(response.responses[0].response, response.responses[0].cuid ));
    } else {
      yield put(
        loadHunterOutputError(
          response.reason ||
            `No Hunter Output data for ${JSON.parse(
              sessionStorage.getItem('id'),
            )} `,
        ),
      );
      message.error(
        response.reason ||
          `No Hunter Output data for ${JSON.parse(
            sessionStorage.getItem('id'),
          )} `,
      );
    }
  } catch (err) {
    // const errorResponse = yield err.json();
    // message.error(errorResponse.error.message);
    yield put(
      loadHunterOutputError('Network error occurred! Check connection!'),
    );
    message.info('App Id is not found!');
  }
}

export default function* hunterOutputSaga() {
  yield takeEvery(actions.LOAD_HUNTER_OUTPUT, fetchHunterOutputSaga);
}
