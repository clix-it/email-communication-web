import * as actions from './constants';
export function loadHunterOutput() {
  return {
    type: actions.LOAD_HUNTER_OUTPUT,
  };
}

export function loadHunterOutputSuccess(data, cuid) {
  return {
    type: actions.LOAD_HUNTER_OUTPUT_SUCCESS,
    payload: {
      data,
      cuid,
    },
  };
}

export function loadHunterOutputError(errorMessage, severity = 'error') {
  return {
    type: actions.LOAD_HUNTER_OUTPUT_ERROR,
    payload: {
      errorMessage,
      severity,
    },
  };
}
