import { createSelector } from 'reselect';

const selectHunterOutput = state => state.hunterOutput;

const makeSelectHunterOutput = () =>
  createSelector(
    selectHunterOutput,
    hunterOutput => hunterOutput,
  );

export { makeSelectHunterOutput };
