/**
 *Credit Portal
 */
import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { makeSelectApplications } from './selectors';

import useInjectSaga from '../../utils/injectSaga';
import useInjectReducer from '../../utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import { loadEmails } from './actions';

import EmailListingPage from '../../components/EmailListingPage';

export function EmailPortal({
  dispatch,
  emailPortal,
  requestType,
  activity,
}) {
  const { isLoading = false, error, applications, emailCommunications=[] } = emailPortal;
  if(activity){
    sessionStorage.setItem('activity', activity);
  }  
  return (
    <EmailListingPage
      //applications={applications}
      error={error}
      dispatch={dispatch}
      requestType={requestType}
      isLoading={isLoading}
      loadEmails={loadEmails}
      //setCurrentApp={setCurrentApp}
      activityType={activity}
      emailCommunications={emailCommunications}
      emailPortal={emailPortal}
    />
  );
}

EmailPortal.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

// const mapStateToProps = state => ({
//   isLoading: state.creditPortal.loading,
//   error: state.creditPortal.error,
//   applications: state.creditPortal.applications,
// });

const mapStateToProps = createStructuredSelector({
  emailPortal: makeSelectApplications(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

EmailPortal.propTypes = {
  emailPortal: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  requestType: PropTypes.string.isRequired,
};

EmailPortal.defaultProps = {
  emailPortal: {},
};

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = useInjectReducer({ key: 'emailPortal', reducer });

const withSaga = useInjectSaga({ key: 'emailPortal', saga });
export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(EmailPortal);
