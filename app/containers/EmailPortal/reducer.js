/**
 * Email Portal Reducer
 */
import * as actions from './constants';
import produce from 'immer';

export const initialState = {
  loading: true,
  emailCommunications: [],
  //  error: {},
  response: false,
  error: false,
  payload: false,
};

const emailPortalReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case actions.LOAD_EMAILS:
        draft.loading = true;
        break;
      case actions.LOAD_EMAILS_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.emailCommunications =
          action.response.data &&
          action.response.data.map((item, index) => {
            return { ...item, index: index + 1 };
          });
        break;
      case actions.LOAD_EMAILS_ERROR: {
        draft.error = action.error;
      }

      //Excel
      case actions.UPDATE_EXCEL_DETAILS:
        draft.response = false;
        draft.error = false;
        draft.payload = action.data;
        break;
      case actions.UPDATE_EXCEL_DETAILS_SUCCESS:
        draft.error = false;
        draft.response = action.data.message;
        break;
      case actions.UPDATE_EXCEL_DETAILS_ERROR:
        draft.response = false;
        draft.error = action.data.message;
        break;
      default: {
        return state;
      }
      //Search

      case actions.LOAD_EMAILS_SEARCH:
        draft.loading = true;
        draft.error = false;
        draft.payload = action.data;
        break;
      case actions.LOAD_EMAILS_SEARCH_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.emailCommunications =  action.response.data &&
        action.response.data.map((item, index) => {
          return { ...item, index: index + 1 };
        })
        break;
      case actions.LOAD_EMAILS_SEARCH_ERROR: {
        draft.error = action.error
      }
    }
  });

export default emailPortalReducer;
