/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
// import { useParams, useLocation, useHistory } from 'react-router-dom';
import {
  useParams,
  useLocation,
  useHistory,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { ToastContainer, toast } from 'react-toastify';

import Header from 'components/Header';
import { Layout, Col, Row, PageHeader, Button, message } from 'antd';

import { compose } from 'redux';
import useInjectSaga from 'utils/injectSaga';
import LeadsList from '../LeadsList';
import LeadDetails from '../LeadDetails';
import { resetState } from './actions';
import LoginPage from '../LoginPage';
import GlobalStyle from '../../global-styles';
import saga from './saga';
import Slider from '../../components/Slider';
import EmailPortal from '../EmailPortal';
import CustomerDetails from '../CustomerDetails';
import CustomerDetailsPortal from '../CustomerDetailsPortal';
import PostSanction from '../PostSanction';
import ApplicantDetails from '../ApplicantDetails';
import UserDecision from '../UserDecision';
import DoaApprovalPortal from '../DoaApprovalPortal';
import AppAuthorization from '../AppAuthorization';
import DedupeHeader from '../DedupeHeader';
import EmailUpdate from '../EmailUpdate';
import TemplatePortal from '../TemplatePortal';
import AddTemplate from '../AddTemplate';
import TemplateUpdate from '../TemplateUpdate';
import SendEmail from '../SendEmail';
import AddEmailDetails from '../AddEmailDetails';

export function App({
  history,
  global,
  appId,
  isAuthorized,
  dispatch,
  match,
  error,
  success,
}) {
  const [title, setTitle] = React.useState(false);

  const { Content } = Layout;

  const loc = useLocation();

  // const history = useHistory();

  console.log('query', loc);
  const value = loc.pathname.split('/');

  useEffect(() => {
    if (isAuthorized) {
      const value2 = history.location.pathname.split('/');
      if (value2[2]) {
        history.push(`./../${value2[1]}`);
      }
      dispatch(resetState());
    }
  }, []);

  {
    try {
      !isNaN(value[2])
        ? sessionStorage.setItem('id', JSON.stringify(value[2]))
        : sessionStorage.getItem('id')
        ? sessionStorage.removeItem('id')
          : '';
    } catch (e) {
      console.log(e);
    }
  }

  // const onClickSideMenuItem = key => {
  //   sessionStorage.removeItem('id');
  //   sessionStorage.setItem('menuKey', key);
  //   sessionStorage.setItem('title', key);
  //   setTitle(key);
  // };

  return (
    <div>
      {isAuthorized ? (
        <Layout style={{ minHeight: '100vh' }}>
          <Slider />
          <Layout className="site-layout" style={{ minHeight: '100vh' }}>
            <Header
              className="site-layout-background"
              isAuthorized={isAuthorized}
              dispatch={dispatch}
            />

            <Row>
              <Col span={24}>
                <Content
                  className="site-layout-background"
                  style={{
                    margin: '24px 16px',
                  }}
                >
                  {sessionStorage.getItem('id') &&
                  !loc.pathname.toLowerCase().includes('customer') ? (
                    <PageHeader
                      className="site-page-header"
                      title={
                        sessionStorage.getItem('id')
                          ? 'Details Viewed for Id : '
                          : ''
                      }
                      subTitle={
                        sessionStorage.getItem('id')
                          ? JSON.parse(sessionStorage.getItem('id'))
                          : ''
                      }
                      extra={
                        sessionStorage.getItem('id') &&
                        loc.pathname.toLowerCase().includes('pma/') &&
                        !loc.pathname.toLowerCase().includes('dedupe')
                          ? [
                              <UserDecision
                                title="Approve"
                                status="Approved"
                              />,
                            <UserDecision title="Reject" status="Rejected" />,
                            <UserDecision title="Check Employment" status="check_employment" />,
                              <UserDecision
                                title="Send Back"
                                status="Sent_Back"
                              />,
                              <UserDecision
                                title="Refer to FCU"
                                status="REFERRED"
                                referTo="FCU"
                              />,
                              <UserDecision
                                title="Refer to Credit Underwriting"
                                status="REFERRED"
                                referTo="Credit"
                              />,
                              <UserDecision
                                title="Forward"
                                status="FORWARDED"
                              />,
                            ]
                          : loc.pathname.toLowerCase().includes('pma/') &&
                            loc.pathname.toLowerCase().includes('dedupe')
                          ? [<DedupeHeader />]
                          : sessionStorage.getItem('id') &&
                            loc.pathname.toLowerCase().includes('clo/')
                          ? [
                              <UserDecision
                                title="Re-Initiate"
                                status="Relook"
                              />,
                              <UserDecision
                                title="Pass Waiver"
                                status="Waiver"
                              />,
                            ]
                          : sessionStorage.getItem('id') &&
                            !loc.pathname.toLowerCase().includes('doa') &&
                            loc.pathname.toLowerCase().includes('poa/')
                          ? [
                                    <UserDecision
                                      title="Assign to me"
                                      status="TRANSFERRED"
                              />,
                            ]
                                  : null
                      }
                    />
                  ) : null}
                  <div className="content-wrap">
                    <ToastContainer autoClose={5000} />
                    <Switch>
                      <Route
                        exact
                        path="/"
                        render={routerProps => (
                          <Redirect to="/emailDetails" />
                        )}
                      />
                      <Route
                        path="/addEmail"
                        render={routerProps => (
                          <AppAuthorization
                            activity="EMAIL_MASTER"
                            component={
                              <AddEmailDetails
                                {...routerProps}
                                // requestType="PMA"
                                 activity="EMAIL_MASTER"
                              />
                            }
                          />
                        )}
                      />
                      <Route
                        path="/addTemplate"
                        render={routerProps => (
                          <AppAuthorization
                            activity="TEMPLATE_MASTER"
                            component={
                              <AddTemplate
                                {...routerProps}
                                // requestType="PMA"
                                 activity="TEMPLATE_MASTER"
                              />
                            }
                          />
                        )}
                      />
                      <Route
                        path="/emailDetails/:appIdEmail"
                        render={routerProps => (
                          <AppAuthorization
                            activity="EMAIL_MASTER"
                            component={
                              <EmailUpdate
                                {...routerProps}
                                // requestType="PMA"
                               activity="EMAIL_MASTER"
                              />
                            }
                          />
                        )}
                      />
                      <Route
                        path="/emailDetails"
                        render={routerProps => (
                          <AppAuthorization
                            activity="EMAIL_MASTER"
                            component={
                              <EmailPortal
                              //  {...props}
                                requestType="PMA"
                                activity="EMAIL_MASTER"
                              />
                            }
                          />
                        )}
                      />
                      <Route
                        path="/templateDetails"
                        render={routerProps => (
                          <AppAuthorization
                            activity="TEMPLATE_MASTER"
                            component={
                              <TemplatePortal
                              //  {...props}
                                requestType="PMA"
                                activity="TEMPLATE_MASTER"
                              />
                            }
                          />
                        )}
                      />
                      <Route
                        path="/sendEmail"
                        render={routerProps => (
                          <AppAuthorization
                            activity="SEND_MAIL"
                            component={
                              <SendEmail
                              //  {...props}
                                requestType="PMA"
                                activity="SEND_MAIL"
                              />
                            }
                          />
                        )}
                      />
                      <Route
                        path="/templateUpdate/:linkTemplateId"
                        render={routerProps => (
                          <AppAuthorization
                            activity="TEMPLATE_MASTER"
                            component={
                              <TemplateUpdate
                                {...routerProps}
                                // requestType="PMA"
                                 activity="TEMPLATE_MASTER"
                              />
                            }
                          />
                        )}
                      />
                    </Switch>
                    <GlobalStyle />
                  </div>
                </Content>
              </Col>
            </Row>
          </Layout>
        </Layout>
      ) : (
        <Layout className="login-layout" style={{ minHeight: '100vh' }}>
          <Header />

          <Layout className="site-layout-login">
            <Row>
              <Col span={24}>
                <Content
                  className="site-layout-background-login"
                  style={{
                    margin: '24px 16px',
                  }}
                >
                  <div className="content-wrap-login">
                    <ToastContainer autoClose={5000} />

                    <Route
                      exact
                      path="/"
                      render={routerProps => <LoginPage />}
                    />
                    <Route
                      path="*"
                      render={routerProps => <Redirect to="/" />}
                    />
                    <GlobalStyle />
                  </div>
                </Content>
              </Col>
            </Row>
          </Layout>
          {/* <Footer style={{ textAlign: 'center', zIndex: '-1' }}>
            Ant Design ©2018 Created by Ant UED
          </Footer> */}
        </Layout>
      )}
    </div>
  );
}
const withSaga = useInjectSaga({ key: 'app', saga });
const mapStateToProps = state => ({
  isAuthorized: state.global.user.isAuthorized,
  error: state.global.error,
  success: state.global.success,
});
const mapDispatchtoProps = dispatch => ({ dispatch });
const withConnect = connect(
  mapStateToProps,
  mapDispatchtoProps,
);
export default compose(
  withSaga,
  withRouter,
  withConnect,
)(App);
