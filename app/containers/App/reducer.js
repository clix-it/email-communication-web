import * as actions from './constants';

const initialState = {
  user: { isAuthorized: false, id: '', role: '' },
  error: {},
  success: {},
  approval: false,
  approvalData: false,
  approvalDataError: false,
  // user: { isAuthorized: true, id: '100693', role: 'OPC' },
};

export default function globalReducer(state = initialState, action) {
  switch (action.type) {
    case actions.LOG_USER_IN: {
      return {
        ...state,
        user: {
          isAuthorized: true,
          id: action.userId,
          role: action.userRole,
        },
      };
    }
    case actions.RESET_STATE: {
      return {
        ...state,
        approval: false,
        approvalData: false,
        approvalDataError: false,
        user: {
          isAuthorized: true,
          id: action.userId,
          role: action.userRole,
        },
      };
    }
    case actions.LOG_USER_OUT: {
      return {
        ...state,
        user: { isAuthorized: false, id: '' },
      };
    }
    case actions.CLEAR_MESSAGES: {
      return {
        ...state,
        error: {},
        success: {},
      };
    }
    case actions.SET_ERROR: {
      return {
        ...state,
        error: { status: true, message: action.payload.error },
      };
    }
    case actions.SET_SUCCESS: {
      return {
        ...state,
        success: { status: true, message: action.payload.success },
      };
    }
    default:
      return state;
  }
}
