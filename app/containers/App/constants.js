/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */
export const LOG_USER_IN = 'LOG_USER_IN';
export const LOG_USER_OUT = 'LOG_USER_OUT';
export const REGISTER_USER = 'REGISTER_USER ';
export const DEREGISTER_USER = 'DEREGISTER_USER ';

export const CHECK_VALID_USER = 'CHECK_VALID_USER';
export const APPROVE_USER = 'APPROVE_USER';
export const SET_ERROR = 'SET_ERROR';
export const SET_SUCCESS = 'SET_SUCCESS';
export const CLEAR_MESSAGES = 'CLEAR_MESSAGES';
export const RESET_STATE = 'RESET_STATE';

export const mobilePattern = {
  value: /^([+]\d{2})?\d{10}$/,
  message: 'Invalid Mobile Number',
};
export const emailPattern = {
  // eslint-disable-next-line no-useless-escape
  value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
  message: 'Invalid Email',
};
export const numberPattern = {
  value: /^(0|[1-9][0-9]*)$/,
  message: 'Invalid Number',
};
