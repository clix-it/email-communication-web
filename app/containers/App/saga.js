// import { takeEvery, put, select, call } from 'redux-saga/effects';
import {
  takeEvery,
  put,
  take,
  call,
  select,
  debounce,
} from 'redux-saga/effects';
import * as actions from './constants';
import {
  checkValidUser,
  logUserIn,
  deRegisterUser,
  setError,
  setSuccess,
} from './actions';
import request from 'utils/request';
import { checkNSDLSuccess } from '../ApplicantDetails/actions';
import env from '../../../environment';
import makeSelectApplicantDetails from '../ApplicantDetails/selectors';
import { push } from 'react-router-redux';
import _ from 'lodash';

function* checkValidUserSaga(action) {
  const key = yield sessionStorage.getItem('key');
  if (key === btoa(new Date().toLocaleDateString())) {
    const user = yield JSON.parse(atob(sessionStorage.getItem('user')));
    if (user) {
      yield put(logUserIn(user.id, user.role));
    }
  }
}
function* deRegisterUserSaga(action) {
  yield sessionStorage.removeItem('key');
  yield sessionStorage.removeItem('user');
  yield put(push('/'));
  location.reload();
}

function* registerUserSaga(action) {
  const key = btoa(new Date().toLocaleDateString());
  const user = btoa(
    JSON.stringify({ id: action.userId, role: action.userRole }),
  );
  yield sessionStorage.setItem('key', key);
  yield sessionStorage.setItem('user', user);
}
export function* partialFullMatch({ docId }) {
  let userResponse = fetch(`${env.USER_SERVICE_API_URL}/1000002494`, {
    headers: env.headers,
  });

  let leadDetails = fetch(`${env.SELF_SERVICE}/myaccount/1000002494`, {
    headers: env.headers,
  });

  try {
    [leadDetails, userResponse] = yield Promise.all([
      leadDetails,
      userResponse,
    ]).then(values => values);
  } catch (error) {}

  // const reqUrl = `${DMS_API_URL}/${docId}`;
  // const options = {
  //   method: 'GET',
  // };
  // try {
  //   const response = yield call(request, reqUrl, options);
  //   console.log('DMS API response', response);
  //   return response;
  // } catch (error) {}
}

export function* updateApplication(params, id) {
  try {
    const result = yield fetch(`${env.APP_SERVICE_API_URL}/${id}`, {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify(params),
    });

    return result;
  } catch (err) {}
}

export function* updateCompositeApplication(params) {
  try {
    const response = yield call(
      request,
      `${env.API_URL}/profiles/update/details`,
      {
        method: 'PUT',
        headers: env.headers,
        body: JSON.stringify(params),
      },
    );
    return response;
  } catch (err) {}
}

export function* updateUser(params, id, type) {
  try {
    const result = yield fetch(`${env.DEDUPE_URL}/${type}/${id}`, {
      method: 'PUT',
      headers: env.headers,
      body: JSON.stringify(params),
    });

    return result;
  } catch (err) {}
}

export function* approveUser() {
  try {
    const creditData = yield select(makeSelectApplicantDetails());
    const { cuid } = creditData.entity;
    const appId = sessionStorage.getItem('id')
      ? JSON.parse(sessionStorage.getItem('id'))
      : '';
    // console.log('appid', appId, 'cuid', cuid);
    const result = yield fetch(`${env.API_URL}`, {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        cuid,
        appId,
        applicationStage: 'DEDUPE_REVIEW',
        rejectionStage: 'DEDUPE_REVIEW',
        reasonDetails: cuid,
        rejectionReason: 'EXACT_MATCH_REFER_TO_CREDIT',
        workflowId: appId,
      }),
    });

    if (result.status === 200) {
      const responseBody = yield result.json();
      if (responseBody.success || responseBody.status) {
        yield put(setSuccess(responseBody.data || 'User Approved'));
      } else {
        yield put(setError('Oops! User Could Not be approved :( '));
      }
    } else {
      yield put(setError(`HTTP Status${result.status} occurred.Try Again`));
    }
  } catch (err) {
    yield put(setError('Network error occurred! Check connection!'));
  }
}

export function* checkNSDL(params) {
  const { formData } = params;

  let pan;
  let panArray = [];
  try {
    for (var i in formData) {
      pan = formData[i].pan;

      const nsdlData = {
        pan: pan.toUpperCase(),
      };
      console.log('nsdl hitting JSON data', nsdlData);
      const nsdlUrl = `${env.NSDL_API_URL}`;
      const nsdlOptions = {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify(nsdlData),
      };

      const nsdlResponse = yield call(request, nsdlUrl, nsdlOptions);
      console.log('nsdl response', nsdlResponse);

      panArray.push({
        type: formData[i].type,
        pan: formData[i].pan,
        nsdlResponse,
      });
    }
    yield put(checkNSDLSuccess(panArray));
    // return nsdlResponse;
  } catch (error) {
    console.log('nsdl error', error);
    return error;
  }
}

export default function* appSaga() {
  yield takeEvery(actions.REGISTER_USER, registerUserSaga);
  yield takeEvery(actions.DEREGISTER_USER, deRegisterUserSaga);
  yield takeEvery(actions.CHECK_VALID_USER, checkValidUserSaga);
  yield takeEvery(actions.APPROVE_USER, approveUser);
  // yield put(checkValidUser());
}
