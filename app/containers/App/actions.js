import * as actions from './constants';
export function logUserIn(userId, userRole) {
  return {
    type: actions.LOG_USER_IN,
    userId,
    userRole,
  };
}
export function logUserOut() {
  return {
    type: actions.LOG_USER_OUT,
  };
}
export function registerUser(userId, userRole) {
  return {
    type: actions.REGISTER_USER,
    userId,
    userRole,
  };
}
export function resetState() {
  return {
    type: actions.RESET_STATE,
  };
}
export function checkValidUser() {
  return {
    type: actions.CHECK_VALID_USER,
  };
}

export function deRegisterUser() {
  return {
    type: actions.DEREGISTER_USER,
  };
}

export function approveUser() {
  return {
    type: actions.APPROVE_USER,
  };
}

export function setError(error) {
  return {
    type: actions.SET_ERROR,
    payload: {
      error,
    },
  };
}

export function setSuccess(success) {
  return {
    type: actions.APPROVE_USER,
    payload: {
      success,
    },
  };
}

export function clearMessages() {
  return {
    type: actions.CLEAR_MESSAGES,
  };
}
