import { takeEvery, put, call } from 'redux-saga/effects';
import { message } from 'antd';
import request from 'utils/request';
import env from '../../../environment';
import * as actions from './constants';
import { ownershipsLoaded, ownserrshipFailed } from './actions';

function* loadOwnerships() {
  try {
    const response = yield call(
      request,
      `${env.MASTER_URL}?masterType=OWNERSHIP_HFSAPP_HFS`,
      {
        method: 'GET',
        headers: env.headers,
      },
    );

    // const responseBody = yield result.json();
    // const responseBody = data;
    if (response && response.success) {
      yield put(ownershipsLoaded(response.masterData));
    } else {
      yield put(ownserrshipFailed([]));
      message.info('Oops! No Remarks found :( ');
    }
  } catch (err) {
    yield put(ownserrshipFailed('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}

export default function* ownerShipTypeSaga() {
  yield takeEvery(actions.LOAD_OWNSERHIP_TYPE, loadOwnerships);
}
