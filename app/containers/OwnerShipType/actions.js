/*
 *
 * OwnerShipType actions
 *
 */

import {
  LOAD_OWNSERHIP_TYPE,
  LOAD_OWNSERHIP_TYPE_SUCCESS,
  LOAD_OWNSERHIP_TYPE_FAILED,
} from './constants';

export function loadOwnerships() {
  return {
    type: LOAD_OWNSERHIP_TYPE,
  };
}

export function ownershipsLoaded(response) {
  return {
    type: LOAD_OWNSERHIP_TYPE_SUCCESS,
    response,
  };
}

export function ownserrshipFailed(error) {
  return {
    type: LOAD_OWNSERHIP_TYPE_FAILED,
    error,
  };
}
