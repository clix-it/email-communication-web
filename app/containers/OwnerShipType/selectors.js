import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the ownerShipType state domain
 */

const selectOwnerShipTypeDomain = state => state.ownerShipType || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by OwnerShipType
 */

const makeSelectOwnerShipType = () =>
  createSelector(
    selectOwnerShipTypeDomain,
    substate => substate,
  );

export default makeSelectOwnerShipType;
export { selectOwnerShipTypeDomain };
