/**
 *
 * OwnerShipType
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Spin } from 'antd';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import InputField from 'components/InputField';
import makeSelectOwnerShipType from './selectors';
import reducer from './reducer';
import saga from './saga';
import { loadOwnerships } from './actions';

export function OwnerShipType({ loadOwnerships, ownerShipType, ...rest }) {
  useInjectReducer({ key: 'ownerShipType', reducer });
  useInjectSaga({ key: 'ownerShipType', saga });
  useEffect(loadOwnerships, []);
  const { loading, response } = ownerShipType;
  return (
    <Spin spinning={loading}>
      <InputField {...rest} options={response} />
    </Spin>
  );
}

OwnerShipType.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ownerShipType: makeSelectOwnerShipType(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    loadOwnerships: () => dispatch(loadOwnerships()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(OwnerShipType);
