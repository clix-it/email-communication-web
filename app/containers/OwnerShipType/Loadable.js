/**
 *
 * Asynchronously loads the component for OwnerShipType
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
