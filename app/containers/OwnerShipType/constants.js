/*
 *
 * OwnerShipType constants
 *
 */

export const LOAD_OWNSERHIP_TYPE = 'app/OwnerShipType/LOAD_OWNSERHIP_TYPE';
export const LOAD_OWNSERHIP_TYPE_SUCCESS =
  'app/OwnerShipType/LOAD_OWNSERHIP_TYPE_SUCCESS';
export const LOAD_OWNSERHIP_TYPE_FAILED =
  'app/OwnerShipType/LOAD_OWNSERHIP_TYPE_FAILED';
