/*
 *
 * OwnerShipType reducer
 *
 */
import _ from 'lodash';
import produce from 'immer';
import {
  LOAD_OWNSERHIP_TYPE,
  LOAD_OWNSERHIP_TYPE_SUCCESS,
  LOAD_OWNSERHIP_TYPE_FAILED,
} from './constants';

export const initialState = {
  loading: false,
  error: false,
  response: false,
};

/* eslint-disable default-case, no-param-reassign */
const ownerShipTypeReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case LOAD_OWNSERHIP_TYPE:
        draft.loading = true;
        draft.response = false;
        draft.error = false;
        break;
      case LOAD_OWNSERHIP_TYPE_SUCCESS:
        draft.loading = false;
        draft.response = _.map(action.response, 'cmValue');
        draft.error = false;
        break;
      case LOAD_OWNSERHIP_TYPE_FAILED:
        draft.loading = false;
        draft.response = false;
        draft.error = action.error;
        break;
    }
  });

export default ownerShipTypeReducer;
