/**
 *
 * Asynchronously loads the component for User Decisoin
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
