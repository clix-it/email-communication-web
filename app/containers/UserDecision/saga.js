import { takeEvery, put, select, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { message } from 'antd';
import _ from 'lodash';
import request from 'utils/request';
import moment from 'moment';
import {
  USER_DECISION,
  RELOOK_DECISION,
  PASS_WAIVER,
  FETCH_REJECTION_CODES,
  FETCH_HISTORY,
  FETCH_TENURE,
  UPLOAD_REQUEST,
  FETCH_FEE,
  FETCH_SEND_TO,
  REFER_TO_STAGE,
  CHECK_EMPLOYMENT,
} from './constants';
import {
  setError,
  setSuccess,
  fetchRejectionCodesSuccess,
  fetchRejectionCodesError,
  loadRemarksSuccess,
  loadRemarksError,
  fetchLoanTenureSuccess,
  fetchLoanTenureError,
  uploadleadsDocSuccess,
  uploadleadsDocFail,
  fetchFeeCodeSuccess,
  fetchFeeCodeError,
  fetchSendToOptionsSuccess,
  fetchSendToOptionsError,
  checkEmploymentSuccess,
  checkEmploymentError,
} from './actions';
import env from '../../../environment';
import { makeSelectApplications } from '../CreditPortal/selectors';
import { makeSelectApplicantDetails2 } from '../ApplicantDetails/selectors';
import { fetchMasterData } from '../ApplicantDetails/saga';
import { updateCompositeApplication } from '../App/saga';
import { loadEntityDetails } from '../ApplicantDetails/actions';
import employmentCheck from './employmentCheck.json';

const activities = {
  applicantDetailsClo: 'CREDIT',
  dedupeListclo: 'DEDUPE',
  hunterReviewclo: 'HUNTER',
  employmentReviewclo: 'EMPLOYMENT',
  incomeReviewclo: 'INCOME',
  fraudReviewclo: 'CREDIT_FCU',
  doaApprovalclo: 'CREDIT_DOA',
  gamReviewclo: 'GAM_REVIEW',
};

export function* userDecision(action) {
  const { data } = action.payload;
  const { currentApplication = {} } = yield select(makeSelectApplications());
  const dataToPass = {
    activitySrlNo: currentApplication.activitySrlNo,
    employeeId: JSON.parse(sessionStorage.getItem('userId')),
    status: data.status.toUpperCase(),
    remarks: data.remark,
    nextApprovalUserId: '',
  };
  if (data.status.toUpperCase() === 'SENT_BACK') {
    dataToPass.sendTo = data.sentTo;
  }
  try {
    const requestURL = `${env.API_URL}/${
      env.MAKER_CHECKER_API_URL
    }/v1/user/decision`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(dataToPass),
    };
    const response = yield call(request, requestURL, options);
    if (response.success || response.status) {
      yield* notifyApplicationService();
      message.info(response.message || `User ${data.status} Successfully!`);
      yield put(
        setSuccess(response.message || `User ${data.status} Successfully!`),
      );
      yield put(push(`/${location.pathname.split('/')[1]}`));
    } else {
      message.info(response.message || 'Oops! User Could Not be approved :(');
      yield put(
        setError(response.message || 'Oops! User Could Not be approved :('),
      );
    }
  } catch (err) {
    yield put(setError(err));
    message.error(
      (err || {}).message || 'Network error occurred! Check connection!',
    );
  }
}

export function* notifyApplicationService() {
  const applicantDetails = yield select(makeSelectApplicantDetails2());
  const creditManagerId = _.get(
    applicantDetails,
    'entity.appSourcing.creditManagerId',
  );
  if (creditManagerId) return;
  const requestURL = `${env.APP_SERVICE_API_URL}/${JSON.parse(
    sessionStorage.getItem('id'),
  )}`;
  const options = {
    method: 'PUT',
    headers: env.headers,
    body: JSON.stringify({
      appSourcing: {
        creditManagerId: JSON.parse(sessionStorage.getItem('userId')),
      },
    }),
  };
  try {
    const response = yield call(request, requestURL, options);
    if (!response.success && !response.status) {
      message.info(
        response.message || 'Oops! Error on updating Credit Manager Id :(',
      );
      yield put(
        setError(
          response.message || 'Oops! Error on updating Credit Manager Id :(',
        ),
      );
    }
  } catch (err) {
    yield put(setError('Network error occurred! Check connection!'));
  }
}

export function* relookDecision(action) {
  // const { data } = action.payload;
  const address = location.pathname.split('/');
  const appId = sessionStorage.getItem('id') || '';
  const str = appId.replace(/^"(.*)"$/, '$1');
  if (!appId) {
    yield put(setError('Activity Serial Number Not found! :( '));
    return;
  }
  try {
    const response = yield call(
      request,
      `${env.API_URL}${env.CREDIT_REFERRAL}`,
      {
        method: 'POST',
        headers: env.headers,
        body: JSON.stringify({
          activity: activities[address[1]],
          state:
            activities[address[1]] === 'CREDIT' ||
            activities[address[1]] === 'INCOME' ||
            activities[address[1]] === 'GAM_REVIEW' ||
            activities[address[1]] === 'CREDIT_FCU' ||
            activities[address[1]] === 'HUNTER'
              ? 'ACTIVE'
              : 'APPROVED',
          actorId: JSON.parse(sessionStorage.getItem('userId')),
          appId: str,
        }),
      },
    );
    if (response.success || response.status) {
      yield* notifyApplicationService();
      message.info(response.message || 'App reinitiated Successfully!');
      yield put(
        setSuccess(response.message || 'App reinitiated Successfully!'),
      );
      yield put(push(`/${location.pathname.split('/')[1]}`));
    } else {
      message.info(
        response.message || 'Oops! App reinitiation unsuccessful :(',
      );
      yield put(
        setSuccess(
          response.message || 'Oops! App reinitiation unsuccessful :(',
        ),
      );
    }
  } catch (err) {
    yield put(setError(err));
    message.error('Network error occurred! Check connection!');
  }
}

export function* fetchRejectCodes({ partner, product }) {
  try {
    const response = yield call(
      fetchMasterData,
      partner,
      product,
      'REJECTIONCODES',
    );
    if (response && response.success) {
      yield put(fetchRejectionCodesSuccess(response.masterData));
    } else {
      yield put(fetchRejectionCodesError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchRejectionCodesError('Master Data not found'));
  }
}

function* fetchHistory(action) {
  try {
    const responseBody = yield call(
      request,
      `${env.REMARK_URL}/?appid=${action.payload.appId}`,
      {
        method: 'GET',
      },
    );
    if (responseBody && responseBody.length > 0) {
      yield put(loadRemarksSuccess(responseBody));
    } else {
      yield put(loadRemarksSuccess([]));
      message.info('Oops! No Remarks found :( ');
    }
  } catch (err) {
    yield put(loadRemarksError('Network error occurred! Check connection!'));
    message.error('Network error occurred! Check connection!');
  }
}

export function* fetchLoanTenure({ partner, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=LOANTENURE_${partner}_${product}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchLoanTenureSuccess(response.masterData));
    } else {
      yield put(fetchLoanTenureError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchLoanTenureError('Master Data not found'));
  }
}

export function* passWaiverDecision({ data, loanChargeCode }) {
  // debugger;
  const entityData = yield select(makeSelectApplicantDetails2());
  const appData = {
    loanOffers: [
      {
        ...data.loanOffers,
        id: _.get(
          _.orderBy(
            _.filter(
              _.get(entityData, 'entity.loanOffers'),
              loanOffer =>
                loanOffer.type === 'credit_amount' ||
                loanOffer.type === 'eligible_amount',
            ),
            'id',
            'desc',
          ),

          '[0].id',
        ),
      },
    ],
    loanCharges: [
      {
        ...data.loanCharges,
        id: _.get(
          _.orderBy(_.get(entityData, 'entity.loanCharges'), 'id', 'desc'),
          '[0].id',
        ),
        chargeCode: loanChargeCode,
      },
    ],
    loanActorsHistories: [
      {
        userRole: JSON.parse(sessionStorage.getItem('userRole')),
        userCode: JSON.parse(sessionStorage.getItem('userId')),
        remarks: data.remark,
        endTime: moment().format('YYYY-MM-DD'),
      },
    ],
  };

  try {
    const response = yield updateCompositeApplication({
      ...appData,
      appId: JSON.parse(sessionStorage.getItem('id')),
    });
    const appServiceResponse = _.get(response, 'data[0].appServiceResponse');
    if (_.get(appServiceResponse, 'body.success')) {
      message.success('Waiver Passed Successfully!');
      yield put(setSuccess('Waiver Passed Successfully!'));
      yield put(loadEntityDetails(JSON.parse(sessionStorage.getItem('id'))));
      // reset(values);
    } else {
      message.info('Some error occured!');
    }
  } catch (err) {
    yield put(setError(err));
    message.error('Network error occurred! Check connection!');
  }
}

export function* uploadDoc(action) {
  const { filestoBeSend } = action.payload;
  const file = [action.payload.binaryData.split(',')[1]];
  const body = {
    objId: action.payload.id,
    objType: action.payload.type,
    docType: action.payload.docType,
    fileName: action.payload.file.name,
    fileAttributes: {
      isPasswordProtected: false,
      filePassword: '',
    },
    files: file,
  };
  if (action.payload.otherDocumentName) {
    body.fileAttributes.docName = action.payload.otherDocumentName;
  }
  if (true) {
    yield call(fileUpload, { data: { ...body, filestoBeSend } });
  }
}

export function* fileUpload({ data }) {
  const reqUrl = `${env.API_URL}/app-docs/v2/upload`;
  const {
    fileAttributes,
    objId,
    objType,
    docType,
    fileName,
    filestoBeSend,
  } = data;
  const fInalData = {
    objId,
    objType,
    docType,
    fileName,
    fileAttributes,
    files: [],
  };
  const options = {
    method: 'POST',
    body: JSON.stringify(fInalData),
    headers: env.headers,
  };
  try {
    const response = yield call(request, reqUrl, options);

    if (response.status) {
      const optionsNew = {
        method: 'PUT',
        body: filestoBeSend,
      };
      const doNotCopyHeader = true;
      const response2 = yield fetch(response.data.presignedUrl, {
        ...optionsNew,
        doNotCopyHeader,
      });
      if (response2.status == 200) {
        yield put(uploadleadsDocSuccess());
      } else {
        yield put(uploadleadsDocFail('Doc upload failed'));
      }
    } else {
      yield put(uploadleadsDocFail('Doc upload failed 1'));
    }
  } catch (err) {
    yield put(uploadleadsDocFail('Doc upload failed 3'));
  }
}

export function* fetchFeeCode({ partner }) {
  const requestURL = `${env.MASTER_URL}?masterType=FEECODES_${partner}`;
  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    if (response && response.success) {
      yield put(fetchFeeCodeSuccess(response.masterData));
    } else {
      yield put(fetchFeeCodeError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchFeeCodeError('Master Data not found'));
  }
}

export function* fetchSendToOptions({ stage, product }) {
  const requestURL = `${
    env.MASTER_URL
  }?masterType=SENDBACK_${stage}_${product}`;

  try {
    const response = yield call(request, requestURL, {
      headers: env.headers,
    });
    /* const response = {
      "masterType": "SENDBACK_CREDIT_REVIEW_BL",
      "active": "Y",
      "count": 4,
      "masterData": [
          {
              "cmCode": "CREDIT_REVIEW",
              "cmValue": "Credit Review",
              "order": "1",
              "parentMasterType": null,
              "parentCmCode": null
          },
          {
              "cmCode": "SALES_REVIEW",
              "cmValue": "Sales Review",
              "order": "2",
              "parentMasterType": null,
              "parentCmCode": null
          },
          {
              "cmCode": "OPS_REVIEW",
              "cmValue": "Ops Review ",
              "order": "3",
              "parentMasterType": null,
              "parentCmCode": null
          }
      ],
      "success": true,
      "error": null
  } */
    if (response && response.success) {
      yield put(fetchSendToOptionsSuccess(response.masterData));
    } else {
      yield put(fetchSendToOptionsError('Master Data not found'));
    }
  } catch (err) {
    yield put(fetchSendToOptionsError('Master Data not found'));
  }
}

export function* referToStage(payload) {
  const {
    customerName = '',
    product = '',
    partner = '',
    panNumber = '',
    activityRequestId = '',
    applicationId = '',
  } = payload.currentApplication;
  const dataToPass = {
    userActivity: payload.referTo == 'FCU' ? 'CREDIT_FCU' : 'CREDIT_REVIEW',
    source: 'ELIGIBILITY_CHECK',
    applicationId,
    partner,
    product,
    cuid: '',
    activityRequestId,
    ruleDesc: '1-CM',
    nextActor: '',
    followHierarchy: true,
    panNumber,
    customerName,
  };
  try {
    const requestURL = `${env.API_URL}/${
      env.MAKER_CHECKER_API_URL
    }/v1/transaction/create`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(dataToPass),
    };
    const response = yield call(request, requestURL, options);
    if (response.success || response.status) {
      message.info(`Referred successfully`);
      yield put(setSuccess('Referred successfully'));
      yield put(push(`/${location.pathname.split('/')[1]}`));
    } else {
      message.info(response.message || 'Oops! unable to refer :(');
      yield put(setError(response.message || 'Oops! unable to refer :('));
    }
  } catch (err) {
    yield put(setError(err));
    message.error('Network error occurred! Check connection!');
  }
}

function* checkEmployment() {
  try {
    const { entity: { users = [] } = {} } = yield select(
      makeSelectApplicantDetails2(),
    );
    const requestURL = `${env.API_URL}/profiles/employmentVerification`;
    const options = {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify({
        cuid: _.get(users, '[0].cuid'),
        appId: JSON.parse(sessionStorage.getItem('id')),
        banking: true,
      }),
    };

    const response = yield call(request, requestURL, options);
    // const response = employmentCheck;
    if (_.get(response, 'mergeResult.decison.status') === 'Accept') {
      yield put(checkEmploymentSuccess(response));
      message.info('Employement Check is done!');
    } else {
      yield put(
        checkEmploymentError(
          _.get(response, 'mergeResult.decison.reasons') ||
            `Employment check failed for for ${JSON.parse(
              sessionStorage.getItem('id'),
            )}!`,
        ),
      );
      message.info(
        _.get(response, 'mergeResult.decison.reasons') ||
          `Employment check failed for for ${JSON.parse(
            sessionStorage.getItem('id'),
          )}!`,
      );
    }
  } catch (err) {
    yield put(checkEmploymentError(err));
    message.error(
      _.get(err, 'mergeResult.decison.reasons') ||
        'Network error occurred! Check connection!',
    );
  }
}

export default function* appSaga() {
  yield takeEvery(USER_DECISION, userDecision);
  yield takeEvery(RELOOK_DECISION, relookDecision);
  yield takeEvery(FETCH_REJECTION_CODES, fetchRejectCodes);
  yield takeEvery(FETCH_HISTORY, fetchHistory);
  yield takeEvery(FETCH_TENURE, fetchLoanTenure);
  yield takeEvery(PASS_WAIVER, passWaiverDecision);
  yield takeEvery(UPLOAD_REQUEST, uploadDoc);
  yield takeEvery(FETCH_FEE, fetchFeeCode);
  yield takeEvery(FETCH_SEND_TO, fetchSendToOptions);
  yield takeEvery(REFER_TO_STAGE, referToStage);
  yield takeEvery(CHECK_EMPLOYMENT, checkEmployment);
}
