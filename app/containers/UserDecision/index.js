import React, { useState, memo, useEffect } from 'react';
import _ from 'lodash';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import useInjectSaga from 'utils/injectSaga';
import useInjectReducer from 'utils/injectReducer';
import { makeSelectAllDocs } from 'containers/LeadDocuments/selectors';

import { createStructuredSelector } from 'reselect';
import { useForm, Controller } from 'react-hook-form';
import ErrorMessage from 'components/ErrorMessage';
import {
  Button,
  Modal,
  Select,
  Radio,
  Col,
  Row,
  Spin,
  Divider,
  Input,
  Form,
  message,
  Checkbox,
  InputNumber,
  Space,
  Upload,
} from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { useLocation } from 'react-router-dom';
import { STATE_GSTS, STATUS_WISE_COLOR } from '../../utils/constants';
import { makeSelectUserDecision } from './selectors';
import {
  makeSelectApplicantDetails,
  makeSelectApplicantDetails2,
} from '../ApplicantDetails/selectors';
import { makeSelectApplications } from '../CreditPortal/selectors';
import {
  userDecision,
  relookDecision,
  fetchRejectionCodes,
  fetchRejectionCodesSuccess,
  loadRemarks,
  fetchLoanTenure,
  passWaiver,
  uploadRequest,
  fetchFeeCode,
  fetchSendToOptions,
  referToStage,
  checkEmployment,
  resetEmploymentCheck,
} from './actions';
import reducer from './reducer';
import saga from './saga';
import { REJECTION_CODES } from './constants';

const { TextArea } = Input;
const { Option } = Select;

const responsiveColumns = {
  xs: 24,
  sm: 24,
  md: 24,
  lg: 24,
  xl: 24,
};

const responsiveColumnsWaiver = {
  xs: 24,
  sm: 24,
  md: 12,
  lg: 12,
  xl: 12,
};

const FORM_SCREENS = [
  'applicantDetails',
  'loanDetails',
  'bankDetails',
  'otherDetails',
];

const CUSTOMER_STAGE_OPTIONS = [
  { label: 'Insurance', value: '11.0' },
  { label: 'E-Mandate', value: '12.0' },
  { label: 'Clickwrap', value: '14.0' },
  { label: 'DKYC', value: '15.0' },
];

const UploadDiv = styled.div`
  margin-bottom: 10px;
  margin-left: 10px;
`;

function UserDecision({
  status,
  title,
  referTo,
  userDecisionData,
  applicantDetails,
  creditPortal,
  docsData,
  dispatch,
  applicationsData,
  history,
}) {
  const loc = useLocation();
  const activity = sessionStorage.getItem('activity');
  const {
    loading = false,
    response,
    rejectionCodes = [],
    remarks = [],
    masterLoanTenureData = {},
    isFileUploaded = false,
    masterFeeCodeData = {},
    masterSendToData = {},
    employmentCheckResult = {},
  } = userDecisionData;

  const loanChargesLabel = Object.keys(masterFeeCodeData)[0]
    ? Object.keys(masterFeeCodeData)[0]
    : 'Processing Fee';
  const loanChargesLabelType = Object.values(masterFeeCodeData)[0]
    ? Object.values(masterFeeCodeData)[0].split('^')[1]
    : '';
  const loanChargeCode = Object.values(masterFeeCodeData)[0]
    ? Object.values(masterFeeCodeData)[0].split('^')[0]
    : 'pf';

  const {
    formValidationErrors = {},
    entity: { users = [], partner = '', product = '' } = {},
    appGsts = [],
    masterData: { MANDATORY_DOCS = [] } = {},
    delphiData = {},
  } = applicantDetails;
  const mainApplicant =
    _.find(users, { appLMS: { role: 'Applicant' } }, {}) || {};
  console.log('mainApplicant', mainApplicant);
  if (
    mainApplicant.type === 'COMPANY' &&
    !FORM_SCREENS.includes('entityDetails')
  ) {
    FORM_SCREENS.push('entityDetails');
  }
  const [showModal, setShowModal] = useState(false);
  const { currentApplication = {} } = applicationsData;
  const [customerStage, setCustomerStage] = useState([]);
  const [showCustomerStage, setShowCustomerStage] = useState(false);
  const [fileToUpload, setFileToUpload] = useState('');
  const [objType, setObjType] = useState('');
  const [sendTo, setSendTo] = useState('');

  function handleFileTypeChange(value) {
    if (value !== 'default') {
      setFileToUpload(value);
      setObjType('app');
    } else {
      setFileToUpload('');
      setObjType('');
    }
  }

  useEffect(() => {
    if (
      applicantDetails &&
      applicantDetails.entity &&
      applicantDetails.entity.loanOffers
    ) {
      const fee = _.get(
        _.orderBy(_.get(applicantDetails, 'entity.loanCharges'), 'id', 'desc'),
        '[0].amount',
      );
      const amount = _.get(
        _.orderBy(
          _.filter(
            _.get(applicantDetails, 'entity.loanOffers'),
            loanOffer =>
              loanOffer.type === 'credit_amount' ||
              loanOffer.type === 'eligible_amount',
          ),
          'id',
          'desc',
        ),
        '[0].loanAmount',
      );
      const roi = _.get(
        _.orderBy(
          _.filter(
            _.get(applicantDetails, 'entity.loanOffers'),
            loanOffer =>
              loanOffer.type === 'credit_amount' ||
              loanOffer.type === 'eligible_amount',
          ),
          'id',
          'desc',
        ),
        '[0].roi',
      );

      const tenure = _.get(
        _.orderBy(
          _.filter(
            _.get(applicantDetails, 'entity.loanOffers'),
            loanOffer =>
              loanOffer.type === 'credit_amount' ||
              loanOffer.type === 'eligible_amount',
          ),
          'id',
          'desc',
        ),
        '[0].loanTenure',
      );

      // debugger;
      setValue([
        { 'loanCharges.amount': fee },
        { 'loanOffers.loanAmount': amount },
        { 'loanOffers.roi': roi },
        { 'loanOffers.loanTenure': tenure },
      ]);
      reset(defaultValues);
    }
  }, [applicantDetails.entity]);

  const defaultValues = {
    'loanCharges.amount': _.get(
      _.orderBy(_.get(applicantDetails, 'entity.loanCharges'), 'id', 'desc'),
      '[0].amount',
    ),
    'loanOffers.loanAmount': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].loanAmount',
    ),
    'loanOffers.roi': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].roi',
    ),
    'loanOffers.loanTenure': _.get(
      _.orderBy(
        _.filter(
          _.get(applicantDetails, 'entity.loanOffers'),
          loanOffer =>
            loanOffer.type === 'credit_amount' ||
            loanOffer.type === 'eligible_amount',
        ),
        'id',
        'desc',
      ),
      '[0].loanTenure',
    ),
  };

  const {
    handleSubmit,
    errors,
    control,
    reset,
    setValue,
    register,
    triggerValidation,
  } = useForm({
    mode: 'onChange',
    defaultValues,
  });

  useEffect(() => {
    triggerValidation();
  }, []);

  useEffect(() => {
    dispatch(resetEmploymentCheck());
  }, [JSON.parse(sessionStorage.getItem('id'))]);

  useEffect(() => {
    if (response) {
      setShowModal(false);
      reset();
    }
  }, [response]);

  useEffect(() => {
    if (employmentCheckResult) {
      setShowModal(false);
      reset();
    }
  }, [employmentCheckResult]);

  const onSubmit = data => {
    let { remark } = data;
    const { approver } = data;
    if (status.toUpperCase() === 'CHECK_EMPLOYMENT') {
      return dispatch(checkEmployment());
    }
    if (status.toUpperCase() === 'SENT_BACK' && approver == 'default') {
      return message.info('Select at least one option from dropdown');
    }
    let sentTo = _.findKey(masterSendToData, function(o) {
      return o == approver;
    });
    if (
      showCustomerStage &&
      (data.customerStage && data.customerStage.length === 0)
    ) {
      return message.info('Select at least one option in Customer Stage');
    }
    if (
      loc.pathname.toLowerCase().includes('postsanctionpma/') &&
      sentTo === 'CUSTOMER_REVIEW'
    ) {
      sentTo = '';
      sentTo = data.customerStage.sort().join(',');
    }
    if (status.toUpperCase() === 'REJECTED') {
      if (data.rejectCode) remark = `${data.rejectCode}-${data.remark}`;
    }
    if (status === 'Relook') {
      dispatch(
        relookDecision({
          status: status.toUpperCase(),
          remark,
        }),
      );
    } else if (status === 'Waiver') {
      if (data.loanCharges.amount) {
        if (
          loanChargesLabelType == 'PER' &&
          parseInt(data.loanCharges.amount) > 100
        ) {
          message.info('Processing fee should be less than 100');
          return;
        }
        if (
          loanChargesLabelType == 'FIXED' &&
          parseInt(data.loanCharges.amount) < 100
        ) {
          message.info('Restructuring fee should be greater than 100');
          return;
        }
      }
      if (isFileUploaded) {
        dispatch(passWaiver(data, loanChargeCode));
      } else {
        message.info('Please upload approval email before submitting!');
      }
    } else if (status.toUpperCase() == 'REFERRED') {
      dispatch(referToStage(currentApplication, referTo));
    } else {
      dispatch(
        userDecision({
          remark,
          status,
          sentTo,
        }),
      );
    }
  };

  useEffect(() => {
    if (showReasonDropdown()) dispatch(fetchRejectionCodes(partner, product));
    if (status.toUpperCase() === 'WAIVER') {
      dispatch(
        fetchLoanTenure(
          applicantDetails.entity.partner,
          applicantDetails.entity.product,
        ),
      );
      dispatch(fetchFeeCode(applicantDetails.entity.partner));
    }
    if (status.toUpperCase() === 'SENT_BACK') {
      dispatch(fetchSendToOptions(activity, product));
    }
    if (
      // loc.pathname.toLowerCase().includes('applicantdetailspma/') &&
      sessionStorage.getItem('activity') == 'CREDIT_PSV' &&
      partner == 'HFSAPP'
    ) {
      dispatch(loadRemarks(JSON.parse(sessionStorage.getItem('id'))));
    }
  }, [partner, product]);

  useEffect(() => {
    if (status.toUpperCase() === 'WAIVER')
      dispatch(loadRemarks(JSON.parse(sessionStorage.getItem('id'))));
  }, []);

  const setWaiver = () => {
    const opsReviewCheck = remarks.filter(
      item => item.user_activity == 'OPS_REVIEW',
    );
    if (opsReviewCheck.length == 0) {
      setShowModal(true);
    } else {
      message.info('Please process the request from Post Sanction Queue!');
    }
  };

  const setApprove = () => {
    const {
      entity: {
        users = [],
        additionalData: { data: { deviations = [] } = {} } = {},
      } = {},
    } = creditPortal;

    if (formValidationErrors) {
      if (
        (partner == 'HFSAPP',
        users.length > 1 && !FORM_SCREENS.includes('guarantorDetails'))
      ) {
        FORM_SCREENS.push('guarantorDetails');
      }
      let allFormChecked = !creditPortal.isNoEntity
        ? FORM_SCREENS.every(key =>
            Object.keys(formValidationErrors).includes(key),
          )
        : FORM_SCREENS.filter(item => item !== 'entityDetails').every(key =>
            Object.keys(formValidationErrors).includes(key),
          );

      if (partner == 'DL') {
        if (!formValidationErrors.applicantDetails) {
          allFormChecked = true;
          allFormChecked = FORM_SCREENS.filter(
            item => item !== 'applicantDetails',
          ).every(key => Object.keys(formValidationErrors).includes(key));
        }
      }

      if (partner == 'HFSAPP') {
        if (
          !formValidationErrors.guarantorDetails ||
          !formValidationErrors.applicantDetails
        ) {
          allFormChecked = true;
          allFormChecked = FORM_SCREENS.filter(
            item => item !== 'guarantorDetails' && item !== 'applicantDetails',
          ).every(key => Object.keys(formValidationErrors).includes(key));
        }
      }

      if (!allFormChecked) {
        message.info(
          'Please go to all the tabs (all Co-Applicants as well) and check for mandatory fields before Approving!',
        );
        return;
      }
      const noErrorsInAllForms = Object.keys(formValidationErrors).every(
        key => {
          if (
            key === 'applicantDetails' ||
            key === 'entityDetails' ||
            key === 'guarantorDetails'
          ) {
            return Object.values(formValidationErrors[key]).every(
              applicantError => applicantError === true,
            );
          }
          return formValidationErrors[key] === true;
        },
      );
      if (!noErrorsInAllForms) {
        message.info('Please fill all the mandatory fields!');
        return;
      }
    }

    // the below condition checks if gst is from the same state as the office address of the entity
    if (users && users.length > 0) {
      let isGstnValid = true;
      const entityUsers = users.filter(user => user.type === 'COMPANY');
      entityUsers.forEach(user => {
        const gstn =
          _.get(_.orderBy(user.appGsts, 'id', 'desc'), '[0].gstn', '') || '';
        const officeAddressState = _.get(
          _.orderBy(_.get(user, 'contactibilities'), 'id', 'desc'),
          '[0].state',
        );
        // console.log('state', STATE_GSTS[gstn.substring(0, 2)]);
        if (officeAddressState && gstn) {
          if (
            STATE_GSTS[gstn.substring(0, 2)] !== _.upperCase(officeAddressState)
          ) {
            message.info(
              'GSTN Number is not of the State where the Office Address, please change either of two!',
            );
            isGstnValid = false;
          }
        }
      });
      if (!isGstnValid) return;
    }

    if (!docsData.length && !creditPortal.isNoEntity) {
      message.info('Please review/upload all mandatory documents');
      return;
    }
    let mandatoryDocList = [
      'bank statement',
      'cibil bureau report',
      'commercial cibil bureau report',
      'pd questionnaire',
    ];
    let uploadedDocsList = _.map(
      _.uniqBy(
        _.filter(
          docsData,
          doc =>
            mandatoryDocList.includes(doc.type.toLowerCase()) &&
            (_.get(doc, 'fileAttrs.isDocumentActive') === true ||
              _.get(doc, 'fileAttrs.isDocumentActive') === undefined),
        ),
        'type',
      ),
      item => item.type.toLowerCase(),
      // ),
    );

    if (partner === 'HFSAPP') {
      if (activity === 'CREDIT_PSV') {
        mandatoryDocList = MANDATORY_DOCS.map(item => item.code);
        uploadedDocsList = _.map(
          _.uniqBy(
            _.filter(
              docsData,
              doc =>
                mandatoryDocList.includes(_.get(doc, 'fileAttrs.docName')) &&
                (_.get(doc, 'fileAttrs.isDocumentActive') === true ||
                  _.get(doc, 'fileAttrs.isDocumentActive') === undefined),
            ),
            data => _.get(data, 'fileAttrs.docName'),
          ),
          item => _.get(item, 'fileAttrs.docName'),
        );
      } else {
        mandatoryDocList = [];
      }
    }

    const pendingDocs = _.join(
      _.filter(mandatoryDocList, doc => !uploadedDocsList.includes(doc)),
      ', ',
    );
    if (
      mandatoryDocList.length > uploadedDocsList.length &&
      (!creditPortal.isNoEntity || partner === 'HFSAPP')
    ) {
      message.info(`Please upload ${pendingDocs} documents`);
      return;
    }

    if (
      partner !== 'HFSAPP' &&
      deviations.length === 0 &&
      !creditPortal.isNoEntity
    ) {
      message.info(
        'Application cannot be approved without at least one deviation, either fill Loan Amount in Loan Details Section or fill Deviation in the Other Details Section!',
      );
      return;
    }
    setShowModal(true);
  };

  const onClick = () => {
    if (status.toUpperCase() === 'APPROVED') setApprove();
    else if (status.toUpperCase() === 'WAIVER') setWaiver();
    else setShowModal(true);
  };

  const checkPerfiosOutput = () => {
    if (
      _.get(employmentCheckResult, 'mergeResult.decison.status') &&
      status.toUpperCase() === 'CHECK_EMPLOYMENT'
    )
      return false;
    if (
      _.get(employmentCheckResult, 'mergeResult.decison.status') === 'Decline'
    ) {
      if (status.toUpperCase() === 'APPROVED') return false;
      if (status.toUpperCase() === 'REJECTED') return true;
      return false;
    }
    if (
      _.get(employmentCheckResult, 'mergeResult.decison.status') === 'Accept'
    ) {
      return true;
    }
    if (
      status.toUpperCase() === 'APPROVED' ||
      status.toUpperCase() === 'REJECTED'
    )
      return false;
    const appId = JSON.parse(sessionStorage.getItem('id')) || '';
    const displayData = delphiData[appId];
    let perfiosFlag = false;
    if (displayData) {
      displayData.forEach(data => {
        if (data.stage === 'perfios') {
          const decision = _.get(
            data,
            'response.bureau_response.decision.status',
          );
          const evPerfiosMatch = _.get(
            data,
            'response.bureau_response.decision.ev_perfios_match',
          );
          if (decision === 'Decline' && !evPerfiosMatch) perfiosFlag = true;
        }
      });
    }
    return perfiosFlag;
  };

  const ifShowButton = () => {
    switch (status.toUpperCase()) {
      case 'SENT_BACK':
        if (Object.keys(masterSendToData).length > 0) return true;
        /* if (
          loc.pathname.toLowerCase().includes('applicantdetailspma/') &&
          !creditPortal.isNoEntity
        ) {
          return true;
        } */
        return false;
      case 'REFERRED':
        if (
          remarks &&
          remarks.length > 0 &&
          remarks.filter(
            item =>
              item.state == 'ACTIVE' && item.user_activity == 'CREDIT_FCU',
          ).length == 0 &&
          partner !== 'HFSAPP'
        ) {
          return true;
        }
        if (
          partner === 'HFSAPP' &&
          currentApplication.userActivity === 'CREDIT_FCU' &&
          referTo === 'Credit'
        )
          return true;
        return (
          partner === 'HFSAPP' &&
          currentApplication.userActivity === 'CREDIT_REVIEW' &&
          referTo === 'FCU'
        );
      case 'FORWARDED':
        if (
          (loc.pathname.toLowerCase().includes('applicantdetailspma/') ||
            loc.pathname.toLowerCase().includes('employmentreviewpma/') ||
            loc.pathname.toLowerCase().includes('incomereviewpma/')) &&
          partner !== 'HFSAPP' &&
          creditPortal.isNoEntity
        )
          return true;
        return false;
      case 'RELOOK':
        if (
          (currentApplication.state === 'REJECTED' ||
            currentApplication.state === 'AUTO_REJECTED') &&
          (partner === 'HFSAPP' ||
            !loc.pathname.toLowerCase().includes('fraudreviewclo/'))
        )
          return true;
        return false;
      case 'WAIVER':
        if (
          (currentApplication.state === 'APPROVED' ||
            currentApplication.state === 'AUTO_APPROVED') &&
          loc.pathname.toLowerCase().includes('incomereviewclo/')
        )
          return true;
        return false;
      case 'CHECK_EMPLOYMENT': {
        if (
          product === 'PL' &&
          (partner === 'XSELL' || partner === 'D2C') &&
          loc.pathname.toLowerCase().includes('incomereviewpma/')
        )
          return checkPerfiosOutput();
        return false;
      }
      case 'APPROVED':
        if (
          product === 'PL' &&
          (partner === 'XSELL' || partner === 'D2C') &&
          loc.pathname.toLowerCase().includes('incomereviewpma/')
        )
          return checkPerfiosOutput();
        return true;
      case 'REJECTED':
        if (
          product === 'PL' &&
          (partner === 'XSELL' || partner === 'D2C') &&
          loc.pathname.toLowerCase().includes('incomereviewpma/')
        )
          return checkPerfiosOutput();
        return true;
      default:
        return true;
    }
  };

  const onChange = checkedValues => {
    setCustomerStage(checkedValues);
  };

  function getRejectionCodeOptionList() {
    const optionList = [];
    optionList.push(
      <Option disabled value selected key="Select your Reject/Approve Code">
        Select your Reject/Approve Code
      </Option>,
    );
    rejectionCodes.forEach(item => {
      optionList.push(
        <Option key={item.code} value={item.code}>
          {item.code} {item.title ? ` - ${item.title}` : ''}
        </Option>,
      );
    });
    return optionList;
  }

  const onApproverChange = value => {
    if (value === 'Customer Review') setShowCustomerStage(true);
    else setShowCustomerStage(false);
    return value;
  };

  const handleFileUpload = (file, item = fileToUpload) => {
    const appId = JSON.parse(sessionStorage.getItem('id'));
    const fileName = `${appId}-${file.file.name}`;
    const reader = new FileReader();
    reader.onload = function() {
      dispatch(
        uploadRequest(
          { ...file.file, name: fileName },
          objType,
          appId,
          fileToUpload,
          reader.result,
          file.file,
          '',
        ),
      );
    };
    reader.readAsDataURL(file.file);
  };

  const showReasonDropdown = () => {
    if (product === 'PL')
      return (
        status.toUpperCase() === 'REJECTED' ||
        status.toUpperCase() === 'APPROVED'
      );
    return status.toUpperCase() === 'REJECTED';
  };

  return (
    <>
      {ifShowButton() && (
        <Button type="primary" onClick={onClick}>
          {title}
        </Button>
      )}
      <Modal
        title={title}
        visible={showModal}
        footer={null}
        onCancel={() => {
          setShowModal(false);
          setShowCustomerStage(false);
          reset();
        }}
      >
        <Spin spinning={loading}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Row gutter={[16, 16]}>
              {showReasonDropdown() &&
                rejectionCodes &&
                rejectionCodes.length > 0 && (
                  <Col {...responsiveColumns}>
                    <Form.Item label="Reject/Approve Code">
                      <Controller
                        name="rejectCode"
                        control={control}
                        // onChange={([event]) => event.target.value}
                        rules={{
                          required: {
                            value: true,
                            message: 'Please select a Reject/Approve Code',
                          },
                        }}
                        as={
                          <Select
                            size="large"
                            showSearch
                            placeholder="Select Reject/Approve Code"
                          >
                            {getRejectionCodeOptionList()}
                          </Select>
                        }
                      />
                      <ErrorMessage name="rejectCode" errors={errors} />
                    </Form.Item>
                  </Col>
                )}
              {status.toUpperCase() === 'WAIVER' && (
                <>
                  <Col {...responsiveColumnsWaiver}>
                    <Form.Item label="Loan Amount">
                      <Controller
                        // onChange={([e]) => e.target.value}
                        id="loanOffers.loanAmount"
                        control={control}
                        name="loanOffers.loanAmount"
                        type="number"
                        ref={register}
                        defaultValue={defaultValues['loanOffers.loanAmount']}
                        errors={errors}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                          pattern: {
                            value: /[0-9]/i,
                            message: 'Please enter valid number',
                          },
                          min: {
                            value: 10000,
                            message: 'Loan amount should be > 10,000',
                          },
                          validate: value =>
                            Number.isInteger(parseFloat(value)) ||
                            'value should not be in decimal',
                        }}
                        as={<InputNumber size="large" />}
                      />
                      <ErrorMessage
                        name="loanOffers.loanAmount"
                        errors={errors}
                      />
                    </Form.Item>
                  </Col>
                  <Col {...responsiveColumnsWaiver}>
                    <Form.Item label="Rate Of Interest(roi)">
                      <Controller
                        // onChange={([e]) => e.target.value}
                        id="loanOffers.roi"
                        control={control}
                        name="loanOffers.roi"
                        type="string"
                        ref={register}
                        defaultValue={defaultValues['loanOffers.roi']}
                        errors={errors}
                        rules={{
                          required: {
                            value: true,
                            message: 'This field cannot be left empty',
                          },
                        }}
                        as={<Input size="large" />}
                      />
                      <ErrorMessage name="loanOffers.roi" errors={errors} />
                    </Form.Item>
                  </Col>
                  {Object.values(masterLoanTenureData).length == 0 && (
                    <Col {...responsiveColumnsWaiver}>
                      <Form.Item label="Number of Installments (in months)">
                        <Controller
                          // onChange={([e]) => e.target.value}
                          id="loanOffers.loanTenure"
                          control={control}
                          name="loanOffers.loanTenure"
                          type="number"
                          ref={register}
                          errors={errors}
                          defaultValue={defaultValues['loanOffers.loanTenure']}
                          rules={{
                            required: {
                              value: true,
                              message: 'This field cannot be left empty',
                            },
                          }}
                          as={<Input size="large" />}
                        />
                        <ErrorMessage
                          name="loanOffers.loanTenure"
                          errors={errors}
                        />
                      </Form.Item>
                    </Col>
                  )}
                  {Object.values(masterLoanTenureData).length > 0 && (
                    <Col {...responsiveColumnsWaiver}>
                      <Form.Item label="Number of Installments (in months)">
                        <Controller
                          // onChange={([e]) => e.target.value}
                          id="loanOffers.loanTenure"
                          control={control}
                          name="loanOffers.loanTenure"
                          type="select"
                          ref={register}
                          errors={errors}
                          defaultValue={defaultValues['loanOffers.loanTenure']}
                          rules={{
                            required: {
                              value: true,
                              message: 'This field cannot be left empty',
                            },
                          }}
                          as={
                            <Select
                              size="large"
                              placeholder="Select loan tenure"
                            >
                              {(Object.values(masterLoanTenureData) || []).map(
                                (item, index) => (
                                  <Option value={item}>{item}</Option>
                                ),
                              )}
                            </Select>
                          }
                        />
                        <ErrorMessage
                          name="loanOffers.loanTenure"
                          errors={errors}
                        />
                      </Form.Item>
                    </Col>
                  )}
                  <Col {...responsiveColumnsWaiver}>
                    <Form.Item label={loanChargesLabel}>
                      <Controller
                        // onChange={([e]) => e.target.value}
                        id="loanCharges.amount"
                        control={control}
                        name="loanCharges.amount"
                        ref={register}
                        type="number"
                        errors={errors}
                        defaultValue={defaultValues['loanCharges.amount']}
                        rules={{
                          required: false,
                          pattern: {
                            value: /^[0-9]+([.][0-9]+)?$/,
                            message: 'Please enter valid number',
                          },
                        }}
                        as={<InputNumber size="large" />}
                      />
                      <ErrorMessage name="loanCharges.amount" errors={errors} />
                    </Form.Item>
                  </Col>
                  <UploadDiv>
                    <Space>
                      <Upload
                        // accept=".jpg, .png, .pdf"
                        showUploadList={false}
                        customRequest={handleFileUpload}
                        disabled={fileToUpload === '' || objType === ''}
                      >
                        <Button>
                          <UploadOutlined /> Upload
                        </Button>
                      </Upload>
                      <Select
                        defaultValue="default"
                        style={{ width: 300 }}
                        showSearch
                        onChange={handleFileTypeChange}
                      >
                        <Option value="default">
                          Select document to upload
                        </Option>
                        <Option value="Approval Email">
                          {'Approval Email'}
                        </Option>
                      </Select>
                    </Space>
                  </UploadDiv>
                </>
              )}
              {status.toUpperCase() !== 'CHECK_EMPLOYMENT' && (
                <Col {...responsiveColumns}>
                  <Form.Item label="Remarks">
                    <Controller
                      onChange={([e]) => e.target.value}
                      id="remark"
                      control={control}
                      name="remark"
                      type="textarea"
                      errors={errors}
                      rules={{
                        required: {
                          value: true,
                          message: 'This Field should not be empty!',
                        },
                      }}
                      as={
                        <TextArea
                          placeholder="Remarks"
                          rows={4}
                          labelHtml="Remarks"
                        />
                      }
                    />
                    <ErrorMessage name="remark" errors={errors} />
                  </Form.Item>
                </Col>
              )}
              {status.toUpperCase() === 'SENT_BACK' && (
                <>
                  <Col {...responsiveColumns}>
                    <Form.Item label="Send back to">
                      <Controller
                        name="approver"
                        control={control}
                        onChange={([event]) => onApproverChange(event)}
                        rules={{
                          required: {
                            value: true,
                            message: 'Please select either of the options',
                          },
                        }}
                        as={
                          <Select
                            defaultValue="default"
                            style={{ width: 200 }}
                            showSearch
                            // onChange={handleSendToChange}
                          >
                            <Option value="default">Select option</Option>
                            {masterSendToData &&
                              Object.values(masterSendToData).length > 0 &&
                              Object.values(masterSendToData).map(item => (
                                <Option value={item}>{item}</Option>
                              ))}
                          </Select>
                        }
                      />
                      <ErrorMessage name="approver" errors={errors} />
                    </Form.Item>
                  </Col>
                  {showCustomerStage && (
                    <Col {...responsiveColumns}>
                      <Form.Item label="Customer Stage">
                        <Controller
                          name="customerStage"
                          control={control}
                          // onChange={event => onChange(event)}
                          // value={customerStage}
                          defaultValue={
                            partner === 'RLA' ? ['11.0', '14.0'] : ['14.0']
                          }
                          rules={{
                            required: {
                              value: true,
                              message:
                                'Please select one or more Customer Stages',
                            },
                          }}
                          as={
                            <Checkbox.Group
                              name="customerStage"
                              options={CUSTOMER_STAGE_OPTIONS}
                              onChange={onChange}
                              value={customerStage}
                            />
                          }
                        />
                        <ErrorMessage name="customerStage" errors={errors} />
                      </Form.Item>
                    </Col>
                  )}
                </>
              )}
            </Row>
            <Col {...responsiveColumns}>
              <Button htmlType="submit">Submit</Button>
            </Col>
          </form>
        </Spin>
      </Modal>
    </>
  );
}

UserDecision.propTypes = {
  dispatch: PropTypes.func.isRequired,
  userDecisionData: PropTypes.object,
  creditPortal: PropTypes.object,
  docsData: PropTypes.array,
  applicationsData: PropTypes.object,
};

UserDecision.defaultProps = {
  userDecisionData: {},
  creditPortal: {},
  applicationsData: {},
};

const withReducer = useInjectReducer({ key: 'userDecision', reducer });
const withSaga = useInjectSaga({ key: 'userDecision', saga });

const mapStateToProps = createStructuredSelector({
  userDecisionData: makeSelectUserDecision(),
  creditPortal: makeSelectApplicantDetails(),
  docsData: makeSelectAllDocs(),
  applicantDetails: makeSelectApplicantDetails2(),
  applicationsData: makeSelectApplications(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withReducer,
  withSaga,
  withConnect,
  memo,
)(UserDecision);
