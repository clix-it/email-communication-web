export const USER_DECISION = 'USER_DECISION';
export const RELOOK_DECISION = 'RELOOK_DECISION';
export const PASS_WAIVER = 'PASS_WAIVER';
export const UPLOAD_REQUEST = 'UPLOAD_REQUEST';
export const UPLOAD_SUCCESS = 'UPLOAD_SUCCESS';
export const UPLOAD_FAIL = 'UPLOAD_FAIL';
export const SET_ERROR = 'SET_ERROR';
export const SET_SUCCESS = 'SET_SUCCESS';
export const FETCH_REJECTION_CODES = 'FETCH_REJECTION_CODES';
export const FETCH_REJECTION_CODES_ERROR = 'FETCH_REJECTION_CODES_ERROR';
export const FETCH_REJECTION_CODES_SUCCESS = 'FETCH_REJECTION_CODES_SUCCESS';

export const FETCH_FEE = 'FETCH_FEE';
export const FETCH_FEE_SUCCESS = 'FETCH_FEE_SUCCESS';
export const FETCH_FEE_ERROR = 'FETCH_FEE_ERROR';

export const FETCH_HISTORY = 'LOAD_REMARKS';
export const FETCH_HISTORY_SUCCESS = 'FETCH_HISTORY_SUCCESS';
export const FETCH_HISTORY_ERROR = 'FETCH_HISTORY_ERROR';

export const FETCH_TENURE = 'FETCH_TENURE';
export const FETCH_TENURE_SUCCESS = 'FETCH_TENURE_SUCCESS';
export const FETCH_TENURE_ERROR = 'FETCH_TENURE_ERROR';

export const FETCH_SEND_TO = 'FETCH_SEND_TO';
export const FETCH_SEND_TO_SUCCESS = 'FETCH_SEND_TO_SUCCESS';
export const FETCH_SEND_TO_ERROR = 'FETCH_SEND_TO_ERROR';

export const REFER_TO_STAGE = 'REFER_TO_STAGE';
export const REFER_TO_STAGE_SUCCESS = 'REFER_TO_STAGE_UCCESS';
export const REFER_TO_STAGE_ERROR = 'REFER_TO_STAGE_ERROR';

export const CHECK_EMPLOYMENT = 'CHECK_EMPLOYMENT';
export const CHECK_EMPLOYMENT_SUCCESS = 'CHECK_EMPLOYMENT_SUCCESS';
export const CHECK_EMPLOYMENT_ERROR = 'CHECK_EMPLOYMENT_ERROR';

export const RESET_EMPLOYMENT_CHECK = 'RESET_EMPLOYMENT_CHECK';

export const REJECTION_CODES = [
  { code: 'CODES', title: '' },
  { code: 'RCFU', title: 'FCU Negative' },
  { code: 'RCNI ', title: 'Customer not interested' },
  { code: 'RPD', title: 'Negative tele / Video PD' },
  {
    code: 'RNOGO',
    title: 'No GO / Negative industry ( Applicable for fintech)',
  },
  {
    code: 'RCBL',
    title:
      'Negative bureau check (As on 29 Feb >60 DPD or > 25 Crore total exposure norms)',
  },
  { code: 'RCLOSED', title: 'Existing LAN already closed by customer' },
  { code: 'RTURNOVER', title: 'Turnover is more then prescribed limit' },
  { code: 'ROTHERS', title: 'Other Reasons' },
  {
    code: 'RDUPLICATE',
    title:
      'Duplicate entry/ processed at pennant / processed by some other users',
  },
];
