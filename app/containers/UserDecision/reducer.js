import produce from 'immer';
import {
  USER_DECISION,
  SET_ERROR,
  SET_SUCCESS,
  RELOOK_DECISION,
  PASS_WAIVER,
  FETCH_REJECTION_CODES_ERROR,
  FETCH_REJECTION_CODES_SUCCESS,
  FETCH_HISTORY,
  FETCH_HISTORY_SUCCESS,
  FETCH_HISTORY_ERROR,
  FETCH_TENURE_SUCCESS,
  FETCH_TENURE_ERROR,
  UPLOAD_REQUEST,
  UPLOAD_SUCCESS,
  UPLOAD_FAIL,
  FETCH_FEE_SUCCESS,
  FETCH_FEE_ERROR,
  FETCH_SEND_TO_SUCCESS,
  FETCH_SEND_TO_ERROR,
  CHECK_EMPLOYMENT_SUCCESS,
  CHECK_EMPLOYMENT_ERROR,
  RESET_EMPLOYMENT_CHECK,
} from './constants';
import { LOCATION_CHANGE } from 'react-router-redux';

const initialState = {
  error: {},
  success: {},
  response: {},
  rejectionCodes: [],
  remarks: [],
  masterLoanTenureData: {},
  masterFeeCodeData: {},
  isFileUploading: false,
  isFileUploaded: false,
  FileUploadError: '',
  masterSendToData: {},
  employmentCheckResult: {},
};

/* eslint-disable default-case, no-param-reassign */
const userDecisionReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case USER_DECISION:
        draft.loading = true;
        draft.response = false;
        draft.error = false;
        break;
      case RELOOK_DECISION:
        draft.loading = true;
        draft.response = action.payload.response;
        draft.error = false;
        break;
      case PASS_WAIVER:
        draft.loading = true;
        draft.response = '';
        break;
      case SET_SUCCESS:
        draft.loading = false;
        draft.response = action.payload.success;
        draft.error = false;
        break;
      case FETCH_REJECTION_CODES_SUCCESS: {
        const tempObj2 = [];
        action.data.forEach(item => {
          tempObj2.push({
            code: item.cmCode,
            title: item.cmValue,
          });
        });
        draft.rejectionCodes = tempObj2;
        break;
      }
      case FETCH_REJECTION_CODES_ERROR:
        draft.rejectionCodes = [];
        break;

      case SET_ERROR:
        draft.loading = false;
        draft.response = false;
        draft.error = action.payload.error;
        break;
      case FETCH_HISTORY:
        draft.loading = true;
        break;
      case FETCH_HISTORY_SUCCESS:
        draft.remarks = action.payload.data;
        draft.loading = false;
        break;
      case FETCH_HISTORY_ERROR:
        draft.remarks = [];
        draft.loading = false;
        break;
      case FETCH_TENURE_SUCCESS:
        let tempObj = {};
        action.data.forEach(item => {
          tempObj[item.cmValue] = item.cmCode;
        });
        draft.masterLoanTenureData = tempObj;
        break;
      case FETCH_TENURE_ERROR:
        draft.masterLoanTenureData = {};
        break;
      case UPLOAD_REQUEST:
        draft.isFileUploading = true;
        draft.isFileUploaded = false;
        draft.FileUploadError = '';
        break;
      case UPLOAD_SUCCESS:
        draft.isFileUploading = false;
        draft.isFileUploaded = true;
        draft.FileUploadError = '';
        break;
      case UPLOAD_FAIL:
        draft.isFileUploading = false;
        draft.isFileUploaded = false;
        draft.FileUploadError = action.payload.error;
        break;
      case FETCH_FEE_SUCCESS:
        let tempObj3 = {};
        action.data.forEach(item => {
          tempObj3[item.cmValue] = item.cmCode;
        });
        draft.masterFeeCodeData = tempObj3;
        break;
      case FETCH_FEE_ERROR:
        draft.masterFeeCodeData = {};
        break;
      case FETCH_SEND_TO_SUCCESS:
        let tempObj4 = {};
        action.data.forEach(item => {
          tempObj4[item.cmCode] = item.cmValue;
        });
        draft.masterSendToData = tempObj4;
        break;
      case FETCH_SEND_TO_ERROR:
        draft.masterSendToData = {};
        break;
      case LOCATION_CHANGE:
        draft.error = {};
        draft.success = {};
        draft.response = {};
        draft.rejectionCodes = [];
        draft.remarks = [];
        draft.masterLoanTenureData = {};
        draft.isFileUploading = false;
        draft.isFileUploaded = false;
        draft.FileUploadError = '';
        draft.masterSendToData = {};
        break;
      case CHECK_EMPLOYMENT_SUCCESS:
        draft.employmentCheckResult = action.data;
        break;
      case CHECK_EMPLOYMENT_ERROR:
        draft.employmentCheckResult = action.data;
        break;
      case RESET_EMPLOYMENT_CHECK:
        draft.employmentCheckResult = {};
        break;
    }
  });

export default userDecisionReducer;
