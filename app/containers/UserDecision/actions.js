import * as actions from './constants';

export function userDecision(data) {
  return {
    type: actions.USER_DECISION,
    payload: {
      data,
    },
  };
}

export function relookDecision(data) {
  return {
    type: actions.RELOOK_DECISION,
    payload: {
      data,
    },
  };
}

export function passWaiver(data, loanChargeCode) {
  return {
    type: actions.PASS_WAIVER,
    data,
    loanChargeCode,
  };
}

export function setError(error) {
  return {
    type: actions.SET_ERROR,
    payload: {
      error,
    },
  };
}

export function setSuccess(success) {
  return {
    type: actions.SET_SUCCESS,
    payload: {
      success,
    },
  };
}

export function fetchRejectionCodes(partner, product) {
  return {
    type: actions.FETCH_REJECTION_CODES,
    partner,
    product,
  };
}

export function fetchRejectionCodesSuccess(data) {
  return {
    type: actions.FETCH_REJECTION_CODES_SUCCESS,
    data,
  };
}

export function fetchRejectionCodesError(error) {
  return {
    type: actions.FETCH_REJECTION_CODES_ERROR,
    error,
  };
}

export function loadRemarks(appId) {
  return {
    type: actions.FETCH_HISTORY,
    payload: {
      appId,
    },
  };
}

export function loadRemarksSuccess(data) {
  return {
    type: actions.FETCH_HISTORY_SUCCESS,
    payload: {
      data,
    },
  };
}

export function loadRemarksError() {
  return {
    type: actions.FETCH_HISTORY_ERROR,
  };
}

export function fetchLoanTenure(partner, product) {
  // ;
  return {
    type: actions.FETCH_TENURE,
    partner,
    product,
  };
}

export function fetchLoanTenureSuccess(data) {
  // ;
  return {
    type: actions.FETCH_TENURE_SUCCESS,
    data,
  };
}

export function fetchLoanTenureError(error) {
  // ;
  return {
    type: actions.FETCH_TENURE_ERROR,
    error,
  };
}

export function uploadRequest(
  file,
  type,
  id,
  docType,
  binaryData,
  filestoBeSend,
  otherDocumentName,
) {
  return {
    type: actions.UPLOAD_REQUEST,
    payload: {
      file,
      type,
      id,
      docType,
      binaryData,
      filestoBeSend,
      otherDocumentName,
    },
  };
}

export function uploadleadsDocSuccess() {
  return {
    type: actions.UPLOAD_SUCCESS,
  };
}

export function uploadleadsDocFail(error) {
  return {
    type: actions.UPLOAD_FAIL,
    payload: {
      error,
    },
  };
}

export function fetchFeeCode(partner) {
  // ;
  return {
    type: actions.FETCH_FEE,
    partner,
  };
}

export function fetchFeeCodeSuccess(data) {
  // ;
  return {
    type: actions.FETCH_FEE_SUCCESS,
    data,
  };
}

export function fetchFeeCodeError(error) {
  // ;
  return {
    type: actions.FETCH_FEE_ERROR,
    error,
  };
}

export function fetchSendToOptions(stage, product) {
  // ;
  return {
    type: actions.FETCH_SEND_TO,
    stage,
    product,
  };
}



export function fetchSendToOptionsSuccess(data) {
  // ;
  return {
    type: actions.FETCH_SEND_TO_SUCCESS,
    data,
  };
}



export function fetchSendToOptionsError(error) {
  // ;
  return {
    type: actions.FETCH_SEND_TO_ERROR,
    error,
  };
}

export function referToStage(currentApplication, referTo) {
  return {
    type: actions.REFER_TO_STAGE,
    currentApplication,
    referTo,
  };
}

export function referToStageSuccess() {
  return {
    type: actions.REFER_TO_STAGE_SUCCESS,
  };
}

export function referToStageError() {
  return {
    type: actions.REFER_TO_STAGE_ERROR,
  };
}

export function checkEmployment() {
  return {
    type: actions.CHECK_EMPLOYMENT,
  };
}

export function checkEmploymentSuccess(data) {
  return {
    type: actions.CHECK_EMPLOYMENT_SUCCESS,
    data,
  };
}

export function checkEmploymentError(data) {
  return {
    type: actions.CHECK_EMPLOYMENT_ERROR,
    data,
  };
}

export function resetEmploymentCheck() {
  return {
    type: actions.RESET_EMPLOYMENT_CHECK,
  };
}
