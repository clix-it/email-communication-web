/**
 *
 * Asynchronously loads the component for RejectApplication
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
