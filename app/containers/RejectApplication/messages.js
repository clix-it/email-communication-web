/*
 * RejectApplication Messages
 *
 * This contains all the text for the RejectApplication container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.RejectApplication';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the RejectApplication container!',
  },
});
