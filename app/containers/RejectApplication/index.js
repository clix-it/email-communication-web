/**
 *
 * RejectApplication
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectRejectApplication from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

export function RejectApplication() {
  useInjectReducer({ key: 'rejectApplication', reducer });
  useInjectSaga({ key: 'rejectApplication', saga });

  return (
    <div>
      <Helmet>
        <title>RejectApplication</title>
        <meta name="description" content="Description of RejectApplication" />
      </Helmet>
      <FormattedMessage {...messages.header} />
    </div>
  );
}

RejectApplication.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  rejectApplication: makeSelectRejectApplication(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(RejectApplication);
