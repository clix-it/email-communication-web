import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the rejectApplication state domain
 */

const selectRejectApplicationDomain = state =>
  state.rejectApplication || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by RejectApplication
 */

const makeSelectRejectApplication = () =>
  createSelector(
    selectRejectApplicationDomain,
    substate => substate,
  );

export default makeSelectRejectApplication;
export { selectRejectApplicationDomain };
