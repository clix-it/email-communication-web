/*
 *
 * DownloadEmiSchedule actions
 *
 */

import { FETCH_RPS, FETCH_RPS_SUCCESS, FETCH_RPS_FAILURE } from './constants';

export function fetchingRps(params, callFrom) {
  return {
    type: FETCH_RPS,
    params,
    callFrom,
  };
}

export function rpsFetched(response) {
  return {
    type: FETCH_RPS_SUCCESS,
    response,
  };
}

export function rpsFetchFailed(error) {
  return {
    type: FETCH_RPS_FAILURE,
    error,
  };
}
