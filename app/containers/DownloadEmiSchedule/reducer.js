/*
 *
 * DownloadEmiSchedule reducer
 *
 */
import produce from 'immer';
import { LOCATION_CHANGE } from 'react-router-redux';

import { FETCH_RPS, FETCH_RPS_SUCCESS, FETCH_RPS_FAILURE } from './constants';

export const initialState = {
  loading: false,
  error: false,
  response: false,
};

/* eslint-disable default-case, no-param-reassign */
const downloadEmiScheduleReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case FETCH_RPS:
        draft.loading = true;
        draft.error = false;
        draft.params = action.params;
        break;

      case FETCH_RPS_SUCCESS:
        draft.loading = false;
        draft.error = false;
        draft.response = action.response;
        break;

      case FETCH_RPS_FAILURE:
        draft.loading = false;
        draft.error = action.error;
        break;
    }
  });

export default downloadEmiScheduleReducer;
