/**
 *
 * Asynchronously loads the component for DownloadEmiSchedule
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
