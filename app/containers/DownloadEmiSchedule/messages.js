/*
 * DownloadEmiSchedule Messages
 *
 * This contains all the text for the DownloadEmiSchedule container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.DownloadEmiSchedule';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the DownloadEmiSchedule container!',
  },
});
