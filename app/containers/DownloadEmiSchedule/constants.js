/*
 *
 * DownloadEmiSchedule constants
 *
 */

export const FETCH_RPS = 'app/DownloadEmiSchedule/FETCH_RPS';
export const FETCH_RPS_SUCCESS = 'app/DownloadEmiSchedule/FETCH_RPS_SUCCESS';
export const FETCH_RPS_FAILURE = 'app/DownloadEmiSchedule/FETCH_RPS_FAILURE';
