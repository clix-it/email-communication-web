/**
 *
 * DownloadEmiSchedule
 *
 */

import React, { memo, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { DownloadOutlined } from '@ant-design/icons';
import { Spin, Empty } from 'antd';
import styled from 'styled-components';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import _ from 'lodash';
import makeSelectDownloadEmiSchedule from './selectors';
import reducer from './reducer';
import saga from './saga';
import { fetchingRps } from './actions';
import messages from './messages';
import makeSelectExistingLans from '../ExistingLans/selectors';
//import PDFViewer from 'pdf-viewer-reactjs';
import PdfNavigation from '../../components/PdfNavigation';

const SHOW_DOWNLOAD_LMS = ['INDUS', 'PENNANT', 'ORBI'];

const Wrapper = styled.div`
  /* width: 700px; */
  /* height: 700px; */
`;

export function DownloadEmiSchedule({
  loan,
  fetchingRps,
  existingLans,
  downloadEmiSchedule,
}) {
  useInjectReducer({ key: 'downloadEmiSchedule', reducer });
  useInjectSaga({ key: 'downloadEmiSchedule', saga });

  useEffect(() => {
    if (SHOW_DOWNLOAD_LMS.includes(_.upperCase(sourceSystem))) {
      const params = {
        loanAccountNumber,
        responseFormat: 'PDF',
        lms: sourceSystem,
        product,
        partner,
      };
      fetchingRps(params, 'init');
    }
  }, []);

  const {
    loanAccountNumber,
    //  source_System = 'Indus',
    product,
    partner,
  } = loan;
  const sourceSystem = _.get(
    _.filter(existingLans.response, { loanAccountNumber }),
    '[0].lmsSystem',
  );

  const handleFetchRps = () => {
    event.preventDefault();

    const params = {
      loanAccountNumber,
      responseFormat: 'PDF',
      lms: sourceSystem,
      product,
      partner,
    };
    fetchingRps(params);
  };

  if (!SHOW_DOWNLOAD_LMS.includes(_.upperCase(sourceSystem))) return <Empty />;
  return (
    <div>
      <Spin spinning={downloadEmiSchedule.loading}>
        <Link to="#" className="blueTag" onClick={handleFetchRps}>
          <DownloadOutlined /> EMI Schedule
        </Link>
        {downloadEmiSchedule.response &&
          downloadEmiSchedule.response.data &&
          downloadEmiSchedule.response.data.documentBase64 && (
            <Wrapper>
              {/* <PDFViewer
                document={{
                  base64: downloadEmiSchedule.response.data.documentBase64,
                }}
                scale={1.6}
                hideZoom
                hideRotation
                scaleStep={0.2}
                maxScale={3}
                minScale={1}
                navigation={PdfNavigation}
                navbarOnTop
              /> */}
            </Wrapper>
          )}
      </Spin>
    </div>
  );
}

DownloadEmiSchedule.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  downloadEmiSchedule: makeSelectDownloadEmiSchedule(),
  existingLans: makeSelectExistingLans(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchingRps: (params, callFrom = 'download') => dispatch(fetchingRps(params, callFrom)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(DownloadEmiSchedule);
