import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the downloadEmiSchedule state domain
 */

const selectDownloadEmiScheduleDomain = state =>
  state.downloadEmiSchedule || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by DownloadEmiSchedule
 */

const makeSelectDownloadEmiSchedule = () =>
  createSelector(
    selectDownloadEmiScheduleDomain,
    substate => substate,
  );

export default makeSelectDownloadEmiSchedule;
export { selectDownloadEmiScheduleDomain };
