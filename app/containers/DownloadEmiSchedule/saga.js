import { call, put, select, takeEvery } from 'redux-saga/effects';
import { RPS_API_URL } from 'containers/App/constants';
import { rpsFetched, rpsFetchFailed } from './actions';
import { FETCH_RPS } from './constants';
import moment from 'moment';
import env from '../../../environment';
import request from 'utils/request';
import { downloadFile } from '../../utils/helpers';

/**
 * Github repos request/response handler
 */
export function* fetchRps({ params: { lms, product, ...rest }, callFrom }) {
  const requestURL = `${env.SOA_API_URL}/lms_integrator/v1/getRPS?lms=${lms}`;

  try {
    // Call our request helper (see 'utils/request')
    const response = yield call(request, requestURL, {
      method: 'POST',
      headers: env.headers,
      body: JSON.stringify(rest),
    });
    if (callFrom == 'download') {
      yield downloadFile(
        response.data.documentBase64,
        `repayment-schedule-${moment().format('YYYYMMDDkkmm')}.pdf`,
      );
    }
    yield put(rpsFetched(response));
    // );
  } catch (err) {
    yield put(rpsFetchFailed(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* downloadEmiScheduleSaga() {
  // Watches for FETCH RPA actions and calls fetchRps when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  yield takeEvery(FETCH_RPS, fetchRps);
}
